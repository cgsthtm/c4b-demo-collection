// C4B.TestSomething.Cpp.Demo.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Windows.h"

typedef int(*Dllfun)(const unsigned char*, unsigned int, const unsigned int, const char*,
	unsigned char*, unsigned int, unsigned int&);

int main()
{
    std::cout << "Hello World!\n";

	unsigned char Seed[8] = { 0x1e,0xdd,0xa3,0x59,0x1e,0xdd,0xa3,0x59 };//8 byte seed get from RAS
	unsigned char Key[8] = { 0,0,0,0,0,0,0,0 };

	Dllfun funname;
	HINSTANCE hDll;
	hDll = LoadLibrary(TEXT("D:\\MyWork\\SvnSourceCode\\东风柳汽电液转向诊断仪项目\\博世UDS诊断协议相关材料\\SecurityAccess_DFLZ_customer.dll"));
	if (hDll == NULL)
	{
		FreeLibrary(hDll);
	}
	funname = (Dllfun)GetProcAddress(hDll, "GenerateKeyEx");

	if (funname == NULL)
	{
		FreeLibrary(hDll);
	}

	char size = 'A';
	unsigned int SIZE = 8;
	unsigned int* osize = &SIZE;
	const unsigned char* iSeedArray = &Seed[0];
	unsigned int iSeedArraySize = 8;
	const unsigned int iSecurityLevel = 3; //different security levels for different session
	const char* iVariant = &size;
	unsigned char* ioKeyArray = &Key[0];
	unsigned int iKeyArraySize = 8;
	unsigned int& oSize = SIZE;
	int result = 0;
	result = funname(iSeedArray, iSeedArraySize, iSecurityLevel, iVariant, ioKeyArray, iKeyArraySize, oSize);


	//calculation result print
	printf("The Security level is %d:\n", iSecurityLevel);
	printf("The Seed is:\n");
	for (int i = 0;i < 8;i++)
	{
		printf("0x%0x\n", *(iSeedArray + i));
	}

	printf("The Key is:\n");
	for (int j = 0;j < 8;j++)
	{
		printf("0x%0x\n", *(ioKeyArray + j));
	}

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
