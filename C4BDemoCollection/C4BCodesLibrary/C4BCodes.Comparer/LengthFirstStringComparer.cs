﻿// <copyright file="LengthFirstStringComparer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Comparer
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 长度优先字符串比较器.
    /// </summary>
    public class LengthFirstStringComparer : IComparer<string>
    {
        /// <inheritdoc/>
        public int Compare(string x, string y)
        {
            // 首先比较字符串长度，长度大的在前
            int lengthComparison = y.Length.CompareTo(x.Length);
            if (lengthComparison != 0)
            {
                return lengthComparison;
            }
            else
            {
                // 如果长度相同，则按字典序比较
                return string.Compare(x, y, StringComparison.Ordinal);
            }
        }
    }
}
