﻿// <copyright file="RegionStructure.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.FlashSupport
{
    using System.Collections.Generic;

    /// <summary>
    /// 区域结构.
    /// </summary>
    public class RegionStructure
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegionStructure"/> class.
        /// </summary>
        public RegionStructure()
        {
            this.Data = new List<byte>();
        }

        /// <summary>
        /// Gets or sets 起始地址.
        /// </summary>
        public string StartAddress { get; set; }

        /// <summary>
        /// Gets or sets 结束地址.
        /// </summary>
        public string EndAddress { get; set; }

        /// <summary>
        /// Gets or sets 数据.
        /// </summary>
        public List<byte> Data { get; set; }
    }
}
