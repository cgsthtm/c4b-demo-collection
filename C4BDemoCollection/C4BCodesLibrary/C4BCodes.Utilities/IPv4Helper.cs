﻿// <copyright file="IPv4Helper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;

    /// <summary>
    /// IPv4帮助类.
    /// </summary>
    public static class IPv4Helper
    {
        /// <summary>
        /// 当前IP是否合适.
        /// </summary>
        /// <param name="ip">ip.</param>
        /// <returns>true- false-.</returns>
        public static bool CurrentIpIsAdequate(string ip)
        {
            string hostName = Dns.GetHostName();
            IPHostEntry ipHostEntry = Dns.GetHostEntry(hostName);
            var ipAddress = string.Empty;
            bool isContainCurrIp = ipHostEntry.AddressList
                .Where(x => x.AddressFamily == AddressFamily.InterNetwork)
                .Any(x => x.ToString() == ip);
            return ip == "127.0.0.1" || isContainCurrIp;
        }
    }
}
