﻿// <copyright file="Log4NetHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Xml;
    using C4BCodesLibrary.C4BCodes.Model;
    using log4net;
    using log4net.Appender;
    using log4net.Repository.Hierarchy;

    /// <summary>
    /// log4net帮助类.
    /// </summary>
    public sealed class Log4NetHelper // sealed阻止发生派生，而派生可能会增加实例
    {
        /// <summary>
        /// 在第一次引用类的任何成员时创建实例。公共语言运行库负责处理变量初始化.
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger("myLogger");

        private Log4NetHelper()
        {
        }

        /// <summary>
        /// 获取实例.
        /// </summary>
        /// <returns>ILog实例.</returns>
        public static ILog GetInstance()
        {
            return Log;
        }

        /// <summary>
        /// 配置log4net数据库连接字符串.
        /// </summary>
        public static void ConfigureLog4Net()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

            Hierarchy hierarchy = LogManager.GetRepository() as Hierarchy;
            if (hierarchy != null)
            {
                foreach (IAppender appender in hierarchy.GetAppenders())
                {
                    if (appender is AdoNetAppender)
                    {
                        var adoNetAppender = (AdoNetAppender)appender;
                        adoNetAppender.ConnectionString = connectionString;
                        adoNetAppender.ActivateOptions(); // Refresh AdoNetAppenders Settings
                    }
                }
            }

            // 加载配置文件
            var configFile = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\Config\\log4net.config");
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(configFile.FullName);

            // 修改 connectionString (假设你知道如何定位到具体的节点)
            XmlNode connectionStringNode = xmlDoc.SelectSingleNode("//log4net/appender[@name='AdoNetAppender']/connectionString");
            if (connectionStringNode != null)
            {
                connectionStringNode.Attributes["value"].Value = connectionString;
            }

            // 保存修改后的配置文件
            xmlDoc.Save(configFile.FullName);
        }

        /// <summary>
        /// 改变指定appender节点的文件名.
        /// </summary>
        /// <param name="appenderName">appender配置节点.</param>
        /// <param name="newFilename">新文件名称.</param>
        /// <returns>true-成功, false-失败.</returns>
        public static bool ChangeLogFileName(string appenderName, string newFilename)
        {
            log4net.Repository.ILoggerRepository rootRep = Log.Logger.Repository;
            foreach (IAppender iApp in rootRep.GetAppenders())
            {
                if (iApp.Name.CompareTo(appenderName) == 0 && iApp is FileAppender)
                {
                    FileAppender fApp = (FileAppender)iApp;
                    fApp.File = newFilename;
                    fApp.ActivateOptions();
                    return true; // Appender found and name changed to NewFilename
                }
            }

            return false; // appender not found
        }

        /// <summary>
        /// 打印Debug信息.
        /// </summary>
        /// <param name="message">信息.</param>
        /// <param name="e">ex.</param>
        public void Debug(object message, Exception e = null)
        {
            if (e == null)
            {
                Log.Debug(message);
            }
            else
            {
                Log.Debug(message, e);
            }
        }

        /// <summary>
        /// 打印Info信息.
        /// </summary>
        /// <param name="message">信息.</param>
        /// <param name="e">ex.</param>
        public void Info(object message, Exception e = null)
        {
            if (typeof(string) != message.GetType())
            {
                log4net.LogicalThreadContext.Properties["Id"] = ((SystemLog)message).Id;
                log4net.LogicalThreadContext.Properties["LogTime"] = ((SystemLog)message).LogTime;
                log4net.LogicalThreadContext.Properties["LogType"] = ((SystemLog)message).LogType;
                log4net.LogicalThreadContext.Properties["LogContent"] = ((SystemLog)message).LogContent;
                log4net.LogicalThreadContext.Properties["LastModifiedUserId"] = ((SystemLog)message).LastModifiedUserId;
            }
            else
            {
                log4net.LogicalThreadContext.Properties.Clear();
            }

            if (e == null)
            {
                Log.Info(message);
            }
            else
            {
                Log.Info(message, e);
            }
        }

        /// <summary>
        /// 打印Error信息.
        /// </summary>
        /// <param name="message">信息.</param>
        /// <param name="e">ex.</param>
        public void Error(object message, Exception e = null)
        {
            if (e == null)
            {
                Log.Error(message);
            }
            else
            {
                Log.Error(message, e);
            }
        }

        /// <summary>
        /// 打印Fatal信息.
        /// </summary>
        /// <param name="message">信息.</param>
        /// <param name="e">ex.</param>
        public void Fatal(object message, Exception e = null)
        {
            if (e == null)
            {
                Log.Fatal(message);
            }
            else
            {
                Log.Fatal(message, e);
            }
        }

        /// <summary>
        /// 打印Debug信息.
        /// </summary>
        /// <param name="message">信息.</param>
        /// <param name="args">params参数.</param>
        public void DebugFormat(string message, params object[] args)
        {
            Log.DebugFormat(message, args);
        }

        /// <summary>
        /// 打印Info信息.
        /// </summary>
        /// <param name="message">信息.</param>
        /// <param name="args">params参数.</param>
        public void InfoFormat(string message, params object[] args)
        {
            Log.InfoFormat(message, args);
        }

        /// <summary>
        /// 打印Error信息.
        /// </summary>
        /// <param name="message">信息.</param>
        /// <param name="args">params参数.</param>
        public void ErrorFormat(string message, params object[] args)
        {
            Log.ErrorFormat(message, args);
        }

        /// <summary>
        /// 打印Fatal信息.
        /// </summary>
        /// <param name="message">信息.</param>
        /// <param name="args">params参数.</param>
        public void FatalFormat(string message, params object[] args)
        {
            Log.FatalFormat(message, args);
        }
    }
}