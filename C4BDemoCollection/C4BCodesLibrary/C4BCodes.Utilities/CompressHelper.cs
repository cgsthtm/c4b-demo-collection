﻿// <copyright file="CompressHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System.IO;
    using System.IO.Compression;

    /// <summary>
    /// 压缩帮助类.
    /// </summary>
    public class CompressHelper
    {
        /// <summary>
        /// GZip压缩.
        /// </summary>
        /// <param name="rawData">原始字节数组数据.</param>
        /// <returns>压缩后的字节数组.</returns>
        public static byte[] Compress(byte[] rawData)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream compressedzipStream = new GZipStream(ms, CompressionMode.Compress, true);
            compressedzipStream.Write(rawData, 0, rawData.Length);
            compressedzipStream.Close();
            return ms.ToArray();
        }

        /// <summary>
        /// GZip解压.
        /// </summary>
        /// <param name="zippedData">经过压缩后的字节数组数据.</param>
        /// <returns>解压后的字节数组.</returns>
        public static byte[] Decompress(byte[] zippedData)
        {
            MemoryStream ms = new MemoryStream(zippedData);
            GZipStream compressedzipStream = new GZipStream(ms, CompressionMode.Decompress);
            MemoryStream outBuffer = new MemoryStream();
            byte[] block = new byte[1024];
            while (true)
            {
                int bytesRead = compressedzipStream.Read(block, 0, block.Length);
                if (bytesRead <= 0)
                {
                    break;
                }
                else
                {
                    outBuffer.Write(block, 0, bytesRead);
                }
            }

            compressedzipStream.Close();
            return outBuffer.ToArray();
        }
    }
}