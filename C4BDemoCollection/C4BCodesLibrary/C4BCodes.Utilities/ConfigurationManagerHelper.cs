﻿// <copyright file="ConfigurationManagerHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System;
    using System.Configuration;

    /// <summary>
    /// ConfigurationManager帮助类.
    /// </summary>
    public static class ConfigurationManagerHelper
    {
        /// <summary>
        /// 读取配置.
        /// </summary>
        /// <param name="key">节点key.</param>
        /// <returns>节点value.</returns>
        public static string ReadSetting(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string result = appSettings[key] ?? "Not Found";
                Console.WriteLine(result);
                return result;
            }
            catch (Exception)
            {
                Console.WriteLine("Error reading app settings");
                return string.Empty;
            }
        }

        /// <summary>
        /// 修改配置.
        /// </summary>
        /// <param name="key">节点key.</param>
        /// <param name="value">节点value.</param>
        public static void AddUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }

                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (Exception)
            {
                Console.WriteLine("Error writing app settings");
            }
        }
    }
}