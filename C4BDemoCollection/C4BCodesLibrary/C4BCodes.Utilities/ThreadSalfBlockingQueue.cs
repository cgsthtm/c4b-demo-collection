﻿// <copyright file="ThreadSalfBlockingQueue.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System.Collections.Generic;
    using System.Threading;

    /// <summary>
    /// 线程安全队列.
    /// </summary>
    /// <typeparam name="T">泛型T.</typeparam>
    public class ThreadSalfBlockingQueue<T>
    {
        private readonly Queue<T> queue;
        private readonly AutoResetEvent readLock = new AutoResetEvent(true);
        private readonly object thisObject = new object();
        private readonly AutoResetEvent writeLock = new AutoResetEvent(true);
        private bool readWaitAborted;

        /// <summary>
        /// Initializes a new instance of the <see cref="ThreadSalfBlockingQueue{T}"/> class.
        /// </summary>
        public ThreadSalfBlockingQueue()
            : this(1000)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ThreadSalfBlockingQueue{T}"/> class.
        /// </summary>
        /// <param name="capacity">容量.</param>
        protected ThreadSalfBlockingQueue(int capacity)
        {
            this.queue = capacity == 0 ? new Queue<T>() : new Queue<T>(capacity);
        }

        /// <summary>
        /// Gets 队列中元素个数.
        /// </summary>
        public int Count
        {
            get
            {
                lock (this.queue)
                {
                    return this.queue.Count;
                }
            }
        }

        /// <summary>
        /// Gets 队列中最多元素个数.
        /// </summary>
        public int MaxCount { get; private set; } = int.MaxValue - 1;

        /// <summary>
        /// 入队.
        /// </summary>
        /// <param name="item">泛型item.</param>
        public void Enqueue(T item)
        {
            lock (this.writeLock)
            {
                while (this.queue.Count >= this.MaxCount)
                {
                    this.writeLock.WaitOne(Timeout.Infinite, false);
                }

                lock (this.queue)
                {
                    this.queue.Enqueue(item);
                }

                this.readWaitAborted = false;
                this.readLock.Set();
            }
        }

        /// <summary>
        /// 出队.
        /// Dequeue 方法会阻塞线程，如果要终止阻塞，则可以调用 AbortReadWait 方法。
        /// 当 AbortReadWait 方法调用后，WaitAbortException 异常将被抛出.
        /// </summary>
        /// <returns>泛型T.</returns>
        public T Dequeue()
        {
            lock (this.readLock)
            {
                while (this.queue.Count <= 0)
                {
                    this.readLock.WaitOne(Timeout.Infinite, false);
                    if (this.readWaitAborted)
                    {
                        this.readWaitAborted = false;
                        throw new System.Exception();
                    }
                }

                lock (this.queue)
                {
                    var item = this.queue.Dequeue();
                    this.writeLock.Set();
                    return item;
                }
            }
        }

        /// <summary>
        /// 重新设置队列的最大容量.
        /// </summary>
        /// <param name="maxCount">大于1的整数.</param>
        public void ResetMaxCount(int maxCount)
        {
            lock (this.thisObject)
            {
                if (maxCount > 1 || maxCount < int.MaxValue)
                {
                    this.MaxCount = maxCount;
                }
            }
        }

        /// <summary>
        /// Resets the read wait.
        /// 调用此方法会导致 Pop 方法引发 WaitAbortException 异常.
        /// </summary>
        public void AbortReadWait()
        {
            this.readWaitAborted = true;
            this.readLock.Set();
        }
    }
}
