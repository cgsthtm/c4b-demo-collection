﻿// <copyright file="DynamicXmlHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System.Dynamic;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// 动态解析XML.
    /// </summary>
    public class DynamicXmlHelper : DynamicObject
    {
        private XElement element;

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicXmlHelper"/> class.
        /// </summary>
        /// <param name="element">XElement.</param>
        public DynamicXmlHelper(XElement element)
        {
            this.element = element;
        }

        /// <summary>
        /// 语法分析.
        /// </summary>
        /// <param name="text">待分析字符串.</param>
        /// <returns>动态解析类.</returns>
        public static DynamicXmlHelper Parse(string text)
        {
            return new DynamicXmlHelper(XElement.Parse(text));
        }

        /// <summary>
        /// 尝试获取成员.
        /// </summary>
        /// <param name="binder">GetMemberBinder.</param>
        /// <param name="result">结果.</param>
        /// <returns>成功或者失败.</returns>
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            bool success = false;
            result = null;
            XElement firstDescendant = this.element.Descendants(binder.Name).FirstOrDefault();
            if (firstDescendant != null)
            {
                if (firstDescendant.Descendants().Any())
                {
                    result = new DynamicXmlHelper(firstDescendant);
                }
                else
                {
                    result = firstDescendant.Value;
                }

                success = true;
            }

            return success;
        }

        /// <summary>
        /// 尝试设置成员.
        /// </summary>
        /// <param name="binder">SetMemberBinder.</param>
        /// <param name="value">设置的值.</param>
        /// <returns>成功或者失败.</returns>
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            bool success = false;
            XElement firstDescendant = this.element.Descendants(binder.Name).FirstOrDefault();
            if (firstDescendant != null)
            {
                if (value.GetType() == typeof(XElement))
                {
                    firstDescendant.ReplaceWith(value);
                }
                else
                {
                    firstDescendant.Value = value.ToString();
                }

                success = true;
            }

            return success;
        }
    }
}