﻿// <copyright file="ByteHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System;
    using System.Linq;
    using System.Text;
    using C4BCodesLibrary.C4BCodes.Enum;

    /// <summary>
    /// 字节操作帮助类.
    /// </summary>
    public static class ByteHelper
    {
        /// <summary>
        /// 字节数组转十六进制字符串，如：[0xae,0x00,0xcf] => "AE00CF".
        /// </summary>
        /// <param name="bytes">源字节数组.</param>
        /// <param name="withSpace">字节之间是否带空格.</param>
        /// <param name="reverse">是否反转字节数组.</param>
        /// <returns>十六进制字符串.</returns>
        public static string ToHexString(this byte[] bytes, bool withSpace = true, bool reverse = false)
        {
            string hexString = string.Empty;
            if (bytes != null)
            {
                StringBuilder strB = new StringBuilder();
                bytes = reverse ? bytes.Reverse().ToArray() : bytes;
                for (int i = 0; i < bytes.Length; i++)
                {
                    if (withSpace)
                    {
                        strB.Append(bytes[i].ToString("X2") + " ");
                    }
                    else
                    {
                        strB.Append(bytes[i].ToString("X2"));
                    }
                }

                hexString = strB.ToString().TrimEnd();
            }

            return hexString;
        }

        /// <summary>
        /// 十六进制字符串转字节数组，如："AE 00 CF" => [0xae,0x00,0xcf].
        /// </summary>
        /// <param name="hexString">源十六进制字符串.</param>
        /// <returns>字节数组.</returns>
        public static byte[] ToByteArray(this string hexString)
        {
            hexString = hexString.Replace(" ", string.Empty).Replace("0x", string.Empty);
            if ((hexString.Length % 2) != 0)
            {
                hexString += " ";
            }

            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
            {
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2).Trim(), 16);
            }

            return returnBytes;
        }

        /// <summary>
        /// 十六进制字符串转字节数组，如："AE 00 CF" => [0xae,0x00,0xcf].
        /// </summary>
        /// <param name="hexString">源十六进制字符串.</param>
        /// <param name="bytes">目标字节数组.</param>
        /// <param name="maxLen">最大长度.</param>
        /// <returns>字节数组长度.</returns>
        public static int ToByteArray(this string hexString, ref byte[] bytes, int maxLen)
        {
            string[] dataArray = hexString.Split(' ');
            for (int i = 0; (i < maxLen) && (i < dataArray.Length); i++)
            {
                bytes[i] = Convert.ToByte(dataArray[i].Substring(0, 2), 16);
            }

            return dataArray.Length;
        }

        /// <summary>
        /// 十六进制字符串转ushort，如："41 FF" => [0x41,0xFF] => 0x41FF(大端序)或0xFF4(小端序).
        /// </summary>
        /// <param name="hexString">十六进制字符串,如"41 FF".</param>
        /// <param name="byteOrdering">字节序.</param>
        /// <returns>ushort.</returns>
        public static ushort ToUShort(this string hexString, ByteOrdering byteOrdering)
        {
            byte[] bytes = hexString.ToByteArray();
            ushort result = 0;
            switch (byteOrdering)
            {
                case ByteOrdering.BigEndian:
                    result = BitConverter.ToUInt16(bytes, 0);
                    break;
                case ByteOrdering.LittleEndian:
                    Array.Reverse(bytes);
                    result = BitConverter.ToUInt16(bytes, 0);
                    break;
                default:
                    break;
            }

            return result;
        }

        /// <summary>
        /// 十六进制字符串转字节数组，如："0x725" => [0x07,0x25].
        /// </summary>
        /// <param name="data">源字符串，如：0x725.</param>
        /// <returns>字节数组.</returns>
        public static byte[] ToCanIdByteArray(this string data)
        {
            byte[] bytes = new byte[2];
            short i16 = Convert.ToInt16(data, 16);
            bytes = BitConverter.GetBytes(i16).Reverse().ToArray();
            return bytes;
        }

        /// <summary>
        /// 获取该字节的bit位是0还是1.
        /// <para>index：bit位，从低到高1-8.</para>
        /// </summary>
        /// <param name="input">该字节.</param>
        /// <param name="index">bit位，从低到高1-8.</param>
        /// <returns>位值.</returns>
        public static int GetBitValue(byte input, int index)
        {
            if (index > 8 || index < 1)
            {
                index = 0;
            }
            else
            {
                index = index - 1;
            }

            return ((input & (1 << index)) > 0) ? 1 : 0;
        }

        /// <summary>
        /// 设置该字节的某一位的值(将该位设置成0或1).
        /// <para>flag：要设置的值 true(1) / false(0).</para>
        /// </summary>
        /// <param name="data">要设置的字节byte.</param>
        /// <param name="index">要设置的位， 值从低到高为 1-8.</param>
        /// <param name="flag">要设置的值 true(1) / false(0).</param>
        /// <returns>byte.</returns>
        public static byte SetBitValue(byte data, int index, bool flag)
        {
            if (index > 8 || index < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            int v = index < 2 ? index : (2 << (index - 2));
            return flag ? (byte)(data | v) : (byte)(data & ~v);
        }

        /// <summary>
        /// 比较源字节数组baseBytes是否包含目标字节数组subbytes，连续比较.
        /// </summary>
        /// <param name="basebytes">源字节数组.</param>
        /// <param name="subbytes">目标字节数组.</param>
        /// <returns>是否包含.</returns>
        public static bool Contain(byte[] basebytes, byte[] subbytes)
        {
            if (basebytes == null)
            {
                return false;
            }

            int baseNums = basebytes.Length;
            int subNums = subbytes.Length;
            bool ret = false;
            int lxsamenums = 0;
            for (int i = 0; i < baseNums; i++)
            {
                if (baseNums - i >= subNums)
                {
                    for (int j = 0; j < subNums; j++)
                    {
                        if (basebytes[i + j] == subbytes[j])
                        {
                            lxsamenums++;
                        }
                        else
                        {
                            lxsamenums = 0;
                        }
                    }

                    if (lxsamenums == subNums)
                    {
                        ret = true;
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            return ret;
        }

        /// <summary>
        /// 将字节数组转换成等价的十进制数.
        /// </summary>
        /// <typeparam name="T">short|int|long.</typeparam>
        /// <param name="data">字节数组.</param>
        /// <param name="endian">字节顺序.</param>
        /// <returns>.</returns>
        public static T ConvertToDecimalistValue<T>(byte[] data, ByteOrdering endian = ByteOrdering.BigEndian)
            where T : struct,
            System.IConvertible,
            System.IComparable<T>,
            System.IEquatable<T>
        {
            if (typeof(T) == null)
            {
                throw new ArgumentNullException("泛型参数不允许为null");
            }

            if (!new Type[] { typeof(short), typeof(int), typeof(long) }.Contains(typeof(T)))
            {
                throw new ArgumentNullException("泛型参数必须是 short|int|long");
            }

            if (data == null)
            {
                throw new ArgumentNullException("参数不允许为null");
            }

            if (!new int[] { 2, 4, 8 }.Contains(data.Length))
            {
                throw new ArgumentException("参数长度必须是 2|4|8");
            }

            if (endian == ByteOrdering.BigEndian)
            {
                data = data.Reverse().ToArray();
            }

            if (typeof(T) == typeof(short))
            {
                return (T)(object)BitConverter.ToInt16(data, 0);
            }

            if (typeof(T) == typeof(int))
            {
                return (T)(object)BitConverter.ToInt32(data, 0);
            }

            if (typeof(T) == typeof(long))
            {
                return (T)(object)BitConverter.ToInt64(data, 0);
            }
            else
            {
                throw new ArgumentException("未知错误");
            }
        }

        /// <summary>
        /// byte数组取short数值，本方法适用于(高位在前，低位在后)的顺序.
        /// </summary>
        /// <param name="src">字节数组.</param>
        /// <param name="offset">位置.</param>
        /// <returns>short.</returns>
        public static short BytesToShort(this byte[] src, int offset)
        {
            var value = (short)(((src[offset] & 0xFF) << 8)
                        | (src[offset + 1] & 0xFF));
            return value;
        }

        /// <summary>
        /// 将数值转换成ushort数组.
        /// </summary>
        /// <param name="value">待转换值.</param>
        /// <returns>ushort.</returns>
        public static ushort[] ConvertBytesToUShorts(double value)
        {
            var bytes = BitConverter.GetBytes((int)value);
            Array.Reverse(bytes);
            return new[] { (ushort)bytes.BytesToShort(0), (ushort)bytes.BytesToShort(2) };
        }
    }
}