﻿// <copyright file="ListHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// List帮助类.
    /// </summary>
    public class ListHelper
    {
        /// <summary>
        /// 将成员为double的List转换成字节数组.
        /// </summary>
        /// <param name="matrix">List.</param>
        /// <returns>字节数组.</returns>
        public static byte[] ConvertDoubleListToBytes(List<double> matrix)
        {
            if (matrix == null)
            {
                return new byte[0];
            }

            using (MemoryStream stream = new MemoryStream())
            {
                BinaryWriter bw = new BinaryWriter(stream);
                foreach (var item in matrix)
                {
                    bw.Write(item);
                }

                return stream.ToArray();
            }
        }

        /// <summary>
        /// 将字节数组转换成成员为double的List.
        /// </summary>
        /// <param name="matrix">字节数组.</param>
        /// <returns>成员为double的List.</returns>
        public static List<double> ConvertBytesToDoubleList(byte[] matrix)
        {
            if (matrix == null)
            {
                return null;
            }

            List<double> result = new List<double>();
            using (var br = new BinaryReader(new MemoryStream(matrix)))
            {
                var ptCount = matrix.Length / 8;
                for (int i = 0; i < ptCount; i++)
                {
                    result.Add(br.ReadDouble());
                }

                return result;
            }
        }
    }
}
