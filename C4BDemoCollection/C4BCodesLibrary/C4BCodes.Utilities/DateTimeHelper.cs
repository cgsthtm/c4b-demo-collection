﻿// <copyright file="DateTimeHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System;

    /// <summary>
    /// 日期帮助类.
    /// </summary>
    public class DateTimeHelper
    {
        /// <summary>
        /// 获取开始日期和结束日期之间的天数.
        /// </summary>
        /// <param name="dateStart">开始日期.</param>
        /// <param name="dateEnd">结束日期.</param>
        /// <returns>相差天数.</returns>
        public static int DateDiff(DateTime dateStart, DateTime dateEnd)
        {
            DateTime start = Convert.ToDateTime(dateStart.ToShortDateString());
            DateTime end = Convert.ToDateTime(dateEnd.ToShortDateString());
            TimeSpan sp = end.Subtract(start);
            return sp.Days;
        }
    }
}