﻿// <copyright file="ZlgCanHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities.ZLGCAN
{
    using System;
    using System.Runtime.InteropServices;
    using C4BCodesLibrary.C4BCodes.ZLGCAN;

    /// <summary>
    /// SetValueFunc.
    /// </summary>
    /// <param name="path">path.</param>
    /// <param name="value">value.</param>
    /// <returns>res.</returns>
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int SetValueFunc(string path, byte[] value);

    /// <summary>
    /// GetValueFunc.
    /// </summary>
    /// <param name="path">path.</param>
    /// <returns>res.</returns>
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetValueFunc(string path);

    /// <summary>
    /// GetPropertysFunc.
    /// </summary>
    /// <param name="path">path.</param>
    /// <param name="value">value.</param>
    /// <returns>res.</returns>
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetPropertysFunc(string path, string value);

    /// <summary>
    /// 周立功CAN ZLG致远电子 USBCANFD-800U 帮助类.
    /// </summary>
    public class ZlgCanHelper
    {
        /// <summary>
        /// CAN最大数据长度.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        private const int CAN_MAX_DLEN = 8;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 获取指定通道的CAN数据.
        /// </summary>
        /// <param name="channel_handle_">通道句柄.</param>
        /// <param name="can_data">接收数据数组.</param>
        /// <param name="max_len">每次最多接收数量.</param>
        /// <returns>接收到的数量.</returns>
        public static uint GetChannelReceiveData(IntPtr channel_handle_, ref ZCAN_Receive_Data[] can_data, uint max_len = 1000)
        {
            uint len = Method.ZCAN_GetReceiveNum(channel_handle_, 0);
            if (len > 0)
            {
                int size = Marshal.SizeOf(typeof(ZCAN_Receive_Data));
                len = len > max_len ? max_len : len; // 每次从缓冲区接收最多1000个字节
                IntPtr ptr = Marshal.AllocHGlobal((int)len * size);
                len = Method.ZCAN_Receive(channel_handle_, ptr, len, 50);
                for (int i = 0; i < len; ++i)
                {
                    can_data[i] = (ZCAN_Receive_Data)Marshal.PtrToStructure(
                        (IntPtr)((long)ptr + (i * size)), typeof(ZCAN_Receive_Data));
                }

                Marshal.FreeHGlobal(ptr);
            }

            return len;
        }

        /// <summary>
        /// 获取指定通道的CAN数据1.
        /// </summary>
        /// <param name="channel_handle_">通道句柄.</param>
        /// <param name="can_data">接收数据数组.</param>
        /// <returns>接收到的数量.</returns>
        public static uint GetChannelReceiveData1(IntPtr channel_handle_, ref ZCAN_Receive_Data1[] can_data)
        {
            uint len = Method.ZCAN_GetReceiveNum(channel_handle_, 0);
            if (len > 0)
            {
                int size = Marshal.SizeOf(typeof(ZCAN_Receive_Data));
                ////len = len > 1000 ? 1000 : len; // 每次从缓冲区接收最多1000个字节
                IntPtr ptr = Marshal.AllocHGlobal((int)len * size);
                len = Method.ZCAN_Receive(channel_handle_, ptr, len, 50);
                for (int i = 0; i < len; ++i)
                {
                    can_data[i] = (ZCAN_Receive_Data1)Marshal.PtrToStructure(
                        (IntPtr)((long)ptr + (i * size)), typeof(ZCAN_Receive_Data1));
                    byte[] idBytes = can_data[i].frame.can_id.ToString("X4").ToCanIdByteArray();
                    Array.Copy(idBytes, 0, can_data[i].frame.data1, 0, 2);
                    Array.Copy(can_data[i].frame.data, 0, can_data[i].frame.data1, 2, 8);
                }

                Marshal.FreeHGlobal(ptr);
            }

            return len;
        }

        /// <summary>
        /// 给指定通道发送CAN数据帧.
        /// </summary>
        /// <param name="channel_handle_">通道句柄.</param>
        /// <param name="hexIDStr">帧ID，如：0725.</param>
        /// <param name="hexDataStr">发送的数据，用空格隔开，如：02 10 03 00 00 00 00 00.</param>
        /// <returns>res.</returns>
        public static bool SendCANData(IntPtr channel_handle_, string hexIDStr = "0725", string hexDataStr = "02 10 03 00 00 00 00 00")
        {
            uint id = (uint)Convert.ToInt32(hexIDStr, 16);
            string data = hexDataStr;
            int frame_type_index = 0; // 帧类型 0-标准帧 1-扩展帧
            ////int protocol_index = 0;   // 协议 0-CAN 1-CANFD
            int send_type_index = 0;  // 发送方式 0-正常发送 1-单次发送 2-自发自收 3-单次自发自收
            ////int canfd_exp_index = 0;  // CANFD加速 0-否 1-是
            uint result; // 发送的帧数

            // CAN协议
            ZCAN_Transmit_Data can_data = default(ZCAN_Transmit_Data);
            can_data.frame.can_id = MakeCanId(id, frame_type_index, 0, 0);
            can_data.frame.data = new byte[8];
            can_data.frame.can_dlc = (byte)data.ToByteArray(ref can_data.frame.data, CAN_MAX_DLEN);
            can_data.transmit_type = (uint)send_type_index;
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(can_data));
            Marshal.StructureToPtr(can_data, ptr, true);
            result = Method.ZCAN_Transmit(channel_handle_, ptr, 1);
            Marshal.FreeHGlobal(ptr);
            return result == 1;
        }

        /// <summary>
        /// 给指定通道发送CAN数据帧.
        /// </summary>
        /// <param name="channel_handle_">通道句柄.</param>
        /// <param name="hexIDStr">帧ID，如：0725.</param>
        /// <param name="data">发送的数据 字节数组.</param>
        /// <returns>res.</returns>
        public static bool SendCANData(IntPtr channel_handle_, string hexIDStr, byte[] data)
        {
            uint id = (uint)Convert.ToInt32(hexIDStr, 16);
            int frame_type_index = 0; // 帧类型 0-标准帧 1-扩展帧
            ////int protocol_index = 0;   // 协议 0-CAN 1-CANFD
            int send_type_index = 0;  // 发送方式 0-正常发送 1-单次发送 2-自发自收 3-单次自发自收
            ////int canfd_exp_index = 0;  // CANFD加速 0-否 1-是
            uint result; // 发送的帧数

            // CAN协议
            ZCAN_Transmit_Data can_data = default(ZCAN_Transmit_Data);
            can_data.frame.can_id = MakeCanId(id, frame_type_index, 0, 0);
            can_data.frame.data = data;
            can_data.frame.can_dlc = 8;
            can_data.transmit_type = (uint)send_type_index;
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(can_data));
            Marshal.StructureToPtr(can_data, ptr, true);
            result = Method.ZCAN_Transmit(channel_handle_, ptr, 1);
            Marshal.FreeHGlobal(ptr);
            return result == 1;
        }

        /// <summary>
        /// 给指定通道发送CAN数据帧.
        /// </summary>
        /// <param name="channel_handle_">通道句柄.</param>
        /// <param name="data">经过组装后的10个字节的数组.</param>
        /// <returns>res.</returns>
        public static bool SendCANData(IntPtr channel_handle_, byte[] data)
        {
            var hexIDStr = (new byte[] { data[0], data[1] }).ToHexString(withSpace: false, reverse: false);
            uint id = (uint)Convert.ToInt32(hexIDStr, 16);
            int frame_type_index = 0; // 帧类型 0-标准帧 1-扩展帧
            ////int protocol_index = 0;   // 协议 0-CAN 1-CANFD
            int send_type_index = 0;  // 发送方式 0-正常发送 1-单次发送 2-自发自收 3-单次自发自收
            ////int canfd_exp_index = 0;  // CANFD加速 0-否 1-是
            uint result; // 发送的帧数

            // CAN协议
            ZCAN_Transmit_Data can_data = default(ZCAN_Transmit_Data);
            can_data.frame.can_id = MakeCanId(id, frame_type_index, 0, 0);
            can_data.frame.data = new byte[8];
            Array.Copy(data, 2, can_data.frame.data, 0, 8);
            can_data.frame.can_dlc = 8;
            can_data.transmit_type = (uint)send_type_index;
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(can_data));
            Marshal.StructureToPtr(can_data, ptr, true);
            result = Method.ZCAN_Transmit(channel_handle_, ptr, 1);
            Marshal.FreeHGlobal(ptr);
            return result == 1;
        }

        /// <summary>
        /// 生产CANID.
        /// </summary>
        /// <param name="id">canid.</param>
        /// <param name="eff">eff.</param>
        /// <param name="rtr">rtr.</param>
        /// <param name="err">err.</param>
        private static uint MakeCanId(uint id, int eff, int rtr, int err) // 1:extend frame 0:standard frame
        {
            uint ueff = (uint)(!!Convert.ToBoolean(eff) ? 1 : 0);
            uint urtr = (uint)(!!Convert.ToBoolean(rtr) ? 1 : 0);
            uint uerr = (uint)(!!Convert.ToBoolean(err) ? 1 : 0);
            return id | ueff << 31 | urtr << 30 | uerr << 29;
        }
    }
}