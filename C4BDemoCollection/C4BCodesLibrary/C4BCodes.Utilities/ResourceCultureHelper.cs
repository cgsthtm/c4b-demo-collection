﻿// <copyright file="ResourceCultureHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System.Globalization;
    using System.Reflection;
    using System.Resources;
    using System.Runtime.CompilerServices;
    using System.Threading;

    /// <summary>
    /// 国际化资源类.
    /// </summary>
    public class ResourceCultureHelper
    {
        /// <summary>
        /// Gets or sets the root name of the resource files that the ResourceManager searches for resources.
        /// <para>e.g. "ElectricitySafelyTest.Strings.MyResources".ElectricitySafelyTest is your assembly.
        /// MyResources.resx exists in Strings folder,and Strings folder have other .resx files, i.e. MyResources.en-US.resx, MyResource.zh-CN.resx...
        /// </para>
        /// </summary>
        public static string BaseName { get; set; } = "ElectricitySafelyTest.Strings.MyResources";

        /// <summary>
        /// Gets or sets Assembly.GetExecutingAssembly().
        /// </summary>
        public static Assembly ExecutingAssembly { get; set; } = Assembly.GetExecutingAssembly();

        /// <summary>
        /// Set current culture by name.
        /// </summary>
        /// <param name="name">name. such as 'en-US', 'zh-CN'...</param>
        public static void SetCurrentCulture(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = "en-US";
            }

            Thread.CurrentThread.CurrentCulture = new CultureInfo(name);
        }

        /// <summary>
        /// Get string by id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>current language .</returns>
        public static string GetString(string id)
        {
            string strCurLanguage = string.Empty;
            try
            {
                ////ResourceManager rm = ElectricitySafelyTest.Strings.MyResources.ResourceManager;
                ResourceManager rm = new ResourceManager(BaseName, ExecutingAssembly);
                CultureInfo ci = Thread.CurrentThread.CurrentCulture;
                strCurLanguage = rm.GetString(id, ci);
            }
            catch
            {
                strCurLanguage = "No id:" + id + ", please add.";
            }

            return strCurLanguage;
        }
    }
}
