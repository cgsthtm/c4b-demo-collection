﻿// <copyright file="Crc16Ccitt.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    /// <summary>
    /// 标准的CRC16-CCITT算法.它通常用于通信协议中，比如Modbus等.
    /// </summary>
    public class Crc16Ccitt
    {
        private static ushort[] crcTable;

        static Crc16Ccitt()
        {
            // 预先计算好的CRC16-CCITT查找表
            if (crcTable == null)
            {
                crcTable = new ushort[256];
                for (int i = 0; i < 256; i++)
                {
                    ushort crc = (ushort)i;
                    for (int j = 0; j < 8; j++)
                    {
                        if ((crc & 0x0001) != 0)
                        {
                            crc = (ushort)((crc >> 1) ^ 0xA001);
                        }
                        else
                        {
                            crc >>= 1;
                        }
                    }

                    crcTable[i] = crc;
                }
            }
        }

        /// <summary>
        /// 计算校验和.
        /// </summary>
        /// <param name="data">待校验的数据.</param>
        /// <returns>检验和.</returns>
        public static ushort ComputeChecksum(byte[] data)
        {
            ushort crc = 0xFFFF; // 初始值
            foreach (byte b in data)
            {
                crc = (ushort)((crc >> 8) ^ crcTable[(crc & 0xFF) ^ b]);
            }

            return crc;
        }
    }
}
