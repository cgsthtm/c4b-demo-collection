﻿// <copyright file="CommandQueue.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System.Collections.Concurrent;

    /// <summary>
    /// 命令队列.
    /// </summary>
    /// <typeparam name="T">泛型T.</typeparam>
    public class CommandQueue<T>
    {
        /// <summary>
        /// Gets or sets 命令队列.
        /// </summary>
        public static ThreadSalfBlockingQueue<T> AllCommandQueue { get; set; } = new ThreadSalfBlockingQueue<T>();

        /// <summary>
        /// Gets or sets ConcurrentQueue.
        /// </summary>
        public static ConcurrentQueue<T> AllCommandConcurrentQueue { get; set; } = new ConcurrentQueue<T>();
    }
}
