﻿// <copyright file="CheckCodeHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System;
    using System.Linq;
    using C4BCodesLibrary.C4BCodes.Enum;

    /// <summary>
    /// 校验码帮助类.
    /// </summary>
    public class CheckCodeHelper
    {
        /// <summary>
        /// 根据XModem标准计算CRC16.
        /// </summary>
        /// <param name="buffer">待计算的字节数组.</param>
        /// <returns>CRC16.</returns>
        public static ushort Calculate_CRC16_XModem(byte[] buffer)
        {
            const ushort POLYNOMIAL = 0x1021;
            ushort crc = 0x0000;

            foreach (byte b in buffer)
            {
                crc ^= (ushort)(b << 8);
                for (int i = 0; i < 8; i++)
                {
                    if ((crc & 0x8000) != 0)
                    {
                        crc = (ushort)((crc << 1) ^ POLYNOMIAL);
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }

            return crc;
        }

        /// <summary>
        /// 根据XModem标准计算CRC16.
        /// </summary>
        /// <param name="buffer">待计算的字节数组.</param>
        /// <param name="byteOrdering">字节排序.</param>
        /// <returns>CRC16字节数组.</returns>
        public static byte[] Calculate_CRC16_XModem(byte[] buffer, ByteOrdering byteOrdering)
        {
            const ushort POLYNOMIAL = 0x1021;
            ushort crc = 0x0000;
            byte[] res = null;

            foreach (byte b in buffer)
            {
                crc ^= (ushort)(b << 8);
                for (int i = 0; i < 8; i++)
                {
                    if ((crc & 0x8000) != 0)
                    {
                        crc = (ushort)((crc << 1) ^ POLYNOMIAL);
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }

            switch (byteOrdering)
            {
                case ByteOrdering.BigEndian:
                    res = BitConverter.GetBytes(crc).Reverse().ToArray();
                    break;
                case ByteOrdering.LittleEndian:
                    res = BitConverter.GetBytes(crc);
                    break;
                default:
                    break;
            }

            return res;
        }

        /// <summary>
        /// 中航电测仪器有限公司动态数字模块（H8C）的协议帧格式.
        /// 检验字段使用CS方法对数据帧的计算得出，不包括起始、结束符以及检验字段自身.
        /// </summary>
        /// <param name="data">协议帧中除起始、结束符以及检验字段自身的部分.</param>
        /// <param name="nLength">长度?.</param>
        /// <returns>校验字节.</returns>
        public static byte Calculate_CS(byte[] data, ushort nLength)
        {
            byte uchCS = 0;
            for (int i = 0; i < nLength; i++)
            {
                uchCS += data[i];
            }

            return uchCS;
        }

        /// <summary>
        /// Calculate_CRC16_CCITT.
        /// </summary>
        /// <param name="buffer">字节数组.</param>
        /// <returns>返回校验和字节数组.</returns>
        public static byte[] Calculate_CRC16_CCITT(byte[] buffer)
        {
            ushort checksum = Crc16Ccitt.ComputeChecksum(buffer);
            return BitConverter.GetBytes(checksum);
        }
    }
}
