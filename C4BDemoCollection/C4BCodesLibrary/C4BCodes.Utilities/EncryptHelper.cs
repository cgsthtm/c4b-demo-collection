﻿// <copyright file="EncryptHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    /// <summary>
    /// EncryptHelper.
    /// </summary>
    public class EncryptHelper
    {
        /// <summary>
        ///  Modbus CRC-16多项式.
        /// </summary>
        private const ushort Polynomial = 0xA001;

        /// <summary>
        /// AES密匙.
        /// </summary>
        private static string aesKey = "6YHN7ujm1qaz6yhn1qaz6yhn";

        /// <summary>
        /// 默认密钥向量.
        /// </summary>
        private static byte[] keys = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };

        /// <summary>
        /// AES密匙-燕通.
        /// </summary>
        private static string aesKeyYt = "[45/*YUIdse..e;]";

        /// <summary>
        /// DES加密算法.
        /// </summary>
        /// <param name="text">待加密文本.</param>
        /// <param name="sKey">加密密匙.</param>
        /// <returns>加密文本.</returns>
        public static string DESEncrypt(string text, string sKey = "HYSYS.HY")
        {
            try
            {
                byte[] rgbKey = Encoding.UTF8.GetBytes(sKey.Substring(0, 8));
                byte[] rgbIV = keys;
                byte[] inputByteArray = Encoding.UTF8.GetBytes(text);
                DESCryptoServiceProvider dCSP = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, dCSP.CreateEncryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                cStream.Close();
                mStream.Close();
                return Convert.ToBase64String(mStream.ToArray());
            }
            catch
            {
                return text;
            }
        }

        /// <summary>
        /// DES解密算法.
        /// </summary>
        /// <param name="text">待解密文本.</param>
        /// <param name="sKey">解密密匙.</param>
        /// <returns>解密文本.</returns>
        public static string DESDecrypt(string text, string sKey = "HYSYS.HY")
        {
            try
            {
                byte[] rgbKey = Encoding.UTF8.GetBytes(sKey);
                byte[] rgbIV = keys;
                byte[] inputByteArray = Convert.FromBase64String(text);
                DESCryptoServiceProvider dcsp = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, dcsp.CreateDecryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                cStream.Close();
                mStream.Close();
                return Encoding.UTF8.GetString(mStream.ToArray());
            }
            catch
            {
                return text;
            }
        }

        /// <summary>
        /// AES加密.
        /// </summary>
        /// <param name="encryptString">待加密的密文.</param>
        /// <param name="encryptKey">加密密匙.</param>
        /// <returns>加密文本.</returns>
        public static string AESEncrypt(string encryptString, string encryptKey = "zhongguohangkong")
        {
            string returnValue;
            byte[] temp = Convert.FromBase64String(aesKey);
            Rijndael aesProvider = Rijndael.Create();
            try
            {
                byte[] byteEncryptString = Encoding.Default.GetBytes(encryptString);
                MemoryStream memoryStream = new MemoryStream();
                CryptoStream cryptoStream = new CryptoStream(memoryStream, aesProvider.CreateEncryptor(Encoding.Default.GetBytes(encryptKey), temp), CryptoStreamMode.Write);
                cryptoStream.Write(byteEncryptString, 0, byteEncryptString.Length);
                cryptoStream.FlushFinalBlock();
                returnValue = Convert.ToBase64String(memoryStream.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return returnValue;
        }

        /// <summary>
        /// AES解密.
        /// </summary>
        /// <param name="decryptString">待解密密文.</param>
        /// <param name="decryptKey">解密密钥.</param>
        /// <returns>解密文本.</returns>
        public static string AESDecrypt(string decryptString, string decryptKey = "zhongguohangkong")
        {
            string returnValue = string.Empty;
            byte[] temp = Convert.FromBase64String(aesKey);
            Rijndael aesProvider = Rijndael.Create();
            aesProvider.Mode = CipherMode.ECB;
            aesProvider.Padding = PaddingMode.PKCS7;
            try
            {
                byte[] byteDecryptString = System.Convert.FromBase64String(decryptString);
                MemoryStream memoryStream = new MemoryStream();
                CryptoStream cryptoStream = new CryptoStream(memoryStream, aesProvider.CreateDecryptor(Encoding.Default.GetBytes(decryptKey), temp), CryptoStreamMode.Write);
                cryptoStream.Write(byteDecryptString, 0, byteDecryptString.Length);
                cryptoStream.FlushFinalBlock();
                returnValue = Encoding.Default.GetString(memoryStream.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return returnValue;
        }

        /// <summary>
        /// AES解密1.
        /// </summary>
        /// <param name="value">value.</param>
        /// <param name="aeskey">asekey.</param>
        /// <returns>res.</returns>
        public static string AESDecrypt1(string value, string aeskey = null)
        {
            try
            {
                if (string.IsNullOrEmpty(aeskey))
                {
                    aeskey = aesKeyYt;
                }

                value = value.Trim();
                byte[] keyArray = Encoding.UTF8.GetBytes(aeskey);
                byte[] toEncryptArray = Convert.FromBase64String(value.Trim(trimChars: '\r').Trim(trimChars: '\0'));
                RijndaelManaged rDel = new RijndaelManaged();
                rDel.Key = keyArray;
                rDel.Mode = CipherMode.ECB;
                rDel.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = rDel.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                return Encoding.UTF8.GetString(resultArray);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 利用MD5对字符串进行加密.
        /// </summary>
        /// <param name="encryptString">待加密的字符串.</param>
        /// <returns>返回加密后的字符串.</returns>
        public static string MD5Encrypt(string encryptString)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] encryptedBytes = md5.ComputeHash(Encoding.ASCII.GetBytes(encryptString));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < encryptedBytes.Length; i++)
            {
                sb.AppendFormat("{0:x2}", encryptedBytes[i]);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 将字符串进行Base64加密.
        /// </summary>
        /// <param name="str">待加密文本.</param>
        /// <returns>加密文本.</returns>
        public static string Base64Encode(string str)
        {
            byte[] bytes = Encoding.Default.GetBytes(str);
            return Convert.ToBase64String(bytes);
        }

        /// <summary>
        /// 将Base64形式的字符串进行解密.
        /// </summary>
        /// <param name="str">待解密文本.</param>
        /// <returns>解密文本.</returns>
        public static string Base64Decode(string str)
        {
            string result;
            try
            {
                byte[] bytes = Convert.FromBase64String(str);
                result = Encoding.Default.GetString(bytes);
            }
            catch
            {
                result = str;
            }

            return result;
        }

        /// <summary>
        /// 计算ModbusCRC16.
        /// </summary>
        /// <param name="message">待计算的字节数组.</param>
        /// <returns>CRC校验值.</returns>
        public static ushort CalculateModbusCRC16(byte[] message)
        {
            ushort crc = 0xFFFF; // 初始化CRC寄存器为0xFFFF
            foreach (byte data in message)
            {
                crc ^= data;
                for (int bit = 0; bit < 8; ++bit)
                {
                    if ((crc & 0x0001) != 0)
                    {
                        crc = (ushort)((crc >> 1) ^ Polynomial);
                    }
                    else
                    {
                        crc >>= 1;
                    }
                }
            }

            // 根据Modbus的规定，CRC的高位在前、低位在后，这里做一下字节交换
            crc = SwapBytes(crc);
            return crc;
        }

        /// <summary>
        /// 字节交换.
        /// </summary>
        /// <param name="value">待交换值.</param>
        /// <returns>字节交换后的值.</returns>
        private static ushort SwapBytes(ushort value)
        {
            return (ushort)((value << 8) | (value >> 8));
        }
    }
}