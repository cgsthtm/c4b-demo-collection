﻿// <copyright file="HexFileHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Hex文件帮助类.
    /// </summary>
    public class HexFileHelper
    {
        // 0xE000 = 57344
        // byte[] a = new byte[] { 0, 0, 0xe0, 0 };
        // Console.WriteLine(BitConverter.ToInt32(a, 0)); // 14680064 错误
        // byte[] a = new byte[] { 0, 0xe0, 0, 0 };
        // Console.WriteLine(BitConverter.ToInt32(a, 0)); // 57344 正确

        /// <summary>
        /// Hex文件.
        /// </summary>
        public class HexFile
        {
            private byte[] startsAt;

            private byte[] endsAt;

            private byte[] data;

            /// <summary>
            /// Hex文件路径.
            /// </summary>
            private string fileName;

            /// <summary>
            /// Initializes a new instance of the <see cref="HexFile"/> class.
            /// </summary>
            /// <param name="fileName">包含文件名的绝对路径.</param>
            public HexFile(string fileName)
            {
                this.fileName = fileName;
            }

            /// <summary>
            /// Gets 起始地址.
            /// </summary>
            public byte[] StartsAt => this.startsAt;

            /// <summary>
            /// Gets 结束地址.
            /// </summary>
            public byte[] EndsAt => this.endsAt;

            /// <summary>
            /// Gets 数据.
            /// </summary>
            public byte[] Data => this.data;

            /// <summary>
            /// Gets 长度.
            /// </summary>
            public byte[] Length
            {
                get => BitConverter.GetBytes(this.data.Length);
            }

            /// <summary>
            /// Gets Hex文件路径.
            /// </summary>
            public string FileName => this.fileName;

            /// <summary>
            /// 读取Hex文件.
            /// </summary>
            /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
            public async Task ReadHexFile()
            {
                if (!string.IsNullOrEmpty(this.fileName) && this.fileName.Trim() != string.Empty)
                {
                    using (FileStream fs = new FileStream(this.fileName, FileMode.Open))
                    {
                        StreamReader sr = new StreamReader(fs);
                        string szLine = string.Empty;
                        StringBuilder szHex = new StringBuilder();
                        StringBuilder szAddress = new StringBuilder();
                        int szBaseAddress = 0;
                        StringBuilder szLength = new StringBuilder();
                        while (true)
                        {
                            szLine = await sr.ReadLineAsync();
                            if (szLine == null)
                            {
                                break; // 读取完毕，退出
                            }

                            if (szLine.Substring(0, 1) == ":")
                            {
                                if (szLine.Substring(1, 8) == "02000004") // 第一行 扩展线性地址记录
                                {
                                    byte[] a = szLine.Substring(9, 4).ToByteArray().Reverse().ToArray();
                                    szBaseAddress = BitConverter.ToUInt16(a, 0) << 16; // 基地址，例如0x00C3 << 16
                                    this.startsAt = BitConverter.GetBytes(szBaseAddress);
                                }

                                if (szLine.Substring(1, 8) == "00000001")
                                {
                                    break; // 文件结束标识
                                }

                                if ((szLine.Substring(8, 1) == "0") || (szLine.Substring(8, 1) == "1")) // 直接解析数据类型标识为 : 00 和 01 的格式
                                {
                                    szHex.Append(szLine.Substring(9, szLine.Length - 11));  // 所有数据分一组
                                    szAddress.Append(szLine.Substring(3, 4)); // 所有起始地址分一组
                                    szLength.Append(szLine.Substring(1, 2)); // 所有数据长度归一组
                                }
                            }
                        }

                        // 将数据字符转换为Hex，并保存在数组 szBin[]
                        int j = 0;
                        var szHexStr = szHex.ToString();
                        int length = szHexStr.Length; // 获取长度
                        byte[] szBin = new byte[length / 2];
                        for (int i = 0; i < length; i += 2)
                        {
                            szBin[j] = (byte)short.Parse(szHexStr.Substring(i, 2), System.Globalization.NumberStyles.HexNumber); // 两个字符合并一个Hex
                            j++;
                        }

                        this.data = szBin;
                        this.endsAt = BitConverter.GetBytes(szBaseAddress + this.data.Length - 1);

                        // 将起始地址的字符转换为Hex，并保存在数组 szAdd []
                        j = 0;
                        var szAddressStr = szAddress.ToString();
                        length = szAddressStr.Length;
                        short[] szAdd = new short[length / 4];
                        for (int i = 0; i < length; i += 4)
                        {
                            szAdd[j] = short.Parse(szAddressStr.Substring(i, 4), System.Globalization.NumberStyles.HexNumber); // 4个字符合并一个Hex
                            j++;
                        }

                        // 将长度字符转换为Hex，并保存在数组 szLen []
                        j = 0;
                        var szLengthStr = szLength.ToString();
                        length = szLengthStr.Length;
                        byte[] szLen = new byte[length / 2];
                        for (int i = 0; i < length; i += 2)
                        {
                            szLen[j] = (byte)short.Parse(szLengthStr.Substring(i, 2), System.Globalization.NumberStyles.HexNumber); // 两个字符合并一个Hex
                            j++;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// S-Record文件.
        /// </summary>
        public class SRecFile
        {
            private byte[] startsAt;
            private byte[] endsAt;
            private byte[] data;

            /// <summary>
            /// SRec文件路径.
            /// </summary>
            private string fileName;

            /// <summary>
            /// Initializes a new instance of the <see cref="SRecFile"/> class.
            /// </summary>
            /// <param name="fileName">文件路径.</param>
            public SRecFile(string fileName)
            {
                this.fileName = fileName;
            }

            /// <summary>
            /// Gets 起始地址.
            /// </summary>
            public byte[] StartsAt => this.startsAt;

            /// <summary>
            /// Gets 结束地址.
            /// </summary>
            public byte[] EndsAt => this.endsAt;

            /// <summary>
            /// Gets 数据.
            /// </summary>
            public byte[] Data => this.data;

            /// <summary>
            /// Gets 长度.
            /// </summary>
            public byte[] Length
            {
                get => BitConverter.GetBytes(this.data.Length);
            }

            /// <summary>
            /// Gets SRec文件路径.
            /// </summary>
            public string FileName => this.fileName;

            /// <summary>
            /// 读取SRec文件.
            /// </summary>
            public void ReadSRecFile()
            {
                if (!string.IsNullOrEmpty(this.fileName) && this.fileName.Trim() != string.Empty)
                {
                    using (FileStream fs = new FileStream(this.fileName, FileMode.Open))
                    {
                        StreamReader sr = new StreamReader(fs);
                        string szLine = string.Empty;
                        StringBuilder szHex = new StringBuilder();
                        StringBuilder szAddress = new StringBuilder();
                        while (true)
                        {
                            szLine = sr.ReadLine();
                            if (szLine == null)
                            {
                                break; // 读取完毕，退出
                            }

                            if (szLine.Substring(0, 2) == "S3")
                            {
                                szHex.Append(szLine.Substring(12, szLine.Length - 14));  // 所有数据分一组
                                szAddress.Append(szLine.Substring(4, 8) + ",");  // 所有地址分一组
                            }
                        }

                        // 将数据字符转换为Hex，并保存在数组 szBin[]
                        int j = 0;
                        int length = szHex.Length; // 获取长度
                        byte[] szBin = new byte[length / 2];
                        string szHexStr = szHex.ToString();
                        for (int i = 0; i < length; i += 2)
                        {
                            szBin[j] = Convert.ToByte(szHexStr.Substring(i, 2), 16); // 两个字符合并一个Hex
                            j++;
                        }

                        string[] addrStrs = szAddress.ToString().Split(',');
                        this.startsAt = addrStrs[0].ToByteArray();
                        this.data = szBin;
                        this.endsAt = addrStrs[addrStrs.Length - 2].ToByteArray();
                    }
                }
            }
        }
    }
}