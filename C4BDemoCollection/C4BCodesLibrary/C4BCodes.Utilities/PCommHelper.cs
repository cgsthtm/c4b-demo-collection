﻿// <copyright file="PCommHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Utilities.PComm
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// PComm帮助类.
    /// </summary>
    public static class PCommHelper
    {
        /// <summary>
        /// 波特率B50.
        /// </summary>
        public const int B50 = 0x0;

        /// <summary>
        /// 波特率B75.
        /// </summary>
        public const int B75 = 0x1;

        /// <summary>
        /// 波特率B110.
        /// </summary>
        public const int B110 = 0x2;

        /// <summary>
        /// 波特率B134.
        /// </summary>
        public const int B134 = 0x3;

        /// <summary>
        /// 波特率B150.
        /// </summary>
        public const int B150 = 0x4;

        /// <summary>
        /// 波特率B300.
        /// </summary>
        public const int B300 = 0x5;

        /// <summary>
        /// 波特率B600.
        /// </summary>
        public const int B600 = 0x6;

        /// <summary>
        /// 波特率B1200.
        /// </summary>
        public const int B1200 = 0x7;

        /// <summary>
        /// 波特率B1800.
        /// </summary>
        public const int B1800 = 0x8;

        /// <summary>
        /// 波特率B2400.
        /// </summary>
        public const int B2400 = 0x9;

        /// <summary>
        /// 波特率B4800.
        /// </summary>
        public const int B4800 = 0xA;

        /// <summary>
        /// 波特率B7200.
        /// </summary>
        public const int B7200 = 0xB;

        /// <summary>
        /// 波特率B9600.
        /// </summary>
        public const int B9600 = 0xC;

        /// <summary>
        /// 波特率B19200.
        /// </summary>
        public const int B19200 = 0xD;

        /// <summary>
        /// 波特率B38200.
        /// </summary>
        public const int B38400 = 0xE;

        /// <summary>
        /// 波特率B57600.
        /// </summary>
        public const int B57600 = 0xF;

        /// <summary>
        /// 波特率B115200.
        /// </summary>
        public const int B115200 = 0x10;

        /// <summary>
        /// 波特率B230400.
        /// </summary>
        public const int B230400 = 0x11;

        /// <summary>
        /// 波特率B460800.
        /// </summary>
        public const int B460800 = 0x12;

        /// <summary>
        /// 波特率B921600.
        /// </summary>
        public const int B921600 = 0x13;

        /// <summary>
        /// 5位数据位.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int BIT_5 = 0x0;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 6位数据位.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int BIT_6 = 0x1;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 7位数据位.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int BIT_7 = 0x2;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 8位数据位.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int BIT_8 = 0x3;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 1位停止位.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int STOP_1 = 0x0;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 2位停止位.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int STOP_2 = 0x4;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 校验位EVEN.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int P_EVEN = 0x18;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 校验位ODD.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int P_ODD = 0x8;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 校验位SPC.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int P_SPC = 0x38;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 校验位MRK.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int P_MRK = 0x28;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 无校验位.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int P_NONE = 0x0;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 结果OK返回值.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int SIO_OK = 0;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// no such port or port not opened.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int SIO_BADPORT = -1;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// can't control the board.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int SIO_OUTCONTROL = -2;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// no data to read or no buffer to write.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int SIO_NODATA = -4;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// no such port or port has be opened.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int SIO_OPENFAIL = -5;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// RTS can't set because H/W flowctrl.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int SIO_RTS_BY_HW = -6;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// bad parameter.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int SIO_BADPARM = -7;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// call win32 function fail, please call.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int SIO_WIN32FAIL = -8;
#pragma warning restore SA1310 // Field names should not contain underscore

        /// <summary>
        /// 打开串口.
        /// </summary>
        /// <param name="port">串口号.</param>
        /// <returns>结果.</returns>
        [DllImport("PComm.dll", EntryPoint = "sio_open")]
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public static extern int sio_open(int port);
#pragma warning restore SA1300 // Element should begin with upper-case letter

        /// <summary>
        /// 配置串口通讯参数，例如波特率、校验位、数据位和停止位.
        /// <para>例如：ret = sio_ioctl ( 2, B38400, P_NONE | BIT_8 | STOP_1 );.</para>
        /// </summary>
        /// <param name="port">串口号.</param>
        /// <param name="baud">波特率.</param>
        /// <param name="mode">数据位|停止位|校验位.</param>
        /// <returns>结果.</returns>
        [DllImport("PComm.dll", EntryPoint = "sio_ioctl")]
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public static extern int sio_ioctl(int port, int baud, int mode);
#pragma warning restore SA1300 // Element should begin with upper-case letter

        /// <summary>
        /// 获取输入缓冲区中积累的数据长度.
        /// </summary>
        /// <param name="port">串口号.</param>
        /// <returns>结果.</returns>
        [DllImport("PComm.dll", EntryPoint = "sio_iqueue")]
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public static extern int sio_iqueue(int port);
#pragma warning restore SA1300 // Element should begin with upper-case letter

        /// <summary>
        /// 设置DTR状态.
        /// </summary>
        /// <param name="port">串口号.</param>
        /// <param name="mode">DTR设置：0-DTR off；1-DTR on.</param>
        /// <returns>结果.</returns>
        [DllImport("PComm.dll", EntryPoint = "sio_DTR")]
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public static extern int sio_DTR(int port, int mode);
#pragma warning restore SA1300 // Element should begin with upper-case letter

        /// <summary>
        /// 设置RTS状态.
        /// </summary>
        /// <param name="port">串口号.</param>
        /// <param name="mode">RTS设置：0-RTS off；1-RTS on.</param>
        /// <returns>结果.</returns>
        [DllImport("PComm.dll", EntryPoint = "sio_RTS")]
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public static extern int sio_RTS(int port, int mode);
#pragma warning restore SA1300 // Element should begin with upper-case letter

        /// <summary>
        /// 刷新驱动程序上的输入/输出缓冲区中的数据.
        /// </summary>
        /// <param name="port">串口号.</param>
        /// <param name="func">刷新策略：0-刷新输入缓冲区；1-刷新输出缓冲区；2-刷新输入和输出缓冲区.</param>
        /// <returns>结果.</returns>
        [DllImport("PComm.dll", EntryPoint = "sio_flush")]
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public static extern int sio_flush(int port, int func);
#pragma warning restore SA1300 // Element should begin with upper-case letter

        /// <summary>
        /// 关闭串口.
        /// </summary>
        /// <param name="port">串口号.</param>
        /// <returns>结果.</returns>
        [DllImport("PComm.dll", EntryPoint = "sio_close")]
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public static extern int sio_close(int port);
#pragma warning restore SA1300 // Element should begin with upper-case letter

        /// <summary>
        /// 读取驱动程序中输入缓冲区的数据.
        /// </summary>
        /// <param name="port">串口号.</param>
        /// <param name="buf">接受缓冲区指针.</param>
        /// <param name="length">每次被读取的数据长度.</param>
        /// <returns>结果.</returns>
        [DllImport("PComm.dll", EntryPoint = "sio_read")]
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public static extern int sio_read(int port, ref byte buf, int length);
#pragma warning restore SA1300 // Element should begin with upper-case letter

        /// <summary>
        /// 将一个数据块放到驱动程序的输出缓冲区中.
        /// </summary>
        /// <param name="port">串口号.</param>
        /// <param name="buf">发送缓冲区指针.</param>
        /// <param name="length">发送数据长度.</param>
        /// <returns>结果.</returns>
        [DllImport("PComm.dll", EntryPoint = "sio_write")]
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public static extern int sio_write(int port, ref byte buf, int length);
#pragma warning restore SA1300 // Element should begin with upper-case letter

        /// <summary>
        /// 将一个字符写入到驱动程序的输出缓冲区中.
        /// </summary>
        /// <param name="port">串口号.</param>
        /// <param name="term">字符，0-255.</param>
        /// <returns>结果.</returns>
        [DllImport("PComm.dll", EntryPoint = "sio_putch")]
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public static extern int sio_putch(int port, int term);
#pragma warning restore SA1300 // Element should begin with upper-case letter

        /// <summary>
        /// 从驱动程序的输入缓冲区中读取一个字符.
        /// </summary>
        /// <param name="port">串口号.</param>
        /// <returns>结果.</returns>
        [DllImport("PComm.dll", EntryPoint = "sio_getch")]
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public static extern int sio_getch(int port);
#pragma warning restore SA1300 // Element should begin with upper-case letter
    }
}