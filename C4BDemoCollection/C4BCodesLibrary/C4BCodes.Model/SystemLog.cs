﻿// <copyright file="SystemLog.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Model
{
    using System;

    /// <summary>
    /// 系统日志.
    /// </summary>
    public class SystemLog
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets 日志时间.
        /// </summary>
        public DateTime LogTime { get; set; }

        /// <summary>
        /// Gets or sets 日志类型.
        /// </summary>
        public int LogType { get; set; }

        /// <summary>
        /// Gets or sets 日志内容.
        /// </summary>
        public string LogContent { get; set; }

        /// <summary>
        /// Gets or sets 最后编辑人.
        /// </summary>
        public string LastModifiedUserId { get; set; }

        /// <summary>
        /// 重写ToString().
        /// </summary>
        /// <returns>res.</returns>
        public override string ToString()
        {
            return $"Id：{this.Id},LogTime：{this.LogTime.ToString()},LogType：{this.LogType},LogContent：" +
                $"{this.LogContent},LastModifiedUserId：{this.LastModifiedUserId}";
        }
    }
}
