﻿// <copyright file="canfd_frame.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// canfd_frame.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
#pragma warning disable SA1300 // Element should begin with upper-case letter
    public struct canfd_frame
#pragma warning restore SA1300 // Element should begin with upper-case letter
    {
        /// <summary>
        /// can_id.
        /// </summary>
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
#pragma warning disable SA1310 // Field names should not contain underscore
        public uint can_id;  /* 32 bit MAKE_CAN_ID + EFF/RTR/ERR flags */

        /// <summary>
        /// len.
        /// </summary>
        public byte len;     /* frame payload length in byte */

        /// <summary>
        /// flags.
        /// </summary>
        public byte flags;   /* additional flags for CAN FD,i.e error code */

        /// <summary>
        /// __res0.
        /// </summary>
        public byte res0;  /* reserved / padding */

        /// <summary>
        /// __res1.
        /// </summary>
        public byte res1;  /* reserved / padding */

        /// <summary>
        /// data.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public byte[] data/* __attribute__((aligned(8)))*/;
#pragma warning restore SA1310 // Field names should not contain underscore
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter
    }
}
