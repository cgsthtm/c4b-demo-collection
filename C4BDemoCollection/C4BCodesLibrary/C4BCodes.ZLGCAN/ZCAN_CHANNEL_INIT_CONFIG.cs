﻿// <copyright file="ZCAN_CHANNEL_INIT_CONFIG.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// ZCAN_CHANNEL_INIT_CONFIG.
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct ZCAN_CHANNEL_INIT_CONFIG
    {
        /// <summary>
        /// can_type.
        /// </summary>
        [FieldOffset(0)]
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
#pragma warning disable SA1310 // Field names should not contain underscore
        public uint can_type; // type:TYPE_CAN TYPE_CANFD

        /// <summary>
        /// can.
        /// </summary>
        [FieldOffset(4)]
        public ZCAN can;

        /// <summary>
        /// canfd.
        /// </summary>
        [FieldOffset(4)]
        public CANFD canfd;
#pragma warning restore SA1310 // Field names should not contain underscore
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter
    }
}
