﻿// <copyright file="IProperty.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using C4BCodesLibrary.C4BCodes.Utilities.ZLGCAN;

    /// <summary>
    /// IProperty.
    /// </summary>
    public struct IProperty
    {
        /// <summary>
        /// SetValue.
        /// </summary>
        public SetValueFunc SetValue;

        /// <summary>
        /// GetValue.
        /// </summary>
        public GetValueFunc GetValue;

        /// <summary>
        /// GetPropertys.
        /// </summary>
        public GetPropertysFunc GetPropertys;
    }
}
