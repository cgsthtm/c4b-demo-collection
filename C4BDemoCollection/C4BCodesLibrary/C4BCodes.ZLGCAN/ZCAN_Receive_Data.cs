﻿// <copyright file="ZCAN_Receive_Data.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// ZCAN_Receive_Data.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN_Receive_Data
    {
        /// <summary>
        /// frame.
        /// </summary>
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
        public can_frame frame;
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter

        /// <summary>
        /// timestamp.
        /// </summary>
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
        public ulong timestamp; // us
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter
    }
}
