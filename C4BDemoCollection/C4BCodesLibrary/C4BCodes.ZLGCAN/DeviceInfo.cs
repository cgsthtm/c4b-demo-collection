﻿// <copyright file="DeviceInfo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    /// <summary>
    /// DeviceInfo.
    /// </summary>
    public struct DeviceInfo
    {
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
#pragma warning disable SA1310 // Field names should not contain underscore
        /// <summary>
        /// device_type.
        /// </summary>
        public uint device_type;   // 设备类型

        /// <summary>
        /// channel_count.
        /// </summary>
        public uint channel_count; // 设备的通道个数
#pragma warning restore SA1310 // Field names should not contain underscore
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceInfo"/> struct.
        /// </summary>
        /// <param name="type">type.</param>
        /// <param name="count">count.</param>
        public DeviceInfo(uint type, uint count)
        {
            this.device_type = type;
            this.channel_count = count;
        }
    }
}
