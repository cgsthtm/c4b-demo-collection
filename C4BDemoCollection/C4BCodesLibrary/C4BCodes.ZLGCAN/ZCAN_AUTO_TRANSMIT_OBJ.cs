﻿// <copyright file="ZCAN_AUTO_TRANSMIT_OBJ.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// ZCAN_AUTO_TRANSMIT_OBJ.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN_AUTO_TRANSMIT_OBJ // CAN定时发送帧结构体
    {
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
        /// <summary>
        /// enable.
        /// </summary>
        public ushort enable;           // 0-禁用，1-使能

        /// <summary>
        /// index.
        /// </summary>
        public ushort index;            // 定时报文索引

        /// <summary>
        /// interval.
        /// </summary>
        public uint interval;           // 定时周期

        /// <summary>
        /// obj.
        /// </summary>
        public ZCAN_Transmit_Data obj;
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter
    }
}
