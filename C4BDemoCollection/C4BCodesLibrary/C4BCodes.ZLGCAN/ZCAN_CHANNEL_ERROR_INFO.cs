﻿// <copyright file="ZCAN_CHANNEL_ERROR_INFO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// ZCAN_CHANNEL_ERROR_INFO.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN_CHANNEL_ERROR_INFO
    {
        /// <summary>
        /// error_code.
        /// </summary>
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
#pragma warning disable SA1310 // Field names should not contain underscore
        public uint error_code;

        /// <summary>
        /// passive_ErrData.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public byte[] passive_ErrData;

        /// <summary>
        /// arLost_ErrData.
        /// </summary>
        public byte arLost_ErrData;
#pragma warning restore SA1310 // Field names should not contain underscore
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter
    }
}
