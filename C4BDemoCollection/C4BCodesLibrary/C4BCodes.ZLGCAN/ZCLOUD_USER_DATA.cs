﻿// <copyright file="ZCLOUD_USER_DATA.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System.Runtime.InteropServices;
    using C4BCodesLibrary.C4BCodes.Utilities.ZLGCAN;

    /// <summary>
    /// ZCLOUD_USER_DATA.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ZCLOUD_USER_DATA
    {
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
        /// <summary>
        /// username.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] username;

        /// <summary>
        /// mobile.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] mobile;

        /// <summary>
        /// dllVer.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public char[] dllVer;

        /// <summary>
        /// devCnt.
        /// </summary>
        public uint devCnt;

        /// <summary>
        /// devices.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 100)]
        public ZCLOUD_DEVINFO[] devices;
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter
    }
}
