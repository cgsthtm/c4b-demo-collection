﻿// <copyright file="Define.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    /// <summary>
    /// Define.
    /// </summary>
    public class Define
    {
        /// <summary>
        /// TYPE_CAN.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public const int TYPE_CAN = 0;

        /// <summary>
        /// TYPE_CANFD.
        /// </summary>
        public const int TYPE_CANFD = 1;

        /// <summary>
        /// ZCAN_USBCAN1.
        /// </summary>
        public const int ZCAN_USBCAN1 = 3;

        /// <summary>
        /// ZCAN_USBCAN2.
        /// </summary>
        public const int ZCAN_USBCAN2 = 4;

        /// <summary>
        /// ZCAN_CANETUDP.
        /// </summary>
        public const int ZCAN_CANETUDP = 12;

        /// <summary>
        /// ZCAN_CANETTCP.
        /// </summary>
        public const int ZCAN_CANETTCP = 17;

        /// <summary>
        /// ZCAN_USBCAN_E_U.
        /// </summary>
        public const int ZCAN_USBCAN_E_U = 20;

        /// <summary>
        /// ZCAN_USBCAN_2E_U.
        /// </summary>
        public const int ZCAN_USBCAN_2E_U = 21;

        /// <summary>
        /// ZCAN_PCIECANFD_100U.
        /// </summary>
        public const int ZCAN_PCIECANFD_100U = 38;

        /// <summary>
        /// ZCAN_PCIECANFD_200U.
        /// </summary>
        public const int ZCAN_PCIECANFD_200U = 39;

        /// <summary>
        /// ZCAN_PCIECANFD_200U_EX.
        /// </summary>
        public const int ZCAN_PCIECANFD_200U_EX = 62;

        /// <summary>
        /// ZCAN_PCIECANFD_400U.
        /// </summary>
        public const int ZCAN_PCIECANFD_400U = 61;

        /// <summary>
        /// ZCAN_USBCANFD_800U.
        /// </summary>
        public const int ZCAN_USBCANFD_800U = 59;

        /// <summary>
        /// ZCAN_USBCANFD_200U.
        /// </summary>
        public const int ZCAN_USBCANFD_200U = 41;

        /// <summary>
        /// ZCAN_USBCANFD_100U.
        /// </summary>
        public const int ZCAN_USBCANFD_100U = 42;

        /// <summary>
        /// ZCAN_USBCANFD_MINI.
        /// </summary>
        public const int ZCAN_USBCANFD_MINI = 43;

        /// <summary>
        /// ZCAN_CLOUD.
        /// </summary>
        public const int ZCAN_CLOUD = 46;

        /// <summary>
        /// ZCAN_CANFDNET_200U_TCP.
        /// </summary>
        public const int ZCAN_CANFDNET_200U_TCP = 48;

        /// <summary>
        /// ZCAN_CANFDNET_200U_UDP.
        /// </summary>
        public const int ZCAN_CANFDNET_200U_UDP = 49;

        /// <summary>
        /// ZCAN_CANFDNET_400U_TCP.
        /// </summary>
        public const int ZCAN_CANFDNET_400U_TCP = 52;

        /// <summary>
        /// ZCAN_CANFDNET_400U_UDP.
        /// </summary>
        public const int ZCAN_CANFDNET_400U_UDP = 53;

        /// <summary>
        /// ZCAN_CANFDNET_800U_TCP.
        /// </summary>
        public const int ZCAN_CANFDNET_800U_TCP = 57;

        /// <summary>
        /// ZCAN_CANFDNET_800U_UDP.
        /// </summary>
        public const int ZCAN_CANFDNET_800U_UDP = 58;

        /// <summary>
        /// STATUS_ERR.
        /// </summary>
        public const int STATUS_ERR = 0;

        /// <summary>
        /// STATUS_OK.
        /// </summary>
        public const int STATUS_OK = 1;
#pragma warning restore SA1310 // Field names should not contain underscore
    }
}
