﻿// <copyright file="ZCLOUD_DEVINFO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// ZCLOUD_DEVINFO. for zlg cloud.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ZCLOUD_DEVINFO
    {
        /// <summary>
        /// devIndex.
        /// </summary>
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
        public int devIndex;

        /// <summary>
        /// type.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] type;

        /// <summary>
        /// id.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] id;

        /// <summary>
        /// name.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] name;

        /// <summary>
        /// owner.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] owner;

        /// <summary>
        /// model.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] model;

        /// <summary>
        /// fwVer.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public char[] fwVer;

        /// <summary>
        /// hwVer.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public char[] hwVer;

        /// <summary>
        /// serial.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] serial;

        /// <summary>
        /// status.
        /// </summary>
        public int status;             // 0:online, 1:offline

        /// <summary>
        /// bGpsUploads.
        /// </summary>
        //// [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte bGpsUploads;

        /// <summary>
        /// channelCnt.
        /// </summary>
        public byte channelCnt;   // each channel enable can upload

        /// <summary>
        /// channels.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public ZCLOUD_CHNINFO[] channels;
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter
    }
}
