﻿// <copyright file="ZCAN_Transmit_Data.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// ZCAN_Transmit_Data.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN_Transmit_Data
    {
        /// <summary>
        /// frame.
        /// </summary>
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
        public can_frame frame;

        /// <summary>
        /// transmit_type.
        /// </summary>
#pragma warning disable SA1310 // Field names should not contain underscore
        public uint transmit_type;
#pragma warning restore SA1310 // Field names should not contain underscore
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter

    }
}
