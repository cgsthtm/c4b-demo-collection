﻿// <copyright file="ZCLOUD_CHNINFO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// ZCLOUD_CHNINFO. for zlg cloud.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ZCLOUD_CHNINFO
    {
        /// <summary>
        /// enable.
        /// </summary>
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
        public byte enable;

        /// <summary>
        /// type.
        /// </summary>
        public byte type;

        /// <summary>
        /// isUpload.
        /// </summary>
        public byte isUpload;

        /// <summary>
        /// isDownload.
        /// </summary>
        public byte isDownload;
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter
    }
}
