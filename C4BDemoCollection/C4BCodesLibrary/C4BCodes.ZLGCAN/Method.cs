﻿// <copyright file="Method.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Method.
    /// </summary>
    public class Method
    {
        /// <summary>
        /// ZCAN_OpenDevice.
        /// </summary>
        /// <param name="device_type">设备类型.</param>
        /// <param name="device_index">设备索引.</param>
        /// <param name="reserved">保留.</param>
        /// <returns>打开设备的指针.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ZCAN_OpenDevice(uint device_type, uint device_index, uint reserved);

        /// <summary>
        /// ZCAN_CloseDevice.
        /// </summary>
        /// <param name="device_handle">设备指针.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_CloseDevice(IntPtr device_handle);

        /// <summary>
        /// ZCAN_InitCAN.
        /// </summary>
        /// <param name="device_handle">设备指针.</param>
        /// <param name="can_index">can_index.</param>
        /// <param name="pInitConfig">pInitConfig.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ZCAN_InitCAN(IntPtr device_handle, uint can_index, IntPtr pInitConfig);

        /// <summary>
        /// 设置设备属性.
        /// </summary>
        /// <param name="device_handle">设备指针.</param>
        /// <param name="path">路径.</param>
        /// <param name="value">值.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_SetValue(IntPtr device_handle, string path, byte[] value);

        /// <summary>
        /// 设置设备属性.
        /// </summary>
        /// <param name="device_handle">设备指针.</param>
        /// <param name="path">path.</param>
        /// <param name="value">val.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_SetValue(IntPtr device_handle, string path, string value);

        /// <summary>
        /// 设置设备属性.
        /// </summary>
        /// <param name="device_handle">设备指针.</param>
        /// <param name="path">path.</param>
        /// <param name="value">val.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_SetValue(IntPtr device_handle, string path, IntPtr value);

        /// <summary>
        /// ZCAN_StartCAN.
        /// </summary>
        /// <param name="channel_handle">设备指针.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_StartCAN(IntPtr channel_handle);

        /// <summary>
        /// ZCAN_ResetCAN.
        /// </summary>
        /// <param name="channel_handle">设备指针.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_ResetCAN(IntPtr channel_handle);

        /// <summary>
        /// 清除缓冲区.
        /// </summary>
        /// <param name="channel_handle">通道指针.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_ClearBuffer(IntPtr channel_handle);

        /// <summary>
        /// ZCAN_Transmit.
        /// </summary>
        /// <param name="channel_handle">通道指针.</param>
        /// <param name="pTransmit">pTransmit.</param>
        /// <param name="len">len.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_Transmit(IntPtr channel_handle, IntPtr pTransmit, uint len);

        /// <summary>
        /// ZCAN_TransmitFD.
        /// </summary>
        /// <param name="channel_handle">通道指针.</param>
        /// <param name="pTransmit">pTransmit.</param>
        /// <param name="len">len.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_TransmitFD(IntPtr channel_handle, IntPtr pTransmit, uint len);

        /// <summary>
        /// ZCAN_GetReceiveNum.
        /// </summary>
        /// <param name="channel_handle">通道指针.</param>
        /// <param name="type">type.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_GetReceiveNum(IntPtr channel_handle, byte type);

        /// <summary>
        /// 获取设备属性.
        /// </summary>
        /// <param name="device_handle">设备指针.</param>
        /// <param name="path">path.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ZCAN_GetValue(IntPtr device_handle, string path);

        /// <summary>
        /// ZCAN_Receive.
        /// </summary>
        /// <param name="channel_handle">通道指针.</param>
        /// <param name="data">data.</param>
        /// <param name="len">len.</param>
        /// <param name="wait_time">waittime.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_Receive(IntPtr channel_handle, IntPtr data, uint len, int wait_time = -1);

        /// <summary>
        /// ZCAN_ReceiveFD.
        /// </summary>
        /// <param name="channel_handle">通道句柄.</param>
        /// <param name="data">data.</param>
        /// <param name="len">len.</param>
        /// <param name="wait_time">wait_time.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_ReceiveFD(IntPtr channel_handle, IntPtr data, uint len, int wait_time = -1);

        /// <summary>
        /// ZCAN_ReadChannelErrInfo.
        /// </summary>
        /// <param name="channel_handle">通道句柄.</param>
        /// <param name="pErrInfo">pErrInfo.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_ReadChannelErrInfo(IntPtr channel_handle, IntPtr pErrInfo);

        /// <summary>
        /// GetIProperty.
        /// </summary>
        /// <param name="device_handle">设备句柄.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr GetIProperty(IntPtr device_handle);

        /// <summary>
        /// ZCLOUD_IsConnected.
        /// </summary>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool ZCLOUD_IsConnected();

        /// <summary>
        /// ZCLOUD_SetServerInfo.
        /// </summary>
        /// <param name="httpAddr">httpAddr.</param>
        /// <param name="httpPort">httpPort.</param>
        /// <param name="mqttAddr">mqttAddr.</param>
        /// <param name="mqttPort">mqttPort.</param>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void ZCLOUD_SetServerInfo(string httpAddr, ushort httpPort, string mqttAddr, ushort mqttPort);

        /// <summary>
        /// ZCLOUD_ConnectServer.
        /// </summary>
        /// <param name="username">username.</param>
        /// <param name="password">password.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCLOUD_ConnectServer(string username, string password);

        /// <summary>
        /// ZCLOUD_DisconnectServer.
        /// </summary>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCLOUD_DisconnectServer();

        /// <summary>
        /// ZCLOUD_GetUserData.
        /// </summary>
        /// <param name="updata">updata.</param>
        /// <returns>res.</returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ZCLOUD_GetUserData(int updata);
    }
}