﻿// <copyright file="ZCAN.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// ZCAN.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN
    {
        /// <summary>
        /// acc_code.
        /// </summary>
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
#pragma warning disable SA1310 // Field names should not contain underscore
        public uint acc_code;

        /// <summary>
        /// acc_mask.
        /// </summary>
        public uint acc_mask;

        /// <summary>
        /// reserved.
        /// </summary>
        public uint reserved;

        /// <summary>
        /// filter.
        /// </summary>
        public byte filter;

        /// <summary>
        /// timing0.
        /// </summary>
        public byte timing0;

        /// <summary>
        /// timing1.
        /// </summary>
        public byte timing1;

        /// <summary>
        /// mode.
        /// </summary>
        public byte mode;
#pragma warning restore SA1310 // Field names should not contain underscore
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter
    }
}
