﻿// <copyright file="CANFD.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// CANFD.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CANFD
    {
        /// <summary>
        /// acc_code.
        /// </summary>
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
#pragma warning disable SA1310 // Field names should not contain underscore
        public uint acc_code;

        /// <summary>
        /// acc_mask.
        /// </summary>
        public uint acc_mask;

        /// <summary>
        /// abit_timing.
        /// </summary>
        public uint abit_timing;

        /// <summary>
        /// dbit_timing.
        /// </summary>
        public uint dbit_timing;

        /// <summary>
        /// brp.
        /// </summary>
        public uint brp;

        /// <summary>
        /// filter.
        /// </summary>
        public byte filter;

        /// <summary>
        /// mode.
        /// </summary>
        public byte mode;

        /// <summary>
        /// pad.
        /// </summary>
        public ushort pad;

        /// <summary>
        /// reserved.
        /// </summary>
        public uint reserved;
#pragma warning restore SA1310 // Field names should not contain underscore
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter
    }
}