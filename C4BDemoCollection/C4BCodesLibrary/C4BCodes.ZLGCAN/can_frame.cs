﻿// <copyright file="can_frame.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.ZLGCAN
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// can_frame.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
#pragma warning disable SA1300 // Element should begin with upper-case letter
    public struct can_frame
#pragma warning restore SA1300 // Element should begin with upper-case letter
    {
        /// <summary>
        /// can_id.
        /// </summary>
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
#pragma warning disable SA1310 // Field names should not contain underscore
        public uint can_id;  /* 32 bit MAKE_CAN_ID + EFF/RTR/ERR flags */

        /// <summary>
        /// can_dlc.
        /// </summary>
        public byte can_dlc; /* frame payload length in byte (0 .. CAN_MAX_DLEN) */

        /// <summary>
        /// pad.
        /// </summary>
        public byte pad;   /* padding */

        /// <summary>
        /// res0.
        /// </summary>
        public byte res0;  /* reserved / padding */

        /// <summary>
        /// res1.
        /// </summary>
        public byte res1;  /* reserved / padding */

        /// <summary>
        /// data.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] data/* __attribute__((aligned(8)))*/;
#pragma warning restore SA1310 // Field names should not contain underscore
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter
    }
}