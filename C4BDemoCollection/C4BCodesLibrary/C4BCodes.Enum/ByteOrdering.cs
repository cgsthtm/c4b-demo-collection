﻿// <copyright file="ByteOrdering.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Enum
{
    /// <summary>
    /// 字节序枚举.
    /// </summary>
    public enum ByteOrdering
    {
        /// <summary>
        /// 大端序.高位在前.
        /// </summary>
        BigEndian,

        /// <summary>
        /// 小端序.低位在前.
        /// </summary>
        LittleEndian,
    }
}
