﻿// <copyright file="EnumExtensions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Extensions
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// 枚举扩展类.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// 获取描述特性值.
        /// </summary>
        /// <typeparam name="T">泛型T.</typeparam>
        /// <param name="value">针对哪个枚举值获取Description.</param>
        /// <returns>返回描述信息.</returns>
        /// <exception cref="ArgumentException">ex.</exception>
        public static string GetDescription<T>(this T value)
            where T : struct
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException($"类型 '{typeof(T).FullName}' 不是枚举类型。");
            }

            var fieldInfo = typeof(T).GetField(value.ToString());
            if (fieldInfo == null)
            {
                throw new ArgumentException($"枚举 '{typeof(T).FullName}' 中未找到值 '{value}'.");
            }

            var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }
    }
}