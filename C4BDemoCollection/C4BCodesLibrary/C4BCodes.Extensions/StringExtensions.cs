﻿// <copyright file="StringExtensions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Extensions
{
    using System.Text;

    /// <summary>
    /// 字符串扩展类.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// 扩展方法，删除该字符串中最后一个指定的字符之后的部分.
        /// </summary>
        /// <param name="str">源字符串.</param>
        /// <param name="strchar">指定字符.</param>
        /// <returns>被删除后的字符串.</returns>
        public static string DelAfterLastChar(this string str, string strchar)
        {
            return str.Substring(0, str.LastIndexOf(strchar));
        }

        /// <summary>
        /// 扩展方法，获取字符串的最后N个字符串.
        /// </summary>
        /// <param name="str">源字符串.</param>
        /// <param name="len">长度.</param>
        /// <returns>字符串的最后len个字符串.</returns>
        public static string GetAftermostString(this string str, int len)
        {
            string rtn = string.Empty;
            if (str.Length >= len)
            {
                rtn = str.Substring(str.Length - len, len);
            }

            return rtn;
        }

        /// <summary>
        /// 字符串转Unicode字符串.
        /// </summary>
        /// <param name="source">源字符串.</param>
        /// <returns>Unicode编码后的字符串.</returns>
        public static string ToUnicodeStr(this string source)
        {
            var bytes = Encoding.Unicode.GetBytes(source);
            var stringBuilder = new StringBuilder();
            for (var i = 0; i < bytes.Length; i += 2)
            {
                stringBuilder.AppendFormat("\\u{0:x2}{1:x2}", bytes[i + 1], bytes[i]);
            }

            return stringBuilder.ToString();
        }
    }
}