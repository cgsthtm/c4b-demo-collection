﻿// <copyright file="WINDOWPLACEMENT_FLAGS.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Win32Api
{
    using System;

    /// <summary>
    /// WINDOWPLACEMENT_FLAGS.
    /// </summary>
    [Flags]
    public enum WINDOWPLACEMENT_FLAGS : int
    {
        /// <summary>
        /// WPF_SETMINPOSITION.
        /// </summary>
        WPF_SETMINPOSITION = 0x00000001,

        /// <summary>
        /// WPF_SETMAXPOSITION.
        /// </summary>
        WPF_SETMAXPOSITION = 0x00000002,

        /// <summary>
        /// WPF_SETSHOWSTATE.
        /// </summary>
        WPF_SETSHOWSTATE = 0x00000004,

        /// <summary>
        /// WPF_SETWINDOWPOS.
        /// </summary>
        WPF_SETWINDOWPOS = 0x00000008,

        /// <summary>
        /// WPF_RESTORETOMAXIMIZED.
        /// </summary>
        WPF_RESTORETOMAXIMIZED = 0x00000010,

        /// <summary>
        /// WPF_SETHIDEWINDOW.
        /// </summary>
        WPF_SETHIDEWINDOW = 0x00000020,

        /// <summary>
        /// WPF_SETSHOWWITHOUTACTIVATING.
        /// </summary>
        WPF_SETSHOWWITHOUTACTIVATING = 0x00000040,
    }
}
