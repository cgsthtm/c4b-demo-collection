﻿// <copyright file="WINDOWPLACEMENT.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Win32Api
{
    /// <summary>
    /// WINDOWPLACEMENT.
    /// </summary>
    public struct WINDOWPLACEMENT
    {
        /// <summary>
        /// Length.
        /// </summary>
        public uint Length;

        /// <summary>
        /// Flags.
        /// </summary>
        public uint Flags;

        /// <summary>
        /// ShowCmd.
        /// </summary>
        public uint ShowCmd;

        /// <summary>
        /// PtMinPosition.
        /// </summary>
        public POINT PtMinPosition;

        /// <summary>
        /// PtMaxPosition.
        /// </summary>
        public POINT PtMaxPosition;

        /// <summary>
        /// RcNormalPosition.
        /// </summary>
        public RECT RcNormalPosition;

        /// <summary>
        /// RcDevice.
        /// </summary>
        public RECT RcDevice;
    }
}
