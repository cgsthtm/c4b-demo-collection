﻿// <copyright file="RECT.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Win32Api
{
    /// <summary>
    /// RECT.
    /// </summary>
    public struct RECT
    {
        /// <summary>
        /// Left.
        /// </summary>
        public int Left;

        /// <summary>
        /// Top.
        /// </summary>
        public int Top;

        /// <summary>
        /// Right.
        /// </summary>
        public int Right;

        /// <summary>
        /// Bottom.
        /// </summary>
        public int Bottom;
    }
}
