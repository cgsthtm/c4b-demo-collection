﻿// <copyright file="POINT.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Win32Api
{
    /// <summary>
    /// POINT.
    /// </summary>
    public struct POINT
    {
        /// <summary>
        /// X.
        /// </summary>
        public int X;

        /// <summary>
        /// Y.
        /// </summary>
        public int Y;

        /// <summary>
        /// Initializes a new instance of the <see cref="POINT"/> struct.
        /// </summary>
        /// <param name="x">X.</param>
        /// <param name="y">Y.</param>
        public POINT(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
