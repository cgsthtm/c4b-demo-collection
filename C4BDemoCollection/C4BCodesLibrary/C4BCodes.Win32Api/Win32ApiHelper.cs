﻿// <copyright file="Win32ApiHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4BCodesLibrary.C4BCodes.Win32Api
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Win32API帮助类.
    /// </summary>
    public class Win32ApiHelper
    {
        // 常量定义

        /// <summary>
        /// HWND_TOPMOST.
        /// </summary>
        public const int HWND_TOPMOST = -1;

        /// <summary>
        /// HWND_NOTOPMOST.
        /// </summary>
        public const int HWND_NOTOPMOST = -2;

        /// <summary>
        /// SWP_NOMOVE.
        /// </summary>
        public const int SWP_NOMOVE = 0x0002;

        /// <summary>
        /// SWP_NOSIZE.
        /// </summary>
        public const int SWP_NOSIZE = 0x0001;

        /// <summary>
        /// SWP_SHOWWINDOW.
        /// </summary>
        public const int SWP_SHOWWINDOW = 0x0040;

        // nCmdShow

        /// <summary>
        /// 隐藏窗口并激活另一个窗口.
        /// </summary>
        public const int SW_HIDE = 0;

        /// <summary>
        /// 激活并显示窗口.
        /// </summary>
        public const int SW_SHOWNORMAL = 1;

        /// <summary>
        /// 激活并显示窗口.
        /// </summary>
        public const int SW_NORMAL = 1;

        /// <summary>
        /// SW_SHOWMAXIMIZED.
        /// </summary>
        public const int SW_SHOWMAXIMIZED = 3;

        /// <summary>
        /// SW_MAXIMIZE.
        /// </summary>
        public const int SW_MAXIMIZE = 3;

        // nIndex

        /// <summary>
        /// 检索扩展窗口样式.
        /// </summary>
        public const int GWL_EXSTYLE = -20;

        /// <summary>
        /// 检索应用程序实例的句柄.
        /// </summary>
        public const int GWLP_HINSTANCE = -6;

        /// <summary>
        /// 检索父窗口的句柄（如果有）.
        /// </summary>
        public const int GWLP_HWNDPARENT = -8;

        /// <summary>
        /// 检索窗口的标识符.
        /// </summary>
        public const int GWLP_ID = -12;

        /// <summary>
        /// 检索窗口样式.
        /// </summary>
        public const int GWL_STYLE = -16;

        /// <summary>
        /// 检索与窗口关联的用户数据.
        /// </summary>
        public const int GWLP_USERDATA = -21;

        /// <summary>
        /// 检索指向窗口过程的指针，或表示指向窗口过程的指针的句柄.
        /// </summary>
        public const int GWLP_WNDPROC = -21;

        // 扩展的窗口样式

        /// <summary>
        /// 窗口应放置在所有非最顶部窗口的上方，并且应保持在窗口上方，即使窗口已停用也是如此。若要添加或删除此样式，请使用SetWindowPos函数.
        /// </summary>
        public const long WS_EX_TOPMOST = 0x00000008L;

        /// <summary>
        /// .
        /// </summary>
        public const int WmSyscommand = 0x0112;

        /// <summary>
        /// .
        /// </summary>
        public const int Htcaption = 0x0002;

        /// <summary>
        /// 移动.
        /// </summary>
        public const int ScMove = 0xF010;

        // 定义 Windows API 函数

        /// <summary>
        /// 设置窗体位置.
        /// </summary>
        /// <param name="hWnd">hWnd.</param>
        /// <param name="hWndInsertAfter">hWndInsertAfter.</param>
        /// <param name="X">X.</param>
        /// <param name="Y">Y.</param>
        /// <param name="cx">c.</param>
        /// <param name="cy">cy.</param>
        /// <param name="uFlags">uFlags.</param>
        /// <returns>bool.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        /// <summary>
        /// 获取窗体位置.
        /// </summary>
        /// <param name="hWnd">hWnd.</param>
        /// <param name="lpwndpl">lpwndpl.</param>
        /// <returns>bool.</returns>
        [DllImport("user32.dll")]
        public static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        /// <summary>
        /// 检索有关指定窗口的信息.函数还会将指定偏移量的值检索到额外的窗口内存中.
        /// </summary>
        /// <param name="hWnd">窗口的句柄.</param>
        /// <param name="nIndex">要检索的值的从零开始的偏移量.</param>
        /// <returns>如果函数成功，则返回值是请求的值。如果函数失败，则返回值为零.
        /// <para>如果以前未调用SetWindowLong或SetWindowLongPtr，则GetWindowLongPtr为额外窗口或类内存中的值返回零.</para></returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        /// <summary>
        /// 更改指定窗口的属性.
        /// </summary>
        /// <param name="hWnd">窗口的句柄.</param>
        /// <param name="nIndex">要设置的值的从零开始的偏移量.</param>
        /// <param name="dwNewLong">替换值.</param>
        /// <returns>如果函数成功，则返回值是指定偏移量的上一个值。如果函数失败，则返回值为零.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        /// <summary>
        /// 窗体是否置顶.
        /// </summary>
        /// <param name="hWnd">窗体句柄.</param>
        /// <returns>是否置顶.</returns>
        public static bool IsTopMost(IntPtr hWnd)
        {
            int exStyle = GetWindowLong(hWnd, GWL_EXSTYLE);
            return (exStyle & WS_EX_TOPMOST) != 0;
        }

        /// <summary>
        /// 设置窗体置顶.
        /// </summary>
        /// <param name="hWnd">窗体句柄.</param>
        /// <param name="value">是否置顶.</param>
        public static void SetTopMost(IntPtr hWnd, bool value)
        {
            SetWindowPos(hWnd, (IntPtr)(value ? HWND_TOPMOST : HWND_NOTOPMOST), 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        }

        /// <summary>
        /// <para>该函数从当前线程中的窗口释放鼠标捕获，并恢复通常的鼠标输入处理。捕获鼠标的窗口接收所有</para>
        /// <para>的鼠标输入（无论光标的位置在哪里），除非点击鼠标键时，光标热点在另一个线程的窗口中。</para>
        /// </summary>
        /// <returns>.</returns>
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        /// <summary>
        /// <para>该函数将指定的消息发送到一个或多个窗口.</para>
        /// <para>此函数为指定的窗口调用窗口程序直到窗口程序处理完消息再返回.</para>
        /// <para>而函数PostMessage不同，将一个消息寄送到一个线程的消息队列后立即返回.</para>
        /// return 返回值 : 指定消息处理的结果，依赖于所发送的消息.
        /// </summary>
        /// <param name="hWnd">要接收消息的那个窗口的句柄</param>
        /// <param name="msg">消息的标识符.</param>
        /// <param name="wParam">具体取决于消息.</param>
        /// <param name="lParam">具体取决于消息1.</param>
        /// <returns>指定消息处理的结果，依赖于所发送的消息.</returns>
        [DllImport("User32.dll", CharSet = CharSet.Auto, EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);
    }
}
