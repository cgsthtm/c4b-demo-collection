using C4BCodesLibrary.C4BCodes.Utilities;

namespace C4B.XUnit.Net.Demo
{
    /* xUnit.net includes support for two different major types of unit tests: facts and theories. When describing the difference between facts and theories, we like to say:
    Facts are tests which are always true. They test invariant conditions.
    Theories are tests which are only true for a particular set of data.
    */
    public class UnitTest1
    {
        [Fact]
        public void PassingTest()
        {
            Assert.Equal(4, Add(2, 2));
        }

        [Fact]
        public void FailingTest()
        {
            Assert.Equal(5, Add(2, 2));
        }

        int Add(int x, int y)
        {
            return x + y;
        }

        // each theory with its data set is a separate test. 
        [Theory]
        [InlineData(3)]
        [InlineData(5)]
        [InlineData(6)]
        public void MyFirstTheory(int value)
        {
            Assert.True(IsOdd(value));
        }

        // A good example of this is testing numeric algorithms. Let's say you want to test an algorithm which determines whether a number is odd or not. 
        // If you're writing the positive-side tests (odd numbers), then feeding even numbers into the test would cause it fail, and not because the test or algorithm is wrong.
        bool IsOdd(int value)
        {
            return value % 2 == 1;
        }

        [Fact]
        public void Test_ByteHelper()
        {
            Assert.Equal("00 01 02", ByteHelper.ToHexString(new byte[] { 0x00, 0x01, 0x02 }, withSpace: true, reverse: false));
        }
    }
}