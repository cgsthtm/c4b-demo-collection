using OpenQA.Selenium;
using OpenQA.Selenium.Edge;

namespace C4B.SeleniumWebDriver.Demo
{
    public partial class Form1 : Form
    {
        // IWebDriver
        IWebDriver driver;

        public Form1()
        {
            InitializeComponent();

            //webDriver = new EdgeDriver(); // 使用环境变量寻找driver
            driver = new EdgeDriver(Application.StartupPath); // 使用绝对路径寻找driver（必须下载与浏览器版本对应的driver，版本不对应将无法使用）
            driver.Url = "https://www.csdn.net/";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cmbCmds.Items.Clear();
            cmbCmds.Items.AddRange(new string[]
            {
                "填写搜索条件点击搜索按钮",
                "点击用户标签进入用户主页",
            });
        }

        private void btnAction_Click(object sender, EventArgs e)
        {
            switch (cmbCmds.Text)
            {
                case "填写搜索条件点击搜索按钮":
                    driver.FindElement(By.XPath("//*[@id=\"toolbar-search-input\"]")).SendKeys("地狱高速公路");
                    driver.FindElement(By.XPath("//*[@id=\"toolbar-search-button\"]")).Click();
                    break;
                case "点击用户标签进入用户主页":
                    string originalWindow = driver.CurrentWindowHandle; // 存储原始窗口的 ID
                    if (driver.WindowHandles.Count > 1) // 检查一下，我们是否打开了其他的窗口
                    {
                        foreach (string window in driver.WindowHandles) // 循环执行，直到找到一个新的窗口句柄
                        {
                            if (originalWindow != window)
                            {
                                driver.SwitchTo().Window(window); // 切换到新窗口
                                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(500); // 等待500毫秒，防止点击未加载完成的表单元素
                                break;
                            }
                        }
                        driver.FindElement(By.XPath("//*[@id=\"app\"]/div[2]/div[2]/div[1]/div[1]/div[1]/div/ul/li[5]")).Click(); // 点击用户标签
                        driver.FindElement(By.XPath("//*[@id=\"app\"]/div[2]/div[2]/div[1]/div[2]/div/div[1]/div/a")).Click(); // 进入用户主页                                                                                                          // 进入用户主页
                    }
                    break;
                default:
                    break;
            }
        }
    }
}