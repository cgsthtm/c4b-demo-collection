﻿// <copyright file="CustomerRegularExpressionValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerRegularExpressionValidator.
    /// </summary>
    internal class CustomerRegularExpressionValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRegularExpressionValidator"/> class.
        /// </summary>
        public CustomerRegularExpressionValidator()
        {
            // 确保指定属性的值与给定的正则表达式匹配。
            // 示例错误：“姓氏”的格式不正确。字符串格式参数：
            /*
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {RegularExpression} – Regular expression that was not matched
                {PropertyPath} - The full path of the property
             */
            this.RuleFor(customer => customer.Surname).Matches("some regex here");
        }
    }
}
