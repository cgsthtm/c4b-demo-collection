﻿// <copyright file="CustomerLengthValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerLengthValidator.
    /// </summary>
    internal class CustomerLengthValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerLengthValidator"/> class.
        /// </summary>
        public CustomerLengthValidator()
        {
            // 示例错误：“姓氏”必须介于1到250个字符之间。您输入了251个字符。
            // 注意：仅对字符串属性有效。
            /*字符串格式参数：
                {PropertyName} – Name of the property being validated
                {MinLength} – Minimum length
                {MaxLength} – Maximum length
                {TotalLength} – Number of characters entered
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */

            // 确保特定字符串属性的长度在指定范围内。但是，它不能确保string属性不为null。
            this.RuleFor(customer => customer.Surname).Length(1, 250); // must be between 1 and 250 chars (inclusive)
        }
    }
}
