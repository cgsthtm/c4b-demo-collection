﻿// <copyright file="CustomerPredicateValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerPredicateValidator.
    /// </summary>
    internal class CustomerPredicateValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerPredicateValidator"/> class.
        /// </summary>
        public CustomerPredicateValidator()
        {
            // 也称为Must。将指定属性的值传递到可对该值执行自定义验证逻辑的委托。
            // 示例错误：“姓氏”不满足指定的条件
            /*String format args:
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */
            this.RuleFor(customer => customer.Surname).Must(surname => surname == "Foo");

            // 请注意，Must还有一个额外的重载，它也接受正在验证的父对象的实例。如果您想将当前属性与谓词中的另一个属性进行比较，这可能很有用：
            this.RuleFor(customer => customer.Surname).Must((customer, surname) => surname != customer.Forename);

            // 请注意，在这个特定的示例中，最好使用NotEqual的跨属性版本。
        }
    }
}
