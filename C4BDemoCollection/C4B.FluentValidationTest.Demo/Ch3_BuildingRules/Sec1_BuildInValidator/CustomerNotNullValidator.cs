﻿// <copyright file="CustomerNotNullValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerNotNullValidator.
    /// </summary>
    internal class CustomerNotNullValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerNotNullValidator"/> class.
        /// </summary>
        public CustomerNotNullValidator()
        {
            // 示例错误：“姓氏”不能为空。
            /*String format args:
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */

            this.RuleFor(customer => customer.Surname).NotNull();
        }
    }
}
