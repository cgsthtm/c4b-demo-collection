﻿// <copyright file="CustomerMinLengthValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerMinLengthValidator.
    /// </summary>
    internal class CustomerMinLengthValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerMinLengthValidator"/> class.
        /// </summary>
        public CustomerMinLengthValidator()
        {
            // 示例错误：“姓氏”的长度必须至少为10个字符。您输入了5个字符。
            // 注意：仅对字符串属性有效。
            /*String format args:
                {PropertyName} – Name of the property being validated
                {MinLength} – Minimum length
                {TotalLength} – Number of characters entered
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */

            this.RuleFor(customer => customer.Surname).MinimumLength(10); // must be 10 chars or more
        }
    }
}
