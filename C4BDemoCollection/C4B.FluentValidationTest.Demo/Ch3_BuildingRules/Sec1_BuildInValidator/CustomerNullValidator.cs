﻿// <copyright file="CustomerNullValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerNullValidator.
    /// </summary>
    internal class CustomerNullValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerNullValidator"/> class.
        /// </summary>
        public CustomerNullValidator()
        {
            // 示例错误：“姓氏”必须为空。
            /*
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */
            this.RuleFor(x => x.Surname).Null();
        }
    }
}
