﻿// <copyright file="CustomerEmptyValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerEmptyValidator.
    /// </summary>
    internal class CustomerEmptyValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerEmptyValidator"/> class.
        /// </summary>
        public CustomerEmptyValidator()
        {
            // 与NotEmpty验证器相反。检查属性值是否为空，或者是否为类型的默认值。
            // 当在IEnumerable上使用时（如数组、集合、列表等），验证器确保IEnumerable是空的。
            // 示例错误：“姓氏”必须为空。
            /*
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */
            this.RuleFor(x => x.Surname).Empty();
        }
    }
}
