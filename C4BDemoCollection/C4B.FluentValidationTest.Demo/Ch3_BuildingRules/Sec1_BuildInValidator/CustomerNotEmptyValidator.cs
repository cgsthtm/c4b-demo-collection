﻿// <copyright file="CustomerNotEmptyValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerNotEmptyValidator.
    /// </summary>
    internal class CustomerNotEmptyValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerNotEmptyValidator"/> class.
        /// </summary>
        public CustomerNotEmptyValidator()
        {
            // 示例错误：“姓氏”不应为空。
            /*字符串格式参数：
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */

            this.RuleFor(customer => customer.Surname).NotEmpty();
        }
    }
}
