﻿// <copyright file="CustomerInclusiveBetweenValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerInclusiveBetweenValidator.
    /// </summary>
    internal class CustomerInclusiveBetweenValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerInclusiveBetweenValidator"/> class.
        /// </summary>
        public CustomerInclusiveBetweenValidator()
        {
            // 示例错误：“Id”必须介于1和10之间。您输入了0。
            /*
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {From} – Lower bound of the range
                {To} – Upper bound of the range
                {PropertyPath} - The full path of the property
             */
            this.RuleFor(x => x.Id).InclusiveBetween(1, 10);
        }
    }
}