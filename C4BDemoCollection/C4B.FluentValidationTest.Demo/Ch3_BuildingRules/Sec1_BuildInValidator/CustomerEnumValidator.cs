﻿// <copyright file="CustomerEnumValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using FluentValidation;

    /// <summary>
    /// CustomerEnumValidator.
    /// </summary>
    internal class CustomerEnumValidator : AbstractValidator<ErrorLevelModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerEnumValidator"/> class.
        /// </summary>
        public CustomerEnumValidator()
        {
            // 编译器将允许这样做，但是从技术上讲，值4对于该枚举无效。枚举验证器可以防止这种情况发生。
            ////ErrorLevelModel errorLevelModel = new ErrorLevelModel();
            ////errorLevelModel.ErrorLevel = (ErrorLevel)4;

            // 错误示例：“错误级别”的值范围不包括“4”。
            /*
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */
            this.RuleFor(x => x.ErrorLevel).IsInEnum();
        }
    }
}
