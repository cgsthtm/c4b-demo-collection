﻿// <copyright file="CustomerGreaterThanOrEqualValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerGreaterThanOrEqualValidator.
    /// </summary>
    internal class CustomerGreaterThanOrEqualValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerGreaterThanOrEqualValidator"/> class.
        /// </summary>
        public CustomerGreaterThanOrEqualValidator()
        {
            // 示例错误：“信用限额”必须大于或等于1。注意：仅对实现IComparable<T>的类型有效
            /*
                {PropertyName} – Name of the property being validated
                {ComparisonValue} – Value to which the property was compared
                {ComparisonProperty} – Name of the property being compared against (if any)
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */

            // Greater than a particular value
            this.RuleFor(customer => customer.CreditLimit).GreaterThanOrEqualTo(1);

            // Greater than another property
            this.RuleFor(customer => customer.CreditLimit).GreaterThanOrEqualTo(customer => customer.MinimumCreditLimit);
        }
    }
}
