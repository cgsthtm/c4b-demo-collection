﻿// <copyright file="CustomerEmailValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerEmailValidator.
    /// </summary>
    internal class CustomerEmailValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerEmailValidator"/> class.
        /// </summary>
        public CustomerEmailValidator()
        {
            // 示例错误：“电子邮件”不是有效的电子邮件地址。
            /*String format args:
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
            */
            this.RuleFor(customer => customer.Email).EmailAddress();
        }
    }
}
