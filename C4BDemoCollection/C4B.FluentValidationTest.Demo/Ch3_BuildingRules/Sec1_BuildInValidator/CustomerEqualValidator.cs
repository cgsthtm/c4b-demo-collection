﻿// <copyright file="CustomerEqualValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using System;
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerEqualValidator.
    /// </summary>
    internal class CustomerEqualValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerEqualValidator"/> class.
        /// </summary>
        public CustomerEqualValidator()
        {
            // 示例错误：'Surname'应该等于'Foo'
            /*String格式参数：
                {PropertyName} – Name of the property being validated
                {ComparisonValue} – Value that the property should equal
                {ComparisonProperty} – Name of the property being compared against (if any)
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */

            // Equal to a particular value
            this.RuleFor(customer => customer.Surname).Equal("Foo");

            // Equal to another property
            this.RuleFor(customer => customer.Password).Equal(customer => customer.PasswordConfirmation);

            // 可选地，可以提供比较器以确保执行特定类型的比较：
            this.RuleFor(customer => customer.Surname).Equal("Foo", StringComparer.OrdinalIgnoreCase);

            // 如果您使用的是FluentValidation 8.x（或更早版本），则可以使用
            this.RuleFor(customer => customer.Surname).Equal("Foo", StringComparer.Ordinal);
        }
    }
}
