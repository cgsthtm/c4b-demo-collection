﻿// <copyright file="ErrorLevel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    /// <summary>
    /// ErrorLevel.
    /// </summary>
    internal enum ErrorLevel
    {
        /// <summary>
        /// Error.
        /// </summary>
        Error = 1,

        /// <summary>
        /// Warning.
        /// </summary>
        Warning = 2,

        /// <summary>
        /// Notice.
        /// </summary>
        Notice = 3,
    }
}
