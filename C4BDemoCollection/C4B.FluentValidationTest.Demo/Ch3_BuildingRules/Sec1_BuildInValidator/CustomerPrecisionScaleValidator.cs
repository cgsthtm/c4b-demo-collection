﻿// <copyright file="CustomerPrecisionScaleValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerPrecisionScaleValidator.
    /// </summary>
    internal class CustomerPrecisionScaleValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerPrecisionScaleValidator"/> class.
        /// </summary>
        public CustomerPrecisionScaleValidator()
        {
            // 错误示例：“金额”总数不能超过4位，允许有2位小数。找到了5位数和3位小数。
            /*
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {ExpectedPrecision} – Expected precision
                {ExpectedScale} – Expected scale
                {Digits} – Total number of digits in the property value
                {ActualScale} – Actual scale of the property value
                {PropertyPath} - The full path of the property
             */
            this.RuleFor(x => x.Amount).PrecisionScale(4, 2, false);

            // 请注意，此方法的第三个参数是ignoreTrailingZeros。当设置为true时，小数点后的尾随零将不计入预期的小数位数。
            /*例如：
                当ignoreTrailingZeros为false时，小数123.4500将被视为精度为7，小数位数为4
                当ignoreTrailingZeros为true时，小数123.4500将被视为具有5的精度和2的小数位数。
             */
        }
    }
}