﻿// <copyright file="CustomerMaxLengthValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerMaxLengthValidator.
    /// </summary>
    internal class CustomerMaxLengthValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerMaxLengthValidator"/> class.
        /// </summary>
        public CustomerMaxLengthValidator()
        {
            // 错误示例：“姓氏”的长度必须小于或等于250个字符。您输入了251个字符。
            // 注意：仅对字符串属性有效。
            /*String format args:
                {PropertyName} – Name of the property being validated
                {MaxLength} – Maximum length
                {TotalLength} – Number of characters entered
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */
            this.RuleFor(customer => customer.Surname).MaximumLength(250); // must be 250 chars or fewer
        }
    }
}
