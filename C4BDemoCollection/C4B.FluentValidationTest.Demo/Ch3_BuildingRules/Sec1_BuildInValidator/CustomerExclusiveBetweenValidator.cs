﻿// <copyright file="CustomerExclusiveBetweenValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerExclusiveBetweenValidator.
    /// </summary>
    internal class CustomerExclusiveBetweenValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerExclusiveBetweenValidator"/> class.
        /// </summary>
        public CustomerExclusiveBetweenValidator()
        {
            // 示例错误：“Id”必须介于1和10之间（不含）。您输入了1。
            /*
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {From} – Lower bound of the range
                {To} – Upper bound of the range
                {PropertyPath} - The full path of the property
             */
            this.RuleFor(x => x.Id).ExclusiveBetween(1, 10);
        }
    }
}
