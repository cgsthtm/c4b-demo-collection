﻿// <copyright file="CustomerNotEqualValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using System;
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerNotEqualValidator.
    /// </summary>
    internal class CustomerNotEqualValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerNotEqualValidator"/> class.
        /// </summary>
        public CustomerNotEqualValidator()
        {
            // 示例错误：“姓氏”不应等于“Foo”
            /*String format args:
                {PropertyName} – Name of the property being validated
                {ComparisonValue} – Value that the property should not equal
                {ComparisonProperty} – Name of the property being compared against (if any)
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */

            // Not equal to a particular value
            this.RuleFor(customer => customer.Surname).NotEqual("Foo");

            // Not equal to another property
            this.RuleFor(customer => customer.Surname).NotEqual(customer => customer.Forename);

            // 可选地，可以提供比较器以确保执行特定类型的比较：
            this.RuleFor(customer => customer.Surname).NotEqual("Foo", StringComparer.OrdinalIgnoreCase);

            // 如果您使用的是FluentValidation 8.x（或更早版本），则可以使用
            this.RuleFor(customer => customer.Surname).Equal("Foo", StringComparer.Ordinal);
        }
    }
}
