﻿// <copyright file="ErrorLevelModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    /// <summary>
    /// ErrorLevelModel.
    /// </summary>
    internal class ErrorLevelModel
    {
        /// <summary>
        /// Gets or sets ErrorLevel.
        /// </summary>
        public ErrorLevel ErrorLevel { get; set; }

        /// <summary>
        /// Gets or sets ErrorLevelName.
        /// </summary>
        public string ErrorLevelName { get; set; }
    }
}
