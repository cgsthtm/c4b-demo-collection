﻿// <copyright file="CustomerEnumNameValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using FluentValidation;

    /// <summary>
    /// CustomerEnumNameValidator.
    /// </summary>
    internal class CustomerEnumNameValidator : AbstractValidator<ErrorLevelModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerEnumNameValidator"/> class.
        /// </summary>
        public CustomerEnumNameValidator()
        {
            // 示例错误：“错误级别”的值范围不包括“Foo”。
            /*
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */

            // For a case sensitive comparison
            this.RuleFor(x => x.ErrorLevelName).IsEnumName(typeof(ErrorLevel));

            // For a case-insensitive comparison
            this.RuleFor(x => x.ErrorLevelName).IsEnumName(typeof(ErrorLevel), caseSensitive: false);
        }
    }
}
