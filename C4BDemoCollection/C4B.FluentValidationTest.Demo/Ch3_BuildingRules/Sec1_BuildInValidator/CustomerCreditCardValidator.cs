﻿// <copyright file="CustomerCreditCardValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec1_BuildInValidator
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerCreditCardValidator.
    /// </summary>
    internal class CustomerCreditCardValidator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerCreditCardValidator"/> class.
        /// </summary>
        public CustomerCreditCardValidator()
        {
            // 示例错误：“Credit Card”不是有效的信用卡号。
            /*
                {PropertyName} – Name of the property being validated
                {PropertyValue} – Current value of the property
                {PropertyPath} - The full path of the property
             */
            this.RuleFor(x => x.CreditCard).CreditCard();
        }
    }
}