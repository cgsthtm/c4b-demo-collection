﻿// <copyright file="CustomWritingACustomeValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec2_CustomeValidator
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;
    using FluentValidation.Results;

    /// <summary>
    /// CustomWritingACustomeValidator.
    /// </summary>
    internal class CustomWritingACustomeValidator : AbstractValidator<Person>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomWritingACustomeValidator"/> class.
        /// </summary>
        public CustomWritingACustomeValidator()
        {
            /*如果您需要对验证过程进行比Must更多的控制，则可以使用Custom方法编写自定义规则。
             * 此方法允许您手动创建与验证错误关联的ValidationFailure实例。通常，框架会为您做这件事，所以它比使用Must更冗长。
             */
            this.RuleFor(x => x.Pets).Custom((list, context) =>
            {
                if (list.Count > 10)
                {
                    context.AddFailure("The list must contain 10 items or fewer");
                }
            });

            // 这种方法的优点是它允许您为同一规则返回多个错误（通过多次调用context.AddFailure方法）。
            // 在上面的示例中，生成的错误中的属性名称将被推断为“Pets”，尽管这可以通过调用AddFailure的不同重载来覆盖：
            this.RuleFor(x => x.Pets).Custom((list, context) =>
            {
                if (list.Count > 10)
                {
                    context.AddFailure("SomeOtherProperty", "The list must contain 10 items or fewer");

                    // Or you can instantiate the ValidationFailure directly:
                    context.AddFailure(new ValidationFailure("SomeOtherProperty", "The list must contain 10 items or fewer"));
                }
            });

            // 和以前一样，这可以被包装在一个扩展方法中，以简化使用代码。
            /*
            public static IRuleBuilderOptionsConditions<T, IList<TElement>> ListMustContainFewerThan<T, TElement>(this IRuleBuilder<T, IList<TElement>> ruleBuilder, int num) {
              return ruleBuilder.Custom((list, context) => {
                 if(list.Count > 10) {
                   context.AddFailure("The list must contain 10 items or fewer");
                 }
               });
            }
             */
        }
    }
}
