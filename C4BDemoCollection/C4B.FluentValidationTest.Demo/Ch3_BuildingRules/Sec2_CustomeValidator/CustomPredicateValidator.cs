﻿// <copyright file="CustomPredicateValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec2_CustomeValidator
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// CustomPredicateValidator.
    /// </summary>
    internal class CustomPredicateValidator : AbstractValidator<Person>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomPredicateValidator"/> class.
        /// </summary>
        public CustomPredicateValidator()
        {
            // 有几种方法可以创建自定义的、可重用的验证器。
            // 推荐的方法是使用Predicate Validator编写自定义验证函数，但也可以使用Custom方法完全控制验证过程。
            // 实现自定义验证器的最简单方法是使用Must方法，该方法在内部使用PredicateValidator。
            this.RuleFor(x => x.Pets).Must(list => list.Count < 10)
                .WithMessage("The list must contain fewer than 10 items");

            // 为了使此逻辑可重用，我们可以将其包装为作用于任何List<T>类型的扩展方法。
            /*
            public static class MyCustomValidators {
              public static IRuleBuilderOptions<T, IList<TElement>> ListMustContainFewerThan<T, TElement>(this IRuleBuilder<T, IList<TElement>> ruleBuilder, int num) {
	            return ruleBuilder.Must(list => list.Count < num).WithMessage("The list contains too many items");
              }
            }
             */

            // 在这里，我们在IRuleBuilder<T，TProperty>上创建一个扩展方法，并使用一个泛型类型约束来确保此方法仅在List类型的智能感知中出现。
            // 在方法内部，我们以与前面相同的方式调用Must方法，但这次我们在传入的RuleBuilder实例上调用它。我们还将用于比较的项数作为参数传入。
            // 我们的规则定义现在可以重写为使用此方法：
            this.RuleFor(x => x.Pets).ListMustContainFewerThan(10);
        }
    }
}
