﻿// <copyright file="CustomMessagePlaceholders.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec2_CustomeValidator
{
    using System.Collections.Generic;
    using FluentValidation;

    /// <summary>
    /// CustomMessagePlaceholders.
    /// </summary>
    public static class CustomMessagePlaceholders
    {
        /// <summary>
        /// ListMustContainFewerThan.
        /// </summary>
        /// <typeparam name="T">需要验证规则的字段的类型.</typeparam>
        /// <typeparam name="TElement">泛型集合的类型.</typeparam>
        /// <param name="ruleBuilder">需要扩展的ruleBuilder.</param>
        /// <param name="num">FewerThan的值.</param>
        /// <returns>IRuleBuilderOptions.</returns>
        public static IRuleBuilderOptions<T, IList<TElement>> ListMustContainFewerThan<T, TElement>(this IRuleBuilder<T, IList<TElement>> ruleBuilder, int num)
        {
            // 默认情况下，FluentValidation支持几个消息占位符，包括{PropertyName}和{PropertyValue}（
            // 更多信息请参见此列表 https://docs.fluentvalidation.net/en/latest/built-in-validators.html），但我们也可以添加自己的占位符。

            // 我们需要稍微修改我们的扩展方法，以使用Must方法的不同重载，该重载接受ValidationContext<T>实例。
            // 此上下文提供了我们在执行验证时可以使用的其他信息和方法：
            return ruleBuilder.Must((rootObject, list, context) =>
            {
                context.MessageFormatter.AppendArgument("MaxElements", num);
                return list.Count < num;
            })
            .WithMessage("{PropertyName} must contain fewer than {MaxElements} items.");

            // 请注意，我们使用的Must重载现在接受3个参数：根（父）对象、属性值本身和上下文。我们使用上下文添加MaxElements的自定义消息替换值，
            // 并将其值设置为传递给方法的数字。我们现在可以在WithMessage调用中使用这个占位符作为{MaxElements}。

            // 现在得到的消息将是 'Pets' must contain fewer than 10 items. 我们甚至可以进一步扩展它，以包括列表包含的元素数量，如下所示：
            return ruleBuilder.Must((rootObject, list, context) =>
            {
                context.MessageFormatter
                  .AppendArgument("MaxElements", num)
                  .AppendArgument("TotalElements", list.Count);

                return list.Count < num;
            })
            .WithMessage("{PropertyName} must contain fewer than {MaxElements} items. The list contains {TotalElements} element");
        }
    }
}