﻿// <copyright file="CustomReusablePropertyValidators.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec2_CustomeValidator
{
    using System.Collections.Generic;
    using FluentValidation;
    using FluentValidation.Validators;

    // 在某些情况下，如果您的自定义逻辑非常复杂，您可能希望将自定义逻辑移动到单独的类中。
    // 这可以通过编写一个从抽象类 PropertyValidator<T,TProperty> 继承的类来完成（这是FluentValidation所有内置规则的定义方式）。
    // 这是一种高级技术，通常是不必要的-上面解释的Must和Custom方法通常更合适。

    /// <summary>
    /// CustomReusablePropertyValidators.
    /// </summary>
    /// <typeparam name="T">T.</typeparam>
    /// <typeparam name="TCollectionElement">TCollectionElement.</typeparam>
    internal class CustomReusablePropertyValidators<T, TCollectionElement> : PropertyValidator<T, IList<TCollectionElement>>
    {
        private int max;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomReusablePropertyValidators{T, TCollectionElement}"/> class.
        /// </summary>
        /// <param name="max">max.</param>
        public CustomReusablePropertyValidators(int max)
        {
            this.max = max;
        }

        /// <inheritdoc/>
        public override string Name => "CustomReusablePropertyValidators";

        /// <inheritdoc/>
        public override bool IsValid(ValidationContext<T> context, IList<TCollectionElement> list)
        {
            if (list != null && list.Count >= this.max)
            {
                context.MessageFormatter.AppendArgument("MaxElements", this.max);
                return false;
            }

            return true;
        }

        /// <inheritdoc/>
        protected override string GetDefaultMessageTemplate(string errorCode)
            => "{PropertyName} must contain fewer than {MaxElements} items.";

        // 从PropertyValidator继承时，必须重写IsValid方法。此方法接收两个值-表示当前验证运行的ValidationContext<T>和属性的值。
        // 该方法应返回一个布尔值，指示验证是否成功。基类上的通用类型参数表示正在验证的根实例，以及我们的自定义验证器可以作用的属性的类型。
        // 在本例中，我们将自定义验证器约束为实现IList<TCollectionElement>的类型，但如果需要，可以将其保留为打开状态。

        // 请注意，要使用的错误消息是通过重写GetDefaultMessageTemplate指定的。

        // 要使用新的自定义验证器，您可以在定义验证规则时调用SetValidator。
        /*
        public class PersonValidator : AbstractValidator<Person> {
            public PersonValidator() {
               RuleFor(person => person.Pets).SetValidator(new CustomReusablePropertyValidators<Person, Pet>(10));
            }
        }
         */

        // 与第一个示例一样，您可以将其包装在扩展方法中以使语法更好：
        /*
        public static class MyValidatorExtensions {
           public static IRuleBuilderOptions<T, IList<TElement>> ListMustContainFewerThan<T, TElement>(this IRuleBuilder<T, IList<TElement>> ruleBuilder, int num) {
              return ruleBuilder.SetValidator(new CustomReusablePropertyValidators<T, TElement>(num));
           }
        }
         */

        // .然后可以像任何其他验证器一样链接：
        /*
        public class PersonValidator : AbstractValidator<Person> {
            public PersonValidator() {
               RuleFor(person => person.Pets).ListMustContainFewerThan(10);
            }
        }
         */

        // 作为另一个更简单的例子，这是FluentValidation自己的Nottvalidator的实现方式：
        /*
        public class NotNullValidator<T,TProperty> : PropertyValidator<T,TProperty> {

          public override string Name => "NotNullValidator";

          public override bool IsValid(ValidationContext<T> context, TProperty value) {
            return value != null;
          }

          protected override string GetDefaultMessageTemplate(string errorCode)
            => "'{PropertyName}' must not be empty.";
        }
         */
    }
}
