﻿// <copyright file="CustomMyCustomValidators.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch3_BuildingRules.Sec2_CustomeValidator
{
    using System.Collections.Generic;
    using FluentValidation;

    /// <summary>
    /// CustomMyCustomValidators.
    /// </summary>
    public static class CustomMyCustomValidators
    {
        /// <summary>
        /// ListMustContainFewerThan.
        /// </summary>
        /// <typeparam name="T">需要验证规则的字段的类型.</typeparam>
        /// <typeparam name="TElement">泛型集合的类型.</typeparam>
        /// <param name="ruleBuilder">需要扩展的ruleBuilder.</param>
        /// <param name="num">FewerThan的值.</param>
        /// <returns>IRuleBuilderOptions.</returns>
        public static IRuleBuilderOptions<T, IList<TElement>> ListMustContainFewerThan1<T, TElement>(this IRuleBuilder<T, IList<TElement>> ruleBuilder, int num)
        {
            return ruleBuilder.Must(list => list.Count < num).WithMessage("The list contains too many items");
        }
    }
}
