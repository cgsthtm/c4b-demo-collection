﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo
{
    using System;
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using C4B.FluentValidationTest.Demo.Ch4_OtherFeatures;
    using C4B.FluentValidationTest.Demo.Ch5_Localization;
    using C4B.FluentValidationTest.Demo.Ch6_Testing;
    using C4B.FluentValidationTest.Demo.Ch7_Advanced;

    /// <summary>
    /// Program.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Ch1_GettingStarted
            ////CustomerValidatorTest.Test_CreatingYourFirstValidator();
            ////CustomerValidatorTest.Test_ThrowingExceptions();
            ////CustomerValidatorTest.Test_ComplexProperties();
            ////PersonValidatorTest.Test_Collections_Of_Simple_Types();
            ////CustomerValidatorTest.Test_CollectionsOfComplexTypes();

            // Ch2_ConfiguringValidators
            ////Customer3ValidatorTest.Test_OverridingTheMessage();
            ////Customer3ValidatorTest.Test_Placeholders();
            ////Customer3ValidatorTest.Test_OverridingThePropertyName();
            ////Customer3ValidatorTest.Test_Conditions();

            // Ch4_OtherFeatures
            ////DependencyInjectionTest.Test();
            ////AsynchronousValidationTest.Test();
            ////SettingTheSeverityLevelTest.Test();
            ////CustomErrorCodesTest.Test();
            ////CustomStateTest.Test();

            // Ch5_Localization
            ////LocalizationTest.Test_DefaultMessage();
            ////LocalizationTest.Test_DisablingLocalization();

            // Ch6_Testing
            ////TestExtensionsTest.Should_have_error_when_Name_is_null();

            // Ch7_Advanced
            ////InheritanceValidationTest.Test();
            ////OtherAdvancedFeaturesTest.Test_PreValidate();
            ////OtherAdvancedFeaturesTest.Test_RootContextData();
            OtherAdvancedFeaturesTest.Test_CustomizingTheValidationException();

            Console.ReadLine();
        }
    }
}