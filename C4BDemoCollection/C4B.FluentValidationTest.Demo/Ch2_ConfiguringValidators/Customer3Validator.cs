﻿// <copyright file="Customer3Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators
{
    using FluentValidation;

    /// <summary>
    /// Customer3Validator.
    /// </summary>
    internal class Customer3Validator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer3Validator"/> class.
        /// </summary>
        public Customer3Validator()
        {
            // 您可以通过调用验证器定义上的WithMessage方法来覆盖验证器的默认错误消息：
            this.RuleFor(c => c.Surname).NotNull().WithMessage("Please ensure that you have entered your Surname");

            // 请注意，自定义错误消息可以包含特殊值（如{PropertyName}）的占位符-在此示例中，
            // 将使用正在验证的属性的名称替换该占位符。这意味着上述错误消息可以重写为：
            this.RuleFor(customer => customer.Surname).NotNull().WithMessage("Please ensure you have entered your {PropertyName}");
        }
    }
}
