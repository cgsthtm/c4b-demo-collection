﻿// <copyright file="Customer4Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators
{
    using FluentValidation;

    /// <summary>
    /// Customer4Validator.
    /// </summary>
    internal class Customer4Validator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer4Validator"/> class.
        /// </summary>
        public Customer4Validator()
        {
            /*消息可以包含特殊值的占位符，例如{PropertyName}，这些值将在运行时被替换。每个内置验证器都有自己的占位符列表。
             * 所有验证器中使用的占位符是：
             * - {PropertyName} 正在验证的属性的名称
             * - {PropertyValue} 正在验证的属性的值这些包括谓词验证器（必须验证器），电子邮件和正则表达式验证器。
             * 用于比较验证器：（Equal、NotEqual、GreaterThan、GreaterThanOrEqual等）
             * - {ComparisonValue} 属性应与之进行比较的值
             * - {ComparisonProperty} 要比较的属性的名称（如果有）
             * 仅在长度验证器中使用：
             * - {MinLength} 最小长度
             * - {MaxLength} 最大长度
             * - {TotalLength} 输入的字符数
             * 有关错误消息占位符的完整列表，请参阅内置验证程序页面(https://docs.fluentvalidation.net/en/latest/built-in-validators.html)。
             * 每个内置的验证器都有自己支持的占位符。
             * 
             */

            // 也可以在验证消息中使用您自己的自定义参数。这些值可以是静态值，也可以是对正在验证的对象上的其他属性的引用。
            // 这可以通过使用带有lambda表达式的WithMessage重载，然后将值传递给string.Format或使用字符串插值来完成。
            // Using constant in a custom message:
            this.RuleFor(customer => customer.Surname)
              .NotNull()
              .WithMessage(customer => string.Format("This message references some constant values: {0} {1}", "hello", 5));

            // Result would be "This message references some constant values: hello 5"

            // Referencing other property values:
            this.RuleFor(customer => customer.Surname)
              .NotNull()
              .WithMessage(customer => $"This message references some other properties: Forename: {customer.Forename} Discount: {customer.Discount}");

            // Result would be: "This message references some other properties: Forename: Jeremy Discount: 100"

            // If you want to override all of FluentValidation’s default error messages, check out FluentValidation’s support for Localization
            // https://docs.fluentvalidation.net/en/latest/localization.html
        }

    }
}
