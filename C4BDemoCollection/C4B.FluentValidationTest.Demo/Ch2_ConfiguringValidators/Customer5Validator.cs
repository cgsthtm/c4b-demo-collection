﻿// <copyright file="Customer5Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators
{
    using FluentValidation;

    /// <summary>
    /// Customer5Validator.
    /// </summary>
    internal class Customer5Validator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer5Validator"/> class.
        /// </summary>
        public Customer5Validator()
        {
            // 默认验证错误消息包含正在验证的属性名。例如，如果你要像这样定义一个验证器：
            ////this.RuleFor(customer => customer.Surname).NotNull();

            // 那么默认的错误消息将是“Surname”不能为空。虽然可以通过调用WithMessage覆盖整个错误消息，但也可以通过调用WithName仅替换属性名称：
            this.RuleFor(customer => customer.Surname).NotNull().WithName("Last name");

            // 现在错误消息将是“Last name”不能为空。
            // 请注意，这只会替换错误消息中的属性名称。当您检查ValidationResult上的Errors集合时，此错误仍将与名为Surname的属性关联。
            // 如果要完全重命名属性，则可以改用OverridePropertyName方法。

            // 还有一个WithName的重载，它以与上一节中的WithMessage类似的方式接受lambda表达式：
            this.RuleFor(customer => customer.Surname).NotNull().WithName(customer => "Last name for customer " + customer.Id);

            // 属性名解析也是可插入的。默认情况下，从传递给RuleFor的MemberExpression中提取的属性的名称。如果要更改此逻辑，
            // 可以在ValidatorOptions类上设置DisplayNameResolver属性：
            ValidatorOptions.Global.DisplayNameResolver = (type, member, expression) =>
            {
                if (member != null)
                {
                    return member.Name + "Foo";
                }

                return null;
            };
        }
    }
}
