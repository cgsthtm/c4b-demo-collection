﻿// <copyright file="Customer3ValidatorTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators
{
    using System;

    /// <summary>
    /// Customer3ValidatorTest.
    /// </summary>
    internal class Customer3ValidatorTest
    {
        /// <summary>
        /// Test_OverridingTheMessage.
        /// </summary>
        public static void Test_OverridingTheMessage()
        {
            var customer3 = new Customer3();
            var validator3 = new Customer3Validator();
            var result = validator3.Validate(customer3);
            Console.WriteLine(result.ToString("~"));
        }

        /// <summary>
        /// Test_Placeholders.
        /// </summary>
        public static void Test_Placeholders()
        {
            var customer3 = new Customer3();
            var validator4 = new Customer4Validator();
            var result = validator4.Validate(customer3);
            Console.WriteLine(result.ToString("~"));
        }

        /// <summary>
        /// Test_OverridingThePropertyName.
        /// </summary>
        public static void Test_OverridingThePropertyName()
        {
            var customer3 = new Customer3();
            var validator5 = new Customer5Validator();
            var result = validator5.Validate(customer3);
            Console.WriteLine(result.ToString("~"));
        }

        /// <summary>
        /// Test_Conditions.
        /// </summary>
        public static void Test_Conditions()
        {
            var customer3 = new Customer3();
            var validator6 = new Customer6Validator();
            var result = validator6.Validate(customer3);
            Console.WriteLine(result.ToString("~"));
        }
    }
}
