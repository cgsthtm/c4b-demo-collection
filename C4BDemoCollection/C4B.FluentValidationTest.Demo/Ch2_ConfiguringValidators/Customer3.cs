﻿// <copyright file="Customer3.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators
{
    using System.Collections.Generic;
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;

    /// <summary>
    /// Customer3.
    /// </summary>
    internal class Customer3
    {
        /// <summary>
        /// Gets or sets Surname.
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Gets or sets Forename.
        /// </summary>
        public string Forename { get; set; }

        /// <summary>
        /// Gets or sets Discount.
        /// </summary>
        public double Discount { get; set; }

        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets CustomerDiscount.
        /// </summary>
        public double CustomerDiscount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsPreferredCustomer.
        /// </summary>
        public bool IsPreferredCustomer { get; set; }

        /// <summary>
        /// Gets or sets CreditCardNumber.
        /// </summary>
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// Gets or sets Photo.
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// Gets or sets Password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets PasswordConfirmation.
        /// </summary>
        public string PasswordConfirmation { get; set; }

        /// <summary>
        /// Gets or sets CreditLimit.
        /// </summary>
        public int CreditLimit { get; set; }

        /// <summary>
        /// Gets or sets MaxCreditLimit.
        /// </summary>
        public int MaxCreditLimit { get; set; }

        /// <summary>
        /// Gets or sets MinimumCreditLimit.
        /// </summary>
        public int MinimumCreditLimit { get; set; }

        /// <summary>
        /// Gets or sets Email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets CreditCard.
        /// </summary>
        public string CreditCard { get; set; }

        /// <summary>
        /// Gets or sets Amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets Orders.
        /// </summary>
        public List<Order2> Orders { get; set; } = new List<Order2>();
    }
}
