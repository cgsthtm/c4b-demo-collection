﻿// <copyright file="Customer6Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators
{
    using FluentValidation;

    /// <summary>
    /// Customer6Validator.
    /// </summary>
    internal class Customer6Validator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer6Validator"/> class.
        /// </summary>
        public Customer6Validator()
        {
            // When和Unless方法可用于指定控制规则执行时间的条件。例如，CustomerDiscount属性上的此规则仅在IsPreferredCustomer为true时执行：
            this.RuleFor(customer => customer.CustomerDiscount).GreaterThan(0).When(customer => customer.IsPreferredCustomer);

            // Unless方法与When相反。

            // 如果需要为多个规则指定相同的条件，则可以调用顶级When方法，而不是在规则的末尾链接When调用：
            this.When(customer => customer.IsPreferredCustomer, () =>
            {
                this.RuleFor(customer => customer.CustomerDiscount).GreaterThan(0);
                this.RuleFor(customer => customer.CreditCardNumber).NotNull();
            });

            // 这一次，该条件将应用于两个规则。你也可以链接一个调用Otherwise，这将调用不匹配条件的规则：
            this.When(customer => customer.IsPreferredCustomer, () =>
            {
                this.RuleFor(customer => customer.CustomerDiscount).GreaterThan(0);
                this.RuleFor(customer => customer.CreditCardNumber).NotNull();
            }).Otherwise(() =>
            {
                this.RuleFor(customer => customer.CustomerDiscount).Equal(0);
            });

            // 默认情况下，FluentValidation会将条件应用于同一个RuleFor调用中的所有前面的验证器。
            // 如果您只希望条件应用于紧接在该条件之前的验证器，则必须显式指定：
            this.RuleFor(customer => customer.CustomerDiscount)
                .GreaterThan(0).When(customer => customer.IsPreferredCustomer, ApplyConditionTo.CurrentValidator)
                .Equal(0).When(customer => !customer.IsPreferredCustomer, ApplyConditionTo.CurrentValidator);

            // 如果没有指定第二个参数，那么它默认为 ApplyConditionTo.AllValidators ，这意味着该条件将应用于同一链中所有前面的验证器。

            // 如果你需要这种行为，请注意，你必须指定 ApplyConditionTo.CurrentValidator 作为每个条件的一部分。在下面的示例中，
            // 对When的第一个调用仅应用于对Matches的调用，而不应用于对NotEmpty的调用。第二个对When的调用只应用于对Empty的调用。
            this.RuleFor(customer => customer.Photo)
                .NotEmpty()
                .Matches("https://wwww.photos.io/\\d+\\.png")
                .When(customer => customer.IsPreferredCustomer, ApplyConditionTo.CurrentValidator)
                .Empty()
                .When(customer => !customer.IsPreferredCustomer, ApplyConditionTo.CurrentValidator);
        }
    }
}
