﻿// <copyright file="PersonNameValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// PersonNameValidator.
    /// </summary>
    internal class PersonNameValidator : AbstractValidator<Person>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonNameValidator"/> class.
        /// </summary>
        public PersonNameValidator()
        {
            this.RuleFor(x => x.Surname).NotNull().Length(0, 255);
            this.RuleFor(x => x.Forename).NotNull().Length(0, 255);
        }
    }
}