﻿// <copyright file="Person11Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// Person11Validator.
    /// </summary>
    internal class Person11Validator : AbstractValidator<Person>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Person11Validator"/> class.
        /// </summary>
        public Person11Validator()
        {
            // 自定义错误代码也可以通过调用WithErrorCode方法与验证规则关联：
            this.RuleFor(person => person.Surname).NotNull().WithErrorCode("ERR1234");
            this.RuleFor(person => person.Forename).NotNull();

            // 结果错误代码可以从ValidationFailure的ErrorCode属性中获得：
            ////var validator = new PersonValidator();
            ////var result = validator.Validate(new Person());
            ////foreach (var failure in result.Errors)
            ////{
            ////    Console.WriteLine($"Property: {failure.PropertyName} Error Code: {failure.ErrorCode}");
            ////}

            /*The output would be:
                Property: Surname Error Code: ERR1234
                Property: Forename Error Code: NotNullValidator
             */
        }
    }
}
