﻿// <copyright file="CustomErrorCodesTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using System;

    /// <summary>
    /// CustomErrorCodesTest.
    /// </summary>
    internal class CustomErrorCodesTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            var validator = new Person11Validator();
            var results = validator.Validate(new Ch1_GettingStarted.Person());
            foreach (var failure in results.Errors)
            {
                Console.WriteLine($"Property: {failure.PropertyName} Error Code: {failure.ErrorCode}");
            }

            /*ErrorCode还用于确定特定验证器的默认错误消息。在高级别：
             * - 错误代码用作错误消息的查找键。例如，NotValidator验证器有一个默认的错误代码NotNullValidator，
             *   它用于查找来自LanguageManager的错误消息。请参阅本地化文档。https://docs.fluentvalidation.net/en/latest/localization.html
             * - 如果提供错误代码，还可以提供包含该错误代码名称的本地化消息，以创建自定义消息。
             * - 如果您提供了错误代码但没有自定义消息，则消息将回退到该验证器的默认消息。您不需要添加自定义消息。
             * - 使用ErrorCode也可用于覆盖默认错误消息。例如，如果您使用自定义的Must验证器，但您希望重用NotNull验证器的默认错误消息，
             *   则可以调用 WithErrorCode("NotNullValidator") 来实现此结果。
             */
        }
    }
}
