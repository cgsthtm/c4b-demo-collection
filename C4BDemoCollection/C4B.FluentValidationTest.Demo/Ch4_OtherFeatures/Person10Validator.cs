﻿// <copyright file="Person10Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// Person10Validator.
    /// </summary>
    internal class Person10Validator : AbstractValidator<Person>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Person10Validator"/> class.
        /// </summary>
        public Person10Validator()
        {
            this.RuleFor(person => person.Surname).NotNull();
            this.RuleFor(person => person.Forename).NotNull();

            // 默认情况下，如果这些规则失败，它们的严重性将为Error。这可以通过调用WithSeverity方法来更改。
            // 例如，如果我们希望将缺少的姓氏标识为警告而不是错误，那么我们可以将上面的行修改为：
            this.RuleFor(x => x.Surname).NotNull().WithSeverity(Severity.Warning);

            // 在9.0及更高版本中，可以使用回调，这也使您可以访问正在验证的项目：
            this.RuleFor(person => person.Surname).NotNull().WithSeverity(person => Severity.Warning);

            // 在这种情况下，ValidationResult仍然会有一个IsValid结果false。
            // 但是，在错误列表中，与此字段关联的ValidationFailure的Severity属性将设置为Warning：
            ////var validator = new Person10Validator();
            ////var result = validator.Validate(new Person());
            ////foreach (var failure in result.Errors)
            ////{
            ////    Console.WriteLine($"Property: {failure.PropertyName} Severity: {failure.Severity}");
            ////}

            // The output would be:
            // Property: Surname Severity: Warning
            // Property: Forename Severity: Error

            // 默认情况下，每个验证规则的严重级别都是“Error”。可用选项包括Error、Warning或Info。

            // 要全局设置严重级别，可以在应用程序的启动例程期间设置静态ValidatorOptions类的Severity属性：
            ValidatorOptions.Global.Severity = Severity.Info;
        }
    }
}
