﻿// <copyright file="Customer7Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// CustomerValidator.
    /// </summary>
    internal class Customer7Validator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer7Validator"/> class.
        /// </summary>
        public Customer7Validator()
        {
            this.RuleFor(x => x.Surname).NotNull();
            this.RuleFor(x => x.Forename).NotNull();
            this.RuleForEach(x => x.Orders).SetValidator(new Order7Validator());
        }
    }
}
