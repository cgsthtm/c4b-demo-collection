﻿// <copyright file="Order7Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// Order7Validator.
    /// </summary>
    internal class Order7Validator : AbstractValidator<Order2>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Order7Validator"/> class.
        /// </summary>
        public Order7Validator()
        {
            this.RuleFor(order => order.Total).NotNull().NotEmpty();
            this.RuleFor(order => order.Cost).NotNull().NotEmpty();
        }
    }
}
