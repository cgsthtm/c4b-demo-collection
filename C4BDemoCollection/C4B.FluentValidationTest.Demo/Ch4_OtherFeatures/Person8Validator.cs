﻿// <copyright file="Person8Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// Person8Validator.
    /// </summary>
    internal class Person8Validator : AbstractValidator<Person>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Person8Validator"/> class.
        /// </summary>
        public Person8Validator()
        {
            // 假设你有两个验证器定义为一个规则定义的一部分，一个NotEqual验证器和一个NotEqual验证器：
            this.RuleFor(x => x.Surname).NotNull().NotEqual("foo");

            // 这会首先检查Surname是否为Null，然后检查它是否不等于foo，如果NotNull检查失败，那么NotEqual仍然执行调用。
            // 然而这能够通过指定级联模式的Stop来改变此规则。
            this.RuleFor(x => x.Surname).Cascade(CascadeMode.Stop).NotNull().NotEqual("foo");

            // 现在，如果NotNull验证器失败，那么NotEqual验证器将不会被执行。
            // 如果你有一个复杂的链，其中每个验证器都依赖于前一个验证器来成功，这是特别有用的。

            /*两种级联模式为：
             * Continue（默认值）-总是调用验证器类中的所有规则，或者调用规则中的所有验证器，这取决于它被使用的位置（见下文）。
             * Stop-一旦规则失败，就停止执行验证器类，或者一旦验证器失败，就停止执行规则，这取决于它在哪里使用（见下文）。
             */

            // 停止选项仅在FluentValidation 9.1及更新版本中可用。在旧版本中，您可以使用StopOnFirstFailure代替（请参阅“Stop vs StopOnFirstFailure”）。

            // 如果你有一个包含多个规则的验证器类，并且希望为所有规则设置此Stop行为，你可以这样做，例如：
            this.RuleFor(x => x.Forename).Cascade(CascadeMode.Stop).NotNull().NotEqual("foo");
            this.RuleFor(x => x.MiddleNames).Cascade(CascadeMode.Stop).NotNull().NotEqual("foo");
            this.RuleFor(x => x.Surname).Cascade(CascadeMode.Stop).NotNull().NotEqual("foo");

            // 若要避免重复Cascade（CascadeMode.Stop），可以通过设置 AbstractValidator.RuleLevelCascadeMode 属性来设置规则级级联模式的默认值，结果是
            this.RuleLevelCascadeMode = CascadeMode.Stop;
            this.RuleFor(x => x.Forename).NotNull().NotEqual("foo");
            this.RuleFor(x => x.MiddleNames).NotNull().NotEqual("foo");
            this.RuleFor(x => x.Surname).NotNull().NotEqual("foo");

            // 使用默认的全局设置，这段代码将停止执行NotNull调用失败的任何规则，也不调用NotEqual，
            // 但它将继续执行下一个规则，并始终执行所有三个规则，而不管失败与否。
        }
    }
}
