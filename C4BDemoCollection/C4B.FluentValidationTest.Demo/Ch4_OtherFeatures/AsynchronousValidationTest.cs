﻿// <copyright file="AsynchronousValidationTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using System;
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;

    /// <summary>
    /// AsynchronousValidationTest.
    /// </summary>
    internal class AsynchronousValidationTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static async void Test()
        {
            var customer = new Customer3();
            var validator = new Customer8Validator();
            var result = await validator.ValidateAsync(customer);
            Console.WriteLine(result.ToString("~"));
        }
    }
}
