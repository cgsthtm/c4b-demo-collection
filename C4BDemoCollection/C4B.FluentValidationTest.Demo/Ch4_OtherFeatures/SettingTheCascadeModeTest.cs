﻿// <copyright file="SettingTheCascadeModeTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;

    /// <summary>
    /// SettingTheCascadeModeTest.
    /// </summary>
    internal static class SettingTheCascadeModeTest
    {
        /// <summary>
        /// Test_RuleLevelCascadeModes.
        /// </summary>
        public static void Test_RuleLevelCascadeModes()
        {
            var person = new Person();
            var validator = new Person8Validator();
            validator.Validate(person);
        }

        /// <summary>
        /// Test_ValidatorClassLevelCascadeModes.
        /// </summary>
        public static void Test_ValidatorClassLevelCascadeModes()
        {
            // 除了在规则级别设置之外，还可以使用属性 AbstractValidator.ClassLevelCascadeMode 在验证器类级别设置级联模式。
            // 这控制了验证器内规则之间的级联行为，但不影响上述规则级级联行为。

            // 例如，上面的代码将执行所有三个规则，即使其中任何一个失败。要在任何规则失败时完全停止验证器类的执行，
            // 您可以将 AbstractValidator.ClassLevelCascadeMode 设置为Stop。这将导致完全的“fail fast”行为，并且返回最多只返回一个错误。
        }

        /// <summary>
        /// Test_GlobalDefaultCascadeModes.
        /// </summary>
        public static void Test_GlobalDefaultCascadeModes()
        {
            /*要在规则级和/或验证程序类级全局设置默认级联模式，请在应用程序的启动例程期间设置 
             * ValidatorOptions.Global.DefaultRuleLevelCascadeMode 和/或 ValidatorOptions.Global.DefaultClassLevelCascadeMode 。
             * 这两个选项都默认为“Continue”。
             * RuleLevelCascadeMode、ClassLevelCascadeMode及其全局默认值仅在FluentValidation 11及更新版本中可用。请参见下文。
             */
        }

        /// <summary>
        /// Test_IntroductionOfRuleLevelCascadeModeAndClassLevelCascadeMode.
        /// </summary>
        public static void Test_IntroductionOfRuleLevelCascadeModeAndClassLevelCascadeMode()
        {
            // Introduction of RuleLevelCascadeMode and ClassLevelCascadeMode (and deprecation of CascadeMode)

            /*FluentValidation 11中引入了 AbstractValidator.RuleLevelCascadeMode 、 AbstractValidator.ClassLevelCascadeMode 及其全局默认值
             * 在旧版本中，只有一个属性控制级联模式：AbstractValidator.CascadeMode。更改此值将在验证程序类级别和规则级别设置级联模式。
             * 因此，例如，如果您希望具有上述功能，其中您创建验证错误列表，通过在规则级别失败时停止以避免崩溃，但在验证器类级别继续，
             * 则需要将AbstractValidator.CascadeMode设置为Continue，然后在每个规则链上重复Cascade（CascadeMode.Stop）
             * （或使用已弃用的StopOnFirstFailure并发出警告;请参阅“Stop vs StopOnFirstFailure”）
             * 新的属性使级联模式在不同级别的更精细的控制，具有更少的重复。
             */
        }

        /// <summary>
        /// Test_StopVsStopOnFirstFailure.
        /// </summary>
        public static void Test_StopVsStopOnFirstFailure()
        {
            /*在FluentValidation 9.0及更早版本中， CascadeMode.StopOnFirstFailure 选项用于在规则级别提供对默认级联模式的控制，
             * 但其使用并不直观。没有级联模式停止CascadeMode.Stop选项。
             * 使用StopOnFirstFailure，下面将提供前面描述的示例行为（如果失败，则停止任何规则，但然后继续在验证器类级别执行，以便执行所有规则）：
             */
            var person = new Person();
            var validator = new Person9Validator();
            validator.Validate(person);
        }
    }
}
