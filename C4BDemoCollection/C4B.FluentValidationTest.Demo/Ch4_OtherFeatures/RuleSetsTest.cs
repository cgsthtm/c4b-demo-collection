﻿// <copyright file="RuleSetsTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// RuleSetsTest.
    /// </summary>
    internal static class RuleSetsTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // 在这里，关于姓氏和名字的两个规则被组合在一个“名称”规则集中。我们可以通过向方法传递额外的选项来调用这些规则：
            var validator = new Person7Validator();
            var person = new Person();
            var result = validator.Validate(person, options => options.IncludeRuleSets("Names"));

            // FluentValidation中的许多方法都是扩展方法，例如上面的“Validate”，
            // 并且需要通过using语句导入FluentValidation命名空间，例如“using FluentValidation;"。

            // 这允许您将复杂的验证器定义分解为可以独立执行的更小的段。如果您在不传递规则集的情况下调用Validate，则只会执行不在RuleSet中的规则。
            // 您可以通过将多个规则集名称传递给IncludeRuleSets来执行多个规则集：
            result = validator.Validate(person, options =>
            {
                options.IncludeRuleSets("Names", "MyRuleSet", "SomeOtherRuleSet");
            });

            // 您还可以通过调用IncludeRulesNotInRuleSet或使用特殊名称“default”（不区分大小写）来包含不属于规则集的所有规则：
            validator.Validate(person, options =>
            {
                // Option 1: IncludeRulesNotInRuleSet is the equivalent of using the special ruleset name "default"
                options.IncludeRuleSets("Names").IncludeRulesNotInRuleSet();

                // Option 2: This does the same thing.
                options.IncludeRuleSets("Names", "default");

                // 这将执行MyRuleSet集中的规则，而这些规则不在任何规则库中。请注意，您不应该创建自己的称为“default”的规则库，
                // 因为FluentValidation会将这些规则视为不在规则库中。
            });

            // 您可以通过调用IncludeAllRuleSets（这相当于使用IncludeRuleSets（“*”））来强制执行所有规则，而不管它们是否在规则库中
            validator.Validate(person, options =>
            {
                options.IncludeAllRuleSets();
            });

            // 上述使用"选项"回调的语法仅在FluentValidation 9.1及更新版本中可用。如果您使用的是FluentValidation 9.0及更早版本，请阅读以下部分。
            ////validator.Validate(person, ruleSet: "Names,MyRuleSet,SomeOtherRuleSet")
            ////validator.Validate(person, ruleSet: "*")
        }
    }
}
