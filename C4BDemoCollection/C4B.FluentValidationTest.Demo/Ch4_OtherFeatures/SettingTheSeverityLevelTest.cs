﻿// <copyright file="SettingTheSeverityLevelTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using System;
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;

    /// <summary>
    /// SettingTheSeverityLevelTest.
    /// </summary>
    internal class SettingTheSeverityLevelTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            var validator = new Person10Validator();
            var results = validator.Validate(new Person());
            Console.WriteLine(results.ToString("~"));
            foreach (var failure in results.Errors)
            {
                Console.WriteLine($"Property: {failure.PropertyName} Severity: {failure.Severity}");
            }
        }
    }
}
