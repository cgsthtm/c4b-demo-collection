﻿// <copyright file="Person12Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// Person12Validator.
    /// </summary>
    internal class Person12Validator : AbstractValidator<Person>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Person12Validator"/> class.
        /// </summary>
        public Person12Validator()
        {
            // 在某些情况下，您可能希望返回有关验证规则运行时状态的上下文信息。WithState方法允许您将任何自定义数据与验证结果相关联。
            this.RuleFor(person => person.Surname).NotNull();
            this.RuleFor(person => person.Forename).NotNull().WithState(person => 1234);

            // 然后，此状态在ValidationFailure的CustomState属性中可用。
            ////var validator = new PersonValidator();
            ////var result = validator.Validate(new Person());
            ////foreach (var failure in result.Errors)
            ////{
            ////    Console.WriteLine($"Property: {failure.PropertyName} State: {failure.CustomState}");
            ////}

            /*The output would be:
                Property: Surname State:
                Property: Forename State: 1234
             */

            // 默认情况下，如果未调用WithState，CustomState属性将为空。
        }
    }
}
