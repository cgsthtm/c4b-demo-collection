﻿// <copyright file="Person9Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// Person9Validator.
    /// </summary>
    internal class Person9Validator : AbstractValidator<Person>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Person9Validator"/> class.
        /// </summary>
        public Person9Validator()
        {
            // 使用StopOnFirstFailure，下面将提供前面描述的示例行为（如果失败，则停止任何规则，但然后继续在验证器类级别执行，以便执行所有规则）：
            this.CascadeMode = CascadeMode.StopOnFirstFailure;
            this.RuleFor(x => x.Forename).NotNull().NotEqual("foo");
            this.RuleFor(x => x.MiddleNames).NotNull().NotEqual("foo");
            this.RuleFor(x => x.Surname).NotNull().NotEqual("foo");

            // 如果它们都失败，您将收到三个验证错误。这相当于
            this.RuleFor(x => x.Forename).Cascade(CascadeMode.StopOnFirstFailure).NotNull().NotEqual("foo");
            this.RuleFor(x => x.MiddleNames).Cascade(CascadeMode.StopOnFirstFailure).NotNull().NotEqual("foo");
            this.RuleFor(x => x.Surname).Cascade(CascadeMode.StopOnFirstFailure).NotNull().NotEqual("foo");

            // 多年来，这种行为造成了很多混乱，因此FluentValidation 9.1中引入了Stop选项。
            // 使用Stop而不是StopOnFirstFailure，任何失败都将停止执行，因此只返回第一个失败结果。

            /*引入了Stop选项，而不是改变StopOnFirstFailure的行为，因为这将是一个非常微妙的突破性变化，所以我们认为最好在添加新选项的同时保持现有行为。
             * StopOnFirstFailure在FluentValidation 9.1中被标记为Objective，并生成编译器警告。
             */
        }
    }
}