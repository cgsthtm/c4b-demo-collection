﻿// <copyright file="PersonAgeValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using System;
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// PersonAgeValidator.
    /// </summary>
    internal class PersonAgeValidator : AbstractValidator<Person>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonAgeValidator"/> class.
        /// </summary>
        public PersonAgeValidator()
        {
            this.RuleFor(x => x.DateOfBirth).Must(this.BeOver18);
        }

        /// <summary>
        /// BeOver18.
        /// </summary>
        /// <param name="date">date.</param>
        /// <returns>b.</returns>
        protected bool BeOver18(DateTime date)
        {
            // ...
            return true;
        }
    }
}
