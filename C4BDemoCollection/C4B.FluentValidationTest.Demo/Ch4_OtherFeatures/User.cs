﻿// <copyright file="User.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    /// <summary>
    /// User.
    /// </summary>
    internal class User
    {
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }
    }
}
