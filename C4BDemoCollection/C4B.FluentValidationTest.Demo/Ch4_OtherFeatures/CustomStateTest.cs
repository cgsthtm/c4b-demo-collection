﻿// <copyright file="CustomStateTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using System;

    /// <summary>
    /// CustomStateTest.
    /// </summary>
    internal class CustomStateTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            var validator = new Person12Validator();
            var results = validator.Validate(new Ch1_GettingStarted.Person());
            foreach (var failure in results.Errors)
            {
                Console.WriteLine($"Property: {failure.PropertyName} State: {failure.CustomState}");
            }
        }
    }
}
