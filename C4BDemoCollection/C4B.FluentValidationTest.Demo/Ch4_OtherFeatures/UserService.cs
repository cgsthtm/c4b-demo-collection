﻿// <copyright file="UserService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using System;
    using System.Threading.Tasks;
    using FluentValidation;

    /// <summary>
    /// UserService.
    /// </summary>
    internal class UserService
    {
        private readonly IValidator<User> validator;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserService"/> class.
        /// </summary>
        /// <param name="validator">validator.</param>
        public UserService(IValidator<User> validator)
        {
            this.validator = validator;
        }

        /// <summary>
        /// DoSomething.
        /// </summary>
        /// <param name="user">user.</param>
        /// <returns>Task.</returns>
        public async Task DoSomething(User user)
        {
            var validationResult = await this.validator.ValidateAsync(user);
            Console.WriteLine(validationResult.ToString("~"));
        }
    }
}