﻿// <copyright file="DependencyInjectionTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using Castle.Windsor;
    using FluentValidation;

    /// <summary>
    /// DependencyInjectionTest.
    /// </summary>
    internal class DependencyInjectionTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static async void Test()
        {
            // 验证器可以与任何依赖注入库一起使用，例如 Microsoft.Extensions.DependencyInjection 。
            // 要为特定模型注入验证器，您应该将验证器注册为服务提供程序IValidator<T>，其中T是正在验证的对象的类型。
            var container = new WindsorContainer();
            container.Register(Castle.MicroKernel.Registration.Component.For<IValidator<User>>().ImplementedBy<UserValidator>());
            container.Register(Castle.MicroKernel.Registration.Component.For<UserService>());
            var userService = container.Resolve<UserService>();
            var user = new User();
            await userService.DoSomething(user);
        }

        /// <summary>
        /// Test_AutomaticRegistration.
        /// </summary>
        public static void Test_AutomaticRegistration()
        {
            // 您也可以使用 FluentValidation.DependencyInjectionExtensions 包，它可以使用扩展方法自动查找特定程序集中的所有验证器：
            /*
            using FluentValidation.DependencyInjectionExtensions;

            public class Startup
            {
                public void ConfigureServices(IServiceCollection services)
                {
                    services.AddValidatorsFromAssemblyContaining<UserValidator>();
                    // ...
                }

                // ...
            }
             */
        }

        /// <summary>
        /// Test_FilteringResults.
        /// </summary>
        public static void Test_FilteringResults()
        {
            /*CustomerValidator不会被添加到服务提供者中（但所有其他验证器都会）。
             services.AddValidatorsFromAssemblyContaining<MyValidator>(ServiceLifetime.Scoped,
                filter => filter.ValidatorType != typeof(CustomerValidator));
             */
        }
    }
}
