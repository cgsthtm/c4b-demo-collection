﻿// <copyright file="Person7Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// Person7Validator.
    /// </summary>
    internal class Person7Validator : AbstractValidator<Person>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Person7Validator"/> class.
        /// </summary>
        public Person7Validator()
        {
            // RuleSets允许您将验证规则分组在一起，这些规则可以作为一个组一起执行，同时忽略其他规则：
            // 例如，假设Person对象上有3个属性（Id、Surname和Forename），每个属性都有一个验证规则。我们可以将姓氏和名字规则组合在一个“名称”规则集中：
            this.RuleSet("Names", () =>
            {
                this.RuleFor(x => x.Surname).NotNull();
                this.RuleFor(x => x.Forename).NotNull();
            });

            this.RuleFor(x => x.Id).NotEqual(0);

            // 在这里，关于姓氏和名字的两个规则被组合在一个“名称”规则集中。我们可以通过向方法传递额外的选项来调用这些规则：
            ////var validator = new PersonValidator();
            ////var person = new Person();
            ////var result = validator.Validate(person, options => options.IncludeRuleSets("Names"));

            // 如果您在不传递规则集的情况下调用Validate，则只会执行不在RuleSet中的规则。
            // 您可以通过将多个规则集名称传递给IncludeRuleSets来执行多个规则集：
            ////var result = validator.Validate(person, options =>
            ////{
            ////    options.IncludeRuleSets("Names", "MyRuleSet", "SomeOtherRuleSet");
            ////});
        }
    }
}
