﻿// <copyright file="ValidatingSpecificPropertiesTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// ValidatingSpecificPropertiesTest.
    /// </summary>
    internal class ValidatingSpecificPropertiesTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // 如果您的验证器包含多个属性的规则，则可以使用IncludeProperties选项将执行限制为仅验证特定属性：
            var customer = new Customer3();
            var validator = new Customer7Validator();
            validator.Validate(customer, options =>
            {
                options.IncludeProperties(x => x.Surname);
            });

            // 在上面的示例中，将只执行Surname属性的规则。

            // 当使用集合的子属性时，可以使用一个索引器（[]）来指示集合的所有项。
            // 例如，如果您想验证每个订单的成本属性，您可以使用以下命令：
            validator.Validate(customer, options =>
            {
                options.IncludeProperties("Orders[].Cost");
            });

            // 如果你想要更任意的规则分组，你可以使用规则集。
            // https://docs.fluentvalidation.net/en/latest/rulesets.html
        }
    }
}
