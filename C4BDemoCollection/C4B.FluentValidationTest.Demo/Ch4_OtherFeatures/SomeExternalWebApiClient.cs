﻿// <copyright file="SomeExternalWebApiClient.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using System.Threading.Tasks;
    using RestSharp;

    /// <summary>
    /// SomeExternalWebApiClient.
    /// </summary>
    internal class SomeExternalWebApiClient
    {
        private RestClient client;

        /// <summary>
        /// Initializes a new instance of the <see cref="SomeExternalWebApiClient"/> class.
        /// </summary>
        public SomeExternalWebApiClient()
        {
            this.client = new RestClient();
        }

        /// <summary>
        /// IdExists.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>b.</returns>
        public async Task<bool> IdExists(int id)
        {
            var request = new RestRequest("https://www.bing.com/");
            var response = await this.client.GetAsync(request);
            return id != 0 && response.IsSuccessful ? true : false;
        }
    }
}
