﻿// <copyright file="UserValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using FluentValidation;

    /// <summary>
    /// UserValidator.
    /// </summary>
    internal class UserValidator : AbstractValidator<User>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserValidator"/> class.
        /// </summary>
        public UserValidator()
        {
            this.RuleFor(user => user.Name).NotNull().NotEmpty();
        }
    }
}
