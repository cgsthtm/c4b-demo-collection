﻿// <copyright file="PersonAgeNameValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// PersonAgeNameValidator.
    /// </summary>
    internal class PersonAgeNameValidator : AbstractValidator<Person>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonAgeNameValidator"/> class.
        /// </summary>
        public PersonAgeNameValidator()
        {
            // 因为这两个验证器都针对相同的模型类型（Person），所以您可以使用Include将它们联合收割机组合在一起：
            // 您只能包含与根验证器相同类型的验证器。
            this.Include(new PersonAgeValidator());
            this.Include(new PersonNameValidator());
        }
    }
}