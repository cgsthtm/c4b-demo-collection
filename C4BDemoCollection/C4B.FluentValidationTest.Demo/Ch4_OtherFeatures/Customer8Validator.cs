﻿// <copyright file="Customer8Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch4_OtherFeatures
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// Customer8Validator.
    /// </summary>
    internal class Customer8Validator : AbstractValidator<Customer3>
    {
        private SomeExternalWebApiClient client;

        /// <summary>
        /// Initializes a new instance of the <see cref="Customer8Validator"/> class.
        /// </summary>
        public Customer8Validator()
        {
            // 在某些情况下，您可能希望定义异步规则，例如使用外部API时。默认情况下，
            // FluentValidation允许使用MustAsync或CustomAsync定义的自定义规则异步运行，以及使用WhenAsync定义异步条件。
            this.client = new SomeExternalWebApiClient();
            this.RuleFor(x => x.Id).MustAsync(async (id, cancellation) =>
            {
                bool exists = await this.client.IdExists(id);
                return exists;
            }).WithMessage("ID must be unique");

            // 验证器的验证本质上是相同的，但是你现在应该通过调用ValidateAsync来调用它：
            ////var validator = new Customer8Validator(new SomeExternalWebApiClient());
            ////var result = await validator.ValidateAsync(customer);

            // 调用ValidateAsync将同时运行同步和异步规则。
            // 如果你的验证器包含异步验证器或者异步条件，那么一定要在你的验证器上调用ValidateAsync并且永远不要Validate。
            // 如果你调用了这个Validate函数，那么就会抛出一个异常。

            // 在ASP.NET中使用自动验证时，不应使用异步规则，因为ASP.NET的验证管道不是异步的。如果使用异步规则和ASP.NET的自动验证，
            // 它们将始终同步运行（10.x和更早版本）或抛出异常（11.x和更高版本）。
        }
    }
}
