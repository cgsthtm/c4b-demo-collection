﻿// <copyright file="Customer11Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch6_Testing
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// Customer11Validator.
    /// </summary>
    internal class Customer11Validator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer11Validator"/> class.
        /// </summary>
        public Customer11Validator()
        {
            this.RuleFor(x => x.Id).Must(id => id != 0);

            // If you needed to stub this failure in a unit/integration test,
            // you could do the following:
            ////var validator = new InlineValidator<Customer3>();
            ////validator.RuleFor(x => x.Id).Must(id => false);

            // This instance could then be passed into anywhere expecting an IValidator<Customer>
        }
    }
}