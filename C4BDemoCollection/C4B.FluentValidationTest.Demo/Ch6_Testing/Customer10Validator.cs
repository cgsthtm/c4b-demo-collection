﻿// <copyright file="Customer10Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch6_Testing
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// Customer10Validator.
    /// </summary>
    internal class Customer10Validator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer10Validator"/> class.
        /// </summary>
        public Customer10Validator()
        {
            this.RuleFor(customer => customer.Surname).NotNull();
        }
    }
}
