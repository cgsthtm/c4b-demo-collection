﻿// <copyright file="TestExtensionsTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch6_Testing
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation.TestHelper;
    using Xunit;

    /// <summary>
    /// TestExtensionsTest.
    /// </summary>
    public class TestExtensionsTest
    {
        // FluentValidation提供了一些扩展，可以帮助测试验证器类。
        // 您可以使用TestValidator扩展方法调用验证器进行测试，然后对结果执行断言。这使得为验证器编写测试更容易。
        // 如果断言失败，则会抛出ValidationTestException。

        /// <summary>
        /// Should_have_error_when_Name_is_null.
        /// </summary>
        [Fact]
        public static void Should_have_error_when_Name_is_null()
        {
            var model = new Customer3 { Surname = null };
            var validator = new Customer10Validator();
            var result = validator.TestValidate(model);
            var error = result.ShouldHaveValidationErrorFor(person => person.Surname); // 断言应该有错误-成功
            ////////error.WithErrorMessage("111"); // 单元测试结果失败，因为期待【111】，而实际上是：【'Surname' 不能为Null。】
            ////error.WithErrorMessage("'Surname' 不能为Null。"); // 单元测试结果成功
        }

        /// <summary>
        /// Should_not_have_error_when_name_is_specified.
        /// </summary>
        [Fact]
        public static void Should_not_have_error_when_name_is_specified()
        {
            var model = new Customer3 { Surname = "Jimmy" };
            var validator = new Customer10Validator();
            var result = validator.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(person => person.Surname); // 断言应该没有错误-成功
        }

        // 如果有更复杂的测试，可以使用相同的技术对单个验证结果执行多个断言。举例来说：

        /// <summary>
        /// ExecuteMultipleAssertion.
        /// </summary>
        [Fact]
        public static void ExecuteMultipleAssertion()
        {
            var model = new Customer3 { Surname = null };
            var validator = new Customer10Validator();
            var result = validator.TestValidate(model);

            // 多个断言
            result.ShouldHaveValidationErrorFor(x => x.Surname);
            result.ShouldNotHaveValidationErrorFor(x => x.Forename);
            result.ShouldHaveValidationErrorFor("Orders[0].Cost");
        }

        // 您还可以将其他方法调用链接到ShouldHaveValidationErrorFor的结果，
        // 以测试验证失败的各个组件，包括错误消息，严重性，错误代码和自定义状态：

        /// <summary>
        /// Test_LinkedValidation_For_ShouldHaveValidationErrorFor.
        /// </summary>
        [Fact]
        public static void Test_LinkedValidation_For_ShouldHaveValidationErrorFor()
        {
            var model = new Customer3 { Surname = null };
            var validator = new Customer10Validator();
            var result = validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.Surname)
                .WithErrorMessage("'Surname' 不能为Null。")
                .WithErrorCode("NotNullValidator")
                .WithSeverity(FluentValidation.Severity.Error);
        }

        // 如果要确保没有发生其他验证失败，除非条件指定，请在条件后使用方法Only：

        /// <summary>
        /// Test_Only.
        /// </summary>
        [Fact]
        public static void Test_Only()
        {
            var model = new Customer3 { Surname = null };
            var validator = new Customer10Validator();
            var result = validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.Surname).Only();
        }

        // 也有反向方法可用（WithoutMessage，WithoutErrorCode，WithoutSeverity，WithoutCustomState）。

        // 还有一个异步TestValidateAsync方法，它对应于常规的ValidateAsync方法。用法类似，除了该方法返回一个等待的Task。
    }
}
