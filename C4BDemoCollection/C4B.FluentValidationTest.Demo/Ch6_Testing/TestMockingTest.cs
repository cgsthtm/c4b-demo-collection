﻿// <copyright file="TestMockingTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch6_Testing
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;
    using FluentValidation.TestHelper;
    using Xunit;

    /// <summary>
    /// TestMockingTest.
    /// </summary>
    public class TestMockingTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        [Fact]
        public static void Test()
        {
            /*模拟验证器往往需要您对验证器内部如何构建做出假设（包括它们内部包含的规则，以及FluentValidation自己的内部）。
             * 模拟这种行为会导致不安全的脆弱测试。
             * 但是，如果你发现自己处于绝对需要模拟验证器的情况下，那么我们建议使用InlineValidator<T>来创建一个存根实现，
             * 因为这样你就可以利用重用FluentValidation自己的内部逻辑来创建验证失败。
             * 我们强烈建议不要使用mocking库。使用InlineValidator的示例如下所示：
             */
            // If you needed to stub this failure in a unit/integration test,
            // you could do the following:
            var customer = new Customer3() { Id = 10 };
            var validator = new Customer11Validator();
            var result = validator.TestValidate(customer);
            result.ShouldHaveValidationErrorFor(x => x.Id);
        }
    }
}
