﻿// <copyright file="Order2Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    using FluentValidation;

    /// <summary>
    /// Order2Validator.
    /// </summary>
    internal class Order2Validator : AbstractValidator<Order2>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Order2Validator"/> class.
        /// </summary>
        public Order2Validator()
        {
            this.RuleFor(x => x.Total).GreaterThan(0);
        }
    }
}
