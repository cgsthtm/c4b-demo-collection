﻿// <copyright file="CustomerValidatorTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    using System;
    using FluentValidation;
    using FluentValidation.Results;

    /// <summary>
    /// CustomerValidatorTest.
    /// </summary>
    public class CustomerValidatorTest
    {
        /// <summary>
        /// Test_CreatingYourFirstValidator.
        /// </summary>
        public static void Test_CreatingYourFirstValidator()
        {
            Customer customer = new Customer();
            CustomerValidator validator = new CustomerValidator();

            // 要运行验证器，实例化验证器对象并调用validator方法，传入要验证的对象。
            ValidationResult results = validator.Validate(customer);
            if (!results.IsValid)
            {
                foreach (var failure in results.Errors)
                {
                    Console.WriteLine("Property " + failure.PropertyName + " failed validation. Error was: " + failure.ErrorMessage);
                }
            }

            // 您还可以在ValidationResult上调用ToString，以便将所有错误消息联合收割机组合到单个字符串中。
            // 默认情况下，消息将用新行分隔，但如果您想自定义此行为，则可以向ToString传递不同的分隔符。
            string allMessages = results.ToString("~"); // In this case, each message will be separated with a `~`
            Console.WriteLine(allMessages);
        }

        /// <summary>
        /// Test_ThrowingExceptions.
        /// </summary>
        public static void Test_ThrowingExceptions()
        {
            Customer customer = new Customer();
            CustomerValidator validator = new CustomerValidator();

            // 如果验证失败，您可以使用ValidateAndThrow方法告诉FluentValidation抛出异常，而不是返回ValidationResult：
            try
            {
                validator.ValidateAndThrow(customer);

                // ValidateAndThrow方法是对FluentValidation的选项API的有用包装，相当于执行以下操作：
                ////validator.Validate(customer, options => options.ThrowOnFailures());

                // 如果您需要将抛出异常与规则集或验证单个属性相结合，则可以使用以下语法将这两个选项相结合：
                validator.Validate(customer, options =>
                {
                    options.ThrowOnFailures();
                    options.IncludeRuleSets("MyRuleSets");
                    options.IncludeProperties(x => x.Surname);
                });

                // 可以自定义抛出的异常类型，这将在本节中介绍。
                // https://docs.fluentvalidation.net/en/latest/advanced.html#customizing-the-validation-exception
            }
            catch (ValidationException ex)
            {
                var errors = ex.Errors; // 这将引发一个ValidationException，其中包含Errors属性中的错误消息。
                Console.WriteLine($"{ex.Message}");
            }
        }

        /// <summary>
        /// Test_ComplexProperties.
        /// </summary>
        public static void Test_ComplexProperties()
        {
            ////Customer1 customer1 = new Customer1() { Address1 = new Address1() { }, };
            Customer1 customer1 = new Customer1();
            Customer1Validator validator1 = new Customer1Validator();
            ValidationResult results = validator1.Validate(customer1);
            Console.WriteLine(results.ToString("~"));
        }

        /// <summary>
        /// Test_CollectionsOfComplexTypes.
        /// </summary>
        public static void Test_CollectionsOfComplexTypes()
        {
            Customer2 customer2 = new Customer2();
            for (int i = 0; i < 20; i++)
            {
                customer2.Orders.Add(new Order2() { Cost = i, Total = i });
            }

            Customer2Validator validator2 = new Customer2Validator();
            ValidationResult results = validator2.Validate(customer2);
            Console.WriteLine(results.ToString("~"));
        }
    }
}
