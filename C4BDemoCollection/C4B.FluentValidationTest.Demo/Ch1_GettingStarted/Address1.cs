﻿// <copyright file="Address1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    /// <summary>
    /// Address1.
    /// </summary>
    internal class Address1
    {
        /// <summary>
        /// Gets or sets Line1.
        /// </summary>
        public string Line1 { get; set; }

        /// <summary>
        /// Gets or sets Line2.
        /// </summary>
        public string Line2 { get; set; }

        /// <summary>
        /// Gets or sets Town.
        /// </summary>
        public string Town { get; set; }

        /// <summary>
        /// Gets or sets Country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets Postcode.
        /// </summary>
        public string Postcode { get; set; }
    }
}
