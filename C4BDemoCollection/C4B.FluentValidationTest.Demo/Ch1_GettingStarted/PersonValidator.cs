﻿// <copyright file="PersonValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    using FluentValidation;

    /// <summary>
    /// PersonValidator.
    /// </summary>
    internal class PersonValidator : AbstractValidator<Person>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonValidator"/> class.
        /// </summary>
        public PersonValidator()
        {
            // 可以使用RuleForEach方法将同一规则应用于集合中的多个项：
            this.RuleForEach(x => x.AddressLines).NotNull();

            // 从8.5版开始，如果要访问导致验证失败的集合元素的索引，可以使用特殊的{CollectionIndex}占位符：
            this.RuleForEach(x => x.AddressLines).NotNull().WithMessage("Address {CollectionIndex} is required.");
        }
    }
}
