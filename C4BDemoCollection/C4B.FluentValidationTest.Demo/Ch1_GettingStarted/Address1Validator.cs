﻿// <copyright file="Address1Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    using FluentValidation;

    /// <summary>
    /// Address1Validator.
    /// </summary>
    internal class Address1Validator : AbstractValidator<Address1>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Address1Validator"/> class.
        /// </summary>
        public Address1Validator()
        {
            this.RuleFor(address => address.Postcode).NotNull();
        }
    }
}
