﻿// <copyright file="Customer2Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    using FluentValidation;

    /// <summary>
    /// Customer2Validator.
    /// </summary>
    internal class Customer2Validator : AbstractValidator<Customer2>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer2Validator"/> class.
        /// </summary>
        public Customer2Validator()
        {
            // 当集合是另一个复杂对象时，也可以将RuleForEach与SetValidator结合使用。举例来说：
            this.RuleForEach(x => x.Orders).SetValidator(new Order2Validator());

            // 或者，从FluentValidation 8.5开始，您还可以使用ChildRules方法在线定义子集合元素的规则：
            this.RuleForEach(x => x.Orders).ChildRules(order =>
            {
                order.RuleFor(x => x.Total).GreaterThan(0);
            });

            // 您可以选择使用Where或WhereAsync方法在集合中包含或排除某些项以使其不被验证。请注意，这必须直接在调用RuleForEach之后：
            this.RuleForEach(x => x.Orders)
              .Where(x => x.Cost != null)
              .SetValidator(new Order2Validator());

            // 从8.2版开始，使用RuleForEach的替代方法是调用ForEach作为常规RuleFor的一部分。通过这种方法，
            // 您可以将作用于整个集合的规则与作用于集合中单个元素的规则结合起来。例如，假设你有以下两条规则：
            // This rule acts on the whole collection (using RuleFor)
            ////this.RuleFor(x => x.Orders)
            ////  .Must(x => x.Count <= 10).WithMessage("No more than 10 orders are allowed");

            // This rule acts on each individual element (using RuleForEach)
            ////this.RuleForEach(x => x.Orders)
            ////  .Must(order => order.Total > 0).WithMessage("Orders must have a total of more than 0");

            // 上述两条规则可以改写为：
            this.RuleFor(x => x.Orders)
              .Must(x => x.Count <= 10).WithMessage("No more than 10 orders are allowed")
              .ForEach(orderRule =>
              {
                  orderRule.Must(order => order.Total > 0).WithMessage("Orders must have a total of more than 0");
              });

            // 我们建议使用两个单独的规则，因为这样更清晰，更容易阅读，但ForEach方法可以选择将它们组合在一起。
        }
    }
}
