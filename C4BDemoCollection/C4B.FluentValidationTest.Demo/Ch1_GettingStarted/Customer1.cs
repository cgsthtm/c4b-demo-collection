﻿// <copyright file="Customer1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    /// <summary>
    /// Customer1.
    /// </summary>
    internal class Customer1
    {
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Address1.
        /// </summary>
        public Address1 Address1 { get; set; }
    }
}
