﻿// <copyright file="Person.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Person.
    /// </summary>
    internal class Person
    {
        /// <summary>
        /// Gets or sets AddressLines.
        /// </summary>
        public List<string> AddressLines { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets Pets.
        /// </summary>
        public IList<Pet> Pets { get; set; } = new List<Pet>();

        /// <summary>
        /// Gets or sets DateOfBirth.
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets Surname.
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Gets or sets Forename.
        /// </summary>
        public string Forename { get; set; }

        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets MiddleNames.
        /// </summary>
        public string MiddleNames { get; set; }
    }
}
