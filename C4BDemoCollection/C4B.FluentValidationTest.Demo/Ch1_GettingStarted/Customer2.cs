﻿// <copyright file="Customer2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    using System.Collections.Generic;

    /// <summary>
    /// Customer2.
    /// </summary>
    internal class Customer2
    {
        /// <summary>
        /// Gets or sets Orders.
        /// </summary>
        public List<Order2> Orders { get; set; } = new List<Order2>();
    }
}
