﻿// <copyright file="PersonValidatorTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    using System;

    /// <summary>
    /// PersonValidatorTest.
    /// </summary>
    internal class PersonValidatorTest
    {
        /// <summary>
        /// Test_Collections_Of_Simple_Types.
        /// </summary>
        public static void Test_Collections_Of_Simple_Types()
        {
            var person = new Person();
            person.AddressLines.Add("000");
            person.AddressLines.Add("111");
            person.AddressLines.Add(null); // 'Address Lines' 不能为Null。~Address 2 is required.
            person.AddressLines.Add("333");
            var validator = new PersonValidator();
            var result = validator.Validate(person);
            Console.WriteLine(result.ToString("~"));
        }
    }
}
