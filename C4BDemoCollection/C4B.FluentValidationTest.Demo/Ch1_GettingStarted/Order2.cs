﻿// <copyright file="Order2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    /// <summary>
    /// Order2.
    /// </summary>
    internal class Order2
    {
        /// <summary>
        /// Gets or sets Total.
        /// </summary>
        public double Total { get; set; }

        /// <summary>
        /// Gets or sets Cost.
        /// </summary>
        public decimal Cost { get; set; }
    }
}
