﻿// <copyright file="CustomerValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    using FluentValidation;

    /// <summary>
    /// CustomerValidator.
    /// </summary>
    public class CustomerValidator : AbstractValidator<Customer>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerValidator"/> class.
        /// </summary>
        public CustomerValidator()
        {
            // 验证规则本身应该在验证器类的构造函数中定义。
            // 要为特定属性指定验证规则，请调用RuleFor方法，传递一个指示要验证的属性的lambda表达式。
            // 例如，为了确保Surname属性不为null，验证器类应该如下所示：
            this.RuleFor(customer => customer.Surname).NotNull();

            // 您可以为同一属性链接多个验证器：
            this.RuleFor(customer => customer.Forename).NotNull().NotEqual("foo");
        }
    }
}
