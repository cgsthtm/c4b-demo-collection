﻿// <copyright file="Customer1Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch1_GettingStarted
{
    using FluentValidation;

    /// <summary>
    /// Customer1Validator.
    /// </summary>
    internal class Customer1Validator : AbstractValidator<Customer1>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer1Validator"/> class.
        /// </summary>
        public Customer1Validator()
        {
            this.RuleFor(c1 => c1.Name).NotNull().NotEmpty();

            // 在Customer1Validator定义中重用Address1Validator：
            // 因此，当您在Customer1Validator上调用Validator时，它将运行Customer1Validator和Address1Validator中定义的验证器，
            // 并将结果联合收割机组合为单个ValidationResult
            this.RuleFor(c1 => c1.Address1).SetValidator(new Address1Validator());

            // 如果子属性为null，则子验证器将不会被执行。

            // 不使用子验证器，你可以内联定义子规则，例如：
            ////this.RuleFor(c1 => c1.Address1.Postcode).NotNull();

            // 在这种情况下，不会对Address1自动执行空值检查，因此应显式添加条件
            ////this.RuleFor(c1 => c1.Address1.Postcode).NotNull().When(c1 => c1.Address1 != null);
        }
    }
}
