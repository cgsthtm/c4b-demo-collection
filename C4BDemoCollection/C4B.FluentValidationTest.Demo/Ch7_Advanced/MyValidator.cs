﻿// <copyright file="MyValidator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    using FluentValidation;
    using FluentValidation.Results;

    /// <summary>
    /// MyValidator.
    /// </summary>
    internal class MyValidator : AbstractValidator<Person2>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MyValidator"/> class.
        /// </summary>
        public MyValidator()
        {
            this.RuleFor(x => x.Name).NotNull();
        }

        /*如果需要在每次调用验证器时运行特定的代码，可以通过重写Predict方法来实现。
         * 这个方法接受一个ValidationContext和一个ValidationResult，您可以使用它们来自定义验证过程。
         * 如果验证应该继续，该方法应该返回true，或者返回false以立即中止。您对ValidationResult所做的任何修改都将返回给用户。
         * 请注意，此方法在FluentValidation对正在验证的模型执行标准空值检查之前被调用，因此如果整个模型为空，
         * 您可以使用此方法生成错误，而不是依赖于FluentValidation在这种情况下的标准行为（即抛出异常）：
         */

        /// <inheritdoc/>
        protected override bool PreValidate(ValidationContext<Person2> context, ValidationResult result)
        {
            ////return base.PreValidate(context, result);
            if (context.InstanceToValidate == null)
            {
                result.Errors.Add(new ValidationFailure(string.Empty, "Please ensure a model was supplied."));
                return false;
            }

            return true;
        }
    }
}
