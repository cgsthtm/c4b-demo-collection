﻿// <copyright file="FluentValidationExtensions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    using System;
    using FluentValidation;

    /// <summary>
    /// FluentValidationExtensions.
    /// </summary>
    public static class FluentValidationExtensions
    {
        // 如果您使用ValidateAndThrow方法在验证失败时抛出异常，FluentValidation将在内部抛出ValidationException。
        // 您可以自定义此行为，以便通过覆盖验证器中的RaiseValidationException来抛出不同的异常。

        // 作为一种替代方法，您可以创建自己的扩展方法，该方法调用Validate，然后在出现验证错误时抛出自己的自定义异常。
        // 如果您只想在调用特定方法时抛出自定义异常，而不是在调用ValidateAndThrow时抛出自定义异常，则此方法更有用。

        /// <summary>
        /// ValidateAndThrowArgumentException.
        /// </summary>
        /// <typeparam name="T">待验证的实体的类型.</typeparam>
        /// <param name="validator">validator.</param>
        /// <param name="instance">待验证的实体.</param>
        /// <exception cref="ArgumentException">抛出参数异常.</exception>
        public static void ValidateAndThrowArgumentException<T>(this IValidator<T> validator, T instance)
        {
            var res = validator.Validate(instance);

            if (!res.IsValid)
            {
                var ex = new ValidationException(res.Errors);
                throw new ArgumentException(ex.Message, ex);
            }
        }
    }
}