﻿// <copyright file="IContact.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    /// <summary>
    /// IContact.
    /// </summary>
    internal interface IContact
    {
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets Email.
        /// </summary>
        string Email { get; set; }
    }
}
