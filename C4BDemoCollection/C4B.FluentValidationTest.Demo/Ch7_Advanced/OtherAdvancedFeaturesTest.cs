﻿// <copyright file="OtherAdvancedFeaturesTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    using System;
    using FluentValidation;

    /// <summary>
    /// OtherAdvancedFeaturesTest.
    /// </summary>
    internal class OtherAdvancedFeaturesTest
    {
        /// <summary>
        /// Test_PreValidate.
        /// </summary>
        public static void Test_PreValidate()
        {
            Person2 person = null;
            var validator = new MyValidator();
            var results = validator.Validate(person);
            Console.WriteLine(results.ToString("~"));
        }

        /// <summary>
        /// Test_RootContextData.
        /// </summary>
        public static void Test_RootContextData()
        {
            /*对于高级用户来说，可以将任意数据传递到验证管道中，可以从自定义属性验证器中访问这些数据。
             * 如果您需要根据被验证对象中不可用的任意数据做出有条件的决定，这一点特别有用，因为验证器是无状态的。
             * RootContextData属性是ValidationContext上可用的Dictionary<string，object>。：
             */
            var person = new Person2();
            var context = new ValidationContext<Person2>(person);
            context.RootContextData["MyCustomData"] = "Test";
            var validator = new Person12Validator();
            var results = validator.Validate(context);
            Console.WriteLine(results.ToString("~"));
        }

        /// <summary>
        /// Test_CustomizingTheValidationException.
        /// </summary>
        public static void Test_CustomizingTheValidationException()
        {
            /*如果您使用ValidateAndThrow方法在验证失败时抛出异常，FluentValidation将在内部抛出ValidationException。
             * 您可以自定义此行为，以便通过覆盖验证器中的RaiseValidationException来抛出不同的异常。
             */
            var person = new Person2();
            var validator2 = new MyValidator2();
            try
            {
                validator2.ValidateAndThrow(person);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"ValidateAndThrow :{ex.Message}");
            }
            catch (ValidationException ex)
            {
                // 不执行，因为MyValidator2已经通过重写RaiseValidationException把ValidationException
                // 包装成了ArgumentException
                Console.WriteLine("不执行");
            }

            var validator = new MyValidator();
            try
            {
                validator.ValidateAndThrowArgumentException(person);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"ValidateAndThrowArgumentException: {ex.Message}");
            }
        }
    }
}
