﻿// <copyright file="InheritanceValidationTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    using System;

    /// <summary>
    /// InheritanceValidationTest.
    /// </summary>
    internal class InheritanceValidationTest
    {
        // 从FluentValidation 9.2开始，如果你的对象包含一个属性，它是一个基类或接口，你可以为各个子类/实现者设置特定的子验证器。

        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            var validator = new ContactRequest2Validator();
            var contactRequest2 = new ContactRequest2()
            {
                Contact = new Organisation2()
                {
                    Name = "cgs",
                    Email = null,
                    Headquarters = new Ch1_GettingStarted.Address1(),
                },
            };
            var results = validator.Validate(contactRequest2);
            Console.WriteLine(results.ToString("~"));
        }
    }
}
