﻿// <copyright file="Organisation2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;

    /// <summary>
    /// Organisation2.
    /// </summary>
    internal class Organisation2 : IContact
    {
        /// <inheritdoc/>
        public string Name { get; set; }

        /// <inheritdoc/>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets 总部.
        /// </summary>
        public Address1 Headquarters { get; set; }
    }
}
