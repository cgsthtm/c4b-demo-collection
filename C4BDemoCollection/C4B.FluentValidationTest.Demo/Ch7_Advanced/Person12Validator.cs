﻿// <copyright file="Person12Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    using FluentValidation;

    /// <summary>
    /// Person12Validator.
    /// </summary>
    internal class Person12Validator : AbstractValidator<Person2>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Person12Validator"/> class.
        /// </summary>
        public Person12Validator()
        {
            this.RuleFor(x => x.Name).Custom((x, context) =>
            {
                // 可以在任何自定义属性验证器中访问RootContextData，以及调用Custom：
                if (context.RootContextData.ContainsKey("MyCustomData"))
                {
                    context.AddFailure("My error message");
                }
            });
        }
    }
}
