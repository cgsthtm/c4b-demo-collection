﻿namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// ContactRequest2Validator.
    /// </summary>
    internal class ContactRequest2Validator : AbstractValidator<ContactRequest2>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContactRequest2Validator"/> class.
        /// </summary>
        public ContactRequest2Validator()
        {
            // 我们可以为IContact属性定义特定的验证器，这取决于它的运行时类型。
            // 这是通过调用SetInheritanceValidator来完成的，传入一个可用于定义特定子验证器的函数：
            this.RuleFor(x => x.Contact).SetInheritanceValidator(v =>
            {
                v.Add<Organisation2>(new Organisation2Validator());
                v.Add<Person2>(new Person2Validator());
            });

            // 此方法也适用于集合，其中集合的每个元素可能是不同的子类。
            // 例如，以上面的例子为例，如果ContactRequest不是一个单独的Contact属性，而是一个联系人集合：
            /*
            public class ContactRequest
            {
              public List<IContact> Contacts { get; } = new();
            }
            .然后你可以为集合中的每个项目定义继承验证：
            public class ContactRequestValidator : AbstractValidator<ContactRequest>
            {
              public ContactRequestValidator()
              {

                RuleForEach(x => x.Contacts).SetInheritanceValidator(v =>
                {
                  v.Add<Organisation>(new OrganisationValidator());
                  v.Add<Person>(new PersonValidator());
                });
              }
            }
             */
        }
    }
}