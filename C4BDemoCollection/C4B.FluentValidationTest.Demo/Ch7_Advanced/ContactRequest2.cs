﻿// <copyright file="ContactRequest2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    /// <summary>
    /// ContactRequest2.
    /// </summary>
    internal class ContactRequest2
    {
        /// <summary>
        /// Gets or sets Contact.
        /// </summary>
        public IContact Contact { get; set; }

        /// <summary>
        /// Gets or sets MessageToSend.
        /// </summary>
        public string MessageToSend { get; set; }
    }
}
