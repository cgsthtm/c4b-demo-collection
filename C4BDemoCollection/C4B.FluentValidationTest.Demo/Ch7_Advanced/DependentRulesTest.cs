﻿// <copyright file="DependentRulesTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation.TestHelper;
    using Xunit;

    /// <summary>
    /// DependentRulesTest.
    /// </summary>
    public class DependentRulesTest
    {
        // 默认情况下，FluentValidation中的所有规则都是独立的，不会相互影响。这是有意的，也是异步验证工作所必需的。
        // 但是，在某些情况下，您可能希望确保某些规则仅在另一个规则完成后才执行。您可以使用DependentRules来执行此操作。

        /// <summary>
        /// Test_DependentRules.
        /// </summary>
        [Fact]
        public static void Test_DependentRules()
        {
            var customer = new Customer3() { Surname = null, Forename = null };
            var validator = new Customer12Validator();
            var result = validator.TestValidate(customer);
            result.ShouldHaveValidationErrorFor(x => x.Surname);
            result.ShouldNotHaveValidationErrorFor(x => x.Forename);

            // 作者注：就我个人而言，我并不特别喜欢使用依赖规则，因为我觉得它相当难以阅读，特别是对于一组复杂的规则。
            // 在许多情况下，将When条件与CascadeMode结合使用可以更简单地防止规则在某些情况下运行。尽管这有时意味着更多的重复，但通常更容易阅读。
        }
    }
}