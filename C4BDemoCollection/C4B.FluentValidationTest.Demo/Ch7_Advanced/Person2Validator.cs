﻿// <copyright file="Person2Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    using System;
    using FluentValidation;

    /// <summary>
    /// Person2Validator.
    /// </summary>
    internal class Person2Validator : AbstractValidator<Person2>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Person2Validator"/> class.
        /// </summary>
        public Person2Validator()
        {
            this.RuleFor(x => x.Name).NotNull();
            this.RuleFor(x => x.Email).NotNull();
            this.RuleFor(x => x.DateOfBirth).GreaterThan(DateTime.MinValue);
        }
    }
}