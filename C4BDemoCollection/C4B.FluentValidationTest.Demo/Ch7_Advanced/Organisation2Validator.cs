﻿// <copyright file="Organisation2Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    using C4B.FluentValidationTest.Demo.Ch1_GettingStarted;
    using FluentValidation;

    /// <summary>
    /// Organisation2Validator.
    /// </summary>
    internal class Organisation2Validator : AbstractValidator<Organisation2>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Organisation2Validator"/> class.
        /// </summary>
        public Organisation2Validator()
        {
            this.RuleFor(x => x.Name).NotNull();
            this.RuleFor(x => x.Email).NotNull();
            this.RuleFor(x => x.Headquarters).SetValidator(new Address1Validator());
        }
    }
}