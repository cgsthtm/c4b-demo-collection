﻿// <copyright file="MyValidator2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    using System;
    using FluentValidation;
    using FluentValidation.Results;

    /// <summary>
    /// MyValidator2.
    /// </summary>
    internal class MyValidator2 : AbstractValidator<Person2>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MyValidator2"/> class.
        /// </summary>
        public MyValidator2()
        {
            this.RuleFor(p => p.Name).NotNull();
        }

        /// <inheritdoc/>
        protected override void RaiseValidationException(ValidationContext<Person2> context, ValidationResult result)
        {
            // 这个简单的例子将默认的ValidationException包装在ArgumentException中：
            // 如果您总是希望在每次调用ValidateAndThrow时引发特定的自定义异常类型，则此方法非常有用。
            ////base.RaiseValidationException(context, result);
            var ex = new ValidationException(result.Errors);
            throw new ArgumentException(ex.Message, ex);
        }
    }
}
