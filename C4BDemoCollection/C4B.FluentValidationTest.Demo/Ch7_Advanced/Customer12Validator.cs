﻿// <copyright file="Customer12Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch7_Advanced
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// Customer12Validator.
    /// </summary>
    internal class Customer12Validator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer12Validator"/> class.
        /// </summary>
        public Customer12Validator()
        {
            // 要使用依赖规则，请在您希望其他人依赖的规则的末尾调用DependentRules方法。
            // 此方法接受一个lambda表达式，您可以在其中定义其他规则，这些规则仅在第一个规则通过时才执行：
            this.RuleFor(x => x.Surname).NotNull().DependentRules(() =>
            {
                this.RuleFor(x => x.Forename).NotNull();
            });

            // 这里，只有在通过了姓氏规则的情况下，才会运行针对“名字”的规则。
        }
    }
}
