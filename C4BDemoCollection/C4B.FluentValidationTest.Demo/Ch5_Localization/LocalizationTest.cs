﻿// <copyright file="LocalizationTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch5_Localization
{
    using System;
    using System.Globalization;
    using FluentValidation;

    /// <summary>
    /// LocalizationTest.
    /// </summary>
    internal class LocalizationTest
    {
        /*FluentValidation提供了多种语言的默认验证消息的翻译。
         * 默认情况下，在翻译消息时将使用.NET框架的当前UI区域性中指定的语言（CultureInfo.CurrentUICulture）。
         * 还可以使用WithMessage和WithLocalizedMessage方法为单个验证规则指定本地化的错误消息。
         */

        /// <summary>
        /// Test_WithMessage.
        /// </summary>
        public static void Test_WithMessage()
        {
            var validator = new Customer9Validator();
            var results = validator.Validate(new Ch2_ConfiguringValidators.Customer3());
            Console.WriteLine(results.ToString("~"));
        }

        /// <summary>
        /// Test_IStringLocalizer.
        /// </summary>
        public static void Test_IStringLocalizer()
        {
            /*上面的两个例子假设你在一个资源文件周围使用一个强类型的包装器，其中类上的每个静态属性对应于资源文件中的一个键。
             * 这是在ASP.NET Core之前使用资源的“旧”方法，但如果您使用ASP.NET Core的IStringLocalizer，则不相关。
             * 如果你使用IStringLocalizer来处理本地化，那么你需要做的就是将本地化器注入到验证器中，并在WithMessage回调中使用它，例如：
            public class PersonValidator : AbstractValidator<Person> 
            {
              public PersonValidator(IStringLocalizer<Person> localizer)
               {
                RuleFor(x => x.Surname).NotNull().WithMessage(x => localizer["Surname is required"]);
              }
            }
             */
        }

        /// <summary>
        /// Test_DefaultMessage.
        /// </summary>
        public static void Test_DefaultMessage()
        {
            ValidatorOptions.Global.LanguageManager = new CustomLanguageManager();
            Test_WithMessage();
        }

        /// <summary>
        /// Test_DisablingLocalization.
        /// </summary>
        public static void Test_DisablingLocalization()
        {
            /*您可以完全禁用FluentValidation对本地化的支持，这将强制使用默认的英语消息，而不管线程的CurrentUICulture如何。
             * 这可以在应用程序的启动例程中通过调用静态ValidatorOptions类来完成：
             */
            ValidatorOptions.Global.LanguageManager.Enabled = false;
            Test_WithMessage();

            // 您还可以强制默认消息始终以特定语言显示：
            ValidatorOptions.Global.LanguageManager.Enabled = true;
            ValidatorOptions.Global.LanguageManager.Culture = new CultureInfo("fr");
            Test_WithMessage();
        }
    }
}
