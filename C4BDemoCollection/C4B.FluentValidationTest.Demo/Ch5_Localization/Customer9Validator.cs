﻿// <copyright file="Customer9Validator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch5_Localization
{
    using C4B.FluentValidationTest.Demo.Ch2_ConfiguringValidators;
    using FluentValidation;

    /// <summary>
    /// Customer9Validator.
    /// </summary>
    internal class Customer9Validator : AbstractValidator<Customer3>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer9Validator"/> class.
        /// </summary>
        public Customer9Validator()
        {
            // 如果您使用Visual Studio内置的.resx文件及其强类型包装器支持，则可以通过调用接受lambda表达式的WithMessage重载来本地化消息：
            this.RuleFor(x => x.Surname).NotNull().WithMessage(x => MyLocalizedMessages.SurnameRequired);

            // 如果需要从另一个源（比如数据库）获取本地化消息，也可以使用相同的方法，方法是从lambda中获取字符串。
            this.RuleFor(x => x.Forename).NotNull();
        }
    }
}