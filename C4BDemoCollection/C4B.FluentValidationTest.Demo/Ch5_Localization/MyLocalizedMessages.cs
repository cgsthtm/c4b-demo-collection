﻿// <copyright file="MyLocalizedMessages.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch5_Localization
{
    /// <summary>
    /// MyLocalizedMessages.
    /// </summary>
    internal static class MyLocalizedMessages
    {
        /// <summary>
        /// SurnameRequired.
        /// </summary>
        public static readonly string SurnameRequired = "姓氏不能为空！";
    }
}
