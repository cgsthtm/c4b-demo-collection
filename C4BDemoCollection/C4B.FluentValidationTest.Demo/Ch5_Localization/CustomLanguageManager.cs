﻿// <copyright file="CustomLanguageManager.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.FluentValidationTest.Demo.Ch5_Localization
{
    using FluentValidation;

    /// <summary>
    /// CustomLanguageManager.
    /// </summary>
    internal class CustomLanguageManager : FluentValidation.Resources.LanguageManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomLanguageManager"/> class.
        /// </summary>
        public CustomLanguageManager()
        {
            /*如果您想替换FluentValidation的所有（或部分）默认消息，则可以通过实现ILanguageManager接口的自定义版本来实现。
             * 例如，NotNull验证器的默认消息是 '{PropertyName}' must not be empty. 。
             * 如果你想在你的应用程序中所有使用NotNull验证器的情况下都替换这个消息，你可以写一个自定义的语言管理器：
             */
            this.AddTranslation("en", "NotNullValidator", "'{PropertyName}' is required.");
            this.AddTranslation("en-US", "NotNullValidator", "'{PropertyName}' is required.");
            this.AddTranslation("en-GB", "NotNullValidator", "'{PropertyName}' is required.");
            this.AddTranslation("zh", "NotNullValidator", "'{PropertyName}'是必须的！");
            this.AddTranslation("zh-CN", "NotNullValidator", "'{PropertyName}'是必须的！");

            // 这里我们有一个自定义类，它继承自基本的LanguageManager。在它的构造函数中，我们调用AddTranslation方法，
            // 传入我们正在使用的语言、我们想要覆盖的验证器的名称和新消息。

            // 完成此操作后，我们可以在应用程序的启动例程期间，通过在静态ValidatorOptions类中
            // 设置LanguageManager属性来替换默认的LanguageManager：
            ////ValidatorOptions.Global.LanguageManager = new CustomLanguageManager();

            // 请注意，如果替换en区域性中的消息，则还应考虑替换en-US和en-GB的消息，因为这些区域设置的用户将优先使用这些消息。

            /*这是一个简单的例子，它只替换一个验证器的英语消息，但是可以扩展到替换所有语言的消息。
             * 如果您想从FluentValidation默认值以外的完全不同的位置加载消息（例如，如果您想将FluentValidation的默认消息存储在数据库中），
             * 您也可以直接实现ILanguageManager接口，而不是从默认的LanguageManager继承。
             */

            // 当然，如果您只想替换此消息以供验证器使用，那么您可以使用 WithMessage("'{PropertyName}' is required");
        }
    }
}
