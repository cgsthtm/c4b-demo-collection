﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.CommunityToolkit.Mvvm.Demo
{
    public partial class ObservablePropertyDemo : ObservableObject
    {
        [ObservableProperty]
        [NotifyPropertyChangedFor(nameof(FullName))]
        [NotifyCanExecuteChangedFor(nameof(MyCommand))]
        private string? name;

        partial void OnNameChanging(string? value)
        {
            Console.WriteLine($"Name is about to change to {value}");
        }

        partial void OnNameChanged(string? value)
        {
            Console.WriteLine($"Name has changed to {value}");
        }

        [ObservableProperty]
        private string? fullName;

        partial void OnFullNameChanged(string? value)
        {
            Console.WriteLine($"FullName has changed to {value}");
        }

        public IRelayCommand MyCommand { get; }
    }
}
