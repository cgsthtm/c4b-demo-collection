﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.CommunityToolkit.Mvvm.Demo
{
    public partial class ObservableValidatorDemo : ObservableValidator
    {
        [ObservableProperty]
        [NotifyDataErrorInfo]
        [Required]
        [MinLength(2)] // Any other validation attributes too...
        private string? name;
    }
}
