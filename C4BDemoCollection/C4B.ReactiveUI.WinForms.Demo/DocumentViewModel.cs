﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace C4B.ReactiveUI.WinForms.Demo
{
    public class DocumentViewModel : ReactiveObject
    {
        public ReactiveCommand<Unit, Unit> Close { get; set; }

        public DocumentViewModel()
        {
            // Note that we don't actually *subscribe* to Close here or implement
            // anything in DocumentViewModel, because Closing is a responsibility
            // of the document list.
            Close = ReactiveCommand.Create(() => { Console.WriteLine("DocumentViewModel.Close...."); });
        }
    }
}
