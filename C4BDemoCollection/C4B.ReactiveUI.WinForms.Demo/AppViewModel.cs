﻿using DevExpress.XtraPrinting;
using DynamicData;
using DynamicData.Binding;
using NuGet.Common;
using NuGet.Configuration;
using NuGet.Protocol.Core.Types;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Validation.Abstractions;
using ReactiveUI.Validation.Contexts;
using ReactiveUI.Validation.Extensions;
using ReactiveUI.Validation.States;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using static C4B.ReactiveUI.WinForms.Demo.SomeViewModel1;

namespace C4B.ReactiveUI.WinForms.Demo
{
    // AppViewModel is where we will describe the interaction of our application.
    // We can describe the entire application in one class since it's very small now. 
    // Most ViewModels will derive off ReactiveObject, while most Model classes will 
    // most derive off INotifyPropertyChanged
    public class AppViewModel : ReactiveObject, IEnableLogger, IValidatableViewModel, IActivatableViewModel
    {
        // In ReactiveUI, this is the syntax to declare a read-write property
        // that will notify Observers, as well as WPF, that a property has 
        // changed. If we declared this as a normal property, we couldn't tell 
        // when it has changed!
        private string _searchTerm;
        public string SearchTerm
        {
            get => _searchTerm;
            set => this.RaiseAndSetIfChanged(ref _searchTerm, value);
        }

        private string _red;
        public string Red
        {
            get => _red ?? "207";
            set => this.RaiseAndSetIfChanged(ref _red, value);
        }

        private string _green;
        public string Green
        {
            get => _green ?? "153";
            set => this.RaiseAndSetIfChanged(ref _green, value);
        }

        private string _blue;
        public string Blue
        {
            get => _blue ?? "251";
            set => this.RaiseAndSetIfChanged(ref _blue, value);
        }

        private readonly ObservableAsPropertyHelper<Color> _textColor;
        public Color TextColor => _textColor.Value;

        private string _username;
        public string Username
        {
            get => _username;
            set => this.RaiseAndSetIfChanged(ref _username, value);
        }

        private string _password;
        public string Password
        {
            get => _password;
            set => this.RaiseAndSetIfChanged(ref _password, value);
        }

        private bool _showToolTip;
        public bool ShowToolTip
        {
            get => _showToolTip;
            set => this.RaiseAndSetIfChanged(ref _showToolTip, value);
        }

        [Reactive]
        public Document Document { get; set; }

        // Here's the interesting part: In ReactiveUI, we can take IObservables
        // and "pipe" them to a Property - whenever the Observable yields a new
        // value, we will notify ReactiveObject that the property has changed.
        // 
        // To do this, we have a class called ObservableAsPropertyHelper - this
        // class subscribes to an Observable and stores a copy of the latest value.
        // It also runs an action whenever the property changes, usually calling
        // ReactiveObject's RaisePropertyChanged.
        private readonly ObservableAsPropertyHelper<IEnumerable<NugetDetailsViewModel>> _searchResults;
        public IEnumerable<NugetDetailsViewModel> SearchResults => _searchResults.Value?.ToList();

        // Here, we want to create a property to represent when the application 
        // is performing a search (i.e. when to show the "spinner" control that 
        // lets the user know that the app is busy). We also declare this property
        // to be the result of an Observable (i.e. its value is derived from 
        // some other property)
        private readonly ObservableAsPropertyHelper<bool> _isAvailable;
        public bool IsAvailable => _isAvailable.Value;

        [Reactive]
        public int Age { get; set; }

        [Reactive]
        public string SomeProperty { get; set; }

        [Reactive]
        public string Name { get; set; }

        [Reactive]
        public string SelectedItem { get; set; }

        [Reactive]
        public bool IsSelected { get; set; }

        [Reactive]
        public IObservable<bool> Cancel { get; set; }

        [Reactive]
        public bool ViewModelProperty { get; set; }

        [Reactive]
        public DateTime DateTime { get; set; }

        // Interactions
        private readonly Interaction<string, bool> confirm;
        public Interaction<string, bool> Confirm => this.confirm;
        public ReactiveCommand<Unit, Unit> CommandDeleteFileAsync { get; }
        public Interaction<Unit, Unit> MyInteraction { get; }

        [Reactive]
        public bool CredentialsAreValid { get; set; }

        public ObservableCollection<DocumentViewModel> OpenDocuments { get; protected set; }

        // Observable as property helper - Example
        readonly ObservableAsPropertyHelper<string> firstName;
        public string FirstName => firstName.Value;

        // Observable as property helper - Defer Subscription
        // nameStatusObservable won't be subscribed until the 
        // Name1 property is accessed.
        private readonly ObservableAsPropertyHelper<string> name1;
        public string Name1 => name1.Value;
        readonly ObservableAsPropertyHelper<string> status;
        public string Status => status.Value;

        // Routing
        public RoutingState Router { get; }
        public ReactiveCommand<Unit, Unit> ShowHomeCommand { get; }
        public ReactiveCommand<Unit, Unit> ShowAboutCommand { get; }
        public ReactiveCommand<Unit, Unit> ShowContactCommand { get; }
        public ReactiveCommand<Unit, Unit> GoBackCommand { get; }

        [Reactive]
        public string Name2 { get; set; }
        [Reactive]
        public string ConfirmPassword { get; set; }
        [Reactive]
        public string UserName { get; set; }

        // ObservableAsPropertyHelper properties
        // With ReactiveUI.Fody, you can simply declare a read-only property using the [ObservableAsProperty] attribute,
        // using either option of the two options shown below. One option is to annotate the getter of the property:
        public string FirstName1 { [ObservableAsProperty] get; }
        // Another option is to annotate the property as a whole:
        [ObservableAsProperty]
        public string FirstName2 { get; }


        public AppViewModel()
        {
            Activator = new ViewModelActivator();
            this.WhenActivated(disposables =>
            {
                // Use WhenActivated to execute logic
                // when the view model gets activated.
                this.HandleActivation();

                // Or use WhenActivated to execute logic
                // when the view model gets deactivated.
                Disposable
                    .Create(() => this.HandleDeactivation())
                    .DisposeWith(disposables);

                // Here we create a hot observable and 
                // subscribe to its notifications. The
                // subscription should be disposed when we'd 
                // like to deactivate the ViewModel instance.
                var interval = TimeSpan.FromMinutes(5);
                Observable
                    .Timer(interval, interval)
                    .Subscribe(x => { /* do smth every 5m */ })
                    .DisposeWith(disposables);

                // We also can observe changes of a
                // property belonging to another ViewModel,
                // so we need to unsubscribe from that
                // changes to ensure we won't have the
                // potential for a memory leak.
                ////this.WhenAnyValue(...)
                ////    .InvokeCommand(...)
                ////    .DisposeWith(disposables);
            });

            // Creating our UI declaratively
            // 
            // The Properties in this ViewModel are related to each other in different 
            // ways - with other frameworks, it is difficult to describe each relation
            // succinctly; the code to implement "The UI spinner spins while the search 
            // is live" usually ends up spread out over several event handlers.
            //
            // However, with ReactiveUI, we can describe how properties are related in a 
            // very organized clear way. Let's describe the workflow of what the user does 
            // in this application, in the order they do it.

            // We're going to take a Property and turn it into an Observable here - this
            // Observable will yield a value every time the Search term changes, which in
            // the XAML, is connected to the TextBox. 
            //
            // We're going to use the Throttle operator to ignore changes that happen too 
            // quickly, since we don't want to issue a search for each key pressed! We 
            // then pull the Value of the change, then filter out changes that are identical, 
            // as well as strings that are empty.
            //
            // We then do a SelectMany() which starts the task by converting Task<IEnumerable<T>> 
            // into IObservable<IEnumerable<T>>. If subsequent requests are made, the 
            // CancellationToken is called. We then ObserveOn the main thread, 
            // everything up until this point has been running on a separate thread due 
            // to the Throttle().
            //
            // We then use an ObservableAsPropertyHelper, OAPH, and the ToProperty() method to allow
            // us to have the latest results that we can expose through the property to the View.
            _searchResults = this
                .WhenAnyValue(x => x.SearchTerm)
                .Throttle(TimeSpan.FromMilliseconds(800))
                .Select(term => term?.Trim())
                .DistinctUntilChanged()
                .Where(term => !string.IsNullOrWhiteSpace(term))
                .SelectMany(SearchNuGetPackages)
                .ObserveOn(RxApp.MainThreadScheduler)
                .ToProperty(this, x => x.SearchResults);

            // We subscribe to the "ThrownExceptions" property of our OAPH, where ReactiveUI 
            // marshals any exceptions that are thrown in SearchNuGetPackages method. 
            // See the "Error Handling" section for more information about this.
            _searchResults.ThrownExceptions.Subscribe(error => { /* Handle errors here */ });

            // A helper method we can use for Visibility or Spinners to show if results are available.
            // We get the latest value of the SearchResults and make sure it's not null.
            _isAvailable = this
                .WhenAnyValue(x => x.SearchResults)
                .Select(searchResults => searchResults != null)
                .ToProperty(this, x => x.IsAvailable);

            // Watching a number of properties
            this
                .WhenAnyValue(x => x.Red, x => x.Green, x => x.Blue,
                                // Exposing 'calculated' properties
                                (r, g, b) => Color.FromArgb(Convert.ToInt32(r), Convert.ToInt32(g), Convert.ToInt32(b)))
                .Select(color => color)
                .ToProperty(this, x => x.TextColor, out _textColor);

            // Subscribing
            this.WhenAnyValue(x => x.TextColor)
                .Subscribe(x => Console.WriteLine(x.ToString()));

            // ReactiveCommand.CanExecute observable
            var canCreateUser = this.WhenAnyValue(
                x => x.Username, x => x.Password,
                (user, pass) =>
                    !string.IsNullOrWhiteSpace(user) &&
                    !string.IsNullOrWhiteSpace(pass) &&
                    user.Length >= 3 &&
                    pass.Length >= 8)
                .DistinctUntilChanged();
            CreateUserCommand = ReactiveCommand.CreateFromTask<(string, string), bool>(async (u, p) =>
            {
                Console.WriteLine($"CreateUserCommand start... Username:{u} Password:{p}");
                await Task.Delay(2000);
                Console.WriteLine($"CreateUserCommand end... Username:{u} Password:{p}");
                return true;
            }, canCreateUser);

            // Invoking commands
            this.WhenAnyValue(x => x.Username, x => x.Password)
                .Where(x => !String.IsNullOrWhiteSpace(x.Item1) && !String.IsNullOrWhiteSpace(x.Item2))
                .Throttle(TimeSpan.FromMilliseconds(800))
                .InvokeCommand(CreateUserCommand);

            // WhenAnyObservable???
            Document = new Document();
            this.WhenAnyObservable(x => x.Document.IsSaved).Subscribe(x => Console.WriteLine($"Document Saved: {x}"));

            // An asynchronous command created from IObservable<int> that 
            // waits 2 seconds and then returns 42 integer.
            var command = ReactiveCommand.CreateFromObservable<Unit, int>(
                _ => Observable.Return(420).Delay(TimeSpan.FromSeconds(2)));

            // Subscribing to the observable returned by `Execute()` will 
            // tick through the value `420` with a 2-second delay.
            command.Execute(Unit.Default).Subscribe();

            // We can also subscribe to _all_ values that a command
            // emits by using the `Subscribe()` method on the
            // ReactiveCommand itself.
            command.Subscribe(value => Console.WriteLine(value));

            // Create a command with asynchronous execution logic. The 
            // command is always available for execution, it triggers
            // LoadUsersAsync method which returns Task<List<User>>.
            LoadUsers = ReactiveCommand.CreateFromTask(LoadUsersAsync);

            // Update the UI with a new value when users are loaded.
            // ToProperty extension method allows us to subscribe to 
            // LoadUsers Observable, update our OAPH and notify the UI 
            // that the value of Users property has changed.
            _users = LoadUsers.ToProperty(
                this, x => x.Users, scheduler: RxApp.MainThreadScheduler);

            // Here we subscribe to all exceptions thrown by our 
            // command and log them using ReactiveUI logging system.
            // If we forget to do this, our application will crash
            // if anything goes wrong in LoadUsers command.
            LoadUsers.ThrownExceptions.Subscribe(exception =>
            {
                this.Log().Warn("Error!", exception);
            });

            // Invoking commands in an Observable pipeline
            // Creates a hot Observable<T> that emits a new value every 5 
            // minutes and invokes the SaveCommand<Unit, Unit>. Don't forget
            // to dispose the subscription produced by InvokeCommand().
            var interval = TimeSpan.FromMinutes(5);
            Observable.Timer(interval, interval)
                .Select(time => Unit.Default)
                .InvokeCommand(this, x => x.SaveCommand);

            // Combining commands
            var canClearBrowsingHistory = this.WhenAnyValue(x => x.Red, (red) => !string.IsNullOrEmpty(red));
            var canClearDownloadHistory = this.WhenAnyValue(x => x.Green, (green) => !string.IsNullOrEmpty(green));
            var canClearCookies = this.WhenAnyValue(x => x.Blue, (blue) => !string.IsNullOrEmpty(blue));
            var clearBrowsingHistory = ReactiveCommand.CreateFromObservable(
                this.ClearBrowsingHistoryAsync, canClearBrowsingHistory);
            var clearDownloadHistory = ReactiveCommand.CreateFromObservable(
                this.ClearDownloadHistoryAsync, canClearDownloadHistory);
            var clearCookies = ReactiveCommand.CreateFromObservable(
                this.ClearCookiesAsync, canClearCookies);

            // Combine all these commands into one "parent" command.
            // This "parent" command will respect the executability 
            // of all child commands defined above.
            var clearAll = ReactiveCommand.CreateCombined(
                new[] { clearBrowsingHistory,
             clearDownloadHistory,
             clearCookies });

            // Controlling scheduling
            ////???var command1 = ReactiveCommand.Create(() => { }, outputScheduler: someScheduler);
            var command2 = ReactiveCommand.Create(
                () => Console.WriteLine(Environment.CurrentManagedThreadId),
                outputScheduler: RxApp.MainThreadScheduler);

            // This will output the ID of the thread from which you make 
            // this call, not necessarily the ID of the main thread!
            command2.Execute().Subscribe();

            MyCommand = ReactiveCommand.Create<Unit, Unit>((u) =>
            {
                Console.WriteLine($"MyCommand start... ");
                Console.WriteLine($"MyCommand end... ");
                return Unit.Default;
            });

            this.WhenAnyValue(x => x.SomeProperty)
                .InvokeCommand(ReactiveCommand.Create<string, Unit>((s) =>
            {
                Console.WriteLine(s);
                return Unit.Default;
            }));

            this.WhenAnyValue(x => x.SelectedItem)
                .Log(this, "Debugging Observables") // Debugging Observables
                .Subscribe(x => Console.WriteLine($"SelectedItem: {x}"));

            Cancel = Observable.Return(false);
            this.DateTime = new DateTime(2024, 01, 05);

            // Interactions
            this.confirm = new Interaction<string, bool>();
            CommandDeleteFileAsync = ReactiveCommand.CreateFromTask(() => DeleteFileAsync());
            this.MyInteraction = new Interaction<Unit, Unit>();
            var interactionDisposable = new SerialDisposable();
            // Binding Interactions
            this
                .WhenAnyValue(x => x.MyInteraction)
                .Where(x => x != null)
                .Do(x => interactionDisposable.Disposable = x.RegisterHandler(
                    async context => 
                    {
                        await Task.Run(() => Console.WriteLine("MyInteraction(In ViewModel)...."));
                        context.SetOutput(Unit.Default);
                    }))
                .Finally(() => interactionDisposable?.Dispose())
                .Subscribe();

            // Logging via this.Log() and IEnableLogger
            this.Log().Error("IEnableLogger???");
            // Static Logging
            LogHost.Default.Error("LogHost.Default.Error...");

            CredentialsAreValid = true;

            OpenDocuments = new ObservableCollection<DocumentViewModel>();
            // Whenever the list of documents change, calculate a new Observable
            // to represent whenever any of the *current* documents have been
            // requested to close, then Switch to that. When we get something
            // to close, remove it from the list.
            OpenDocuments
                .ToObservableChangeSet()
                .AutoRefreshOnObservable(document => document.Close)
                .Select(_ => WhenAnyDocumentClosed())
                .Switch()
                .Subscribe(x => OpenDocuments.Remove(x));

            Name = "Cris Wu";
            firstName = this
                .WhenAnyValue(x => x.Name)
                .Select(name => name.Split(' ')[0])
                .ToProperty(this, x => x.FirstName);
            this.WhenAnyValue(x => x.Name)
                .Select(name => name.Split(' ')[0])
                .ToProperty(this, x => x.FirstName, out firstName);
            // Performance considerations
            // nameof() instead of using default Index.
            firstName = this
                .WhenAnyValue(x => x.Name)
                .Select(name => name.Split(' ')[0])
                .ToProperty(this, nameof(FirstName));

            // nameStatusObservable is IObservable<string>
            var nameStatusObservable = this
                .WhenAnyValue(x => x.Name1)
                .Select(name => "nameStatusObservable - hahaha!");
            // name is ObservableAsPropertyHelper<string>
            name1 = nameStatusObservable
                .ToProperty(this, nameof(Name1), deferSubscription: true);

            var replayStatus = new ReplaySubject<string>(1);
            // .OnNext() the 'replayStatus' subject somewhere...
            // Note, that 'replayStatus' is also IObservable<string>, 
            // so we are allowed to add a call to .ToProperty() to
            // convert it to ObservableAsPropertyHelper<string>.
            status = replayStatus.ToProperty(this, nameof(Status), deferSubscription: true);

            // Create router for IScreen
            Router = new RoutingState();
            // Create commands
            ShowHomeCommand = ReactiveCommand.Create(ShowHome);
            ShowAboutCommand = ReactiveCommand.Create(ShowAbout);
            ShowContactCommand = ReactiveCommand.Create(ShowContact);
            GoBackCommand = ReactiveCommand.Create(GoBack, Router.NavigateBack.CanExecute);
            // Navigate to HomeViewModel and reset NavigationStack (shows HomeView at application start)
            Router
                .NavigateAndReset
                .Execute(new HomeViewModel())
                .Subscribe();

            // Creates the validation for the Name2 property.
            this.ValidationRule(
                viewModel => viewModel.Name2,
                name => !string.IsNullOrWhiteSpace(name),
                "You must specify a valid name");
            IObservable<bool> passwordsObservable =
                this.WhenAnyValue(
                    x => x.Password,
                    x => x.ConfirmPassword,
                    (password, confirmation) => password == confirmation);
            this.ValidationRule(
                vm => vm.ConfirmPassword,
                passwordsObservable,
                "Passwords must match.");
            // IObservable<{ Password, Confirmation }>
            var passwordsObservableObj =
                this.WhenAnyValue(
                    x => x.Password,
                    x => x.ConfirmPassword,
                    (password, confirmation) =>
                        new { Password = password, Confirmation = confirmation });
            this.ValidationRule(
                vm => vm.ConfirmPassword,
                passwordsObservableObj,
                state => state.Password == state.Confirmation,
                state => $"Passwords must match: {state.Password} != {state.Confirmation}");
            IObservable<IValidationState> usernameNotEmpty =
                this.WhenAnyValue(x => x.UserName)
                    .Select(name => string.IsNullOrEmpty(name)
                        ? new ValidationState(false, "The username must not be empty")
                        : ValidationState.Valid);
            this.ValidationRule(vm => vm.UserName, usernameNotEmpty);

            // firstNameObservable is IObservable<string>
            var firstNameObservable = this.WhenAnyValue(x => x.Name).Select(name => name.Split(' ')[0]);
            firstNameObservable.ToPropertyEx(this, x => x.FirstName1);
            firstNameObservable.ToPropertyEx(this, x => x.FirstName2);

        }

        private async Task<List<User>> LoadUsersAsync()
        {
            await Task.Delay(200);
            return new List<User>()
            {
                new User(){Name = "cgs01", Email="cgs01@qq.com"},
                new User(){Name = "cgs02", Email="cgs02@qq.com"}
            };
        }

        // Here we search NuGet packages using the NuGet.Client library. Ideally, we should
        // extract such code into a separate service, say, INuGetSearchService, but let's 
        // try to avoid overcomplicating things at this time.
        private async Task<IEnumerable<NugetDetailsViewModel>> SearchNuGetPackages(
            string term, CancellationToken token)
        {
            var providers = new List<Lazy<INuGetResourceProvider>>();
            providers.AddRange(Repository.Provider.GetCoreV3()); // Add v3 API support
            var packageSource = new PackageSource("https://api.nuget.org/v3/index.json");
            var source = new SourceRepository(packageSource, providers);
            NuGet.Common.ILogger logger = NuGet.Common.NullLogger.Instance;

            var filter = new SearchFilter(false);
            var resource = await source.GetResourceAsync<PackageSearchResource>().ConfigureAwait(false);
            var metadata = await resource.SearchAsync(term, filter, 0, 10, logger, token).ConfigureAwait(false);
            return metadata.Select(x => new NugetDetailsViewModel(x));
        }

        public ReactiveCommand<(string u, string p), bool> CreateUserCommand { get; }

        // A synchronous command taking a parameter and returning nothing.
        // The Unit type is often used to denote the successfull completion
        // of a void-returning method (C#) or a sub procedure (VB).
        public ReactiveCommand<int, Unit> command = ReactiveCommand.Create<int>(
            integer => Console.WriteLine(integer));

        // Here we declare a ReactiveCommand, an OAPH and a property.
        private readonly ObservableAsPropertyHelper<List<User>> _users;
        public ReactiveCommand<Unit, List<User>> LoadUsers { get; }
        public List<User> Users => _users.Value;

        public ReactiveCommand<Unit, Unit> SaveCommand { get; }

        Func<IObservable<bool>> ClearBrowsingHistoryAsync = () =>
        {
            return Observable.Return<bool>(true).Delay(TimeSpan.FromMilliseconds(500));
        };
        Func<IObservable<bool>> ClearDownloadHistoryAsync = () =>
        {
            return Observable.Return<bool>(true).Delay(TimeSpan.FromMilliseconds(500));
        };
        Func<IObservable<bool>> ClearCookiesAsync = () =>
        {
            return Observable.Return<bool>(true).Delay(TimeSpan.FromMilliseconds(500));
        };

        public ReactiveCommand<Unit, Unit> MyCommand { get; }

        // The ValidationContext contains all of the functionality surrounding the validation of the ViewModel. 
        public ValidationContext ValidationContext { get; } = new ValidationContext();

        // IActivatableViewModel
        public ViewModelActivator Activator { get; }

        public ReactiveCommand<Unit, Unit> someReactiveCommand = ReactiveCommand.CreateFromTask<Unit, Unit>(async (u) =>
        {
            Console.WriteLine($"someReactiveCommand start...");
            await Task.Delay(2000);
            Console.WriteLine($"someReactiveCommand end...");
            return Unit.Default;
        });

        public async Task DeleteFileAsync()
        {
            var fileName = "";

            // this will throw an exception if nothing handles the interaction
            var delete = await this.confirm.Handle(fileName);

            if (delete)
            {
                // delete the file
                Console.WriteLine("DeleteFileAsync executing...");
            }
        }

        public async Task MyInteractionInvoke()
        {
            await this.MyInteraction.Handle(Unit.Default);
        }

        IObservable<DocumentViewModel> WhenAnyDocumentClosed()
        {
            // Select the documents into a list of Observables
            // who return the Document to close when signaled,
            // then flatten them all together.
            return OpenDocuments
                .Select(x => x.Close.Select(_ => x))
                .Merge();
        }

        private void ShowHome()
        {
            // Navigate to HomeViewModel 
            Router
                .Navigate
                .Execute(new HomeViewModel())
                .Subscribe();
        }

        private void ShowAbout()
        {
            // Navigate to AboutViewModel 
            Router
                .Navigate
                .Execute(new AboutViewModel())
                .Subscribe();
        }

        private void ShowContact()
        {
            // Navigate to ContactViewModel 
            Router
                .Navigate
                .Execute(new ContactViewModel())
                .Subscribe();
        }

        private void GoBack()
        {
            // Navigate back in NavigationStack 
            // Note: You have to check the count to prevent an ArgumentOutOfRangeException or navigate to empty
            if (Router.NavigationStack.Count > 0)
            {
                Router
                    .NavigateBack
                    .Execute()
                    .Subscribe();
            }
        }

        private void HandleActivation() { }
        private void HandleDeactivation() { }

    }

    public class Document
    {
        public IObservable<bool> IsSaved { get; }
    }

    public class User
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
