﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.ReactiveUI.WinForms.Demo
{
    // Resolution - Advanced Usage
    // Splat's dependency resolver, accessible using Locator.Current conceptually resembles the below:
    public interface IDependencyResolver
    {
        // Returns the most recent service registered to this type and contract
        T GetService<T>(string contract = null);

        // Returns all of the services registerd to this type and contract
        IEnumerable<T> GetServices<T>(string contract = null);
    }

    // Note If you are willing to use your own DI container, see Override Default Depenedency Inversion Container.
    // Splat includes dependency resolver packages for Autofac, DryIoc, Microsoft.Extensions.DependencyInjection, Ninject, and SimpleInjector.
}
