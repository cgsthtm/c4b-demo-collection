﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace C4B.ReactiveUI.WinForms.Demo
{
    public class ContactViewModel : ReactiveObject, IRoutableViewModel
    {
        [Reactive]
        public string ViewTitle { get; set; }

        public ContactViewModel()
        {
            ViewTitle = "Contact View";
        }

        public string UrlPathSegment { get; protected set; }

        public IScreen HostScreen { get; protected set; }
    }
}