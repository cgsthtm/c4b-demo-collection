﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace C4B.ReactiveUI.WinForms.Demo
{
    public class SomeViewModel : ReactiveObject
    {
        public SomeViewModel()
        {
            // Canceling via Another Observable
            this.CancelableCommand = ReactiveCommand
                .CreateFromObservable(
                    () => Observable
                        .Return(Unit.Default)
                        .Delay(TimeSpan.FromSeconds(3))
                        .TakeUntil(this.CancelCommand));
            // Note At first glance there may appear to be an irresolvable circular dependency between CancelableCommand and CancelCommand.
            // However, note that CancelableCommand does not need to resolve its execution pipeline until it is executed.
            // So as long as CancelCommand exists before
            this.CancelCommand = ReactiveCommand.Create(
                () => { },
                this.CancelableCommand.IsExecuting);
        }

        public ReactiveCommand<Unit, Unit> CancelableCommand { get; }

        public ReactiveCommand<Unit, Unit> CancelCommand { get; }
    }

    public class SomeViewModel1 : ReactiveObject
    {
        public SomeViewModel1()
        {
            // Cancellation with the Task Parallel Library
            this.CancelableCommand = ReactiveCommand
                .CreateFromTask(
                    ct => this.DoSomethingAsync(ct));

            // The above code allows us to do something like this:
            var subscription = this
                .CancelableCommand
                .Execute()
                .Subscribe();

            // This cancels the execution.
            subscription.Dispose();
        }

        public ReactiveCommand<Unit, Unit> CancelableCommand { get; }

        private async Task DoSomethingAsync(CancellationToken ct)
        {
            await Task.Delay(TimeSpan.FromSeconds(3), ct);
        }

        public class SomeViewModel2 : ReactiveObject
        {
            public SomeViewModel2()
            {
                // Besides forgoing TPL completely (which is recommended if possible, but not always practical),
                // there are actually quite a few ways to achieve this.
                // Perhaps the easiest is to use CreateFromObservable instead:
                this.CancelableCommand = ReactiveCommand
                    .CreateFromObservable(
                        () => Observable
                            .StartAsync(ct => this.DoSomethingAsync(ct))
                            .TakeUntil(this.CancelCommand));
                this.CancelCommand = ReactiveCommand.Create(
                    () => { },
                    this.CancelableCommand.IsExecuting);
            }

            public ReactiveCommand<Unit, Unit> CancelableCommand { get; }

            public ReactiveCommand<Unit, Unit> CancelCommand { get; }

            private async Task DoSomethingAsync(CancellationToken ct)
            {
                await Task.Delay(TimeSpan.FromSeconds(3), ct);
            }
        }

    }

}
