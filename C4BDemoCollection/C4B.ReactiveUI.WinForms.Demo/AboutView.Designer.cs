﻿namespace C4B.ReactiveUI.WinForms.Demo
{
    partial class AboutView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lViewTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lViewTitle
            // 
            this.lViewTitle.AutoSize = true;
            this.lViewTitle.Location = new System.Drawing.Point(18, 24);
            this.lViewTitle.Name = "lViewTitle";
            this.lViewTitle.Size = new System.Drawing.Size(58, 13);
            this.lViewTitle.TabIndex = 0;
            this.lViewTitle.Text = "ViewName";
            // 
            // AboutView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lViewTitle);
            this.Name = "AboutView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lViewTitle;
    }
}
