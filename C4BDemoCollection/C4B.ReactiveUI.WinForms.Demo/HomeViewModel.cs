﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.ReactiveUI.WinForms.Demo
{
    public class HomeViewModel : ReactiveObject, IRoutableViewModel
    {
        [Reactive]
        public string ViewTitle { get; set; }

        public HomeViewModel() 
        {
            ViewTitle = "Home View";
        }

        public string UrlPathSegment { get; set; }

        public IScreen HostScreen { get; set; }
    }
}
