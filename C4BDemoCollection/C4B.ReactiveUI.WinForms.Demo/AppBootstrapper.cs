﻿using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C4B.ReactiveUI.WinForms.Demo
{
    // It is recommended to register your dependencies all at one place. This can be achieved for example with an AppBootstrapper.
    public class AppBootstrapper : IEnableLogger
    {
        public AppBootstrapper()
        {
            Locator.CurrentMutable.RegisterConstant(new FeedService(), typeof(IFeedService));
            // Other registrations go here...

            // Register views
            Locator.CurrentMutable.Register(() => new Form1(), typeof(IViewFor<AppViewModel>));
            Locator.CurrentMutable.Register(() => new HomeView(), typeof(IViewFor<HomeViewModel>));
            Locator.CurrentMutable.Register(() => new AboutView(), typeof(IViewFor<AboutViewModel>));
            Locator.CurrentMutable.Register(() => new ContactView(), typeof(IViewFor<ContactViewModel>));
        }
    }

}
