﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.ReactiveUI.WinForms.Demo
{
    public class StudentService : IStudentService
    {
        public void Login(string username, string password)
        {
            Console.WriteLine($"StudentService.Login({username}, {password})");
        }
    }
}
