﻿using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace C4B.ReactiveUI.WinForms.Demo
{
    [DataContract]
    public class SearchViewModel : ReactiveObject////, ISearchViewModel
    {
        private readonly ObservableAsPropertyHelper<IEnumerable<SearchResults>> _searchResults;
        private readonly ISearchService searchService;
        private string searchQuery;

        public SearchViewModel(ISearchService searchService = null)
        {
            this.searchService = searchService ?? Locator.Current.GetService<ISearchService>();

            var canSearch = this
                .WhenAnyValue(x => x.SearchQuery)
                .Select(query => !string.IsNullOrWhiteSpace(query));

            Search = ReactiveCommand.CreateFromTask(
                () => this.searchService.Search(SearchQuery),
                canSearch);

            _searchResults = Search.ToProperty(this, x => x.SearchResults);
        }

        public IEnumerable<SearchResults> SearchResults => _searchResults.Value;

        public ReactiveCommand<Unit, IEnumerable<SearchResults>> Search { get; }

        [DataMember]
        public string SearchQuery
        {
            get => searchQuery;
            set => this.RaiseAndSetIfChanged(ref searchQuery, value);
        }
    }

}
