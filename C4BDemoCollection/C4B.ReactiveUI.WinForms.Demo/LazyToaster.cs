﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.ReactiveUI.WinForms.Demo
{
    public class LazyToaster : IToaster
    {
        public void Toast()
        {
            Console.WriteLine("LazyToaster.Toast()...");
        }
    }
}
