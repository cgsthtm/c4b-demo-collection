﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.ReactiveUI.WinForms.Demo
{
    public interface ISearchService
    {
        Task<IEnumerable<SearchResults>> Search(string searchQuery);
    }
}
