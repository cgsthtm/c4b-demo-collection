﻿namespace C4B.ReactiveUI.WinForms.Demo
{
    partial class NugetDetailsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.iconImage = new System.Windows.Forms.PictureBox();
            this.descriptionRun = new System.Windows.Forms.Label();
            this.titleRun = new System.Windows.Forms.Label();
            this.openButton = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.iconImage)).BeginInit();
            this.SuspendLayout();
            // 
            // iconImage
            // 
            this.iconImage.Location = new System.Drawing.Point(22, 25);
            this.iconImage.Name = "iconImage";
            this.iconImage.Size = new System.Drawing.Size(100, 50);
            this.iconImage.TabIndex = 0;
            this.iconImage.TabStop = false;
            // 
            // descriptionRun
            // 
            this.descriptionRun.AutoSize = true;
            this.descriptionRun.Location = new System.Drawing.Point(128, 48);
            this.descriptionRun.Name = "descriptionRun";
            this.descriptionRun.Size = new System.Drawing.Size(78, 13);
            this.descriptionRun.TabIndex = 1;
            this.descriptionRun.Text = "descriptionRun";
            // 
            // titleRun
            // 
            this.titleRun.AutoSize = true;
            this.titleRun.Location = new System.Drawing.Point(131, 32);
            this.titleRun.Name = "titleRun";
            this.titleRun.Size = new System.Drawing.Size(43, 13);
            this.titleRun.TabIndex = 2;
            this.titleRun.Text = "titleRun";
            // 
            // openButton
            // 
            this.openButton.AutoSize = true;
            this.openButton.Location = new System.Drawing.Point(131, 62);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(31, 13);
            this.openButton.TabIndex = 3;
            this.openButton.TabStop = true;
            this.openButton.Text = "open";
            // 
            // NugetDetailsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.titleRun);
            this.Controls.Add(this.descriptionRun);
            this.Controls.Add(this.iconImage);
            this.Name = "NugetDetailsView";
            this.Size = new System.Drawing.Size(412, 104);
            ((System.ComponentModel.ISupportInitialize)(this.iconImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox iconImage;
        private System.Windows.Forms.Label descriptionRun;
        private System.Windows.Forms.Label titleRun;
        private System.Windows.Forms.LinkLabel openButton;
    }
}
