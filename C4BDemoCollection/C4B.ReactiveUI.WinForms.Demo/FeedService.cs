﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.ReactiveUI.WinForms.Demo
{
    public class FeedService : IFeedService
    {
        public void Feed()
        {
            Console.WriteLine("FeedService.Feed()...");
        }
    }
}
