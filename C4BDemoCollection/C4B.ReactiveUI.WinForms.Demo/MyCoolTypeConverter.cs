﻿using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.ReactiveUI.WinForms.Demo
{
    // Value Converters
    // A value converter should implement the IBindingTypeConverter interface. 
    public class MyCoolTypeConverter : IBindingTypeConverter
    {
        public int GetAffinityForObjects(Type fromType, Type toType)
        {
            if (fromType == typeof(string))
            {
                return 100; // any number other than 0 signifies conversion is possible.
            }
            return 0;
        }

        public bool TryConvert(object from, Type toType, object conversionHint, out object result)
        {
            try
            {
                result = !string.IsNullOrWhiteSpace((string)from);
            }
            catch (Exception ex)
            {
                this.Log().Warn(ex, "Couldn't convert object to type: " + toType);
                result = null;
                return false;
            }

            return true;
        }
    }

    public class CustomTypeConverter : IBindingTypeConverter
    {
        public int GetAffinityForObjects(Type fromType, Type toType)
        {
            if (fromType == typeof(bool))
            {
                return 100; // any number other than 0 signifies conversion is possible.
            }
            return 0;
        }

        public bool TryConvert(object from, Type toType, object conversionHint, out object result)
        {
            try
            {
                result = !string.IsNullOrWhiteSpace((string)from);
            }
            catch (Exception ex)
            {
                this.Log().Warn(ex, "Couldn't convert object to type: " + toType);
                result = null;
                return false;
            }

            return true;
        }
    }
}
