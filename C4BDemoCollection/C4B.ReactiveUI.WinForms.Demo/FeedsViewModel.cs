﻿using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.ReactiveUI.WinForms.Demo
{
    public class FeedsViewModel : ReactiveObject
    {
        public IFeedService FeedService { get; set; }

        // Resolution
        // Recommended usage is:
        public FeedsViewModel(IFeedService feedService = null)
        {
            FeedService = feedService ?? Locator.Current.GetService<IFeedService>();
        }
    }
}
