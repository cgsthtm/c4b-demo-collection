﻿using ReactiveUIViewLocator = ReactiveUI.IViewLocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReactiveUI;
using Splat;

namespace C4B.ReactiveUI.WinForms.Demo
{
    // View Location
    // Overriding ViewLocator
    public class SimpleViewLocator : ReactiveUIViewLocator
    {
        global::ReactiveUI.IViewFor ReactiveUIViewLocator.ResolveView<T>(T viewModel, string contract) where T : default
        {
            // Find view's by chopping of the 'Model' on the view model name
            // MyApp.ShellViewModel => MyApp.ShellView
            var viewModelName = viewModel.GetType().FullName;
            var viewTypeName = viewModelName.TrimEnd("Model".ToCharArray());
            try
            {
                var viewType = Type.GetType(viewTypeName);
                if (viewType == null)
                {
                    this.Log().Error($"Could not find the view {viewTypeName} for view model {viewModelName}.");
                    return null;
                }
                return Activator.CreateInstance(viewType) as IViewFor;
            }
            catch (Exception)
            {
                this.Log().Error($"Could not instantiate view {viewTypeName}.");
                throw;
            }

            ////if (viewModel is HomeViewModel)
            ////    return new HomeView { ViewModel = viewModel as HomeViewModel };
            ////if (viewModel is AboutViewModel)
            ////    return new AboutView { ViewModel = viewModel as AboutViewModel };
            ////if (viewModel is ContactViewModel)
            ////    return new ContactView { ViewModel = viewModel as ContactViewModel };
            ////if (viewModel is AppViewModel)
            ////    return new Form1 { ViewModel = viewModel as AppViewModel };
            ////throw new Exception($"Could not find the view for view model {typeof(T).Name}.");
        }
    }
}
