﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace C4B.ReactiveUI.WinForms.Demo
{
    public class AboutViewModel : ReactiveObject, IRoutableViewModel
    {
        [Reactive]
        public string ViewTitle { get; set; }

        public AboutViewModel() 
        {
            ViewTitle = "About View";
        }

        public string UrlPathSegment { get; protected set; }

        public IScreen HostScreen { get; protected set; }
    }
}