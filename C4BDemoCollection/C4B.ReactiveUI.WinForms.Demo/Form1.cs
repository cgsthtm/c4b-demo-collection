﻿using Autofac.Util;
using DevExpress.Utils.Helpers;
using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using static System.Runtime.CompilerServices.RuntimeHelpers;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolBar;

namespace C4B.ReactiveUI.WinForms.Demo
{
    public partial class Form1 : Form, IViewFor<AppViewModel>
    {
        public Form1()
        {
            InitializeComponent();

            ViewModel = new AppViewModel();

            // Shutdown the painting of the ListBox as items are added.
            SomeList.BeginUpdate();

            // Loop through and add 50 items to the ListBox.
            for (int x = 0; x < 50; x++)
            {
                SomeList.Items.Add("Item " + x.ToString());
            }

            // Allow the ListBox to repaint and display the new items.
            SomeList.EndUpdate();

            // We create our bindings here. These are the code behind bindings which allow 
            // type safety. The bindings will only become active when the Window is being shown.
            // We register our subscription in our disposableRegistration, this will cause 
            // the binding subscription to become inactive when the Window is closed.
            // The disposableRegistration is a CompositeDisposable which is a container of 
            // other Disposables. We use the DisposeWith() extension method which simply adds 
            // the subscription disposable to the CompositeDisposable.
            this.WhenActivated(async disposableRegistration =>
            {
                // If you put the WhenActivated block into your IViewFor
                // implementation constructor, the view model will also
                // get activated if it implements IActivatableViewModel.

                // Notice we don't have to provide a converter, on WPF a global converter is
                // registered which knows how to convert a boolean into visibility.
                this.OneWayBind(ViewModel,
                    viewModel => viewModel.IsAvailable,
                    view => view.searchResultsListBox.Visible)
                    .DisposeWith(disposableRegistration);

                this.OneWayBind(ViewModel,
                    viewModel => viewModel.SearchResults,
                    view => view.searchResultsListBox.DataSource)
                    .DisposeWith(disposableRegistration);

                this.Bind(ViewModel,
                    viewModel => viewModel.SearchTerm,
                    view => view.searchTextBox.Text)
                    .DisposeWith(disposableRegistration);

                this.OneWayBind(ViewModel,
                    viewModel => viewModel.TextColor,
                    view => view.lColor.ForeColor)
                    .DisposeWith(disposableRegistration);

                this.Bind(ViewModel,
                    viewModel => viewModel.Red,
                    view => view.txRed.Text)
                    .DisposeWith(disposableRegistration);

                this.Bind(ViewModel,
                    viewModel => viewModel.Green,
                    view => view.txGreen.Text)
                    .DisposeWith(disposableRegistration);

                this.Bind(ViewModel,
                    viewModel => viewModel.Blue,
                    view => view.txBlue.Text)
                    .DisposeWith(disposableRegistration);

                this.Bind(ViewModel,
                    vm => vm.Username,
                    v => v.txUsername.Text)
                    .DisposeWith(disposableRegistration);

                this.Bind(ViewModel,
                    vm => vm.Password,
                    v => v.txPassword.Text)
                    .DisposeWith(disposableRegistration);

                // Performing view-specific transforms as an input to BindTo
                ViewModel.WhenAnyValue(x => x.ShowToolTip)
                    .Select(show => show ? 1f : 0f)
                    .BindTo(this, x => x.ToolTipLabel.Text)
                    .DisposeWith(disposableRegistration);
                // The preferable option is to use the OneWayBind property to perform the binding.
                ////this.OneWayBind(this.ViewModel, vm => vm.ShowToolTip, view => view.ToolTipLabel.Text);

                // Binding Commands
                // In a view
                this.BindCommand(
                    this.ViewModel,
                    vm => vm.MyCommand,
                    v => v.myControl,
                    nameof(myControl.Click))
                    .DisposeWith(disposableRegistration);
                // Note: The above example shows a naked call to BindCommand; however, BindCommand will often be called in a WhenActivated block:
                ////this.WhenActivated(
                ////    d =>
                ////    {
                ////        d(this.BindCommand(
                ////            this.ViewModel,
                ////            vm => vm.MyCommand,
                ////            v => v.myControl));
                ////    });

                // Converting between types
                // Note: Age is an integer, Text is a string
                this.OneWayBind(ViewModel,
                    viewModel => viewModel.Age,
                    view => view.Age.Text,
                    value => value.ToString())  // In the last parameter, we .ToString() the integer
                .DisposeWith(disposableRegistration);
                this.OneWayBind(ViewModel,
                    viewModel => viewModel.Age,
                    view => view.lAge.Text,
                    value => value.ToString())
                .DisposeWith(disposableRegistration);

                // Choosing when to update the source???
                // Note: We're using the ReactiveUI.Events NuGet package here, which 
                // wraps traditional .NET events on UI controls into IObservables and 
                // exposes them via the Events() extension method.
                ////this.Bind(ViewModel,
                ////    viewModel => viewModel.SomeProperty,
                ////    view => view.SomeTextBox,
                ////    SomeTextBox.Events().LostKeyboardFocus);

                // Why not use Subscribe?
                // Although both are similar, BindTo does offer a number of benefits:
                // In general BindTo is the recommended approach to binding. 
                //1. Using BindTo
                ViewModel.WhenAnyValue(x => x.Name)
                         .Select(x => $"Label: {x}")
                         .BindTo(this, x => x.loginButton.Text)
                         .DisposeWith(disposableRegistration);
                //2. Using Subscribe
                ViewModel.WhenAnyValue(x => x.Name)
                         .Select(x => $"Label: {x}")
                         .Subscribe(x => loginButton.Text = x)
                         .DisposeWith(disposableRegistration);

                // Inline Binding Converters
                this.Bind(this.ViewModel,
                    viewModel => viewModel.DateTime,
                    view => view.DateTextBox.Text,
                    this.ViewModelToViewConverterFunc,
                    this.ViewToViewModelConverterFunc)
                    .DisposeWith(disposableRegistration);

                // Events
                var codes = new[]
                {
                    Key.Up,
                    Key.Up,
                    Key.Down,
                    Key.Down,
                    Key.Left,
                    Key.Right,
                    Key.Left,
                    Key.Right,
                    Key.A,
                    Key.B
                };
                // convert the array into an sequence
                var koanmi = codes.ToObservable();
                ////this.Events().KeyUp
                ////    // we want the keycode
                ////    .Select(x => x.Key)
                ////    .Do(key => Debug.WriteLine($"{key} was pressed."))

                ////    // get the last ten keys
                ////    .Window(10)

                ////    // compare to known konami code sequence
                ////    .SelectMany(x => x.SequenceEqual(koanmi))
                ////    .Do(isMatch => Debug.WriteLine(isMatch))

                ////    // where we match
                ////    .Where(x => x)
                ////    .Do(x => Debug.WriteLine("Konami sequence"))
                ////    .Subscribe(y => { });
                ///

                // Using events with WhenActivated
                ////RefreshButton
                ////  // observe button click events
                ////  // namespace: System.Windows.Controls.Primitives
                ////  .Events().Click
                ////  // transform arguments
                ////  .Select(args => Unit.Default)
                ////  // invoke command when button is clicked
                ////  .InvokeCommand(this, x => x.ViewModel.Refresh)
                ////            // dispose subscription when the view
                ////            // gets deactivated.
                ////  .DisposeWith(disposables);

                // How do I convert my own C# events into Observables?
                var refreshButObs = Observable.FromEventPattern(RefreshButton, nameof(RefreshButton.Click));

                this.BindCommand(
                    this.ViewModel,
                    vm => vm.CommandDeleteFileAsync,
                    v => v.DeleteFileAsync,
                    nameof(DeleteFileAsync.Click))
                    .DisposeWith(disposableRegistration);
                this
                    .ViewModel
                    .Confirm
                    .RegisterHandler(
                        async interaction =>
                        {
                            var deleteIt = await Task.Run(() => MessageBox.Show(null,
                                "Confirm Delete",
                                $"Are you sure you want to delete '{interaction.Input}'?",
                                MessageBoxButtons.YesNo));

                            interaction.SetOutput(deleteIt == DialogResult.Yes);
                        })
                    .DisposeWith(disposableRegistration);
                this.BindInteraction(
                    this.ViewModel,
                    vm => vm.MyInteraction,
                    context => Task.Run(() =>
                    {
                        Console.WriteLine("MyInteraction(In View)....");
                        context.SetOutput(Unit.Default);
                    })
                .DisposeWith(disposableRegistration));
                await this.ViewModel.MyInteractionInvoke();

                // Message Bus - The Basics
                // Listen for anyone sending instances of the KeyUpEventArgs class. Since
                // MessageBus simply returns an IObservable, it can be combined or used in
                // many different ways
                MessageBus.Current.Listen<System.Windows.Forms.KeyEventArgs>()
                    .Where(e => e.KeyCode == Keys.Up)
                    .Subscribe(x => Console.WriteLine("Up Pressed!"))
                    .DisposeWith(disposableRegistration);
                // Now, connect an IObservable to the bus via RegisterMessageSource:???
                ////MessageBus.Current.RegisterMessageSource();
                // Or, if you're feeling very imperative and not very Functional:
                MessageBus.Current.SendMessage(new System.Windows.Forms.KeyEventArgs(Keys.Up));

                // Message Bus - Ways to avoid using MessageBus
                // As soon as the CredentialsAreValid turns to 'true', set the focus
                // to the Ok button.
                this.WhenAny(x => x.ViewModel.CredentialsAreValid, x => x.Value)
                    .Where(x => x != false)
                    .Subscribe(_ => OkButton.Focus());

                // Bind router
                this.OneWayBind(ViewModel, vm => vm.Router, v => v.routedControlHost1.Router)
                    .DisposeWith(disposableRegistration);
                // Bind commands
                this.BindCommand(ViewModel, vm => vm.ShowHomeCommand, v => v.btHome, nameof(btHome.Click))
                        .DisposeWith(disposableRegistration);
                this.BindCommand(ViewModel, vm => vm.ShowAboutCommand, v => v.btAbout, nameof(btAbout.Click))
                        .DisposeWith(disposableRegistration);
                this.BindCommand(ViewModel, vm => vm.ShowContactCommand, v => v.btContact, nameof(btContact.Click))
                        .DisposeWith(disposableRegistration);
                this.BindCommand(ViewModel, vm => vm.GoBackCommand, v => v.btGoBack, nameof(btGoBack.Click))
                        .DisposeWith(disposableRegistration);

                this.Bind(ViewModel, vm => vm.Name2,v => v.txName2.Text)
                    .DisposeWith(disposableRegistration);
                this.Bind(ViewModel, vm => vm.ConfirmPassword, v => v.txConfirmPassword.Text)
                    .DisposeWith(disposableRegistration);
                this.Bind(ViewModel, vm => vm.UserName, v => v.txUserName1.Text)
                    .DisposeWith(disposableRegistration);
                // Bind any validations that reference the Name property
                // to the text of the NameError UI control.
                this.BindValidation(ViewModel, vm => vm.Name2, view => view.lName2ErrorText.Text)
                    .DisposeWith(disposableRegistration);
                // Bind any validations attached to this particular view model
                // to the text of the FormErrors UI control.
                this.BindValidation(ViewModel, view => view.lFormErrorText.Text)
                    .DisposeWith(disposableRegistration);

                this.OneWayBind(ViewModel, vm => vm.FirstName1, v => v.txFirstName1.Text)
                    .DisposeWith(disposableRegistration);

            });

            // WhenAny???
            ////this.WhenAny(x => x.ComboBox.SelectedItem).Subscribe(x => Console.WriteLine($"The {x.Sender} changed value to {x.Value}"));

            dataGridView1.DataSource = new List<GridView1Source>
            {
                new GridView1Source() { Url = "111", Name = "111" },
                new GridView1Source() { Url = "222", Name = "222" }
            };

            // This outputs: 42
            ViewModel.command.Execute(42).Subscribe();

            // Basic Cancelation
            // This cancels the command's execution.
            var subscription = ViewModel.someReactiveCommand.Execute().Subscribe();
            subscription.Dispose();

            // Canceling via Another Observable
            var cancel = new Subject<Unit>();
            var command = ReactiveCommand
                .CreateFromObservable(
                    () => Observable
                        .Return(Unit.Default)
                        .Delay(TimeSpan.FromSeconds(3))
                        .TakeUntil(cancel));

            // Somewhere else.
            command.Execute().Subscribe();

            // This cancels the above execution.
            cancel.OnNext(Unit.Default);

            // Select three items from the ListBox.
            SomeList.SetSelected(1, true);
            SomeList.SetSelected(3, true);
            SomeList.SetSelected(5, true);
            SomeList.SetSelected(7, true);
            SomeList.SetSelected(9, true);

            // "Hack" bindings and BindTo???
            // Bind the View's SelectedItem to the ViewModel
            this.WhenAnyValue(x => x.SomeList.SelectedItem)
                .BindTo(this, x => x.ViewModel.SelectedItem);
            // Bind ViewModel's IsSelected via SelectedItem. Note that this
            // is only for illustrative purposes, it'd be better to bind this
            // at the ViewModel layer (i.e. WhenAnyValue + ToProperty)
            this.WhenAnyValue(x => x.SomeList.SelectedItem)
                .Select(x => x != null)
                .BindTo(this, x => x.ViewModel.IsSelected);

            // "Hack" Command Bindings
            // Invoke a command whenever the Escape key is pressed???
            ////this.Events().KeyUpObs
            ////    .Where(x => x.EventArgs.Key == Key.Escape)
            ////    .InvokeCommand(this, x => x.ViewModel.Cancel);
            // Subscribe to Cancel, and close the Window when it happens
            this.WhenAnyObservable(x => x.ViewModel.Cancel)
                .Where(x => x == true)
                .Subscribe(x => this.Close());

            // Usage - Value Converter
            this.Bind(ViewModel,
                viewModel => viewModel.ViewModelProperty,
                view => view.Control.Enabled,
                viewToVMConverterOverride: new CustomTypeConverter());

            ViewModel.OpenDocuments.Add(new DocumentViewModel());
            ViewModel.OpenDocuments.Add(new DocumentViewModel());
            ViewModel.OpenDocuments.Add(new DocumentViewModel());
            ViewModel.OpenDocuments.Add(new DocumentViewModel());
            ViewModel.OpenDocuments.First().Close.Execute().Subscribe();

        }

        public AppViewModel ViewModel { get; set; }
        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (AppViewModel)value;
        }

        public class GridView1Source
        {
            public string Url { get; set; }
            public string Name { get; set; }
        }

        private void SetSelectedClick(object sender, EventArgs e)
        {
            SomeList.SetSelected(10, true);
            Console.WriteLine($"SetSelectedClick: {SomeList.SelectedItem}");
            Console.WriteLine($"SetSelectedClick: {SomeList.SelectedItem}");
        }

        private void SomeListSelectedValueChanged(object sender, EventArgs e)
        {
            Console.WriteLine($"SomeListSelectedValueChanged: {SomeList.SelectedItem}");
            Console.WriteLine($"ViewModel.SelectedItem: {ViewModel.SelectedItem}");
        }

        private void btCancelClick(object sender, EventArgs e)
        {
            ViewModel.Cancel = Observable.Return(true);
        }

        private void btNoCancelClick(object sender, EventArgs e)
        {
            ViewModel.Cancel = Observable.Return(false);
        }

        private string ViewModelToViewConverterFunc(DateTime dateTime)
        {
            return dateTime.ToString("O"); // return ISO 8601 Date ime
        }

        private DateTime ViewToViewModelConverterFunc(string value)
        {
            DateTime.TryParse(value, out DateTime returnValue);
            return returnValue;
        }
    }
}
