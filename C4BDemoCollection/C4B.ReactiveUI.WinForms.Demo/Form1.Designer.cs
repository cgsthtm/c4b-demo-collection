﻿using ReactiveUIWinForm = ReactiveUI.Winforms;

namespace C4B.ReactiveUI.WinForms.Demo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.searchResultsListBox = new System.Windows.Forms.DataGridView();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lColor = new System.Windows.Forms.Label();
            this.txBlue = new System.Windows.Forms.TextBox();
            this.lBlue = new System.Windows.Forms.Label();
            this.txGreen = new System.Windows.Forms.TextBox();
            this.lGreen = new System.Windows.Forms.Label();
            this.txRed = new System.Windows.Forms.TextBox();
            this.lRed = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txPassword = new System.Windows.Forms.TextBox();
            this.txUsername = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ToolTipLabel = new System.Windows.Forms.Label();
            this.ComboBox = new System.Windows.Forms.ComboBox();
            this.myControl = new System.Windows.Forms.Button();
            this.Age = new System.Windows.Forms.TextBox();
            this.lAge = new System.Windows.Forms.Label();
            this.SomeTextBox = new System.Windows.Forms.TextBox();
            this.loginButton = new System.Windows.Forms.Button();
            this.SomeList = new System.Windows.Forms.ListBox();
            this.SetSelected = new System.Windows.Forms.Button();
            this.Control = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.btNoCancel = new System.Windows.Forms.Button();
            this.DateTextBox = new System.Windows.Forms.TextBox();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.DeleteFileAsync = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.routedControlHost1 = new global::ReactiveUI.Winforms.RoutedControlHost();
            this.btHome = new System.Windows.Forms.Button();
            this.btAbout = new System.Windows.Forms.Button();
            this.btContact = new System.Windows.Forms.Button();
            this.btGoBack = new System.Windows.Forms.Button();
            this.txName2 = new System.Windows.Forms.TextBox();
            this.txConfirmPassword = new System.Windows.Forms.TextBox();
            this.txUserName1 = new System.Windows.Forms.TextBox();
            this.lName2ErrorText = new System.Windows.Forms.Label();
            this.lFormErrorText = new System.Windows.Forms.Label();
            this.txFirstName1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.searchResultsListBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Search for: ";
            // 
            // searchTextBox
            // 
            this.searchTextBox.Location = new System.Drawing.Point(95, 28);
            this.searchTextBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(432, 25);
            this.searchTextBox.TabIndex = 1;
            // 
            // searchResultsListBox
            // 
            this.searchResultsListBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.searchResultsListBox.Location = new System.Drawing.Point(8, 58);
            this.searchResultsListBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.searchResultsListBox.Name = "searchResultsListBox";
            this.searchResultsListBox.Size = new System.Drawing.Size(321, 377);
            this.searchResultsListBox.TabIndex = 2;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(337, 58);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(191, 377);
            this.dataGridView1.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.searchResultsListBox);
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.searchTextBox);
            this.groupBox1.Location = new System.Drawing.Point(16, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Size = new System.Drawing.Size(540, 443);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lColor);
            this.groupBox2.Controls.Add(this.txBlue);
            this.groupBox2.Controls.Add(this.lBlue);
            this.groupBox2.Controls.Add(this.txGreen);
            this.groupBox2.Controls.Add(this.lGreen);
            this.groupBox2.Controls.Add(this.txRed);
            this.groupBox2.Controls.Add(this.lRed);
            this.groupBox2.Location = new System.Drawing.Point(565, 15);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Size = new System.Drawing.Size(485, 60);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // lColor
            // 
            this.lColor.AutoSize = true;
            this.lColor.Location = new System.Drawing.Point(325, 23);
            this.lColor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lColor.Name = "lColor";
            this.lColor.Size = new System.Drawing.Size(47, 15);
            this.lColor.TabIndex = 6;
            this.lColor.Text = "color";
            // 
            // txBlue
            // 
            this.txBlue.Location = new System.Drawing.Point(251, 20);
            this.txBlue.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBlue.Name = "txBlue";
            this.txBlue.Size = new System.Drawing.Size(52, 25);
            this.txBlue.TabIndex = 5;
            this.txBlue.Text = "251";
            // 
            // lBlue
            // 
            this.lBlue.AutoSize = true;
            this.lBlue.Location = new System.Drawing.Point(213, 23);
            this.lBlue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lBlue.Name = "lBlue";
            this.lBlue.Size = new System.Drawing.Size(39, 15);
            this.lBlue.TabIndex = 4;
            this.lBlue.Text = "blue";
            // 
            // txGreen
            // 
            this.txGreen.Location = new System.Drawing.Point(148, 20);
            this.txGreen.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txGreen.Name = "txGreen";
            this.txGreen.Size = new System.Drawing.Size(52, 25);
            this.txGreen.TabIndex = 3;
            this.txGreen.Text = "153";
            // 
            // lGreen
            // 
            this.lGreen.AutoSize = true;
            this.lGreen.Location = new System.Drawing.Point(111, 23);
            this.lGreen.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lGreen.Name = "lGreen";
            this.lGreen.Size = new System.Drawing.Size(31, 15);
            this.lGreen.TabIndex = 2;
            this.lGreen.Text = "gre";
            // 
            // txRed
            // 
            this.txRed.Location = new System.Drawing.Point(47, 20);
            this.txRed.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txRed.Name = "txRed";
            this.txRed.Size = new System.Drawing.Size(52, 25);
            this.txRed.TabIndex = 1;
            this.txRed.Text = "207";
            // 
            // lRed
            // 
            this.lRed.AutoSize = true;
            this.lRed.Location = new System.Drawing.Point(9, 23);
            this.lRed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lRed.Name = "lRed";
            this.lRed.Size = new System.Drawing.Size(31, 15);
            this.lRed.TabIndex = 0;
            this.lRed.Text = "red";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txPassword);
            this.groupBox3.Controls.Add(this.txUsername);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(565, 83);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Size = new System.Drawing.Size(485, 66);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // txPassword
            // 
            this.txPassword.Location = new System.Drawing.Point(291, 25);
            this.txPassword.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txPassword.Name = "txPassword";
            this.txPassword.Size = new System.Drawing.Size(123, 25);
            this.txPassword.TabIndex = 10;
            this.txPassword.Text = "153";
            // 
            // txUsername
            // 
            this.txUsername.Location = new System.Drawing.Point(87, 25);
            this.txUsername.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txUsername.Name = "txUsername";
            this.txUsername.Size = new System.Drawing.Size(113, 25);
            this.txUsername.TabIndex = 8;
            this.txUsername.Text = "207";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(213, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 15);
            this.label2.TabIndex = 9;
            this.label2.Text = "password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 29);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "username";
            // 
            // ToolTipLabel
            // 
            this.ToolTipLabel.AutoSize = true;
            this.ToolTipLabel.Location = new System.Drawing.Point(565, 157);
            this.ToolTipLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ToolTipLabel.Name = "ToolTipLabel";
            this.ToolTipLabel.Size = new System.Drawing.Size(103, 15);
            this.ToolTipLabel.TabIndex = 7;
            this.ToolTipLabel.Text = "ToolTipLabel";
            // 
            // ComboBox
            // 
            this.ComboBox.FormattingEnabled = true;
            this.ComboBox.Location = new System.Drawing.Point(665, 153);
            this.ComboBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ComboBox.Name = "ComboBox";
            this.ComboBox.Size = new System.Drawing.Size(160, 23);
            this.ComboBox.TabIndex = 8;
            // 
            // myControl
            // 
            this.myControl.Location = new System.Drawing.Point(835, 151);
            this.myControl.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.myControl.Name = "myControl";
            this.myControl.Size = new System.Drawing.Size(100, 27);
            this.myControl.TabIndex = 9;
            this.myControl.Text = "myControl";
            this.myControl.UseVisualStyleBackColor = true;
            // 
            // Age
            // 
            this.Age.Location = new System.Drawing.Point(569, 185);
            this.Age.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Age.Name = "Age";
            this.Age.Size = new System.Drawing.Size(123, 25);
            this.Age.TabIndex = 11;
            this.Age.Text = "Age";
            // 
            // lAge
            // 
            this.lAge.AutoSize = true;
            this.lAge.Location = new System.Drawing.Point(701, 188);
            this.lAge.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lAge.Name = "lAge";
            this.lAge.Size = new System.Drawing.Size(31, 15);
            this.lAge.TabIndex = 12;
            this.lAge.Text = "Age";
            // 
            // SomeTextBox
            // 
            this.SomeTextBox.Location = new System.Drawing.Point(783, 185);
            this.SomeTextBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.SomeTextBox.Name = "SomeTextBox";
            this.SomeTextBox.Size = new System.Drawing.Size(123, 25);
            this.SomeTextBox.TabIndex = 13;
            this.SomeTextBox.Text = "SomeTextBox";
            // 
            // loginButton
            // 
            this.loginButton.Location = new System.Drawing.Point(915, 182);
            this.loginButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(100, 27);
            this.loginButton.TabIndex = 14;
            this.loginButton.Text = "loginButton";
            this.loginButton.UseVisualStyleBackColor = true;
            // 
            // SomeList
            // 
            this.SomeList.FormattingEnabled = true;
            this.SomeList.ItemHeight = 15;
            this.SomeList.Location = new System.Drawing.Point(569, 216);
            this.SomeList.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.SomeList.Name = "SomeList";
            this.SomeList.Size = new System.Drawing.Size(159, 109);
            this.SomeList.TabIndex = 15;
            this.SomeList.SelectedValueChanged += new System.EventHandler(this.SomeListSelectedValueChanged);
            // 
            // SetSelected
            // 
            this.SetSelected.Location = new System.Drawing.Point(737, 216);
            this.SetSelected.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.SetSelected.Name = "SetSelected";
            this.SetSelected.Size = new System.Drawing.Size(100, 27);
            this.SetSelected.TabIndex = 16;
            this.SetSelected.Text = "SetSelected";
            this.SetSelected.UseVisualStyleBackColor = true;
            this.SetSelected.Click += new System.EventHandler(this.SetSelectedClick);
            // 
            // Control
            // 
            this.Control.Location = new System.Drawing.Point(845, 216);
            this.Control.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Control.Name = "Control";
            this.Control.Size = new System.Drawing.Size(169, 27);
            this.Control.TabIndex = 17;
            this.Control.Text = "CustomTypeConverter";
            this.Control.UseVisualStyleBackColor = true;
            // 
            // btCancel
            // 
            this.btCancel.Location = new System.Drawing.Point(737, 249);
            this.btCancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(100, 27);
            this.btCancel.TabIndex = 18;
            this.btCancel.Text = "Cancel";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancelClick);
            // 
            // btNoCancel
            // 
            this.btNoCancel.Location = new System.Drawing.Point(845, 249);
            this.btNoCancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btNoCancel.Name = "btNoCancel";
            this.btNoCancel.Size = new System.Drawing.Size(100, 27);
            this.btNoCancel.TabIndex = 19;
            this.btNoCancel.Text = "NoCancel";
            this.btNoCancel.UseVisualStyleBackColor = true;
            this.btNoCancel.Click += new System.EventHandler(this.btNoCancelClick);
            // 
            // DateTextBox
            // 
            this.DateTextBox.Location = new System.Drawing.Point(737, 283);
            this.DateTextBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DateTextBox.Name = "DateTextBox";
            this.DateTextBox.Size = new System.Drawing.Size(276, 25);
            this.DateTextBox.TabIndex = 20;
            // 
            // RefreshButton
            // 
            this.RefreshButton.Location = new System.Drawing.Point(951, 249);
            this.RefreshButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(100, 27);
            this.RefreshButton.TabIndex = 21;
            this.RefreshButton.Text = "RefreshButton";
            this.RefreshButton.UseVisualStyleBackColor = true;
            // 
            // DeleteFileAsync
            // 
            this.DeleteFileAsync.Location = new System.Drawing.Point(565, 332);
            this.DeleteFileAsync.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DeleteFileAsync.Name = "DeleteFileAsync";
            this.DeleteFileAsync.Size = new System.Drawing.Size(128, 27);
            this.DeleteFileAsync.TabIndex = 22;
            this.DeleteFileAsync.Text = "DeleteFileAsync";
            this.DeleteFileAsync.UseVisualStyleBackColor = true;
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(699, 332);
            this.OkButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(128, 27);
            this.OkButton.TabIndex = 23;
            this.OkButton.Text = "OkButton";
            this.OkButton.UseVisualStyleBackColor = true;
            // 
            // routedControlHost1
            // 
            this.routedControlHost1.DefaultContent = null;
            this.routedControlHost1.Location = new System.Drawing.Point(16, 504);
            this.routedControlHost1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.routedControlHost1.Name = "routedControlHost1";
            this.routedControlHost1.Router = null;
            this.routedControlHost1.Size = new System.Drawing.Size(540, 242);
            this.routedControlHost1.TabIndex = 24;
            this.routedControlHost1.ViewLocator = null;
            // 
            // btHome
            // 
            this.btHome.Location = new System.Drawing.Point(16, 471);
            this.btHome.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btHome.Name = "btHome";
            this.btHome.Size = new System.Drawing.Size(100, 27);
            this.btHome.TabIndex = 25;
            this.btHome.Text = "Show Home";
            this.btHome.UseVisualStyleBackColor = true;
            // 
            // btAbout
            // 
            this.btAbout.Location = new System.Drawing.Point(124, 471);
            this.btAbout.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btAbout.Name = "btAbout";
            this.btAbout.Size = new System.Drawing.Size(100, 27);
            this.btAbout.TabIndex = 26;
            this.btAbout.Text = "Show About";
            this.btAbout.UseVisualStyleBackColor = true;
            // 
            // btContact
            // 
            this.btContact.Location = new System.Drawing.Point(232, 471);
            this.btContact.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btContact.Name = "btContact";
            this.btContact.Size = new System.Drawing.Size(113, 27);
            this.btContact.TabIndex = 27;
            this.btContact.Text = "Show Contact";
            this.btContact.UseVisualStyleBackColor = true;
            // 
            // btGoBack
            // 
            this.btGoBack.Location = new System.Drawing.Point(353, 471);
            this.btGoBack.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btGoBack.Name = "btGoBack";
            this.btGoBack.Size = new System.Drawing.Size(113, 27);
            this.btGoBack.TabIndex = 28;
            this.btGoBack.Text = "Go Back";
            this.btGoBack.UseVisualStyleBackColor = true;
            // 
            // txName2
            // 
            this.txName2.Location = new System.Drawing.Point(835, 335);
            this.txName2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txName2.Name = "txName2";
            this.txName2.Size = new System.Drawing.Size(123, 25);
            this.txName2.TabIndex = 11;
            this.txName2.Text = "Name2";
            // 
            // txConfirmPassword
            // 
            this.txConfirmPassword.Location = new System.Drawing.Point(967, 335);
            this.txConfirmPassword.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txConfirmPassword.Name = "txConfirmPassword";
            this.txConfirmPassword.Size = new System.Drawing.Size(123, 25);
            this.txConfirmPassword.TabIndex = 29;
            this.txConfirmPassword.Text = "ConfirmPassword";
            // 
            // txUserName1
            // 
            this.txUserName1.Location = new System.Drawing.Point(1099, 335);
            this.txUserName1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txUserName1.Name = "txUserName1";
            this.txUserName1.Size = new System.Drawing.Size(123, 25);
            this.txUserName1.TabIndex = 30;
            this.txUserName1.Text = "UserName1";
            // 
            // lName2ErrorText
            // 
            this.lName2ErrorText.Location = new System.Drawing.Point(836, 361);
            this.lName2ErrorText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lName2ErrorText.Name = "lName2ErrorText";
            this.lName2ErrorText.Size = new System.Drawing.Size(255, 136);
            this.lName2ErrorText.TabIndex = 11;
            this.lName2ErrorText.Text = "Name2ErrorText";
            // 
            // lFormErrorText
            // 
            this.lFormErrorText.Location = new System.Drawing.Point(1095, 361);
            this.lFormErrorText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lFormErrorText.Name = "lFormErrorText";
            this.lFormErrorText.Size = new System.Drawing.Size(263, 136);
            this.lFormErrorText.TabIndex = 31;
            this.lFormErrorText.Text = "FormErrorText";
            // 
            // txFirstName1
            // 
            this.txFirstName1.Location = new System.Drawing.Point(569, 366);
            this.txFirstName1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txFirstName1.Name = "txFirstName1";
            this.txFirstName1.Size = new System.Drawing.Size(123, 25);
            this.txFirstName1.TabIndex = 32;
            this.txFirstName1.Text = "FirstName1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1373, 760);
            this.Controls.Add(this.txFirstName1);
            this.Controls.Add(this.lFormErrorText);
            this.Controls.Add(this.lName2ErrorText);
            this.Controls.Add(this.txUserName1);
            this.Controls.Add(this.txConfirmPassword);
            this.Controls.Add(this.txName2);
            this.Controls.Add(this.btGoBack);
            this.Controls.Add(this.btContact);
            this.Controls.Add(this.btAbout);
            this.Controls.Add(this.btHome);
            this.Controls.Add(this.routedControlHost1);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.DeleteFileAsync);
            this.Controls.Add(this.RefreshButton);
            this.Controls.Add(this.DateTextBox);
            this.Controls.Add(this.btNoCancel);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.Control);
            this.Controls.Add(this.SetSelected);
            this.Controls.Add(this.SomeList);
            this.Controls.Add(this.loginButton);
            this.Controls.Add(this.SomeTextBox);
            this.Controls.Add(this.lAge);
            this.Controls.Add(this.Age);
            this.Controls.Add(this.myControl);
            this.Controls.Add(this.ComboBox);
            this.Controls.Add(this.ToolTipLabel);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.searchResultsListBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.DataGridView searchResultsListBox;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lColor;
        private System.Windows.Forms.TextBox txBlue;
        private System.Windows.Forms.Label lBlue;
        private System.Windows.Forms.TextBox txGreen;
        private System.Windows.Forms.Label lGreen;
        private System.Windows.Forms.TextBox txRed;
        private System.Windows.Forms.Label lRed;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txPassword;
        private System.Windows.Forms.TextBox txUsername;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label ToolTipLabel;
        private System.Windows.Forms.ComboBox ComboBox;
        private System.Windows.Forms.Button myControl;
        private System.Windows.Forms.TextBox Age;
        private System.Windows.Forms.Label lAge;
        private System.Windows.Forms.TextBox SomeTextBox;
        private System.Windows.Forms.Button loginButton;
        private System.Windows.Forms.ListBox SomeList;
        private System.Windows.Forms.Button SetSelected;
        private System.Windows.Forms.Button Control;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btNoCancel;
        private System.Windows.Forms.TextBox DateTextBox;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.Button DeleteFileAsync;
        private System.Windows.Forms.Button OkButton;
        private ReactiveUIWinForm.RoutedControlHost routedControlHost1;
        private System.Windows.Forms.Button btHome;
        private System.Windows.Forms.Button btAbout;
        private System.Windows.Forms.Button btContact;
        private System.Windows.Forms.Button btGoBack;
        private System.Windows.Forms.TextBox txName2;
        private System.Windows.Forms.TextBox txConfirmPassword;
        private System.Windows.Forms.TextBox txUserName1;
        private System.Windows.Forms.Label lName2ErrorText;
        private System.Windows.Forms.Label lFormErrorText;
        private System.Windows.Forms.TextBox txFirstName1;
    }
}

