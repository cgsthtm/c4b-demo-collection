﻿/// 

namespace C4B.ReactiveUI.WinForms.Demo
{
    using System;
    using System.Reflection;
    using System.Windows.Forms;
    using Autofac;
    using global::ReactiveUI;
    using Serilog;
    using Splat;
    using Splat.Autofac;
    using Splat.Serilog;

    internal static class Program
    {
        public static IContainer Container { get; set; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // A helper method that will register all classes that derive off IViewFor
            // into our dependency injection container. ReactiveUI uses Splat for it's
            // dependency injection by default, but you can override this if you like.
            Assembly assembly = Assembly.GetCallingAssembly();
            Locator.CurrentMutable.RegisterViewsForViewModels(assembly);

            // Registration
            // If you'd like to use a custom converter globally, you need to register it using Splat Locator.
            ////Locator.CurrentMutable.RegisterConstant(
            ////    new MyCoolTypeConverter(),
            ////    typeof(IBindingTypeConverter)
            ////);

            // Dependency Injection
            // Create a new Toaster any time someone asks
            Locator.CurrentMutable.Register(() => new Toaster(), typeof(IToaster));
            // Register a singleton instance
            Locator.CurrentMutable.RegisterConstant(new ExtraGoodToaster(), typeof(IToaster));
            // Register a singleton which won't get created until the first user accesses it
            Locator.CurrentMutable.RegisterLazySingleton(() => new LazyToaster(), typeof(IToaster));

            AppBootstrapper bootstrapper = new AppBootstrapper();

            // Resolution
            // Splat provides methods to resolve dependencies to single or multiple instances.
            var toaster = Locator.Current.GetService<IToaster>();
            var allToasterImpls = Locator.Current.GetServices<IToaster>();
            toaster.Toast();

            // Logging
            // I only want to hear about errors
            var logger = new ConsoleLogger() { Level = LogLevel.Error };
            Locator.CurrentMutable.RegisterConstant(logger, typeof(Splat.ILogger));
            var log = Locator.Current.GetService<Splat.ILogger>();
            log.Write("hello splat.log", LogLevel.Error);
            // Serilog
            var serilogger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console() // sent to the console
                .WriteTo.File("logs/myapp.txt", rollingInterval: RollingInterval.Day) // sent to the log file
                .CreateLogger();
            Locator.CurrentMutable.RegisterConstant(serilogger, typeof(Serilog.ILogger));
            // Then in your service locator initialisation
            Locator.CurrentMutable.UseSerilogFullLogger();
            var serilog = Locator.Current.GetService<Serilog.ILogger>();
            serilog.Write(Serilog.Events.LogEventLevel.Debug, "hello splat.serilog");

            // Create ShellViewModel and register as IScreen
            var viewModel = new AppViewModel();
            Locator.CurrentMutable.RegisterConstant(viewModel, typeof(IScreen));
            // Resolve view for ShellViewModel
            var view = ViewLocator.Current.ResolveView(viewModel);
            view.ViewModel = viewModel;

            // Override Default Depenedency Inversion Container -
            // Use a Dependency Resolver Package - Splat.Autofac
            // Create a new Autofac container builder.
            var builder = new Autofac.ContainerBuilder();
            // etc.
            // Register the Adapter to Splat.
            // Creates and sets the Autofac resolver as the Locator.
            var autofacResolver = builder.UseAutofacDependencyResolver();
            // Register the resolver in Autofac so it can be later resolved.
            builder.RegisterInstance(autofacResolver);
            // Initialize ReactiveUI components.
            autofacResolver.InitializeReactiveUI();
            // If you need to override any service (such as the ViewLocator), register it after InitializeReactiveUI.
            // https://autofaccn.readthedocs.io/en/latest/register/registration.html#default-registrations
            // builder.RegisterType<MyCustomViewLocator>().As<IViewLocator>().SingleInstance();
            builder.RegisterType<StudentService>().As<IStudentService>();
            ////builder.RegisterType<Form1>().As<IViewFor<AppViewModel>>();
            ////builder.RegisterType<AppViewModel>();
            ////// Register views
            ////builder.RegisterType<HomeView>().As<IViewFor<HomeViewModel>>();
            ////builder.RegisterType<AboutView>().As<IViewFor<AboutViewModel>>();
            ////builder.RegisterType<ContactView>().As<IViewFor<ContactViewModel>>();
            // View Location???
            builder.RegisterType<SimpleViewLocator>().As<IViewLocator>().SingleInstance();
            ////Locator.CurrentMutable.RegisterLazySingleton(() => new SimpleViewLocator(), typeof(IViewLocator));

            // Set Autofac Locator's lifetime after the ContainerBuilder has been built:
            Container = builder.Build();
            var resolver = Container.Resolve<AutofacDependencyResolver>();
            // Set a lifetime scope (either the root or any of the child ones) to Autofac resolver.
            // This is needed because Autofac became immutable since version 5+.
            // https://github.com/autofac/Autofac/issues/811
            resolver.SetLifetimeScope(Container);

            using (var scope = Program.Container.BeginLifetimeScope())
            {
                var studentService = scope.Resolve<IStudentService>();
                studentService.Login("cgs", "xnm");
            }

            Application.Run(view as Form1);
        }
    }
}