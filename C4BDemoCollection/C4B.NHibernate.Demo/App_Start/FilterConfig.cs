﻿using System.Web;
using System.Web.Mvc;

namespace C4B.NHibernate.Demo
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
