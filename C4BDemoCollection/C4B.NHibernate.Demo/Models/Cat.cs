﻿// <copyright file="Cat.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.NHibernate.Demo.Models
{
    /// <summary>
    /// First persistent class.
    /// </summary>
    public class Cat
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public virtual string Id { get; set; }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets Sex.
        /// </summary>
        public virtual char Sex { get; set; }

        /// <summary>
        /// Gets or sets Weight.
        /// </summary>
        public virtual float Weight { get; set; }
    }
}