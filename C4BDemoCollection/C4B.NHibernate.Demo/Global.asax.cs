﻿// <copyright file="Global.asax.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.NHibernate.Demo
{
    using System.Linq;
    using System;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using C4B.NHibernate.Demo.Models;
    using global::NHibernate;
    using global::NHibernate.Cfg;

    /// <summary>
    /// MvcApplication.
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Application_Start.
        /// </summary>
        protected void Application_Start()
        {
            ISession session = NHibernateHelper.GetCurrentSession();
            try
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    var princess = new Cat
                    {
                        Name = "Princess",
                        Sex = 'F',
                        Weight = 7.4f,
                    };

                    session.Save(princess);
                    tx.Commit();
                }
            }
            finally
            {
                using (var tx = session.BeginTransaction())
                {
                    var females = session
                        .Query<Cat>()
                        .Where(c => c.Sex == 'F')
                        .ToList();
                    foreach (var cat in females)
                    {
                        Console.Out.WriteLine("Female Cat: " + cat.Name);
                    }

                    tx.Commit();
                }

                NHibernateHelper.CloseSession();
            }

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}