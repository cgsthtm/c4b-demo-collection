﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C4B.WinFormControl.Demo.控件事件
{
    public partial class FrmLabelEvent : Form
    {
        public FrmLabelEvent()
        {
            InitializeComponent();
        }

        private void FrmLabelEvent_Shown(object sender, EventArgs e)
        {
            this.label1.AllowDrop = true;
            this.label2.AllowDrop = true;
        }

        private void label1_DragEnter(object sender, DragEventArgs e)
        {
            Console.WriteLine("触发label1_DragEnter事件");
        }

        private void label2_DragEnter(object sender, DragEventArgs e)
        {
            Console.WriteLine("触发label1_DragEnter事件");
        }

        
    }
}
