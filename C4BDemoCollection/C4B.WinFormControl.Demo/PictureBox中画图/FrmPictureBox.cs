﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C4B.WinFormControl.Demo.PictureBox中画图
{
    public partial class FrmPictureBox : Form
    {
        public FrmPictureBox()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 不起作用，可以参考：https://www.cnblogs.com/Jezze/archive/2011/12/23/2299883.html
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmPictureBox_Shown(object sender, EventArgs e)
        {
            Pen p = new Pen(Color.Yellow);
            Graphics g = pictureBox1.CreateGraphics();
            g.DrawEllipse(p, 50, 20, 30, 30); // BuildHead
            g.DrawRectangle(p, 60, 50, 10, 50); // BuildBody
            g.DrawLine(p, 60, 50, 40, 100); // BuildArmLeft
            g.DrawLine(p, 70, 50, 90, 100); // BuildArmRight
            g.DrawLine(p, 60, 100, 45, 150); // BuildLegLeft
            g.DrawLine(p, 70, 100, 85, 150); // BuildLegRight
        }

        /// <summary>
        /// 起作用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Pen p = new Pen(Color.Yellow);
            Graphics g = pictureBox1.CreateGraphics();
            g.DrawEllipse(p, 50, 20, 30, 30); // BuildHead
            g.DrawRectangle(p, 60, 50, 10, 50); // BuildBody
            g.DrawLine(p, 60, 50, 40, 100); // BuildArmLeft
            g.DrawLine(p, 70, 50, 90, 100); // BuildArmRight
            g.DrawLine(p, 60, 100, 45, 150); // BuildLegLeft
            g.DrawLine(p, 70, 100, 85, 150); // BuildLegRight
        }

        /// <summary>
        /// 不起作用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmPictureBox_Load(object sender, EventArgs e)
        {
            Pen p = new Pen(Color.Yellow);
            Graphics g = pictureBox1.CreateGraphics();
            g.DrawEllipse(p, 50, 20, 30, 30); // BuildHead
            g.DrawRectangle(p, 60, 50, 10, 50); // BuildBody
            g.DrawLine(p, 60, 50, 40, 100); // BuildArmLeft
            g.DrawLine(p, 70, 50, 90, 100); // BuildArmRight
            g.DrawLine(p, 60, 100, 45, 150); // BuildLegLeft
            g.DrawLine(p, 70, 100, 85, 150); // BuildLegRight
        }
    }
}
