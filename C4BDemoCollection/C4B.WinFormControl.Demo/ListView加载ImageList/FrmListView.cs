﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C4B.WinFormControl.Demo.ListView加载ImageList
{
    public partial class FrmListView : Form
    {
        static readonly Form frmShowPic = new Form();
        List<Image> images = new List<Image>();

        public FrmListView()
        {
            InitializeComponent();


            List<string> tifNames = new List<string>();
            string path = @"C:\Users\Administrator\Pictures\Saved Pictures";
            DirectoryInfo TheFolder = new DirectoryInfo(path); // 文件路径
            imageList1.Images.Clear();
            imageList1.ColorDepth = ColorDepth.Depth32Bit;
            for (int i = 0; i < TheFolder.GetFiles().Length; i++)
            {
                if (TheFolder.GetFiles()[i].Length > 0 && TheFolder.GetFiles()[i].Extension == ".jpg")
                {
                    Image image = Image.FromFile(TheFolder.GetFiles()[i].DirectoryName + "\\" + TheFolder.GetFiles()[i].Name);
                    images.Add(image);
                    tifNames.Add(TheFolder.GetFiles()[i].Name);
                    imageList1.Images.Add(image, Color.Transparent);
                }
            }
            imageList1.ImageSize = new Size(256, 256);
            this.listView1.View = View.LargeIcon;
            this.listView1.LargeImageList = this.imageList1;
            this.listView1.BeginUpdate();
            this.listView1.Items.Clear();
            ListViewItem items = new ListViewItem();
            items.SubItems.Clear();
            for (int i = 0; i < tifNames.Count; i++)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageIndex = i;
                lvi.Text = tifNames[i];
                this.listView1.Items.Add(lvi);
                Thread.Sleep(50);
            }
            this.listView1.EndUpdate();
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            frmShowPic.Controls.Clear();
            int picIndex = listView1.SelectedIndices[0];
            Image currImage = listView1.LargeImageList.Images[picIndex];
            PictureBox pictureBox = new PictureBox();
            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox.Size = new Size(1024, 768);
            frmShowPic.Size = new Size(1024, 768);
            pictureBox.Image = images[picIndex];
            frmShowPic.Controls.Add(pictureBox);
            frmShowPic.ShowDialog();
        }
    }
}
