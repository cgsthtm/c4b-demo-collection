﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace C4B.IO.Demo
{
    public class OtherElseToTest
    {
        static int bkPoint = 0;
        static int currOldTick = 0;

        /// <summary>
        /// 测试固定频率的循环中每过1秒执行一次 if ((Environment.TickCount - currOldTick) % 1000 == 0)
        /// </summary>
        public static void TestEnvironmentTickCount()
        {
            System.Timers.Timer aTimer = new System.Timers.Timer(10);
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }
        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("The Elapsed event was raised at {0:HH:mm:ss.fff}", e.SignalTime);
            switch (bkPoint)
            {
                case 0:
                    {
                        bkPoint = 1;
                        currOldTick = Environment.TickCount;
                    }
                    break;
                case 1:
                    {
                        if ((Environment.TickCount - currOldTick) % 3000 == 0)
                        {
                            Console.WriteLine("ok");
                        }
                    }
                    break;
            }
        }

        /* //CRC
            uint8_t CalculateCRC8(void)
            {
              uint8_t i,j,crc8,poly;
              crc8 = 0;
              poly=0x1D;
              for(i=1;i<8;i++)
              {
                crc8 ^= Data_test[i];
                for(j=0;j<8;j++)
                {
                  if(crc8&0x80)
                  {
                    crc8 = (crc8 << 1)^poly;
                  }
                  else
                  {
                    crc8 <<= 1;
                  }
                }
              }
              return crc8;
            }
         */
        public static byte CalculateCRC8(byte[] Data_test)
        {
            byte i, j, crc8, poly;
            crc8 = 0;
            poly = 0x1D;
            for (i = 1; i < 8; i++)
            {
                crc8 ^= Data_test[i];
                for (j = 0; j < 8; j++)
                {
                    if (Convert.ToBoolean(crc8 & 0x80))
                    {
                        crc8 = (byte)Convert.ToInt32((crc8 << 1) ^ poly);
                    }
                    else
                    {
                        crc8 <<= 1;
                    }
                }
            }
            return crc8;
        }

        public static void TestWhileLoopBreak()
        {
            while (true)
            {
                Task.Delay(1000).Wait();
                switch (bkPoint)
                {
                    case 0:
                        {
                            Console.WriteLine($"bkPoint:{bkPoint}==========================================");
                            bkPoint = 1;
                        }
                        break;
                    case 1:
                        {
                            Console.WriteLine($"bkPoint:{bkPoint}==========================================");
                            bkPoint = 2;
                        }
                        break;
                    case 2:
                        {
                            Console.WriteLine($"bkPoint:{bkPoint}==========================================");
                            for (int i = 0; i < 100; i++)
                            {
                                Console.WriteLine(i);
                                if (i == 21)
                                {
                                    break;
                                }
                            }
                            bkPoint = 3;
                        }
                        break;
                    case 3:
                        {
                            Console.WriteLine($"bkPoint:{bkPoint}==========================================");
                            bkPoint = 0;
                        }
                        break;
                }
            }
        }
    }
}
