﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.IO.Demo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //FileStreamDemo.WriteBytesToFile();
            //OtherElseToTest.TestEnvironmentTickCount();
            //OtherElseToTest.CalculateCRC8(new byte[] { 0, 1, 0, 0, 0, 0, 0, 0 });
            OtherElseToTest.TestWhileLoopBreak();

            Console.ReadLine();
        }
    }
}
