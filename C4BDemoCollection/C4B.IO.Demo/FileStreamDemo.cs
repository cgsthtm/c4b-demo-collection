﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.IO.Demo
{
    public class FileStreamDemo
    {
        /// <summary>
        /// 向文件中追加字节数组
        /// </summary>
        public async static void WriteBytesToFile()
        {
            string fileName = $"{AppDomain.CurrentDomain.BaseDirectory}WriteBytesToFile.txt";
            for (int i = 0; i < 5; i++)
            {
                using (FileStream SourceStream = File.Open(fileName, FileMode.OpenOrCreate)) // 打开或创建文件
                {
                    SourceStream.Seek(0, SeekOrigin.End);
                    for (int ii = 0; ii < 255; ii++)
                    {
                        byte[] stabilityData = new byte[] { 0xAA, 0xAA, 0x55, 0x55, 0x00, 0x00, 0x00, Convert.ToByte(ii), Convert.ToByte(i), 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
                        await SourceStream.WriteAsync(stabilityData, 0, stabilityData.Length);
                    }
                }
            }
        }
    }
}
