﻿using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C4B.Bluetooth.Demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 搜索蓝牙方式1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button1.Text = "搜索蓝牙...";
            listBox1.Items.Clear();
            var task = Task.Run(() =>
            {
                BluetoothClient client = new BluetoothClient();
                foreach (BluetoothDeviceInfo bdi in client.DiscoverDevices())
                {
                    // do something with the device...
                    listBox1.BeginInvoke(new Action(() => { listBox1.Items.Add(bdi.DeviceName + " " + bdi.DeviceAddress); }));
                    System.Diagnostics.Debug.WriteLine(bdi.DeviceName + " " + bdi.DeviceAddress);
                }
            });
            await task;
            button1.Text = "搜索蓝牙";
            button1.Enabled = true;
        }

        /// <summary>
        /// 搜索蓝牙方式2并连接 利用本机操作系统设备选择器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button2_Click(object sender, EventArgs e)
        {
            // 搜索蓝牙
            var picker = new BluetoothDevicePicker();
            var device = await picker.PickSingleDeviceAsync();
            // 连接蓝牙并发送数据
            BluetoothClient client = new BluetoothClient();
            client.Connect(device.DeviceAddress, BluetoothService.BluetoothBase); // https://inthehand.github.io/html/T_InTheHand_Net_Bluetooth_BluetoothService.htm
            if (client.Connected)
            {
                System.Diagnostics.Debug.WriteLine(device?.DeviceName + " " + device?.DeviceAddress);
                var stream = client.GetStream();
                StreamWriter sw = new StreamWriter(stream, System.Text.Encoding.UTF8);
                sw.WriteLine("Hello world!");
                sw.Flush();
                sw.Close();
            }
        }
    }
}
