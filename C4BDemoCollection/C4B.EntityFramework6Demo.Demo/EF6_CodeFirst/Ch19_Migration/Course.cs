﻿// <copyright file="Course.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch19_Migration
{
    /// <summary>
    /// Course.
    /// </summary>
    internal class Course
    {
        /// <summary>
        /// Gets or sets CourseId.
        /// </summary>
        public int CourseId { get; set; }

        /// <summary>
        /// Gets or sets CourseName.
        /// </summary>
        public string CourseName { get; set; }
    }
}
