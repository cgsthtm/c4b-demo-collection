﻿# Migration in EF 6 Code-First
Entity Framework Code-First有不同的数据库初始化策略，如DCreateDatabaseIfNotExists, DropCreateDatabaseIfModelChanges, 和 DropCreateDatabaseAlways. 
但是，这些策略也存在一些问题，例如，如果您的数据库中已经有数据（除了种子数据）或现有的存储过程，触发器等。
这些策略用于删除整个数据库并重新创建它，因此您将丢失数据和其他DB对象。

Entity Framework引入了一个迁移工具，当您的模型更改时，该工具可以自动更新数据库模式，而不会丢失任何现有数据或其他数据库对象。
它使用一个新的数据库初始化器MigrateDatabaseToLatestVersion。

有两种迁移：
1. Automated Migration
2. Code-based Migration

## Automated Migration in Entity Framework 6
Entity Framework引入了自动迁移，这样您就不必为在域类中所做的每个更改手动处理数据库迁移。
可以通过在Package Manager Console中执行enable-migrations命令来实现自动迁移。从Tools → Library Package Manager → Package Manager Console
打开Package Manager Console，然后运行 enable-migrations -EnableAutomaticMigration:$true 命令（确保默认项目是上下文类所在的项目）。
命令成功运行后，它将在项目的Migration文件夹中创建一个从DbMigrationConfiguration派生的内部密封Configuration类。
正如您在Configuration类的构造函数中看到的，AutomaticMigrationsEnabled被设置为true。
下一步是将上下文类中的数据库初始化器设置为 MigrateDatabaseToLatestVersion ，如下所示。
~~~
public class SchoolContext: DbContext 
{
    public SchoolDBContext(): base("SchoolDB") 
    {
        Database.SetInitializer(new MigrateDatabaseToLatestVersion<SchoolDBContext, EF6Console.Migrations.Configuration>());
    }

    public DbSet<Student> Students { get; set; }
        
    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
    }
}
~~~
现在，您已经为自动迁移做好了准备。当您更改域类时，EF将自动处理迁移。到目前为止，我们只有上面的SchoolContext类中的Student实体。运行应用程序并查看创建的数据库：
您会发现EF API创建了一个系统表__MigrationHistory和Students表。__MigrationHistory表包含所有迁移的数据库更改历史。
现在，您可以添加新的域类，当您再次运行应用程序时，您将看到数据库自动包含所有实体的表。你不需要运行任何命令。
但是，这只在您添加新的域类或删除类时有效，但在您添加、修改或删除域类中的属性时无效。因此此，从任何域类中删除任何属性，然后运行应用程序。您将得到以下异常。
~~~
System.Data.Entity.Migrations.Infrastructure.AutomaticDataLossE
xception: 'Automatic migration was not applied because it would
result in dataloss.Set AutomaticMigrationDataLossAllowed to'true'on
your DbMigrationsConfiguration to allow application of automatic
migrations even if they might cause data loss. Alternately, use Update-
Database with the '-Force' option, or scaffold an explicit migration.
~~~
这是因为您将丢失属性的相应列中的数据。因此，要处理这种情况，必须在Configuration类构造函数中将 AutomaticMigrationDataLossAllowed 设置为true，
同时设置 AutomaticMigrationsEnabled = true; 。
要了解有关enable-migrations命令参数的更多信息，请在PMC中执行get-help enable-migrations和 get-help enable-migrations -detailed 命令。

## Code-Based Migration in Entity Framework 6
在前一章中，您了解了自动迁移，它在您更改域类时自动更新数据库模式。在这里，您将了解基于代码的迁移。
基于代码的迁移为迁移提供了更多的控制，并允许您配置其他内容，例如设置列的默认值，配置计算列等。
为了使用基于代码的迁移，您需要在Visual Studio的包管理器控制台中执行以下命令：
1. Enable-Migrations：通过创建Configuration类在项目中启用迁移。
2. Add-Migration：使用Up（）和Down（）方法根据指定的名称创建新的迁移类。
3. Update-Database：执行Add-Migration命令创建的最后一个迁移文件，并将更改应用于数据库架构。
要使用基于代码的迁移，请首先在Package Manager Console中执行enable-migrations命令。从Tools → Library Package Manager → Package Manager Console
打开Package Manager Console，然后运行enable-migrations命令（确保默认项目是上下文类所在的项目）。
Enable-Migrations命令将使用 AutomaticMigrationsEnabled = false 创建从DbMigrationsConfiguration派生的Configuration类。
现在，您需要在上下文类中设置数据库初始化器 MigrateDatabaseToLatestVersion ，如下所示。
~~~
public class SchoolContext: DbContext 
{
    public SchoolDBContext(): base("SchoolDB") 
    {
        Database.SetInitializer(new MigrateDatabaseToLatestVersion<SchoolDBContext, EF6Console.Migrations.Configuration>());
    }

    public DbSet<Student> Students { get; set; }
        
    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {

    }
}
~~~
现在，您必须使用Add-Migration命令和您的迁移类的名称创建一个迁移类，如下所示。
~~~
add-migration SchoolDB-v1
~~~
上面的命令将创建一个<timestamp>_SchoolDB-v1.cs文件并且文件中携带Up（）和Down（）方法，如下所示。
~~~
namespace EF6Console.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class SchoolDBv1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Students",
                C => new
                    {
                        StudentId = c.Int(nullable: false,identity:true),
                        Name = c.String(),
                        DoB = c.DateTime(),
                    })
                .PrimaryKey(t => t.StudentId);
        }

        public override void Down()
        {
            DropTable("dbo.Students");
~~~
可以看到，Up（）方法包含创建数据库对象的代码，Down（）方法包含删除数据库对象的代码。您也可以编写自己的自定义代码来进行其他配置。这是自动化迁移的优势。
要了解有关add-migration命令参数的更多信息，请在PMC中执行get-help add-migration或 get-help add-migration -detailed 命令.
使用add-migration命令创建迁移文件后，必须更新数据库。执行Update-Database命令以创建或修改数据库模式。使用-verbose选项查看应用于目标数据库的SQL语句。
~~~
update-database -verbose
~~~
在PMC中执行get-help update-database或 get-help update-database -detailed 命令以了解有关该命令的更多信息。
此时，将创建或更新数据库。现在，无论何时更改域类，都要使用name参数执行Add-Migration以创建新的迁移文件，然后执行Update-Database命令将更改应用到数据库模式。

## Rollback Migration
假设您要将数据库模式回滚到以前的任何状态，那么您可以使用-TargetMigration参数执行update-database命令到您要回滚到的点。
例如，假设有许多迁移应用于上述SchoolDB数据库，但您希望回滚到第一次迁移。然后执行以下命令。
~~~
update-database -TargetMigration:SchoolDB-v1
~~~