﻿// <copyright file="SchoolDBMigrationContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch19_Migration
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBMigrationContext.
    /// </summary>
    internal class SchoolDBMigrationContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBMigrationContext"/> class.
        /// </summary>
        public SchoolDBMigrationContext()
            : base("Ch19_Migration")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SchoolDBMigrationContext, C4B.EntityFramework6Demo.Demo.Migrations.Configuration>());
        }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets Courses.
        /// </summary>
        public DbSet<Course> Courses { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
