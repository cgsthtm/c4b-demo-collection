﻿// <copyright file="MigrationTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch19_Migration
{
    /// <summary>
    /// MigrationTest.
    /// </summary>
    internal class MigrationTest
    {
        /// <summary>
        /// Test_AutomatedMigration.
        /// </summary>
        public static void Test_AutomatedMigration()
        {
            // 当SchoolDBMigrationContext中只有DbSet<Student>
            using (var ctx = new SchoolDBMigrationContext())
            {
                var student = new Student() { StudentName = "cgs" };
                ctx.Students.Add(student);
                ctx.SaveChanges();
            }

            // 当SchoolDBMigrationContext中添加了DbSet<Course>
            using (var ctx = new SchoolDBMigrationContext())
            {
                var course = new Course() { CourseName = "cgs" };
                ctx.Courses.Add(course);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Test_CodeBasedMigration.
        /// </summary>
        public static void Test_CodeBasedMigration()
        {
            // 当SchoolDBMigration2Context中只有DbSet<Student>
            using (var ctx = new SchoolDBMigration2Context())
            {
                var student = new Student() { StudentName = "cgs" };
                ctx.Students.Add(student); // 运行前需要使用Add-Migration迁移，迁移后使用Update-Database创建或修改数据库模式
                ctx.SaveChanges();

                // add-migration SchoolDBMigration2-v1 -ConfigurationTypeName C4B.EntityFramework6Demo.Demo.Migration2s.Configuration
                // update-database -verbose -ConfigurationTypeName C4B.EntityFramework6Demo.Demo.Migration2s.Configuration
            }

            // 当SchoolDBMigration2Context中添加了DbSet<Course>
            using (var ctx = new SchoolDBMigration2Context())
            {
                var course = new Course() { CourseName = "cgs" };
                ctx.Courses.Add(course);
                ctx.SaveChanges();
            }

            // 回滚迁移
            // update-database -TargetMigration:SchoolDBMigration2-v1 -ConfigurationTypeName C4B.EntityFramework6Demo.Demo.Migration2s.Configuration
        }
    }
}
