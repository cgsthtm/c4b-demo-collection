﻿// <copyright file="SchoolDBMigration2Context.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch19_Migration
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBMigration2Context.
    /// </summary>
    internal class SchoolDBMigration2Context : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBMigration2Context"/> class.
        /// </summary>
        public SchoolDBMigration2Context()
            : base("Ch19_Migration2")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SchoolDBMigration2Context, C4B.EntityFramework6Demo.Demo.Migration2s.Configuration>());
        }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets Courses.
        /// </summary>
        public DbSet<Course> Courses { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
