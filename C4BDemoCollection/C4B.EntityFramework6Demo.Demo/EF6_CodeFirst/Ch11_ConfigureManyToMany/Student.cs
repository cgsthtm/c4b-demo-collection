﻿// <copyright file="Student.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch11_ConfigureManyToMany
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Student.
    /// </summary>
    internal class Student
    {
        // EF 6包含多对多关系的默认约定。您需要在两端包含集合导航属性。例如，Student类应该有一个Course类型的集合导航属性，
        // 而Course类应该有一个Student类型的集合导航属性，以在它们之间创建多对多关系，而无需任何配置

        /// <summary>
        /// Initializes a new instance of the <see cref="Student"/> class.
        /// </summary>
        public Student()
        {
            this.Courses = new HashSet<Course>();
        }

        /// <summary>
        /// Gets or sets StudentId.
        /// </summary>
        public int StudentId { get; set; }

        /// <summary>
        /// Gets or sets StudentName.
        /// </summary>
        [Required]
        public string StudentName { get; set; }

        /// <summary>
        /// Gets or sets Courses.
        /// </summary>
        public virtual ICollection<Course> Courses { get; set; }
    }
}