﻿// <copyright file="ConfigureManyToManyTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch11_ConfigureManyToMany
{
    using System.Collections.Generic;

    /// <summary>
    /// ConfigureManyToManyTest.
    /// </summary>
    internal class ConfigureManyToManyTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            using (var context = new SchoolDBConfigureManyToManyContext())
            {
                var course1 = new Course()
                {
                    CourseId = 1,
                    CourseName = "CourseA1",
                };
                var course2 = new Course()
                {
                    CourseId = 2,
                    CourseName = "CourseA2",
                };
                var course3 = new Course()
                {
                    CourseId = 3,
                    CourseName = "CourseA3",
                };
                var course4 = new Course()
                {
                    CourseId = 4,
                    CourseName = "CourseA4",
                };
                var student1 = new Student()
                {
                    StudentId = 1,
                    StudentName = "cgs1",
                    Courses = new List<Course>()
                    {
                        course3,
                        course4,
                    },
                };
                var student2 = new Student()
                {
                    StudentId = 2,
                    StudentName = "cgs2",
                    Courses = new List<Course>()
                    {
                        course2,
                        course3,
                    },
                };
                var student3 = new Student()
                {
                    StudentId = 3,
                    StudentName = "cgs3",
                    Courses = new List<Course>()
                    {
                        course1,
                        course2,
                    },
                };
                var student4 = new Student()
                {
                    StudentId = 4,
                    StudentName = "cgs4",
                };

                context.Courses.AddRange(new List<Course> { course1, course2, course3, course4 });
                context.Students.AddRange(new List<Student> { student1, student2, student3, student4 });
                context.SaveChanges();
            }
        }
    }
}
