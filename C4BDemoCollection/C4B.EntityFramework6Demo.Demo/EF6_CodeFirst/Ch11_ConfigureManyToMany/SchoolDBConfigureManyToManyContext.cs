﻿// <copyright file="SchoolDBConfigureManyToManyContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch11_ConfigureManyToMany
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBConfigureManyToManyContext.
    /// </summary>
    internal class SchoolDBConfigureManyToManyContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBConfigureManyToManyContext"/> class.
        /// </summary>
        public SchoolDBConfigureManyToManyContext()
            : base("Ch11_ConfigureManyToMany")
        {
        }

        /// <summary>
        /// Gets or sets Courses.
        /// </summary>
        public DbSet<Course> Courses { get; set; }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // EF API将在上述示例的数据库中创建Students、Courses以及连接表StudentCourses。
            // StudentCourses表将包括两个表的PK（主键）-Student_StudentId和Course_CourseId.
            ////base.OnModelCreating(modelBuilder);

            // 正如您在上面看到的，多对多关系的默认约定使用默认命名约定创建了一个连接表。使用Fluent API自定义连接表名和列名，如下所示：
            modelBuilder.Entity<Student>()
                .HasMany<Course>(s => s.Courses) // HasMany（）和WithMany（）方法用于配置Student和Course实体之间的多对多关系
                .WithMany(c => c.Students)
                .Map(cs =>
                {
                    // Map（）方法接受Action类型的委托，因此，我们可以传递lambda表达式来定制连接表中的列名。
                    // 我们可以在MapLeftKey（）中指定Student的PK属性名（我们从Student实体开始，所以它将是左表），并在MapRightKey（）方法中指定Course表的PK。
                    // ToTable（）方法指定连接表的名称（本例中为StudentCourse）。
                    cs.MapLeftKey("StudentRefId");
                    cs.MapRightKey("CourseRefId");
                    cs.ToTable("StudentCourse");
                });

            // 通过这种方式，您可以覆盖多对多关系的默认约定，并自定义连接表名称及其列。
        }
    }
}
