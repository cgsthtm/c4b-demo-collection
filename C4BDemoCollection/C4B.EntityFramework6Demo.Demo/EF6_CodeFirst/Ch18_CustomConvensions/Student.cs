﻿// <copyright file="Student.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch18_CustomConvensions
{
    /// <summary>
    /// Student.
    /// </summary>
    internal class Student
    {
        /// <summary>
        /// Gets or sets Student_ID.
        /// </summary>
        public int Student_ID { get; set; }

        /// <summary>
        /// Gets or sets Student_Name.
        /// </summary>
        public string Student_Name { get; set; }
    }
}
