﻿# Define Custom Code-First Conventions in EF 6
在前一章中，我们学习了代码优先的约定。EF 6还提供了定义自己的自定义约定的能力，这将成为模型的默认行为。
有两种主要类型的约定，配置约定和模型约定。

## Configuration Conventions
配置约定是一种在不覆盖Fluent API中提供的默认行为的情况下配置实体的方法。您可以在OnModelCreating（）方法和自定义类中定义配置约定，
这与使用Fluent API定义普通实体映射的方式类似。
例如，您希望将属性配置为名称与{实体名称}_ID匹配的键属性，例如，Student实体的Student_ID属性将是主键。以下是对该约定的定义。
~~~
protected override void OnModelCreating(DbModelBuilder modelBuilder)
{
    modelBuilder
        .Properties()
        .Where(p => p.Name == p.DeclaringType.Name + "_ID")
        .Configure(p => p.IsKey());

    base.OnModelCreating(modelBuilder);
}
~~~
下面定义字符串属性的约定。它将在SQL Server中为实体的所有字符串类型属性创建大小为50的nvarchar列。
~~~
protected override void OnModelCreating(DbModelBuilder modelBuilder)
{
        modelBuilder
            .Properties()
            .Where(p => p.PropertyType.Name == "String")
            .Configure(p => p.HasMaxLength(50));

    base.OnModelCreating(modelBuilder);
}
~~~
您还可以通过派生Convention类来定义此约定的自定义类，如下所示：
~~~
public class PKConvention : Convention
{
    public PKConvention()
    {
        .Properties()
        .Where(p => p.Name == p.DeclaringType.Name + "_ID")
        .Configure(p => p.IsKey());
    }
}
~~~
创建自定义约定类后，将其添加到约定中，如下所示：
~~~
protected override void OnModelCreating(DbModelBuilder modelBuilder)
{
    modelBuilder.Conventions.Add<PKConvention>();
}
~~~

## Model Conventions
模型约定基于底层模型元数据。CSDL和SSDL都有约定。创建一个类，实现IConceptualModelConvention从CSDL约定和IStoreModelConvention从SSDL约定。
请访问EF 6中的Custom Convention https://msdn.microsoft.com/en-us/library/dn469439(v=vs.113).aspx 以获取更多信息。

CSDL 和 SSDL 是与实体框架（Entity Framework, EF）相关的两个概念，它们通常出现在使用ADO.NET Entity Data Model时。这两个术语具体指的是：
1. CSDL (Conceptual Schema Definition Language):
  - CSDL是一种XML语言，用于定义概念模型或领域模型。
  - 概念模型描述了应用程序从用户角度看到的数据结构，它代表了业务逻辑层面的实体及其关系。
  - 在CSDL中定义的实体和关联映射到现实世界中的对象，比如顾客、订单等。
2. SSDL (Storage Schema Definition Language):
  - SSDL同样是一种XML语言，但它用来描述存储模型。
  - 存储模型反映了数据库的实际物理结构，包括表、视图、存储过程等。
  - 通过SSDL文件，可以明确地指定数据如何在底层数据库中被组织。
此外，在讨论Entity Framework时，还会提到MSSL (Mapping Specification Language) 或者有时称为MSL，它用于定义概念模型(CSDL)与存储模型(SSDL)之间的映射规则。
这种映射告诉Entity Framework如何将概念模型中的类转换为数据库中的表，以及相反的过程。