﻿// <copyright file="PKConvention.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch18_CustomConvensions
{
    using System.Data.Entity.ModelConfiguration.Conventions;

    /// <summary>
    /// PKConvention.
    /// </summary>
    internal class PKConvention : Convention
    {
        // 还可以通过派生Convention类来定义此约定的自定义类，如下所示：

        /// <summary>
        /// Initializes a new instance of the <see cref="PKConvention"/> class.
        /// </summary>
        public PKConvention()
        {
            this.Properties().Where(p => p.Name == p.DeclaringType.Name + "_ID")
                .Configure(p => p.IsKey());
        }
    }
}
