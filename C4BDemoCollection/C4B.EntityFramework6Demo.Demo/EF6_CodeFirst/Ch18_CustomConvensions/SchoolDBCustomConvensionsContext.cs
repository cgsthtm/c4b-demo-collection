﻿// <copyright file="SchoolDBCustomConvensionsContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch18_CustomConvensions
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBCustomConvensionsContext.
    /// </summary>
    internal class SchoolDBCustomConvensionsContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBCustomConvensionsContext"/> class.
        /// </summary>
        public SchoolDBCustomConvensionsContext()
            : base("Ch18_CustomConvensions")
        {
        }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // 将属性配置为名称与{实体名称}_ID匹配的键属性，例如，Student实体的Student_ID属性将是主键。以下是对该约定的定义。
            modelBuilder
                .Properties()
                .Where(p => p.Name == p.DeclaringType.Name + "_ID")
                .Configure(p => p.IsKey());

            // 下面定义字符串属性的约定。它将在SQL Server中为实体的所有字符串类型属性创建大小为50的nvarchar列。
            modelBuilder
                .Properties()
                .Where(p => p.PropertyType.Name == "String")
                .Configure(p => p.HasMaxLength(50));

            // 创建自定义约定类后，将其添加到约定中，如下所示：
            modelBuilder.Conventions.Add<PKConvention>();

            base.OnModelCreating(modelBuilder);
        }
    }
}
