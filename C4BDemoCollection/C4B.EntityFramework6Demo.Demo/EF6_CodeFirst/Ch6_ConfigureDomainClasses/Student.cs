﻿// <copyright file="Student.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch6_ConfigureDomainClasses
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    // 数据注释属性不支持实体框架的所有配置选项。因此，您可以使用Fluent API，它提供了EF的所有配置选项。

    /// <summary>
    /// Student.
    /// </summary>
    [Table("StudentInfo")]
    public class Student
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Student"/> class.
        /// </summary>
        public Student()
        {
        }

        /// <summary>
        /// Gets or sets SID.
        /// </summary>
        [Key]
        public int SID { get; set; }

        /// <summary>
        /// Gets or sets StudentName.
        /// </summary>
        [Column("Name", TypeName = "ntext")]
        [MaxLength(20)]
        public string StudentName { get; set; }

        /// <summary>
        /// Gets or sets Age.
        /// </summary>
        [NotMapped]
        public int? Age { get; set; }

        /// <summary>
        /// Gets or sets StdId.
        /// </summary>
        public int StdId { get; set; }

        /// <summary>
        /// Gets or sets Standard.
        /// </summary>
        [ForeignKey("StdId")]
        public virtual Standard Standard { get; set; }
    }
}