﻿// <copyright file="SchoolDBContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch6_ConfigureDomainClasses
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBContext.
    /// </summary>
    internal class SchoolDBContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBContext"/> class.
        /// </summary>
        public SchoolDBContext()
            : base("SchoolDBConnectionString")
        {
        }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets Standards.
        /// </summary>
        public DbSet<Standard> Standards { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // 当EF从您的域类构建模型时，可以应用流畅的API配置。您可以通过覆盖Entity Framework 6.x中DbContext的OnModelCreating方法来注入Fluent API配置
            // Configure domain classes using modelBuilder here..

            // 您可以使用modelBuilder，DbModelBuilder类的对象来配置域类。DbModelBuilder被称为Fluent API，因为您可以在链中调用不同的方法。
        }
    }
}
