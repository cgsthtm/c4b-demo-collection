﻿// <copyright file="Standard.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch6_ConfigureDomainClasses
{
    /// <summary>
    /// Standard.
    /// </summary>
    public class Standard
    {
        /// <summary>
        /// Gets or sets StdId.
        /// </summary>
        public int StdId { get; set; }
    }
}
