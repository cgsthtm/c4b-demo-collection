﻿# Generate Context and Entity Classes from an Existing Database in EF 6 Code-First Approach
在这里，您将学习如何使用代码优先的方法为现有数据库生成上下文和实体类。
Entity Framework为现有数据库提供了一种使用代码优先方法的简单方法。它将为现有数据库中的所有表和视图创建实体类，并使用数据注释属性和Fluent API配置它们。
若要对现有数据库使用代码优先，请在Visual Studio ->添加->新建项中右键单击项目。
在AddNewItem对话框中选择ADO.NET Entity Data Model，指定模型名称（这将是一个上下文类名），然后单击Add。
这将打开实体数据模型向导，选择Code First from database，然后单击下一步。
现在，选择现有数据库的数据连接。如果连接不包括与现有数据库的连接，请为数据库创建新连接。单击下一步继续。
现在，选择要为其生成类的表和视图，然后单击Finish。
这将为您的DB表和视图生成所有实体类。