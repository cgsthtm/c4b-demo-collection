using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch12_FromExistingDB
{
    public partial class SchoolDBFromExistingDBContext : DbContext
    {
        public SchoolDBFromExistingDBContext()
            : base("name=SchoolDBFromExistingDBContext")
        {
        }

        public virtual DbSet<Courses> Courses { get; set; }
        public virtual DbSet<Students> Students { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Courses>()
                .HasMany(e => e.Students)
                .WithMany(e => e.Courses)
                .Map(m => m.ToTable("StudentCourse").MapLeftKey("CourseRefId").MapRightKey("StudentRefId"));
        }
    }
}
