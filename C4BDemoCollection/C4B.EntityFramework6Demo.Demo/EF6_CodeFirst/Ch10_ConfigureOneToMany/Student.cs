﻿// <copyright file="Student.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch10_ConfigureOneToMany
{
    /// <summary>
    /// Student.
    /// </summary>
    internal class Student
    {
        /// <summary>
        /// Gets or sets StudentId.
        /// </summary>
        public int StudentId { get; set; }

        /// <summary>
        /// Gets or sets StudentName.
        /// </summary>
        public string StudentName { get; set; }

        // 在实体框架中有一些约定，如果在实体类（域类）中遵循这些约定，将自动导致数据库中两个表之间的一对多关系。您不需要配置其他任何东西。
        // 这可以通过在Student实体类中包含Grade类型的引用导航属性来实现，如下所示。
        // Student类包括Grade类的引用导航属性。所以，一个年级可以有很多学生。这将在数据库中的Students和Grades表之间产生一对多关系，其中Students表包括外键Grade_GradeId

        /// <summary>
        /// Gets or sets Grade.
        /// </summary>
        public Grade Grade { get; set; } // 另一种约定是在主体实体中包含集合导航属性，见Grade.cs

        /// <summary>
        /// Gets or sets GradeId.
        /// </summary>
        public int GradeId { get; set; } // 两端完全定义的关系将创建一对多关系，即在Student中包含GradeId和Grade，在Grade中包含Student集合。
    }
}
