﻿// <copyright file="ConfigureOneToManyTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch10_ConfigureOneToMany
{
    /// <summary>
    /// ConfigureOneToManyTest.
    /// </summary>
    internal class ConfigureOneToManyTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            using (var context = new SchoolDBConfigureOneToManyContext())
            {
                var student = new Student
                {
                    StudentName = "cgs",
                    GradeId = 1,
                };
                var grade = new Grade
                {
                    GradeId = 1,
                    GradeName = "A1",
                };
                context.Grades.Add(grade);
                context.Students.Add(student);
                context.SaveChanges();
            }
        }
    }
}
