﻿// <copyright file="Grade.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch10_ConfigureOneToMany
{
    using System.Collections.Generic;

    /// <summary>
    /// Grade.
    /// </summary>
    internal class Grade
    {
        /// <summary>
        /// Gets or sets GradeId.
        /// </summary>
        public int GradeId { get; set; }

        /// <summary>
        /// Gets or sets GradeName.
        /// </summary>
        public string GradeName { get; set; }

        /// <summary>
        /// Gets or sets Section.
        /// </summary>
        public string Section { get; set; }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public ICollection<Student> Students { get; set; }

        // 在上面的示例中，Grade实体包括类型为ICollection<Student>的集合导航属性。这也会导致学生和年级实体之间的一对多关系。
        // 这个例子在数据库中产生与Student.cs中的例子相同的结果。

        // 在两端包含导航属性也将导致一对多关系，即在Student中包含Grade，在Grade中包含Student的集合。
    }
}
