﻿// <copyright file="SchoolDBConfigureOneToManyContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch10_ConfigureOneToMany
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBConfigureOneToManyContext.
    /// </summary>
    internal class SchoolDBConfigureOneToManyContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBConfigureOneToManyContext"/> class.
        /// </summary>
        public SchoolDBConfigureOneToManyContext()
            : base("Ch10_ConfigureOneToMany")
        {
        }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets Grades.
        /// </summary>
        public DbSet<C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch10_ConfigureOneToMany.Grade> Grades { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // configures one-to-many relationship
            modelBuilder.Entity<Student>() // 首先，我们需要开始配置任何一个实体类。所以， modelBuilder.Entity<student>() 从学生实体开始。
                .HasRequired<Grade>(s => s.Grade) // 然后， .HasRequired<Grade>(s => s.Grade) 指定Student实体需要Grade属性。这将在数据库中创建一个NotNull外键列。
                .WithMany(g => g.Students) // 现在，该配置关系的另一端-Grade实体了。.WithMany（g => g.Students）指定Grade实体类包括许多Student实体。
                .HasForeignKey<int>(s => s.GradeId); // 现在，如果Student实体不遵循外键的Id属性约定，那么我们可以使用HasForeignKey方法指定外键的名称。

            // 或者，您可以开始配置与Grade实体（而不是Student实体）的关系。下面的代码产生与上面相同的结果。
            modelBuilder.Entity<C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch10_ConfigureOneToMany.Grade>()
                .HasMany<Student>(g => g.Students)
                .WithRequired(s => s.Grade)
                .HasForeignKey<int>(s => s.GradeId);

            // 级联删除意味着在删除相关父行时自动删除子行。例如，如果删除了年级，那么该年级的所有学生也应该自动删除。
            // 下面的代码使用WillCascadeOnDelete方法配置级联删除。
            modelBuilder.Entity<Grade>()
            .HasMany<Student>(g => g.Students)
            .WithRequired(s => s.Grade)
            .WillCascadeOnDelete();
        }
    }
}
