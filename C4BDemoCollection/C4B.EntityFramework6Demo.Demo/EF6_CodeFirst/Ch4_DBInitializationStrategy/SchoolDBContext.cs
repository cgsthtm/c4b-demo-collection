﻿// <copyright file="SchoolDBContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch4_DBInitializationStrategy
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBContext.
    /// </summary>
    internal class SchoolDBContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBContext"/> class.
        /// </summary>
        public SchoolDBContext()
            : base("SchoolDBConnectionString")
        {
            Database.SetInitializer<SchoolDBContext>(new CreateDatabaseIfNotExists<SchoolDBContext>());

            ////Database.SetInitializer<SchoolDBContext>(new DropCreateDatabaseIfModelChanges<SchoolDBContext>());
            ////Database.SetInitializer<SchoolDBContext>(new DropCreateDatabaseAlways<SchoolDBContext>());
            ////Database.SetInitializer<SchoolDBContext>(new SchoolDBInitializer());

            // 您可以关闭应用程序的数据库初始值设定项。假设您不想在生产环境中丢失现有数据，那么您可以关闭初始化器
            Database.SetInitializer<SchoolDBContext>(null);
        }

        ////public DbSet<Student> Students { get; set; }
        ////public DbSet<Standard> Standards { get; set; }
    }
}