﻿// <copyright file="SchoolDBInitializer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch4_DBInitializationStrategy
{
    using System.Data.Entity;

    // 您也可以通过继承其中一个初始化器来创建自定义DB初始化器

    /// <summary>
    /// SchoolDBInitializer.
    /// </summary>
    internal class SchoolDBInitializer : CreateDatabaseIfNotExists<SchoolDBContext>
    {
        /// <inheritdoc/>
        protected override void Seed(SchoolDBContext context)
        {
            base.Seed(context);
        }

        // 在上面的示例中，SchoolDBInitializer是一个自定义的初始化器类，它派生自SQLDatabaseIfNotitialist。这将数据库初始化代码从上下文类中分离出来。
    }
}
