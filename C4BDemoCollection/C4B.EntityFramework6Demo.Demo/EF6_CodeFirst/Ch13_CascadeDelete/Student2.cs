﻿// <copyright file="Student2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch13_CascadeDelete
{
    /// <summary>
    /// Student2.
    /// </summary>
    internal class Student2
    {
        /// <summary>
        /// Gets or sets Student2Id.
        /// </summary>
        public int Student2Id { get; set; }

        /// <summary>
        /// Gets or sets Student2Name.
        /// </summary>
        public string Student2Name { get; set; }

        /// <summary>
        /// Gets or sets Standard2.
        /// </summary>
        public virtual Standard2 Standard2 { get; set; }
    }
}
