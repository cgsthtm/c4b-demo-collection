﻿# Cascade Delete in Entity Framework 6
级联删除会在删除数据库中的父记录时自动删除相关记录或将ForeignKey列设置为null。
在Entity Framework中，级联删除默认情况下对所有类型的关系（如一对一、一对多和多对多）启用。
