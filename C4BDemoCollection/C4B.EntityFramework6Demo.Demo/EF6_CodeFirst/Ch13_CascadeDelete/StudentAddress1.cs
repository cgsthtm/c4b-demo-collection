﻿// <copyright file="StudentAddress1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch13_CascadeDelete
{
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// StudentAddress1.
    /// </summary>
    internal class StudentAddress1
    {
        /// <summary>
        /// Gets or sets StudentAddress1Id.
        /// </summary>
        [ForeignKey("Student1")]
        public int StudentAddress1Id { get; set; }

        /// <summary>
        /// Gets or sets Address1.
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets Address2.
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets City.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets Zipcode.
        /// </summary>
        public int Zipcode { get; set; }

        /// <summary>
        /// Gets or sets State.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets Country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets Student1.
        /// </summary>
        public virtual Student1 Student1 { get; set; }
    }
}
