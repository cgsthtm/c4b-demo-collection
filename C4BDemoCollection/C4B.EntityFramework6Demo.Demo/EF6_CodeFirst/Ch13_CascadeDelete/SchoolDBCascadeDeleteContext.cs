﻿// <copyright file="SchoolDBCascadeDeleteContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch13_CascadeDelete
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBCascadeDeleteContext.
    /// </summary>
    internal class SchoolDBCascadeDeleteContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBCascadeDeleteContext"/> class.
        /// </summary>
        public SchoolDBCascadeDeleteContext()
            : base("Ch13_CascadeDelete")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<SchoolDBCascadeDeleteContext>());
        }

        /// <summary>
        /// Gets or sets Student1s.
        /// </summary>
        public DbSet<Student1> Student1s { get; set; }

        /// <summary>
        /// Gets or sets StudentAddress1s.
        /// </summary>
        public DbSet<StudentAddress1> StudentAddress1s { get; set; }

        /// <summary>
        /// Gets or sets Student2s.
        /// </summary>
        public DbSet<Student2> Student2s { get; set; }

        /// <summary>
        /// Gets or sets Standard2s.
        /// </summary>
        public DbSet<Standard2> Standard2s { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // 使用Fluent API配置实体以使用WillCascadeOnDelete（）方法关闭级联删除，例如：
            ////modelBuilder.Entity<Student2>()
            ////    .HasOptional<Standard2>(s => s.Standard2)
            ////    .WithMany()
            ////    .WillCascadeOnDelete(false);
            // 没有任何一个数据注释属性可用于关闭级联删除。只能使用上述方式。
        }
    }
}
