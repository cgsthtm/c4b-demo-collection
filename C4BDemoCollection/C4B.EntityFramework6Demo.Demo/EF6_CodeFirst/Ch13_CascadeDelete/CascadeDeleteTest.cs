﻿// <copyright file="CascadeDeleteTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch13_CascadeDelete
{
    /// <summary>
    /// CascadeDeleteTest.
    /// </summary>
    internal class CascadeDeleteTest
    {
        /// <summary>
        /// Test_InOneToOneRelationships.
        /// </summary>
        public static void Test_InOneToOneRelationships()
        {
            using (var ctx = new SchoolDBCascadeDeleteContext())
            {
                var stud = new Student1() { Student1Name = "James" };
                var add = new StudentAddress1() { Address1 = "address" };

                stud.Address1 = add;
                ctx.Student1s.Add(stud);
                ctx.SaveChanges();
                ctx.Student1s.Remove(stud); // student and its address will be removed from db
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Test_InOneToManyRelationships.
        /// </summary>
        public static void Test_InOneToManyRelationships()
        {
            using (var ctx = new SchoolDBCascadeDeleteContext())
            {
                var student1 = new Student2() { Student2Name = "James" };
                var student2 = new Student2() { Student2Name = "Gandhi" };
                var standard1 = new Standard2() { Standard2Name = "Standard 1" };
                student1.Standard2 = standard1;
                student2.Standard2 = standard1;
                ctx.Student2s.Add(student1);
                ctx.Student2s.Add(student2);

                // inserts students and standard1 into db
                ctx.SaveChanges();

                // deletes standard1 from db and also set standard_StandardId FK column in Students table to null for
                // all the students that reference standard1.
                ctx.Standard2s.Remove(standard1);
                ctx.SaveChanges();
            }
        }
    }
}
