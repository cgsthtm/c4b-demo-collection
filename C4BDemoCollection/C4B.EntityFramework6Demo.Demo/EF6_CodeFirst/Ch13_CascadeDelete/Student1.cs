﻿// <copyright file="Student1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch13_CascadeDelete
{
    /// <summary>
    /// Student1.
    /// </summary>
    internal class Student1
    {
        /// <summary>
        /// Gets or sets Student1Id.
        /// </summary>
        public int Student1Id { get; set; }

        /// <summary>
        /// Gets or sets Student1Name.
        /// </summary>
        public string Student1Name { get; set; }

        /// <summary>
        /// Gets or sets StudentAddress1.
        /// </summary>
        public virtual StudentAddress1 Address1 { get; set; }
    }
}
