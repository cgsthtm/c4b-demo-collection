﻿// <copyright file="Standard2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch13_CascadeDelete
{
    using System.Collections.Generic;

    /// <summary>
    /// Standard2.
    /// </summary>
    internal class Standard2
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Standard2"/> class.
        /// </summary>
        public Standard2()
        {
            this.Student2s = new List<Student2>();
        }

        /// <summary>
        /// Gets or sets Standard2Id.
        /// </summary>
        public int Standard2Id { get; set; }

        /// <summary>
        /// Gets or sets Standard2Name.
        /// </summary>
        public string Standard2Name { get; set; }

        /// <summary>
        /// Gets or sets Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets Student2s.
        /// </summary>
        public virtual ICollection<Student2> Student2s { get; set; }
    }
}
