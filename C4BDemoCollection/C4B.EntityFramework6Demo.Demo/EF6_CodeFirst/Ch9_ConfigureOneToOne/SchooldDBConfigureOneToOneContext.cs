﻿// <copyright file="SchooldDBConfigureOneToOneContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch9_ConfigureOneToOne
{
    using System.Data.Entity;

    /// <summary>
    /// SchooldDBConfigureOneToOneContext.
    /// </summary>
    internal class SchooldDBConfigureOneToOneContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchooldDBConfigureOneToOneContext"/> class.
        /// </summary>
        public SchooldDBConfigureOneToOneContext()
            : base("Ch9_ConfigureOneToOne")
        {
        }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets StudentAddresses.
        /// </summary>
        public DbSet<StudentAddress> StudentAddresses { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // 我们可以使用Fluent API配置实体之间的一对一关系，其中两端都需要，这意味着Student实体对象必须包括StudentAddress实体对象，
            // StudentAddress实体必须包括Student实体对象，以便保存它。
            // Configure StudentId as FK for StudentAddress
            modelBuilder.Entity<Student>()
                        .HasRequired(s => s.Address) // 使StudentAddress的Address属性成为必需的
                        .WithRequiredPrincipal(ad => ad.Student); // 使StudentAddress实体的Student属性成为必需的。
        }
    }
}
