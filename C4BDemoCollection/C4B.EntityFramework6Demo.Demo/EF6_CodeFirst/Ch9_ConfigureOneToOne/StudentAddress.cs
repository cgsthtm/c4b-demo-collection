﻿// <copyright file="StudentAddress.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch9_ConfigureOneToOne
{
    using System.ComponentModel.DataAnnotations.Schema;

    // 当一个表的主键在关系数据库（如SQL Server）的另一个表中变成PK & FK时，就会发生一对零或一的关系。因此，我们需要以这样的方式配置上述实体，
    // 即EF在DB中创建Students和StudentAddresses表，并将Student表中的StudentId列设置为PrimaryKey（PK），
    // 将StudentAddresses表中的StudentAddressId列设置为PK和ForeignKey（FK）。

    /// <summary>
    /// StudentAddress.
    /// </summary>
    internal class StudentAddress
    {
        // 对于StudentAddress实体，我们需要将StudentAddressId配置为PK和FK两者。StudentAddressId属性遵循主键的默认约定。因此，我们不需要为PK应用任何属性。
        // 但是，我们还需要使其成为指向Student实体的StudentId的外键。

        /// <summary>
        /// Gets or sets StudentAddressId.
        /// </summary>
        [ForeignKey("Student")] // 您可以使用数据注释属性来配置两个实体之间的一对零或一关系。
        public int StudentAddressId { get; set; }

        /// <summary>
        /// Gets or sets Address1.
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets Address2.
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets City.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets Zipcode.
        /// </summary>
        public int Zipcode { get; set; }

        /// <summary>
        /// Gets or sets State.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets Country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets Student.
        /// </summary>
        public virtual Student Student { get; set; }

        // 注意：Student包括StudentAddress导航属性，StudentAddress包括Student导航属性。使用一对零或一的关系，可以在没有StudentAddress的情况下保存Student，
        // 但不能在没有Student实体的情况下保存StudentAddress实体。如果您尝试在不保存Student实体的情况下保存StudentAddress实体，EF将引发异常。
    }
}
