﻿// <copyright file="Student.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch16_MoveConfigurations
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Student.
    /// </summary>
    internal class Student
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Student"/> class.
        /// </summary>
        public Student()
        {
            this.Courses = new HashSet<Course>();
        }

        /// <summary>
        /// Gets or sets StudentKey.
        /// </summary>
        public int StudentKey { get; set; }

        /// <summary>
        /// Gets or sets StudentName.
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// Gets or sets DateOfBirth.
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets Courses.
        /// </summary>
        public virtual ICollection<Course> Courses { get; set; }
    }
}
