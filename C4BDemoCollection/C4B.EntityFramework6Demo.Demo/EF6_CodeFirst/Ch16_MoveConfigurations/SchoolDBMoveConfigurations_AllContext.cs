﻿// <copyright file="SchoolDBMoveConfigurations_AllContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch16_MoveConfigurations
{
    using System.Data.Entity;

    // 正如您在前几章中看到的，我们在OnModelCreating（）方法中使用Fluent-API配置了所有域类。但是，如果在OnModelCreating中配置了大量的域类，则很难进行维护。
    // EF 6允许您为每个实体创建一个单独的类，并放置与实体相关的所有配置。

    /// <summary>
    /// SchoolDBMoveConfigurations_AllContext.
    /// </summary>
    internal class SchoolDBMoveConfigurations_AllContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBMoveConfigurations_AllContext"/> class.
        /// </summary>
        public SchoolDBMoveConfigurations_AllContext()
            : base("Ch16_MoveConfigurations_1")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<SchoolDBMoveConfigurations_AllContext>());
        }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets Courses.
        /// </summary>
        public DbSet<Course> Courses { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().ToTable("StudentInfo");

            modelBuilder.Entity<Student>().HasKey<int>(s => s.StudentKey);

            modelBuilder.Entity<Student>()
                    .Property(p => p.DateOfBirth)
                    .HasColumnName("DoB")
                    .HasColumnOrder(3)
                    .HasColumnType("datetime2");

            modelBuilder.Entity<Student>()
                    .Property(p => p.StudentName)
                    .HasMaxLength(50);

            modelBuilder.Entity<Student>()
                .Property(p => p.StudentName)
                .IsConcurrencyToken();

            modelBuilder.Entity<Student>()
                .HasMany<Course>(s => s.Courses)
                .WithMany(c => c.Students)
                .Map(cs =>
                {
                    cs.MapLeftKey("StudentId");
                    cs.MapRightKey("CourseId");
                    cs.ToTable("StudentCourse");
                });
        }
    }
}
