﻿// <copyright file="SchoolDBMoveConfigurations_SepContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch16_MoveConfigurations
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBMoveConfigurations_SepContext.
    /// </summary>
    internal class SchoolDBMoveConfigurations_SepContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBMoveConfigurations_SepContext"/> class.
        /// </summary>
        public SchoolDBMoveConfigurations_SepContext()
            : base("Ch16_MoveConfigurations_2")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<SchoolDBMoveConfigurations_SepContext>());
        }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets Courses.
        /// </summary>
        public DbSet<Course> Courses { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Moved all Student related configuration to StudentEntityConfiguration class
            modelBuilder.Configurations.Add(new StudentEntityConfiguration()); // 您可以使用多个配置类来增加可读性和可维护性。
        }
    }
}
