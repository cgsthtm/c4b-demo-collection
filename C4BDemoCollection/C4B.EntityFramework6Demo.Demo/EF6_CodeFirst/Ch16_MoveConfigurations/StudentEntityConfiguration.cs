﻿// <copyright file="StudentEntityConfiguration.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch16_MoveConfigurations
{
    using System.Data.Entity.ModelConfiguration;

    // 现在，您可以将与Student实体相关的所有配置移动到从 EntityTypeConfiguration<TEntity> 派生的单独类中。
    // 考虑下面的StudentEntity类，它包括Student实体的所有配置。

    /// <summary>
    /// StudentEntityConfiguration.
    /// </summary>
    internal class StudentEntityConfiguration : EntityTypeConfiguration<Student>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentEntityConfiguration"/> class.
        /// </summary>
        public StudentEntityConfiguration()
        {
            this.ToTable("StudentInfo");

            this.HasKey<int>(s => s.StudentKey);

            this.Property(p => p.DateOfBirth)
                    .HasColumnName("DoB")
                    .HasColumnOrder(3)
                    .HasColumnType("datetime2");

            this.Property(p => p.StudentName)
                    .HasMaxLength(50);

            this.Property(p => p.StudentName)
                    .IsConcurrencyToken();

            this.HasMany<Course>(s => s.Courses)
                .WithMany(c => c.Students)
                .Map(cs =>
                {
                    cs.MapLeftKey("StudentId");
                    cs.MapRightKey("CourseId");
                    cs.ToTable("StudentCourse");
                });
        }
    }
}
