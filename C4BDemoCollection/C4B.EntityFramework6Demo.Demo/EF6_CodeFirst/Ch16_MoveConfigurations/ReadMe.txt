﻿# Move Fluent API Configurations to a Separate Class in Entity Framework
正如您在前几章中看到的，我们在OnModelCreating（）方法中使用Fluent-API配置了所有域类。但是，如果在OnModelCreating中配置了大量的域类，则很难进行维护。
EF 6允许您为每个实体创建一个单独的类，并放置与实体相关的所有配置。
