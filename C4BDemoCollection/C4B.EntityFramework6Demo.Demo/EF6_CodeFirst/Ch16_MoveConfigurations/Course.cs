﻿// <copyright file="Course.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch16_MoveConfigurations
{
    using System.Collections.Generic;

    /// <summary>
    /// Course.
    /// </summary>
    internal class Course
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Course"/> class.
        /// </summary>
        public Course()
        {
            this.Students = new HashSet<Student>();
        }

        /// <summary>
        /// Gets or sets CourseId.
        /// </summary>
        public int CourseId { get; set; }

        /// <summary>
        /// Gets or sets CourseName.
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public ICollection<Student> Students { get; set; }
    }
}
