﻿// <copyright file="SchoolDBContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch8_FluentAPI.Sec1_EntityMappings
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Configuration;

    /// <summary>
    /// SchoolDBContext.
    /// </summary>
    internal class SchoolDBContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBContext"/> class.
        /// </summary>
        public SchoolDBContext()
            : base("Ch8_FluentAPI_EntityMappings")
        {
        }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets Standards.
        /// </summary>
        public DbSet<Standard> Standards { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Configure default schema
            modelBuilder.HasDefaultSchema("Admin");

            // Map entity to table
            modelBuilder.Entity<Student>().ToTable("StudentInfo");
            modelBuilder.Entity<Standard>().ToTable("StandardInfo", "dbo");

            // Map Entity to Multiple Tables
            modelBuilder.Entity<Student>().Map(m =>
            {
                m.Properties(p => new { p.StudentId, p.StudentName });
                m.ToTable("StudentInfo");
            }).Map(m =>
            {
                m.Properties(p => new { p.StudentId, p.Height, p.Weight, p.Photo, p.DateOfBirth });
                m.ToTable("StudentInfoDetail");
            });

            // Map（）方法需要一个委托方法参数。可以在Map（）方法中传递Action委托或lambda表达式
            Action<EntityMappingConfiguration<Student>> studentMapping = m =>
            {
                m.Properties(p => new { p.StudentId, p.Height, p.Weight, p.Photo, p.DateOfBirth });
                m.ToTable("StudentInfoDetail");
            };
        }
    }
}
