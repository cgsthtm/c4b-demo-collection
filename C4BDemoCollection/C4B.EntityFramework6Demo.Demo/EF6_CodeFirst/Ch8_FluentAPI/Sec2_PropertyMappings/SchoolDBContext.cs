﻿// <copyright file="SchoolDBContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch8_FluentAPI.Sec2_PropertyMappings
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBContext.
    /// </summary>
    internal class SchoolDBContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBContext"/> class.
        /// </summary>
        public SchoolDBContext()
            : base("Ch8_FluentAPI_PropertyMappings")
        {
        }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets Standards.
        /// </summary>
        public DbSet<Standard> Standards { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Configure primary key
            modelBuilder.Entity<Student>().HasKey<int>(s => s.StudentKey);
            modelBuilder.Entity<Standard>().HasKey<int>(s => s.StandardKey);

            // Configure composite primary key
            modelBuilder.Entity<Student>().HasKey(s => new { s.StudentKey, s.StudentName });

            // Configure Column
            modelBuilder.Entity<Student>()
                        .Property(p => p.DateOfBirth)
                        .HasColumnName("DoB")
                        .HasColumnOrder(3)
                        .HasColumnType("datetime2");

            /*modelBuilder.Entity<TEntity>().Property(expression) 允许您使用不同的方法来配置特定的属性，如下：
                HasColumnAnnotation
                HasColumnName
                HasColumnOrder
                HasColumnType
                HasDatabaseGeneratedOption
                HasParameterlName
                HasPrecision
                IsConcurrencyToken
                IsOptional
             */

            // Configure Null Column
            modelBuilder.Entity<Student>()
                    .Property(p => p.Height)
                    .IsOptional();

            // Configure NotNull Column
            modelBuilder.Entity<Student>()
                .Property(p => p.Weight)
                .IsRequired();

            // Set StudentName column size to 50
            modelBuilder.Entity<Student>()
                    .Property(p => p.StudentName)
                    .HasMaxLength(50);

            // Set StudentName column size to 50 and change datatype to nchar
            // IsFixedLength() change datatype from nvarchar to nchar
            modelBuilder.Entity<Student>()
                    .Property(p => p.StudentName)
                    .HasMaxLength(50).IsFixedLength();

            // Set size decimal(2,2)
            modelBuilder.Entity<Student>()
                .Property(p => p.Height)
                .HasPrecision(2, 2);

            // Set StudentName as concurrency column
            modelBuilder.Entity<Student>()
                    .Property(p => p.StudentName)
                    .IsConcurrencyToken(); // 还可以对byte[]类型属性使用IsRowVersion（）方法，使其成为并发列。
        }
    }
}