﻿// <copyright file="Standard.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch8_FluentAPI.Sec2_PropertyMappings
{
    using System.Collections.Generic;

    /// <summary>
    /// Standard.
    /// </summary>
    internal class Standard
    {
        /// <summary>
        /// Gets or sets StandardKey.
        /// </summary>
        public int StandardKey { get; set; }

        /// <summary>
        /// Gets or sets StandardName.
        /// </summary>
        public string StandardName { get; set; }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public ICollection<Student> Students { get; set; }
    }
}
