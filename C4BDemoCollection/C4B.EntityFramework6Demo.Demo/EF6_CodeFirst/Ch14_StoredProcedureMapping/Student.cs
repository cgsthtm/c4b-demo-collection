﻿// <copyright file="Student.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch14_StoredProcedureMapping
{
    using System;

    /// <summary>
    /// Student.
    /// </summary>
    internal class Student
    {
        /// <summary>
        /// Gets or sets StudentId.
        /// </summary>
        public int StudentId { get; set; }

        /// <summary>
        /// Gets or sets StudentName.
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// Gets or sets DateOfBirth.
        /// </summary>
        public DateTime DoB { get; set; }
    }
}
