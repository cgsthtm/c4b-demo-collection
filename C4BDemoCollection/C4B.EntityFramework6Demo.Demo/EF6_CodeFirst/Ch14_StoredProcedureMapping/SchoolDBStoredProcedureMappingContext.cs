﻿// <copyright file="SchoolDBStoredProcedureMappingContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch14_StoredProcedureMapping
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBStoredProcedureMappingContext.
    /// </summary>
    internal class SchoolDBStoredProcedureMappingContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBStoredProcedureMappingContext"/> class.
        /// </summary>
        public SchoolDBStoredProcedureMappingContext()
            : base("Ch14_StoredProcedureMapping")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<SchoolDBStoredProcedureMappingContext>());
        }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // 将Student实体映射到默认存储过程。
            modelBuilder.Entity<Student>()
                .MapToStoredProcedures(); // EF API将为上述Student实体创建三个存储过程Student_Insert、Student_Update和Student_Delete

            // EF 6允许您使用自己的自定义存储过程并将它们映射到实体。还可以配置参数与实体属性的映射。
            // 将自定义存储过程映射到Student实体
            ////modelBuilder.Entity<Student>()
            ////.MapToStoredProcedures(p => p.Insert(sp => sp.HasName("sp_InsertStudent").Parameter(pm => pm.StudentName, "name").Result(rs => rs.StudentId, "Id"))
            ////        .Update(sp => sp.HasName("sp_UpdateStudent").Parameter(pm => pm.StudentName, "name"))
            ////        .Delete(sp => sp.HasName("sp_DeleteStudent").Parameter(pm => pm.StudentId, "Id")));

            // 您可以在单个语句中使用默认存储过程映射所有实体，如下所示。
            ////modelBuilder.Types().Configure(t => t.MapToStoredProcedures());

            /*限制：
               - 只有Fluent API可用于映射存储过程。EF 6中没有数据注释属性可用于存储过程映射。
               - 如果要将存储过程用于CUD操作，则必须将插入、更新和删除存储过程映射到实体。不允许只映射其中一个。
             */
        }
    }
}
