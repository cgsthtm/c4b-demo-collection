﻿# CUD Operations using Stored Procedures in Entity Framework 6 Code-First Approach
Entity Framework 6 Code-First提供了在调用SaveChanges（）方法时为添加、更新和删除操作创建和使用存储过程的能力。