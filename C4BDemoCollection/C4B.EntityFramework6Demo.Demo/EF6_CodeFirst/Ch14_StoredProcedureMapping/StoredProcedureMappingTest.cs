﻿// <copyright file="StoredProcedureMappingTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch14_StoredProcedureMapping
{
    /// <summary>
    /// StoredProcedureMappingTest.
    /// </summary>
    internal class StoredProcedureMappingTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            using (var ctx = new SchoolDBStoredProcedureMappingContext())
            {
                var student = new Student { StudentName = "cgs", DoB = System.DateTime.Now };
                ctx.Students.Add(student);
                ctx.SaveChanges();
                ctx.Students.Remove(student);
                ctx.SaveChanges();
            }
        }
    }
}
