﻿// <copyright file="SchoolDBSeedDataInitializer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch17_SeedData
{
    using System.Collections.Generic;
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBSeedDataInitializer.
    /// </summary>
    internal class SchoolDBSeedDataInitializer : DropCreateDatabaseAlways<SchoolDBSeedDataContext>
    {
        // 要将数据播种到数据库中，您必须创建自定义DB初始化器（如您在DB初始化策略一章中创建的那样），并重写Seed方法。

        /// <inheritdoc/>
        protected override void Seed(SchoolDBSeedDataContext context)
        {
            IList<Standard> defaultStandards = new List<Standard>();
            defaultStandards.Add(new Standard() { StandardName = "Standard 1", Description = "First Standard" });
            defaultStandards.Add(new Standard() { StandardName = "Standard 2", Description = "Second Standard" });
            defaultStandards.Add(new Standard() { StandardName = "Standard 3", Description = "Third Standard" });

            context.Standards.AddRange(defaultStandards);

            base.Seed(context);
        }
    }
}
