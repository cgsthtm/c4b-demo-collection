﻿// <copyright file="SchoolDBSeedDataContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch17_SeedData
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolDBSeedDataContext.
    /// </summary>
    internal class SchoolDBSeedDataContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolDBSeedDataContext"/> class.
        /// </summary>
        public SchoolDBSeedDataContext()
            : base("Ch17_SeedData")
        {
            // 要将数据播种到数据库中，您必须创建自定义DB初始化器（如您在DB初始化策略一章中创建的那样），并重写Seed方法。
            Database.SetInitializer(new SchoolDBSeedDataInitializer());
        }

        /// <summary>
        /// Gets or sets Standards.
        /// </summary>
        public DbSet<Standard> Standards { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
