﻿// <copyright file="Standard.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch17_SeedData
{
    /// <summary>
    /// Standard.
    /// </summary>
    internal class Standard
    {
        /// <summary>
        /// Gets or sets StandardId.
        /// </summary>
        public int StandardId { get; set; }

        /// <summary>
        /// Gets or sets StandardName.
        /// </summary>
        public string StandardName { get; set; }

        /// <summary>
        /// Gets or sets Description.
        /// </summary>
        public string Description { get; set; }
    }
}
