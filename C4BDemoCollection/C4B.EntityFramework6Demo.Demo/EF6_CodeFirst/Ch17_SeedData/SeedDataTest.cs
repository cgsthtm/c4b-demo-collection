﻿// <copyright file="SeedDataTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch17_SeedData
{
    /// <summary>
    /// SeedDataTest.
    /// </summary>
    internal class SeedDataTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            using (var ctx = new SchoolDBSeedDataContext())
            {
                var standard = new Standard { StandardName = "Test111", Description = string.Empty };
                ctx.Standards.Add(standard);
                ctx.SaveChanges();
            }
        }
    }
}
