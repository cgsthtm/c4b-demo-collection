﻿// <copyright file="Student.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch1_SampleCodeFirstExample
{
    using System;

    /// <summary>
    /// Student.
    /// </summary>
    internal class Student
    {
        /// <summary>
        /// Gets or sets StudentID.
        /// </summary>
        public int StudentID { get; set; }

        /// <summary>
        /// Gets or sets StudentName.
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// Gets or sets DateOfBirth.
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets Photo.
        /// </summary>
        public byte[] Photo { get; set; }

        /// <summary>
        /// Gets or sets Height.
        /// </summary>
        public decimal Height { get; set; }

        /// <summary>
        /// Gets or sets Weight.
        /// </summary>
        public float Weight { get; set; }

        /// <summary>
        /// Gets or sets Grade.
        /// </summary>
        public Grade Grade { get; set; }
    }
}
