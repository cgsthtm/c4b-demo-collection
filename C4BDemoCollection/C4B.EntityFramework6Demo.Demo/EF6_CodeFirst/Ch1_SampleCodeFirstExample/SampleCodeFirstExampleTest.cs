﻿// <copyright file="SampleCodeFirstExampleTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch1_SampleCodeFirstExample
{
    /// <summary>
    /// SampleCodeFirstExampleTest.
    /// </summary>
    internal class SampleCodeFirstExampleTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            using (var ctx = new SchoolContext())
            {
                var stud = new Student() { StudentName = "Bill" };

                ctx.Students.Add(stud);
                ctx.SaveChanges();
            }
        }
    }
}
