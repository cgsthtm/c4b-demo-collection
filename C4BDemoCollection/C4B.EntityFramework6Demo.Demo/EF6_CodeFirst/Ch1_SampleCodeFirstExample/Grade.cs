﻿// <copyright file="Grade.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch1_SampleCodeFirstExample
{
    using System.Collections.Generic;

    /// <summary>
    /// Grade.
    /// </summary>
    internal class Grade
    {
        /// <summary>
        /// Gets or sets GradeId.
        /// </summary>
        public int GradeId { get; set; }

        /// <summary>
        /// Gets or sets GradeName.
        /// </summary>
        public string GradeName { get; set; }

        /// <summary>
        /// Gets or sets Section.
        /// </summary>
        public string Section { get; set; }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public ICollection<Student> Students { get; set; }
    }
}
