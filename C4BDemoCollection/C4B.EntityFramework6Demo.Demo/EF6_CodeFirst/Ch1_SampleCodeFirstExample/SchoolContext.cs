﻿// <copyright file="SchoolContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch1_SampleCodeFirstExample
{
    using System.Data.Entity;

    /// <summary>
    /// SchoolContext.
    /// </summary>
    internal class SchoolContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolContext"/> class.
        /// </summary>
        public SchoolContext()
            ////: base("name=SchoolDBConnectionString")
            ////: base("MySchoolDB")
            : base() // {Namespace}.{Context class name}
        {
        }

        /// <summary>
        /// Gets or sets Students.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets Grades.
        /// </summary>
        public DbSet<C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch1_SampleCodeFirstExample.Grade> Grades { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch1_SampleCodeFirstExample.Grade>().ToTable("Grades");
            base.OnModelCreating(modelBuilder);
        }
    }
}
