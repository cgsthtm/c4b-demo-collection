﻿// <copyright file="Configuration.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Configuration.
    /// </summary>
    internal sealed class Configuration : DbMigrationsConfiguration<C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch19_Migration.SchoolDBMigrationContext>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration"/> class.
        /// </summary>
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = true;
        }

        /// <inheritdoc/>
        protected override void Seed(C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch19_Migration.SchoolDBMigrationContext context)
        {
            // This method will be called after migrating to the latest version.
            // You can use the DbSet<T>.AddOrUpdate() helper extension method
            // to avoid creating duplicate seed data.
        }
    }
}