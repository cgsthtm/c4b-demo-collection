﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo
{
    using System;
    using C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch1_SampleCodeFirstExample;
    using C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch10_ConfigureOneToMany;
    using C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch11_ConfigureManyToMany;
    using C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch13_CascadeDelete;
    using C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch14_StoredProcedureMapping;
    using C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch17_SeedData;
    using C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch19_Migration;
    using C4B.EntityFramework6Demo.Demo.EF6_DBFirst.Ch4_DbSetEntities;
    using C4B.EntityFramework6Demo.Demo.EF6_DBFirst.Ch5_AddUpdateDeleteData;

    /// <summary>
    /// Program.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            // EF6_CodeFirst
            ////SampleCodeFirstExampleTest.Test();
            ////ConfigureOneToManyTest.Test();
            ////ConfigureManyToManyTest.Test();
            ////CascadeDeleteTest.Test_InOneToOneRelationships();
            ////CascadeDeleteTest.Test_InOneToManyRelationships();
            ////StoredProcedureMappingTest.Test();
            ////SeedDataTest.Test();
            ////MigrationTest.Test_AutomatedMigration();
            ////MigrationTest.Test_CodeBasedMigration();

            // EF6_DBFirst
            ////DbSetEntitiesTest.Test();
            AddUpdateDeleteDataTest.Test();

            Console.ReadLine();
        }
    }
}