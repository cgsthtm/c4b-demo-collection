﻿# Creating an Entity Data Model in Entity Framework 6 DB-First Approach
Entity Framework将EDM用于所有与数据库相关的操作。实体数据模型是描述实体及其之间关系的模型。
1. 通过在解决方案资源管理器中右键单击项目->Add->New Item来添加实体数据模型。这将打开添加新项目弹出窗口。在弹出窗口中，
   选择ADO.NET Entity Data Model并为EDM提供一个适当的名称（本例中为“School”），然后单击Add按钮。
2. Visual Studio中的Entity Data Model Wizard（2012/2015/2017）有四个选项可供选择：数据库优先的EF Designer from database，模型优先的Empty EF Designer model，
   空代码优先的Empty Code First model和代码优先的Code First from database。我们在这里使用数据库优先的方法，因此选择EF Designer from database选项并单击Next。
3. 在此步骤中，您需要创建与现有数据库的连接。如果这是您第一次为数据库创建EDM，则需要通过单击“新建连接”来创建新连接。
   在“连接属性”弹出窗口中，提供服务器名称（.对于本地数据库），选择数据库名称，然后单击确定按钮。
4. 下一步将显示数据库中的所有表，视图和存储过程（SP）。选择所需的表、视图和SP，选中默认复选框，然后单击完成。如果需要，您可以更改模型空间。
   注意：
   - 如果数据库中的表名为复数，则Pluralize or singularize generated object names复选框将实体集名称单数化。例如，如果SchoolDB数据库包含Students表名，
     则实体集应为单数Student。类似地，如果表与其他表具有一对多或多对多的关系，则模型之间的关系将为复数。例如，Student表与Course表具有多对多关系，
     因此Student实体集的Course类型集合导航属性将具有复数名称Courses。
   - 第二个复选框Include foreignkey columns in the model包含一个明确表示外键的外键属性。例如，“学生”表与“标准”表具有一对多关系。为了在模型中表示这一点，
     Student entityset包括一个StandardId属性和Standard引用导航属性。如果未选中此复选框，则它将仅包括标准引用导航属性，而不包括StandardId。
   - 第三个复选框，Import selected stored procedures and functions into entity model，自动为存储过程和函数创建函数导入。你不需要像Entity Framework 6.0之前那样手动导入函数。
6. 点击完成后，一个School.edmx文件将添加到您的项目。双击School.edmx打开EDM designer。这将显示选定表的所有实体以及它们之间的关系.
您可以用XML视图打开EDM设计器，在该视图中，您可以在XML视图中看到EDM的所有三个部分：概念架构（CSDL）、存储架构（SSDL）和映射架构（MSL）。

## Entity-Table Mapping
EDM中的每个实体都与数据库表映射。您可以通过在EDM设计器中右键单击任何实体来检查实体-表映射->选择表映射。此外，如果您从设计器更改了实体的任何属性名称，那么表映射将自动反映该更改。

## Context & Entity Classes
每个实体数据模型为每个数据库表生成一个上下文和一个实体类。在解决方案资源管理器中展开.edmx文件，然后打开两个重要文件<EDM Name>.Context.tt和<EDM Name>.tt，如下所示：
- School.Context.tt：每当您更改实体数据模型（.edmx文件）时，此T4模板文件都会生成一个上下文类。您可以通过展开School.Context.tt查看上下文类文件。
  上下文类位于<EDM Name>.context.cs文件中。默认上下文类名为<DB Name >Entities。例如，SchoolDB的上下文类名称是SchoolDBEntities，并从DBContext类派生。
- School.tt：School.tt是一个T4模板文件，它为每个DB表生成实体类。实体类是POCO（Plain Old Object）类。下面的代码片段显示了Student实体。
  ~~~
    public partial class Student
    {
        public Student()
        {
            this.Courses = new HashSet<Course>();
        }
    
        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public Nullable<int> StandardId { get; set; }
        public byte[] RowVersion { get; set; }
    
        public virtual Standard Standard { get; set; }
        public virtual StudentAddress StudentAddress { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
    }
  ~~~
在下一节中，了解如何使用模型浏览器查看EDM的所有对象。