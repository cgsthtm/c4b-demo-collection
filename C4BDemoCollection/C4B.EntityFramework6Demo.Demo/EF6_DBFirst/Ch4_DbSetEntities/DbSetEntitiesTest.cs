﻿// <copyright file="DbSetEntitiesTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_DBFirst.Ch4_DbSetEntities
{
    using System.Collections.Generic;
    using System.Linq;
    using C4B.EntityFramework6Demo.Demo.EF6_DBFirst.Ch1_CraeteEntityDataModel;
    using Xunit;

    /// <summary>
    /// DbSetEntitiesTest.
    /// </summary>
    public class DbSetEntitiesTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        [Fact]
        public static void Test()
        {
            using (var ctx = new DBFirst_Ch1_CreateEDMEntities())
            {
                /* System.Data.Entity.Core.MetadataException: 'Schema specified is not valid. Errors:
The mapping of CLR type to EDM type is ambiguous because multiple CLR types match the EDM type 'Grade'. Previously found CLR type 'C4B.EntityFramework6Demo.Demo.EF6_DBFirst.Ch1_CraeteEntityDataModel.Grade', newly found CLR type 'C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch1_SampleCodeFirstExample.Grade'.
The mapping of CLR type to EDM type is ambiguous because multiple CLR types match the EDM type 'Grade'. Previously found CLR type 'C4B.EntityFramework6Demo.Demo.EF6_DBFirst.Ch1_CraeteEntityDataModel.Grade', newly found CLR type 'C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch10_ConfigureOneToMany.Grade'.'
                 */
                // 解决办法：修改DBFirst_Ch1_CreateEDM数据库中的Grades表增加一列使其与其他Grades类的属性不完全一致，双击.edmx选择Model Browser上的.edmx右键
                // 选择从数据库更新模型，保存.edmx的修改，此时就不会ambiguous了。
                ctx.Students.Add(new Student()
                    {
                        StudentId = 1,
                        StudentName = "zyn",
                        Courses = new List<Cours>()
                        {
                            new Cours()
                            {
                                CourseId = 1,
                                CourseName = "Java",
                            },
                        },
                        Grade = new Grade()
                        {
                            GradeId = 1,
                            GradeName = "G1",
                            Section = "G1",
                            Description = "G1",
                        },
                    });
                ctx.SaveChanges();
                var student = ctx.Students.SqlQuery("SELECT * FROM STUDENTS WHERE STUDENTID=1").FirstOrDefault<Student>();
                Assert.Equal("zyn", student.StudentName);
            }
        }
    }
}
