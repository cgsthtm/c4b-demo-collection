﻿# Asynchronous Querying and Saving in EF 6
.NET 4.5中引入了异步执行，这在Entity Framework中很有用。EF 6允许我们使用DbContext实例异步执行查询和命令。

## Asynchronous Query
下面是一个异步执行LINQ-to-Entity查询并返回结果的的示例。
~~~
private static async Task<Student> GetStudent()
{
    Student student = null;

    using (var context = new SchoolDBEntities())
    {
        Console.WriteLine("Start GetStudent...");
              
        student = await (context.Students.Where(s => s.StudentID == 1).FirstOrDefaultAsync<Student>());
            
        Console.WriteLine("Finished GetStudent...");
    }

    return student;
}
~~~
正如您在上面的代码中所看到的，GetStudent（）方法标记有Async关键字，这使其成为一个异步方法。非同步方法的返回类型必须是Task<T>。
GetStudent（）方法返回Student实体的对象，因此返回类型必须为Task<Student>类型。
此外，LINQ查询还标记了await关键字。这将释放调用线程以执行其他代码，直到它执行查询并返回结果。我们使用了FirstOrDefaultAsync扩展方法来获得结果。
您可以适当地使用其他的async方法，如SingleOrDefaultAsync，ToListAsync等。

## Asynchronous Save
EF API提供SaveChangesAsync（）方法来异步地将实体保存到数据库。下面的SaveStudent方法将Student实体异步保存到数据库。
~~~
private static async Task SaveStudent(Student editedStudent)
{
    using (var context = new SchoolDBEntities())
    {
        context.Entry(editedStudent).State = EntityState.Modified;
                
        Console.WriteLine("Start SaveStudent...");
                
        int x = await (context.SaveChangesAsync());
                
        Console.WriteLine("Finished SaveStudent...");
    }
}
~~~

## Getting the async Query Result
下面的示例演示了如何执行一个async查询，获取结果并保存修改后的实体。
~~~
public static void AsyncQueryAndSave()
{
    var query = GetStudent();
            
    Console.WriteLine("Do something else here till we get the query result..");

    query.Wait();

    var student = query.Result;
	
    student.FirstName = "Steve";
            
    var numOfSavedStudent = SaveStudent(student);
            
    Console.WriteLine("Do something else here till we save a student.." );

    numOfSavedStudent.Wait();

    Console.WriteLine("Saved Entities: {0}", numOfSavedStudent.Result);
}
~~~
Output:
~~~
Start GetStudent...
Do something else here till we get the query result..
Finished GetStudent...
Start SaveStudent...
Do something else here till we save a student..
Finished SaveStudent...
Saved Entities: 1
~~~