﻿# Validate Entity in Entity Framework 6 DB-First Approach
您可以为任何实体编写自定义服务器端验证。要实现这一点，请重写DbContext的ValidateEntity方法，如下所示。
~~~
protected override System.Data.Entity.Validation.DbEntityValidationResult ValidateEntity(DbEntityEntry entityEntry, System.Collections.Generic.IDictionary<object, object> items)
{
    if (entityEntry.Entity is Student)
    {
        if (entityEntry.CurrentValues.GetValue<string>("StudentName") == "")
        {
            var list = new List<System.Data.Entity.Validation.DbValidationError>();
            list.Add(new System.Data.Entity.Validation.DbValidationError("StudentName", "StudentName is required"));

            return new System.Data.Entity.Validation.DbEntityValidationResult(entityEntry, list);
        }
    }
    return base.ValidateEntity(entityEntry, items);
}
~~~
正如您在上面的代码中看到的，我们正在验证Student实体。如果StudentName为空，则我们将DBValidationError添加到DBValidationResult中。因此，
每当您调用DbContext.SaveChanges方法并尝试保存不带StudentName的Student实体时，它都会抛出DbContextyValidationException。请看下面的例子。
~~~
try
{
    using (var ctx = new SchoolDBEntities())
    {
        ctx.Students.Add(new Student() { StudentName = "" });
        ctx.Standards.Add(new Standard() { StandardName = "" });

        ctx.SaveChanges();
    }
}
catch (DbEntityValidationException dbEx)
{
    foreach (DbEntityValidationResult entityErr in dbEx.EntityValidationErrors)
    {
        foreach (DbValidationError error in entityErr.ValidationErrors)
        {
            Console.WriteLine("Error Property Name {0} : Error Message: {1}",
                                error.PropertyName, error.ErrorMessage);
        }
    }
}
~~~