﻿# Table-Valued Function in Entity Framework 6 DB-First Approach
表值函数类似于存储过程，除了一个关键的区别：TVF的结果是可组合的，这意味着它可以在LINQ-to-Entity查询中使用。
(总的来说，选择使用表值函数还是存储过程取决于具体的需求。如果主要是为了返回数据集，表值函数可能是更好的选择；如果需要执行复杂的业务逻辑，
尤其是涉及数据修改的操作，那么存储过程更为合适。)
以下是SQL Server数据库中的示例TVFGetCourseListByStudentID，它将返回特定学生的所有课程。
~~~
USE [SchoolDB]
GO
/****** Object:  UserDefinedFunction [dbo].[GetCourseListByStudentID]  */  
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetCourseListByStudentID]
(    
    -- Add the parameters for the function here
    @studentID int
)
RETURNS TABLE 
AS
RETURN 
(
    -- Add the SELECT statement with parameter references here
    select c.courseid, c.coursename,c.Location, c.TeacherId
    from student s left outer join studentcourse sc on sc.studentid = s.studentid 
        left outer join course c on c.courseid = sc.courseid
        where s.studentid = @studentID
)
~~~
现在，更新您的EDM并通过右键单击设计器将此TVF添加到您现有的EDM中->从数据库中选择更新模型。，如下所示。
在更新向导中，展开“存储过程和函数”->“架构”->选择“GetCourseListByStudentID”并单击“完成”。
确保选中了Import selected procedures and functions into the entity model的复选框（这将自动导入函数）。
这将添加TVF作为函数，并为结果创建一个新的复杂类型 GetCourseListByStudentID_Result 。我们的TVFGetCourseListByStudentID返回Course表中的列，
因此我们可以用结果映射Course实体，而不是使用复杂类型。为此，打开模型浏览器->函数导入->右键单击导入的函数'GetCourseListByStudentID' ->单击编辑：
您可以看到EDM已经自动创建了复杂类型 GetCourseListByStudentID_Result 作为返回集合类型。
将Returns a Collection Of更改为Entities，然后选择Course实体，如下所示
现在，您可以将表值函数作为DBContext的函数执行，如下所示：
~~~
using (var ctx = new SchoolDBEntities())
{
    //Execute TVF and filter result
    var courses = ctx.GetCourseListByStudentID(1)
                     .ToList<Course>();
}
~~~