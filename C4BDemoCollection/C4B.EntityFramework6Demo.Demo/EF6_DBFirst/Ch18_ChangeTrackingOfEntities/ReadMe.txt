﻿# Change Tracking in Entity Framework
Entity Framework支持在上下文的生命周期内对加载的实体进行自动更改跟踪。DbChangeTracker类为您提供有关上下文跟踪的当前实体的所有信息。
请注意，每个实体都必须有一个键（主键）属性，以便上下文跟踪。Entity Framework不会在概念模型中添加任何不具有IdentyKey属性的实体。
下面的代码片段显示了上下文类如何跟踪实体及其上发生的更改：
~~~
static void Main(string[] args)
{
    using (var ctx = new SchoolDBEntities())
    {
        Console.WriteLine("Find Student");
        var std1 = ctx.Students.Find(1);
        Console.WriteLine("Context tracking changes of {0} entity.", ctx.ChangeTracker.Entries().Count());
        DisplayTrackedEntities(ctx.ChangeTracker);

        Console.WriteLine("Find Standard");
        var standard = ctx.Standards.Find(1);
        Console.WriteLine("Context tracking changes of {0} entities.", ctx.ChangeTracker.Entries().Count());
        Console.WriteLine("");
        Console.WriteLine("Editing Standard");
        standard.StandardName = "Edited name";
        DisplayTrackedEntities(ctx.ChangeTracker);

        Teacher tchr = new Teacher() { TeacherName = "new teacher" };
        Console.WriteLine("Adding New Teacher");
        ctx.Teachers.Add(tchr);
        Console.WriteLine("");
        Console.WriteLine("Context tracking changes of {0} entities.", ctx.ChangeTracker.Entries().Count());
        DisplayTrackedEntities(ctx.ChangeTracker);

        Console.WriteLine("Remove Student");
        Console.WriteLine("");
        ctx.Students.Remove(std1);
        DisplayTrackedEntities(ctx.ChangeTracker);
    }
}

private static void DisplayTrackedEntities(DbChangeTracker changeTracker)
{
    Console.WriteLine("");
    var entries = changeTracker.Entries();
    foreach (var entry in entries)
    {
        Console.WriteLine("Entity Name: {0}", entry.Entity.GetType().FullName);
        Console.WriteLine("Status: {0}", entry.State);
    }
    Console.WriteLine("");
    Console.WriteLine("---------------------------------------");
}
~~~
Output:
~~~
Find Student
Context tracking changes of 1 entity.

Entity Name: EFTutorials.Student
Status: Unchanged

---------------------------------------
Find Standard
Context tracking changes of 2 entities.

Editing Standard

Entity Name: EFTutorials.Standard
Status: Modified
Entity Name: EFTutorials.Student
Status: Unchanged

---------------------------------------
Adding New Teacher

Context tracking changes of 3 entities.

Entity Name: EFTutorials.Teacher
Status: Added
Entity Name: EFTutorials.Standard
Status: Modified
Entity Name: EFTutorials.Student
Status: Unchanged

---------------------------------------
Remove Student

Entity Name: EFTutorials.Teacher
Status: Added
Entity Name: EFTutorials.Standard
Status: Modified
Entity Name: EFTutorials.Student
Status: Deleted

---------------------------------------
~~~
正如您在上面的示例代码片段和输出中所看到的，无论何时检索、添加、修改或删除实体，上下文都会跟踪实体。请注意，在对实体的任何操作期间，上下文都是活动的。
如果您对超出其范围的实体执行任何操作，上下文将不会跟踪。