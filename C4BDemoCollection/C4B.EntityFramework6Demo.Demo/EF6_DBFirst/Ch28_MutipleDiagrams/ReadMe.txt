﻿# Multiple Diagrams for EDM in Entity Framework 6
大型数据库的实体数据模型（EDM）将所有实体显示在单个可视化设计器中。这使得在设计器上组织和标识实体变得困难。Visual Studio（2012/2015/2017）提供了一种工具，可以将实体数据模型的可视化表示拆分为多个图。
若要为EDM创建新的关系图，请通过右键单击设计器图面打开“模型浏览器”，然后在上下文菜单中选择“模型浏览器”。在模型浏览器中，您将看到EDM的现有图表。
通过右键单击模型浏览器中的Diagrams节点并选择“Add New Diagram”来创建一个新的图，如下所示。
这将创建一个新的图表。您可以按如下所示重命名图表。例如：StudentDiagram.
现在，您可以将实体类型从模型浏览器拖放到新图表中，如下所示：例如将SchoolDBModel下的Entity Types文件夹下的Student和StudentAddress拖到StudentDiagram图表中。
您还可以将实体从现有关系图移动到新关系图，而无需先创建新关系图。
例如，如果要将Teacher和Course实体类型移动到新的图中，则在现有图中选择这些实体，右键单击并从上下文菜单中选择“移动到新图”。

## Include Related Entities
您还可以在一个图中包含特定实体的相关实体。例如，右键单击Student实体→选择“Include Related”。还将包括Standard和Course实体，因为Student包含Standard和Course的引用属性。
此外，您还可以通过右键单击属性→选择移动属性→选择向上/向下等来向上或向下移动属性，如下图所示：

## Difference between Delete Entity and Remove Entity
您可以通过右键单击实体并选择“从图表中移除”来从图表中移除实体。
从图中移除只会从图中移除实体，而'从模型中删除'会从EDM中删除实体，您将无法使用该实体。