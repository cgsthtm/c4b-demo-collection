﻿# Working with Database View in Entity Framework 6 DB-First Approach
可以像使用表一样使用视图。首先，你必须向EDM添加数据库视图。它将视图视为一个实体。因此，除了CUD（创建、更新、删除）操作之外，您可以使用与普通图元相同的方式来使用它。
我们的数据库中已经有一个视图View_StudentCourse，我们希望使用EF 6执行并从中读取数据。
要从db视图读取数据，首先我们需要将其添加到EDM（实体数据模型）中。您可以在创建新EDM或更新现有EDM时添加此选项。让我们更新现有的EDM并添加一个db视图View_StudentCourse。
要更新EDM，请右键单击设计器，然后单击“从数据库更新模型”。中选择一个选项。这将打开一个更新向导，如下所示。现在，展开视图节点并选择适当的视图。
在这里，我们将选择View_StudentCourse视图并单击Finish按钮。这将在EDM设计器中添加一个新实体作为视图名称。
所以现在您可以使用DbContext的实例执行View_studentCourse，如下所示。
~~~
using (var context = new SchoolDBEntities())
{
    var studentAndCourseList = context.View_StudentCourse.ToList();
    
    foreach (var item in studentAndCourseList)
    {
        Console.WriteLine($"Student: {item.StudentName} Course: {item.CourseName}"); 
    }
}
~~~
因此，通过这种方式，您可以使用Entity Framework 6.x处理数据库视图。