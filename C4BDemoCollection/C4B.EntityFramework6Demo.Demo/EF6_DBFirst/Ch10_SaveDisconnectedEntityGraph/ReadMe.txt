﻿# Saving a Disconnected Entity Graph in EF 6
在断开连接的场景中更新实体图是一项复杂的任务，需要仔细的设计考虑。
在调用SaveChages（）方法之前，我们需要识别实体图中每个实体的状态。识别实体状态有不同的模式，我们在使用实体框架设计数据层时需要考虑这些模式。这里我们将使用key属性来标识实体状态。

## Using Key Property
您可以使用每个实体的key（PrimaryKey）属性来确定其状态。如果key属性的值是MySQL数据类型的默认值，则将其视为新实体。否则，将其视为现有实体。
例如，如果一个key属性的数据类型是int，而属性的值是零，那么它就是一个新的实体，因此EF需要执行这个命令。如果一个值非零，那么它是一个现有的实体，EF需要执行UPDATE命令。
下面的示例演示了如何将Student实体图与相关的Standard和Course实体一起保存：
~~~
var student = new Student() { //Root entity (empty key)
        StudentName = "Bill",
        Standard = new Standard()   //Child entity (with key value)
                    {
                        StandardId = 1,
                        StandardName = "Grade 1"
                    },
        Courses = new List<Course>() {
            new Course(){  CourseName = "Machine Language" }, //Child entity (empty key)
            new Course(){  CourseId = 2 } //Child entity (with key value)
        }
    };

using (var context = new SchoolDBEntities())
{
    //mark standard based on StandardId
    context.Entry(student).State = student.StudentId == 0 ? EntityState.Added : EntityState.Modified;

    context.Entry(student.Standard).State = student.Standard.StandardId == 0 ? EntityState.Added : EntityState.Modified;

    foreach (var course in student.Courses)
        context.Entry(course).State = course.CourseId == 0 ? EntityState.Added : EntityState.Modified;
                
    context.SaveChanges();
}
~~~
在上面的示例中，student实体图包括相关的Standard和Course实体。上下文根据键属性值为每个实体设置适当的状态。如果它为零，则设置Added状态，否则设置Modified状态。
SaveChanges（）方法将对每个实体的数据库执行适当的INSERT或UPDATE命令。

# Deleting a Disconnected Entity in EF 6
删除断开连接的实体很容易。只需使用Entry（）方法将其状态设置为Delete，如下所示。
~~~
// disconnected entity to be deleted
var student = new Student(){ StudentId = 1 };

using (var context = new SchoolDBEntities())
{
    context.Entry(student).State = System.Data.Entity.EntityState.Deleted;    

    context.SaveChanges();
}  
~~~