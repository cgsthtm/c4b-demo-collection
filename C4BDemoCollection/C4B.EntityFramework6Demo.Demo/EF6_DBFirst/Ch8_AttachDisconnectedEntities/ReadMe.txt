﻿# Methods to Attach Disconnected Entities in EF 6
在这里，您将了解Entity Framework 6.x中将断开连接的实体图附加到上下文的不同方法。
在断开连接的方案中保存实体与在连接的方案中保存实体不同。当我们得到一个不连通的实体图，甚至是一个不连通的实体时，我们需要做两件事。
首先，我们需要将实体附加到新的上下文实例，并使上下文感知这些实体。第二，手动为每个实体设置适当的Entity State，
因为实例不知道在断开连接的实体上执行的任何操作，所以它不能自动应用适当的Entity State。
Entity Framework提供了以下方法，这些方法可以将断开连接的实体附加到上下文，并为实体图中的每个实体设置EntityState。
- DbContext.Entry()
- DbSet.Add()
- DbSet.Attach()

## DbContext.Entry()
DbContext类的Entry（）方法返回指定实体的DbEntityEntry实例。DbEntityEntry对象提供有关指定实体的各种信息以及对实体执行操作的能力。
最重要的是，我们可以使用State属性更改指定实体的EntityState，如下所示。
~~~
context.Entry(entity).state = EntityState.Added/Modified/Deleted
~~~
Entry方法将整个实体图附加到上下文，并将指定的状态附加到父实体，还将不同的EntityState设置到其他实体。请看下面的例子。
~~~
var student = new Student() { //Root entity (empty key)
        StudentName = "Bill",
        StandardId = 1,
        Standard = new Standard()   //Child entity (with key value)
                    {
                        StandardId = 1,
                        StandardName = "Grade 1"
                    },
        Courses = new List<Course>() {
            new Course(){  CourseName = "Machine Language" }, //Child entity (empty key)
            new Course(){  CourseId = 2 } //Child entity (with key value)
        }
    };

using (var context = new SchoolDBEntities())
{
    context.Entry(student).State = EntityState.Added; 

    foreach (var entity in context.ChangeTracker.Entries()){
        Console.WriteLine("{0}: {1}", entity.Entity.GetType().Name, entity.State);
    } 
}
~~~
Output:
~~~
Student: Added
Standard: Added
Course: Added
Course: Added
~~~
在上面的示例中，Studnet实体图包括Standard和Course实体。 context.Entry(student).State = EntityState.Added; 将父实体以及所有其他子实体设置为Added状态，
而不管子实体是否包含空键值。因此，建议谨慎使用Entry（）方法。

## DbSet.Add()
DbSet.Add（）方法将整个实体图附加到上下文，并自动将Added状态应用于所有实体。
~~~
//disconnected entity graph
Student disconnectedStudent = new Student() { StudentName = "New Student" };
disconnectedStudent.StudentAddress = new StudentAddress() { Address1 = "Address", City = "City1" };

using (var context = new SchoolDBEntities())
{
    context.Students.Add(disconnectedStudent);
                
    // get DbEntityEntry instance to check the EntityState of specified entity
    var studentEntry = context.Entry(disconnectedStudent);
    var addressEntry = context.Entry(disconnectedStudent.StudentAddress);

    Console.WriteLine("Student: {0}", studentEntry.State);
    Console.WriteLine("StudentAddress: {0}", addressEntry.State);
}
~~~
Output:
~~~
Student: Added
StudentAddress: Added
~~~
DbSet.Add（）方法将整个实体图附加到一个上下文中，并将Added状态附加到每个实体。调用context.savechanges（）将对所有实体执行INSERT命令，这将在适当的数据库表中插入新记录。

## DbSet.Attach()
DbSet.Attach（）方法将整个实体图附加到具有Unchanged实体状态的新上下文。
~~~
//disconnected entity graph
Student disconnectedStudent = new Student() { StudentName = "New Student" };
disconnectedStudent.StudentAddress = new StudentAddress() { Address1 = "Address", City = "City1" };

using (var context = new SchoolDBEntities())
{
    context.Students.Attach(disconnectedStudent);
                
    // get DbEntityEntry instance to check the EntityState of specified entity
    var studentEntry = context.Entry(disconnectedStudent);
    var addressEntry = context.Entry(disconnectedStudent.StudentAddress);

    Console.WriteLine("Student: {0}",studentEntry.State);
    Console.WriteLine("StudentAddress: {0}",addressEntry.State);
}
~~~
Output:
~~~
Student: Unchanged
StudentAddress: Unchanged
~~~