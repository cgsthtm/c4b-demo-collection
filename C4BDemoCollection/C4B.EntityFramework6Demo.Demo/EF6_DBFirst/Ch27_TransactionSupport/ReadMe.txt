﻿# Transaction in Entity Framework
在这里，您将了解EF 6.x和EF Core中的事务。
在Entity Framework中，SaveChanges（）方法在内部创建一个transaction，并将所有的update、UPDATE和delete操作包装在它下面。多个SaveChanges（）调用，
创建单独的transaction，执行CRUD操作，然后提交每个transaction。下面的例子说明了这一点。
~~~
using (var context = new SchoolContext())
{
    context.Database.Log = Console.Write;

    var standard = context.Standards.Add(new Standard() { StandardName = "1st Grade" });

    context.Students.Add(new Student()
    {
        FirstName = "Rama",
        StandardId = standard.StandardId
    });

    context.SaveChanges();

    context.Courses.Add(new Course() { CourseName = "Computer Science" });
    context.SaveChanges();
}
~~~

## Multiple SaveChanges in a Single Transaction
EF 6和EF Core允许我们使用以下方法创建或使用具有多个SaveChanges（）调用的单个事务：
1. DbContext.Database.BeginTransaction（）：为底层数据库创建一个新事务，并允许我们使用多个SaveChanges方法调用提交或回滚对数据库所做的更改。
2. DbContext.Database.DataTransaction（）：允许我们传递在上下文对象范围之外创建的现有事务对象。这将允许EF在外部事务对象中执行命令。或者，传入null以清除框架对该事务的了解。

## DbContext.Database.BeginTransaction()
下面的示例演示如何使用BeginTransaction（）创建一个新的transaction对象，然后与多个SaveChanges（）调用一起使用。
~~~
using (var context = new SchoolContext())
{
    context.Database.Log = Console.Write;

    using (DbContextTransaction transaction = context.Database.BeginTransaction())
    {
        try
        {
            var standard = context.Standards.Add(new Standard() { StandardName = "1st Grade" });

            context.Students.Add(new Student()
            {
                FirstName = "Rama2",
                StandardId = standard.StandardId
            });
            context.SaveChanges();

            context.Courses.Add(new Course() { CourseName = "Computer Science" });
            context.SaveChanges();

            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            Console.WriteLine("Error occurred.");
        }
    }
}
~~~
在上面的示例中，我们创建了新的Standard、Student和Course实体，并通过调用两个SaveChanges（）将它们保存到数据库中，这两个SaveChanges（）在一个事务中执行SQL命令。
注意：您可以在 DbContext.Database.BeginTransaction() 方法中指定不同的隔离级别。访问MSDN以了解有关隔离级别的更多信息。
https://msdn.microsoft.com/en-us/library/system.data.isolationlevel(v=vs.113).aspx
如果发生异常，那么对数据库所做的所有更改都将回滚。
~~~
using (var context = new SchoolContext())
{
    context.Database.Log = Console.Write;

    using (DbContextTransaction transaction = context.Database.BeginTransaction())
    {
        try
        {
            var standard = context.Standards.Add(new Standard() { StandardName = "1st Grade" });

            context.Students.Add(new Student()
            {
                FirstName = "Rama",
                StandardId = standard.StandardId
            });
            context.SaveChanges();
            // throw exectiopn to test roll back transaction
            throw new Exception();

            context.Courses.Add(new Course() { CourseName = "Computer Science" });
            context.SaveChanges();

            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            Console.WriteLine("Error occurred.");
        }
    }
}
~~~
在上面的例子中，我们在第一次SaveChanges（）调用后抛出一个异常。这将执行一个catch块，我们在其中调用RollBack（）方法来回滚对数据库所做的任何更改。

## DbContext.Database.UseTransaction()
DbContext.Database.UseTransaction() 方法允许我们使用在上下文对象范围之外创建的现有事务。如果我们使用transaction（）方法，
那么上下文将不会创建内部transaction对象，而是使用提供的transaction。
下面的示例演示了使用EF 6代码优先方法的UseTransaction()方法。
~~~
private static void Main(string[] args)
{
    string providerName = "System.Data.SqlClient";
    string serverName = ".";
    string databaseName = "SchoolDB";

    // Initialize the connection string builder for the SQL Server provider.
    SqlConnectionStringBuilder sqlBuilder =
        new SqlConnectionStringBuilder();

    // Set the properties for the data source.
    sqlBuilder.DataSource = serverName;
    sqlBuilder.InitialCatalog = databaseName;
    sqlBuilder.IntegratedSecurity = true;

    using (SqlConnection con = new SqlConnection(sqlBuilder.ToString()))
    {
        con.Open();
        using (SqlTransaction transaction = con.BeginTransaction())
        {
            try
            {
                using (SchoolContext context = new SchoolContext(con, false))
                {
                    context.Database.UseTransaction(transaction);

                    context.Students.Add(new Student() { Name = "Ravi" });
                    context.SaveChanges();
                }

                using (SchoolContext context = new SchoolContext(con, false))
                {
                    context.Database.UseTransaction(transaction);

                    context.Grades.Add(new Standard() { GradeName = "Grade 1", Section = "A" });
                    context.SaveChanges();
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();

                Console.WriteLine(ex.InnerException);
            }
        }
    }
}
~~~
下面是上面例子中使用的SchoolContext类。
~~~
public class SchoolContext : DbContext
{
    public SchoolContext(DbConnection con, bool contextOwnsConnection) :base(con, contextOwnsConnection)
    {

    }
    public SchoolContext(): base("SchoolDB")
    {
        Database.SetInitializer<SchoolContext>(new CreateDatabaseIfNotExists<SchoolContext>());
    }

    public DbSet<Student> Students { get; set; }
    public DbSet<Standard> Grades { get; set; }
    public DbSet<Course> Courses { get; set; }
}
~~~