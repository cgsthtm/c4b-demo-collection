﻿# Relationships between Entities in Entity Framework 6
实体框架支持三种类型的关系，与数据库相同：1）一对一 2）一对多 3）多对多。

## One-to-One Relationship
Student和StudentAddress具有一对一关系（零或一）。一个学生只能有一个或零个地址。实体框架将Student引用导航属性添加到StudentAddress实体中，
并将StudentAddress导航实体添加到Student实体中。此外，StudentAddress实体具有作为PrimaryKey和ForeignKey的StudentId属性，这使其成为一对一关系。
~~~
public partial class Student
{
    public Student()
    {
        this.Courses = new HashSet<Course>();
    }
    
    public int StudentID { get; set; }
    public string StudentName { get; set; }
    public Nullable<int> StandardId { get; set; }
    public byte[] RowVersion { get; set; }
    
    public virtual StudentAddress StudentAddress { get; set; }
}
    
public partial class StudentAddress
{
    public int StudentID { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    
    public virtual Student Student { get; set; }
}
~~~
在上面的示例中，StudentId属性需要是PrimaryKey和ForeignKey。这可以在上下文类的OnModelCreating方法中使用Fluent API进行配置。

## One-to-Many Relationship
Standard和Teacher实体具有一对多关系，标记为多重性，其中1表示一，* 表示多。这意味着标准可以有多个教师，而教师只能与一个标准相关联。
为了表示这一点，Standard实体具有集合导航属性Teachers（请注意，它是复数），这表明一个Standard可以有一个Teachers（许多教师）的集合。教师实体有一个标准导航属性（引用属性），
它指示教师与一个标准相关联。此外，它还包含StandardId外键（Standard实体中的PK）。这使其成为一对多关系。
~~~
public partial class Standard
{
    public Standard()
    {
        this.Teachers = new HashSet<Teacher>();
    }
    
    public int StandardId { get; set; }
    public string StandardName { get; set; }
    public string Description { get; set; }
    
    public virtual ICollection<Teacher> Teachers { get; set; }
}

public partial class Teacher
{
    public Teacher()
    {
        this.Courses = new HashSet<Course>();
    }
    
    public int TeacherId { get; set; }
    public string TeacherName { get; set; }
    public Nullable<int> TeacherType { get; set; }
    
    public Nullable<int> StandardId { get; set; }
    public virtual Standard Standard { get; set; }
}
~~~

## Many-to-Many Relationship
Student和Course具有标记为 * 多样性的多对多关系。这意味着一个学生可以注册多门课程，也可以教一门课程给多个学生。数据库包括StudentCourse连接表，
该表包括两个表（Student和Course表）的主键。实体框架通过在CSDL和可视化设计器中不为连接表设置实体（DbSet属性）来表示多对多关系。相反，它通过映射来管理。
Student实体包括集合导航属性Courses，Course实体包括集合导航属性Students，以表示它们之间的多对多关系。
~~~
public partial class Student
{
    public Student()
    {
        this.Courses = new HashSet<Course>();
    }
    
    public int StudentID { get; set; }
    public string StudentName { get; set; }
    public Nullable<int> StandardId { get; set; }
    public byte[] RowVersion { get; set; }
    
    public virtual ICollection<Course> Courses { get; set; }
}
    
public partial class Course
{
    public Course()
    {
        this.Students = new HashSet<Student>();
    }
    
    public int CourseId { get; set; }
    public string CourseName { get; set; }
    public System.Data.Entity.Spatial.DbGeography Location { get; set; }
    
    public virtual ICollection<Student> Students { get; set; }
}
~~~
只有当连接表（本例中为StudentCourse）不包括两个表的PK以外的任何列时，实体框架才支持多对多关系。如果连接表包含其他列，比如DateCreated，
那么EDM也会为中间表创建一个实体，您将不得不手动管理多对多实体的CRUD操作。
在XML视图中打开EDM。您可以看到SSDL（存储模式）有StudentCourse实体集，但CSDL没有，而是映射到Student和Course实体的navigation属性中。
MSL（C-S映射）将Student和Course之间的映射放入<AssociationSetMapping/>中的StudentCourse表中。
因此，多对多关系由EDM中的C-S映射管理。因此，当您在Course中添加Student或在Student实体中添加Course并保存时，它将在StudentCourse表中插入添加的学生和课程的PK。
因此，这种映射不仅可以在两个实体之间直接实现方便的关联，还可以跨这些连接管理查询、插入和更新。