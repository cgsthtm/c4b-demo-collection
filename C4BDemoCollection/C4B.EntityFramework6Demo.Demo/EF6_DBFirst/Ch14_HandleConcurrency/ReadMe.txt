﻿# Concurrency in Entity Framework
Entity Framework默认支持开放式并发。EF将实体数据保存到数据库中，假设自实体加载以来相同的数据没有被更改。
如果它发现数据已更改，则会引发异常，您必须在尝试再次保存数据之前解决冲突。
要使用EF 6数据库优先方法处理并发，请在SQL Server的表中创建一个具有rowversion（时间戳）数据类型的列。 https://docs.microsoft.com/en-us/sql/t-sql/data-types/rowversion-transact-sql
rowversion（同义词时间戳）数据类型只是一个递增的二进制数，在包含rowversion列的表上执行每个插入和更新操作时，它都会递增。
让我们为Student实体实现并发。
为了检查Student实体的并发性，Student表必须具有rowversion列。因此，在Student表中创建一个名为RowVersion的新列，其数据类型为timestamp（timestamp是rowversion的同义数据类型）.
现在，创建一个新的实体数据模型，如创建实体数据模型一章所示，或者，如果您已经有一个EDM，则通过右键单击设计器->从数据库更新模型->刷新学生表来更新它。
您将看到RowVersion属性添加到设计器视图的Student实体中。
现在，您需要通过右键单击设计器上Student实体的RowVersion属性将并发模式设置为Fixed，选择Property并在属性窗口中将Concurrency Mode设置为Fixed.
EF API现在将在每个UPDATE命令的where子句中包含此RowVersion列，如果rowversion值与数据库中的现有值不同，则它将抛出DbUpdateConcurrencyException。
下面的示例演示如何处理并发异常：
~~~
Student student = null; 

using (var context = new SchoolDBEntities())
{
    student = context.Students.First();
}

//Edit student name
Console.Write("Enter New Student Name:");
student.StudentName = Console.ReadLine(); //Assigns student name from console

using (var context = new SchoolDBEntities())
{
    try
    {
        context.Entry(student).State = EntityState.Modified;
        context.SaveChanges();

        Console.WriteLine("Student saved successfully.");
    }
    catch (DbUpdateConcurrencyException ex)
    {
        Console.WriteLine("Concurrency Exception Occurred.");
    }
}
~~~
假设有两个用户执行上面的代码。User1和User2都获得相同的Student实体。现在，User1输入一个新的学生姓名，并在User2之前保存它。
因此，用户1将得到一个“学生成功保存”的消息，而用户2将得到“并发异常已删除”的消息。".

## Further Reading
使用EF 6和EF Core中的timestamp属性以代码优先的方式处理并发。https://www.entityframeworktutorial.net/code-first/concurrencycheck-dataannotations-attribute-in-code-first.aspx