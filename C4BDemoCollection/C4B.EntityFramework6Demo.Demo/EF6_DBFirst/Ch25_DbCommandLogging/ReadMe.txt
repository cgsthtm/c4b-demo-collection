﻿# Logging Database Commands in Entity Framework 6
在EF 6之前，我们使用数据库跟踪工具或第三方跟踪实用程序来跟踪实体框架发送的数据库查询和命令。现在，EF 6提供了DbContext.Database.Log属性来记录DbContext生成的SQL。
Log属性的类型为Action<string>，因此您可以使用string参数附加委托方法并返回void。
~~~
using (var context = new SchoolDBEntities())
{
    context.Database.Log = Console.Write;
    var student = context.Students
                        .Where(s => s.StudentName == "Student1")
                        .FirstOrDefault<Student>();

    student.StudentName = "Edited Name";
    context.SaveChanges();
}
~~~
在上面的示例中，Console.Write（）方法被附加到Log属性，因为它接受字符串参数并返回void。你可以在输出中看到它记录了EF执行的所有活动，例如打开和关闭连接，
执行和完成时间以及数据库查询和命令。
可以将自定义类的方法附加到Log属性。下面的示例使用自定义类的方法记录SQL。
~~~
public class Logger
{
    public static void Log(string message)
    {
        Console.WriteLine("EF Message: {0} ", message);
    }
}

class EF6Demo
{
    public static void DBCommandLogging()
    {
        using (var context = new SchoolDBEntities())
        {
                
            context.Database.Log =  Logger.Log;                
            var student = context.Students
                                .Where(s => s.StudentName == "Student1")
                                .FirstOrDefault<Student>();

            student.StudentName = "Edited Name";
            context.SaveChanges();
        }
    }
}
~~~