﻿# Spatial Data Type in Entity Framework
MS SQL Server 2008引入了两种空间数据类型：geography地理和geometry几何。地理类型表示地球圆坐标系中的数据，几何图形表示欧几里德（平面）坐标系中的数据。
从EF 5.0版本开始，它在System.Data.Entity.Spatial命名空间中包含特殊的数据类型：DbGeography用于地理数据类型，DbGeometry用于SQL Server中的几何数据类型。
于演示目的，我们使用了SQL Server数据库中geography类型的Course表中的Location列，如下所示：
现在，在数据库中添加地理列后，创建或更新实体数据模型（.edmx）。创建或更新EDM后，您可以看到Course实体的Location属性为 System.Data.Spatial.DBGeography ，如下所示：
~~~
public partial class Course
{
    public Course()
    {
        this.Students = new HashSet<Student>();
    }
    
    public int CourseId { get; set; }
    public string CourseName { get; set; }
    public Nullable<int> TeacherId { get; set; }
    public System.Data.Spatial.DbGeography Location { get; set; }
    
    public virtual Teacher Teacher { get; set; }
    public virtual ICollection<Student> Students { get; set; }
}
~~~
现在可以在添加或更新实体时使用Location属性，如下所示。
~~~
using (var ctx = new SchoolDBEntities())
{
    ctx.Courses.Add(new Course() { 
                CourseName = "New Course", 
                Location = DbGeography.FromText("POINT(-122.360 47.656)") 
        });

    ctx.SaveChanges();
}
~~~
有关MS SQL Server中地理数据类型的详细信息，请访问MSDN。https://learn.microsoft.com/en-us/sql/t-sql/spatial-geography/spatial-types-geography?view=sql-server-ver16