﻿# CRUD Operations using Stored Procedure in Entity Framework
在前一章中，我们已经看到了如何使用存储过程读取数据。在本章中，我们将在数据库优先方法中调用SaveChanges（）方法时，使用存储过程对实体进行CUD（创建、更新、删除）操作。
我们将为Student实体使用以下存储过程：
- sp_InsertStudentInfo：将新学生记录插入数据库
- sp_UpdateStudent：更新学生记录
- sp_DeleteStudent：删除数据库中的学生记录。
下面是每个存储过程的SQL脚本。

Sp_InsertStudentInfo:
~~~
CREATE PROCEDURE [dbo].[sp_InsertStudentInfo]
    -- Add the parameters for the stored procedure here
    @StandardId int = null,
    @StudentName varchar(50)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

        INSERT INTO [SchoolDB].[dbo].[Student]([StudentName],[StandardId])
        VALUES(@StudentName, @StandardId)

    SELECT SCOPE_IDENTITY() AS StudentId

END
~~~

sp_UpdateStudent:
~~~
CREATE PROCEDURE [dbo].[sp_UpdateStudent]
    -- Add the parameters for the stored procedure here
    @StudentId int,
    @StandardId int = null,
    @StudentName varchar(50)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    Update [SchoolDB].[dbo].[Student] 
    set StudentName = @StudentName,StandardId = @StandardId
    where StudentID = @StudentId;

END
~~~

sp_DeleteStudent
~~~
CREATE PROCEDURE [dbo].[sp_DeleteStudent]
    -- Add the parameters for the stored procedure here
    @StudentId int
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    DELETE FROM [dbo].[Student]
    where StudentID = @StudentId

END
~~~

首先，您需要更新现有的EDM，以便将这些存储过程添加到EDM中。右键单击您的设计器，然后单击从数据库更新模型。打开更新向导。展开Stored Procedures and Functions节点，
选择上面的存储过程并取消选中Import selected stored procedures and function into the entity model复选框，因为我们将直接将这些过程映射到Student实体。
点击完成按钮。模型浏览器将在存储模型部分中显示过程，但不在函数导入部分中显示，如下所示。
您需要将这些存储过程映射到Student实体。在EDM设计器中，右键单击Student实体并选择StoredProcedureMapping以打开Mapping详细信息，如下所示。
在映射详细信息中，如下所示，您将看到<Select插入函数>、<Select更新函数>和<Select删除函数>。为实例中的每个存储过程选择适当的存储过程，
例如，为Insert函数选择sp_InsertStudentInfo，为update函数选择sp_UpdateStudent，为delete函数选择sp_DeleteStudent。
sp_InsertStudentInfo返回StudentId的值，并将其与Student实体的StudentID进行映射，如下所示：
使用Student实体的相应属性完成插入、更新和删除过程参数的映射，如下所示。
现在，我们需要在执行之前验证它，以确保不会出现运行时错误。要完成此操作，请右键单击设计器中的Student实体，然后单击“Validate”，并确保没有警告或错误。
现在，无论何时添加、更新或删除Student实体，EF都将使用这些存储过程进行CUD操作，而不是执行SQL命令。下面的示例演示了这一点：
~~~
using (var context = new SchoolDBEntities())
{
    Student student = new Student() { StudentName = "New student using SP"};

    context.Students.Add(student);
    //will execute sp_InsertStudentInfo 
    context.SaveChanges();

    student.StudentName = "Edit student using SP";
    //will execute sp_UpdateStudent
    context.SaveChanges();

    context.Students.Remove(student);
    //will execute sp_DeleteStudentInfo 
    context.SaveChanges();
}
~~~
上面的示例将在每次SaveChanges（）调用时执行以下语句：
~~~
exec [dbo].[sp_InsertStudentInfo] @StandardId=NULL,@StudentName='New student using SP'
go

exec [dbo].[sp_UpdateStudent] @StudentId=47,@StandardId=NULL,@StudentName='Edit student using SP'
go

exec [dbo].[sp_DeleteStudent] @StudentId=47
go
~~~
注意：在添加新学生后执行SaveChanges方法后，它将为StudentID属性分配一个数据库生成的值。为了跟踪它并对该实体对象执行进一步的操作，这是必要的。