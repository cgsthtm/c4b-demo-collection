﻿# Working with Enum in EF 6 DB-First
Enum在Entity Framework 6中得到支持。可以为以下数据类型创建枚举：
- Int16
- Int32
- Int64
- Byte
- SByte
枚举可以通过以下方式使用：
1. 从EDM设计器将实体的现有属性转换为枚举类型。
2. 使用来自不同命名空间的现有枚举类型。

## Convert an Existing Property to Enum
在这里，我们将在Teacher表中使用TeacherType整数列。枚举值1用于永久教师，2用于合同制教师，3用于客座教师。
现在，要从设计器中将TeacherType属性转换为枚举类型，请右键单击Teacher实体的TeacherType属性，然后单击上下文菜单中的Convert to Enum
它将打开“添加枚举类型”对话框。输入枚举类型名称，并在“底层类型”选项卡中选择int32。输入枚举成员名称，如下所示。
这将在模型浏览器中添加TeacherType作为Enum Type，如下所示：
此外，您可以看到TeacherType属性的类型转换为TeacherTypeEnum：
现在，您可以使用TeacherType枚举来指定TeacherType的值而不是整数值，如下所示。
~~~
using (var ctx = new SchoolDBEntities())
{
    Teacher tchr = new Teacher();
    tchr.TeacherName = "New Teacher";

    //assign enum value
    tchr.TeacherType = TeacherType.Permanent;

    ctx.Teachers.Add(tchr);

    ctx.SaveChanges();
}
~~~

## Use an Existing Enum from a Different Namespace
如果您已经在代码中创建了Enum类型，则可以将其用作任何实体属性的数据类型。
要使用现有的枚举类型，请右键单击Designer → Add New → Enum Type。在对话框中输入枚举类型名称。不要输入成员，因为您的代码中已经有了该成员。
现在，选择“引用外部类型”复选框，输入现有枚举的名称空间，然后单击确定。这将在模型浏览器中添加枚举类型。现在，您可以从属性窗口中将此Enum类型分配给任何实体的任何适当属性。
注意：如果要对枚举使用位运算符，请选择“设置标志属性”。