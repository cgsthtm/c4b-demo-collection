﻿# DbContext Class in Entity Framework 6 DB-First Approach
正如您在前面的创建实体数据模型一节中所看到的，EDM包括SchoolDBEntities类，它派生自System.Data.Entity.DbContext类。派生DbContext的类在实体框架中称为上下文类。
DbContext是Entity Framework API中的一个重要类。它是域或实体类与数据库之间的桥梁。
DbContext是负责与数据库交互的主要类。它负责以下活动：
- Querying查询：将LINQ-to-Entities查询转换为SQL查询并发送到数据库。
- Change Tracking更改跟踪：跟踪从数据库查询后实体上发生的更改。
- Persisting Data持久化数据：基于实体状态对数据库执行插入、更新和删除操作。
- Caching缓存：默认情况下提供第一级缓存。它存储在上下文类的生命周期中检索到的实体。
- Manage Relationship管理关系：以数据优先或模型优先的方式使用CSDL、MSL和SSDL管理关系，并以代码优先的方式使用流畅的API配置。
- Object Materialization对象物化：将数据库中的原始数据转化为实体对象。

## DbContext Methods
- Entry: 获取给定实体的DbIdentyEntry。该条目提供对实体的更改跟踪信息和操作的访问。
- SaveChanges: 对具有Added、Modified和Reduce状态的实体执行数据库的UPDATE、UPDATE和REPORT命令。
- SaveChangesAsync: SaveChanges（）的异步方法
- Set: 创建一个DbSet<TEntity>，它可用于查询和保存TEntity的实例。
- OnModelCreating: 重写此方法以进一步配置按约定从派生上下文中DbSet<TIdenty>属性中公开的实体类型发现的模型。

## DbContext Properties
- ChangeTracker: 提供对此上下文正在跟踪的实体实例的信息和操作的访问。
- Configuration: 提供对配置选项的访问。
- Database: 提供对数据库相关信息和操作的访问。