﻿# Code-based Configuration in Entity Framework 6
Entity Framework 6引入了基于代码的配置。现在，您可以使用之前在app.config的<entytyframework>部分中配置的代码配置实体框架相关设置。
但是，app.config优先于基于代码的配置。换句话说，如果在代码和app.config中都设置了配置选项，那么将使用app.config中的设置。
让我们看看如何使用实体框架6实现基于代码的配置。
首先，您需要创建一个派生DbConfiguration（ System.Data.Entity.DbConfiguration ）类的新类：
~~~
public class FE6CodeConfig : DbConfiguration
{
    public FE6CodeConfig()
    {
        //define configuration here
    }
}
~~~
现在，您可以在app.config中设置codeConfigurationType属性，如下所示：
~~~
<entityFramework codeConfigurationType="EF6DBFirstTutorials.FE6CodeConfig, EF6DBFirstTutorials">
</entityFramework>
~~~
或者，您可以使用上下文类上的DbConfigurationType属性来设置基于代码的配置类：
~~~
[DbConfigurationType(typeof(EF6CodeConfig))]
public partial class SchoolDBEntities : DbContext
{
    public SchoolDBEntities() : base("name=SchoolDBEntities")
    {
    }
}
~~~
注意：EF不支持在同一AppDomain中使用多个配置类。如果使用此属性为两个上下文设置不同的配置类，则将引发异常。
现在，您可以在构造函数中使用DbConfiguration的不同方法，如下所示：
~~~
public class FE6CodeConfig : DbConfiguration
{
    public FE6CodeConfig()
    {
        //define configuration here
        this.
    }
}
~~~
让我们看看如何使用基于代码的配置以及app.config应用不同的设置。

## Configuring the Default Connection Factory
使用SetDefaultConnectionFactory（）方法可以配置默认连接工厂，如SQL Server的SqlConnectionFactory，如下所示。
~~~
public class FE6CodeConfig : DbConfiguration
{
    public FE6CodeConfig()
    {
        this.SetDefaultConnectionFactory(new System.Data.Entity.Infrastructure.SqlConnectionFactory());
    }
}
~~~
默认连接工厂在app.config中配置，如下所示。
~~~
<entityFramework>
    <defaultConnectionFactory type="System.Data.Entity.Infrastructure.SqlConnectionFactory, EntityFramework" />
</entityFramework>
~~~

## Setting Database Provider
使用SetProviderServices（）方法配置数据库提供程序，如下所示。
~~~
public class FE6CodeConfig : DbConfiguration
{
    public FE6CodeConfig()
    {
        this.SetProviderServices("System.Data.SqlClient", 
                System.Data.Entity.SqlServer.SqlProviderServices.Instance);
    }
}
~~~
数据库提供程序可以在EF6的app.config中配置，如下所示。
~~~
<entityFramework>
    <providers>
        <provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer" />
    </providers>
</entityFramework>
~~~

## Setting Database Initializers
您可以使用基于代码的配置来设置数据库初始值设定项（仅适用于Code-First），如下所示：
~~~
public class FE6CodeConfig : DbConfiguration
{
    public FE6CodeConfig()
    {
        this.SetDatabaseInitializer<SchoolDBEntities>(new CustomDBInitializer<SchoolDBEntities>());
    }
}
~~~
同样的事情可以在app.config中配置，如下所示。
~~~
<entityFramework>
    <contexts>
        <context type="EF6DBFirstTutorials.SchoolDBEntities, EF6DBFirstTutorials"  >
            <databaseInitializer   type="EF6DBFirstTutorials.CustomDBInitializer , EF6DBFirstTutorials">
            </databaseInitializer>
        </context>
    </contexts>    
</entityFramework>
~~~
