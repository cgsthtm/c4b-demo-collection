﻿# Stored Procedure in Entity Framework 6 DB-First Approach
Entity Framework能够根据您的LINQ-to-Entities或Entity SQL查询自动为数据库构建本地命令，以及构建用于插入，更新和删除数据的命令。您可能希望重写这些步骤，
并使用自己的预定义存储过程。您可以使用存储过程来获取数据或添加/更新/删除一个或多个数据库表的记录。
EF API为目标数据库中的每个存储过程和用户定义函数（UDF）创建一个函数，而不是EDM中的实体。
让我们使用存储过程从数据库中获取数据。
首先，在SQL Server数据库中创建以下存储过程GetCoursesByStudentId。此过程返回分配给特定学生的所有课程。
~~~
CREATE PROCEDURE [dbo].[GetCoursesByStudentId]
    -- Add the parameters for the stored procedure here
    @StudentId int = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    -- Insert statements for procedure here
select c.courseid, c.coursename,c.Location, c.TeacherId
from student s 
left outer join studentcourse sc on sc.studentid = s.studentid 
left outer join course c on c.courseid = sc.courseid
where s.studentid = @StudentId
END
~~~
现在，通过在Visual Studio（2012/2015/2017）中的解决方案资源管理器中右键单击项目来添加新的实体数据模型->添加->新项。这将打开一个添加新项弹出窗口。在弹出窗口中，
选择ADO.NET Entity Data Model并为EDM提供适当的名称，然后单击Add按钮。这将打开实体数据模型向导。从数据库中选择EF Designer，然后单击下一步按钮，如下图所示。
接下来，您需要创建与现有数据库的连接。如果这是您第一次为数据库创建EDM，则需要通过单击“新建连接”来创建新连接。而我们已经有了一个连接，所以从数据库中选择数据库，然后单击Next按钮。
在此步骤中，选择“存储过程和函数”下的GetCoursesByStudentId。确保选中了Importselected stored procedures and functions into the entity model复选框，然后单击Finish。
您将看到GetCoursesByStudentId存储过程添加到下面所示的存储过程/函数和函数导入中，并在模型浏览器中添加了新的复杂类型GetCoursesByStudentId_Result。
每当您将存储过程或UDF导入到EDM中时，它都会创建一个新的复杂类型，默认名称为{sp name}_Result。
GetCoursesByStudentId返回在Course实体中定义的相同字段。因此，我们不需要为GetCoursesByStudentId添加新的复杂类型。您可以通过右键单击函数导入中的GetCoursesByStudentId并选择编辑来更改它。
要将Course实体设置为结果类型，请选择Entities，然后在弹出窗口中选择Coursefrom Currency，然后单击OK。
这将在上下文（从DbContext派生）类中添加名为GetCoursesByStudentId的函数。
现在，您可以使用GetCoursesByStudentId作为函数并获取数据，如下所示：
~~~
using (var context = new SchoolDBEntities())
{
    var courses = context.GetCoursesByStudentId(1);

    foreach (Course cs in courses)
        Console.WriteLine(cs.CourseName);
}
~~~
上面的例子将在数据库中执行以下语句：
~~~
exec [dbo].[GetCoursesByStudentId] @StudentId=1
~~~
在下一章中学习如何使用存储过程进行SQL、UPDATE和UPDATE操作，而不是SQL命令。