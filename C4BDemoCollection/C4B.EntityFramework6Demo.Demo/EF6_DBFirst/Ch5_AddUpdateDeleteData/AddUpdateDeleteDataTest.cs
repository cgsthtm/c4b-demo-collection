﻿// <copyright file="AddUpdateDeleteDataTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.EntityFramework6Demo.Demo.EF6_DBFirst.Ch5_AddUpdateDeleteData
{
    using System.Linq;
    using C4B.EntityFramework6Demo.Demo.EF6_DBFirst.Ch1_CraeteEntityDataModel;
    using Xunit;

    /// <summary>
    /// AddUpdateDeleteDataTest.
    /// </summary>
    public class AddUpdateDeleteDataTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        [Fact]
        public static void Test()
        {
            /*System.Data.Entity.Core.MetadataException: 'Schema specified is not valid. Errors:
The mapping of CLR type to EDM type is ambiguous because multiple CLR types match the EDM type 'Grade'. Previously found CLR type 'C4B.EntityFramework6Demo.Demo.EF6_DBFirst.Ch1_CraeteEntityDataModel.Grade', newly found CLR type 'C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch1_SampleCodeFirstExample.Grade'.
The mapping of CLR type to EDM type is ambiguous because multiple CLR types match the EDM type 'Grade'. Previously found CLR type 'C4B.EntityFramework6Demo.Demo.EF6_DBFirst.Ch1_CraeteEntityDataModel.Grade', newly found CLR type 'C4B.EntityFramework6Demo.Demo.EF6_CodeFirst.Ch10_ConfigureOneToMany.Grade'.'
             */
            // 解决办法：修改DBFirst_Ch1_CreateEDM数据库中的Grades表增加一列使其与其他Grades类的属性不完全一致，双击.edmx选择Model Browser上的.edmx右键
            // 选择从数据库更新模型，保存.edmx的修改，此时就不会ambiguous了。
            using (var ctx = new DBFirst_Ch1_CreateEDMEntities())
            {
                // Insert Data
                var course1 = ctx.Courses.Add(new Cours() { CourseId = 66, CourseName = "Python111" });
                ctx.SaveChanges();
                Assert.Equal("Python111", course1.CourseName);

                // Updating Data
                var course2 = ctx.Courses.Find(66);
                course2.CourseName = "Python";
                ctx.SaveChanges();
                var course22 = ctx.Courses.Find(66);
                Assert.Equal("Python", course22.CourseName);

                // Deleting Data
                var std = ctx.Students.First<Student>();
                if (std != null)
                {
                    ctx.Students.Remove(std);
                    ctx.SaveChanges();
                    var std2 = ctx.Students.Find(std.StudentId);
                    Assert.Null(std2);
                }
            }
        }
    }
}
