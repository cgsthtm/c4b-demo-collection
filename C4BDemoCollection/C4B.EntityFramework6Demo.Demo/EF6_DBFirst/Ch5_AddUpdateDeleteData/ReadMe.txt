﻿# Add, Update, and Delete Data using Entity Framework
在Entity Framework中，有两种保存实体数据的持久性场景：连接和断开。在连接的场景中，在检索和保存实体时使用相同的DbContext实例，而在断开连接的场景中则不同。
在本章中，您将了解如何在连接的场景中保存数据。
当调用DbContext.SaveChanges（）方法时，Entity Framework会为那些其EntityState为Added、Modified或Deleted的实体构建并执行INSERT、UPDATE和DELETE statements语句。
在连接的场景中，DbContext的一个实例跟踪所有实体，因此，每当创建、修改或删除实体时，它都会自动为每个实体设置一个适当的EntityState。

## Insert Data
使用DbSet.Add方法将新实体添加到上下文（DbContext的实例）中，这将在调用SaveChanges（）方法时在数据库中插入新记录。
~~~
using (var context = new SchoolDBEntities())
{
    var std = new Student()
    {
        FirstName = "Bill",
        LastName = "Gates"
    };
    context.Students.Add(std);

    context.SaveChanges();
}
~~~
在上面的示例中，context.Students.Add（std）将新创建的Student实体实例添加到带有Added EntityState的上下文中。context.SaveChanges（）方法构建并执行以下对数据库的SQL语句。
~~~
exec sp_executesql N'INSERT [dbo].[Students]([FirstName], [LastName])
VALUES (@0, @1)
SELECT [StudentId]
FROM [dbo].[Students]
WHERE @@ROWCOUNT > 0 AND [StudentId] = scope_identity()',N
''@0 nvarchar(max) ,@1 nvarchar(max) ',@0=N'Bill',@1=N'Gates'
go
~~~

## Updating Data
在连接场景中，EF API跟踪使用上下文检索的所有实体。因此，当您编辑实体数据时，EF会自动将EntityState标记为Modified，这会在您调用SaveChanges（）方法时在数据库中生成更新的语句。
~~~
using (var context = new SchoolDBEntities())
{
    var std = context.Students.First<Student>(); 
    std.FirstName = "Steve";
    context.SaveChanges();
}
~~~
上面的例子中，我们使用 context.Students.First<student>() 从数据库中检索第一个学生。一旦我们修改了FirstName，上下文就会将其属性状态设置为Modified，
因为修改是在DbContext实例（context）的作用域中执行的。因此，当我们调用SaveChanges（）方法时，它会在数据库中构建并执行以下Update语句。
~~~
exec sp_executesql N'UPDATE [dbo].[Students]
SET [FirstName] = @0
WHERE ([StudentId] = @1)',
N'@0 nvarchar(max) ,@1 int',@0=N'Steve',@1=2
Go
~~~
在更新语句中，EF API包括具有修改值的属性，其他属性被忽略。在上面的示例中，只编辑了FirstName属性，因此update语句只包括FirstName列。

## Deleting Data
使用DbSet.Remove（）方法删除数据库表中的记录。
~~~
using (var context = new SchoolDBEntities())
{
    var std = context.Students.First<Student>();
    context.Students.Remove(std);

    context.SaveChanges();
}
~~~
在上面的例子中，context.Students.Remove（std）将std实体对象标记为“Deleted”。因此，EF将在数据库中构建并执行下面的SQL语句。
~~~
exec sp_executesql N'DELETE [dbo].[Students]
WHERE ([StudentId] = @0)',N'@0 int',@0=1
Go
~~~
因此，在连接的场景中，在Entity Framework 6.x中添加、更新或删除数据非常容易。

## Further Reading
在EF Core中保存断开连接场景中的数据。 https://www.entityframeworktutorial.net/efcore/saving-data-in-disconnected-scenario-in-ef-core.aspx