﻿# DbEntityEntry Class in EntityFramework
DbEntityEntry是一个重要的类，用于检索有关实体的各种信息。您可以使用DbContext的Entry方法获取特定实体的DBEntityEntry实例。举例来说：
~~~
DbEntityEntry studentEntry = dbcontext.Entry(entity);
~~~
DbEntityEntry使您能够访问实体状态以及给定实体的所有属性的当前值和原始值。下面的示例代码显示如何检索特定实体的重要信息。
~~~
using (var dbCtx = new SchoolDBEntities())
{
    //get student whose StudentId is 1
    var student = dbCtx.Students.Find(1);

    //edit student name
    student.StudentName = "Edited name";

    //get DbEntityEntry object for student entity object
    var entry = dbCtx.Entry(student);

    //get entity information e.g. full name
    Console.WriteLine("Entity Name: {0}", entry.Entity.GetType().FullName);

    //get current EntityState
    Console.WriteLine("Entity State: {0}", entry.State );

    Console.WriteLine("********Property Values********");

    foreach (var propertyName in entry.CurrentValues.PropertyNames )
    {
        Console.WriteLine("Property Name: {0}", propertyName);

        //get original value
        var orgVal = entry.OriginalValues[propertyName];
        Console.WriteLine("     Original Value: {0}", orgVal);
                    
        //get current values
        var curVal = entry.CurrentValues[propertyName];
        Console.WriteLine("     Current Value: {0}", curVal);
    }
}
~~~
Output:
~~~
Entity Name: Student
Entity State: Modified
********Property Values********
Property Name: StudentID
Original Value: 1
Current Value: 1
Property Name: StudentName
Original Value: First Student Name
Current Value: Edited name
Property Name: StandardId
Original Value:
Current Value:
~~~
DBEntityEntry使您能够设置一个EntityState：
~~~
context.Entry(student).State = System.Data.Entity.EntityState.Modified;
~~~
