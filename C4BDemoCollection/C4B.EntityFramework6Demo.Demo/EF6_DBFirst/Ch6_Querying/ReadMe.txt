﻿# Querying with Entity Framework
您可以使用Entity Framework构建和执行查询，以从底层数据库获取数据。EF 6支持不同类型的查询，这些查询又可以转换为底层数据库的SQL查询。
实体框架支持三种类型的查询：1）LINQ-to-Entities，2）Entity SQL，3）Native SQL

## LINQ-to-Entities
Language-Integrated Query（LINQ）是Visual Studio 2008中引入的一种功能强大的查询语言。顾名思义，LINQ-to-Entities查询对实体集（DbSet类型属性）进行操作，
以访问底层数据库中的数据。使用EDM进行查询时，可以使用LINQ方法语法或查询语法。请访问LINQ Tuesday https://www.tutorialsteacher.com/linq，逐步学习LINQ。
以下示例LINQ-to-Entities查询从数据库的Student表中获取数据。
LINQ Method syntax: LINQ方法语法：
~~~
//Querying with LINQ to Entities 
using (var context = new SchoolDBEntities())
{
    var query = context.Students
                       .where(s => s.StudentName == "Bill")
                       .FirstOrDefault<Student>();
}
~~~
LINQ Query syntax: LINQ查询语法：
~~~
using (var context = new SchoolDBEntities())
{
    var query = from st in context.Students
                where st.StudentName == "Bill"
                select st;
   
    var student = query.FirstOrDefault<Student>();
}
~~~

## Entity SQL
实体SQL是创建查询的另一种方法。它由实体框架的对象服务直接处理。它返回ObjectQuery而不是IQueryable。
您需要一个ObjectContext来使用Entity SQL创建查询。
~~~
//Querying with Object Services and Entity SQL
string sqlString = "SELECT VALUE st FROM SchoolDBEntities.Students " +
                    "AS st WHERE st.StudentName == 'Bill'";
    
var objctx = (ctx as IObjectContextAdapter).ObjectContext;
                
ObjectQuery<Student> student = objctx.CreateQuery<Student>(sqlString);
Student newStudent = student.First<Student>();
~~~
您也可以使用EntityConnection和EntityCommand来执行Entity SQL，如下所示：
~~~
using (var con = new EntityConnection("name=SchoolDBEntities"))
{
    con.Open();
    EntityCommand cmd = con.CreateCommand();
    cmd.CommandText = "SELECT VALUE st FROM SchoolDBEntities.Students as st where st.StudentName='Bill'";
    Dictionary<int, string> dict = new Dictionary<int, string>();
    using (EntityDataReader rdr = cmd.ExecuteReader(CommandBehavior.SequentialAccess | CommandBehavior.CloseConnection))
    {
            while (rdr.Read())
            {
                int a = rdr.GetInt32(0);
                var b = rdr.GetString(1);
                dict.Add(a, b);
            }
    }               
}
~~~

# Native SQL
您可以对关系数据库执行原生SQL查询，如下所示：
~~~
using (var ctx = new SchoolDBEntities())
{
    var studentName = ctx.Students.SqlQuery("Select studentid, studentname, standardId from Student where studentname='Bill'").FirstOrDefault<Student>();
}
~~~
在Raw SQL Query一章中学习使用DbContext执行原始SQL查询。https://www.entityframeworktutorial.net/EntityFramework4.3/raw-sql-query-in-entity-framework.aspx