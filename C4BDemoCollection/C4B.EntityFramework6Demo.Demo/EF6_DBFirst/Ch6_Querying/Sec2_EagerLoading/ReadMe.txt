﻿# Eager Loading in Entity Framework
即时加载是这样一个过程，即对一种类型的实体的查询也会将相关实体作为查询的一部分加载，这样我们就不需要对相关实体执行单独的查询。使用Include（）方法实现提前加载。
在下面的示例中，它使用Include（）方法从数据库沿着其标准获取所有学生。
LINQ Query Syntax:
~~~
using (var context = new SchoolDBEntities())
{
    var stud1 = (from s in context.Students.Include("Standard")
                where s.StudentName == "Bill"
                select s).FirstOrDefault<Student>();
}
~~~
LINQ Method Syntax:
~~~
using (var ctx = new SchoolDBEntities())
{
    var stud1 = ctx.Students
                   .Include("Standard")
                   .Where(s => s.StudentName == "Bill")
                   .FirstOrDefault<Student>();
}
~~~
上述LINQ查询将导致以下SQL查询：
~~~
SELECT TOP (1) 
[Extent1].[StudentID] AS [StudentID], 
[Extent1].[StudentName] AS [StudentName], 
[Extent2].[StandardId] AS [StandardId], 
[Extent2].[StandardName] AS [StandardName], 
[Extent2].[Description] AS [Description]
FROM  [dbo].[Student] AS [Extent1]
LEFT OUTER JOIN [dbo].[Standard] AS [Extent2] ON [Extent1].[StandardId] = [Extent2].[StandardId]
WHERE 'Bill' = [Extent1].[StudentName]
~~~

## Use Lambda Expression
您还可以使用LINQ lambda表达式作为Include方法中的参数。为此，引用System.Data.Entity命名空间并使用lambda表达式，如下所示：
~~~
using System;
using System.Data.Entity; 
   
class Program
{
    static void Main(string[] args)
    {
        using (var ctx = new SchoolDBEntities())
        {
            var stud1 = ctx.Students.Include(s => s.Standard)
                            .Where(s => s.StudentName == "Bill")
                            .FirstOrDefault<Student>();
        }
    }
}
~~~

## Load Multiple Entities
您还可以立即加载多个级别的相关实体。下面的示例查询会立即加载Student、Standard和Teacher实体：
~~~
using (var ctx = new SchoolDBEntities())
{
    var stud1 = ctx.Students.Include("Standard.Teachers")
                    .Where(s => s.StudentName == "Bill")
                    .FirstOrDefault<Student>();
}
~~~
或者使用如下的lambda表达式：
~~~
using (var ctx = new SchoolDBEntities())
{
    var stud1 = ctx.Students.Include(s => s.Standard.Teachers)
                    .Where(s => s.StudentName == "Bill")
                    .FirstOrDefault<Student>();
}
~~~
上述查询将在数据库中执行以下SQL查询：
~~~
SELECT [Project2].[StudentID] AS [StudentID], 
[Project2].[StudentName] AS [StudentName], 
[Project2].[StandardId] AS [StandardId], 
[Project2].[StandardName] AS [StandardName], 
[Project2].[Description] AS [Description], 
[Project2].[C1] AS [C1], 
[Project2].[TeacherId] AS [TeacherId], 
[Project2].[TeacherName] AS [TeacherName], 
[Project2].[StandardId1] AS [StandardId1]
FROM ( SELECT 
    [Limit1].[StudentID] AS [StudentID], 
    [Limit1].[StudentName] AS [StudentName], 
    [Limit1].[StandardId1] AS [StandardId], 
    [Limit1].[StandardName] AS [StandardName], 
    [Limit1].[Description] AS [Description], 
    [Project1].[TeacherId] AS [TeacherId], 
    [Project1].[TeacherName] AS [TeacherName], 
    [Project1].[StandardId] AS [StandardId1], 
    CASE WHEN ([Project1].[TeacherId] IS NULL) THEN CAST(NULL AS int) ELSE 1 END AS [C1]
    FROM   (SELECT TOP (1) [Extent1].[StudentID] AS [StudentID], [Extent1].[StudentName] AS [StudentName], [Extent1].[StandardId] AS [StandardId2], [Extent2].[StandardId] AS [StandardId1], [Extent2].[StandardName] AS [StandardName], [Extent2].[Description] AS [Description]
        FROM  [dbo].[Student] AS [Extent1]
        LEFT OUTER JOIN [dbo].[Standard] AS [Extent2] ON [Extent1].[StandardId] = [Extent2].[StandardId]
        WHERE 'updated student' = [Extent1].[StudentName] ) AS [Limit1]
    LEFT OUTER JOIN  (SELECT 
        [Extent3].[TeacherId] AS [TeacherId], 
        [Extent3].[TeacherName] AS [TeacherName], 
        [Extent3].[StandardId] AS [StandardId]
        FROM [dbo].[Teacher] AS [Extent3]
        WHERE [Extent3].[StandardId] IS NOT NULL ) AS [Project1] ON [Limit1].[StandardId2] = [Project1].[StandardId]
)  AS [Project2]
ORDER BY [Project2].[StudentID] ASC, [Project2].[StandardId] ASC, [Project2].[C1] ASC
~~~
EF Core支持额外的方法IncludeThen用于即时加载。在这里了解它。https://www.entityframeworktutorial.net/efcore/querying-in-ef-core.aspx
