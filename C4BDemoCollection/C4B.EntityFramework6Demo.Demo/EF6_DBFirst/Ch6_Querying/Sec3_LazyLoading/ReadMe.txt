﻿# Lazy Loading in Entity Framework
延迟加载是延迟相关数据的加载，直到您特别请求它。它与即时加载相反。例如，Student实体包含StudentAddress实体。在延迟加载中，上下文首先从数据库加载Student实体数据，
然后当我们访问StudentAddress属性时，它将加载StudentAddress实体，如下所示。
~~~
using (var ctx = new SchoolDBEntities())
{
    //Loading students only
    IList<Student> studList = ctx.Students.ToList<Student>();

    Student std = studList[0];

    //Loads Student address for particular Student only (seperate SQL query)
    StudentAddress add = std.StudentAddress;
}
~~~
上面显示的代码将导致两个SQL查询。首先，它会拿到所有学生：
~~~
SELECT 
[Extent1].[StudentID] AS [StudentID], 
[Extent1].[StudentName] AS [StudentName], 
[Extent1].[StandardId] AS [StandardId]
FROM [dbo].[Student] AS [Extent1]
~~~
然后，当我们获得StudentAddress的引用时，它将发送以下查询：
~~~
exec sp_executesql N'SELECT 
[Extent1].[StudentID] AS [StudentID], 
[Extent1].[Address1] AS [Address1], 
[Extent1].[Address2] AS [Address2], 
[Extent1].[City] AS [City], 
[Extent1].[State] AS [State]
FROM [dbo].[StudentAddress] AS [Extent1]
WHERE [Extent1].[StudentID] = @EntityKeyValue1',N'@EntityKeyValue1 int',@EntityKeyValue1=1
~~~

## Disable Lazy Loading
我们可以为特定的实体或上下文禁用延迟加载。若要关闭特定属性的延迟加载，请不要将其设为虚拟属性。要关闭上下文中所有实体的延迟加载，请将其配置属性设置为false。
~~~
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Linq;
    
public partial class SchoolDBEntities : DbContext
{
    public SchoolDBEntities(): base("name=SchoolDBEntities")
    {
        this.Configuration.LazyLoadingEnabled = false;
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
    }
}
~~~

延迟加载规则:
1. context.Configuration.ProxyCreationEnabled应为true。
2. context.Configuration.LazyLoadingEnabled应为true。
3. 导航属性应定义为public、virtual。如果属性没有被定义为virtual，上下文将不会延迟加载。