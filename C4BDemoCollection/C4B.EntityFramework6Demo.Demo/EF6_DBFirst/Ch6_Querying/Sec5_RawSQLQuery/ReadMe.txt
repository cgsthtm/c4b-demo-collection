﻿# Execute Raw SQL Queries in Entity Framework 6
Entity Framework允许您对底层关系数据库执行原始SQL查询。以下方法可用于使用Entity Framework 6.x对数据库执行原始SQL查询：
- DbSet.SqlQuery()
- DbContext.Database.SqlQuery()
- DbContext.Database.ExecuteSqlCommand()

## DbSet.SqlQuery()
使用DbSet.SqlQuery（）方法编写返回实体实例的原始SQL查询。结果实体将由上下文跟踪，就像它们是由LINQ查询返回的一样。
~~~
using (var ctx = new SchoolDBEntities())
{
    var studentList = ctx.Students
                        .SqlQuery("Select * from Students")
                        .ToList<Student>();
}
~~~
可以使用SqlParameter对象指定参数，如下所示。
~~~
using (var ctx = new SchoolDBEntities())
{
    var student = ctx.Students
                    .SqlQuery("Select * from Students where StudentId=@id", new SqlParameter("@id", 1))
                    .FirstOrDefault();
}
~~~
如果您更改SQL查询中的列名，那么它将抛出异常，因为它必须匹配列名。下面的例子将抛出一个异常。
~~~
using (var ctx = new SchoolDBEntities())
{                
    //this will throw an exception
    var studentName = ctx.Students.SqlQuery("Select studentid as id, studentname as name 
            from Student where studentname='Steve'").ToList();
}
~~~

## Database.SqlQuery()
Database类表示底层数据库，并提供各种方法来处理数据库。SqlQuery（）方法返回任意类型的值
~~~
using (var ctx = new SchoolDBEntities())
{
    //Get student name of string type
    string studentName = ctx.Database.SqlQuery<string>("Select studentname from Student where studentid=1")
                            .FirstOrDefault();

    //or
    string studentName = ctx.Database.SqlQuery<string>("Select studentname from Student where studentid=@id", new SqlParameter("@id", 1))
                            .FirstOrDefault();
}
~~~

## Database.ExecuteSqlCommand()
Database.ExecuteSqlCommnad（）方法在执行数据库命令（如Insert、Update和Delete命令）时很有用。
~~~
using (var ctx = new SchoolDBEntities())
{
    int noOfRowUpdated = ctx.Database.ExecuteSqlCommand("Update student 
            set studentname ='changed student by command' where studentid=1");

    int noOfRowInserted = ctx.Database.ExecuteSqlCommand("insert into student(studentname) 
            values('New Student')");

    int noOfRowDeleted = ctx.Database.ExecuteSqlCommand("delete from student 
            where studentid=1");
}
~~~