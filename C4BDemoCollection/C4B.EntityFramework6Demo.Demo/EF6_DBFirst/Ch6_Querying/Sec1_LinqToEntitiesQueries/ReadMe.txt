﻿# Linq-to-Entities Query
DbSet类派生自IQuerayable。因此，我们可以使用LINQ查询DbSet，它将被转换为SQL查询。EF API对底层数据库执行此SQL查询，获取平面结果集，将其转换为适当的实体对象并将其作为查询结果返回。
以下是一些可用于LINQ-to-Entities查询的标准查询运算符（或扩展方法）。
- First()
- FirstOrDefault()
- Single()
- SingleOrDefault()
- ToList()
- Count()
- Min()
- Max()
- Last()
- LastOrDefault()
- Average()

## Find()
除了LINQ扩展方法之外，我们还可以使用DbSet的Find（）方法根据主键值搜索实体。
~~~
var ctx = new SchoolDBEntities();
var student = ctx.Students.Find(1);
~~~
上面的查询将执行以下SQL查询。
~~~
SELECT 
[Extent1].[StudentID] AS [StudentID], 
[Extent1].[StudentName] AS [StudentName], 
[Extent1].[StandardId] AS [StandardId]
FROM [dbo].[Student] AS [Extent1]
WHERE [Extent1].[StudentId] = @p0',N'@p0 int',@p0=1
go
~~~

## First/FirstOrDefault
LINQ Query Syntax: LINQ查询语法：
~~~
using (var ctx = new SchoolDBEntities())
{    
    var student = (from s in ctx.Students
                where s.StudentName == "Bill"
                select s).FirstOrDefault<Student>();
}
~~~
LINQ Method Syntax: LINQ方法语法：
~~~
using (var ctx = new SchoolDBEntities())
{    
    var student = ctx.Students
                    .Where(s => s.StudentName == "Bill")
                    .FirstOrDefault<Student>();
}
~~~

## Parameterized Query
如果LINQ-to-Entities查询使用参数，则EF在数据库中构建并执行参数化查询，如下所示。
~~~using (var ctx = new SchoolDBEntities())
{    
    string name = "Bill";
    var student = ctx.Students
                  .Where(s => s.StudentName == name)
                  .FirstOrDefault<Student>();
}
~~~
First和FirstOrDefault之间的区别在于，First（）在没有结果数据的情况下抛出异常，而FirstOrDefault（）在没有结果数据的情况下返回默认值（null）。

## ToList
ToList方法返回收集结果。
~~~
using (var ctx = new SchoolDBEntities())
{    
    var studentList = ctx.Students.Where(s => s.StudentName == "Bill").ToList();
}
~~~
我们也可以使用ToArray、ToDictionary或ToList。

## GroupBy
使用group by运算符或GroupBy扩展方法根据实体的特定属性分组来获取结果。
LINQ Query Syntax:
~~~
using (var ctx = new SchoolDBEntities())
{    
    var students = from s in ctx.Students 
                    group s by s.StandardId into studentsByStandard
                    select studentsByStandard;

    foreach (var groupItem in students)
    {
        Console.WriteLine(groupItem.Key);

        foreach (var stud in groupItem)
        {
            Console.WriteLine(stud.StudentId);
        }

    }
}
~~~
LINQ Method Syntax:
~~~
using (var ctx = new SchoolDBEntities())
{    
    var students = ctx.Students.GroupBy(s => s.StandardId);

    foreach (var groupItem in students)
    {
        Console.WriteLine(groupItem.Key);

        foreach (var stud in groupItem)
        {
            Console.WriteLine(stud.StudentId);
        }

    }
}
~~~

## OrderBy
在LINQ查询语法中使用OrderBy运算符和升序/降序关键字来获取已排序的实体列表。
~~~
using (var ctx = new SchoolDBEntities())
{    
        var students = from s in ctx.Students
                       orderby s.StudentName ascending
                       select s;
}
~~~
使用OrderBy或OrderByDescending方法获取已排序的实体列表。
~~~
using (var ctx = new SchoolDBEntities())
{    
        var students = ctx.Students.OrderBy(s => s.StudentName).ToList();
        // or descending order  
        var  descStudents = ctx.Students.OrderByDescending(s => s.StudentName).ToList();
}
~~~

## Anonymous Object Result
LINQ-to-Entities查询并不总是必须返回实体对象。我们可以选择一个实体的一些属性作为结果。
LINQ Query Syntax:
~~~
using (var ctx = new SchoolDBEntities())
{    
    var anonymousObjResult = from s in ctx.Students
                             where s.StandardId == 1
                             select new { 
                                Id = st.StudentId, 
                                Name = st.StudentName
                             };

    foreach (var obj in anonymousObjResult)
    {
        Console.Write(obj.Name);
    }
}
~~~
LINQ Method Syntax:
~~~
using (var ctx = new SchoolDBEntities())
{    
    var anonymousObjResult = ctx.Students
                                .Where(st => st.Standard == 1)
                                .Select(st => new { 
                                            Id = st.StudentId, 
                                            Name = st.StudentName });

    foreach (var obj in anonymousObjResult)
    {
        Console.Write(obj.Name);
    }
}
~~~
上述查询中的projectionResult将是匿名类型，因为没有具有这些属性的类/实体。所以，编译器会将其标记为匿名。

## Nested queries
您还可以执行嵌套的LINQ到实体查询，如下所示：
~~~
using (var ctx = new SchoolDBEntities())
{
    var nestedQuery = from s in ctx.Students
                      from c in ctx.Courses
                      where s.StandardId == 1
                      select new { s.StudentId, c };
    var result = nestedQuery.ToList();
}
~~~
上面显示的嵌套查询将生成一个带有StudentName和Course对象的匿名列表。