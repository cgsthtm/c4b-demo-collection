﻿namespace C4B.EntityFramework6Demo.Demo.Migration2s
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SchoolDBMigration2v1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        StudentId = c.Int(nullable: false, identity: true),
                        StudentName = c.String(),
                    })
                .PrimaryKey(t => t.StudentId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Students");
        }
    }
}
