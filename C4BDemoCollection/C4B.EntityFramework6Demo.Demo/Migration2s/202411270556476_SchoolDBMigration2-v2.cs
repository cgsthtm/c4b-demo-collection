﻿namespace C4B.EntityFramework6Demo.Demo.Migration2s
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SchoolDBMigration2v2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        CourseId = c.Int(nullable: false, identity: true),
                        CourseName = c.String(),
                    })
                .PrimaryKey(t => t.CourseId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Courses");
        }
    }
}
