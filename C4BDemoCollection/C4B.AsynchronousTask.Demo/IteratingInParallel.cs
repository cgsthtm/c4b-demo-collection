﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace C4B.AsynchronousTask.Demo
{
    public class IteratingInParallel
    {
        const int TotalDigits = 100;
        const int BatchSize = 10;

        /// <summary>
        /// Listing 21.1: A for Loop Synchronously Calculating Pi in Sections
        /// </summary>
        public static void _21_1AForLoopSynchronouslyCalculatingPiInSections()
        {
            string pi = "";
            const int iterations = TotalDigits / BatchSize;
            for (int i = 0; i < iterations; i++)
            {
                pi += PiCalculator.Calculate(BatchSize, i * BatchSize);
            }
            Console.WriteLine(pi);
            /* for 循环以同步和顺序执行每次迭代。但是，由于 pi 计算算法将 pi 计算拆分为独立的部分，因此如果结果以正确的顺序附加，则无需按顺序计算部分。
             *   想象一下，如果可以同时运行此循环的所有迭代，会发生什么情况：每个处理器可以进行一次迭代，并与执行其他迭代的其他处理器并行执行。
             *   考虑到迭代的同时执行，我们可以根据处理器的数量越来越多地减少执行时间。
             * 任务并行库（TPL）提供了一种方便的方法Parallel.For（），它正是这样做的。示例 21.2 显示了如何修改示例 21.1 中的顺序单线程程序以使用 helper 方法。
             */
        }

        /// <summary>
        /// Listing 21.2: For Loop Calculating Pi in Sections in Parallel
        /// </summary>
        public static void _21_2ForLoopCalculatingPiInSectionsInParallel()
        {
            string pi = "";
            const int iterations = TotalDigits / BatchSize;
            string[] sections = new string[iterations];
            Parallel.For(0, iterations, i =>
            {
                sections[i] = PiCalculator.Calculate(
                    BatchSize, i * BatchSize);
            });

            pi = string.Join("", sections);
            Console.WriteLine(pi);
            /* 清单 21.2 的输出与输出 21.2 相同;但是，如果您有多个 CPU，则执行时间会明显更快（如果没有，则可能会更慢）。Parallel.For（） API 的设计看起来类似于标准的
             *   for 循环。第一个参数是 fromInclusive 值，第二个参数是 toExclusive 值，最后一个参数是要作为循环体执行的操作。将表达式 lambda 用于操作时，
             *   代码看起来类似于 for 循环语句，只是现在每个迭代都可以并行执行。与 for 循环一样，在所有迭代完成之前，对 Parallel.For（） 的调用不会完成。
             *   换句话说，当执行到达字符串时。Join（） 语句，pi 的所有部分都将被计算出来。
             * 当执行的计算可以很容易地拆分为许多相互独立的处理器绑定计算时，请使用并行循环，这些计算可以在任何线程上以任何顺序执行。
             * TPL 还提供了 foreach 语句的类似并行版本Parallel.ForEach。
             */
        }

        /// <summary>
        /// Listing 21.5: Canceling a Parallel Loop
        /// </summary>
        public static void _21_5CancelingAParallelLoop(string directoryPath, string searchPattern)
        {
            /* 与任务不同，如果任务要在完成之前阻塞，则需要显式调用，并行循环并行执行迭代，但在整个并行循环完成之前不会返回。
             *   因此，取消并行循环通常涉及从执行并行循环的线程以外的线程调用取消请求。在示例 21.5 中，我们使用 Task.Run（） 调用 Parallel.ForEach（）。
             *   通过这种方式，查询不仅可以并行执行，还可以异步执行，从而允许代码提示用户“按任意键退出”。
             */
            string stars =
            "*".PadRight(Console.WindowWidth - 1, '*');

            IEnumerable<string> files = Directory.GetFiles(
               directoryPath, searchPattern,
               SearchOption.AllDirectories);

            CancellationTokenSource cts =
                new CancellationTokenSource();
            ParallelOptions parallelOptions =
                new ParallelOptions { CancellationToken = cts.Token };
            cts.Token.Register(
                () => Console.WriteLine("Canceling..."));

            Console.WriteLine("Press ENTER to exit.");

            Task task = Task.Run(() =>
            {
                Console.WriteLine($"Task.Run() ({Thread.CurrentThread.ManagedThreadId})");
                try
                {
                    Parallel.ForEach(
                        files, parallelOptions,
                        (fileName, loopState) =>
                        {
                            Console.WriteLine($"Parallel.ForEach() ({Thread.CurrentThread.ManagedThreadId})");
                            Thread.Sleep(5000);
                            Console.WriteLine($"{fileName}...");
                        });
                }
                catch (OperationCanceledException) { }
            });
            Console.WriteLine($"等待用户输入 ({Thread.CurrentThread.ManagedThreadId})");
            // Wait for the user's input
            Console.ReadLine();

            // Cancel the query
            cts.Cancel();
            Console.Write(stars);
            task.Wait();
            /* 并行循环使用与任务相同的取消令牌模式。从 CancelTokenSource 获取的令牌通过调用具有 ParallelOptions 类型的参数的 ForEach（） 方法的重载与并行循环相关联。
             *   此对象包含取消令牌。
             */
        }

        /// <summary>
        /// Listing 21.7: Parallel LINQ Select()
        /// </summary>
        public static List<string> _21_7ParallelLINQSelect()
        {
            IEnumerable<string> data = new List<string>()
            {
                "111","222","333","444","555","666"
            };
            return data.AsParallel().Select(item => item + "_hello").ToList();
            /* 如清单 21.7 所示，启用并行支持的更改非常小！它所涉及的只是一个标准的查询运算符AsParallel（），它可以在静态类System.Linq.ParallelEnumerable上找到。
             *   这个简单的扩展方法告诉运行时它可以并行执行查询。结果是，在具有多个可用 CPU 的计算机上，执行查询所花费的总时间可以大大缩短。
             * System.Linq.ParallelEnumerable 是 Microsoft .NET Framework 4.0 中引入的用于启用 PLINQ 的引擎，它包括 System.Linq.Enumerable 上可用的查询运算符的超集。
             *   因此，它提供了一个 API，可以提高所有常见查询运算符的性能，包括用于排序、筛选 （Where（））、投影 （Select（））、联接、分组和聚合的运算符。
             *   示例 21.8 显示了如何进行并行排序。
             */
        }
    }
}
