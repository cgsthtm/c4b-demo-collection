﻿using C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1;
using C4B.AsynchronousTask.Demo.Test01;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace C4B.AsynchronousTask.Demo
{
    internal class Program
    {
        /// <summary>
        /// 在C# 7.1之前，Main()不支持使用async。
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static async Task Main(string[] args)
        {
            // Listing 19.1: Invoking an Asynchronous Task
            //AsynchronousTasks._19_1InvokingAnAsynchronousTask();

            // Listing 19.2: Polling a Task<T>
            //AsynchronousTasks._19_2PollingATask();

            // Listing 19.3: Calling Task.ContinueWith()
            //AsynchronousTasks._19_3Calling_Task_ContinueWith();

            // Listing 19.4: Registering for Notifications of Task Behavior with ContinueWith()
            //AsynchronousTasks._19_4_RegisteringForNotificationsOfTaskBehaviorWithContinueWith();

            // Listing 19.5: Handling a Task’s Unhandled Exception
            //AsynchronousTasks._19_5HandlingATasksUnhandledException();

            // Listing 19.6: Observing Unhandled Exceptions on a Task Using ContinueWith()
            //AsynchronousTasks._19_6ObservingUnhandledExceptionsOnATaskUsingContinueWith();

            // Listing 19.7: Registering for Unhandled Exceptions
            //AsynchronousTasks._19_7RegisteringForUnhandledExceptions();


            // Listing 19.8: Canceling a Task Using CancellationToken
            //CancelingATask._19_8CancelingATaskUsingCancellationToken();

            // Listing 19.9 provides an example of using Task.Factory.StartNew().
            //Console.WriteLine(CancelingATask.CalculatePiAsync(10).Result);

            // Listing 19.10: Cooperatively Executing Long-Running Tasks
            //CancelingATask._19_10CooperativelyExecutingLongRunningTasks();


            // Listing 20.1: A Synchronous Web Request
            //ProgrammingTaskBasedAsynchronousPattern._20_1ASynchronousWebRequest(args);

            // Listing 20.2: An Asynchronous Web Request
            //ProgrammingTaskBasedAsynchronousPattern._20_2AnAsynchronousWebRequest2(args);

            // Listing 20.3: Asynchronous High-Latency Invocation with the Task-Based Asynchronous Pattern
            //ProgrammingTaskBasedAsynchronousPattern._20_3AsynchronousHighLatencyInvocationWithTheTaskBasedAsynchronousPattern(args);

            // Listing 20.10: An Asynchronous Client-Server Interaction as a Lambda Expression
            //ProgrammingTaskBasedAsynchronousPattern._20_10AnAsynchronousClientServerInteractionAsALambdaExpression();

            // Listing 20.13: Calling Task.ContinueWith()
            //ProgrammingTaskBasedAsynchronousPattern._20_13CallingTask_ContinueWith();


            // Listing 21.1: A for Loop Synchronously Calculating Pi in Sections
            //IteratingInParallel._21_1AForLoopSynchronouslyCalculatingPiInSections();

            // Listing 21.2: For Loop Calculating Pi in Sections in Parallel
            //IteratingInParallel._21_2ForLoopCalculatingPiInSectionsInParallel();

            // Listing 21.5: Canceling a Parallel Loop
            //IteratingInParallel._21_5CancelingAParallelLoop(Directory.GetCurrentDirectory(),"*");

            // Listing 21.7: Parallel LINQ Select()
            //IteratingInParallel._21_7ParallelLINQSelect();


            // Listing 22.1: Unsynchronized State
            //ThreadSynchronization._22_1_UnsynchronizedState(args);

            // Listing 22.2: Unsynchronized Local Variables
            //ThreadSynchronization._22_2UnsynchronizedLocalVariables(args);

            // Listing 22.3: Synchronizing with a Monitor Explicitly
            //ThreadSynchronization._22_3SynchronizingWithAMonitorExplicitly(args);

            // Listing 22.4: Synchronization Using the lock Keyword
            //ThreadSynchronization._22_4SynchronizationUsingTheLockKeyword(args);

            // Listing 22.5: async Main() with C# 7.1
            //ThreadSynchronization._22_5AsyncMainWithC_7_1(args);

            // Listing 22.6: Synchronization Using System.Threading.Interlocked
            //ThreadSynchronization._22_6SynchronizationUsingSystem_Threading_Interlocked(args);

            // Listing 22.9: Creating a Single Instance Application
            //ThreadSynchronization._22_9CreatingASingleInstanceApplication();

            // Listing 22.10: Waiting for ManualResetEventSlim
            //ThreadSynchronization._22_10WaitingForManualResetEventSlim();

            // Listing 22.11: Using ThreadLocal<T> for Thread Local Storage
            ThreadSynchronization._22_11UsingThreadLocal_T_ForThreadLocalStorage();

            // Listing 22.12: Using ThreadStaticAttribute for Thread Local Storage
            //ThreadSynchronization._22_12UsingThreadStaticAttributeForThreadLocalStorage();

            // C#并发编程实例
            //Form1 form1 = new Form1();
            //form1.ShowDialog();
            //_2异步编程基础 _2 = new _2异步编程基础();
            //_2.ShowDialog();
            //_3并行开发的基础 _3 = new _3并行开发的基础();
            //_3.ShowDialog();
            //_4数据流基础 _4 = new _4数据流基础();
            //_4.ShowDialog();
            //_5Rx基础 _5 = new _5Rx基础();
            //_5.ShowDialog();
            //_6测试技巧 _6 = new _6测试技巧();
            //_6.ShowDialog();
            //_8集合 _8 = new _8集合();
            //_8.ShowDialog();
            //_9取消 _9 = new _9取消();
            //_9.ShowDialog();
            //_10函数式OOP _10 = new _10函数式OOP();
            //_10.ShowDialog();
            //_11同步 _11 = new _11同步();
            //_11.ShowDialog();
            //_12调度 _12 = new _12调度();
            //_12.ShowDialog();
            //_13实用技巧 _13 = new _13实用技巧();
            //_13.ShowDialog();

            //_01十三所ECU刷写多线程 _01 = new _01十三所ECU刷写多线程();
            //_01.ShowDialog();

            // 代码清单20.16　在循环中使用await操作符
            //EssentialCSharp80._01 _01 = new EssentialCSharp80._01();
            //_01.ShowDialog();

            //Interlocked.CompareExchange()
            //Mutex mutex = new Mutex()

            await Task.Delay(100);
            Console.ReadLine();
        }
    }
}
