﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C4B.AsynchronousTask.Demo.EssentialCSharp80
{
    public partial class _01 : Form
    {
        public _01()
        {
            InitializeComponent();
        }

        public List<string> urls =
            new List<string>()
            {
                "www.bing.com",
                "www.baidu.com",
                "www.google.ws",
                "www.bilibili.com.cn"
            };

        /// <summary>
        /// 将一个异步Lambda赋值给func变量，要使用await操作符来调用这个变量
        /// </summary>
        public Func<string, Task<IPStatus>> func =
            async (localUrl) =>
            {
                Ping ping = new Ping();
                PingReply pingReply =
                    await ping.SendPingAsync(localUrl);
                return pingReply.Status;
            };

        async private void button1_Click(object sender, EventArgs e)
        {
            IPStatus status;
            foreach (var url in urls)
            {
                // func是个Func<string, Task<IPStatus>>类型的变量，这是个委托，执行这个委托则返回Task<IPStatus>，
                // 而对该委托返回的Task<IPStatus>结果使用await，则得到IPStatus
                status = await func(url); // 使用await操作符调用异步lambda
                Console.WriteLine($"{url} {status.ToString()} ({Thread.CurrentThread.ManagedThreadId})");
            }
            Console.WriteLine("循环结束");
        }
    }
}
