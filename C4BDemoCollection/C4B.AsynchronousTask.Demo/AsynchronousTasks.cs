﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace C4B.AsynchronousTask.Demo
{
    public class AsynchronousTasks
    {
        /// <summary>
        /// Listing 19.1: Invoking an Asynchronous Task
        /// </summary>
        public static void _19_1InvokingAnAsynchronousTask()
        {
            /* 在此方案中，我们只有一个任务，但也有许多任务异步运行。
             * 通常有一组任务，您希望在继续执行当前线程之前等待所有任务完成或其中任何一个任务完成。
             * Task.WaitAll（） 和 Task.WaitAny（） 方法分别就是这样做的。
             */
            const int repetitions = 10000;
            // Use Task.Factory.StartNew<string>() for
            // TPL prior to .NET 4.5
            Task task = Task.Run(() =>
            {
                for (int count = 0; count < repetitions; count++)
                {
                    Console.Write('-');
                }
            });
            for (int count = 0; count < repetitions; count++)
            {
                Console.Write('+');
            }
            // Wait until the Task completes
            task.Wait();
            Console.WriteLine("_19_1InvokingAnAsynchronousTask Completed.");
            Console.ReadLine();
        }

        /// <summary>
        /// Listing 19.2: Polling a Task<T>
        /// </summary>
        public static void _19_2PollingATask()
        {
            /* 异步执行任务时，我们可以从一个线程轮询它以查看它是否完成，并在完成时获取结果.
             * 示例 19.2 演示了如何在控制台应用程序中执行此操作。
             */
            // Use Task.Factory.StartNew<string>() for
            // TPL prior to .NET 4.5
            Task<string> task = Task.Run<string>(() => PiCalculator.Calculate(100));

            foreach (char busySymbol in Utility.BusySymbols())
            {
                if (task.IsCompleted)
                {
                    Console.Write('\b');
                    break;
                }
                Console.Write(busySymbol);
            }

            Console.WriteLine();

            Console.WriteLine(task.Result);
            if (!task.IsCompleted)
            {
                throw new Exception("Task Should Be Completed");
            }
            /* 请注意，19.2 没有调用 Wait（）。相反，从 Result 属性读取会自动导致当前线程阻塞，直到结果可用（如果尚未可用）;在这种情况下，我们知道在获取结果时它已经完成。
             * 除了 Task<T> 上的 IsCompleted 和 Result 属性之外，其他几个属性还值得注意：
             *   ·当任务完成时，IsCompleted 属性设置为 true，无论它正常完成还是出错（即，由于引发异常而结束）。可以通过读取 Status 属性获取有关任务状态的更多详细信息，
             *    该属性返回 TaskStatus 类型的值。可能的值包括“已创建Created”、“等待激活WaitingForActivation”、“等待运行WaitingToRun”、“正在运行Running”、
             *    “等待子项完成WaitingForChildrenToComplete”、“运行到完成RanToCompletion”、“已取消Canceled”和“出错Faulted”。
             *    只要状态为“运行到完成RanToCompletion”、“已取消Canceled”或“出错Faulted”，则为“已完成IsCompleted”。
             *    当然，如果任务在另一个线程上运行，并且您将状态读取为正在运行，则状态可能随时更改为已完成，包括在读取属性值后立即更改。
             *    许多其他状态也是如此 - 如果不同的线程启动它，即使是 Created 也可能会更改。只有 RanToComplete、Canceled 和 Faulted 可以被视为无法再转换的最终状态。
             *   ·任务可以由Id属性的值唯一标识。静态Task.CurrentId属性提供当前正在执行的任务（即正在执行 Task.CurrentId 调用的任务）的标识符。这些属性在调试时特别有用。
             *   ·可以使用 AsyncState 将其他数据与任务相关联。例如，假设一个List<T>，其值将由各种任务计算。每个任务都可以包含 AsyncState 属性中的值索引。
             *    这样，当任务完成时，代码可以使用 AsyncState 索引到列表中（首先将其强制转换为 int）
             */
        }

        /// <summary>
        /// Listing 19.3: Calling Task.ContinueWith()
        /// </summary>
        public static void _19_3Calling_Task_ContinueWith()
        {
            /* 异步任务还允许通过描述异步延续将较大的任务从较小的任务中组合出来。与常规控制流一样，任务可以具有不同的延续来处理错误情况，
             *   并且可以通过操作任务的延续来合并在一起。有几种技术可以做到这一点，其中最明确的是 ContinueWith（） 方法（参见19.3 及其相应的输出，输出 19.1）。
             */
            Console.WriteLine("Before");
            // Use Task.Factory.StartNew<string>() for
            // TPL prior to .NET 4.5
            Task taskA =
                Task.Run(() =>
                     Console.WriteLine("Starting..."))
                .ContinueWith(antecedent =>
                     Console.WriteLine("Continuing A..."));
            Task taskB = taskA.ContinueWith(antecedent =>
                Console.WriteLine("Continuing B..."));
            Task taskC = taskA.ContinueWith(antecedent =>
                Console.WriteLine("Continuing C..."));
            Task.WaitAll(taskB, taskC);
            Console.WriteLine("Finished!");
            /* Output 19.1：
             * Before
               Starting...
               Continuing A...
               Continuing C...
               Continuing B...
               Finished!
             */
            /* ContinueWith（）方法支持将两个任务“链接”在一起，这样当前置任务（前置任务）完成时，第二个任务（延续任务）将自动异步启动。
             *   例如，在清单 19.3 中，Console.WriteLine（“Starting...”） 是先行任务正文，Console.WriteLine（“Continuing A...”） 是其延续任务正文。
             *   延续任务将 Task 作为其参数（前置任务），从而允许延续任务的代码访问前置任务的完成状态。当前一个任务完成后，继续任务将自动启动，异步执行第二个委托，
             *   并将刚刚完成的先行任务作为参数传递给该委托。此外，由于 ContinueWith（） 方法也返回一个任务，因此该任务可以用作另一个任务的前置项，依此类推，
             *   形成可以任意长的任务的延续链。
             * 如果对同一个先行任务调用 ContinueWith（）两次（如清单 19.3 所示，任务B和任务C表示任务A的延续任务），则前置任务 （taskA） 有两个延续任务，
             *   当前置任务完成时，两个延续任务都将异步执行。请注意，在编译时，单个先行项的延续任务的执行顺序是不确定的。输出 19.1 恰好显示任务 C 在任务 B 之前执行，
             *   但在程序的第二次执行中，顺序可能会颠倒。但是，任务A将始终在任务B和任务C之前执行，因为后者是任务A的延续任务，因此无法在任务A完成之前启动。
             *   类似地，Console.WriteLine（“Starting...”）委托将始终在任务 A （Console.WriteLine（“Continuing A...”））之前执行完成，因为后者是前者的延续任务。
             *   此外，Finished！将始终显示在最后，因为对 Task.WaitAll（taskB、taskC）的调用会阻止控制流继续，直到任务 B 和任务 C 都完成。
             * 许多不同的 ContinueWith（） 重载是可能的，其中一些采用 TaskContinuationOptions 值来调整延续链的行为。
             *   这些值是标志，因此可以使用逻辑 OR 运算符 （|） 组合它们。表 19.1 中列出了一些可能的标志值的简要说明;有关更多详细信息，请参阅联机文档
             *   Table 19.1: List of Available TaskContinuationOptions Enums
             *   ·None 这是默认行为。延续任务将在前一个任务完成时执行，无论其任务状态如何。
             *   ·PreferFairness 如果两个任务都是异步启动的，一个先于另一个，则无法保证首先启动的任务实际上首先运行。
             *    此标志要求任务计划程序尝试增加启动的第一个任务是要执行的第一个任务的可能性 — 当您描述的两个任务是从不同的线程池线程创建时，这一点尤其重要。
             *   ·LongRunning 这会告知任务计划程序该任务可能是 I/O 密集型高延迟任务。然后，计划程序可以允许处理其他排队的工作，而不是由于长时间运行的任务而匮乏。
             *     应谨慎使用此选项。
             *   ·AttachedToParent 这指定任务应尝试附加到任务层次结构中的父任务。
             *   ·DenyChildAttach (.NET 4.5) 如果尝试创建子任务，则会引发异常。如果延续中的代码尝试使用 AttachedToParent，它将表现得好像没有父级一样。
             *   ·NotOnRanToCompletion* 这指定如果延续任务的前置任务运行完成，则不应计划该任务。此选项对多任务延续无效。
             *   ·NotOnFaulted* 这指定如果继续任务的前置引发未处理的异常，则不应计划该任务。此选项对多任务延续无效。
             *   ·OnlyOnCanceled* 这指定仅当取消了延续任务的前置任务时，才应计划该任务。此选项对多任务延续无效。
             *   ·NotOnCanceled* 这指定如果取消了延续任务的前置任务，则不应计划该任务。此选项对多任务延续无效。
             *   ·OnlyOnFaulted* 这指定仅当延续任务的前置引发未处理的异常时，才应计划该任务。此选项对多任务延续无效。
             *   ·OnlyOnRanToCompletion* 这指定仅当延续任务的前置任务运行完成时，才应计划该任务。此选项对多任务延续无效。
             *   ·ExecuteSynchronously 这指定应同步执行继续任务。指定此选项后，计划将尝试执行工作的延续与导致先前任务转换到其最终状态的线程相同。
             *    如果在创建延续时先前的已经完成，则延续将在创建延续的线程上运行。
             *   ·HideScheduler (.NET 4.5) 当延续应在特定调度程序上运行，但延续调用不应在同一调度程序上调度工作的其他代码时，这很有用。
             *   ·LazyCancellation (.NET 4.5) 这会导致继续延迟监视提供的取消令牌以进行取消请求，直到前置完成。考虑任务 t1、t2 和 t3，其中后者是前者的延续。
             *    如果在 t1 完成之前取消 t2，则 t3 可能会在 t1 完成之前启动。设置延迟取消可避免这种情况。
             *   ·RunContinuationsAsynchronously (.NET 4.6) 当使用“RunContinuationsAsyncly”选项创建任务时，这会告知任务应强制其延续异步运行。
             *    即使任务本身是延续，此选项也不会影响该任务的运行方式，而只会影响该任务的延续的运行方式。
             *    可以使用 TaskContinuationOptions.ExecuteSync 和 TaskContinuationOptions.RunContinuationsAsyncly 创建延续任务。
             *    前者会导致延续在其前置完成时同步执行，并导致延续的延续在延续完成时异步运行。
             * 在表 19.1 中，用星号 （*） 表示的项表示在什么条件下执行延续任务;因此，它们对于创建延续特别有用，这些延续充当先前任务行为的事件处理程序。
             */
        }

        /// <summary>
        /// Listing 19.4: Registering for Notifications of Task Behavior with ContinueWith()
        /// </summary>
        public static void _19_4_RegisteringForNotificationsOfTaskBehaviorWithContinueWith()
        {
            /* 示例 19.4 演示了如何为一个前置任务提供多个有条件执行的延续，具体取决于前置任务的完成方式。
             */
            // Use Task.Factory.StartNew<string>() for
            // TPL prior to .NET 4.5
            Task<string> task =
                Task.Run<string>(
                    () => PiCalculator.Calculate(10));
            Task faultedTask = task.ContinueWith(
                (antecedentTask) =>
                {
                    if (!antecedentTask.IsFaulted)
                    {
                        throw new Exception("Antecedent Task Should Be Faulted");
                    }
                    Console.WriteLine(
                        "Task State: Faulted");
                },
                TaskContinuationOptions.OnlyOnFaulted);

            Task canceledTask = task.ContinueWith(
                (antecedentTask) =>
                {
                    if (!antecedentTask.IsCanceled)
                    {
                        throw new Exception("Antecedent Task Should Be Canceled");
                    }
                    Console.WriteLine(
                        "Task State: Canceled");
                },
                TaskContinuationOptions.OnlyOnCanceled);

            Task completedTask = task.ContinueWith(
                (antecedentTask) =>
                {
                    if (!antecedentTask.IsCompleted)
                    {
                        throw new Exception("Antecedent Task Should Be Completed");
                    }
                    Console.WriteLine(
                        "Task State: Completed");
                }, TaskContinuationOptions.
                        OnlyOnRanToCompletion);

            completedTask.Wait();
            /* 在此列表中，我们有效地为前置任务的事件注册侦听器，以便在任务正常或异常完成时，特定的“侦听”任务将开始执行。这是一项强大的功能，
             *   特别是如果原始任务是即发即弃的任务，即我们启动的任务，连接到延续任务，然后再也不会引用。
             * 在清单 19.4 中，请注意，最后的 Wait（） 调用是在 completeTask 上，而不是在task上，即使用 Task.Run（） 创建的原始先行任务。
             *   尽管每个委托的 antecedentTask 是对前置任务（任务）的引用，但从委托侦听器外部，我们可以有效地丢弃对原始任务的引用。
             *   然后，我们可以完全依赖异步开始执行的延续任务，而无需检查原始任务状态的后续代码。
             * 在这种情况下，我们调用 completeTask.Wait（），以便主线程在完成的输出出现之前不会退出程序（参见输出 19.2）。
             * 在这种情况下，调用 completeTask.Wait（） 有点做作，因为我们知道原始任务将成功完成。但是，在已取消的任务或错误的任务上调用 Wait（） 将导致异常。
             *   仅当先前任务被取消或引发异常时，这些继续任务才会运行;鉴于此程序中不会发生这种情况，这些任务永远不会被安排运行，并且等待它们完成将引发异常。
             *   清单 19.1 中的延续选项恰好是互斥的，因此当先行任务运行到完成并且与completeTask关联的任务执行时，
             *   任务调度程序会自动取消与canceledTask和faultedTask关联的任务。已取消的任务结束时的状态设置为“已取消”。
             *   因此，对其中任何一个任务调用 Wait（）（或任何其他会导致当前线程等待任务完成的调用）将引发异常，指示它们已被取消。
             *   一种不太做作的方法可能是调用 Task.WaitAny（completeTask， canceledTask， faultedTask），这将抛出一个 AggregateException，然后需要处理该异常。
             * 对具有聚合异常AggregateException的任务进行未处理的异常处理：
             *   同步调用方法时，我们可以将其包装在带有 catch 子句的 try 块中，以向编译器标识发生异常时我们要执行的代码。但是，这不适用于异步调用。
             *   我们不能简单地将 try 块包装在对 Start（） 的调用周围以捕获异常，因为控制会立即从调用中返回，然后控制将离开 try 块，可能在工作线程上发生异常之前很久。
             *   一种解决方案是使用 try/catch 块包装任务委托的正文。因此，在工作线程上抛出并随后捕获的异常不会出现问题，因为 try 块将在工作线程上正常工作。
             *   但是，对于未经处理的异常（工作线程未捕获的异常），情况并非如此。
             *   通常（从 CLR 版本 2.0 开始），任何线程上未经处理的异常都被视为致命异常，触发操作系统错误报告对话框，并导致应用程序异常终止。
             *   必须捕获所有线程上的所有异常;否则，不允许应用程序继续运行。（有关处理未经处理的异常的一些高级技术，请参阅即将发布的高级主题：
             *   处理线程上未经处理的异常。）幸运的是，异步运行的任务中未经处理的异常并非如此。在这种情况下，任务计划程序在委托周围插入一个 catchall 异常处理程序，
             *   以便在任务引发未处理的异常时，catchall 处理程序将捕获它并在任务中记录异常的详细信息，从而避免 CLR 的任何触发器自动终止进程。
             * 正如我们在清单 19.4 中看到的，处理错误任务的一种技术是显式创建一个延续任务，该任务是该任务的错误处理程序;
             *   任务计划程序将在检测到先前任务引发未经处理的异常时自动计划继续。但是，如果不存在这样的处理程序，并且 Wait（）（或尝试获取结果）对出错的任务执行，
             *   则会抛出 AggregateException（参见清单 19.5 和输出 19.3）。
             */
        }

        /// <summary>
        /// Listing 19.5: Handling a Task’s Unhandled Exception
        /// </summary>
        public static void _19_5HandlingATasksUnhandledException()
        {
            /* 示例 19.5： 处理任务的未处理异常
             */
            // Use Task.Factory.StartNew<string>() for
            // TPL prior to .NET 4.5
            Task task = Task.Run(() =>
            {
                throw new InvalidOperationException();
            });

            try
            {
                task.Wait();
            }
            catch (AggregateException exception)
            {
                exception.Handle(eachException =>
                {
                    Console.WriteLine(
                        $"ERROR: {eachException.Message}");
                    return true;
                });
            }
            /* 之所以称为聚合异常，是因为它可能包含从一个或多个错误任务收集的许多异常。例如，假设异步并行执行十个任务，其中五个任务引发异常。
             *   为了报告所有五个异常并在单个 catch 块中处理它们，框架使用 AggregateException 作为收集异常并将其报告为单个异常的方法。
             *   此外，由于在编译时不知道工作任务是否会引发一个或多个异常，因此未处理的错误任务将始终引发 AggregateException。
             *   即使工作线程上引发的未经处理的异常类型为 InvalidOperationException，在主线程上捕获的异常类型仍然是 AggregateException。
             *   此外，正如预期的那样，要捕获异常需要一个 AggregateException 捕获块。
             * AggregateException 中包含的异常列表可从 InnerExceptions 属性获得。因此，您可以循环访问此属性以检查每个异常并确定适当的操作过程。
             *   或者，如清单 19.5 所示，您可以使用 AggregateException.Handle（） 方法，指定要针对 AggregateException 中包含的每个单独异常执行的表达式。
             *   但是，要考虑的 Handle（） 方法的一个重要特征是它是一个谓词。因此，对于 Handle（） 委托成功解决的任何异常，谓词应返回 true。
             *   如果任何异常处理调用为异常返回 false，则 Handle（） 方法将引发一个新的 AggregateException，其中包含此类相应异常的复合列表。
             */
        }

        /// <summary>
        /// Listing 19.6: Observing Unhandled Exceptions on a Task Using ContinueWith()
        /// </summary>
        public static void _19_6ObservingUnhandledExceptionsOnATaskUsingContinueWith()
        {
            /* 您还可以通过简单地查看任务的 Exception 属性来观察出错任务的状态，而不会导致在当前线程上重新引发异常。
             * 示例 19.6 通过等待我们知道会抛出异常的任务的错误延续完成来演示这种方法。
             */
            bool parentTaskFaulted = false;
            Task task = new Task(() =>
            {
                throw new InvalidOperationException();
            });
            Task continuationTask = task.ContinueWith(
                (antecedentTask) =>
                {
                    parentTaskFaulted =
                        antecedentTask.IsFaulted;
                }, TaskContinuationOptions.OnlyOnFaulted);
            task.Start();
            continuationTask.Wait();
            if (!parentTaskFaulted)
            {
                throw new Exception("Parent task should be faulted");
            }
            if (!task.IsFaulted)
            {
                throw new Exception("Task should be faulted");
            }

            task.Exception?.Handle(eachException =>
            {
                Console.WriteLine(
                    $"ERROR: {eachException.Message}");
                return true;
            });
            /* 请注意，为了检索原始任务上未处理的异常，我们使用 Exception 属性（以及使用 null-forgiveness 运算符取消引用，因为我们知道该值不会为 null）。
             * 如果任务中发生的异常完全未被观察到，也就是说:
             *   （1） 它不是从任务中捕获的;
             *   （2） 例如，通过 Wait（）、Result 或访问 Exception 属性，从不观察到任务的完成;
             *   （3） 从未观察到错误的 ContinueWith（） — 则异常可能完全未处理，从而导致进程范围的未处理异常。
             * 在 .NET 4.0 中，此类出错的任务将被终结器线程重新引发，并可能导致进程崩溃。相比之下，在 .NET 4.5 中，崩溃已被抑制（尽管如果愿意，可以为崩溃行为配置 CLR）。
             * 无论哪种情况，您都可以通过 TaskScheduler.UnobservedTaskException 事件注册未处理的任务异常。
             */
        }

        private static Stopwatch Clock { get; } = new Stopwatch();
        /// <summary>
        /// Listing 19.7: Registering for Unhandled Exceptions
        /// </summary>
        public static void _19_7RegisteringForUnhandledExceptions()
        {
            /* 处理线程上未经处理的异常
             * 与其在发生未经处理的异常时尽快关闭应用程序，不如保存任何工作数据和/或记录异常以供错误报告和将来调试。这需要一种机制来注册未处理异常的通知。
             * 对于 Microsoft .NET Framework 和 .NET Core 2.0（或更高版本），每个 AppDomain 都提供这样的机制，并且要观察 AppDomain 中发生的未经处理的异常，
             *   您必须向 UnhandledException 事件添加一个处理程序。UnhandledException 事件将为应用程序域中线程上的所有未经处理的异常触发，
             *   无论它是主线程还是工作线程。请注意，此机制的目的是通知;它不允许应用程序从未处理的异常中恢复并继续执行。
             *   事件处理程序运行后，应用程序将显示操作系统的错误报告对话框，然后应用程序将退出。（对于控制台应用程序，异常详细信息也将显示在控制台上。)
             * 在示例 19.7 中，我们展示了如何创建第二个线程，该线程抛出异常，然后由应用程序域的未处理异常事件处理程序处理。
             *   出于演示目的，为了确保线程计时问题不会发挥作用，我们使用 Thread.Sleep 插入了一些人为的延迟。
             * 示例 19.7. 注册未处理的异常
             */
            try
            {
                Clock.Start();
                // Register a callback to receive notifications
                // of any unhandled exception

                AppDomain.CurrentDomain.UnhandledException +=
                  (s, e) =>
                  {
                      Message("Event handler starting");
                      Delay(2000); // 2000，4000
                  };
                Thread thread = new Thread(() =>
                {
                    Message("Throwing exception.");
                    throw new Exception();
                });
                thread.Start();

                Delay(4000); // 4000，2000
            }
            finally
            {
                Message("Finally block running.");
            }
            /* 正如您在输出中看到的，新线程被分配了线程 ID 3，主线程被分配了线程 ID 1。操作系统计划线程 3 运行一段时间;它引发未经处理的异常，调用事件处理程序，
             *   然后进入睡眠状态。此后不久，操作系统意识到可以调度线程 1，但其代码立即将其置于睡眠状态。线程 1 首先唤醒并运行 finally 块，然后 2 秒后线程 3 唤醒，
             *   未处理的异常最终导致进程崩溃。
             * 此事件序列（执行的事件处理程序和进程在完成后崩溃）是典型的，但不能保证。一旦程序中出现未经处理的异常，
             *   所有赌注都将关闭;该程序现在处于未知且可能非常不稳定的状态，因此其行为可能是不可预测的。在这种情况下，如您所见，CLR 允许主线程继续运行并执行其 finally 块，
             *   即使它知道当控件到达 finally 块时，另一个线程位于 AppDomain 的未处理异常事件处理程序中。
             * 若要强调这一事实，请尝试更改延迟，以便主线程的睡眠时间比事件处理程序长。在这种情况下，最终块将永远不会执行！
             *   在线程 1 唤醒之前，该进程将被未处理的异常销毁。您还可以获得不同的结果，具体取决于引发异常的线程是否由线程池创建。
             *   因此，最佳做法是避免所有可能的未经处理的异常，无论它们发生在工作线程中还是在主线程中。
             * 这与任务有何关系？如果要关闭系统时，系统周围有未完成的任务怎么办？我们将在下一节中介绍任务取消。
             */
        }

        static void Delay(int i)
        {
            Message($"Sleeping for {i} ms");
            Thread.Sleep(i);
            Message("Awake");
        }

        static void Message(string text)
        {
            Console.WriteLine("{0}:{1:0000}:{2}",
                Thread.CurrentThread.ManagedThreadId,
                Clock.ElapsedMilliseconds, text);
        }
    }



    public class PiCalculator
    {
        public static string Calculate(int digits = 100)
        {
            // TODO 此示例使用 PiCalculator.Calculate（） 方法，我们将在第 21 章的“并行执行循环迭代”一节中进一步深入探讨。
            Thread.Sleep(5000);
            return "";
        }

        public static string Calculate(int batchSize,int digits = 100)
        {
            // TODO 此示例使用 PiCalculator.Calculate（） 方法，我们将在第 21 章的“并行执行循环迭代”一节中进一步深入探讨。
            Thread.Sleep(200);
            return "111111111";
        }
    }

    public class Utility
    {
        public static IEnumerable<char> BusySymbols()
        {
            string busySymbols = @"-\|/-\|/";
            int next = 0;
            while (true)
            {
                yield return busySymbols[next];
                next = (next + 1) % busySymbols.Length;
                yield return '\b';
            }
        }
    }
}