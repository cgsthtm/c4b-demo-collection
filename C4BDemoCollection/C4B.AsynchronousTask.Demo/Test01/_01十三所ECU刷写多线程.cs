﻿using C4B.AsynchronousTask.Demo.Test01.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace C4B.AsynchronousTask.Demo.Test01
{
    public partial class _01十三所ECU刷写多线程 : Form
    {
        /// <summary>
        /// CAN1线程终止Token
        /// </summary>
        private CancellationTokenSource can1TokenSource = new CancellationTokenSource();

        private CancellationToken can1CancelToken = new CancellationToken();

        /// <summary>
        /// CAN1线程集合
        /// </summary>
        private List<Task<List<string>>> can1TaskLst = new List<Task<List<string>>>();

        public _01十三所ECU刷写多线程()
        {
            InitializeComponent();
        }

        // 1.1 测试Task函数局部变量
        private async void button1_Click(object sender, EventArgs e)
        {
            List<CarLibParameter> carLibParamList = new List<CarLibParameter>()
            {
                new CarLibParameter(){ dm="B-SOP+1hotfix", dids="F187", carname="ECU Part Number", data="1155463401", bits=5, datacategory="BCD", iswritetoecu="是" },
                new CarLibParameter(){ dm="B-SOP+1hotfix", dids="F18A", carname="System Supplier Identifier", data="1801323601", bits=5, datacategory="BCD", iswritetoecu="否" },
                new CarLibParameter(){ dm="B-SOP+1hotfix", dids="F191", carname="ECU Hardware Number", data="1149813801", bits=5, datacategory="BCD", iswritetoecu="否" },
                new CarLibParameter(){ dm="B-SOP+1hotfix", dids="F192", carname="Supplier ECU Hardware Reference Number", data="5331314c2d2d30313330", bits=10, datacategory="HEX", iswritetoecu="否" },
                new CarLibParameter(){ dm="B-SOP+1hotfix", dids="F194", carname="Supplier ECU Software Reference Number", data="30313032303030303132", bits=10, datacategory="HEX", iswritetoecu="否" },
                new CarLibParameter(){ dm="B-SOP+1hotfix", dids="F1A0", carname="ECU Application Software Number", data="1154049701", bits=5, datacategory="BCD", iswritetoecu="否" },
                new CarLibParameter(){ dm="B-SOP+1hotfix", dids="F1A1", carname="ECU Calibration Software Number", data="1151081801", bits=5, datacategory="BCD", iswritetoecu="否" },
                new CarLibParameter(){ dm="B-SOP+1hotfix", dids="F1A2", carname="ECU NCF Reference Number", data="6401731105520904", bits=8, datacategory="HEX", iswritetoecu="是" },
                new CarLibParameter(){ dm="B-SOP+1hotfix", dids="F1A9", carname="ECU Configuration File Number", data="1149813501", bits=5, datacategory="BCD", iswritetoecu="是" },
                new CarLibParameter(){ dm="B-SOP+1hotfix", dids="F1AA", carname="ECU Programming Process File Number", data="1149813601", bits=5, datacategory="BCD", iswritetoecu="否" },
                new CarLibParameter(){ dm="B-SOP+1hotfix", dids="F183", carname="ECU Bootloader Software Reference Number", data="494D5520220701313033", bits=10, datacategory="HEX", iswritetoecu="否" },
            };
            can1TaskLst.Add(CAN1DataFunc("U-2", 0, new IntPtr(0x003b0001), "F54634AP6260456", carLibParamList));
            can1TaskLst.Add(CAN1DataFunc("U-1", 0, new IntPtr(0x003b0000), "F54634AP6260455", carLibParamList));
            List<string> can1EvlLst = await can1TaskLst[0];
            List<string> can2EvlLst = await can1TaskLst[1];
        }

        /// <summary>
        /// 通讯日志类型
        /// </summary>
        private enum LogType
        { Tx = 0, Rx, None }

        private void AppendLog(LogType logType, string title, string msg, bool clearLog = false)
        {
            if (logType == LogType.Tx)
            {
                Debug.WriteLine($" 发送：-----> {title} {msg} \r\n");
            }
            else if (logType == LogType.Rx)
            {
                Debug.WriteLine($" 接收：<----- {title} {msg} \r\n");
            }
            else
            {
                Debug.WriteLine($" {title} {msg} \r\n");
            }
        }

        /// <summary>
        /// 获取扫码枪序列号中的生产日期
        /// </summary>
        /// <param name="xlhByteData">序列号的字节数组</param>
        /// <returns>生产日期，如：220315</returns>
        private string GetManufactureDate(byte[] xlhByteData)
        {
            if (xlhByteData.Length < 15) return "";
            byte yearByte = xlhByteData[7]; // 年
            string yearStr = "";
            byte monthByte = xlhByteData[8]; // 月
            string monthStr = "";
            byte[] dayBytes = new byte[2]; // 日
            Array.Copy(xlhByteData, 9, dayBytes, 0, 2);
            string dayStr = Encoding.UTF8.GetString(dayBytes);
            switch (yearByte)
            {/** 解析序列号中包含的年月日，15位长度的序列号第8、9、10~11位分别代表年月日，年份代码表如下：
                            * 2006 --> 6
                            * 2009 --> 9
                            * 2010 --> A
                            * 2017 --> H
                            * 2018 --> J
                            * 2022 --> N
                            * 2023 --> P
                            * 2024 --> R
                            * 2027 --> v
                            * 2028 --> W
                            * 2029 --> X
                            * 2030 --> Y
                            * 2031 --> 1
                            * 2035 --> 5
                            */
                case 0x36: yearStr = "06"; break; // '6'
                case 0x37: yearStr = "07"; break; // '7'
                case 0x38: yearStr = "08"; break; // '8'
                case 0x39: yearStr = "09"; break; // '9'
                case 0x41: yearStr = "10"; break; // 'A'
                case 0x42: yearStr = "11"; break; // 'B'
                case 0x43: yearStr = "12"; break; // 'C'
                case 0x44: yearStr = "13"; break; // 'D'
                case 0x45: yearStr = "14"; break; // 'E'
                case 0x46: yearStr = "15"; break; // 'F'
                case 0x47: yearStr = "16"; break; // 'G'
                case 0x48: yearStr = "17"; break; // 'H'
                case 0x4A: yearStr = "18"; break; // 'J'
                case 0x4B: yearStr = "19"; break; // 'K'
                case 0x4C: yearStr = "20"; break; // 'L'
                case 0x4D: yearStr = "21"; break; // 'M'
                case 0x4E: yearStr = "22"; break; // 'N'
                case 0x50: yearStr = "23"; break; // 'P'
                case 0x52: yearStr = "24"; break; // 'R'
                case 0x53: yearStr = "25"; break; // 'S'
                case 0x54: yearStr = "26"; break; // 'T'
                case 0x56: yearStr = "27"; break; // 'V'
                case 0x57: yearStr = "28"; break; // 'W'
                case 0x58: yearStr = "29"; break; // 'X'
                case 0x59: yearStr = "30"; break; // 'Y'
                case 0x31: yearStr = "31"; break; // '1'
                case 0x32: yearStr = "32"; break; // '2'
                case 0x33: yearStr = "33"; break; // '3'
                case 0x34: yearStr = "34"; break; // '4'
                case 0x35: yearStr = "35"; break; // '5'
                default: yearStr = "22"; break;
            }
            switch (monthByte)
            {
                case 0x31: monthStr = "01"; break; // '1'
                case 0x32: monthStr = "02"; break; // '2'
                case 0x33: monthStr = "03"; break; // '3'
                case 0x34: monthStr = "04"; break; // '4'
                case 0x35: monthStr = "05"; break; // '5'
                case 0x36: monthStr = "06"; break; // '6'
                case 0x37: monthStr = "07"; break; // '7'
                case 0x38: monthStr = "08"; break; // '8'
                case 0x39: monthStr = "09"; break; // '9'
                case 0x41: monthStr = "10"; break; // 'A'
                case 0x42: monthStr = "11"; break; // 'B'
                case 0x43: monthStr = "12"; break; // 'C'
                default: monthStr = "01"; break;
            }
            return yearStr + monthStr + dayStr;
        }

        /// <summary>
        /// 16进制转byte[]
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static byte[] HexStrTobyte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2).Trim(), 16);
            return returnBytes;
        }

        /// <summary>
        /// 获取CAN1刷写参数队列
        /// </summary>
        private Tuple<Queue, Dictionary<string, byte[]>> GetCAN1WriteParamQueue(List<CarLibParameter> currcarLibParamList)
        {
            Dictionary<string, byte[]> writeParamDic = new Dictionary<string, byte[]>(); // 每次获取CAN1刷写参数前，先清空一下字典
            Queue writeParamQueue = new Queue();
            foreach (var item in currcarLibParamList)
            {
                // 根据协议文档可知，只要参数字节数大于4，就需要连续帧
                byte byteCount = (byte)(1 + 2 + item.bits); // 根据UDS协议，计算连续帧的全部字节数（写标识2e+DID字节数+数据字节数）；强制转换有风险，需限制录入车型库参数的字节数（0 < x < 253）
                int frameCount = 1; // 记录连续帧数量（除去了首帧，还剩多少帧）
                if (item.bits > 4) // 计算连续帧数量
                {
                    int exceptHeadByteLen = (byteCount - 6); // 除去首帧的2个头数据（0x10+有效数据长度），首帧数据还剩6，所以要减去6
                    frameCount = exceptHeadByteLen / 7; // 写入的参数应该用几帧数据发送（每帧8字节，连续帧的第一个字节是标识这是第几帧，所以连续帧有效数据是7个字节）
                    if (exceptHeadByteLen % 7 > 0)
                    {
                        frameCount += 1;
                    }
                }
                byte[] didBytes = HexStrTobyte(item.dids);
                byte[] dataBytes = new byte[item.bits]; // 写入参数的数据字节数组
                switch (item.datacategory)
                {
                    case "BCD": // 如：f187
                        dataBytes = HexStrTobyte(item.data);
                        break;

                    case "ASCII": // 如：1234
                        dataBytes = Encoding.UTF8.GetBytes(item.data);
                        break;

                    case "HEX": // 如：0xf1 0x87
                        dataBytes = HexStrTobyte(item.data);
                        break;

                    default:
                        break;
                }
                writeParamDic.Add(item.dids, dataBytes);
                if (item.iswritetoecu == "是") // 需要写入ECU的设备存入writeParamQueue队列
                {
                    // 根据协议文档可知，只要参数字节数大于4，就需要连续帧
                    if (item.bits > 4)
                    {
                        byte[] tempCmdBytes = new byte[8];
                        tempCmdBytes[0] = 0x10; // 标识为多帧数据
                        tempCmdBytes[1] = byteCount; // 有效字节个数
                        tempCmdBytes[2] = 0x2e; // 标识为写入
                        byte[] validDataBytes = new byte[2 + dataBytes.Length]; // 有效字节个数（2个DID字节+参数字节个数）
                        validDataBytes[0] = didBytes[0]; // did地低位
                        validDataBytes[1] = didBytes[1]; // did高位
                        Array.Copy(dataBytes, 0, validDataBytes, 2, dataBytes.Length);
                        Array.Copy(validDataBytes, 0, tempCmdBytes, 3, 5); // 组织首帧数据，每帧8字节
                        int indexOfValidDataBytes = 5;
                        Commander commander = new Commander(0);
                        byte[] tempSendBytes = new byte[10]; // CAN1透传刷写时是10个字节，因为需要加上Tx的标识0x07 0x25
                        tempSendBytes[0] = 0x07; // Tx的标识0x07 0x25
                        tempSendBytes[1] = 0x25;
                        Array.Copy(tempCmdBytes, 0, tempSendBytes, 2, tempCmdBytes.Length);
                        commander.cmdArray = tempSendBytes;
                        writeParamQueue.Enqueue(commander); // 首帧数据入队列
                        Commander rep = new Commander(1);
                        rep.cmdArray = new byte[3] { 0x07, 0x2d, 0x30 };
                        writeParamQueue.Enqueue(rep); // 首帧数据回传入队列
                        for (int j = 0; j < frameCount; j++) // 组织剩余帧数据
                        {
                            byte firstByteOfFrame = Convert.ToByte(0x21 + j); // 第一个字节代表第几个连续帧，如：0x21代表第一个连续帧，0x22代表第二个连续帧。可能会报错吗？
                            byte[] tempCmdBytes1 = new byte[8]; // 清空tempCmdBytes
                            tempCmdBytes1[0] = firstByteOfFrame;
                            if (validDataBytes.Length - indexOfValidDataBytes > 7)
                            {
                                Array.Copy(validDataBytes, indexOfValidDataBytes, tempCmdBytes1, 1, 7);
                                indexOfValidDataBytes += 7;
                            }
                            else
                            {
                                Array.Copy(validDataBytes, indexOfValidDataBytes, tempCmdBytes1, 1, validDataBytes.Length - indexOfValidDataBytes);
                            }
                            Commander commanderFrame = new Commander(0);
                            byte[] tempSendBytes1 = new byte[10]; // CAN1透传刷写时是10个字节，因为需要加上Tx的标识0x07 0x25
                            tempSendBytes1[0] = 0x07; // Tx的标识0x07 0x25
                            tempSendBytes1[1] = 0x25;
                            Array.Copy(tempCmdBytes1, 0, tempSendBytes1, 2, tempCmdBytes1.Length);
                            commanderFrame.cmdArray = tempSendBytes1;
                            writeParamQueue.Enqueue(commanderFrame); // 剩余帧数据入队列
                            if (frameCount - j == 1) // 最后一帧数据需要回传，非最后一帧无需回传
                            {
                                Commander repFrame = new Commander(1);
                                repFrame.cmdArray = new byte[6];
                                repFrame.cmdArray[0] = 0x07;
                                repFrame.cmdArray[1] = 0x2d;
                                repFrame.cmdArray[2] = 0x03;
                                repFrame.cmdArray[3] = 0x6e;
                                repFrame.cmdArray[4] = didBytes[0];
                                repFrame.cmdArray[5] = didBytes[1];
                                writeParamQueue.Enqueue(repFrame); // 最后一帧帧数据回传入队列
                            }
                        }
                    }
                    else // 只有一帧数据的情况下
                    {
                        byte[] tempCmdBytes = new byte[8];
                        tempCmdBytes[0] = byteCount;
                        tempCmdBytes[1] = 0x2e;
                        tempCmdBytes[2] = didBytes[0];
                        tempCmdBytes[3] = didBytes[1];
                        Array.Copy(dataBytes, 0, tempCmdBytes, 4, dataBytes.Length);
                        Commander commander = new Commander(0);
                        byte[] tempSendBytes = new byte[10]; // CAN1透传刷写时是10个字节，因为需要加上Tx的标识0x07 0x25
                        tempSendBytes[0] = 0x07; // Tx的标识0x07 0x25
                        tempSendBytes[1] = 0x25;
                        Array.Copy(tempCmdBytes, 0, tempSendBytes, 2, tempCmdBytes.Length);
                        commander.cmdArray = tempSendBytes;
                        writeParamQueue.Enqueue(commander); // 首帧数据入队列
                        Commander rep = new Commander(1);
                        rep.cmdArray = new byte[6];
                        rep.cmdArray[0] = 0x07;
                        rep.cmdArray[1] = 0x2d;
                        rep.cmdArray[2] = 0x03;
                        rep.cmdArray[3] = 0x6e;
                        rep.cmdArray[4] = didBytes[0];
                        rep.cmdArray[5] = didBytes[1];
                        writeParamQueue.Enqueue(rep); // 首帧数据回传入队列
                    }
                }
            }
            return Tuple.Create(writeParamQueue, writeParamDic);
        }

        /// <summary>
        /// 字节数组转换成十六进制字符串
        /// </summary>
        /// <param name="bytes">字节数组</param>
        /// <returns>字节数组对应的十六进制字符串</returns>
        public static string ToHexString(byte[] bytes, bool withSpace = true, bool reverse = false) // 0xae00cf => "AE00CF "
        {
            string hexString = string.Empty;
            if (bytes != null)
            {
                System.Text.StringBuilder strB = new System.Text.StringBuilder();
                bytes = reverse ? bytes.Reverse().ToArray() : bytes;
                for (int i = 0; i < bytes.Length; i++)
                {
                    if (withSpace)
                    {
                        strB.Append(bytes[i].ToString("X2") + " ");
                    }
                    else
                    {
                        strB.Append(bytes[i].ToString("X2"));
                    }
                }
                hexString = strB.ToString();
            }
            return hexString;
        }

        /// <summary>
        /// 获取CAN1读取参数队列
        /// </summary>
        private void GetCAN1ReadParamQueue(List<CarLibParameter> currcarLibParamList, ref Queue readParamQueue, ref Dictionary<string, byte[]> readParamDic)
        {
            readParamDic = new Dictionary<string, byte[]>(); // 每次获取CAN1读取参数前，先清空一下字典
            readParamQueue = new Queue();
            foreach (var item in currcarLibParamList)
            {
                // 根据协议文档可知，只要参数字节数大于4，就需要连续帧
                byte byteCount = (byte)(1 + 2 + item.bits); // 根据UDS协议，计算连续帧的全部字节数（写标识2e+DID字节数+数据字节数）；强制转换有风险，需限制录入车型库参数的字节数（0 < x < 253）
                int frameCount = 1; // 记录连续帧数量（除去了首帧，还剩多少帧）
                if (item.bits > 4) // 计算连续帧数量
                {
                    int exceptHeadByteLen = (byteCount - 6); // 除去首帧的2个头数据（0x10+有效数据长度），首帧数据还剩6，所以要减去6
                    frameCount = exceptHeadByteLen / 7; // 写入的参数应该用几帧数据发送（每帧8字节，连续帧的第一个字节是标识这是第几帧，所以连续帧有效数据是7个字节）
                    if (exceptHeadByteLen % 7 > 0)
                    {
                        frameCount += 1;
                    }
                }
                byte[] didBytes = HexStrTobyte(item.dids);
                /** 读取参数字典readParamDic要在实时读取时添加读取到的参数数据
                byte[] dataBytes = new byte[item.bits]; // 写入参数的数据字节数组
                switch (item.datacategory)
                {
                    case "BCD": // 如：f187
                        dataBytes = PubFun.HexStrTobyte(item.data);
                        break;

                    case "ASCII": // 如：1234
                        dataBytes = Encoding.UTF8.GetBytes(item.data);
                        break;

                    case "HEX": // 如：0xf1 0x87
                        dataBytes = PubFun.HexStrTobyte(item.data);
                        break;

                    default:
                        break;
                }
                readParamDic.Add(item.dids, dataBytes);
                 */
                // 根据协议文档可知，只要参数字节数大于4，就需要连续帧
                if (item.bits > 4)
                {
                    byte[] sendBytes = new byte[10]; // 发送给烧写器的字节数据（Tx标识0x07 0x25 + 一帧数据8字节）
                    sendBytes[0] = 0x07; // Tx标识 0x07 0x25
                    sendBytes[1] = 0x25;
                    sendBytes[2] = 0x03; // 读取标识 0x03 0x22
                    sendBytes[3] = 0x22;
                    sendBytes[4] = didBytes[0]; // DID标识
                    sendBytes[5] = didBytes[1];
                    Commander commander = new Commander(0);
                    commander.cmdArray = sendBytes;
                    readParamQueue.Enqueue(commander); // 读取参数指令入队列，如：0x07 0x25 0x03 0x22 0xf1 0x87 0x00 0x00 0x00 0x00
                    Commander rep = new Commander(1);
                    rep.cmdArray = new byte[7] { 0x07, 0x2d, 0x10, byteCount, 0x62, didBytes[0], didBytes[1] }; // 具有连续帧的响应除Tx外，第一个字节为0x10，第二个字节为该参数的有效数据长度（包括DID和参数值）
                    readParamQueue.Enqueue(rep); // 连续帧响应读取指令入队列
                    byte[] sendBytes1 = new byte[10]; // 响应连续帧，如：0x30 0x00 0x14
                    sendBytes1[0] = 0x07; // Tx标识 0x07 0x25
                    sendBytes1[1] = 0x25;
                    sendBytes1[2] = 0x30; // 响应连续帧 0x30 0x00 0x14
                    sendBytes1[3] = 0x00;
                    sendBytes1[4] = 0x14;
                    Commander commanderFrame = new Commander(0);
                    commanderFrame.cmdArray = sendBytes1;
                    readParamQueue.Enqueue(commanderFrame); // 响应连续帧指令入队列
                    for (int j = 0; j < frameCount; j++) // 组织剩余帧数据
                    {
                        Commander repFrame = new Commander(1);
                        byte firstByteOfFrame = Convert.ToByte(0x21 + j); // 第一个字节代表第几个连续帧，如：0x21代表第一个连续帧，0x22代表第二个连续帧。可能会报错吗？
                        repFrame.cmdArray = new byte[] { 0x07, 0x2d, firstByteOfFrame };
                        readParamQueue.Enqueue(repFrame); // 剩余帧数据入队列
                        /** 相旭说读取时下位机发送完最后一个连续帧后，上位机无需响应
                        if (frameCount - j == 1) // 最后一帧数据需要回传，非最后一帧无需回传
                        {
                            Commander commanderFrame1 = new Commander(0);
                            commanderFrame1.cmdArray = new byte[6];
                            commanderFrame1.cmdArray[0] = 0x07;
                            commanderFrame1.cmdArray[1] = 0x25;
                            commanderFrame1.cmdArray[2] = 0x03;
                            commanderFrame1.cmdArray[3] = 0x6e;
                            commanderFrame1.cmdArray[4] = didBytes[0];
                            commanderFrame1.cmdArray[5] = didBytes[1];
                            readParamQueue.Enqueue(commanderFrame1); // 最后一帧帧数据回传入队列
                        }
                        */
                    }
                }
                else // 只有一帧数据的情况下
                {
                    byte[] sendBytes = new byte[10]; // 发送给烧写器的字节数据（Tx标识0x07 0x25 + 一帧数据8字节）
                    sendBytes[0] = 0x07; // Tx标识 0x07 0x25
                    sendBytes[1] = 0x25;
                    sendBytes[2] = 0x03; // 读取标识 0x03 0x22
                    sendBytes[3] = 0x22;
                    sendBytes[4] = didBytes[0]; // DID标识
                    sendBytes[5] = didBytes[1];
                    Commander commander = new Commander(0);
                    commander.cmdArray = sendBytes;
                    readParamQueue.Enqueue(commander); // 读取参数指令入队列，如：0x07 0x25 0x03 0x22 0xf1 0x87 0x00 0x00 0x00 0x00
                    Commander rep = new Commander(1);
                    rep.cmdArray = new byte[6] { 0x07, 0x2d, byteCount, 0x62, didBytes[0], didBytes[1] }; // 具有连续帧的响应除Tx外，第一个字节为该参数的有效数据长度（包括DID和参数值）
                    readParamQueue.Enqueue(rep); // 连续帧响应读取指令入队列
                }
            }
            // 处理CAN1参数读取B002-B008
            AddCAN1B002ToB008ReadQueue(ref readParamQueue);
        }

        /// <summary>
        /// 添加CAN1B002到B008的读取参数队列
        /// </summary>
        public void AddCAN1B002ToB008ReadQueue(ref Queue readParamQueue)
        {
            AddReadParamQueue(ref readParamQueue, new byte[] { 0xb0, 0x02 }, 0x04);
            AddReadParamQueue(ref readParamQueue, new byte[] { 0xb0, 0x03 }, 0x05);
            AddReadParamQueue(ref readParamQueue, new byte[] { 0xb0, 0x04 }, 0x05);
            AddReadParamQueue(ref readParamQueue, new byte[] { 0xb0, 0x05 }, 0x05);
            AddReadParamQueue(ref readParamQueue, new byte[] { 0xb0, 0x06 }, 0x05);
            AddReadParamQueue(ref readParamQueue, new byte[] { 0xb0, 0x07 }, 0x05);
            AddReadParamQueue(ref readParamQueue, new byte[] { 0xb0, 0x08 }, 0x05);
        }

        /// <summary>
        /// 添加读取参数队列
        /// </summary>
        /// <param name="didBytes">DID字节数组，长度2</param>
        /// <param name="rxDataLen">响应帧第一个字节，代表该帧有效数据长度</param>
        public void AddReadParamQueue(ref Queue readParamQueue, byte[] didBytes, byte rxDataLen)
        {
            byte[] sendBytes = new byte[10]; // 发送给烧写器的字节数据（Tx标识0x07 0x25 + 一帧数据8字节）
            sendBytes[0] = 0x07; // Tx标识 0x07 0x25
            sendBytes[1] = 0x25;
            sendBytes[2] = 0x03; // 读取标识 0x03 0x22
            sendBytes[3] = 0x22;
            sendBytes[4] = didBytes[0]; // DID标识
            sendBytes[5] = didBytes[1];
            Commander commander = new Commander(0);
            commander.cmdArray = sendBytes;
            readParamQueue.Enqueue(commander); // 读取参数指令入队列，如：0x07 0x25 0x03 0x22 0xf1 0x87 0x00 0x00 0x00 0x00
            Commander rep = new Commander(1);
            rep.cmdArray = new byte[6] { 0x07, 0x2d, rxDataLen, 0x62, didBytes[0], didBytes[1] }; // 具有连续帧的响应除Tx外，第一个字节为该参数的有效数据长度（包括DID和参数值）
            readParamQueue.Enqueue(rep); // 连续帧响应读取指令入队列
        }

        private async Task<List<string>> CAN1DataFunc(string device_num_, int ch_num_, IntPtr channel_handle_, string product_number, List<CarLibParameter> carLibParamList)
        {
            uint bk = 2; // 流程点
            int currOldTick = Environment.TickCount; // 用于在时钟中记录时间间隔
            Queue writeParamQueue = new Queue(); // 写入参数队列
            Dictionary<string, byte[]> writeParamDic = new Dictionary<string, byte[]>(); // 写入参数字典
            Commander currResponseCmder = new Commander(); // 回传指令，用于判断预期的回传指令和接收到回传指令是否一致，从而确定ECU是否正常响应
            Queue readParamQueue = new Queue(); // 读取参数队列
            Dictionary<string, byte[]> readParamDic = new Dictionary<string, byte[]>(); // 读取参数字典

            //var currcarLibParamList = carLibParamList; // 模拟调试时发现的多个线程函数的局部变量writeParamDic的内容一样的情况
            var currcarLibParamList = new List<CarLibParameter>(carLibParamList); // 使用创建浅拷贝的方式，解决上述问题
            var title = $"{product_number} {device_num_}:{ch_num_}";

            while (true)
            {
                if (can1TokenSource.IsCancellationRequested)
                    can1TokenSource.Token.ThrowIfCancellationRequested();
                switch (bk)
                {
                    case 2:
                        {
                            var numberStr = product_number;
                            var carLibParamModel = currcarLibParamList.Where(x => x.dids == "F187").FirstOrDefault();
                            if (carLibParamModel != null)
                            {
                                AppendLog(LogType.None, title, $"车型库ECU Part Number参数为：{carLibParamModel.data}");
                                var ecuPartNumber = carLibParamModel.data.Substring(4, 4); // 5~8位
                                if (true) // 序列号与车型库匹配
                                {
                                    AppendLog(LogType.None, title, $"序列号{numberStr}与车型库ECU Part Number参数{carLibParamModel.data}匹配！");
                                    var manufactureDate = GetManufactureDate(Encoding.UTF8.GetBytes(numberStr));
                                    var serialNumber = $" {numberStr}"; // 0x20即空格，ECU Serial Number参数为：空格0+序列号
                                    AppendLog(LogType.None, title, $"序列号： {serialNumber}");
                                    var dm = "DM-AAAA";
                                    // 将扫码获得的ECU Manufacture Date以及ECU Serial Number存到车型库参数集合carLibParamList中
                                    var modelF18B = currcarLibParamList.Where(x => x.dids == "F18B").FirstOrDefault();
                                    if (modelF18B == null)
                                    {
                                        currcarLibParamList.Add(new CarLibParameter
                                        {
                                            dids = "F18B",
                                            dm = dm,
                                            carname = "ECU Manufacture Date",
                                            data = manufactureDate,
                                            bits = 3,
                                            datacategory = "BCD",
                                            iswritetoecu = "是"
                                        });
                                    }
                                    else
                                    {
                                        modelF18B.data = manufactureDate;
                                    }
                                    AppendLog(LogType.None, title, $"生成ECU Manufacture Date参数完毕！");
                                    var modelF18C = currcarLibParamList.Where(x => x.dids == "F18C").FirstOrDefault();
                                    if (modelF18C == null)
                                    {
                                        currcarLibParamList.Add(new CarLibParameter
                                        {
                                            dids = "F18C",
                                            dm = dm,
                                            carname = "ECU Serial Number",
                                            data = serialNumber,
                                            bits = 16,
                                            datacategory = "ASCII",
                                            iswritetoecu = "是"
                                        });
                                    }
                                    else
                                    {
                                        modelF18C.data = serialNumber;
                                    }
                                    AppendLog(LogType.None, title, $"生成ECU Serial Number参数完毕！ {serialNumber}");
                                    AppendLog(LogType.None, title, $"生成完之后的序列号：{currcarLibParamList.Where(x => x.dids == "F18C").FirstOrDefault().data}");
                                    AppendLog(LogType.None, title, $"开始进行CAN1检测？");
                                    currOldTick = Environment.TickCount;
                                    bk = 11; // 开始CAN1检测流程
                                }
                            }
                            else
                            {
                                AppendLog(LogType.None, title, $"车型库参数中不存在DID为F187的参数！无法检查序列号与车型库是否匹配！检测终止！");
                                bk = 999;
                            }
                        }
                        break;

                    case 11: // 开始CAN1检测流程：参数写-->准备参数
                        {
                            try
                            {
                                var tupleObj = GetCAN1WriteParamQueue(currcarLibParamList); // 获取CAN1参数刷写队列
                                writeParamQueue = tupleObj.Item1;
                                writeParamDic = tupleObj.Item2;
                                AppendLog(LogType.None, title, $"获取CAN1参数刷写队列完毕！");
                                foreach (var item in writeParamDic)
                                {
                                    AppendLog(LogType.None, title, $"遍历CAN1参数刷写字典 {item.Key} - {ToHexString(item.Value)}");
                                }
                                currOldTick = Environment.TickCount;
                                bk = 12; // 开始CAN1检测流程：参数写-->进行刷写
                            }
                            catch (Exception ex)
                            {
                                AppendLog(LogType.None, title, $"准备车型库参数写入队列失败！发生错误：{ex.ToString()}");
                                bk = 9999;
                            }
                        }
                        break;

                    case 12: // 开始CAN1检测流程：参数写-->进行刷写
                        {
                            // 处理参数刷写队列
                            if (writeParamQueue.Count > 0)
                            {
                                Commander commander = writeParamQueue.Dequeue() as Commander;
                                if (commander.cmdType == 0) // 如果是发送指令
                                {
                                    byte[] sendBytes = commander.cmdArray;
                                    AppendLog(LogType.Tx, title, ToHexString(sendBytes));
                                    Commander tempCmder = writeParamQueue.Peek() as Commander;
                                    if (tempCmder.cmdType == 1) // 如果是回传指令
                                    {
                                        currResponseCmder = writeParamQueue.Dequeue() as Commander;
                                        AppendLog(LogType.None, title, $"预期回传：{ToHexString(currResponseCmder.cmdArray)}");
                                        currOldTick = Environment.TickCount;
                                        bk = 13; // 开始CAN1检测流程：参数写-->进行刷写-->处理回传
                                    }
                                }
                            }
                        }
                        break;

                    case 13: // 开始CAN1检测流程：参数写-->进行刷写-->处理回传
                        {
                            if (true)
                            {
                                if (true)
                                {
                                    if (writeParamQueue.Count > 0)
                                    {
                                        Commander commandertmp = writeParamQueue.Dequeue() as Commander;
                                        if (commandertmp.cmdType == 1) // 这种情况按理不会出来，因为从协议中看，没有也不需要多帧回传指令
                                        {
                                        }
                                        else
                                        {
                                            bk = 12; // 回到开始CAN1检测流程：参数写-->进行刷写，继续进行刷写
                                        }
                                    }
                                    else // 如果刷写队列中没有数据
                                    {
                                        AppendLog(LogType.None, title, $"队列中所有参数均已刷写完毕!");
                                        AppendLog(LogType.None, title, $"开始读取CAN1写入的参数!");
                                        bk = 16; // 开始CAN1检测流程：参数读-->发送扩展诊断会话指令
                                    }
                                }
                            }
                        }
                        break;

                    case 16: // 开始CAN1检测流程：参数读-->准备参数
                        {
                            //carLibParameter
                            // 遍历carLibParameter车型库参数列表
                            readParamDic.Clear();
                            //progressBar1.Maximum = carLibParamList.Count;
                            try
                            {
                                GetCAN1ReadParamQueue(currcarLibParamList, ref readParamQueue, ref readParamDic); // 获取CAN1参数读取队列
                                AppendLog(LogType.None, title, $"获取CAN1参数读取队列完毕！");
                                currOldTick = Environment.TickCount;
                                bk = 17; // 开始CAN1检测流程：参数读-->进行读取
                            }
                            catch (Exception ex)
                            {
                                AppendLog(LogType.None, title, $"准备车型库参数读取队列失败！发生错误：{ex.ToString()}");
                                bk = 9999;
                            }
                        }
                        break;

                    case 17: // 开始CAN1检测流程：参数读-->进行读取
                        {
                            // 处理参数读取队列
                            if (readParamQueue.Count > 0)
                            {
                                Commander commander = readParamQueue.Dequeue() as Commander;
                                if (commander.cmdType == 0) // 如果是发送指令
                                {
                                    byte[] sendBytes = commander.cmdArray;
                                    AppendLog(LogType.Tx, title, ToHexString(sendBytes));
                                    Commander tempCmder = readParamQueue.Peek() as Commander;
                                    if (tempCmder.cmdType == 1) // 如果是回传指令
                                    {
                                        currResponseCmder = readParamQueue.Dequeue() as Commander;
                                        AppendLog(LogType.None, title, $"预期回传：{ToHexString(currResponseCmder.cmdArray)}");
                                        currOldTick = Environment.TickCount;
                                        bk = 18; // 开始CAN1检测流程：参数读-->进行读取-->处理回传
                                    }
                                }
                            }
                        }
                        break;

                    case 99: // 检测结束
                        {
                            AppendLog(LogType.None, title, $"CAN1检测结束...");
                            bk = 99999;
                        }
                        break;

                    case 999: // 检测终止
                        {
                            AppendLog(LogType.None, title, $"CAN1检测终止...");
                            bk = 99999;
                        }
                        break;

                    case 9999: // 检测异常终止
                        {
                            AppendLog(LogType.None, title, $"CAN1检测异常终止...");
                            bk = 99999;
                        }
                        break;

                    case 99999: // 防止取消线程后依然执行一小段时间导致重复执行某一节点的问题
                        {
                            return new List<string>() { "can1Evl", "dtcEvl", "testtimes.ToString()", product_number };
                        }
                    default:
                        bk = 99999;
                        break;
                }
                await Task.Delay(2000).ConfigureAwait(false);
            }
        }

        // 1.2 测试盲发时间间隔
        private async void button2_Click(object sender, EventArgs e)
        {
            channelHandleDic.Add("U-1", new Dictionary<int, IntPtr>() {
                { 0, new IntPtr(0x003b0000) },{ 1, new IntPtr(0x003b0001) },
                { 2, new IntPtr(0x003b0002) },{ 3, new IntPtr(0x003b0003) },
                { 4, new IntPtr(0x003b0004) },{ 5, new IntPtr(0x003b0005) },
                { 6, new IntPtr(0x003b0006) },{ 7, new IntPtr(0x003b0007) },
            });
            channelHandleDic.Add("U-2", new Dictionary<int, IntPtr>() {
                { 0, new IntPtr(0x003b0100) },{ 1, new IntPtr(0x003b0101) },
                { 2, new IntPtr(0x003b0102) },{ 3, new IntPtr(0x003b0103) },
                { 4, new IntPtr(0x003b0104) },{ 5, new IntPtr(0x003b0105) },
                { 6, new IntPtr(0x003b0106) },{ 7, new IntPtr(0x003b0107) },
            });
            channelHandleDic.Add("U-3", new Dictionary<int, IntPtr>() {
                { 0, new IntPtr(0x003b0200) },{ 1, new IntPtr(0x003b0201) },
                { 2, new IntPtr(0x003b0202) },{ 3, new IntPtr(0x003b0203) },
                { 4, new IntPtr(0x003b0204) },{ 5, new IntPtr(0x003b0205) },
                { 6, new IntPtr(0x003b0206) },{ 7, new IntPtr(0x003b0207) },
            });

            //blindCancelToken = blindCancelTokenSource.Token;
            //BlindSend();
            //await Task.Delay(10000).ConfigureAwait(false);
            //if (blindCancelTokenSource != null)
            //    blindCancelTokenSource.Cancel(); // 取消盲发线程
            //foreach (var blindTask in blindSendTaskLst)
            //{
            //    try
            //    {
            //        await blindTask;
            //    }
            //    catch (OperationCanceledException ex)
            //    {
            //        Debug.WriteLine($"盲发线程：{blindTask.Id} {nameof(OperationCanceledException)} thrown with message: {ex.Message}");
            //    }
            //    finally
            //    {
            //        blindTask.Dispose();
            //        //blindCancelTokenSource.Dispose();
            //    }
            //}
            //blindSendTaskLst.Clear();

            BlindSend1();
            await Task.Delay(10000).ConfigureAwait(false);
            foreach (var item in blindSendTimerLst)
            {
                item.Stop();
                item.Dispose();
            }
        }

        /// <summary>
        /// CAN最大数据长度
        /// </summary>
        private const int CAN_MAX_DLEN = 8;

        /// <summary>
        /// 生产CANID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="eff"></param>
        /// <param name="rtr"></param>
        /// <param name="err"></param>
        /// <returns></returns>
        private uint MakeCanId(uint id, int eff, int rtr, int err)//1:extend frame 0:standard frame
        {
            uint ueff = (uint)(!!(Convert.ToBoolean(eff)) ? 1 : 0);
            uint urtr = (uint)(!!(Convert.ToBoolean(rtr)) ? 1 : 0);
            uint uerr = (uint)(!!(Convert.ToBoolean(err)) ? 1 : 0);
            return id | ueff << 31 | urtr << 30 | uerr << 29;
        }

        /// <summary>
        /// 拆分text到发送data数组
        /// </summary>
        /// <param name="data"></param>
        /// <param name="transData"></param>
        /// <param name="maxLen"></param>
        /// <returns></returns>
        private int SplitData(string data, ref byte[] transData, int maxLen)
        {
            string[] dataArray = data.Split(' ');
            for (int i = 0; (i < maxLen) && (i < dataArray.Length); i++)
            {
                transData[i] = Convert.ToByte(dataArray[i].Substring(0, 2), 16);
            }

            return dataArray.Length;
        }

        /// <summary>
        /// 记录日志到富文本框
        /// </summary>
        /// <param name="logType">日志类型</param>
        /// <param name="msg">日志内容</param>
        /// <param name="clearLog">是否清除文本框的日志</param>
        private void AppendLog(LogType logType, string msg, bool clearLog = false)
        {
            if (logType == LogType.Tx)
            {
                Debug.WriteLine($" 发送：-----> {msg} \r\n");
            }
            else if (logType == LogType.Rx)
            {
                Debug.WriteLine($" 接收：<----- {msg} \r\n");
            }
            else
            {
                Debug.WriteLine($" {msg} \r\n");
            }
        }

        /// <summary>
        /// 给指定通道发送CAN数据帧
        /// </summary>
        /// <param name="device_num_">设备序号/名称，如U-1</param>
        /// <param name="ch_num_">通道号，如0</param>
        /// <param name="channel_handle_">通道句柄</param>
        /// <param name="hexIDStr">帧ID，如：0725</param>
        /// <param name="hexDataStr">发送的数据，用空格隔开，如：02 10 03 00 00 00 00 00</param>
        /// <param name="appendLog">是否打印日志</param>
        /// <returns></returns>
        private bool SendCANData(string device_num_, int ch_num_, IntPtr channel_handle_, string hexIDStr = "0725", string hexDataStr = "02 10 03 00 00 00 00 00", bool appendLog = false)
        {
            uint id = (uint)System.Convert.ToInt32(hexIDStr, 16);
            string data = hexDataStr;
            int frame_type_index = 0; // 帧类型 0-标准帧 1-扩展帧
            //int protocol_index = 0;   // 协议 0-CAN 1-CANFD
            int send_type_index = 0;  // 发送方式 0-正常发送 1-单次发送 2-自发自收 3-单次自发自收
            //int canfd_exp_index = 0;  // CANFD加速 0-否 1-是
            uint result; //发送的帧数

            // CAN协议
            ZCAN_Transmit_Data can_data = new ZCAN_Transmit_Data();
            can_data.frame.can_id = MakeCanId(id, frame_type_index, 0, 0);
            can_data.frame.data = new byte[8];
            can_data.frame.can_dlc = (byte)SplitData(data, ref can_data.frame.data, CAN_MAX_DLEN);
            can_data.transmit_type = (uint)send_type_index;
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(can_data));
            Marshal.StructureToPtr(can_data, ptr, true);
            //result = Method.ZCAN_Transmit(channel_handle_, ptr, 1); // 测试不连CAN接口卡，注释掉先
            result = 1;
            Marshal.FreeHGlobal(ptr);

            if (appendLog)
                AppendLog(LogType.Tx, $"{device_num_}:{ch_num_} 发数据：{ToHexString(can_data.frame.data)}");
            return result == 1;
        }

        /// <summary>
        /// 给指定通道发送CAN数据帧
        /// </summary>
        /// <param name="device_num_">设备序号/名称，如U-1</param>
        /// /// <param name="ch_num_">通道号，如0</param>
        /// <param name="channel_handle_">通道句柄</param>
        /// <param name="hexIDStr">帧ID，如：0725</param>
        /// <param name="data">发送的数据 字节数组</param>
        /// <param name="appendLog">是否打印日志</param>
        /// <returns></returns>
        private bool SendCANData(string device_num_, int ch_num_, IntPtr channel_handle_, string hexIDStr, byte[] data, bool appendLog = false)
        {
            uint id = (uint)System.Convert.ToInt32(hexIDStr, 16);
            int frame_type_index = 0; // 帧类型 0-标准帧 1-扩展帧
            //int protocol_index = 0;   // 协议 0-CAN 1-CANFD
            int send_type_index = 0;  // 发送方式 0-正常发送 1-单次发送 2-自发自收 3-单次自发自收
            //int canfd_exp_index = 0;  // CANFD加速 0-否 1-是
            uint result; //发送的帧数

            // CAN协议
            ZCAN_Transmit_Data can_data = new ZCAN_Transmit_Data();
            can_data.frame.can_id = MakeCanId(id, frame_type_index, 0, 0);
            can_data.frame.data = data;
            can_data.frame.can_dlc = 8;
            can_data.transmit_type = (uint)send_type_index;
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(can_data));
            Marshal.StructureToPtr(can_data, ptr, true);
            //result = Method.ZCAN_Transmit(channel_handle_, ptr, 1); // 测试用 注释先
            result = 1;
            Marshal.FreeHGlobal(ptr);

            if (appendLog)
                AppendLog(LogType.Tx, $"{device_num_}:{ch_num_} 发数据：{ToHexString(can_data.frame.data)}");
            return result == 1;
        }

        /// <summary>
        /// 盲发线程终止Token
        /// </summary>
        private CancellationTokenSource blindCancelTokenSource = new CancellationTokenSource();

        private CancellationToken blindCancelToken = new CancellationToken();

        /// <summary>
        /// 盲发线程集合
        /// </summary>
        private List<Task> blindSendTaskLst = new List<Task>();

        private List<System.Timers.Timer> blindSendTimerLst = new List<System.Timers.Timer>();

        /// <summary>
        /// 盲发算法计算第一个字节的值
        /// </summary>
        /// <param name="Data_test"></param>
        /// <returns></returns>
        private byte CalculateCRC8(byte[] Data_test)
        {
            byte i, j, crc8, poly;
            crc8 = 0;
            poly = 0x1D;
            for (i = 1; i < 8; i++)
            {
                crc8 ^= Data_test[i];
                for (j = 0; j < 8; j++)
                {
                    if (Convert.ToBoolean(crc8 & 0x80))
                    {
                        crc8 = (byte)Convert.ToInt32((crc8 << 1) ^ poly);
                    }
                    else
                    {
                        crc8 <<= 1;
                    }
                }
            }
            return crc8;
        }

        /// <summary>
        /// 存放接口卡序列号和对应的通道及通道句柄
        /// 例如：<U-1,[<0,0x3b000000>,<1,0x3c000000>,...]> 表示 U-1接口卡的0通道对应的通道句柄为0x3b000000，U-1接口卡的1通道对应的通道句柄为0x3c000000 ...
        /// </summary>
        private Dictionary<string, Dictionary<int, IntPtr>> channelHandleDic = new Dictionary<string, Dictionary<int, IntPtr>>();

        /// <summary>
        /// CAN1通道号数组 目前固定死 4个接口卡的0,2,4,6代表CAN1
        /// </summary>
        private readonly int[] CAN1ChArr = new int[] { 0, 2, 4, 6 };

        /// <summary>
        /// CAN2通道号数组 目前固定死 4个接口卡的1,3,5,7代表CAN2
        /// </summary>
        private readonly int[] CAN2ChArr = new int[] { 1, 3, 5, 7 };

        /// <summary>
        /// 盲发CAN1/CAN2数据
        /// </summary>
        private void BlindSend()
        {
            TaskFactory factory = new TaskFactory(blindCancelToken);
            // 遍历通道字典创建任务线程
            foreach (var item in channelHandleDic)
            {
                foreach (var chs in item.Value)
                {
                    if (CAN1ChArr.Contains(chs.Key)) // 当前接口卡设备的当前通道
                    {
                        // 100ms
                        blindSendTaskLst.Add(factory.StartNew(() =>
                        {
                            List<string> lst = new List<string>();
                            int can1_blind_rc_i = 0;
                            byte[] can1_blind_data = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
                            do
                            {
                                if (blindCancelToken.IsCancellationRequested)
                                {
                                    foreach (var it in lst)
                                    {
                                        Debug.WriteLine(LogType.Tx, $"{it}");
                                    }
                                    blindCancelToken.ThrowIfCancellationRequested();
                                }
                                Thread.Sleep(100);
                                SendCANData(item.Key, chs.Key, chs.Value, "0335", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 100ms
                                SendCANData(item.Key, chs.Key, chs.Value, "043A", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 100ms
                                SendCANData(item.Key, chs.Key, chs.Value, "033F", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 100ms
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 35 00 00 00 00 00 00 00 00 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：04 3A 00 00 00 00 00 00 00 00 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 3F 00 00 00 00 00 00 00 00 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");

                                can1_blind_rc_i = can1_blind_rc_i > 0x0F ? 0 : can1_blind_rc_i;
                                byte crc = CalculateCRC8(can1_blind_data);
                                can1_blind_data[0] = crc;
                                can1_blind_data[1] = (byte)can1_blind_rc_i;
                                SendCANData(item.Key, chs.Key, chs.Value, "032A", can1_blind_data); // 100ms
                                SendCANData(item.Key, chs.Key, chs.Value, "03CE", can1_blind_data); // 100ms
                                SendCANData(item.Key, chs.Key, chs.Value, "036A", can1_blind_data); // 100ms
                                SendCANData(item.Key, chs.Key, chs.Value, "032B", can1_blind_data); // 100ms
                                SendCANData(item.Key, chs.Key, chs.Value, "0359", can1_blind_data); // 100ms
                                lst.Add($"{item.Key}:{chs.Key} 0359 100ms {DateTime.Now.ToString("mm:ss:fff")}");
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 2A {ToHexString(can1_blind_data)} 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 CE {ToHexString(can1_blind_data)} 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 6A {ToHexString(can1_blind_data)} 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 2B {ToHexString(can1_blind_data)} 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 59 {ToHexString(can1_blind_data)} 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                                can1_blind_rc_i++;
                            } while (true);
                        }, blindCancelTokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default));
                        // 10ms
                        blindSendTaskLst.Add(factory.StartNew(() =>
                        {
                            List<string> lst = new List<string>();
                            do
                            {
                                if (blindCancelToken.IsCancellationRequested)
                                {
                                    foreach (var it in lst)
                                    {
                                        Debug.WriteLine(LogType.Tx, $"{it}");
                                    }
                                    blindCancelToken.ThrowIfCancellationRequested();
                                }
                                Thread.Sleep(10); // 10ms
                                SendCANData(item.Key, chs.Key, chs.Value, "00B4", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 10ms
                                lst.Add($"{item.Key}:{chs.Key} 00B4 10ms {DateTime.Now.ToString("mm:ss:fff")}");
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：00 B4 00 00 00 00 00 00 00 00 频率：10ms {DateTime.Now.ToString("mm:ss:fff")}");
                            } while (true);
                        }, blindCancelTokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default));
                        // 20ms
                        blindSendTaskLst.Add(factory.StartNew(() =>
                        {
                            do
                            {
                                if (blindCancelToken.IsCancellationRequested)
                                    blindCancelToken.ThrowIfCancellationRequested();
                                Thread.Sleep(20);
                                SendCANData(item.Key, chs.Key, chs.Value, "01F1", new byte[] { 0x12, 0x0b, 0xef, 0xff, 0xfc, 0x6e, 0x05, 0x3f }); // 20ms
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：01 F1 12 0B EF FF FC 6E 05 3F 频率：20ms {DateTime.Now.ToString("mm:ss:fff")}");
                            } while (true);
                        }, blindCancelTokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default));
                        // 200ms
                        blindSendTaskLst.Add(factory.StartNew(() =>
                        {
                            do
                            {
                                if (blindCancelToken.IsCancellationRequested)
                                    blindCancelToken.ThrowIfCancellationRequested();
                                Thread.Sleep(200);
                                SendCANData(item.Key, chs.Key, chs.Value, "0541", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 200ms
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：05 41 00 00 00 00 00 00 00 00 频率：200ms {DateTime.Now.ToString("mm:ss:fff")}");
                            } while (true);
                        }, blindCancelTokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default));
                    }
                    if (CAN2ChArr.Contains(chs.Key))
                    {
                        // 50ms
                        blindSendTaskLst.Add(factory.StartNew(() =>
                        {
                            do
                            {
                                if (blindCancelToken.IsCancellationRequested)
                                    blindCancelToken.ThrowIfCancellationRequested();
                                Thread.Sleep(50);
                                SendCANData(item.Key, chs.Key, chs.Value, "0014", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 50ms
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：00 14 00 00 00 00 00 00 00 00 频率：50ms {DateTime.Now.ToString("mm:ss:fff")}");
                            } while (true);
                        }, blindCancelTokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default));
                        // 20ms
                        blindSendTaskLst.Add(factory.StartNew(() =>
                        {
                            do
                            {
                                if (blindCancelToken.IsCancellationRequested)
                                    blindCancelToken.ThrowIfCancellationRequested();
                                Thread.Sleep(20);
                                SendCANData(item.Key, chs.Key, chs.Value, "0161", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 20ms
                                //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：01 61 00 00 00 00 00 00 00 00 频率：20ms {DateTime.Now.ToString("mm:ss:fff")}");
                            } while (true);
                        }, blindCancelTokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default));
                    }
                }
            }
        }

        private void BlindSend1()
        {

            foreach (var item in channelHandleDic)
            {
                foreach (var chs in item.Value)
                {
                    if (CAN1ChArr.Contains(chs.Key)) // 当前接口卡设备的当前通道
                    {
                        // 100ms
                        int can1_blind_rc_i = 0;
                        byte[] can1_blind_data = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
                        List<string> lst100ms = new List<string>();
                        System.Timers.Timer timer100ms = new System.Timers.Timer(100);
                        timer100ms.AutoReset = true;
                        timer100ms.Elapsed += (sender, e) =>
                        {
                            SendCANData(item.Key, chs.Key, chs.Value, "0335", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 100ms
                            SendCANData(item.Key, chs.Key, chs.Value, "043A", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 100ms
                            SendCANData(item.Key, chs.Key, chs.Value, "033F", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 100ms
                            //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 35 00 00 00 00 00 00 00 00 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                            //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：04 3A 00 00 00 00 00 00 00 00 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                            //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 3F 00 00 00 00 00 00 00 00 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");

                            can1_blind_rc_i = can1_blind_rc_i > 0x0F ? 0 : can1_blind_rc_i;
                            byte crc = CalculateCRC8(can1_blind_data);
                            can1_blind_data[0] = crc;
                            can1_blind_data[1] = (byte)can1_blind_rc_i;
                            SendCANData(item.Key, chs.Key, chs.Value, "032A", can1_blind_data); // 100ms
                            SendCANData(item.Key, chs.Key, chs.Value, "03CE", can1_blind_data); // 100ms
                            SendCANData(item.Key, chs.Key, chs.Value, "036A", can1_blind_data); // 100ms
                            SendCANData(item.Key, chs.Key, chs.Value, "032B", can1_blind_data); // 100ms
                            SendCANData(item.Key, chs.Key, chs.Value, "0359", can1_blind_data); // 100ms
                            lst100ms.Add($"{item.Key}:{chs.Key} 0359 100ms {DateTime.Now.ToString("mm:ss:fff")}");
                            //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 2A {ToHexString(can1_blind_data)} 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                            //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 CE {ToHexString(can1_blind_data)} 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                            //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 6A {ToHexString(can1_blind_data)} 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                            //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 2B {ToHexString(can1_blind_data)} 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                            //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：03 59 {ToHexString(can1_blind_data)} 频率：100ms {DateTime.Now.ToString("mm:ss:fff")}");
                            can1_blind_rc_i++;
                        };
                        timer100ms.Disposed += (sender, e) =>
                        {
                            foreach (var it in lst100ms)
                            {
                                Debug.WriteLine(LogType.Tx, $"{it}");
                            }
                        };
                        timer100ms.Enabled = true;
                        blindSendTimerLst.Add(timer100ms);
                        // 10ms
                        List<string> lst10ms = new List<string>();
                        System.Timers.Timer timer10ms = new System.Timers.Timer(10);
                        timer10ms.AutoReset = true;
                        timer10ms.Elapsed += (sender, e) =>
                        {
                            SendCANData(item.Key, chs.Key, chs.Value, "00B4", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 10ms
                            lst10ms.Add($"{item.Key}:{chs.Key} 00B4 10ms {DateTime.Now.ToString("mm:ss:fff")}");
                        };
                        timer10ms.Disposed += (sender, e) =>
                        {
                            foreach (var it in lst10ms)
                            {
                                Debug.WriteLine(LogType.Tx, $"{it}");
                            }
                        };
                        timer10ms.Enabled = true;
                        blindSendTimerLst.Add(timer10ms);
                        // 20ms
                        System.Timers.Timer timer20ms = new System.Timers.Timer(10);
                        timer20ms.AutoReset = true;
                        timer20ms.Elapsed += (sender, e) =>
                        {
                            SendCANData(item.Key, chs.Key, chs.Value, "01F1", new byte[] { 0x12, 0x0b, 0xef, 0xff, 0xfc, 0x6e, 0x05, 0x3f }); // 20ms
                            //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：01 F1 12 0B EF FF FC 6E 05 3F 频率：20ms {DateTime.Now.ToString("mm:ss:fff")}");
                        };
                        timer20ms.Enabled = true;
                        blindSendTimerLst.Add(timer20ms);
                        // 200ms
                        System.Timers.Timer timer200ms = new System.Timers.Timer(10);
                        timer200ms.AutoReset = true;
                        timer200ms.Elapsed += (sender, e) =>
                        {
                            SendCANData(item.Key, chs.Key, chs.Value, "0541", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 200ms
                            //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：05 41 00 00 00 00 00 00 00 00 频率：200ms {DateTime.Now.ToString("mm:ss:fff")}");
                        };
                        timer200ms.Enabled = true;
                        blindSendTimerLst.Add(timer200ms);
                    }
                    if (CAN2ChArr.Contains(chs.Key))
                    {
                        // 50ms
                        System.Timers.Timer timer50ms = new System.Timers.Timer(10);
                        timer50ms.AutoReset = true;
                        timer50ms.Elapsed += (sender, e) =>
                        {
                            SendCANData(item.Key, chs.Key, chs.Value, "0014", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 50ms
                            //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：00 14 00 00 00 00 00 00 00 00 频率：50ms {DateTime.Now.ToString("mm:ss:fff")}");
                        };
                        timer50ms.Enabled = true;
                        blindSendTimerLst.Add(timer50ms);
                        // 50ms
                        System.Timers.Timer timer20ms = new System.Timers.Timer(10);
                        timer20ms.AutoReset = true;
                        timer20ms.Elapsed += (sender, e) =>
                        {
                            SendCANData(item.Key, chs.Key, chs.Value, "0161", new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }); // 20ms
                            //Debug.WriteLine(LogType.Tx, $"{item.Key}:{chs.Key} 盲发数据：01 61 00 00 00 00 00 00 00 00 频率：20ms {DateTime.Now.ToString("mm:ss:fff")}");
                        };
                        timer20ms.Enabled = true;
                        blindSendTimerLst.Add(timer20ms);
                    }
                }
            }
        }
    }
}