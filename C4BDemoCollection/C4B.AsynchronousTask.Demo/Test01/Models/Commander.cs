﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.AsynchronousTask.Demo.Test01.Models
{
    /// <summary>
    /// 命令
    /// </summary>
    public class Commander
    {
        public Commander()
        {

        }
        /// <summary>
        /// 命令构造
        /// </summary>
        /// <param name="pcmdType"> 命令类型 0：发送指令 1：预期的回复指令，用于比较</param>
        /// <param name="pcmdIndex">命令执行索引</param>
        public Commander(int pcmdType = 0, int pcmdIndex = 0)
        {
            _cmdType = pcmdType;
            cmdIndex = pcmdIndex;
        }
        private int _cmdType = 0;
        /// <summary>
        /// 命令类型 0：发送指令 1：预期的回复指令
        /// </summary>
        public int cmdType
        {
            get { return _cmdType; }
            set { _cmdType = value; }
        }
        private byte[] _cmdArray = new byte[8].Select(p => p = 0x00).ToArray();//debug2021-05-11 new add
        /// <summary>
        /// 命令字节  _cmdType=1 时即为预期要接收到的内容，用于比对
        /// </summary>
        public byte[] cmdArray
        {
            get { return _cmdArray; }
            set { _cmdArray = value; }
        }
        private bool _needResponse = false;
        /// <summary>
        /// true :需要等待回复 fasle:不用回复
        /// </summary>
        public bool needResponse
        {
            get { return _needResponse; }
            set { _needResponse = value; }
        }
        private byte[] _response;
        /// <summary>
        /// 预期回复内容
        /// </summary>
        public byte[] response
        {
            get { return _response; }
            set { _response = value; }
        }


        private int _cmdIndex = 0;
        /// <summary>
        /// 命令索引顺序
        /// </summary>
        public int cmdIndex
        {
            get { return _cmdIndex; }
            set { _cmdIndex = value; }
        }
        private string _cmdDescription = "";
        /// <summary>
        /// 命令描述
        /// </summary>
        public string cmdDescription
        {
            get { return _cmdDescription; }
            set { _cmdDescription = value; }
        }

    }
}
