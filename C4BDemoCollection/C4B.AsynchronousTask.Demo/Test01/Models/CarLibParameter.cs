﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.AsynchronousTask.Demo.Test01.Models
{
    /// <summary>
    /// CarLibParameter 的摘要说明
    /// </summary>
    public class CarLibParameter
    {
        /// <summary>
        /// 车型库ID（主键）
        /// </summary>
        public string dm { get; set; }

        /// <summary>
        /// DIDS（主键）
        /// </summary>
        public string dids { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string carname { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public string data { get; set; }

        /// <summary>
        /// 字节数
        /// </summary>
        public int bits { get; set; }

        /// <summary>
        /// 数据类型
        /// </summary>
        public string datacategory { get; set; }

        /// <summary>
        /// 标识该DID参数是否写入ECU
        /// </summary>
        public string iswritetoecu { get; set; }
    }
}
