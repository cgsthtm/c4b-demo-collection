﻿using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1
{
    /* 本章介绍在异步操作中使用async和await的基础知识。本章只涉及本质上适合异步的操作，例如HTTP请求、数据库指令、Web服务调用等。
     * 如果要把CPU密集型的操作当作异步操作来处理（让它不阻塞UI线程），请阅读第3章和7.4节。另外，本章只涉及只启动一次、结束一次的操作。如果要处理事件流，请阅读第5章。
     */
    public partial class _2异步编程基础 : Form
    {
        public _2异步编程基础()
        {
            InitializeComponent();
        }

        // 2.1 暂停一段时间
        private void button1_Click(object sender, EventArgs e)
        {
            /* 需要让程序（以异步方式）等待一段时间。这在进行单元测试或者实现重试延迟时非常有用。本解决方案也能用于实现简单的超时。
             * Task类有一个返回Task对象的静态函数Delay，这个Task对象会在指定的时间后完成。
             * 在模拟一个异步操作时，至少要测试“同步成功”“异步成功”和“异步失败”这三种情况，这一点很重要。下面的例子返回一个Task对象，用于“异步成功”测试。
             * 指数退避是一种重试策略，重试的延迟时间会逐次增加。在访问Web服务时，最好的方式就是采用指数退避，它可以防止服务器被太多的重试阻塞。
             * 在实际产品的开发中，建议你采用更周密的方案，例如微软企业库中的瞬间错误处理模块（Transient Error Handling Block）。
             *   下面的代码只是一个使用Task.Delay的简单例子。
             * 最后的例子用Task.Delay实现一个简单的超时功能。本例中代码的目的是：如果服务在3秒内没有响应，就返回null。
             * Task.Delay适合用于对异步代码进行单元测试或者实现重试逻辑。要实现超时功能的话，最好使用CancellationToken。
             */
        }
        // 用于“异步成功”测试。
        static async Task<T> DelayResult<T>(T result, TimeSpan delay)
        {
            await Task.Delay(delay);
            return result;
        }
        // 使用Task.Delay的简单例子。
        static async Task<string> DownloadStringWithRetries(string uri)
        {
            using (var client = new HttpClient())
            {
                //第1次重试前等1秒，第2次等2秒，第3次等4秒。
                var nextDelay = TimeSpan.FromSeconds(1);
                for (int i = 0; i != 3; ++i)
                {
                    try
                    {
                        return await client.GetStringAsync(uri);
                    }
                    catch
                    {
                    }

                    await Task.Delay(nextDelay);
                    nextDelay = nextDelay + nextDelay;
                }

                //最后重试一次，以便让调用者知道出错信息。
                return await client.GetStringAsync(uri);
            }
        }
        // 如果服务在3秒内没有响应，就返回null。
        static async Task<string> DownloadStringWithTimeout(string uri)
        {
            using (var client = new HttpClient())
            {
                var downloadTask = client.GetStringAsync(uri);
                var timeoutTask = Task.Delay(3000);

                var completedTask = await Task.WhenAny(downloadTask, timeoutTask); // 返回第一个完成的Task
                if (completedTask == timeoutTask)
                    return null;
                return await downloadTask;
            }
        }

        // 2.2 返回完成的任务
        private void button2_Click(object sender, EventArgs e)
        {
            /* 如何实现一个具有异步签名的同步方法。如果从异步接口或基类继承代码，但希望用同步的方法来实现它，就会出现这种情况。
             *   对异步代码做单元测试，以及用简单的生成方法存根（stub）或者模拟对象（mock）来产生异步接口，这两种情况下都可使用这种技术。
             * 可以使用Task.FromResult方法创建并返回一个新的Task<T>对象，这个Task对象是已经完成的，并有指定的值。
             * 在用同步代码实现异步接口时，要避免使用任何形式的阻塞操作。在异步方法中进行阻塞操作，然后返回一个完成的Task对象，这种做法并不可取。
             *   作为一个反例，我们来看一下.NET 4.5中Console类的文本读取器。Console.In.ReadLineAsync一定会阻塞调用它的线程，直到它读取完一行文字，
             *   然后会返回一个已完成的Task对象。这种实现方式并不直观，很多开发人员也觉得很奇怪。一旦异步方法阻塞，调用它的线程就无法启动其他任务，
             *   这会干扰程序的并发性，甚至可能产生死锁。
             * Task.FromResult只能提供结果正确的同步Task对象。如果要让返回的Task对象有一个其他类型的结果（例如以NotImplementedException结束的Task对象），
             *   就得自行创建使用TaskCompletionSource的辅助方法：
             * 如果用Task.FromResult反复调用同一参数，则可考虑用一个实际的task变量。例如，可以一次性建立一个结果为0的Task<int>对象，
             *   在以后的调用中就不需要创建额外的实例了，这样可减少垃圾回收的次数：
             */
        }
        // 异步接口
        interface IMyAsyncInterface
        {
            Task<int> GetValueAsync();
        }
        // 实现异步接口
        class MySynchronousImplementation : IMyAsyncInterface
        {
            public Task<int> GetValueAsync()
            {
                return Task.FromResult(13);
            }
        }
        // 自行创建使用TaskCompletionSource的辅助方法
        static Task<T> NotImplementedAsync<T>()
        {
            var tcs = new TaskCompletionSource<T>();
            tcs.SetException(new NotImplementedException());
            return tcs.Task;
        }
        // 可以一次性建立一个结果为0的Task<int>对象
        private static readonly Task<int> zeroTask = Task.FromResult(0);
        static Task<int> GetValueAsync()
        {
            return zeroTask;
        }

        // 2.3 报告进度
        private async void button3_Click(object sender, EventArgs e)
        {
            /* 异步操作执行的过程中，需要展示操作的进度。
             * 使用IProgress<T>和Progress<T>类型。编写的async方法需要有IProgress<T>参数，其中T是需要报告的进度类型：
             * 按照惯例，如果不需要报告进度，IProgress<T>参数可以是null，因此在async方法中一定要对此进行检查。
             * 需要注意的是，IProgress<T>.Report方法可以是异步的。这意味着真正报告进度之前，MyMethodAsync方法会继续运行。基于这个原因，最好把T定义为一个不可变类型，
             *   或者至少是值类型。如果T是一个可变的引用类型，就必须在每次调用IProgress<T>.Report时，创建一个单独的副本。
             * Progress<T>会在创建时捕获当前上下文，并且在这个上下文中调用回调函数。这意味着，如果在UI线程中创建了Progress<T>，
             *   就能在Progress<T>的回调函数中更新UI，即使异步方法是在后台线程中调用Report的。
             * 如果一个方法可以报告进度，就该尽量做到可以被取消。9.4节介绍如何实现异步方法的取消功能。
             */
            //await CallMyMethodAsync();

            progressBar1.Minimum = 0;
            progressBar1.Maximum = 16;
            progressBar1.Step = 1;
            List<Task> can1TaskLst = new List<Task>();
            List<Task> can2TaskLst = new List<Task>();
            can1TaskLst.Add(CAN1DataFunc(0));
            can1TaskLst.Add(CAN1DataFunc(1));
            for (int i = 0; i < can1TaskLst.Count(); i++)
            {
                await can1TaskLst[i];
                can2TaskLst.Add(CAN2DataFunc(i));
            }
            foreach (var can2Task in can2TaskLst)
            {
                await can2Task;
            }
            Console.WriteLine("progress done.");
        }
        static bool done = false;
        async Task MyMethodAsync(IProgress<double> progress = null)
        {
            double percentComplete = 0;
            while (!done)
            {
                await Task.Run(async () =>
                {
                    await Task.Delay(200).ConfigureAwait(false);
                    percentComplete += 1;
                    if (percentComplete == 100)
                        done = true;
                });
                if (progress != null)
                    progress.Report(percentComplete);
            }
        }
        async Task CallMyMethodAsync()
        {
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 100;
            progressBar1.Step = 1;
            var progress = new Progress<double>();
            progress.ProgressChanged += (sender, args) =>
            {
                progressBar1.PerformStep();
                Console.WriteLine(args);
            };
            await MyMethodAsync(progress);
        }

        CancellationTokenSource canTokenSource = new CancellationTokenSource();
        async Task CAN1DataFunc(int index)
        {
            int bk = 0;
            int percentComplete = 0;
            var progress = new Progress<int>();
            progress.ProgressChanged += (sender, args) =>
            {
                progressBar1.PerformStep();
                Console.WriteLine(args);
            };
            while (true)
            {
                if (canTokenSource.IsCancellationRequested)
                    canTokenSource.Token.ThrowIfCancellationRequested();
                switch (bk)
                {
                    case 0:
                        {
                            Random random = new Random();
                            await Task.Delay(random.Next(1000, 3000)).ConfigureAwait(false);
                            Console.WriteLine($"can1 index:{index} bk:{bk}");
                            ((IProgress<int>)progress).Report(percentComplete++);
                            bk = 1;
                        }
                        break;
                    case 1:
                        {
                            Random random = new Random();
                            await Task.Delay(random.Next(1000, 3000)).ConfigureAwait(false);
                            Console.WriteLine($"can1 index:{index} bk:{bk}");
                            ((IProgress<int>)progress).Report(percentComplete++);
                            bk = 2;
                        }
                        break;
                    case 2:
                        {
                            Random random = new Random();
                            await Task.Delay(random.Next(1000, 3000)).ConfigureAwait(false);
                            Console.WriteLine($"can1 index:{index} bk:{bk}");
                            ((IProgress<int>)progress).Report(percentComplete++);
                            bk = 3;
                        }
                        break;
                    case 3:
                        {
                            Random random = new Random();
                            await Task.Delay(random.Next(1000, 3000)).ConfigureAwait(false);
                            Console.WriteLine($"can1 index:{index} bk:{bk}");
                            ((IProgress<int>)progress).Report(percentComplete++);
                            bk = 4;
                        }
                        break;
                    case 4:
                        {
                            return;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        async Task CAN2DataFunc(int index)
        {
            int bk = 0;
            int percentComplete = 0;
            var progress = new Progress<int>();
            progress.ProgressChanged += (sender, args) =>
            {
                progressBar1.PerformStep();
                Console.WriteLine(args);
            };
            while (true)
            {
                if (canTokenSource.IsCancellationRequested)
                    canTokenSource.Token.ThrowIfCancellationRequested();
                switch (bk)
                {
                    case 0:
                        {
                            Random random = new Random();
                            await Task.Delay(random.Next(1000, 3000)).ConfigureAwait(false);
                            Console.WriteLine($"can2 index:{index} bk:{bk}");
                            ((IProgress<int>)progress).Report(percentComplete++);
                            bk = 1;
                        }
                        break;
                    case 1:
                        {
                            Random random = new Random();
                            await Task.Delay(random.Next(1000, 3000)).ConfigureAwait(false);
                            Console.WriteLine($"can2 index:{index} bk:{bk}");
                            ((IProgress<int>)progress).Report(percentComplete++);
                            bk = 2;
                        }
                        break;
                    case 2:
                        {
                            Random random = new Random();
                            await Task.Delay(random.Next(1000, 3000)).ConfigureAwait(false);
                            Console.WriteLine($"can2 index:{index} bk:{bk}");
                            ((IProgress<int>)progress).Report(percentComplete++);
                            bk = 3;
                        }
                        break;
                    case 3:
                        {
                            Random random = new Random();
                            await Task.Delay(random.Next(1000, 3000)).ConfigureAwait(false);
                            Console.WriteLine($"can2 index:{index} bk:{bk}");
                            ((IProgress<int>)progress).Report(percentComplete++);
                            bk = 4;
                        }
                        break;
                    case 4:
                        {
                            return;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        // 2.4 等待一组任务完成
        private async void button4_Click(object sender, EventArgs e)
        {
            /* 执行几个任务，等待它们全部完成。
             * 框架提供的Task.WhenAll方法可以实现这个功能。这个方法的输入为若干个任务，当所有任务都完成时，返回一个完成的Task对象：
             * 如果所有任务的结果类型相同，并且全部成功地完成，则Task.WhenAll返回存有每个任务执行结果的数组：
             * Task.WhenAll方法有以IEnumerable类型作为参数的重载，但建议大家不要使用。只要异步代码与LINQ结合，显式的“具体化”序列（即对序列求值，创建集合）就会使代码更清晰：
             * 如果有一个任务抛出异常，则Task.WhenAll会出错，并把这个异常放在返回的Task中。如果多个任务抛出异常，则这些异常都会放在返回的Task中。
             *   但是，如果这个Task在被await调用，就只会抛出其中的一个异常。如果要得到每个异常，可以检查Task.WhenAll返回的Task的Exception属性：
             * 使用Task.WhenAll时，我一般不会检查所有的异常。通常情况下，只处理第一个错误就足够了，没必要处理全部错误。
             */
            Stopwatch sp = Stopwatch.StartNew();
            sp.Start();
            Task task1 = Task.Delay(TimeSpan.FromSeconds(1));
            Task task2 = Task.Delay(TimeSpan.FromSeconds(2));
            Task task3 = Task.Delay(TimeSpan.FromSeconds(1));
            await Task.WhenAll(task1, task2, task3);
            sp.Stop();
            Console.WriteLine($"{sp.ElapsedMilliseconds} ms");

            Task<int> task11 = Task.FromResult<int>(3);
            Task<int> task22 = Task.FromResult<int>(5);
            Task<int> task33 = Task.FromResult<int>(7);
            int[] results = await Task.WhenAll(task11, task22, task33);
            // "results" 含有 { 3, 5, 7 }
        }
        // Task.WhenAll方法有以IEnumerable类型作为参数的重载
        static async Task<string> DownloadAllAsync(IEnumerable<string> urls)
        {
            var httpClient = new HttpClient();

            // 定义每一个url的使用方法。
            var downloads = urls.Select(url => httpClient.GetStringAsync(url));
            // 注意，到这里，序列还没有求值，所以所有任务都还没真正启动。

            // 下面，所有的URL下载同步开始。
            Task<string>[] downloadTasks = downloads.ToArray();
            // 到这里，所有的任务已经开始执行了。

            // 用异步方式等待所有下载完成。
            string[] htmlPages = await Task.WhenAll(downloadTasks);

            return string.Concat(htmlPages);
        }
        // 要得到每个异常，可以检查Task.WhenAll返回的Task的Exception属性
        static async Task ThrowNotImplementedExceptionAsync()
        {
            throw new NotImplementedException();
        }
        static async Task ThrowInvalidOperationExceptionAsync()
        {
            throw new InvalidOperationException();
        }
        static async Task ObserveOneExceptionAsync()
        {
            var task1 = ThrowNotImplementedExceptionAsync();
            var task2 = ThrowInvalidOperationExceptionAsync();

            try
            {
                await Task.WhenAll(task1, task2);
            }
            catch (Exception ex)
            {
                // ex要么是NotImplementedException，要么是InvalidOperationException
                //...
            }
        }
        static async Task ObserveAllExceptionsAsync()
        {
            var task1 = ThrowNotImplementedExceptionAsync();
            var task2 = ThrowInvalidOperationExceptionAsync();

            Task allTasks = Task.WhenAll(task1, task2);
            try
            {
                await allTasks;
            }
            catch
            {
                AggregateException allExceptions = allTasks.Exception;
                //...
            }
        }

        // 2.5 等待任意一个任务完成
        private void button5_Click(object sender, EventArgs e)
        {
            /* 执行若干个任务，只需要对其中任意一个的完成进行响应。这主要用于：对一个操作进行多种独立的尝试，只要一个尝试完成，任务就算完成。
             *   例如，同时向多个Web服务询问股票价格，但是只关心第一个响应的。
             * 使用Task.WhenAny方法。该方法的参数是一批任务，当其中任意一个任务完成时就会返回。作为返回值的Task对象，就是那个完成的任务。
             *   不要觉得迷惑，这个听起来有点难，但从代码看很容易实现：
             * Task.WhenAny返回的task对象永远不会以“故障”或“已取消”状态作为结束。该方法的运行结果总是一个Task首先完成。
             *   如果这个任务完成时有异常，这个异常也不会传递给Task.WhenAny返回的Task对象。因此，通常需要在Task对象完成后继续使用await。
             * 第一个任务完成后，考虑是否要取消剩下的任务。如果其他任务没有被取消，也没有被继续await，那它们就处于被遗弃的状态。
             *   被遗弃的任务会继续运行直到完成，它们的结果会被忽略，抛出的任何异常也会被忽略。
             * 使用Task.WhenAny可以实现超时功能（例如用Task.Delay作为其中的一个任务），但这种做法并不可取。更常见的做法是采用专门有取消功能的超时函数，
             *   并且取消功能还有一个好处，就是可以把已经超时的任务彻底取消。
             * 
             */
        }
        // 返回第一个响应的URL的数据长度。
        private static async Task<int> FirstRespondingUrlAsync(string urlA, string urlB)
        {
            var httpClient = new HttpClient();

            // 并发地开始两个下载任务。
            Task<byte[]> downloadTaskA = httpClient.GetByteArrayAsync(urlA);
            Task<byte[]> downloadTaskB = httpClient.GetByteArrayAsync(urlB);

            // 等待任意一个任务完成。
            Task<byte[]> completedTask =
              await Task.WhenAny(downloadTaskA, downloadTaskB);

            // 返回从URL得到的数据的长度。
            byte[] data = await completedTask;
            return data.Length;
        }

        // 2.6 任务完成时的处理
        private async void button6_Click(object sender, EventArgs e)
        {
            /* 正在await一批任务，希望在每个任务完成时对它做一些处理。另外，希望在任务一完成就立即进行处理，而不需要等待其他任务。
             *   举个例子，下面的代码启动了3个延时任务，然后对每一个进行await。
             * 虽然列表中的第三个任务是首先完成的，当前这段代码仍按列表的顺序对任务进行await。我们希望按任务完成的次序进行处理（例如Trace.WriteLine），不必等待其他任务。
             * 解决这个问题的方法有好几种。本节首先介绍一种推荐大家使用的方法，另一种则将在“讨论部分”给出。最简单的方案是通过引入更高级的async方法来await任务，
             *   并对结果进行处理，从而重新构建代码。提取出处理过程后，代码就明显简化了。
             * 重构后的代码是解决本问题最清晰、可移植性最好的方法。不过它与原始代码有一个细微的区别。重构后的代码并发地执行处理过程，而原始代码是一个接着一个地处理。
             *   大多数情况下这不会有什么影响，但如果不允许有这种区别，可考虑使用锁（11.2节介绍）或者后面介绍的可选方案。
             * 如果上面重构代码的办法不可取，我们还有可选方案。Stephen Toub和Jon Skeet都开发了一个扩展方法，可以让任务按顺序完成，并返回一个任务数组。
             *   Setphen Toub的解决方案见博客文档“Parallel Programming with .NET”（http://t.cn/RhR2V6n）, 
             *   Jon Skeet的解决方案见他的博客（http://blogs.msmvps.com/jonskeet/2012/01/16/eduasync-part-19-ordering-by-completion-ahead-of-time/）。
             *   [插图]这个扩展方法也可在开源项目AsyncEx库（https://nitoasyncex.codeplex.com）找到，
             *   它在NuGet包Nito.AsyncEx（https://www.nuget.org/packages/Nito.AsyncEx）中。
             * 使用像OrderByCompletion这样的扩展方法，就能让修改原代码的量降到最低。
             */
            await ProcessTasksAsync();
            await ProcessTasksAsync1();
            await ProcessTasksAsync2();
            await UseOrderByCompletionAsync3(); // 使用像OrderByCompletion这样的扩展方法，就能让修改原代码的量降到最低。
        }
        static async Task<int> DelayAndReturnAsync(int val)
        {
            await Task.Delay(TimeSpan.FromSeconds(val));
            return val;
        }
        // 当前，此方法输出“2”,“3”,“1”。
        // 我们希望它输出“1”,“2”,“3”。
        static async Task ProcessTasksAsync()
        {
            // 创建任务队列。
            Task<int> taskA = DelayAndReturnAsync(2);
            Task<int> taskB = DelayAndReturnAsync(3);
            Task<int> taskC = DelayAndReturnAsync(1);
            var tasks = new[] { taskA, taskB, taskC };

            // 按顺序await每个任务。
            foreach (var task in tasks)
            {
                var result = await task;
                Trace.WriteLine(result);
            }
        }
        static async Task<int> DelayAndReturnAsync1(int val)
        {
            await Task.Delay(TimeSpan.FromSeconds(val));
            return val;
        }

        static async Task AwaitAndProcessAsync1(Task<int> task)
        {
            var result = await task;
            Trace.WriteLine(result);
        }

        // 现在，这个方法输出“1”,“2”,“3”。
        static async Task ProcessTasksAsync1()
        {
            // 创建任务队列。
            Task<int> taskA = DelayAndReturnAsync1(2);
            Task<int> taskB = DelayAndReturnAsync1(3);
            Task<int> taskC = DelayAndReturnAsync1(1);
            //var tasks = new [] { taskA, taskB, taskC };
            var tasks = new List<Task<int>> { taskA, taskB, taskC };

            var processingTasks = (from t in tasks
                                   select AwaitAndProcessAsync1(t)).ToArray();

            // 等待全部处理过程的完成。
            await Task.WhenAll(processingTasks);
        }
        static async Task<int> DelayAndReturnAsync2(int val)
        {
            await Task.Delay(TimeSpan.FromSeconds(val));
            return val;
        }
        // 现在，这个方法输出“1”,“2”,“3”。
        static async Task ProcessTasksAsync2()
        {
            // 创建任务队列。
            Task<int> taskA = DelayAndReturnAsync2(2);
            Task<int> taskB = DelayAndReturnAsync2(3);
            Task<int> taskC = DelayAndReturnAsync2(1);
            var tasks = new[] { taskA, taskB, taskC };

            var processingTasks = tasks.Select(async t =>
            {
                var result = await t;
                Trace.WriteLine(result);
            }).ToArray();

            // 等待全部处理过程的完成。
            await Task.WhenAll(processingTasks);
        }
        static async Task<int> DelayAndReturnAsync3(int val)
        {
            await Task.Delay(TimeSpan.FromSeconds(val));
            return val;
        }

        // 现在，这个方法输出“1”,“2”,“3”。
        static async Task UseOrderByCompletionAsync3()
        {
            // 创建任务队列。
            Task<int> taskA = DelayAndReturnAsync3(2);
            Task<int> taskB = DelayAndReturnAsync3(3);
            Task<int> taskC = DelayAndReturnAsync3(1);
            var tasks = new[] { taskA, taskB, taskC };

            // 等待每一个任务完成。
            foreach (var task in tasks.OrderByCompletion())
            {
                var result = await task;
                Trace.WriteLine(result);
            }
        }

        // 2.7 避免上下文延续
        private async void button7_Click(object sender, EventArgs e)
        {
            /* 在默认情况下，一个async方法在被await调用后恢复运行时，会在原来的上下文中运行。如果是UI上下文，并且有大量的async方法在UI上下文中恢复，就会引起性能上的问题。
             * 为了避免在上下文中恢复运行，可让await调用ConfigureAwait方法的返回值，参数continueOnCapturedContext设为false：
             * 如果在UI线程上运行的延续任务（continuation）太多，会导致性能上的问题。因为使系统变慢的方法不止一个，所以这种类型的性能问题是很难诊断的。
             *   而且随着程序复杂性的增加，UI性能会因为“成千上万的剪纸”（在UI线程中有太多任务切换，就像剪纸）而变慢。
             * 真正的问题是，在UI线程中有多少延续任务，才算是太多？这没有固定的答案，不过微软公司的Lucian Wischik公布了一个WinRT团队的指导标准（http://t.cn/RhR2KGi）：
             *   每秒100个左右尚可，但每秒1000个左右就太多了。
             * 最好在一开始就避免这个问题。对于每一个async方法，如果它没有必要恢复到原来的上下文，就要使用ConfigureAwait。这么做没有什么坏处。
             * 还有一个好点子，就是在编写async代码时特别注意上下文。通常一个async方法要么需要上下文（处理UI元素或ASP.NET请求/响应），要么需要摆脱上下文（执行后台指令）。
             *   如果一个asnync方法的一部分需要上下文、一部分不需要上下文，则可考虑把它拆分为两个（或更多）async方法。这种做法有利于更好地将代码组织成不同层次。
             */
            await ResumeOnContextAsync();
            Console.WriteLine("hello ResumeOnContextAsync");
            await ResumeWithoutContextAsync();
            Console.WriteLine("hello ResumeWithoutContextAsync");
        }
        async Task ResumeOnContextAsync()
        {
            await Task.Delay(TimeSpan.FromSeconds(5));
            Console.WriteLine("ResumeOnContextAsync");
            // 这个方法在同一个上下文中恢复运行。
        }
        async Task ResumeWithoutContextAsync()
        {
            await Task.Delay(TimeSpan.FromSeconds(5)).ConfigureAwait(false);
            Console.WriteLine("ResumeWithoutContextAsync");
            // 这个方法在恢复运行时，会丢弃上下文。
        }

        // 2.8 处理async Task方法的异常
        private async void button8_Click(object sender, EventArgs e)
        {
            /* 对任何设计来说，异常处理都是一个关键的部分。只考虑成功情况的设计是很简单的，但是正确的设计必须要能处理异常。还好，处理async Task方法的异常是很简单、很直观的。
             * 可以用简单的try/catch来捕获异常，和同步代码使用的方法一样：
             * 在async Task方法中引发的异常，存放在返回的Task对象中。只有当Task对象被await调用时，才会引发异常：
             * 从async Task方法中抛出的异常会被捕获并放在返回的Task对象中。因为async void方法没有返回Task对象，无法存放异常，所以做法就会不同。我们将在另一节介绍这方面的内容。
             * 有些情况下（例如Task.WhenAll），一个Task对象包含多个异常，而await只会重新抛出第一个。2.4节有一个处理所有异常的例子。
             */
            await TestAsync();
            await TestAsync1();
        }
        static async Task ThrowExceptionAsync()
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            throw new InvalidOperationException("Test");
        }
        static async Task TestAsync()
        {
            try
            {
                await ThrowExceptionAsync();
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static async Task ThrowExceptionAsync1()
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            throw new InvalidOperationException("Test1");
        }

        static async Task TestAsync1()
        {
            // 抛出异常并将其存储在Task中。
            Task task = ThrowExceptionAsync1();
            try
            {
                // Task对象被await调用，异常在这里再次被引发。
                await task;
            }
            catch (InvalidOperationException ex)
            {
                // 这里，异常被正确地捕获。
                Console.WriteLine(ex.Message);
            }
        }

        // 2.9 处理async void方法的异常
        private void button9_Click(object sender, EventArgs e)
        {
            /* 需要处理从async void方法传递出来的异常。
             * 没有什么好的办法。如果可能的话，方法的返回类型不要用void，把它改为Task。某些情况下这是不可能的，例如，需要对一个ICommand的实现（必须返回void）做单元测试。
             *   这种情况下，可以为Execute方法提供一个返回Task类型的重载，就像这样：
             * 最好不要从async void方法传递出异常。如果必须使用async void方法，可考虑把所有代码放在try块中，直接处理异常。
             * 处理async void方法的异常还有一个办法。一个异常从async void方法中传递出来时，会在SynchronizationContext中引发出来。
             *   当async void方法启动时，SynchronizationContext就处于激活状态。如果系统运行环境支持SynchronizationContext，通常就可以在全局范围内处理这些顶层的异常。
             *   例如，WPF有Application.DispatcherUnhandledException, WinRT有Application.UnhandledException, ASP.NET有Application Error。
             * 通过控制SynchronizationContext，也可以处理从async void方法传出的异常。自己编写SynchronizationContext的工作量太大，可以使用免费的NuGet库Nito.AsyncEx，
             *   里面有AsyncContext类。AsyncContext可以在没有自带SynchronizationContext的场合发挥作用，例如控制台程序、Win32服务程序。
             * 下面的例子是在控制台程序中使用AsyncContext，其中async方法不返回Task，但AsyncContext仍能在async void方法中起作用：
             * 推荐使用async Task而不是async void，原因之一就是返回Task的方法更容易测试。至少要用Task方法重载void方法，那样可以提供便于测试的API外壳。
             */
            try
            {
                AsyncContext.Run(() => MainAsync());
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
        }
        static async Task<int> MainAsync()
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            throw new InvalidOperationException("MainAsync");
        }

    }
}
