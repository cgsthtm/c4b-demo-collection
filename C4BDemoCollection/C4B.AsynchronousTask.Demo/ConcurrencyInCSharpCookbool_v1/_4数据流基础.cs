﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows;
using System.Windows.Forms;

namespace C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1
{
    /* TPL数据流（dataflow）库的功能很强大，可用来创建网格（mesh）和管道（pipleline），并通过它们以异步方式发送数据。数据流的代码具有很强的“声明式编程”风格。
     *   通常要先完整地定义网格，然后才能开始处理数据，最终让网格成为一个让数据流通的体系架构。这种编程风格会让你觉得有些不适应，但一旦迈过这一步，你会发觉数据流适用于许多场合。
     * 每个网格由各种互相链接的数据流块（block）构成。独立的块比较简单，只负责数据处理中某个单独的步骤。当块处理完它的数据后，就会把数据传递给与它链接的块。
     * 使用TPL数据流之前，需要在程序中安装一个NuGet包：Microsoft.Tpl.Dataflow。各种平台对TPL数据流库的支持情况见表4-1。
     */
    public partial class _4数据流基础 : Form
    {
        public _4数据流基础()
        {
            InitializeComponent();
        }

        // 4.1 链接数据流块
        private async void button1_Click(object sender, EventArgs e)
        {
            /* 创建网格时，需要把数据流块互相链接起来。
             * TPL数据流库提供的块只有一些基本的成员，很多实用的方法实际上是扩展方法。这里我们来看LinkTo方法：
             * 默认情况下，链接的数据流块只传递数据，不传递完成情况（或出错信息）。如果数据流是线性的（例如管道），一般需要传递完成情况。
             *   要实现完成情况（和出错信息）的传递，可以在链接中设置PropagateCompletion属性：
             * 一旦建立了链接，数据就会自动从源块传递到目标块。如果设置了PropagateCompletion属性，情况完成的同时也会传递数据。
             *   在管道的每个节点上，当出错的块把错误信息传递给下一块时，它会把错误信息封装进AggregateException对象。因此，如果传递完成情况的管道很长，
             *   错误信息就会被嵌套在很多个AggregateException实例中。在这种情形下，AggregateException有几个成员（例如Flatten）就可以进行错误处理了。
             * 链接数据流块的方式有很多种，可以在网格中包含分叉、连接、甚至循环。不过在大多数情况下，线性的管道就足够管用了。
             *   本书主要介绍管道（此外简单地介绍一下分叉），更多的高级内容则超出了本书范围。
             * 利用DataflowLinkOptions类，可以对链接设置多个不同的参数（例如前面用到的PropagateCompletion参数）。另外，可以在重载的LinkTo方法中设置断言，
             *   形成一个数据通行的过滤器。数据被过滤器拦截时也不会被删除。通过过滤器的数据会继续下一步流程，被过滤器拦截的数据也会尝试从其他链接通过，
             *   如果所有链接都无法通过，则会留在原来的块中。
             */
            var multiplyBlock = new TransformBlock<int, int>(item => { Console.WriteLine($"multiplyBlock:{item * 2}"); return item * 2; });
            var subtractBlock = new TransformBlock<int, int>(item => { Console.WriteLine($"subtractBlock:{item - 2}"); return item - 2; });
            // 建立链接后，从multiplyBlock出来的数据将进入subtractBlock。
            //multiplyBlock.LinkTo(subtractBlock);

            var options = new DataflowLinkOptions { PropagateCompletion = true };
            multiplyBlock.LinkTo(subtractBlock, options);
            //multiplyBlock.Post(1);
            // 第一个块的完成情况自动传递给第二个块。
            multiplyBlock.Complete();
            await subtractBlock.Completion; // 为啥取消multiplyBlock.Post(1);的注释后，会一直异步等待不结束？
            //subtractBlock.Completion.Wait(); 同步方式等待，会阻塞
            Console.WriteLine($"数据流块执行完毕...");
        }

        // 4.2 传递出错信息
        private async void button2_Click(object sender, EventArgs e)
        {
            /* 需要处理数据流网格中发生的错误。
             * 如果数据流块内的委托抛出异常，这个块就进入故障状态。一旦数据流块进入故障状态，就会删除所有的数据（并停止接收新数据）。该数据流块将不会生成任何新数据。
             *   下面的代码中，第一个值引发了一个错误，第二个值被直接删除：
             * 用await调用它的Completion属性，即可捕获数据流块的错误。Completion属性返回一个任务，一旦数据流块执行完成，这个任务也完成。如果数据流块出错，这个任务也出错：
             * 如果用PropagateCompletion这个参数传递完成情况，错误信息也会被传递。只不过这个异常是被封装在AggregateException类中传递给下一个块。
             *   下面的例子中，程序在管道的末尾捕获到了异常。这说明，如果异常是从前面的块传来的，程序就会捕获到AggregateException：
             * 数据流块收到传过来的出错信息后，即使它已经被封装在AggregateException，仍会用AggregateException进行封装。如果在管道的前面部分发生错误，
             *   经过了多个链接后才被发现，这个原始错误就会被AggregateException封装很多层。这时用AggregateException.Flatten方法可以简化错误处理过程。
             * 思考一下这个问题，在构建了网格（或管道）后怎么处理错误。对于最简单的情况，最好是把错误传递下去，等到最后再作一次性处理。
             *   对于更复杂的网格，在数据流完成后需要检查每一个数据流块。
             */
            var block = new TransformBlock<int, int>(item =>
            {
                if (item == 1)
                    throw new InvalidOperationException("Blech1.");
                return item * 2;
            });
            block.Post(1); // 出错，删除当前块的数据（无法接收当前块的数据，因为被删除了）并停止接收新数据
            block.Post(2); // 不执行，因为上面的语句执行时，数据流块进入故障状态了

            try
            {
                var block1 = new TransformBlock<int, int>(item =>
                {
                    if (item == 1)
                        throw new InvalidOperationException("Blech2.");
                    return item * 2;
                });
                block1.Post(1);
                await block1.Completion;
            }
            catch (InvalidOperationException ex)
            {
                // 这里捕获异常。
                Console.WriteLine(ex.Message);
            }

            try
            {
                var multiplyBlock = new TransformBlock<int, int>(item =>
                {
                    if (item == 1)
                        throw new InvalidOperationException("Blech3.");
                    return item * 2;
                });
                var subtractBlock = new TransformBlock<int, int>(item => item - 2);
                multiplyBlock.LinkTo(subtractBlock,
                  new DataflowLinkOptions { PropagateCompletion = true });
                multiplyBlock.Post(1);
                await subtractBlock.Completion;
            }
            catch (AggregateException ex)
            {
                // 这里捕获异常。
                Console.WriteLine(ex.Message);
            }
        }

        // 4.3 断开链接
        private void button3_Click(object sender, EventArgs e)
        {
            /* 要在处理的过程中修改数据流结构。这是一种高级应用，很少会用到。
             * 可以随时对数据流块建立链接或断开链接。数据在网格中的自由传递，不会受此影响。建立或断开链接时，线程都是完全安全的。
             * 在创建数据流块之间的链接时，保留LinkTo方法返回的IDisposable接口。想断开它们的链接时，只需释放该接口：
             * 除非能保证链接是空闲的，否则在断开数据流块的链接时就会出现竞态条件（race condition）。但是，通常不需要担心这类竞态条件。
             *   数据要么在链接断开之前就已经传递到下一块，要么就永远不会传递。这些竞态条件不会重复出现数据，也不会丢失数据。
             * 断开链接是一个高级应用，但它仍能用于一些场合。举个例子，在链接建立后是无法修改过滤器的，要修改一个已有链接的过滤器，必须先断开旧链接，
             *   然后用新的过滤器建立新链接（可以把DataflowLinkOptions.Append设为false）。另外，要暂停数据流网格运行的话，可断开一个关键链接。
             */
            var multiplyBlock = new TransformBlock<int, int>(item => { Console.WriteLine($"multiplyBlock:{item * 2}"); return item * 2; });
            var subtractBlock = new TransformBlock<int, int>(item => { Console.WriteLine($"subtractBlock:{item - 2}"); return item - 2; });
            IDisposable link = multiplyBlock.LinkTo(subtractBlock);
            multiplyBlock.Post(1);
            multiplyBlock.Post(2);
            // 断开数据流块的链接。
            // 前面的代码中，数据可能已经通过链接传递过去，也可能还没有。
            // 在实际应用中，考虑使用代码块，而不是调用Dispose。
            link.Dispose();
        }

        // 4.4 限制流量
        private void button4_Click(object sender, EventArgs e)
        {
            /* 需要在数据流网格中进行分叉，并且希望数据流量能在各分支之间平衡。
             * 默认情况下，数据流块生成输出的数据后，会检查每个链接（按照创建的次序），逐个地尝试通过链接传递数据。同样，默认情况下，每个数据流块会维护一个输入缓冲区，
             *   在处理数据之前接收任意数量的数据。
             * 有分叉时，一个源块链接了两个目标块，上述做法就会产生问题：第一个目标块会不停地缓冲数据，第二个目标块就永远没有机会得到数据。
             *   这个问题的解决办法是使用数据流块的BoundedCapacity属性，来限制目标块的流量（throttling）。BoundedCapacity的默认设置是DataflowBlockOptions.Unbounded，
             *   这会导致第一个目标块在还来不及处理数据时就得对所有数据进行缓冲了。BoundedCapacity可以是大于0的任何数值（当然也可以是DataflowBlockOptions.Unbounded）。
             *   只要目标块来得及处理来自源块的数据，将这个参数设为1就足够了：
             * 限流可用于分叉的负载平衡，但也可用在任何限流行为中。例如，在用I/O操作的数据填充数据流网格时，可以设置数据流块的BoundedCapacity属性。
             *   这样，在网格来不及处理数据时，就不会读取过多的I/O数据，网格也不会缓存所有数据。
             */
            var sourceBlock = new BufferBlock<int>();
            var options = new DataflowBlockOptions { BoundedCapacity = 1 };
            var targetBlockA = new BufferBlock<int>(options);
            var targetBlockB = new BufferBlock<int>(options);
            sourceBlock.LinkTo(targetBlockA);
            sourceBlock.LinkTo(targetBlockB);
        }

        // 4.5 数据流块的并行处理
        private void button5_Click(object sender, EventArgs e)
        {
            /* 想对数据流网格进行并行处理。
             * 默认情况下每个数据流块是互相独立的。将两个数据流块链接起来后，它们也是独立运行的。因此每个数据流网格本身就有并行特性。如果想更进一步，
             *   例如某个特定的数据流块的计算量特别大，那就可以设置MaxDegreeOfParallelism参数，使数据流块在处理输入的数据时采用并行的方式。
             *   MaxDegreeOfParallelism的默认值是1，因此每个数据流块同时只能处理一块数据。MaxDegreeOfParallelism可以设为DataflowBlockOptions.Unbounded或任何大于0的值。
             *   下面的例子允许任意数量的任务，来同时对数据进行倍增：
             * 利用MaxDegreeOfParallelism参数，就可以很容易地在数据流块中实现并行处理。而真正的难点，在于找出哪些数据流块需要并行处理，有一个办法是在调试时暂停数据流的运行，
             *   在调试器中查看等待的数据项的数量（就是还没有被数据流块处理的数据项）。如果等待的数据项很多，就表明需要进行重构或并行化处理。
             * 在数据流块进行异步处理时，MaxDegreeOfParallelism参数也会发挥作用。这时，MaxDegreeOfParallelism参数代表的是并发的层次，即一定数量的槽（slot）。
             *   在数据流块开始处理数据项之际，每个数据项就会占用一个槽。只有当整个异步处理过程完成后，才会释放槽。
             */
            var multiplyBlock = new TransformBlock<int, int>(
                item => item * 2,
                new ExecutionDataflowBlockOptions
                {
                    MaxDegreeOfParallelism = DataflowBlockOptions.Unbounded
                }
            );
            var subtractBlock = new TransformBlock<int, int>(item => item - 2);
            multiplyBlock.LinkTo(subtractBlock);
        }

        // 4.6 创建自定义数据流块
        private void button6_Click(object sender, EventArgs e)
        {
            /* 希望一些可重用的程序逻辑在自定义数据流块中使用。这有助于创建更大的、包含复杂逻辑的数据流块。
             * 通过使用Encapsulate方法，可以取出数据流网格中任何具有单一输入块和输出块的部分。Encapsulate方法会利用这两个端点，创建一个单独的数据流块。
             *   开发者得自己负责端点之间数据的传递以及完成情况。下面的代码利用两个数据流块创建了一个自定义数据流块，并实现了数据和完成情况的传递：
             * 在把一个网格封装成一个自定义数据流块时，得考虑一下对外提供什么类型的参数，考虑每个块参数应该怎样传递进内部的网格（或不传递）。在很多情况下，
             *   有些块参数是不适合的，或者是没有意义的。基于这个原因，创建自定义数据流块时，通常得自行定义参数，而不是沿用DataflowBlockOptions参数。
             * DataflowBlock.Encapsulate只会封装只有一个输入块和一个输出块的网格。如果一个可重用的网格带有多个输入或输出，就应该把它封装进一个自定义对象，
             *   并以属性的形式对外暴露出这些输入和输出，输入的属性类型是ITargetBlock<T>，输出的属性类型是IReceivableSourceBlock<T>。
             * 前面的例子都采用Encapsulate来创建自定义数据流块。开发者也可以自行实现数据流的接口，但技术难度很大。创建自定义数据流块的高级技术，可参阅微软公司发布的有关文章。
             */
        }
        IPropagatorBlock<int, int> CreateMyCustomBlock()
        {
            var multiplyBlock = new TransformBlock<int, int>(item => item * 2);
            var addBlock = new TransformBlock<int, int>(item => item + 2);
            var divideBlock = new TransformBlock<int, int>(item => item / 2);

            var flowCompletion = new DataflowLinkOptions { PropagateCompletion = true };
            multiplyBlock.LinkTo(addBlock, flowCompletion);
            addBlock.LinkTo(divideBlock, flowCompletion);

            return DataflowBlock.Encapsulate(multiplyBlock, divideBlock);
        }
    }
}
