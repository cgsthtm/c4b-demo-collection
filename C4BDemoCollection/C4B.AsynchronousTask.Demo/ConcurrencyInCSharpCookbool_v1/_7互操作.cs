﻿using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;
using static C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1._7互操作;

namespace C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1
{
    /* 有一种老式的异步编程模式，用的是OperationAsync方法和OperationCompleted事件。我们希望实现类似的操作，并且用await来等待返回的结果。
     * 异步、并行、响应式——每种技术都有自己的用武之地，但是结合起来使用会怎样呢？本章我们来看一下各种互操作的场景，学习如何把这些不同的技术结合起来。
     *   我们将了解到这几种技术是互为补充而不是互相排斥的。当它们结合在一起时，在边界上几乎没有什么冲突。
     */
    public partial class _7互操作 : Form
    {
        public _7互操作()
        {
            InitializeComponent();
        }

        // 7.1 用async代码封装Async方法与Completed事件
        private async void button1_Click(object sender, EventArgs e)
        {
            /* 使用OperationAsync和OperationCompleted的模式称为基于事件的异步模式（EAP）。我们要把它们封装成返回Task对象的方法，并且让它符合基于任务的异步模式（TAP）。
             * 可以使用TaskCompletionSource<TResult>类创建异步操作的容器。这个类控制一个Task<TResult>对象，并且可以在适当的时机完成该任务。
             * 下面的例子定义了一个WebClient下载文本的扩展方法。WebClient类定义了Download StringAsync和DownloadStringCompleted，利用这些方法，
             *   就可这样定义DownloadStringTaskAsync方法：
             * 因为有了TryCompleteFromEventArgs扩展方法，若早已经在使用NuGet库Nito.AsyncEx了，那么实现这种容器就会更简单：
             * WebClient已经定义了DownloadStringTaskAsync，并且还可以使用更加适合async的HttpClient类，因此这个实例并没有太大的实用价值。
             *   然而，对于那些还没有升级到使用Task类的异步代码，可以用这种技术交互。
             * 在新编写代码时都要使用HttpClient。只有在维护以前遗留的代码时才用WebClient。
             * 下载文本的TAP方法一般应该命名为OperationAsync（例如DownloadStringAsync），但本例的情况无法接受这样的命名习惯，因为EAP中已经用了这个名称。这时，
             *   习惯上把TAP方法命名为OperationTaskAsync（例如DwonloadStringTaskAsync）。
             */
        }
        //public static Task<string> DownloadStringTaskAsync(this WebClient client, Uri address)
        //{
        //    var tcs = new TaskCompletionSource<string>();

        //    // 这个事件处理程序会完成Task对象，并自行注销。
        //    DownloadStringCompletedEventHandler handler = null;
        //    handler = (s, e) =>
        //    {
        //        client.DownloadStringCompleted -= handler;
        //        if (e.Cancelled)
        //            tcs.TrySetCanceled();
        //        else if (e.Error != null)
        //            tcs.TrySetException(e.Error);
        //        else
        //            tcs.TrySetResult(e.Result);
        //    };

        //    // 登记事件，然后开始操作。
        //    client.DownloadStringCompleted += handler;
        //    client.DownloadStringAsync(address);

        //    return tcs.Task;
        //}
        //public static Task<string> DownloadStringTaskAsync1(this WebClient client, Uri address)
        //{
        //    var tcs = new TaskCompletionSource<string>();

        //    // 这个事件处理程序会完成Task对象，并自行注销。
        //    DownloadStringCompletedEventHandler handler = null;
        //    handler = (s, e) =>
        //    {
        //        client.DownloadStringCompleted -= handler;
        //        tcs.TryCompleteFromCompletedTask(Task.Run(() => e.Result));
        //    };

        //    // 登记事件，然后开始操作。
        //    client.DownloadStringCompleted += handler;
        //    client.DownloadStringAsync(address);

        //    return tcs.Task;
        //}

        // 7.2 用async代码封装Begin/End方法
        private void button2_Click(object sender, EventArgs e)
        {
            /* 有一种老式的异步编程模式，它使用一对名为BeginOperation和EndOperation的方法来和表示这个异步操作的IAsyncResult接口。我们希望能用await来调用这种模式的操作。
             * 使用BeginOperation和EndOperation的模式称为异步编程模型（APM）。我们要把它们封装成返回Task对象的方法，符合基于任务的异步模式（TAP）。
             * 封装APM最好的办法是使用TaskFactory类型的一个FromAsync方法。FromAsync在内部使用TaskCompletionSource<TResult>，但在封装APM时，FromAsync用起来更方便。
             * 下面的例子定义了一个WebRequest的扩展方法，发送一个HTTP请求并获取响应。Web Request类定义了BeginGetResponse和EndGetResponse。
             *   我们可以这样定义GetResponseAsync方法：
             * FromAsync的重载个数多得一塌糊涂！通常来讲，最好用例子中的方式调用FromAsync。首先传入BeginOperation方法（不调用）和EndOperation方法（不调用）。
             *   接着传入BeginOperation所需的全部参数（后面的AsyncCallback和object参数除外）。最后传入null。
             * 也许你会感到奇怪，怎么推荐的这个模式总是在最后传入null。FromAsync是在.NET 4.0版本中和Task类一起被引入的，当时还没有关键字async。
             *   当时在异步回调函数中普遍使用state对象，Task类通过AsyncState成员来支持这种调用方式。新的async模式就再也不需要state对象了。
             */
        }
        //public static Task<WebResponse> GetResponseAsync(this WebRequest client)
        //{
        //    return Task<WebResponse>.Factory.FromAsync(client.BeginGetResponse,
        //      client.EndGetResponse, null);
        //}

        // 7.3 用async代码封装所有异步操作
        private void button3_Click(object sender, EventArgs e)
        {
            /* 有一个不常见或不标准的异步操作或事件，我们希望能用await来调用。
             * 任何情况下都可以用TaskCompletionSource<T>类来构造Task<T>对象。使用Task CompletionSource<T>时，Task对象的完成可以有三种不同的方式：
             *   成功得到结果、出错、被取消。
             * 在async出现前，微软推荐另外两种异步编程模式：APM（见7.2节）和EAP（见7.1节）。但APM和EAP都相当繁琐，也经常难以得到正确结果。
             *   因此产生了一种非官方的通行做法，即使用回调函数，就像下面的方法：
             * 此类方法遵循这样的通行流程：DwonloadString启动（异步地）下载，下载完成时，包含返回信息或异常信息的callback被触发。通常callback是在后台线程中被触发的。
             *   这个非标准类型的异步编程方法，也能用TaskCompletionSource<T>进行封装，能让await进行正常调用：
             * 这种模式结合TaskCompletionSource<T>，可以封装任何异步方法，不管它有多么不标准。首先创建TaskCompletionSource<T>实例。接着准备一个回调函数，
             *   以便TaskCompletion Source<T>能顺利完成它的Task对象。然后开始真正的异步操作。最后返回附属于TaskCompletionSource<T>的Task<T>。
             * 关于这种模式有一点十分重要，就是必须确保TaskCompletionSource<T>总是处于完成状态。尤其要仔细地检查一下错误处理过程，
             *   并且确保TaskCompletionSource<T>会正常完成。在最后一个例子中，异常被显式传递进回调函数，因此程序中不需要有catch块。
             *   但是一些非标准的模式会要求在回调函数中捕获异常，并把异常信息放在TaskCompletionSource<T>中。
             */
        }
        public interface IMyAsyncHttpService
        {
            void DownloadString(Uri address, Action<string, Exception> callback);
        }
        //public static Task<string> DownloadStringAsync(this IMyAsyncHttpService httpService, Uri address)
        //{
        //    var tcs = new TaskCompletionSource<string>();
        //    httpService.DownloadString(address, (result, exception) =>
        //    {
        //        if (exception != null)
        //            tcs.TrySetException(exception);
        //        else
        //            tcs.TrySetResult(result);
        //    });
        //    return tcs.Task;
        //}

        // 7.4 用async代码封装并行代码
        private void button4_Click(object sender, EventArgs e)
        {
            /* 希望用await调用计算密集型的处理过程。采用这种做法后，在等待并行处理完成时能避免UI线程阻塞。
             * Parallel类和并行LINQ利用线程池做并行处理。它们也会把调用线程作为并行处理的线程之一，因此从UI线程调用并行方法时，UI会在并行处理结束前一直保持停止响应状态。
             *   要让UI保持响应的话，就可将并行处理过程封装进Task.Run，并使用await：
             * 这个方法的关键，是并行代码把调用线程也看做是用于并行处理的线程池的一部分。并行LINQ和Parallel类都是这样来处理的。
             * 这个方法很简单，却经常被忽视。通过使用Task.Run，所有的并行处理过程都推给了线程池。Task.Run返回一个代表并行任务的Task对象，UI线程可以（异步地）等待它完成。
             *   这个方法只能用于UI代码。在服务器端（例如ASP.NET）很少用并行处理。如果一定要在服务器端使用并行处理过程，那也应该直接调用它，而不能把它推给线程池。
             */
            //await Task.Run(() => Parallel.ForEach(...));
        }

        // 7.5 用async代码封装Rx Observable对象
        private void button5_Click(object sender, EventArgs e)
        {
            /* 希望用await来处理一个可观察流。
             * 首先需要确定事件流中的哪一个事件是需要关注的。通常有几种情况：· 事件流结束前的最后一个事件；· 下一个事件；· 所有事件。
             * 要捕获事件流的最后一个事件，可用await调用LastAsync方法的结果，或者直接对Observable对象进行await：
             * 在await调用Observable对象或LastAsync时，代码（异步地）等待事件流完成，然后返回最后一个元素。在内部，await实际是在订阅事件流。
             * 使用FirstAsync可捕获事件流中的下一个事件。本例中await订阅事件流，然后在第一个事件到达后立即结束（并退订）：
             * 使用ToList可捕获事件流中的所有事件：
             * Rx库提供了await处理事件流所需的全部工具。唯一的难点是我们必须考虑这些方法是否会一直等待，直到事件流结束。本节的例子中，
             *   LastAsync、ToList和直接使用await会等待事件流结束，FirstAsync只会等待下一个事件到达。
             * 如果这些例子不能满足需求，还可以考虑使用完整的LINQ功能和新版Rx控制器。如果只要异步地等待某些元素而不是整个事件流完成，可以使用Task和Buffer等操作符。
             * 某些和await一起使用的操作符（如FirstAsync和LastAsync）并不会真正地返回Task<T>对象。如果要使用Task.WhenAll或Task.WhenAny，就需要有实际的Task<T>对象。
             *   可在Observable对象上调用ToTask，以得到这个Task<T>对象，该对象代表着事件流结束时的最后一个值。
             */
            //IObservable<int> observable = ...;
            //int lastElement = await observable.LastAsync();
            // 或者int lastElement = await observable;

            //IObservable<int> observable = ...;
            //int nextElement = await observable.FirstAsync();

            //IObservable<int> observable = ...;
            //IList<int> allElements = await observable.ToList();
        }

        // 7.6 用Rx Observable对象封装async代码
        private void button6_Click(object sender, EventArgs e)
        {
            /* 需要把一个异步操作与一个observable对象结合。
             * 任何异步操作都可看作一个满足以下条件之一的可观察流：· 生成一个元素后就完成；· 发生错误，不生成任何元素。
             * Rx库中有一个将Task<T>转换成IObservable<T>的简单方法。下面的代码启动一个异步的网页下载过程，并把它作为一个observable序列：
             * 使用ToObservable前必须调用async方法并转换成Task对象。
             * 另一个办法是调用StartAsync。StartAsync也会立即调用async方法，但它支持取消功能：如果订阅已被处理，这个async方法就会被取消：
             * ToObservable和StartAsync都会立即启动异步操作，而不会等待订阅。如果要让observable对象在接受订阅后才启动操作，
             *   可使用FromAsync（跟StartAsync一样，它也支持取消功能）：
             * FromAsync和ToObservable、StartAsync有着显著的区别。ToObservable和StartAsync都返回一个observable对象，表示一个已经启动的异步操作。
             *   FromAsync在每次被订阅时都会启动一个全新独立的异步操作。
             * 最后，如果要在源事件流中每到达一个事件就启动一个异步操作，就可使用SelectMany的特殊重载。SelectMany也支持取消功能。
             *   下面的例子使用一个已有的URL事件流，在每个URL到达时发出一个请求：
             * 响应式扩展在async引进之前就存在了，但后来增加了上述（和其他）操作符，以便与async代码互通。即使能够用其他Rx操作符实现同样的功能，
             *   我还是建议大家使用上面提到的操作符。
             */
            var client = new HttpClient();
            IObservable<HttpResponseMessage> response =
                client.GetAsync("http://www.example.com/")
                .ToObservable();

            var client1 = new HttpClient();
            IObservable<HttpResponseMessage> response1 = Observable
                .StartAsync(token => client.GetAsync("http://www.example.com/", token));

            var client2 = new HttpClient();
            IObservable<HttpResponseMessage> response2 = Observable
                .FromAsync(token => client.GetAsync("http://www.example.com/", token));

            //IObservable<string> urls = ...
            //var client3 = new HttpClient();
            //IObservable<HttpResponseMessage> responses3 = urls
            //    .SelectMany((url, token) => client.GetAsync(url, token));
        }

        // 7.7 Rx Observable对象和数据流网格
        private void button7_Click(object sender, EventArgs e)
        {
            /* Rx Observable对象和数据流网格有各自的用途，也存在一些概念上的重叠。本节说明它们能互相配合得很好，因此可以在项目中的不同部分选用最合适的工具。
             * 首先，我们考虑把数据流块用作可观察流的输入。下面的代码创建一个缓冲块（它不处理数据），然后调用AsObservable来创建一个缓冲块到Observable对象的接口：
             * 缓冲数据流块和可观察流都会正常完成或者出错，AsObservable方法会把数据流块的完成信息（或出错信息）转化为可观察流的完成信息。如果数据流块出错并抛出异常，
             *   这个异常信息在传递给可观察流时，会被封装在AggregateException对象中。这种方式与互相连接的数据流块之间传递错误的方式有些相似。
             * 如果使用一个网格并把它作为可观察流的目的，情况只会稍微复杂一点。下面的代码调用AsObserver让一个块订阅一个可观察流：
             * 数据流块和可观察流的很多基础概念是一样的。它们都能传递数据，都能处理完成信息和错误信息。它们是为不同的场景设计的。TPL数据流针对异步和并行混合编程，
             *   而Rx针对响应式编程。但概念上的重叠部分具有足够的兼容性，两者能配合得很好、很自然。
             */
            var buffer = new BufferBlock<int>();
            IObservable<int> integers = buffer.AsObservable();
            integers.Subscribe(data => Trace.WriteLine(data),
                ex => Trace.WriteLine(ex),
                () => Trace.WriteLine("Done"));
            buffer.Post(13);

            IObservable<DateTimeOffset> ticks =
                Observable.Interval(TimeSpan.FromSeconds(1))
                .Timestamp()
                .Select(x => x.Timestamp)
                .Take(5);
            var display = new ActionBlock<DateTimeOffset>(x => Trace.WriteLine(x));
            ticks.Subscribe(display.AsObserver());
            try
            {
                display.Completion.Wait();
                Trace.WriteLine("Done.");
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }
    }

    public static class AClass
    {
        public static Task<string> DownloadStringTaskAsync(this WebClient client, Uri address)
        {
            var tcs = new TaskCompletionSource<string>();

            // 这个事件处理程序会完成Task对象，并自行注销。
            DownloadStringCompletedEventHandler handler = null;
            handler = (s, e) =>
            {
                client.DownloadStringCompleted -= handler;
                if (e.Cancelled)
                    tcs.TrySetCanceled();
                else if (e.Error != null)
                    tcs.TrySetException(e.Error);
                else
                    tcs.TrySetResult(e.Result);
            };

            // 登记事件，然后开始操作。
            client.DownloadStringCompleted += handler;
            client.DownloadStringAsync(address);

            return tcs.Task;
        }
        public static Task<string> DownloadStringTaskAsync1(this WebClient client, Uri address)
        {
            var tcs = new TaskCompletionSource<string>();

            // 这个事件处理程序会完成Task对象，并自行注销。
            DownloadStringCompletedEventHandler handler = null;
            handler = (s, e) =>
            {
                client.DownloadStringCompleted -= handler;
                tcs.TryCompleteFromCompletedTask(Task.Run(() => e.Result));
            };

            // 登记事件，然后开始操作。
            client.DownloadStringCompleted += handler;
            client.DownloadStringAsync(address);

            return tcs.Task;
        }

        public static Task<WebResponse> GetResponseAsync(this WebRequest client)
        {
            return Task<WebResponse>.Factory.FromAsync(client.BeginGetResponse,
              client.EndGetResponse, null);
        }

        public static Task<string> DownloadStringAsync(this IMyAsyncHttpService httpService, Uri address)
        {
            var tcs = new TaskCompletionSource<string>();
            httpService.DownloadString(address, (result, exception) =>
            {
                if (exception != null)
                    tcs.TrySetException(exception);
                else
                    tcs.TrySetResult(result);
            });
            return tcs.Task;
        }
    }
}
