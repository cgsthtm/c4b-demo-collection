﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows;
using System.Windows.Forms;

namespace C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1
{
    /* .NET 4.0框架引入了详尽的、精心设计的取消功能。取消采用协作方式，即可以请求某段代码取消，但不能强制它取消。如果某段代码本身不支持取消，就无法请求它取消运行。
     *   基于这个原因，建议大家在编写代码时尽量支持取消。
     * 取消是一种信号，包含两个不同的方面：触发取消的源头和响应取消的接收器。在.NET中源头是CancellationTokenSource，接收器是CancellationToken。
     *   本章将介绍这两方面常规用法，还将介绍如何与非标准的取消模式进行互操作。
     * 取消被看作是一种特殊类型的错误。通常被取消的代码会抛出类型为OperationCanceled Exception（或者它的子类，如TaskCanceledException）的异常。
     *   调用的代码用这种方式确认取消信号已被接收。
     * 为了表明某个方法支持取消，需要用一个CancellationToken作为该方法的参数。这个参数通常放在最后，除非该方法也支持进度报告（见2.3节）。
     *   也可考虑提供一个重载函数或一个参数默认值，供不需要取消的程序使用：
     * CancellationToken.None是一个等同于default(CancellationToken)的特殊值，表示这个方法是永远不会被取消的。如果启动这个操作后不准备取消，就可以在调用时使用这个值。
     */
    public partial class _9取消 : Form
    {
        public void CancelableMethodWithOverload(CancellationToken cancellationToken)
        {
            // 这里放代码
        }
        public void CancelableMethodWithOverload()
        {
            CancelableMethodWithOverload(CancellationToken.None);
        }
        public void CancelableMethodWithDefault(
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // 这里放代码
        }
        public _9取消()
        {
            InitializeComponent();
        }

        // 9.1 发出取消请求
        private void button1_Click(object sender, EventArgs e)
        {
            /* CancellationToken来源于CancellationTokenSource类。CancellationToken只是让代码能够响应取消请求，用CancellationTokenSource的用户可发出取消请求。
             *   多个CancellationTokenSource互相之间是独立的（除非把它们连接起来，具体操作将在9.8节介绍）。CancellationTokenSource的Token属性返回它的CancellationToken,
             *   Cancel方法发出真正的取消请求。
             * 下面的代码展示了如何创建一个CancellationTokenSource实例和如何使用Token和Cancel。用简短的例子更容易说明问题，因此代码使用了async方法。
             *   同样的Token/Cancel组合可以用来取消所有类型的代码：
             * 在前面的例子中，当任务启动后就把Task变量忽略了。在实际项目的开发中，这个变量可能会被存储起来并使用await等待，以便最终用户能看到运行结果。
             * 在取消任务运行时通常会产生竞态条件。如果在发出取消请求的时刻，被取消的代码即将完成，若来不及检查取消标记，那它就会正常地结束。
             *   在取消代码时实际上会有三种可能性：响应取消请求（抛出OperationCanceledException），正常结束，或者出现跟取消无关的错误并结束（抛出其他异常）。
             * 下面的例子与前面类似，但使用了await，说明了三种可能的结果：
             * 创建CancellationTokenSource实例和发出取消请求，这两步一般放在不同方法中。CancellationTokenSource实例一旦销毁就无法恢复。
             *   如果需要另一个取消标记源，就必须创建另一个实例。
             * 下面是一个更接近实际开发的GUI界面的例子，用一个按钮启动异步操作，另一个按钮用来取消这个操作。程序会禁用或启用“开始”和“取消”这两个按钮，
             *   以保证同一时间内只有一个操作：
             */
        }
        private Task CancelableMethodAsync(CancellationToken token)
        {
            throw new NotImplementedException();
        }
        void IssueCancelRequest()
        {
            var cts = new CancellationTokenSource();
            var task = CancelableMethodAsync(cts.Token);
            // 到这里，操作已经启动。
            // 发出取消请求。
            cts.Cancel();
        }
        async Task IssueCancelRequestAsync()
        {
            var cts = new CancellationTokenSource();
            var task = CancelableMethodAsync(cts.Token);
            // 这里，操作在正常运行。
            // 发出取消请求。
            cts.Cancel();
            //（异步地）等待操作结束。
            try
            {
                await task;
                // 如运行到这里，说明在取消请求生效前，操作正常完成。
            }
            catch (OperationCanceledException)
            {
                // 如运行到这里，说明操作在完成前被取消。
            }
            catch (Exception)
            {
                // 如运行到这里，说明在取消请求生效前，操作出错并结束。
                throw;
            }
        }
        private CancellationTokenSource cts;
        private async void StartButtonClick(object sender, RoutedEventArgs e)
        {
            //StartButton.IsEnabled = false;
            //CancelButton.IsEnabled = true;

            try
            {
                cts = new CancellationTokenSource();
                var token = cts.Token;
                await Task.Delay(TimeSpan.FromSeconds(5), token);
                MessageBox.Show("Delay completed successfully.");
            }
            catch (OperationCanceledException)
            {
                MessageBox.Show("Delay was canceled.");
            }
            catch (Exception)
            {
                MessageBox.Show("Delay completed with error.");
                throw;
            }
            finally
            {
                //StartButton.IsEnabled = true;
                //CancelButton.IsEnabled = false;
            }
        }
        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            cts.Cancel();
        }

        // 9.2 通过轮询响应取消请求
        private void button2_Click(object sender, EventArgs e)
        {
            /* 在代码的循环中实现取消。
             * 代码中正在运行一个循环时，就没有更低级别的API来接收CancellationToken了。这时可以周期性地检查标记是否已被取消。
             *   下面的代码在运行计算密集型的循环时，周期性地监测标记：
             * 如果循环很密集（即循环体的运行速度很快），就需要限制检查取消标记的频率。在确定最佳做法之前，通常要对此类修改前后的程序性能进行评估。
             *   下面的代码与上一个例子类似，但是循环速度更快、次数更多，因此对检查标记的频率进行了限制：
             * 大多数情况下，只需要把CancellationToken传递给下一层就行了。9.4节至9.7节有这方面的例子。只有要求在循环代码中支持取消时，才需要轮询检查标记。
             * CancellationToken类还有一个成员IsCancellationRequested，它会在标记被取消后返回true。有些人使用这个成员来响应取消请求，即通常返回一个默认值或null。
             *   不过我一般并不推荐这种做法。处理取消请求的标准模式是抛出一个OperationCanceledException异常，这个过程由ThrowIfCancellationRequested来负责。
             * 通过轮询取消标记使ThrowIfCancellationRequested起效，代码中必须以固定间隔调用这个方法。还有一种做法是注册一个回调函数，收到取消请求时会被调用。
             *   这种用回调函数的做法更像是与其他取消体系的互操作，因此我们把它放到9.9节。
             */
        }
        public int CancelableMethod(CancellationToken cancellationToken)
        {
            for (int i = 0; i != 100; ++i)
            {
                Thread.Sleep(1000); // 这里做一些计算工作。
                cancellationToken.ThrowIfCancellationRequested();
            }
            return 42;
        }
        public int CancelableMethod1(CancellationToken cancellationToken)
        {
            for (int i = 0; i != 100000; ++i)
            {
                Thread.Sleep(1); // 这里做一些计算工作。
                if (i % 1000 == 0)
                    cancellationToken.ThrowIfCancellationRequested();
            }
            return 42;
        }

        // 9.3 超时后取消
        private void button3_Click(object sender, EventArgs e)
        {
            /* 需要让一些代码在发生超时后停止运行。
             * 发生超时后，取消便是一种很自然的解决方案。超时只是一种取消请求的类型。需要取消的代码仅仅监视取消标记，不管取消的类型，代码不知道也不关心取消的来源是一个定时器。
             * .NET 4.5为取消标记源添加了几个便捷的方法，这些方法会基于定时器自动发出取消请求。可以把超时数据传给构造函数：
             * 还有一个方案，如果已经有了一个CancellationTokenSource实例，可以对该实例启动一个超时：
             * 只要执行代码时用到了超时，就该使用CancellationTokenSource和CancelAfter（或者用构造函数）。虽然还有其他途径可实现这个功能，
             *   但是使用现有的取消体系是最简单也是最高效的。别忘了被取消的代码需要监视取消标记。不支持取消的代码，是不可能被轻易取消的。
             */
        }
        async Task IssueTimeoutAsync()
        {
            var cts = new CancellationTokenSource(TimeSpan.FromSeconds(5));
            var token = cts.Token;
            await Task.Delay(TimeSpan.FromSeconds(10), token);
        }
        async Task IssueTimeoutAsync1()
        {
            var cts = new CancellationTokenSource();
            var token = cts.Token;
            cts.CancelAfter(TimeSpan.FromSeconds(5));
            await Task.Delay(TimeSpan.FromSeconds(10), token);
        }

        // 9.4 取消async代码
        private void button4_Click(object sender, EventArgs e)
        {
            /* 要使异步代码支持取消，最简单的办法就是把CancellationToken传递给下一层代码。这个例子执行一个异步的延时，然后返回一个值。
             *   它只是把标记传递给Task.Delay，就实现了对取消的支持：
             * 很多异步API都支持CancellationToken，因此在程序中实现取消是很容易的，只需要将标记传递下去即可。这有一个通用的准则，
             *   如果一个方法调用了支持CancellationToken的API，那这个方法也要支持CancellationToken并把它传给每个支持它的API。
             * 很可惜，有一些方法是不支持取消的。没有简单的解决方案可处理这种情况。要安全地停止这种“专横的”代码是不可能的，除非把它封装在一个独立的可执行程序中。
             *   碰到此类情况，你可以选择忽略它的返回结果，假装已经取消了这个操作。
             * 只要有可能，每个方法都要支持取消以供调用者选择使用。这是因为只有较低层次的代码正确地支持取消，较高层次的代码才有可能正确地支持取消。
             *   因此在编写自己的async方法时，要尽最大可能支持取消。你无法预料哪个较高层次的方法会调用你的方法，而该方法可能需要支持取消。
             */
        }
        public async Task<int> CancelableMethodAsync1(CancellationToken cancellationToken)
        {
            await Task.Delay(TimeSpan.FromSeconds(2), cancellationToken);
            return 42;
        }

        // 9.5 取消并行代码
        private void button5_Click(object sender, EventArgs e)
        {
            /* 需要让并行代码支持取消。
             * 要支持取消，最简单的方法是把CancellationToken传递进并行代码。并行方法使用ParallelOptions实例来支持取消。
             *   可以用下面的方法设置ParallelOptions实例的CancellationToken：
             * 另一个方法在循环体中直接监视CancellationToken：
             * 但是第二种方法更麻烦，用起来也不协调。使用第二种方法时，并行循环会把OperationCanceledException封装进AggregateException。
             *   另外，如果把CancellationToken传入到ParallelOptions的实例中，Parallel类会做出更加智能化的判断，确定检查标记的频率。因此，把标记作为参数传入是最好的做法。
             * 并行LINQ（PLINQ）本身也支持取消，可用WithCancellation操作符：
             * 在并行处理中支持取消，对于提高用户体验非常重要。程序在做并行处理时，至少会在短时间内使用大量的CPU。当CPU使用率很高时，即使没有妨碍同一电脑上的其他程序，
             *   用户也会注意到。因此建议大家在做并行计算（或其他CPU密集型程序）时一定要支持取消，即使维持CPU高使用率的总时间不是很长。
             */
        }
        static void RotateMatrices(IEnumerable<Matrix> matrices, float degrees, CancellationToken token)
        {
            Parallel.ForEach(matrices,
              new ParallelOptions { CancellationToken = token },
              matrix => matrix.Rotate(degrees));
        }
        static void RotateMatrices2(IEnumerable<Matrix> matrices, float degrees, CancellationToken token)
        {
            // 警告：不推荐使用，原因见后面。
            Parallel.ForEach(matrices, matrix =>
            {
                matrix.Rotate(degrees);
                token.ThrowIfCancellationRequested();
            });
        }
        static IEnumerable<int> MultiplyBy2(IEnumerable<int> values, CancellationToken cancellationToken)
        {
            return values.AsParallel()
              .WithCancellation(cancellationToken)
              .Select(item => item * 2);
        }

        // 9.6 取消响应式代码
        private void button6_Click(object sender, EventArgs e)
        {
            /* 需要让响应式代码支持取消。
             * 响应式扩展库有一个订阅可观察事件流的概念。要停止对事件流的订阅，只需在代码中释放订阅接口。一般情况下要在逻辑上停止对事件流的订阅，用这种方法就足够了。
             *   例如下面的代码，按一个按钮后就订阅鼠标点击事件流，按另一个按钮后就停止订阅：
             * 但是，在Rx框架中融合被广泛使用的CancellationTokenSource/CancellationToken体系其实是很方便的。本节的其余部分介绍Rx与CancellationToken交互的方法。
             * 首先来看一个主要场景：异步代码封装了可观察流代码。7.5节详细介绍了这种情况，现在我们要增加它对CancellationToken的支持。
             *   一般来说最简单的做法是：用响应式操作符实现所有操作，然后调用ToTask把最后一个作为结果的元素转换成支持await的Task对象。
             *   下面的代码展示用异步方式使用队列中的最后一个元素：
             *   使用第一个元素的方法也类似，只需要在调用ToTask前改一下observable：
             *   用异步方式把整个observable序列转换成task对象，也很类似：
             * 最后我们来看相反的情况。我们刚看了Rx代码响应CancellationToken的几种方法，也就是说，CancellationTokenSource的取消请求被转化为一个停止订阅指令（释放订阅接口）。
             *   我们也可以走另一个途径：把发出取消请求作为对释放订阅接口的响应。
             * 正如7.6节讲的，操作符FromAsync、StartAsync、SelectMany都支持取消。在绝大部分情况下这已经够用了。Rx也支持CancellationDisposable类，可以这样直接使用：
             * Rx有它自己关于取消的理念，那就是：停止订阅。.NET 4.0引入了通用的取消框架。本节介绍了几种让Rx与这种通用框架有机融合的方法。如果某段代码中只用到了Rx，
             *   那就使用Rx的“订阅/停止订阅”体系。只有在边界上才引入CancellationToken，以保持代码清晰。
             */
            //CancellationToken cancellationToken = new CancellationToken();
            //IObservable<int> observable = ...
            //int lastElement = await observable.TakeLast(1).ToTask(cancellationToken);
            // 或者int lastElement = await observable.ToTask(cancellationToken);

            //CancellationToken cancellationToken = ...
            //IObservable<int> observable = ...
            //int firstElement = await observable.Take(1).ToTask(cancellationToken);

            //CancellationToken cancellationToken = ...
            //IObservable<int> observable = ...
            //IList<int> allElements = await observable.ToList().ToTask(cancellationToken);

            using (var cancellation = new CancellationDisposable())
            {
                CancellationToken token = cancellation.Token;
                // 把这个标记传给会对它作出响应的方法。
            }
            // 到这里，这个标记已经是取消的。
        }
        private IDisposable mouseMovesSubscription;
        private void StartButtonClick1(object sender, RoutedEventArgs e)
        {
            var mouseMoves = Observable
              .FromEventPattern<MouseEventHandler, MouseEventArgs>(
                  handler => (s, a) => handler(s, a),
                  handler => MouseMove += handler,
                  handler => MouseMove -= handler)
              .Select(x => x.EventArgs.Location);
            mouseMovesSubscription = mouseMoves.Subscribe(val =>
            {
                //MousePositionLabel.Content = "(" + val.X + ", " + val.Y + ")";
            });
        }
        private void CancelButtonClick1(object sender, RoutedEventArgs e)
        {
            if (mouseMovesSubscription != null)
                mouseMovesSubscription.Dispose();
        }

        // 9.7 取消数据流网格
        private void button7_Click(object sender, EventArgs e)
        {
            /* 需要让数据流网格支持取消。
             * 在自己的代码中支持取消，最好的方法就是把CancellationToken传递给支持取消的API。数据流网格的每一个块都支持取消，可在DataflowBlockOptions中设置。
             *   要让自定义数据流块也支持取消，只需要在块的参数中设置CancellationToken属性：
             * 这个例子中网格的每一个块都使用了CancellationToken。这并不是十分必要的。因为完成信息也在块之间传递，可以只在第一块使用CancellationToken，
             *   然后让它在块之间传递。取消被认为是一种特殊形式的错误信息，而错误信息会在管道中传递下去，因此管道中的其他块也会产生错误并结束。
             *   但是当我们取消一个网格时，会希望每一个块同时被取消。因此通常是在每一个块中设置CancellationToken。
             * 在数据流网格中，取消过程不会有任何缓冲。块被取消后就会清除所有输入数据并停止接收新项目。因此如果取消一个运行中的块，数据就会丢失。
             */
        }
        IPropagatorBlock<int, int> CreateMyCustomBlock(CancellationToken cancellationToken)
        {
            var blockOptions = new ExecutionDataflowBlockOptions
            {
                CancellationToken = cancellationToken
            };
            var multiplyBlock = new TransformBlock<int, int>(item => item * 2,
                blockOptions);
            var addBlock = new TransformBlock<int, int>(item => item + 2,
                blockOptions);
            var divideBlock = new TransformBlock<int, int>(item => item / 2,
                blockOptions);

            var flowCompletion = new DataflowLinkOptions
            {
                PropagateCompletion = true
            };
            multiplyBlock.LinkTo(addBlock, flowCompletion);
            addBlock.LinkTo(divideBlock, flowCompletion);

            return DataflowBlock.Encapsulate(multiplyBlock, divideBlock);
        }

        // 9.8 注入取消请求
        private void button8_Click(object sender, EventArgs e)
        {
            /* 某一个层次的代码需要响应取消请求，同时它本身也要向下一层代码发出取消请求。
             * .NET 4.0的取消体系本身就有这种功能，即连接的取消标记。在创建一个取消标记源时，可把它连接到一个（或多个）已有的标记。建立连接后，
             *   这些已有标记中的任何一个被取消，新建的标记源中的标记也会被取消。也可以显式地取消这个标记源。
             * 下面的代码执行一个异步的HTTP请求。传入该方法的标记代表用户发出的取消请求，而这个方法也对HTTP请求使用了一个超时：
             * 如果用户取消了原有的cancellationToken，或者关联的标记源被CancelAfter取消，这个创建的combinedToken就会被取消。
             * 例子中只用了一个CancellationToken对象，但是CreateLinkedTokenSource方法的参数可以是任意多个取消标记。我们可以用它来创建一个组合标记，
             *   实现具有某种逻辑的取消功能。例如，ASP.NET有一个表示请求超时的标记（HttpRequest.TimedOutToken）
             *   和一个表示用户断开连接的标记（HttpResponse.ClientDisconnectedToken）。可以在代码中创建一个连接的标记，对这些取消请求中的任意一个做出响应。
             * 需要注意已连接的取消标记源的生命周期。前面的例子代表了常见的情况，一个或多个取消标记传入方法，然后被连接在一起并作为组合标记继续传下去。
             *   注意例子中的代码使用了using语句，当操作完成时（不需要继续使用组合标记了）会释放已连接的取消标记源。假定不释放已连接的取消标记源，
             *   会发生什么情况：这个方法可能被多次调用，每次使用同一个（长寿命的）原有标记，这样每次都会连接一个新的标记源。即使HTTP请求已经结束了（没有用到组合标记），
             *   标记源仍会连在原有的标记上。为了防止这种内存泄漏的情况，一旦不再需要组合标记了，就要释放已连接的取消标记源。
             */
        }
        async Task<HttpResponseMessage> GetWithTimeoutAsync(string url, CancellationToken cancellationToken)
        {
            var client = new HttpClient();

            using (var cts = CancellationTokenSource
              .CreateLinkedTokenSource(cancellationToken))
            {
                cts.CancelAfter(TimeSpan.FromSeconds(2));
                var combinedToken = cts.Token;
                return await client.GetAsync(url, combinedToken);
            }
        }

        // 9.9 与其他取消体系的互操作
        private void button9_Click(object sender, EventArgs e)
        {
            /* 有一些外部的或以前遗留下来的代码采用了非标准的取消模式。现在要用标准的CancellationToken来控制这些代码。
             * CancellationToken类响应取消请求有两种主要的方式：轮询（见9.2节）和回调函数（本节的主题）。轮询通常用于计算密集型代码，如数据处理的循环。
             *   回调函数通常用于其他情况。可以用CancellationToken.Register方法注册一个取消标记的回调函数。
             * 例如，假设我们要封装System.Net.NetworkInformation.Ping类，并且要实现对一个ping过程的取消。这个Ping类已经有基于Task的API，但不支持CancellationToken。
             *   但是Ping类有自己的SendAsyncCancel方法，可以用来取消一个ping过程。因此我们注册一个调用这个方法的回调函数，如下所示：
             * 这样，在发出取消请求时这个回调函数就会调用SendAsyncCancel方法，来取消SendPing Async的执行。
             * 可以用CancellationToken.Register方法与任何类型的非主流取消体系进行互操作。但是有一点一定要记住，当一个方法使用CancellationToken后，
             *   一个取消请求应该只用来取消那一个操作。有些非主流的取消体系通过关闭一些资源来实现取消，而关闭资源会取消多个操作。
             *   此类取消体系就不大适合使用CancellationToken。如果你一定要用CancellationToken封装此类取消体系，那就在文档中把这个不寻常的语义描述清楚吧。
             * 要特别注意回调函数注册的生命周期。Register方法返回一个可释放的对象，应该在不再需要回调函数时把它释放。前面的例子使用了using语句，会在异步操作结束时清理资源。
             *   如果不使用using语句，每次调用这个例子时使用同一个（长寿命的）CancellationToken，它就会每次都添加一个回调函数（这个回调函数又会使Ping对象继续存活）。
             *   为了避免内存和资源的泄漏，一旦不再需要使用回调函数了，就要释放这个回调函数注册。
             */
        }
        async Task<PingReply> PingAsync(string hostNameOrAddress, CancellationToken cancellationToken)
        {
            var ping = new Ping();
            using (cancellationToken.Register(() => ping.SendAsyncCancel()))
            {
                return await ping.SendPingAsync(hostNameOrAddress);
            }
        }
    }
}
