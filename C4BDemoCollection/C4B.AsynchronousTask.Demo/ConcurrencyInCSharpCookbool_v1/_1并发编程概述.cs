﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Timers;
using System.Windows.Forms;

namespace C4B.AsynchronousTask.Demo
{
    /* 现代的异步.NET程序使用两个关键字：async和await。async关键字加在方法声明上，它的主要目的是使方法内的await关键字生效（为了保持向后兼容，同时引入了这两个关键字）。
     *   如果async方法有返回值，应返回Task<T>；如果没有返回值，应返回Task。
     * 不要用void作为async方法的返回类型！async方法可以返回void，但是这仅限于编写事件处理程序。一个普通的async方法如果没有返回值，要返回Task，而不是void。
     */
    public partial class Form1 : Form
    {
        string fileName = $"{Application.StartupPath}\\StabilityTest.txt";
        public Form1()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            //await DoSomethingAsync1();
            //await DoSomethingAsync2();
            //Deadlock();
            //UnDeadlock();
            //await TrySomethingAsync1();
            await TrySomethingAsync2();
        }

        async Task DoSomethingAsync1()
        {
            /* 和其他方法一样，async方法在开始时以同步方式执行。在async方法内部，await关键字对它的参数执行一个异步等待。它首先检查操作是否已经完成，如果完成了，
             *   就继续运行（同步方式）。否则，它会暂停async方法，并返回，留下一个未完成的task。一段时间后，操作完成，async方法就恢复运行。
             */
            int val = 13;

            //异步方式等待3秒
            await Task.Delay(TimeSpan.FromSeconds(3));

            val *= 2;
            //异步方式等待1秒
            await Task.Delay(TimeSpan.FromSeconds(1));

            Trace.WriteLine(val);
            /* 一个async方法是由多个同步执行的程序块组成的，每个同步程序块之间由await语句分隔。
             * 一般来说，运行UI线程时采用UI上下文，处理ASP.NET请求时采用ASP.NET请求上下文，其他很多情况下则采用线程池上下文。
             * 如果在UI线程中调用DoSomethingAsync，这个方法的每个同步程序块都将在此UI线程上运行。但是，如果在线程池线程中调用，每个同步程序块将在线程池线程上运行。
             *   要避免这种错误行为，可以在await中使用ConfigureAwait方法，将参数continueOn CapturedContext设为false。接下来的代码刚开始会在调用的线程里运行，
             *   在被await暂停后，则会在线程池线程里继续运行：
             */
        }

        async Task DoSomethingAsync2()
        {
            // 最好的做法是，在核心库代码中一直使用ConfigureAwait。在外围的用户界面代码中，只在需要时才恢复上下文。
            int val = 13;

            //异步方式等待3秒
            await Task.Delay(TimeSpan.FromSeconds(3)).ConfigureAwait(false);

            val *= 2;

            //异步方式等待1秒
            await Task.Delay(TimeSpan.FromSeconds(1)).ConfigureAwait(false);

            Trace.WriteLine(val.ToString());
        }

        async Task TrySomethingAsync1()
        {
            /* 使用async和await时，自然要处理错误。在下面的代码中，PossibleExceptionAsync会抛出一个NotSupportedException异常，
             *   而TrySomethingAsync方法可很顺利地捕捉到这个异常。这个捕捉到的异常完整地保留了栈轨迹，没有人为地将它封装进TargetInvocation Exception
             *   或AggregateException类：
             */
            try
            {
                await PossibleExceptionAsync();
            }
            catch (NotSupportedException ex)
            {
                LogException(ex);
                //throw;
            }
        }

        /* 一旦异步方法抛出（或传递出）异常，该异常会放在返回的Task对象中，并且这个Task对象的状态变为“已完成”。当await调用该Task对象时，
         *   await会获得并（重新）抛出该异常，并且保留着原始的栈轨迹。因此，如果PossibleExceptionAsync2是异步方法，以下代码就能正常运行：
         */
        async Task TrySomethingAsync2()
        {
            //发生异常时，任务结束。不会直接抛出异常。
            Task task = PossibleExceptionAsync2();

            try
            {
                //Task对象中的异常，会在这条await语句中引发
                await task;
            }
            catch (NotSupportedException ex)
            {
                LogException(ex);
                //throw;
            }
        }

        private void LogException(NotSupportedException ex)
        {
            Trace.WriteLine($"捕获到异常：{ex}");
        }

        private Task PossibleExceptionAsync()
        {
            throw new NotSupportedException();
        }

        private async Task PossibleExceptionAsync2()
        {
            throw new NotSupportedException();
        }

        async Task WaitAsync()
        {
            //这里awati会捕获当前上下文……
            await Task.Delay(TimeSpan.FromSeconds(3));
            // ……这里会试图用上面捕获的上下文继续执行
        }

        /* 关于异步方法，还有一条重要的准则：你一旦在代码中使用了异步，最好一直使用。调用异步方法时，应该（在调用结束时）用await等待它返回的task对象。
         *   一定要避免使用Task.Wait或Task<T>.Result方法，因为它们会导致死锁。参考一下下
         */
        void Deadlock()
        {
            //开始延迟
            Task task = WaitAsync();

            //同步程序块，正在等待异步方法完成
            task.Wait();
            /* 如果从UI或ASP.NET的上下文调用这段代码，就会发生死锁。这是因为，这两种上下文每次只能运行一个线程。Deadlock方法调用WaitAsync方法，
             *   WaitAsync方法开始调用delay语句。然后，Deadlock方法（同步）等待WaitAsync方法完成，同时阻塞了上下文线程。
             *   当delay语句结束时，await试图在已捕获的上下文中继续运行WaitAsync方法，但这个步骤无法成功，因为上下文中已经有了一个阻塞的线程，
             *   并且这种上下文只允许同时运行一个线程。
             * 这里有两个方法可以避免死锁：在WaitAsync中使用ConfigureAwait(false)（导致await忽略该方法的上下文），
             *   或者用await语句调用WaitAsync方法（让Deadlock变成一个异步方法）。
             */
            Trace.WriteLine($"Deadlock执行结束");
        }

        async void UnDeadlock()
        {
            //开始延迟
            Task task = WaitAsync();

            //同步程序块，正在等待异步方法完成
            Stopwatch stopwatch = Stopwatch.StartNew();
            stopwatch.Start();
            await task;
            stopwatch.Stop();
            Trace.WriteLine($"ElapsedMilliseconds:{stopwatch.ElapsedMilliseconds}");
            Trace.WriteLine($"Deadlock执行结束");
            /* Output:
                ElapsedMilliseconds:3005
                Deadlock执行结束
             */
        }

        System.Timers.Timer aTimer = new System.Timers.Timer(10);

        private async void button2_Click(object sender, EventArgs e)
        {
            //CancellationTokenSource cts0 = new CancellationTokenSource();
            //CancellationTokenSource cts1 = new CancellationTokenSource();
            //CancellationTokenSource cts2 = new CancellationTokenSource();
            //CancellationTokenSource cts3 = new CancellationTokenSource();
            //CancellationTokenSource cts4 = new CancellationTokenSource();
            //Task<int> task0 = Func2(cts0, "task0");
            //Task<int> task1 = Func2(cts1, "task1");
            //Task<int> task2 = Func2(cts2, "task2");
            //Task<int> task3 = Func2(cts3, "task3");

            //var s = Task.Factory.StartNew(() => Func222(1, 2));

            //try
            //{
            //    Task task = Task.Run(() =>
            //    {
            //        richTextBox1.BeginInvoke(new Action(() => richTextBox1.AppendText("1111")));
            //    });
            //    int ret0 = await task0;
            //    int ret1 = await task1;
            //    int ret2 = await task2;
            //    int ret3 = await task3;
            //    Console.WriteLine($"ret0:{ret0}");
            //    Console.WriteLine($"ret1:{ret1}");
            //    Console.WriteLine($"ret2:{ret2}");
            //    Console.WriteLine($"ret3:{ret3}");
            //    Task<int> task4 = Func22(cts4, "task4",ret0);
            //    int ret4 = await task4;
            //    Console.WriteLine($"ret4:{ret4}");
            //    Console.WriteLine("oh~~~~~~~~~~~~~~~~~~");
            //}
            //catch (Exception ex)
            //{
            //    throw;
            //}

            timer1.Enabled = true;
        }

        List<int> gLst = new List<int>()
        {
            11,22,33,44,55,66,77,88,99,1010,1111,1212,1313,1414,1515
        };
        async Task<int> Func2(CancellationTokenSource cts, string taskName)
        {
            int ret = 0;
            int i = 0;
            while (!cts.Token.IsCancellationRequested)
            {
                switch (i)
                {
                    case 0:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 1:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 2:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 3:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 4:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 5:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                            foreach (int item in gLst)
                            {
                                if (item == 1414)
                                {
                                    Console.WriteLine($"{taskName} Func2:{i} 遍历gLst找到{item}");
                                    IAsyncResult ir = richTextBox1.BeginInvoke(new Func<string>(() => { return richTextBox1.Text; }));
                                    Console.WriteLine($"{richTextBox1.EndInvoke(ir)}");
                                    //MessageBox.Show("00000000");
                                }
                            }
                        }
                        break;
                    case 6:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 7:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 8:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 9:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                            ret = 100;
                            //cts.Cancel();
                            return ret;
                        }
                        break;
                    default:
                        break;
                }
                await Task.Delay(1000).ConfigureAwait(false);
            }
            return ret;
        }

        async Task<int> Func22(CancellationTokenSource cts, string taskName, int res)
        {
            int ret = 0;
            int i = 0;
            while (!cts.Token.IsCancellationRequested)
            {
                switch (i)
                {
                    case 0:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 1:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 2:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 3:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 4:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 5:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                            foreach (int item in gLst)
                            {
                                if (item == 1414)
                                {
                                    Console.WriteLine($"{taskName} Func2:{i} 遍历gLst找到{item}");
                                    IAsyncResult ir = richTextBox1.BeginInvoke(new Func<string>(() => { return richTextBox1.Text; }));
                                    Console.WriteLine($"{richTextBox1.EndInvoke(ir)}");
                                    MessageBox.Show("00000000");
                                }
                            }
                        }
                        break;
                    case 6:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 7:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 8:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                        }
                        break;
                    case 9:
                        {
                            ret = i;
                            i++;
                            Console.WriteLine($"{taskName} Func2:{i}");
                            if(res == 100)
                                ret = 100;
                            else
                                ret = 0;
                            cts.Cancel();
                        }
                        break;
                    default:
                        break;
                }
                await Task.Delay(1000).ConfigureAwait(false);
            }
            return ret;
        }

        List<string> Func222(int a,int b)
        {
            return new List<string>() { "aaa", "bbb" };
        }

        int bkPoint = 0;
        List<Task<int>> task0 = new List<Task<int>>();
        List<Task<int>> task1 = new List<Task<int>>();
        CancellationTokenSource cts0 = new CancellationTokenSource();
        private async void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            //Console.WriteLine("The Elapsed event was raised at {0:HH:mm:ss.fff}", e.SignalTime);

            
        }

        private async void timer1_Tick(object sender, EventArgs e)
        {
            switch (bkPoint)
            {
                case 0:
                    {
                        Console.WriteLine($"bkPoint:{bkPoint}==========================================");
                        timer1.Enabled = false;
                        task0.Add(Func2(cts0, "task0"));
                        task0.Add(Func2(cts0, "task1"));
                        bkPoint = 1;
                        timer1.Enabled = true;
                    }
                    break;
                case 1:
                    {
                        Console.WriteLine($"bkPoint:{bkPoint}==========================================");
                        timer1.Enabled = false;
                        foreach (var task in task0)
                        {
                            int ret0 = await task;
                            task1.Add(Func2(cts0, $"task{ret0}"));
                        }
                        bkPoint = 2;
                        timer1.Enabled = true;
                    }
                    break;
                case 2:
                    {
                        Console.WriteLine($"bkPoint:{bkPoint}==========================================");
                        timer1.Enabled = false;
                        foreach (var task in task1)
                        {
                            Console.WriteLine($"最后结果{await task}");
                        }
                        bkPoint = 3;
                        timer1.Enabled = true;
                    }
                    break;
                case 3:
                    {
                        Console.WriteLine($"bkPoint:{bkPoint}==========================================");
                        timer1.Enabled = false;
                        bkPoint = 0;
                        task0.Clear();
                        task1.Clear();
                    }
                    break;
            }
        }

        // 1.3 并行编程简介
        private void button3_Click(object sender, EventArgs e)
        {
            /* 如果程序中有大量的计算任务，并且这些任务能分割成几个互相独立的任务块，那就应该使用并行编程。
             * 并行的形式有两种：数据并行（data parallelism）和任务并行（task parallelism）。
             *   数据并行是指有大量的数据需要处理，并且每一块数据的处理过程基本上是彼此独立的。任务并行是指需要执行大量任务，并且每个任务的执行过程基本上是彼此独立的。
             * 实现数据并行有几种不同的做法。一种做法是使用Parallel.ForEach方法，它类似于foreach循环，应尽可能使用这种做法。
             *   另一种做法是使用PLINQ（Parallel LINQ），它为LINQ查询提供了AsParallel扩展。跟PLINQ相比，Parallel对资源更加友好，
             *   Parallel与系统中的其他进程配合得比较好，而PLINQ会试图让所有的CPU来执行本进程。Parallel的缺点是它太明显。很多情况下，PLINQ的代码更加优美。
             * 不管选用哪种方法，在并行处理时有一个非常重要的准则。[插图]每个任务块要尽可能的互相独立。
             * 只要任务块是互相独立的，并行性就能做到最大化。一旦你在多个线程中共享状态，就必须以同步方式访问这些状态，那样程序的并行性就变差了。
             * 数据并行重点在处理数据，任务并行则关注执行任务。
             * Parallel类的Parallel.Invoke方法可以执行“分叉/联合”（fork/join）方式的任务并行。
             * 现在Task这个类也被用于异步编程，但当初它是为了任务并行而引入的。任务并行中使用的一个Task实例表示一些任务。可以使用Wait方法等待任务完成，
             *   还可以使用Result和Exception属性来检查任务执行的结果。直接使用Task类型的代码比使用Parallel类要复杂，但是，如果在运行前不知道并行任务的结构，就需要使用Task类型。
             * 如果使用动态并行机制，在开始处理时，任务块的个数是不确定的，只有继续执行后才能确定。通常情况下，一个动态任务块要启动它所需的所有子任务，
             *   然后等待这些子任务执行完毕。为实现这个功能，可以使用Task类型中的一个特殊标志：TaskCreationOptions.AttachedToParent。动态并行机制在3.4节中详述。
             * 对所有并行处理类型来讲，错误处理的方法都差不多。由于操作是并行执行的，多个异常就会同时发生，系统会把这些异常封装在AggregateException类里，
             *   在程序中抛给代码。这一特点对所有方法都是一样的，包括Parallel.ForEach、Parallel.Invoke、Task.Wait等。
             *   AggregateException类型有几个实用的Flatten和Handle方法，用来简化错误处理的代码：
             * 根据一个通用的准则，只要没有导致性能问题，我会让任务尽可能短（如果任务太短，程序性能会突然降低）。更好的做法是使用Parallel类型或者PLINQ，而不是直接使用任务。
             *   这些并行处理的高级形式，自带有自动分配任务的算法（并且会在运行时自动调整）。
             */
            try
            {
                Parallel.Invoke(() => { throw new Exception(); },
                  () => { throw new Exception(); });
            }
            catch (AggregateException ex)
            {
                ex.Handle(exception =>
                {
                    Trace.WriteLine(exception);
                    return true; //“已经处理”
                });
            }
        }
        void ProcessArray(double[] array)
        {
            Parallel.Invoke(
              () => ProcessPartialArray(array, 0, array.Length / 2),
              () => ProcessPartialArray(array, array.Length / 2, array.Length)

            );
        }

        void ProcessPartialArray(double[] array, int begin, int end)
        {
            // CPU密集型的操作……
        }

        // 1.4 响应式编程简介
        private void button4_Click(object sender, EventArgs e)
        {
            /* 响应式编程可以像处理数据流一样处理事件流。根据经验，如果事件中带有参数，那么最好采用响应式编程，而不是常规的事件处理程序。
             * 响应式编程基于“可观察的流”（observable stream）这一概念。你一旦申请了可观察流，就可以收到任意数量的数据项（OnNext），
             *   并且流在结束时会发出一个错误（OnError）或一个“流结束”的通知（OnCompleted）。
             * 微软的Reactive Extensions（Rx）库已经实现了所有接口。响应式编程的最终代码非常像LINQ，可以认为它就是“LINQ to events”。
             * LINQ具有的特性，Rx也都有。Rx在此基础上增加了很多它自己的操作符，特别是与时间有关的操作符：
             */
            Observable.Interval(TimeSpan.FromSeconds(1))
                .Timestamp()
                .Where(x => x.Value % 2 == 0)
                .Select(x => x.Timestamp)
                .Subscribe(x => Trace.WriteLine(x));
            /* 上面的代码中，首先是一个延时一段时间的计数器（Interval），随后为每个事件加了一个时间戳（Timestamp）。
             *   接着对事件进行过滤，只包含偶数值（Where），选择了时间戳的值（Timestamp），然后当每个时间戳值到达时，把它输入调试器（Subscribe）。
             * 现在只要记住这是一个LINQ查询，与你以前见过的LINQ查询很类似。主要区别在于：LINQ to Object和LINQ to Entity使用“拉取”模式，
             *   LINQ的枚举通过查询拉出数据。而LINQ to event（Rx）使用“推送”模式，事件到达后就自行穿过查询。
             */

            // 可观察流的定义和其订阅是互相独立的。上面最后一个例子与下面的代码等效：
            //IObservable<DateTimeOffset> timestamps =
            //    Observable.Interval(TimeSpan.FromSeconds(1))
            //    .Timestamp()
            //    .Where(x => x.Value % 2 == 0)
            //    .Select(x => x.Timestamp);
            //timestamps.Subscribe(x => Trace.WriteLine(x));
            /* 一种常规的做法是把可观察流定义为一种类型，然后将其作为IObservable<T>资源使用。其他类型可以订阅这些流，或者把这些流与其他操作符组合，创建另一个可观察流。
             */

            // 同样，所有Subscribe操作符都需要有处理错误的参数。前面的例子没有错误处理参数。下面则是一个更好的例子，在可观察流发生错误时，它能正确处理：
            Observable.Interval(TimeSpan.FromSeconds(1))
                .Timestamp()
                .Where(x => x.Value % 2 == 0)
                .Select(x => x.Timestamp)
                .Subscribe(x => Trace.WriteLine(x),
                  ex => Trace.WriteLine(ex));

            // Rx的操作符非常多，本书只介绍了一部分。想了解关于Rx的更多信息，建议阅读优秀的在线图书Introduction to Rx。 http://introtorx.com/
        }

        // 1.5 数据流简介
        private void button5_Click(object sender, EventArgs e)
        {
            /* TPL数据流很有意思，它把异步编程和并行编程这两种技术结合起来。如果需要对数据进行一连串的处理，TPL数据流就很有用。
             *   例如，需要从一个URL上下载数据，接着解析数据，然后把它与其他数据一起做并行处理。
             * TPL数据流通常作为一个简易的管道，数据从管道的一端进入，在管道中穿行，最后从另一端出来。不过，TPL数据流的功能比普通管道要强大多了。
             *   对于处理各种类型的网格（mesh），在网格中定义分叉（fork）、连接（join）、循环（loop）的工作，TPL数据流都能正确地处理。
             *   当然了，大多数时候TPL数据流网格还是被用作管道。
             * 数据流网格的基本组成单元是数据流块（dataflow block）。数据流块可以是目标块（接收数据）或源块（生成数据），或两者皆可。
             *   源块可以连接到目标块，创建网格。连接的具体内容在4.1节介绍。数据流块是半独立的，当数据到达时，数据流块会试图对数据进行处理，并且把处理结果推送给下一个流程。
             *   使用TPL数据流的常规方法是创建所有的块，再把它们链接起来，然后开始在一端填入数据。然后，数据会自行从另一端出来。
             * 再强调一次，数据流的功能比这要强大得多，数据穿过的同时，可能会断开连接、创建新的块并加入到网格，不过这是非常高级的使用场景。
             * 目标块带有缓冲区，用来存放收到的数据。因此，在还来不及处理数据的时候，它仍能接收新的数据项，这就让数据可以持续地在网格上流动。
             *   在有分叉的情况下，一个源块链接了两个目标块，这种缓冲机制就会产生问题。当源块有数据需要传递下去时，它会把数据传给与它链接的块，并且一次只传一个数据。
             *   默认情况下，第一个目标块会接收数据并缓存起来，而第二个目标块就收不到任何数据。解决这个问题的方法是把目标块设置为“非贪婪”模式，以限制缓冲区的数量，
             *   这部分将在4.4节介绍。
             * 如果某些步骤出错，例如委托在处理数据项时抛出异常，数据流块就会出错。数据流块出错后就会停止接收数据。默认情况下，一个块出错不会摧毁整个网格。
             *   这让程序有能力重建部分网格，或者对数据重新定向。然而这是一个高级用法。通常来讲，你是希望这些错误通过链接传递给目标块。
             *   数据流也提供这个选择，唯一比较难办的地方是当异常通过链接传递时，它就会被封装在AggregateException类中。因此，如果管道很长，最后异常的嵌套层次会非常多，
             *   这时就可以使用AggregateException.Flatten方法：
             * 数据流错误的处理方法将在4.2节详细介绍。
             */
            try
            {
                var multiplyBlock = new TransformBlock<int, int>(item =>
                {
                    if (item == 1)
                        throw new InvalidOperationException("Blech.");
                    return item * 2;
                });
                var subtractBlock = new TransformBlock<int, int>(item => item - 2);
                multiplyBlock.LinkTo(subtractBlock,
                  new DataflowLinkOptions { PropagateCompletion = true });

                multiplyBlock.Post(1);
                subtractBlock.Completion.Wait();
            }
            catch (AggregateException exception)
            {
                AggregateException ex = exception.Flatten();
                Trace.WriteLine(ex.InnerException);
            }
            /* 数据流网格给人的第一印象是与可观察流非常类似，实际上它们确实有很多共同点。网格和流都有“数据项”这一概念，数据项从网格或流的中间穿过。
             *   还有，网格和流都有“正常完成”（表示没有更多数据需要接收时发出的通知）和“不正常完成”（在处理数据中发生错误时发出的通知）这两个概念。
             *   但是，Rx和TPL数据流的性能并不相同。如果执行需要计时的任务，最好使用Rx的observable对象，而不是数据流块。
             *   如果进行并行处理，最好使用数据流块，而不是Rx的observable对象。
             * 从概念上说，Rx更像是建立回调函数：observable对象中的每个步骤都会直接调用下一步。相反，数据流网格中的每一块都是互相独立的。
             *   Rx和TPL数据流有各自的应用领域，也有一些交叉的领域。另一方面，Rx和TPL数据流也非常适合同时使用。Rx和TPL数据流的互操作性将在7.7节详细介绍。
             * 最常用的块类型有TransformBlock<TInput, TOutput>（与LINQ的Select类似）、TransformManyBlock<TInput, Toutput>（与LINQ的SelectMany类似）
             *   和ActionBlock<T>（为每个数据项运行一个委托）。要了解TPL数据流的更多知识，建议阅读MSDN的文档和Guide to Implementing Custom TPL Dataflow Blocks。
             */
        }

        // 1.6 多线程编程简介
        private void button6_Click(object sender, EventArgs e)
        {
            /* 线程是一个独立的运行单元，每个进程内部有多个线程，每个线程可以各自同时执行指令。每个线程有自己独立的栈，但是与进程内的其他线程共享内存。
             *   对某些程序来说，其中有一个线程是特殊的，例如用户界面程序有一个UI线程，控制台程序有一个main线程。
             * 每个.NET程序都有一个线程池，线程池维护着一定数量的工作线程，这些线程等待着执行分配下来的任务。线程池可以随时监测线程的数量。
             *   配置线程池的参数多达几十个，但是建议采用默认设置，线程池的默认设置是经过仔细调整的，适用于绝大多数现实中的应用场景。
             * 线程是低级别的抽象，线程池是稍微高级一点的抽象，当代码段遵循线程池的规则运行时，线程池就会在需要时创建线程。
             *   本书介绍的技术抽象级别更高：并行和数据流的处理队列会根据情况遵循线程池运行。抽象级别更高，正确代码的编写就更容易。
             * 基于这个原因，本书根本不介绍Thread和BackgroundWorker这两种类型。它们曾经非常流行，但那个时代已经过去了。
             */
        }

        // 1.7 并发编程的集合
        private void button7_Click(object sender, EventArgs e)
        {
            /* 并发编程所用到的集合有两类：并发集合和不可变集合。这两种类别的集合将在第8章详细介绍。多个线程可以用安全的方式同时更新并发集合。
             *   大多数并发集合使用快照（snapshot），当一个线程在增加或删除数据时，另一个线程也能枚举数据。比起给常规集合加锁以保护数据的方式，采用并发集合的方式要高效得多。
             * 不可变集合则有些不同。不可变集合实际上是无法修改的。要修改一个不可变集合，需要建立一个新的集合来代表这个被修改了的集合。
             *   这看起来效率非常低，但是不可变集合的各个实例之间尽可能多地共享存储区，因此实际上效率没想象得那么差。不可变集合的优点之一，就是所有的操作都是简洁的，
             *   因此特别适合在函数式代码中使用。
             */
        }

        // 1.8 现代设计
        private void button8_Click(object sender, EventArgs e)
        {
            /* 大多数并发编程技术有一个类似点：它们本质上都是函数式（functional）的。这里“functional”的意思不是“实用，能完成任务”[插图]，
             *   而是把它作为一种基于函数组合的编程模式。如果你接受函数式的编程理念，并发编程的设计就会简单得多。
             * 函数式编程的一个原则就是简洁（换言之，就是避免副作用）。解决方案中的每一个片段都用一些值作为输入，生成一些值作为输出。
             *   应该尽可能避免让这些段落依赖于全局（或共享）变量，或者修改全局（或共享）数据结构。不论这个片段是异步方法、并行任务、Rx操作还是数据流块，都应该这么做。
             *   当然了，具体做法迟早会受到计算内容的影响，但如果能用简洁的段落来处理，然后用结果来执行更新，代码就会更加清晰。
             * 函数式编程的另一个原则是不变性。不变性是指一段数据是不能被修改的。在并发编程中使用不可变数据的原因之一，是程序永远不需要对不可变数据进行同步。
             *   数据不能修改，这一事实让同步变得没有必要。不可变数据也能避免副作用。在编写本书时（2014年），虽然不可变数据还没有被广泛接受，
             *   但本书中有几节会介绍不可变数据结构。
             */
        }
    }
}
