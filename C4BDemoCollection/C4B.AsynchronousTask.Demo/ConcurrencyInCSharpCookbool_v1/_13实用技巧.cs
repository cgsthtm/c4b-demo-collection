﻿using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1
{
    public partial class _13实用技巧 : Form
    {
        public _13实用技巧()
        {
            InitializeComponent();
        }

        // 13.1 初始化共享资源
        private void button1_Click(object sender, EventArgs e)
        {
            /* 程序的多个部分共享了一个资源，现在要在第一次访问该资源时对它初始化。
             * .NET框架中有一个专门用来解决这种问题的类：Lazy<T>。在构造这个类的实例时，用一个工厂委托（factory delegate）进行初始化。
             *   通过Value属性使这个实例变得可用。下面的代码演示了Lazy<T>类：
             * 不管同时有多少线程调用UseSharedInteger，这个工厂委托只会运行一次，并且所有线程都等待同一个实例。实例在创建后会被缓存起来，
             *   以后所有对Value属性的访问都返回同一个实例（前面的例子中，MySharedInteger.Value永远是0）。
             * 如果初始化过程需要执行异步任务，可以采用一个非常类似的方法。本例使用Lazy<Task<T>>：
             * 本例中委托返回一个Task<int>对象，就是一个用异步方式得到的整数值。不管有多少代码段同时调用Value, Task<int>对象只会创建一次，并且每个调用都返回同一个对象。
             *   每个调用者可以用await调用这个Task对象，（异步地）等待它完成。
             * 这种模式是可行的，但还有一点需要注意。这个异步的委托可能在任何调用Value的线程中运行，也就会在对应的上下文中运行。如果有几种不同类型的线程会调用Value
             *   （例如一个UI线程和一个线程池线程，或者两个不同的ASP.NET请求线程），那最好让委托只在线程池线程中运行。这实现起来很简单，
             *   只要把工厂委托封装在Task.Run调用中： 
             * 最后一个例子是Lazy对象异步初始化的通用模式，可惜有些繁琐。AsyncEx库中有一个与Lazy<Task<T>>功能相似的类AsyncLazy<T>，它会在线程池中执行工厂委托。
             *   它也可以直接进行await，声明和使用的方法如下：
             */
        }
        static int simpleValue;
        static readonly Lazy<int> MySharedInteger = new Lazy<int>(() => simpleValue++);
        void UseSharedInteger()
        {
            int sharedValue = MySharedInteger.Value;
        }
        static int simpleValue1;
        static readonly Lazy<Task<int>> MySharedAsyncInteger1 =
            new Lazy<Task<int>>(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(2)).ConfigureAwait(false);
                return simpleValue1++;
            });

        async Task GetSharedIntegerAsync()
        {
            int sharedValue = await MySharedAsyncInteger1.Value;
        }
        static readonly Lazy<Task<int>> MySharedAsyncInteger2 = new Lazy<Task<int>>(
            () => Task.Run(async () =>
              {
                  await Task.Delay(TimeSpan.FromSeconds(2));
                  return simpleValue++;
              }));
        private static readonly AsyncLazy<int> MySharedAsyncInteger3 =
            new AsyncLazy<int>(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(2));
                return simpleValue++;
            });
        public async Task UseSharedIntegerAsync()
        {
            int sharedValue = await MySharedAsyncInteger3;
        }

        // 13.2 Rx延迟求值
        private void button2_Click(object sender, EventArgs e)
        {
            /* 想要在每次被订阅时就创建一个新的源observable对象。例如让每个订阅代表一个不同的Web服务请求。
             * Rx库有一个操作符Observable.Defer，每次observable对象被订阅时，它就会执行一个委托。该委托相当于是一个创建observable对象的工厂。
             *   下面的代码中每次订阅observable对象，都会使用Defer调用一个异步方法：
             * 应用程序的代码一般不会多次订阅一个observable对象，但有些Rx操作符会在内部多次订阅一个observable对象。例如一旦条件满足，
             *   Observable.While操作符会重新订阅一个源序列。用Defer可以让observable对象在每次有新的订阅时就重新求值。如果需要刷新或更新observable对象的数据，
             *   就可以用这个方法。
             */
            var invokeServerObservable = Observable.Defer(() => GetValueAsync().ToObservable());
            invokeServerObservable.Subscribe( x => { });
            invokeServerObservable.Subscribe( x => { });
            Console.ReadKey();
        }
        static async Task<int> GetValueAsync()
        {
            Console.WriteLine("Calling server...");
            await Task.Delay(TimeSpan.FromSeconds(2));
            Console.WriteLine("Returning result...");
            return 13;
        }

        // 13.3 异步数据绑定
        private void button3_Click(object sender, EventArgs e)
        {
            /* 在异步地检索数据时，需要对结果进行数据绑定（例如绑定到Model-View-ViewModel设计模式中的ViewModel）。
             * 如果使用属性进行数据绑定，这个属性必须立即同步地返回某种结果。如果需要异步地确定实际值，那可以先返回一个默认值，以后用正确值来更新这个属性。
             *   需要注意的是，异步操作的结果可能是成功，也可能是失败。因为编写的是ViewModel，在出错的情况下也可以用数据绑定的方式来更新UI。
             *   可以使用AsyncEx库中的NotifyTaskCompletion类（5.1.2版本的AsyncEx库中已经没有NotifyTaskCompletion类了。）：
             * 也可以自己编写数据绑定的封装类代替AsyncEx库中的类。下面的代码介绍了基本思路：
             * 注意，这里有一个空的catch语句，其目的是明确地捕获所有的异常，并且通过数据绑定来处理那些情况。另外，这里显然不能使用ConfigureAwait(false)，
             *   因为PropertyChanged事件会在UI线程中引发。
             */
        }
        //class MyViewModel
        //{
        //    public MyViewModel()
        //    {
        //        MyValue = NotifyTaskCompletion.Create(CalculateMyValueAsync());
        //    }

        //    public INotifyTaskCompletion<int> MyValue { get; private set; }
        //    private async Task<int> CalculateMyValueAsync()
        //    {
        //        await Task.Delay(TimeSpan.FromSeconds(10));
        //        return 13;
        //    }
        //}
        class BindableTask<T> : INotifyPropertyChanged
        {
            private readonly Task<T> task;

            public BindableTask(Task<T> task)
            {
                task = task;
                var t = WatchTaskAsync();
            }

            private async Task WatchTaskAsync()
            {
                try
                {
                    await task;
                }
                catch
                {
                }

                OnPropertyChanged("IsNotCompleted");
                OnPropertyChanged("IsSuccessfullyCompleted");
                OnPropertyChanged("IsFaulted");
                OnPropertyChanged("Result");
            }

            public bool IsNotCompleted { get { return !task.IsCompleted; } }
            public bool IsSuccessfullyCompleted
            { get { return task.Status == TaskStatus.RanToCompletion; } }
            public bool IsFaulted { get { return task.IsFaulted; } }
            public T Result
            { get { return IsSuccessfullyCompleted ? task.Result : default(T); } }

            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged(string propertyName)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        // 13.4 隐式状态
        private void button4_Click(object sender, EventArgs e)
        {
            /* 程序中有一些状态变量，要求在调用栈的不同位置都可以访问。例如，在记录日志时要使用一个当前操作的标识符，但是又不希望把它作为参数添加到每一个方法中。
             * 最好的做法是在方法中增加参数，存储在类的成员变量中，或者使用依赖注入来为代码的各个部分提供数据。但是在有些情况下，这么做会使代码变得过于复杂。
             *   使用.NET中CallContext类的LogicalSetData和LogicalGetData方法，可以为一个状态命名，并把它放在一个逻辑“上下文”中。用完这个状态后，
             *   可以调用FreeNamedDataSlot把它从上下文中移除。以下代码演示了如何用这些方法来设置操作标识符，然后在日志方法中使用：
             * 可以在async方法中使用逻辑调用上下文（logical call context），但仅限于.NET 4.5和更高版本。如果在.NET 4.0和NuGet包Microsoft.Bcl.Async上使用，
             *   代码能够编译通过但不会正确地运行。
             * 在逻辑调用上下文中，应该只存储不可变数据。如果要修改逻辑调用上下文中的数据，应该重新调用LogicalSetData来覆盖已有的数据。
             * 逻辑调用上下文的效率不是特别高。我建议大家只要有可能，就在方法中添加参数或者把数据存储在类的成员中，而不是用隐式的逻辑调用上下文。
             * 在编写ASP.NET程序时可考虑使用HttpContext.Current.Items，它的功能和CallContext一样，但效率更高。
             */
        }
        void DoLongOperation()
        {
            var operationId = Guid.NewGuid();
            CallContext.LogicalSetData("OperationId", operationId);

            DoSomeStepOfOperation();

            CallContext.FreeNamedDataSlot("OperationId");
        }
        void DoSomeStepOfOperation()
        {
            // 在这里记录日志。
            Trace.WriteLine("In operation: " +
              CallContext.LogicalGetData("OperationId"));
        }
    }
}
