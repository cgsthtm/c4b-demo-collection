﻿using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1
{
    /* 开发者们意识到必须学习异步编程，他们在这个领域进行探索之后，发现异步编程经常会与已经习惯了的传统面向对象编程冲突。
     * 冲突的主要原因在于异步编程是函数式的（functional）。这里“functional”的意思并不是“具备功能”，它是一种函数式编程方式，而不是过程式编程方式。
     * 引入async对异步开发的主要突破，是在异步编程时仍然可以用过程式编程的思维方式思考。这让异步方法的编写和理解变得更加容易。但是在内部实现中，异步代码本质上仍是函数式。
     *   在经典的面向对象设计中生硬地使用async方法，就会产生一些问题。本章讲述如何应对异步代码与面向对象编程发生的冲突。
     * 在把已有的OOP（面向对象编程）基础代码转换成async风格的基础代码时，这些冲突尤为明显。
     */
    public partial class _10函数式OOP : Form
    {
        public _10函数式OOP()
        {
            InitializeComponent();
        }

        // 10.1 异步接口和继承
        private void button1_Click(object sender, EventArgs e)
        {
            /* 接口或基类中有一个方法，现在希望实现异步。
             * 理解本问题和解决方案的关键，是要知道异步是一种具体的实现方式。不可能把接口方法或抽象方法标记为async。但是可以把一个方法编写得跟async方法一样，
             *   只不过不用async这个关键字。   
             * 要知道可以用await等待的是类，而不是方法。可以用await等待某个方法返回的Task对象，不管它是不是async方法。因此，一个接口或抽象方法可以返回一个Task（或Task<T>）
             *   对象，这个对象可以用await等待。
             * 下面的代码定义了一个包含异步方法（不用async）的接口、对该接口的实现（用async），还定义了一个独立的方法，来调用该接口的方法（用await）。
             * 异步方法的特征仅仅表示它的实现可以是异步的。如果没有真正的异步任务，用同步方式实现这个方法也是可以的。例如，
             *   在测试存根的代码中可以用FromResult来实现前面的接口（不用async）：
             * 在编写本书时（2014）, async和await推出还没多久。随着异步方法越来越普遍，在接口和基类上实现的异步方法也会越来越多。这其实并不难，只要记住两点：
             *   可以用await等待的是返回的类（而不是方法）；对一个异步方法的定义，可以用异步方式实现，也可以用同步方式实现。
             */
        }
        interface IMyAsyncInterface
        {
            Task<int> CountBytesAsync(string url);
        }
        class MyAsyncClass : IMyAsyncInterface
        {
            public async Task<int> CountBytesAsync(string url)
            {
                var client = new HttpClient();
                var bytes = await client.GetByteArrayAsync(url);
                return bytes.Length;
            }

            internal Task InitializeAsync()
            {
                throw new NotImplementedException();
            }
        }
        static async Task UseMyInterfaceAsync(IMyAsyncInterface service)
        {
            var result = await service.CountBytesAsync("http://www.example.com");
            Trace.WriteLine(result);
        }

        class MyAsyncClassStub : IMyAsyncInterface
        {
            public Task<int> CountBytesAsync(string url)
            {
                return Task.FromResult(13);
            }
        }

        // 10.2 异步构造：工厂
        private async void button2_Click(object sender, EventArgs e)
        {
            /* 需要在一个类的构造函数里进行异步操作。
             * 构造函数是不能异步的，也不能使用await关键字。假如能用await等待一个构造函数，当然能解决问题，但那需要对C#语言进行很大的改动。
             *   一种可能是将构造函数与一个异步的初始化方法配对使用，就像下面这样使用类：
             * 但是这种做法有缺点。很容易忘记调用InitializeAsync方法，并且类的实例在构造完后不能马上使用。更好的解决方案，是为这个类建立自己的工厂。
             *   下面展示了异步工厂方法的模式：
             * 构造函数和InitializeAsync是private，因此其他代码不可能误用。创建实例的唯一方法是使用静态的CreateAsyncfactory工厂方法，并且在初始化完成前，
             *   调用者是不能访问这个实例的。其他代码可以这样创建一个实例：
             * 这种模式的主要好处是，其他代码无法访问尚未初始化的MyAsyncClass实例。因此我建议大家尽可能采用这种模式，而不是其他方法。可惜在有些情况下，这种方法无法使用，
             *   特别是当代码用到了依赖注入提供者的时候。在编写本书时（2014），主要的依赖注入或控制反转库都不能与异步代码一起使用。这种情况下，我们还有几个解决办法。
             */
            var instance = new MyAsyncClass();
            await instance.InitializeAsync();

            var instance1 = await MyAsyncClass1.CreateAsync();
        }
        class MyAsyncClass1
        {
            private MyAsyncClass1()
            {
            }

            private async Task<MyAsyncClass1> InitializeAsync()
            {
                await Task.Delay(TimeSpan.FromSeconds(1));
                return this;
            }

            public static Task<MyAsyncClass1> CreateAsync()
            {
                var result = new MyAsyncClass1();
                return result.InitializeAsync();
            }
        }

        // 10.3 异步构造：异步初始化模式
        private void button3_Click(object sender, EventArgs e)
        {
            /* 一个类的构造函数需要执行异步过程，但是不能使用异步工厂模式（见10.2节），因为这个类的实例是通过反射（如依赖注入/控制反转容器、数据绑定、
             *   Activator.CreateInstance等）创建的。
             * 这种情况下必须返回一个未初始化的实例，但可以使用一种通用模式来减少不利因素：异步初始化模式。对于每个需要异步初始化的类，都要定义一个属性：
             *   Task Initialization { get; }
             * 我一般为需要异步初始化的类建一个标识接口（maker interface），在标识接口内定义这个属性：
             * 实现了这种模式后，就要在构造函数内启动初始化（并赋值给这个Initialization属性）。异步初始化的结果（包括所有的异常）是通过Initialization属性对外公开的。
             *   下面的例子实现了一个使用异步初始化的类：
             * 如果使用了依赖注入/控制反转库，可以用下面的方式创建和初始化一个类的实例：
             * 可以对这种模式进行扩展，将类和异步初始化结合起来。下面的例子定义了另一个类，它以前面建立的IMyFundamentalType为基础：
             * 建议大家尽量不要使用这个解决方案，而是使用异步工厂（见10.2节）或异步Lazy对象初始化（见13.1节）。那些才是最好的方法，因为它们绝不可能暴露未初始化的实例。
             *   但是，如果用依赖注入/控制反转、数据绑定方式等创建实例，那就不可避免地要暴露未初始化的实例。这种情况下，就推荐使用本节讲述的异步初始化模式。
             * 检查某个实例是否实现了IAsyncInitialization，并对它做初始化，这个过程的代码非常臃肿，尤其是这个组合类有很多部件的情况。
             *   有一个很容易的办法可以简化代码，就是创建一个辅助方法：
             * 可以调用WhenAllInitializedAsync并传入需要初始化的任何实例。这个方法会忽略那些未实现IAsyncInitialization的实例。如果一个组合类依赖了三个注入的实例，
             *   它的初始化代码就可以这么写：
             */
            //IMyFundamentalType instance = UltimateDIFactory.Create<IMyFundamentalType>();
            //var instanceAsyncInit = instance as IAsyncInitialization;
            //if (instanceAsyncInit != null)
            //    await instanceAsyncInit.Initialization;
        }
        /// <summary>
        /// 把一个类标记为“需要异步初始化”
        /// 并提供初始化的结果。
        /// </summary>
        public interface IAsyncInitialization
        {
            /// <summary>
            /// 本实例的异步初始化的结果。
            /// </summary>
            Task Initialization { get; }
        }
        //class MyFundamentalType : IMyFundamentalType, IAsyncInitialization
        //{
        //    public MyFundamentalType()
        //    {
        //        Initialization = InitializeAsync();
        //    }

        //    public Task Initialization { get; private set; }

        //    private async Task InitializeAsync()
        //    {
        //        // 对这个实例进行异步初始化。
        //        await Task.Delay(TimeSpan.FromSeconds(1));
        //    }
        //}
        //class MyComposedType : IMyComposedType, IAsyncInitialization
        //{
        //    private readonly IMyFundamentalType fundamental;

        //    public MyComposedType(IMyFundamentalType fundamental)
        //    {
        //        fundamental = fundamental;
        //        Initialization = InitializeAsync();
        //    }

        //    public Task Initialization { get; private set; }

        //    private async Task InitializeAsync()
        //    {
        //        // 如有必要，异步地等待基础实例的初始化。
        //        var fundamentalAsyncInit = fundamental as IAsyncInitialization;
        //        if (fundamentalAsyncInit! = null)
        //            await fundamentalAsyncInit.Initialization;

        //        // 做自己的初始化工作（同步或异步）。
        //        ...
        //    }
        //}
        public static class AsyncInitialization
        {
            static Task WhenAllInitializedAsync(params object[] instances)
            {
                return Task.WhenAll(instances
                    .OfType<IAsyncInitialization>()
                    .Select(x => x.Initialization));
            }
        }
        //private async Task InitializeAsync()
        //{
        //    // 异步地等待三个实例全部初始化完毕（有些可能不需要初始化）。
        //    await AsyncInitialization.WhenAllInitializedAsync(fundamental,
        //        anotherType, yetAnother);

        //    // 做自己的初始化工作（同步或异步）。
        //    ...
        //}

        // 10.4 异步属性
        private void button4_Click(object sender, EventArgs e)
        {
            /* 在使用async改造原有代码时，经常会出现这样的问题。这时你会在属性的get方法中调用一个异步方法。但实际上根本没有“异步属性”这种东西。
             *   不允许在属性中使用async关键字，这么规定是有好处的。属性的get方法应该返回当前值，不应该启动一个后台的操作：
             * 当你发现需要在代码中加入一个“异步属性”时，实际上应该选用其他方案。选用哪种解决方案取决于要对属性值计算一次还是多次。需要确定是下面两种情况中的哪一种：
             *   · 每次读取属性时，都要对值进行异步计算；
             *   · 异步地计算属性值一次，并缓存起来供以后访问。
             * 如果每次读取“异步属性”时都要启动一次新的（异步的）计算过程，那说明它不是一个属性。它实际上是一个经过伪装的方法。如果你在把同步代码转换成异步代码的时候
             *   遇到这种情况，那就要意识到原始的设计就是错误的。实际上这个属性从一开始就应该是一个方法：
             * 属性也可以直接返回一个Task<int>，例如：
             * 但是我建议大家不要采用这种做法。如果每次访问属性都会启动一次新的异步操作，那说明这个“属性”其实应该是一个方法。当它是异步的方法，
             *   人们就会更清楚地知道每次访问都会开始新的异步操作，因此这个API就不会误导别人。
             * 另一种情况是“异步属性”只启动一次（异步）计算，并缓存计算结果供以后使用。针对这种情况，可以使用异步的Lazy对象初始化。这方面将在13.1节详述，现在先来看一下代码：
             * 这段代码只会异步地计算一次，然后每次向调用者返回同一个值。调用的代码如下：int value = await instance.Data1;
             * 有一个重要的问题需要明确：读取属性时是否要启动一个新的异步操作。如果答案为“是”，那就不要用属性，改用异步方法。如果属性要充当一个惰性求值（lazy-evaluated）
             *   的缓存，那就使用异步初始化（见13.1节）。
             */
        }
        // 我们假想的代码（无法编译）。
        //public int Data
        //{
        //    async get
        //    {
        //        await Task.Delay(TimeSpan.FromSeconds(1));
        //        return 13;
        //    }
        //}
        // 作为一个异步方法。
        public async Task<int> GetDataAsync()
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            return 13;
        }
        // 作为一个返回Task的属性。
        // 这个API设计是有问题的。
        public Task<int> Data
        {
            get { return GetDataAsync1(); }
        }
        private async Task<int> GetDataAsync1()
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            return 13;
        }
        // 作为一个缓存的数据。
        public AsyncLazy<int> Data1
        {
            get { return data; }
        }

        private readonly AsyncLazy<int> data =
            new AsyncLazy<int>(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(1));
                return 13;
            });

        // 10.5 异步事件
        private void button5_Click(object sender, EventArgs e)
        {
            /* 需要一个可以异步运行的事件处理器，并且需要监测事件处理器是否已经完成。注意这种情况非常少见。一般来说，提出一个事件后就不再关心事件处理器何时完成。
             * 监测async void类型的事件处理器什么时候返回是根本不可行的，因此需要采用其他办法。Windows应用商店平台引入了一个名为延期（deferral）的概念，
             *   可用来跟踪异步事件处理器。异步事件处理器在第一次使用await前分配一个延期对象，处理结束时通知这个延期对象。同步的事件处理器不需要使用延期。
             * Nito.AsyncEx库中有一个DeferralManager类（延期管理器），引发事件的组件可使用它。事件处理器可以利用这个延期管理器来分配延期对象，并且跟踪每个延期对象的完成时间。
             *   对每个需要等待处理完毕的事件，首先要扩展事件参数类：
             * 在编写异步事件处理器时，事件参数类最好是线程安全的。要做到这点，最简单的办法就是让它成为不可变的（即把所有的属性都设为只读）。
             * 接着，就可以在每次引发事件后（异步地）等待，直到所有异步事件处理器完成。如果没有事件处理器，下面的代码会返回一个已完成的Task对象；
             *   否则会创建一个事件参数类的新实例，并传入事件处理器，然后等待任意异步事件处理器的完成：
             * 然后异步事件处理器可以在using块中使用延期对象，该延期对象会在被销毁时通知延期管理器：
             */
        }
        //public class MyEventArgs : EventArgs
        //{
        //    private readonly DeferralManager deferrals = new DeferralManager();

        //    ... // 自身的构造函数和属性。

        //    public IDisposable GetDeferral()
        //    {
        //        return deferrals.GetDeferral();
        //    }

        //    internal Task WaitForDeferralsAsync()
        //    {
        //        return deferrals.SignalAndWaitAsync();
        //    }
        //}
        //public event EventHandler<MyEventArgs> MyEvent;

        //private Task RaiseMyEventAsync()
        //{
        //    var handler = MyEvent;
        //    if (handler == null)
        //        return Task.FromResult(0);

        //    var args = new MyEventArgs(...);
        //    handler(this, args);
        //    return args.WaitForDeferralsAsync();
        //}
        //async void AsyncHandler(object sender, MyEventArgs args)
        //{
        //    using (args.GetDeferral())
        //    {
        //        await Task.Delay(TimeSpan.FromSeconds(2));
        //    }
        //}

        // 10.6 异步销毁
        private async void button6_Click(object sender, EventArgs e)
        {
            /* 在销毁一个实例时，有几种方法来处理正在执行的操作：可以把销毁看做是一个针对所有正在运行的操作的取消请求，或者实现一个真正的异步完成。
             * 把销毁看成一个取消请求是Windows平台上的惯例。文件流和套接字在关闭时，会取消所有运行中的读/写过程。在.NET环境下也可采用类似的做法，
             *   定义一个私有的CancellationTokenSource对象，并把取消标记传递给内部的操作。用下面的代码，Dispose会取消这些操作，但不会等待操作的完成：
             * 前面的代码展示了有关Dispose的基本模式。在实际开发中应该增加一项检查，以确认对象还没有被销毁，还要支持方法本身的CancellationToken（使用9.8节的技术）：
             * 当Dispose被调用时，调用程序中所有运行中的操作都会被取消：
             * 在Dispose的实现中生成一个取消请求，这种方法对于有些类运行得很好（例如Http-Client就有这种语义）。但是其他一些类需要知道操作完成的时间。
             *   对这些类就需要采用实现“异步完成”的方式了。
             * 异步完成与异步初始化（见10.3节）很相似：它们都很少有官方的指引资料。因此本书介绍一种可行的模式，它基于TPL数据流块的运行方式。
             *   异步完成的重要部分可以封装在一个接口中：
             * 调用它的代码看起来不那么漂亮，Dispose必须是异步的，因此不能使用using语句。但是我们可以定义一对辅助方法，完成类似using语句的功能：
             * 代码中使用了ExceptionDispatchInfo，以保留异常的栈轨迹。准备好这些辅助方法后，调用的代码就可以这样使用Using方法了：
             * 跟用Dispose实现取消请求的方式相比，异步完成的方式显然要麻烦得多，只有在确实需要时才能使用这种方法。其实在大多数情况下是不需要销毁任何东西的，
             *   这当然是最简单的方法，因为什么都不需要做。
             * 本节介绍了两种处理销毁过程的模式，这两种模式也可以同时使用。一个类同时使用这两种模式后，当客户端代码使用Complete和Completion，这个类就会正常地关闭；
             *   当客户端代码使用Dispose，这个类就会“取消”操作。
             */
            await AsyncHelpers.Using(() => new MyClass1(), async resource =>
            {
                // 使用资源。
            });
        }
        class MyClass : IDisposable
        {
            private readonly CancellationTokenSource disposeCts =
              new CancellationTokenSource();

            public async Task<int> CalculateValueAsync()
            {
                await Task.Delay(TimeSpan.FromSeconds(2), disposeCts.Token);
                return 13;
            }

            public void Dispose()
            {
                disposeCts.Cancel();
            }

            public async Task<int> CalculateValueAsync(CancellationToken cancellationToken)
            {
                using (var combinedCts = CancellationTokenSource
                  .CreateLinkedTokenSource(cancellationToken, disposeCts.Token))
                {
                    await Task.Delay(TimeSpan.FromSeconds(2), combinedCts.Token);
                    return 13;
                }
            }
            async Task Test()
            {
                Task<int> task;
                using (var resource = new MyClass())
                {
                    task = resource.CalculateValueAsync();
                }

                // 抛出异常OperationCanceledException.
                var result = await task;
            }
        }
        /// <summary>
        /// 表明一个类需要异步完成，并提供完成的结果。
        /// </summary>
        interface IAsyncCompletion
        {
            /// <summary>
            /// 开始本实例的完成过程。概念上类似于“IDisposable.Dispose”。
            /// 在调用本方法后，就不能调用除了“Completion”以外的任何成员。
            /// </summary>
            void Complete();

            /// <summary>
            /// 取得本实例完成的结果。
            /// </summary>
            Task Completion { get; }
        }
        class MyClass1 : IAsyncCompletion
        {
            private readonly TaskCompletionSource<object> completion =
              new TaskCompletionSource<object>();
            private Task completing;

            public Task Completion
            {
                get { return completion.Task; }
            }
            public void Complete()
            {
                if (completing != null)
                    return;
                completing = CompleteAsync();
            }

            private async Task CompleteAsync()
            {
                try
                {
                    //... // 异步地等待任何运行中的操作。
                }
                catch (Exception ex)
                {
                    completion.TrySetException(ex);
                }
                finally
                {
                    completion.TrySetResult(null);
                }
            }
        }
        static class AsyncHelpers
        {
            public static async Task Using<TResource>(Func<TResource> construct,
              Func<TResource, Task> process) where TResource : IAsyncCompletion
            {
                // 创建需要使用的资源。
                var resource = construct();

                // 使用资源，并捕获所有异常。
                Exception exception = null;
                try
                {
                    await process(resource);
                }
                catch (Exception ex)
                {
                    exception = ex;
                }

                // 完成（逻辑上销毁）资源。
                resource.Complete();
                await resource.Completion;

                // 如果需要，就重新抛出“process”产生的异常。
                if (exception != null)
                    ExceptionDispatchInfo.Capture(exception).Throw();
            }
            public static async Task<TResult> Using<TResource, TResult>(
                Func<TResource> construct, Func<TResource,
                Task<TResult>> process) where TResource : IAsyncCompletion
            {
                // 创建需要使用的资源。
                var resource = construct();

                // 使用资源，并捕获所有异常。
                Exception exception = null;
                TResult result = default(TResult);
                try
                {
                    result = await process(resource);
                }
                catch (Exception ex)
                {
                    exception = ex;
                }

                // 完成（逻辑上销毁）资源。
                resource.Complete();
                try
                {
                    await resource.Completion;
                }
                catch
                {
                    // 只有当“process”没有抛出异常时，才允许抛出“Completion”的异常。
                    if (exception == null)
                        throw;
                }

                // 如果需要，就重新抛出“process”产生的异常。
                if (exception != null)
                    ExceptionDispatchInfo.Capture(exception).Throw();

                return result;
            }
        }
    }
}
