﻿using Nito.AsyncEx;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1
{
    /* 如果程序用到了并发技术（几乎所有.NET程序都用了），那就要特别留意这种情况：一段代码需要修改数据，同时其他代码需要访问同一个数据。这种情况出现时，就需要同步地访问数据。
     * 同步的类型主要有两种：通信和数据保护。当一段代码把某些情况（例如收到新消息）通知给另一段代码时，就得用到通信。在后面的有关案例中会详细讲述通信。
     *   本章的概述部分主要讨论数据保护。
     * 如果下面三个条件都满足，就需要用同步来保护共享的数据。· 多段代码正在并发运行。· 这几段代码在访问（读或写）同一个数据。· 至少有一段代码在修改（写）数据。
     * 如果每段代码都有自己非共享的局部数据，那就不需要同步。局部数据是独立于其他代码的。如果有共享数据，但数据永远不会修改的话，那也没必要使用同步。
     * 如果共享的数据只是用来读取的，就不需要同步。
     * 数据保护是为了每段代码访问数据时能得到一致的结果。一段代码正在修改数据时要使用同步技术，以保证在系统的其他部分看来这些修改具有原子性。
     * 只有经过实践才会知道什么时候需要同步，因此在开始讨论具体方法之前，我们先看几个例子。这是第一个例子：
     *  async Task MyMethodAsync()
        {
            int val = 10;
            await Task.Delay(TimeSpan.FromSeconds(1));
            val = val + 1;
            await Task.Delay(TimeSpan.FromSeconds(1));
            val = val -1;
            await Task.Delay(TimeSpan.FromSeconds(1));
            Trace.WriteLine(val);
        }
     * 如果从一个线程池线程调用这个方法（例如在Task.Run中运行），访问val的代码行会在独立的线程池线程中运行。但它需要同步吗？不，因为这些代码行不会同时运行。
     *   这个方法是异步的，但是它也是按顺序运行的（一次只会处理一部分）。
     * 好，我们把例子改复杂一些。这次来运行并发异步的代码：
     *  class SharedData
        {
            public int Value { get; set; }
        }

        async Task ModifyValueAsync(SharedData data)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            data.Value = data.Value + 1;
        }

        // 警告：可能需要同步，见下面的讨论。
        async Task<int> ModifyValueConcurrentlyAsync()
        {
            var data = new SharedData();

            // 启动三个并发的修改过程。
            var task1 = ModifyValueAsync(data);
            var task2 = ModifyValueAsync(data);
            var task3 = ModifyValueAsync(data);

            await Task.WhenAll(task1, task2, task3);
            return data.Value;
        }
     * 本例中，启动了三个并发运行的修改过程。需要同步吗？答案是“看情况”。如果能确定这个方法是在GUI或ASP.NET上下文中调用的（或同一时间内只允许一段代码运行的任何其他上下文），
     *   那就不需要同步，因为这三个修改数据过程的运行时间是互不相同的。例如，如果它在GUI上下文中运行，就只有一个UI线程可以运行这些数据修改过程，
     *   因此一段时间内只能运行一个过程。因此，如果能够确定是“同一时间只运行一段代码”的上下文，那就不需要同步。但是如果从线程池线程（如Task.Run）调用这个方法，就需要同步了。
     *   在那种情况下，这三个数据修改过程会在独立的线程池线程中运行，并且同时修改data.Value，因此必须同步地访问data.Value。
     * 现在我们把数据改为私有成员，代替需要传递的变量，并且来看一个新的技巧：
     *  private int value;

        async Task ModifyValueAsync()
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            value = value + 1;
        }

        // 警告：可能需要同步，见下面的讨论。
        async Task<int> ModifyValueConcurrentlyAsync()
        {
            // 启动三个并发的修改过程。
            var task1 = ModifyValueAsync();
            var task2 = ModifyValueAsync();
            var task3 = ModifyValueAsync();

            await Task.WhenAll(task1, task2, task3);

            return value;
        }
     * 这里有一个技巧。前一个例子创建了在三个修改方法中共享的SharedData实例。这个例子则用一个具体的私有成员作为共享数据。这意味着如果ModifyValueConcurrentlyAsync被多次调用，
     *   每个独立的调用都会共享这个value。如果要避免这种共享，即使在“同一时间只运行一段代码”上下文中，也需要使用同步。换句话说，
     *   如果要在每次调用ModifyValueConcurrentlyAsync之前等待前面的调用完成，那就需要加入同步。即使上下文能够确保只有一个线程来运行所有代码（即UI线程），也是如此。
     *   这种情况下的同步，实际上是对异步方法的一种限流（见11.2节）。
     * 注意，即使val是一个局部变量，这段代码也需要同步。虽然它是一个方法内的局部变量，但仍被多个线程共享:
     *  // 坏代码！！
        async Task<int> SimpleParallelismAsync()
        {
            int val = 0;
            var task1 = Task.Run(() => { val = val + 1; });
            var task2 = Task.Run(() => { val = val + 1; });
            var task3 = Task.Run(() => { val = val + 1; });
            await Task.WhenAll(task1, task2, task3);
            return val;
        }
     * 我们来看一个聚合的例子，与3.2节中的一个例子类似：
     *  // 坏代码！！
        int ParallelSum(IEnumerable<int> values)
        {
            int result = 0;
            Parallel.ForEach(source: values,
              localInit: () => 0,
              body: (item, state, localValue) => localValue + item,
              localFinally: localValue => { result += localValue; });
            return result;
        }
     * 这个例子也使用了多线程，这次每个线程在启动时把局部变量初始化为0（() =>0）。线程对每个输入值的处理，是把输入值累加到它的局部变量
     *   （(item, state,localValue) =>localValue + item）。最后所有的局部变量被累加到返回值（localValue=> { result +=localValue; }）。
     *   前面两步没有问题，因为线程间还没有共享数据。局部变量和输入数据在各个线程之间是互相独立的。但是最后一个步骤就有问题了。当每个线程都把它的局部变量累加到返回值时，
     *   就出现了多个线程访问并修改同一个共享变量（result）的情况。因此最后一个步骤需要同步（见11.1节）。
     * PLINQ、数据流、响应式编程库的情况与Parallel非常类似：只要代码只处理它自己的输入数据，就不需要考虑同步问题。
     * 记住需要同步的三个条件是：多段代码、共享数据、修改数据。
     * 不可变类型本身就是线程安全的，因为它们是不会改变的。修改一个不可变集合是不可能的，因此根本不需要同步。例如下面的代码不需要同步，因为每个独立的线程池线程向栈压入一个值时，
     *   实际上是用这个值创建了一个新的不可变栈，而原始栈保持不变：
     *  async Task<bool> PlayWithStackAsync()
        {
            var stack = ImmutableStack<int>.Empty;

            var task1 = Task.Run(() => Trace.WriteLine(stack.Push(3).Peek()));
            var task2 = Task.Run(() => Trace.WriteLine(stack.Push(5).Peek()));
            var task3 = Task.Run(() => Trace.WriteLine(stack.Push(7).Peek()));
            await Task.WhenAll(task1, task2, task3);

            return stack.IsEmpty; // 总是返回true。
        }
     * 但是，在使用不可变集合时通常会有一个共享的“根”变量，它本身不是不可变的。这时就必须使用同步了。下面的代码中每个线程向栈压入一个值（同时创建一个新的不可变栈），
     *   然后修改这个共享的根变量。在这个例子中，修改这个栈变量时是需要使用同步的：
     *  // 坏代码！！
        async Task<bool> PlayWithStackAsync()
        {
            var stack = ImmutableStack<int>.Empty;

            var task1 = Task.Run(() => { stack = stack.Push(3); });
            var task2 = Task.Run(() => { stack = stack.Push(5); });
            var task3 = Task.Run(() => { stack = stack.Push(7); });
            await Task.WhenAll(task1, task2, task3);

            return stack.IsEmpty;
        }
     * 线程安全集合（例如ConcurrentDictionary）就完全不同了。与不可变集合不同，线程安全集合是可以修改的。线程安全集合本身就包含了所有的同步功能，因此根本不需要担心同步的问题。
     *   下面的代码，如果修改的是Dictionary而不是ConcurrentDictionary，那就需要同步。但事实上它是在修改ConcurrentDictionary，因此就不需要同步：
     *  async Task<int> ThreadsafeCollectionsAsync()
        {
            var dictionary = new ConcurrentDictionary<int, int>();

            var task1 = Task.Run(() => { dictionary.TryAdd(2, 3); });
            var task2 = Task.Run(() => { dictionary.TryAdd(3, 5); });
            var task3 = Task.Run(() => { dictionary.TryAdd(5, 7); });
            await Task.WhenAll(task1, task2, task3);

            return dictionary.Count; // 总是返回3。
        }
     */
    public partial class _11同步 : Form
    {
        public _11同步()
        {
            InitializeComponent();
        }

        async Task MyMethodAsync()
        {
            int val = 10;
            await Task.Delay(TimeSpan.FromSeconds(1));
            val = val + 1;
            await Task.Delay(TimeSpan.FromSeconds(1));
            val = val - 1;
            await Task.Delay(TimeSpan.FromSeconds(1));
            Trace.WriteLine(val);
        }

        class SharedData
        {
            public int Value { get; set; }
        }
        async Task ModifyValueAsync(SharedData data)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            data.Value = data.Value + 1;
        }
        // 警告：可能需要同步，见下面的讨论。
        async Task<int> ModifyValueConcurrentlyAsync()
        {
            var data = new SharedData();

            // 启动三个并发的修改过程。
            var task1 = ModifyValueAsync(data);
            var task2 = ModifyValueAsync(data);
            var task3 = ModifyValueAsync(data);

            await Task.WhenAll(task1, task2, task3);
            return data.Value;
        }
        private int value;
        async Task ModifyValueAsync()
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            value = value + 1;
        }
        // 警告：可能需要同步，见下面的讨论。
        async Task<int> ModifyValueConcurrentlyAsync1()
        {
            // 启动三个并发的修改过程。
            var task1 = ModifyValueAsync();
            var task2 = ModifyValueAsync();
            var task3 = ModifyValueAsync();

            await Task.WhenAll(task1, task2, task3);

            return value;
        }
        // 坏代码！！
        async Task<int> SimpleParallelismAsync()
        {
            int val = 0;
            var task1 = Task.Run(() => { val = val + 1; });
            var task2 = Task.Run(() => { val = val + 1; });
            var task3 = Task.Run(() => { val = val + 1; });
            await Task.WhenAll(task1, task2, task3);
            return val;
        }
        // 坏代码！！
        int ParallelSum(IEnumerable<int> values)
        {
            int result = 0;
            Parallel.ForEach(source: values,
              localInit: () => 0,
              body: (item, state, localValue) => localValue + item,
              localFinally: localValue => { result += localValue; });
            return result;
        }
        async Task<bool> PlayWithStackAsync()
        {
            var stack = ImmutableStack<int>.Empty;

            var task1 = Task.Run(() => Trace.WriteLine(stack.Push(3).Peek()));
            var task2 = Task.Run(() => Trace.WriteLine(stack.Push(5).Peek()));
            var task3 = Task.Run(() => Trace.WriteLine(stack.Push(7).Peek()));
            await Task.WhenAll(task1, task2, task3);

            return stack.IsEmpty; // 总是返回true。
        }
        // 坏代码！！
        async Task<bool> PlayWithStackAsync1()
        {
            var stack = ImmutableStack<int>.Empty;

            var task1 = Task.Run(() => { stack = stack.Push(3); });
            var task2 = Task.Run(() => { stack = stack.Push(5); });
            var task3 = Task.Run(() => { stack = stack.Push(7); });
            await Task.WhenAll(task1, task2, task3);

            return stack.IsEmpty;
        }
        async Task<int> ThreadsafeCollectionsAsync()
        {
            var dictionary = new ConcurrentDictionary<int, int>();

            var task1 = Task.Run(() => { dictionary.TryAdd(2, 3); });
            var task2 = Task.Run(() => { dictionary.TryAdd(3, 5); });
            var task3 = Task.Run(() => { dictionary.TryAdd(5, 7); });
            await Task.WhenAll(task1, task2, task3);

            return dictionary.Count; // 总是返回3。
        }

        // 11.1 阻塞锁
        private void button1_Click(object sender, EventArgs e)
        {
            /* 多个线程需要安全地读写共享数据。
             * 这种情况最好的办法是使用lock语句。一个线程进入锁后，在锁被释放之前其他线程是无法进入的：
             * .NET框架中还有很多其他类型的锁，如Monitor、SpinLock、ReaderWriterLockSlim。对大多数程序来说，这些类型的锁基本上用不到。
             *   尤其是程序员会习惯性地使用ReaderWriterLockSlim，即使没必要用那么复杂的技术。基本的lock语句就可以很好地处理99%的情况了。
             * 千万不要用lock(this)，也不要锁定Type或string类型的实例。因为这些对象是可以被其他代码访问的，这样锁定会产生死锁。
             * 在锁定时执行的代码要尽可能得少。要特别小心阻塞调用。在锁定时不要做任何阻塞操作。
             * 在锁定时绝不要调用随意的代码。随意的代码包括引发事件、调用虚拟方法、调用委托。如果一定要运行随意的代码，就在释放锁之后运行。
             */
        }
        class MyClass
        {
            // 这个锁会保护  value。
            private readonly object mutex = new object();

            private int value;

            public void Increment()
            {
                lock (mutex)
                {
                    value = value + 1;
                }
            }
        }

        // 11.2 异步锁
        private void button2_Click(object sender, EventArgs e)
        {
            /* 多个代码块需要安全地读写共享数据，并且这些代码块可能使用await语句。
             * .NET 4.5对框架中的SemaphoreSlim类进行了升级以兼容async。可以这样使用：
             * 不过，只有在.NET 4.5或更高版本中才能以这种方式使用SemaphoreSlim。如果使用的是旧版本框架或者是在编写可移植类库，那可以使用Nito.AsyncEx库中的AsyncLock类：
             */
        }
        class MyClass1
        {
            // 这个锁保护  value。
            private readonly SemaphoreSlim mutex = new SemaphoreSlim(1);
            private int value;

            public async Task DelayAndIncrementAsync()
            {
                await mutex.WaitAsync();
                try
                {
                    var oldValue = value;
                    await Task.Delay(TimeSpan.FromSeconds(oldValue));
                    value = oldValue + 1;
                }
                finally
                {
                    mutex.Release();
                }
            }
        }
        class MyClass2
        {
            // 这个锁保护  value。
            private readonly AsyncLock mutex = new AsyncLock();

            private int value;

            public async Task DelayAndIncrementAsync()
            {
                using (await mutex.LockAsync())
                {
                    var oldValue = value;
                    await Task.Delay(TimeSpan.FromSeconds(oldValue));
                    value = oldValue + 1;
                }
            }
        }

        // 11.3 阻塞信号
        private void button3_Click(object sender, EventArgs e)
        {
            /* 需要从一个线程发送信号给另一个线程。
             * 最常见和通用的跨线程信号是ManualResetEventSlim。一个人工重置的事件处于这两种状态其中之一：标记的（signaled）或未标记的（unsignaled）。
             *   每个线程都可以把事件设置为signaled状态，也可以把它重置为unsignaled状态。线程也可等待事件变为signaled状态。下面的两个方法被两个独立的线程调用，
             *   一个线程等待另一个线程的信号：
             * ManualResetEventSlim是功能强大、通用的线程间信号，但必须合理地使用。如果这个信号其实是一个线程间发送小块数据的消息，那可考虑使用生产者/消费者队列。
             *   另一方面，如果信号只是用来协调对共享数据的访问，那可改用锁。
             * 在.NET框架中，还有一些不常用的线程同步信号类型。如果ManualResetEventSlim不能满足需求，还可考虑用AutoResetEvent、CountdownEvent或Barrier。
             */
        }
        class MyClass3
        {
            private readonly ManualResetEventSlim initialized =
                new ManualResetEventSlim();

            private int value;

            public int WaitForInitialization()
            {
                initialized.Wait();
                return value;
            }

            public void InitializeFromAnotherThread()
            {
                value = 13;
                initialized.Set();
            }
        }

        // 11.4 异步信号
        private void button4_Click(object sender, EventArgs e)
        {
            /* 需要在代码的各个部分间发送通知，并且要求接收方必须进行异步等待。
             * 如果该通知只需要发送一次，那可用TaskCompletionSource<T>异步发送。发送代码调用TrySetResult，接收代码等待它的Task属性：
             * 在所有情况下都可以用TaskCompletionSource<T>来异步地等待：本例中，通知来自于另一部分代码。如果只需要发送一次信号，这种方法很适合。
             *   但是如果要打开和关闭信号，这种方法就不大合适了。
             * Nito.AsyncEx库中有一个AsyncManualResetEvent类，基本上相当于是异步的ManualReset Event。下面是一个虚拟的例子，但说明了如何使用AsyncManualResetEvent类：
             * 信号是一种通用的通知机制。但如果这个“信号”是一个用来在代码段之间发送数据的消息，那就考虑使用生产者/消费者队列。
             *   同样，不要让通用的信号只是用来协调对共享数据的访问。那种情况下，可使用锁。
             */
        }
        class MyClass4
        {
            private readonly TaskCompletionSource<object> initialized =
              new TaskCompletionSource<object>();

            private int value1;
            private int value2;

            public async Task<int> WaitForInitializationAsync()
            {
                await initialized.Task;
                return value1 + value2;
            }

            public void Initialize()
            {
                value1 = 13;
                value2 = 17;
                initialized.TrySetResult(null);
            }
        }
        class MyClass5
        {
            private readonly AsyncManualResetEvent connected =
              new AsyncManualResetEvent();

            public async Task WaitForConnectedAsync()
            {
                await connected.WaitAsync();
            }

            public void ConnectedChanged(bool connected_)
            {
                if (connected_)
                    connected.Set();
                else
                    connected.Reset();
            }
        }

        // 11.5 限流
        private void button5_Click(object sender, EventArgs e)
        {
            /* 有一段高度并发的代码，由于它的并发程度实在太高了，需要有方法对并发性进行限流。代码并发程度太高，是指程序中的一部分无法跟上另一部分的速度，
             *   导致数据项累积并消耗内存。这种情况下对部分代码进行限流，可以避免占用太多内存。
             * 根据代码的并发类型，解决方法各有不同。这些解决方案都是把并发性限制在某个范围之内。响应式扩展有更多功能强大的方法可以选择，例如滑动时间窗口。
             *   5.4节对Rx限流有更完整的介绍。数据流和并行代码都自带了对并发性限流的方法：
             * 并发性异步代码可以用SemaphoreSlim来限流：
             * 如果发现程序使用的资源（例如CPU或网络连接）太多，说明可能需要使用限流了。需要牢记一点，最终用户的电脑性能可能不如开发者的电脑，
             *   因此限流得稍微严格一点，比限流不充分要好。
             */
        }
        IPropagatorBlock<int, int> DataflowMultiplyBy2()
        {
            var options = new ExecutionDataflowBlockOptions
            {
                MaxDegreeOfParallelism = 10
            };

            return new TransformBlock<int, int>(data => data * 2, options);
        }
        // 使用PLINQ
        IEnumerable<int> ParallelMultiplyBy2(IEnumerable<int> values)
        {
            return values.AsParallel()
              .WithDegreeOfParallelism(10)
              .Select(item => item * 2);
        }
        // 使用Parallel类
        void ParallelRotateMatrices(IEnumerable<Matrix> matrices, float degrees)
        {
            var options = new ParallelOptions
            {
                MaxDegreeOfParallelism = 10
            };
            Parallel.ForEach(matrices, options, matrix => matrix.Rotate(degrees));
        }
        async Task<string[]> DownloadUrlsAsync(IEnumerable<string> urls)
        {
            var httpClient = new HttpClient();
            var semaphore = new SemaphoreSlim(10);
            var tasks = urls.Select(async url =>
            {
                await semaphore.WaitAsync();
                try
                {
                    return await httpClient.GetStringAsync(url);
                }
                finally
                {
                    semaphore.Release();
                }
            }).ToArray();
            return await Task.WhenAll(tasks);
        }
    }
}
