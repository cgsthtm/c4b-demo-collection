﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1
{
    /* 代码必须在某个线程上运行。调度器（scheduler）是一个确定代码运行地点的对象。.NET框架中有几种不同的调度器类型，并行和数据流代码也使用调度器，但方式有些细微的区别。
     * 我建议大家尽量不要指定调度器，系统默认的调度器通常是最合适的。例如，异步代码中的await操作符会自动选择在当前上下文中恢复运行，
     *   除非用2.7节中描述的方法覆盖默认选项。类似地，响应式代码引发事件时用的默认上下文是很合理的，可以如5.2节描述的那样，用ObserveOn覆盖默认选项。
     * 但是要让其他代码在指定的上下文（如UI线程上下文或ASP.NET请求上下文）中运行，就可以使用本章的调度技巧来调度代码。
    */
    public partial class _12调度 : Form
    {
        public _12调度()
        {
            InitializeComponent();
        }

        // 12.1 调度到线程池
        private void button1_Click(object sender, EventArgs e)
        {
            /* 指定一段代码在线程池线程中执行。
             * 绝大多数情况下可以使用Task.Run，它用起来很简单。下面的代码阻塞一个线程池线程2秒钟：
             * 如果在UI程序中有很耗时的任务，但不能在UI线程中执行该任务，这时使用Task.Run就非常合适。例如，7.4节中使用Task.Run把并行处理任务放到线程池线程。
             *   但不要在ASP. NET中使用Task.Run，除非你有绝对的把握。在ASP.NET中，处理请求的代码本来就是在线程池线程中运行的，强行把它放到另一个线程池线程通常会适得其反。
             * Task.Run完全可以替代BackgroundWorker、Delegate.BeginInvoke和ThreadPool.QueueUser WorkItem。新写的代码都不要使用这些过时的技术，
             *   使用Task.Run的代码更利于正确编写和日后的维护。而且Task.Run能处理绝大多数使用Thread类的场景，大多数情况下可以用Task.Run来代替Thread类（有极少数例外情况，如单线程单元线程）。
             * 并行和数据流代码默认在线程池中执行，因此Parallel、Parallel LINQ或TPL数据流库的代码通常不需要使用Task.Run。
             * 在进行动态并行开发时，一定要用Task.Factory.StartNew来代替Task.Run。因为根据默认配置，Task.Run返回的Task对象适合被异步调用（即被异步代码或响应式代码使用）。
             *   Task.Run也不支持动态并行代码中普遍使用的高级概念，例如父/子任务。
             */
            Task task = Task.Run(() =>
            {
                Thread.Sleep(TimeSpan.FromSeconds(2));
            }); 
            //Task.Run也能正常地返回结果，能使用异步Lambda表达式。下面代码中Task.Run返回的task会在2秒后完成，并返回结果13：
            Task<int> task1 = Task.Run(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(2));
                return 13;
            });
        }

        // 12.2 任务调度器
        private void button2_Click(object sender, EventArgs e)
        {
            /* 需要让多个代码段按照指定的方式运行。例如让所有代码段在UI线程中运行，或者只允许特定数量的代码段同时运行。本节介绍如何定义和构造这些代码段的调度器。
             *   后面两节介绍如何应用调度器。
             * .NET中有很多不同的类可以进行任务调度。本节重点讲TaskScheduler，因为它便于移植，使用起来也相对容易。最简单的TaskScheduler对象是TaskScheduler.Default，
             *   它的作用是让任务在线程池中排队。在代码中很少会指定TaskScheduler.Default，但是要特别注意这个问题，因为它是调度时最常用的默认值。
             *   Task.Run、并行、数据流的代码用的都是TaskScheduler.Default。
             * 可以捕获一个特定的上下文，然后用TaskScheduler.FromCurrentSynchronizationContext调度任务，让它回到该上下文：
             * 这条语句创建了一个捕获当前SynchronizationContext的TaskScheduler对象，并将代码调度到这个上下文中。SynchronizationContext类表示一个通用的调度上下文。
             *   .NET中有几个不同的上下文，大多数UI框架有一个表示UI线程的SynchronizationContext, ASP. NET有一个表示HTTP请求上下文的SynchronizationContext。
             * .NET 4.5引入了另一个功能强大的类，即ConcurrentExclusiveSchedulerPair，它实际上是两个互相关联的调度器。只要ExclusiveScheduler上没有运行任务，
             *   ConcurrentScheduler就可以让多个任务同时执行。只有当ConcurrentScheduler没有执行任务时，Exclusive Scheduler才可以执行任务，并且每次只允许运行一个任务：
             * ConcurrentExclusiveSchedulerPair的常见用法是用ExclusiveScheduler来确保每次只运行一个任务。ExclusiveScheduler执行的代码会在线程池中运行，
             *   但是使用了同一个ExclusiveScheduler对象的其他代码不能同时运行。
             * ConcurrentExclusiveSchedulerPair的另一个用法是作为限流调度器。创建的ConcurrentExclusiveSchedulerPair对象可以限制自身的并发数量。
             *   这时通常不使用ExclusiveScheduler：
             * 注意，这种限流方式只是对运行中的代码限流，它与11.5节的逻辑上限流有很大区别。尤其要注意，正在等待一个操作完成的异步代码不属于运行中的代码。
             *   ConcurrentScheduler对运行中的代码做限流。其他限流方法（如SemaphoreSlim）在更高的层次做限流（即完整的异步方法）。
             * 在UI线程上执行代码时，永远不要使用针对特定平台的类型。WPF、Silverlight、iOS、Android都有Dispatcher类，Windows应用商店平台使用CoreDispatcher,
             *   Windows Forms有ISynchronizeInvoke接口（即Control.Invoke）。不要在新写的代码中使用这些类型，就当它们不存在吧。使用这些类型会使代码无谓地绑定在某个特定平台上。
             *   SynchronizationContext是通用的、基于上述类型的抽象类。
             * 如果需要真正地使用调度器抽象类，建议大家使用Rx的IScheduler。它具有良好的设计和定义，测试起来也很方便。不过大多数情况下并不需要调度器抽象类，
             *   也不需要使用早期的库（如任务并行库和TPL数据流），只要掌握TaskScheduler类就行了。
             */
            TaskScheduler scheduler = TaskScheduler.FromCurrentSynchronizationContext();
            var schedulerPair = new ConcurrentExclusiveSchedulerPair();
            TaskScheduler concurrent = schedulerPair.ConcurrentScheduler;
            TaskScheduler exclusive = schedulerPair.ExclusiveScheduler;
            var schedulerPair1 = new ConcurrentExclusiveSchedulerPair(TaskScheduler.Default, maxConcurrencyLevel: 8);
            TaskScheduler scheduler1 = schedulerPair1.ConcurrentScheduler;
        }

        // 12.3 调度并行代码
        private void button3_Click(object sender, EventArgs e)
        {
            /* 需要控制个别代码段在并行代码中的执行方式。
             * 创建了合适的TaskScheduler实例（见12.2节）后，可以把它放入Parallel类的方法参数中。下面的代码使用一系列矩阵的序列。启动一批并行循环，
             *   并且需要限制所有循环的总的并行数量，不管每个序列中有多少矩阵：
             * Parallel.Invoke也能使用ParallelOptions实例，因此可以像Parallel.ForEach那样把TaskScheduler传入Parallel.Invoke。
             *   在编写动态并行代码时，可以直接把TaskScheduler传入TaskFactory.StartNew或者Task.ContinueWith。
             *   没有什么办法能把TaskScheduler传入PLINQ代码。
             */
        }
        void RotateMatrices(IEnumerable<IEnumerable<Matrix>> collections, float degrees)
        {
            var schedulerPair = new ConcurrentExclusiveSchedulerPair(TaskScheduler.Default, maxConcurrencyLevel: 8);
            TaskScheduler scheduler = schedulerPair.ConcurrentScheduler;
            ParallelOptions options = new ParallelOptions { TaskScheduler = scheduler };
            Parallel.ForEach(collections, options,
              matrices => Parallel.ForEach(matrices, options,
                  matrix => matrix.Rotate(degrees)));
        }

        // 12.4 用调度器实现数据流的同步
        private void button4_Click(object sender, EventArgs e)
        {
            /* 需要控制个别代码段在数据流代码中的执行方式。
             * 创建了合适的TaskScheduler实例（见12.2节）后，可以把它放入数据流块的参数中。下面的代码被UI线程调用时，会创建一个数据流网格。
             *   这个数据流网格将每个输入值乘以2（用线程池），然后把结果添加到一个列表控件的项目中（在UI线程中）：
             * 如果要协调位于数据流网格中不同部位的块的行为，就非常需要指定一个TaskScheduler。例如，可以用ConcurrentExclusiveSchedulerPair.ExclusiveScheduler
             *   来确保块A和块C永远不同时执行代码，而块B可以随时执行。
             * 记住，TaskScheduler的同步功能只有在代码运行时才起作用。例如对一个运行异步代码的执行块启用一个独占调度器，当它正在等待时，不被认为是在运行。
             * 可以对任何类型的数据流块指定一个TaskScheduler。即使一个块可能执行外来的代码（如BufferBlock<T>），它仍需要做一些内部协调任务，并且会在内部任务中使用TaskScheduler。
             */
            var options = new ExecutionDataflowBlockOptions
            {
                TaskScheduler = TaskScheduler.FromCurrentSynchronizationContext(),
            };
            var multiplyBlock = new TransformBlock<int, int>(item => item * 2);
            var displayBlock = new ActionBlock<int>(result => listBox1.Items.Add(result), options);
            multiplyBlock.LinkTo(displayBlock);
            multiplyBlock.Post(2);
        }
    }
}
