﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1
{
    /* 并行编程用于分解计算密集型的任务片段，并将它们分配给多个线程。这些并行处理方法只适用于计算密集型的任务。
     *   如果有需要异步操作的任务（例如I/O密集型任务），而你希望它能并行运行，请看第2章，特别是2.4节。
     * 本章介绍的并行处理概念是任务并行库（TPL）的一部分。它只在.NET框架中实现，其他平台不一定适用（见表3-1）。
     */
    public partial class _3并行开发的基础 : Form
    {
        public _3并行开发的基础()
        {
            InitializeComponent();
        }

        // 3.1 数据的并行处理
        private void button1_Click(object sender, EventArgs e)
        {
            /* 有一批数据，需要对每个元素进行相同的操作。该操作是计算密集型的，需要耗费一定的时间。
             * Parallel类型有专门为此设计的ForEach方法。下面的例子使用了一批矩阵，对每一个矩阵都进行旋转：
             * 在某些情况下需要尽早结束这个循环，例如发现了无效值时。下面的例子反转每一个矩阵，但是如果发现有无效的矩阵，则中断循环：
             * 更常见的情况是可以取消并行循环，这与结束循环不同。结束（stop）循环是在循环内部进行的，而取消（cancel）循环是在循环外部进行的。
             *   例如，点击“取消”按钮可以取消一个CancellationTokenSource，以取消并行循环，方法如下：
             * 需要注意的是，每个并行任务可能都在不同的线程中运行，因此必须保护共享的状态。下面的例子反转每个矩阵并统计无法反转的矩阵数量：
             * Parallel.ForEach方法可以对一系列值进行并行处理。还有一个类似的解决方案，就是使用PLINQ（并行LINQ）。PLINQ的大部分功能和Parallel类一样，
             *   并且采用与LINQ类似的语法。Parallel类和PLINQ之间有一个区别：PLINQ假设可以使用计算机内所有的CPU核，而Parallel类则会根据CPU状态的变化动态地调整。
             * Parallel.ForEach是并行版本的foreach循环。Parallel类也提供了并行版本的for循环，即Parallel.For方法。
             *   如果有多个数组的数据，并且它们采用了相同的索引，Parallel.For就特别适用。
             */
        }
        void RotateMatrices(IEnumerable<Matrix> matrices, float degrees)
        {
            Parallel.ForEach(matrices, matrix => matrix.Rotate(degrees));
        }
        void InvertMatrices(IEnumerable<Matrix> matrices)
        {
            Parallel.ForEach(matrices, (matrix, state) =>
            {
                if (!matrix.IsInvertible)
                    state.Stop();
                else
                    matrix.Invert();
            });
        }
        void RotateMatrices(IEnumerable<Matrix> matrices, float degrees, CancellationToken token)
        {
            Parallel.ForEach(matrices,
              new ParallelOptions { CancellationToken = token },
              matrix => matrix.Rotate(degrees));
        }
        // 注意，这不是最高效的实现方式。
        // 只是举个例子，说明用锁来保护共享状态。
        int InvertMatrices1(IEnumerable<Matrix> matrices)
        {
            object mutex = new object();
            int nonInvertibleCount = 0;
            Parallel.ForEach(matrices, matrix =>
            {
                if (matrix.IsInvertible)
                {
                    matrix.Invert();
                }
                else
                {
                    lock (mutex)
                    {
                        ++nonInvertibleCount;
                    }
                }
            });
            return nonInvertibleCount;
        }

        // 3.2 并行聚合
        private void button2_Click(object sender, EventArgs e)
        {
            /* 在并行操作结束时，需要聚合结果，包括累加和、平均值等。
             * Parallel类通过局部值（local value）的概念来实现聚合，局部值就是只在并行循环内部存在的变量。这意味着循环体中的代码可以直接访问值，不需要担心同步问题。
             *   循环中的代码使用LocalFinally委托来对每个局部值进行聚合。需要注意的是，localFinally委托需要以同步的方式对存放结果的变量进行访问。
             *   下面是一个并行求累加和的例子：
             * 并行LINQ对聚合的支持，比Parallel类更加顺手：
             * 好吧，那只是雕虫小技，因为PLINQ本身就支持很多常规操作（例如求累加和）。PLINQ也可通过Aggregate实现通用的聚合功能：
             */
            int res = ParallelSum(new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Console.WriteLine(res);
            int res1 = ParallelSum1(new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Console.WriteLine(res1);
            int res2 = ParallelSum2(new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Console.WriteLine(res2);
        }
        // 注意，这不是最高效的实现方式。
        // 只是举个例子，说明用锁来保护共享状态。
        static int ParallelSum(IEnumerable<int> values)
        {
            object mutex = new object();
            int result = 0;
            Parallel.ForEach(source: values,
              localInit: () => 0,
              body: (item, state, localValue) => localValue + item,
              localFinally: localValue =>
              {
                  lock (mutex)
                      result += localValue;
              });
            return result;
        }
        static int ParallelSum1(IEnumerable<int> values)
        {
            return values.AsParallel().Sum();
        }
        static int ParallelSum2(IEnumerable<int> values)
        {
            return values.AsParallel().Aggregate(
              seed: 0,
              func: (sum, item) => sum + item
            );
        }

        // 3.3 并行调用
        private void button3_Click(object sender, EventArgs e)
        {
            /* 需要并行调用一批方法，并且这些方法（大部分）是互相独立的。
             * Parallel类有一个简单的成员Invoke，就可用于这种场合。下面的例子将一个数组分为两半，并且分别独立处理：
             * 如果在运行之前都无法确定调用的数量，就可以在Parallel.Invoke函数中输入一个委托数组：
             * 就像Parallel类的其他成员一样，Parallel.Invoke也支持取消操作：
             * 对于简单的并行调用，Parallel.Invoke是一个非常不错的解决方案。然而在以下两种情况中使用Parallel.Invoke并不是很合适：
             *   要对每一个输入的数据调用一个操作（改用Parallel.Foreach），或者每一个操作产生了一些输出（改用并行LINQ）。
             */
        }
        static void ProcessArray(double[] array)
        {
            Parallel.Invoke(
              () => ProcessPartialArray(array, 0, array.Length / 2),
              () => ProcessPartialArray(array, array.Length / 2, array.Length)
            );
        }
        static void ProcessPartialArray(double[] array, int begin, int end)
        {
            // 计算密集型的处理过程...
        }
        static void DoAction20Times(Action action)
        {
            Action[] actions = Enumerable.Repeat(action, 20).ToArray();
            Parallel.Invoke(actions);
        }
        static void DoAction20Times(Action action, CancellationToken token)
        {
            Action[] actions = Enumerable.Repeat(action, 20).ToArray();
            Parallel.Invoke(new ParallelOptions { CancellationToken = token }, actions);
        }

        // 3.4 动态并行
        private void button4_Click(object sender, EventArgs e)
        {
            /* 并行任务的结构和数量要在运行时才能确定，这是一种更复杂的并行编程。
             * 任务并行库（TPL）是以Task类为中心构建的。Task类的功能很强大，Parallel类和并行LINQ只是为了使用方便，从而对Task类进行了封装。
             *   实现动态并行最简单的做法就是直接使用Task类。
             * 下面的例子对二叉树的每个节点进行处理，并且该处理是很耗资源的。二叉树的结构在运行时才能确定，因此非常适合采用动态并行。Traverse方法处理当前节点，
             *   然后创建两个子任务，每个子任务对应一个子节点（本例中，假定必须先处理父节点，然后才能处理子节点）。
             *   ProcessTree方法启动处理过程，创建一个最高层的父任务，并等待任务完成：
             * 如果这些任务没有“父/子”关系，那可以使用任务延续（continuation）的方法，安排任务一个接着一个地运行。这里continuation是一个独立的任务，它在原始任务结束后运行：
             * 上面的例子使用了CancellationToken.None和TaskScheduler.Default。取消令牌（cancella-tion token）在9.2节介绍，任务调度器（task scheduler）在12.3节介绍。
             *   最好在StartNew和ContinueWith中明确指定用TaskScheduler。
             * 在动态并行中，通常以“父/子”的方式对任务进行安排，但也不是必须要这么做。也可以把每个新任务存储在线程安全的集合中，然后用Task.WaitAll等待所有任务完成。
             * 在并行处理中使用Task类，和在异步处理中使用完全不同。下面会详细解释。
             * 在并发编程中，Task类有两个作用：作为并行任务，或作为异步任务。并行任务可以使用阻塞的成员函数，例如Task.Wait、Task.Result、Task.WaitAll和Task.WaitAny。
             *   并行任务通常也使用AttachedToParent来建立任务之间的“父/子”关系。并行任务的创建需要用Task.Run或者Task.Factory.StartNew。
             *   相反，异步任务应该避免使用阻塞的成员函数，而应该使用await、Task.WhenAll和Task. WhenAny。异步任务不使用AttachedToParent，但可以通过await另一个任务，
             *   建立一种隐式的“父/子”关系。
             */
            Task task = Task.Factory.StartNew(
                () => Thread.Sleep(TimeSpan.FromSeconds(2)),
                CancellationToken.None,
                TaskCreationOptions.None,
                TaskScheduler.Default);
            Task continuation = task.ContinueWith(
                t => Trace.WriteLine("Task is done"),
                CancellationToken.None,
                TaskContinuationOptions.None,
                TaskScheduler.Default);
            // 对continuation来说，参数“t”相当于“task”
        }
        //void Traverse(Node current)
        //{
        //    DoExpensiveActionOnNode(current);
        //    if (current.Left != null)
        //    {
        //        Task.Factory.StartNew(() => Traverse(current.Left),
        //            CancellationToken.None,
        //            TaskCreationOptions.AttachedToParent,
        //            TaskScheduler.Default);
        //    }
        //    if (current.Right != null)
        //    {
        //        Task.Factory.StartNew(() => Traverse(current.Right),
        //            CancellationToken.None,
        //            TaskCreationOptions.AttachedToParent,
        //            TaskScheduler.Default);
        //    }
        //}
        //public void ProcessTree(Node root)
        //{
        //    var task = Task.Factory.StartNew(() => Traverse(root),
        //      CancellationToken.None,
        //      TaskCreationOptions.None,
        //      TaskScheduler.Default);
        //    task.Wait();
        //}

        // 3.5 并行LINQ
        private void button5_Click(object sender, EventArgs e)
        {
            /* 需要对一批数据进行并行处理，生成另外一批数据，或者对数据进行统计。
             * 大部分开发者对LINQ比较熟悉，LINQ可以实现在序列上“拉取”数据的运算。并行LINQ（PLINQ）扩展了LINQ，以支持并行处理。
             * PLINQ非常适用于数据流的操作，一个数据队列作为输入，一个数据队列作为输出。下面简单的例子将序列中的每个元素都乘以2（实际应用中，计算工作量要大得多）：
             * 按照并行LINQ的默认方式，这个例子中输出数据队列的次序是不固定的。也可以指明要求保持原来的次序。下面的例子也是并行执行的，但保留了数据的原有次序：
             * 并行LINQ的另一个常规用途是用并行方式对数据进行聚合或汇总。下面的代码实现了并行的累加求和：
             * Parallel类可适用于很多场合，但是在做聚合或进行数据序列的转换时，PLINQ的代码更加简洁。有一点需要注意，相比PLINQ, Parallel类与系统中其他进程配合得更好。
             *   如果在服务器上做并行处理，这一点尤其需要考虑。
             * PLINQ为各种各样的操作提供了并行的版本，包括过滤（Where）、投影（Select）以及各种聚合运算，例如Sum、Average和更通用的Aggregate。
             *   一般来说，对常规LINQ的所有操作都可以通过并行方式对PLINQ执行。正因为如此，如果准备把已有的LINQ代码改为并行方式，PLINQ是一种非常不错的选择。
             */
        }
        static IEnumerable<int> MultiplyBy2(IEnumerable<int> values)
        {
            return values.AsParallel().Select(item => item * 2);
        }
        static IEnumerable<int> MultiplyBy2_1(IEnumerable<int> values)
        {
            return values.AsParallel().AsOrdered().Select(item => item * 2);
        }
        static int ParallelSum3(IEnumerable<int> values)
        {
            return values.AsParallel().Sum();
        }
    }
}
