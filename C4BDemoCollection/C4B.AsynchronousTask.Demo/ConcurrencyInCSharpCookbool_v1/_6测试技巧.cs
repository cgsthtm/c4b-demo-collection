﻿using Microsoft.Reactive.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1
{
    /* 大家都知道单元测试在保证代码质量和整个开发过程中的作用，然而大多数开发人员直到今天都没有真正编写过单元测试。
     * 我建议大家至少写一些单元测试，首先从自己觉得最没信心的代码开始。根据我个人的经验，单元测试主要有两大好处。
     *  (1) 更好地理解代码。你是否遇到过这种情况：你了解程序的某个部分能正常运行，却对它的实现原理一无所知。当软件出现了令你不可思议的错误时，这种疑问常常占据你的内心深处。
     *      要理解那些特别“难”的代码的内部机理，编写单元测试就是一个很好的办法。编写描述代码行为的单元测试之后，就不会觉得这部分代码神秘了。
     *      编写一批单元测试后，最终就能搞清那些代码的行为，以及它们和其他代码之间的依赖关系。
     *  (2) 修改代码时更有把握。迟早会有那么一天，你会因为有功能需求而必须修改那些“恐怖”的代码，你将无法继续假装它不存在。（我了解那种感觉。我经历过！）
     *      最好提前做好准备：在此类需求到来之前，为那些恐怖的代码编写单元测试。提前准备，以免以后麻烦。如果你的单元测试是完整的，你就相当于有了一个早期预警系统，
     *      如果修改后的代码影响到已有功能时，它就会立即发出警告。
     * 本章的内容全部是关于测试的。很多开发人员（甚至包括经常编写单元测试的人）都逃避并发代码的单元测试，因为他们总觉得非常难。然而本章的内容将会告诉大家，
     *   并发代码的单元测试并没有想象中那么难。现在的语言功能和开发库，例如async和Rx，在测试的方便性方面做了很多考虑，并且确实能体现出这点。
     *   我建议大家使用本章的方法编写单元测试，尤其是并发编程的新手（就是认为新并发代码“很难”或“可怕”的人）。
     */
    public partial class _6测试技巧 : Form
    {
        public _6测试技巧()
        {
            InitializeComponent();
        }

        // 6.1 async方法的单元测试
        private void button1_Click(object sender, EventArgs e)
        {
            /* 需要对async方法进行单元测试。
             * 现在大多数单元测试框架都支持async Task类型的单元测试，包括MSTest、NUnit、xUnit。从Visual Studio 2012开始，MSTest才支持async Task类型的单元测试
             * 下面是一个async类型MSTest单元测试的例子：
             * 单元测试框架检测到方法的返回类型是Task，会自动加上await等待任务完成，然后将测试结果标记为“成功”或“失败”。
             * 如果单元测试框架不支持async Task类型的单元测试，就需要做一些额外的修改才能等待异步操作。其中一种做法是使用Task.Wait，
             *   并在有错误时拆开AggregateException对象。我的建议是使用NuGet包Nito.AsyncEx中的AsyncContext类：
             * AsyncContext.Run会等待所有异步方法完成。
             * 模拟（mocking）异步方法间的依赖关系，虽说它给人的第一感觉是有点别扭，但至少可以测试某些方法如何响应同步成功（用Task.FromResult模拟）、
             *   同步出错（用TaskCompletionSource<T>模拟）以及异步成功（用Task.Yield模拟，并返回一个值），并且它在做这些测试时，是一个很好的办法。
             * 跟同步代码相比，在测试异步代码时会出现更多的死锁和竞态条件。我发现，对每个测试进行超时设置很有用。在Visual Studio中，可以在解决方案中加一个测试设置文件，
             *   用来对每个测试设置独立的超时参数。这个参数的默认值是很大的，我通常将每一个测试的超时参数设成2秒。
             */
        }
        //[TestMethod]
        //public async Task MyMethodAsyncReturnsFalse()
        //{
        //    var objectUnderTest = ...;
        //    bool result = await objectUnderTest.MyMethodAsync();
        //    Assert.IsFalse(result);
        //}
        //[TestMethod]
        //public void MyMethodAsyncReturnsFalse()
        //{
        //    AsyncContext.Run(async () =>
        //    {
        //        var objectUnderTest = ...;
        //        bool result = await objectUnderTest.MyMethodAsync();
        //        Assert.IsFalse(result);
        //    });
        //}

        // 6.2 预计失败的async方法的单元测试
        private void button2_Click(object sender, EventArgs e)
        {
            /* 需要编写一个单元测试，用来检查async Task方法的一个特定错误。
             * 对于桌面程序或服务器程序，MSTest就可以用常规的ExpectedExceptionAttribute进行错误测试：
             * 但是这种方法并不是最好的。一方面，Windows应用商店并没有ExpectedException支持单元测试。另一个本质的原因是ExpectedException的设计非常槽糕。
             *   单元测试中调用的任何方法都可以抛出这个异常。更好的设计是检查抛出异常的那段代码，而不是检查整个单元测试。
             * 微软公司已经在向这个方向努力了，在Windows应用商店单元测试中去掉了ExpectedException，改成用Assert.ThrowsException<TException>，使用方法如下：
             * 千万别忘了对ThrowsException返回的Task使用await。这样才可以传递出所有监测到的出错信息。如果忘记使用await并且忽视了编译器的警告，
             *   那么不管被测试的方法是否真的正确，单元测试就会一直显示测试成功且不给任何提示。
             */
        }
        // 不推荐用这种方法，原因在后面。
        //[TestMethod]
        //[ExpectedException(typeof(DivideByZeroException))]
        //public async Task DivideWhenDenominatorIsZeroThrowsDivideByZero()
        //{
        //    await MyClass.DivideAsync(4, 0);
        //}
        //[TestMethod]
        //public async Task DivideWhenDenominatorIsZeroThrowsDivideByZero()
        //{
        //    await Assert.ThrowsException<DivideByZeroException>(async () =>
        //    {
        //        await MyClass.DivideAsync(4, 0);
        //    });
        //}

        // 6.3 async void方法的单元测试
        private void button3_Click(object sender, EventArgs e)
        {
            /* 需要对一个asnyc void类型的方法做单元测试。
             * 要尽最大可能避免这个问题，而不是去解决它。只要有可能把async void方法改成async Task，那就得改。
             * 如果一个方法必须采用async void（例如为满足某个接口方法的特征），那可考虑编写两个方法：一个包含所有逻辑的async Task方法和一个async void方法。
             *   这个async void方法只是做一个简单封装，即调用async Task方法，并用await等待结果。这样，async void方法可满足格式要求，
             *   而async Task方法（包含所有逻辑）可用于测试。
             * 如果真的不可能修改这个方法，并且确实必须对一个async void方法做单元测试，可试试这个方法，使用Nito.AsyncEx类库的AsyncContext类：
             * 这个AsyncContext类会等待所有异步操作完成（包括async void方法），再将异常传递出去。
             * 在async代码中，关键准则之一就是避免使用async void。我非常建议大家在对async void方法做单元测试时进行代码重构，而不是使用AsyncContext。
             */
        }
        // 不推荐用这种方法，原因见前面。
        //[TestMethod]
        //public void MyMethodAsync DoesNotThrow()
        //{
        //    AsyncContext.Run(() =>
        //    {
        //        var objectUnderTest = ...;
        //        objectUnderTest.MyMethodAsync();
        //    });
        //}

        // 6.4 数据流网格的单元测试
        private void button4_Click(object sender, EventArgs e)
        {
            /* 程序中有一个数据流网格，需要对它进行正确性验证。
             * 数据流网格是独立的：有自己的生命周期，并且本质上就是异步的。自然而然，它的测试方法就是使用异步的单元测试。下面的单元测试验证4.6节中的自定义数据流块：
             * 可惜的是，对错误进行单元测试就没那么简单了。这是因为在数据流网格中，异常信息在块之间传递时会被一层一层地封装在另一个AggregateException中。
             *   下面的例子使用了一个辅助方法，以确保一个异常在丢弃数据之后，再在自定义块之间传递。
             * 直接对数据流网格做单元测试是可行的，但有些别扭。如果网格是一个大组件的组成部分，只对这个大组件做单元测试（隐式的测试网格），那样会比较简单。
             *   但如果开发可重用的自定义块或网格，那就应该像前面那样做单元测试。
             */
        }
        //[TestMethod]
        //public async Task MyCustomBlockAddsOneToDataItems()
        //{
        //    var myCustomBlock = CreateMyCustomBlock();
        //    myCustomBlock.Post(3);
        //    myCustomBlock.Post(13);
        //    myCustomBlock.Complete();
        //    Assert.AreEqual(4, myCustomBlock.Receive());
        //    Assert.AreEqual(14, myCustomBlock.Receive());
        //    await myCustomBlock.Completion;
        //}
        //[TestMethod]
        //public async Task MyCustomBlockFaultDiscardsDataAndFaults()
        //{
        //    var myCustomBlock = CreateMyCustomBlock();
        //    myCustomBlock.Post(3);
        //    myCustomBlock.Post(13);
        //    myCustomBlock.Fault(new InvalidOperationException());
        //    try
        //    {
        //        await myCustomBlock.Completion;
        //    }
        //    catch (AggregateException ex)
        //    {
        //        AssertExceptionIs<InvalidOperationException>(
        //            ex.Flatten().InnerException, false);
        //    }
        //}
        //public static void AssertExceptionIs<TException>(Exception ex,
        //    bool allowDerivedTypes = true)
        //{
        //    if (allowDerivedTypes && !(ex is TException))
        //        Assert.Fail("Exception is of type " + ex.GetType().Name + ", but "
        //        + typeof(TException).Name + " or a derived type was expected.");
        //    if (!allowDerivedTypes && ex.GetType() != typeof(TException))
        //        Assert.Fail("Exception is of type " + ex.GetType().Name + ", but "
        //        + typeof(TException).Name + " was expected.");
        //}

        // 6.5 Rx Observable对象的单元测试
        private void button5_Click(object sender, EventArgs e)
        {
            /* 程序中用到了IObservable<T>，需要对这部分程序做单元测试。
             * 响应式扩展（Reactive Extension）有很多产生序列的操作符（如Return），还有操作符可把响应式序列转换成普通集合或项目（如SingleAsync）。
             *   我们可使用Return等操作符创建Observable对象依赖项的存根（stub），用SingleAsync等操作符来测试输出。
             * 看下面的代码，它把一个HTTP服务作为依赖项，并且在调用HTTP时使用了一个超时：
             * 我们要测试的代码是MyTimeoutClass，它消耗一个Observable对象依赖项，生成一个Observable对象作为输出。
             * Return操作符创建一个只有一个元素的冷序列（cold sequence），可用它来构建简单的存根（stub）。SingleAsync操作符返回一个Task<T>对象，
             *   该对象在下一个事件到达时完成。SingleAsync可用来做简单的单元测试，如下所示：
             * 存根代码中另一个重要操作符是Throw，它返回一个以错误结束的Observable对象。这样我们也可对有错误的场景做单元测试。
             *   下面的例子使用了6.2节中的辅助方法ThrowsExceptionAsync：
             * Return和Throw操作符很适合创建observable对象的存根，而要在async单元测试中测试observable对象，比较容易的方法就是使用SingleAsync。
             *   对于简单的observable对象，这两个操作符结合起来使用效果很好。但如果observable对象与时间有关，它们就不那么管用了。
             *   例如要测试MyTimeoutClass类的超时能力，单元测试就必须真正地等待那么长时间。一旦增加更多的单元测试，这种方式就不大合适了。
             *   6.6节介绍一种特殊的方法，Reactive Extensions可以把时间本身排除在外。
             */
        }
        public interface IHttpService
        {
            IObservable<string> GetString(string url);
        }
        public class MyTimeoutClass
        {
            private readonly IHttpService httpService;

            public MyTimeoutClass(IHttpService httpService)
            {
                httpService = httpService;
            }

            public IObservable<string> GetStringWithTimeout(string url)
            {
                return httpService.GetString(url)
                    .Timeout(TimeSpan.FromSeconds(1));
            }
        }
        class SuccessHttpServiceStub : IHttpService
        {
            public IObservable<string> GetString(string url)
            {
                return Observable.Return("stub");
            }
        }
        [TestMethod]
        public async Task MyTimeoutClassSuccessfulGetReturnsResult()
        {
            var stub = new SuccessHttpServiceStub();
            var my = new MyTimeoutClass(stub);

            var result = await my.GetStringWithTimeout("http://www.example.com/")
                .SingleAsync();

            Assert.AreEqual("stub", result);
        }
        private class FailureHttpServiceStub : IHttpService
        {
            public IObservable<string> GetString(string url)
            {
                return Observable.Throw<string>(new HttpRequestException());
            }
        }
        [TestMethod]
        public async Task MyTimeoutClassFailedGetPropagatesFailure()
        {
            var stub = new FailureHttpServiceStub();
            var my = new MyTimeoutClass(stub);

            await Assert.ThrowsExceptionAsync<HttpRequestException>(async () =>
            {
                await my.GetStringWithTimeout("http://www.example.com/")
                    .SingleAsync();
            });
        }

        // 6.6 用虚拟时间测试Rx Observable对象
        private void button6_Click(object sender, EventArgs e)
        {
            /* 需要写一个不依赖于时间的单元测试，来测试一个依赖于时间的observable对象。如observable对象中使用了超时、窗口/缓冲、限流/抽样等方法，那它就是依赖于时间的。
             *   我们要对它们做单元测试，但要求运行时间不能太长。
             * 我们当然可以让延迟函数在单元测试中运行。但是这样做会产生两个问题：
             *   1)单元测试的运行时间会很长；
             *   2)因为所有的单元测试是同时运行的，这样做会导致竞态条件，无法预测运行时机。
             * Rx库在设计时就考虑到了测试问题。实际上，Rx库本身就已经过大量单元测试。为了解决上面的问题，Rx引入了调度器（scheduler）这一概念，
             *   每个与时间有关的Rx操作都在实现时使用了这个抽象的调度器。
             * 要让observable对象便于测试，就要允许调用它的程序指定调度器。例如可以使用6.5节的MyTimeoutClass，并加上一个调度器：
             * 接下来，修改HTTP服务存根，加入调度功能，然后加入一个可变延迟：
             * 这样就可以使用Rx库中的TestScheduler了。可以用TestScheduler对（虚拟）时间进行很好的控制。
             * TestScheduler在Rx中一个单独的NuGet包中，需要安装NuGet包Rx-Testing。
             * 用TestScheduler可以对时间进行完整的控制，但通常只需要写好代码，然后调用TestScheduler.Start。在整个测试结束前，Start方法可以用虚拟的方式推进时间。
             *   下面是一个成功测试的简单例子：
             * 这段代码模拟了0.5秒的网络延时。需要强调的是，这个单元测试实际运行时间并没有0.5秒。在我的电脑上，做这个测试只需70毫秒左右。这个0.5秒的延时只不过是虚拟的。
             *   另一个值得注意的差别是，这个单元测试不是异步的。因为使用了TestScheduler，所有的测试都会立即完成。
             * 好了，现在使用了调度器，测试超时的情况就很容易了：
             * 前面我们介绍了Reactive Extensions的调度器和虚拟时间的入门知识。Rx编程和单元测试最好能同时进行。请放心，即使代码越来越复杂，Rx的测试功能也足以应对了。
             */
        }
        public interface IHttpService1
        {
            IObservable<string> GetString(string url);
        }
        public class MyTimeoutClass1
        {
            private readonly IHttpService1 httpService;
            public MyTimeoutClass1(IHttpService1 httpService)
            {
                httpService = httpService;
            }

            public IObservable<string> GetStringWithTimeout(string url,
              IScheduler scheduler = null)
            {
                return httpService.GetString(url)
                    .Timeout(TimeSpan.FromSeconds(1), scheduler ?? Scheduler.Default);
            }
        }
        private class SuccessHttpServiceStub1 : IHttpService1
        {
            public IScheduler Scheduler { get; set; }
            public TimeSpan Delay { get; set; }

            public IObservable<string> GetString(string url)
            {
                return Observable.Return("stub")
                    .Delay(Delay, Scheduler);
            }
        }
        [TestMethod]
        public void MyTimeoutClassSuccessfulGetShortDelayReturnsResult()
        {
            var scheduler = new TestScheduler();
            var stub = new SuccessHttpServiceStub1
            {
                Scheduler = scheduler,
                Delay = TimeSpan.FromSeconds(0.5),
            };
            var my = new MyTimeoutClass1(stub);
            string result = null;

            my.GetStringWithTimeout("http://www.example.com/", scheduler)
              .Subscribe(r => { result = r; });
            scheduler.Start();

            Assert.AreEqual("stub", result);
        }
        [TestMethod]
        public void MyTimeoutClassSuccessfulGetLongDelayThrowsTimeoutException()
        {
            var scheduler = new TestScheduler();
            var stub = new SuccessHttpServiceStub1
            {
                Scheduler = scheduler,
                Delay = TimeSpan.FromSeconds(1.5),
            };
            var my = new MyTimeoutClass1(stub);
            Exception result = null;
            my.GetStringWithTimeout("http://www.example.com/", scheduler)
              .Subscribe( onNext => Assert.Fail("Received value"), ex => { result = ex; });

            scheduler.Start();

            Assert.IsInstanceOfType(result, typeof(TimeoutException));
        }
    }
}
