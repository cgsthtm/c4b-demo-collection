﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1
{
    /* Reactive Extensions（Rx）把事件看作是依次到达的数据序列。因此，将Rx认作是LINQ to events（基于IObservable<T>）也是可以的，它与其他LINQ提供者的主要区别在于，
     *   Rx采用“推送”模式。就是说，Rx的查询规定了在事件到达时程序该如何响应。Rx在LINQ的基础上构建，增加了一些功能强大的操作符，作为扩展方法。
     * 本章介绍一些比较常用的Rx操作。需要注意的是，所有的LINQ操作都可以在Rx中使用。从概念上看，过滤（Where）、投影（Select）等简单操作，和其他LINQ提供者的操作是一样的。
     *   本章不介绍那些常见的LINQ操作，而将重点放在Rx在LINQ基础上增加的新功能，尤其是与时间有关的功能。
     * 要使用Rx，需要在应用中安装一个NuGet包Rx-Main。支持Reactive Extensions的平台非常丰富（参见表5-1）。
     */
    public partial class _5Rx基础 : Form
    {
        public _5Rx基础()
        {
            InitializeComponent();
        }

        // 5.1 转换.NET事件
        private void button1_Click(object sender, EventArgs e)
        {
            /* 把一个事件作为Rx输入流，每次事件发生时通过OnNext生成数据。
             * Observable类定义了一些事件转换器。大部分.NET框架事件与FromEventPattern兼容，对于不遵循通用模式的事件，需要改用FromEvent。
             *   FromEventPattern最适合使用委托类型为EventHandler<T>的事件。很多较新框架类的事件都采用了这种委托类型。例如，Progress<T>类定义了事件ProgressChanged，
             *   这个事件的委托类型就是EventHandler<T>，因此，它就很容易被封装到FromEventPattern：
             * 请注意，data.EventArgs是强类型的int。FromEventPattern的类型参数（上例中为int）与EventHandler<T>的T相同。
             *   Rx用FromEventPattern中的两个Lambda参数来实现订阅和退订事件。
             * 较新的UI框架采用EventHandler<T>，可以很方便地应用在FromEventPattern中。但是有些较旧的类常为每个事件定义不同的委托类型。
             *   这些事件也能在FromEventPattern中使用，但需要做一些额外的工作。例如，System.Timers.Timer类有一个事件Elapsed，它的类型是ElapsedEventHandler。
             *   对此旧类事件，可以用下面的方法封装进FromEventPattern：
             * 注意，data.EventArgs仍然是强类型的。现在FromEventPattern的类型参数是对应的事件处理程序和EventArgs的派生类。
             *   FromEventPattern的第一个Lambda参数是一个转换器，它将EventHandler<ElapsedEventArgs>转换成ElapsedEventHandler。
             *   除了传递事件，这个转换器不应该做其他处理。上面代码的语法明显有些别扭。另一个方法是使用反射机制：
             * 采用这种方法后，调用FromEventPattern就简单多了。但是这种方法也有缺点：出现了一个怪异的字符串（"Elapsed"），并且消息的使用者不是强类型了。
             *   就是说，这时data. EventArgs是object类型，需要人为地转换成ElapsedEventArgs。
             * 事件是Rx流数据的主要来源。本节介绍如何封装遵循标准模式的事件（标准事件模式：第一个参数是事件发送者，第二个参数是事件的类型参数）。
             *   对于不标准的事件类型，可以用重载Observable.FromEvent的办法，把事件封装进Observable对象。把事件封装进Observable对象后，每次引发该事件都会调用OnNext。
             *   在处理AsyncCompletedEventArgs时会发生令人奇怪的现象，所有的异常信息都是通过数据形式传递的（OnNext），而不是通过错误传递（OnError）。
             *   看一个封装WebClient.DownloadStringCompleted的例子：
             * WebClient.DownloadStringAsync出错并结束时，引发带有异常AsyncCompletedEventArgs.Error的事件。可惜Rx会把这作为一个数据事件，
             *   因此这个程序的结果是显示“OnNext:(Error)”，而不是“OnError:”。
             * 有些事件的订阅和退订必须在特定的上下文中进行。例如，很多UI控件的事件必须在UI线程中订阅。Rx提供了一个操作符SubscribeOn，可以控制订阅和退订的上下文。
             *   大多数情况下没必要使用这个操作符，因为基于UI的事件订阅通常就是在UI线程中进行的。
             */
            var progress = new Progress<int>();
            var progressReports = Observable.FromEventPattern<int>(
                handler => progress.ProgressChanged += handler,
                handler => progress.ProgressChanged -= handler);
            progressReports.Subscribe(data => Trace.WriteLine("OnNext:" + data.EventArgs));

            var timer = new System.Timers.Timer(interval: 1000) { Enabled = true };
            var ticks = Observable.FromEventPattern<ElapsedEventHandler, ElapsedEventArgs>(
                handler => (s, a) => handler(s, a),
                handler => timer.Elapsed += handler,
                handler => timer.Elapsed -= handler);
            ticks.Subscribe(data => Trace.WriteLine("OnNext: " + data.EventArgs.SignalTime));

            var timer1 = new System.Timers.Timer(interval: 1000) { Enabled = true };
            var ticks1 = Observable.FromEventPattern(timer1, "Elapsed");
            ticks1.Subscribe(data => Trace.WriteLine("OnNext: "
                + ((ElapsedEventArgs)data.EventArgs).SignalTime));

            var client = new WebClient();
            var downloadedStrings = Observable.FromEventPattern(client,
                "DownloadStringCompleted");
            downloadedStrings.Subscribe(
                data =>
                {
                    var eventArgs = (DownloadStringCompletedEventArgs)data.EventArgs;
                    if (eventArgs.Error != null)
                        Trace.WriteLine("OnNext: (Error) " + eventArgs.Error);
                    else
                        Trace.WriteLine("OnNext: " + eventArgs.Result);
                },
                ex => Trace.WriteLine("OnError: " + ex.ToString()),
                () => Trace.WriteLine("OnCompleted"));
            client.DownloadStringAsync(new Uri("http://invalid.example.com/"));
        }

        // 5.2 发通知给上下文
        private void button2_Click(object sender, EventArgs e)
        {
            /* Rx尽量做到了线程不可知（thread agnostic）。因此它会在任意一个活动线程中发出通知（例如OnNext）。但是我们通常希望通知只发给特定的上下文。
             *   例如UI元素只能被它所属的UI线程控制，因此，如果要根据Rx的通知来修改UI，就应该把通知“转移”到UI线程。
             * Rx提供了ObserveOn操作符，用来把通知转移到其他线程调度器。看下面的例子，使用Interval，每秒钟产生一个OnNext通知：
             * 因为Interval基于一个定时器（没有指定的线程），通知会在线程池线程中引发，而不是在UI线程中。要更新UI元素，可以通过ObserveOn输送通知，
             *   并传递一个代表UI线程的同步上下文：
             * ObserveOn的另一个常用功能是可以在必要时离开UI线程。假设有这样的情况：鼠标一移动，就意味着需要进行一些CPU密集型的计算。默认情况下，
             *   所有的鼠标移动事件都发生在UI线程，因此可以使用ObserveOn把通知移动到一个线程池线程，在那里进行计算，然后再把表示结果的通知返回给UI线程：
             * 运行这段代码的话，就会发现计算过程是在线程池线程中进行的，计算结果在UI线程中显示。另外，还会发现计算和结果会滞后于输入，形成等待的队列，
             *   这种现象出现的原因在于，比起100秒1次的计算，鼠标移动的更新频率更高。Rx中有几种技术可以处理这种情况，其中一个常用方法是对输入流速进行限制，具体会在5.4节介绍。
             * 实际上，ObserveOn是把通知转移到一个Rx调度器上了。本节介绍了默认调度器（即线程池）和一种创建UI调度器的方法。ObserveOn最常用的功能是移到或移出UI线程，
             *   但调度器也能用于别的场合。6.6节介绍高级测试时，将再次关注调度器。
             */
            //Trace.WriteLine("UI thread is " + Environment.CurrentManagedThreadId);
            //Observable.Interval(TimeSpan.FromSeconds(1))
            //  .Subscribe(x => Trace.WriteLine("Interval " + x + " on thread " +
            //      Environment.CurrentManagedThreadId));

            //var uiContext = SynchronizationContext.Current;
            //Trace.WriteLine("UI thread is " + Environment.CurrentManagedThreadId);
            //Observable.Interval(TimeSpan.FromSeconds(1))
            //  .ObserveOn(uiContext)
            //  .Subscribe(x => Trace.WriteLine("Interval1 " + x + " on thread " +
            //      Environment.CurrentManagedThreadId));

            var uiContext1 = SynchronizationContext.Current;
            Trace.WriteLine("UI thread is " + Environment.CurrentManagedThreadId);
            Observable.FromEventPattern<MouseEventHandler, MouseEventArgs>(
                  handler => (s, a) => handler(s, a),
                  handler => MouseMove += handler,
                  handler => MouseMove -= handler)
              .Select(evt => evt.EventArgs.Location)
              .ObserveOn(Scheduler.Default)
              .Select(position =>
              {
                  // 复杂的计算过程。
                  Thread.Sleep(100);
                  var result = position.X + position.Y;
                  Trace.WriteLine("Calculated result " + result + " on thread " +
                      Environment.CurrentManagedThreadId);
                  return result;
              })
              .ObserveOn(uiContext1)
              .Subscribe(x => Trace.WriteLine("Result " + x + " on thread " +
                  Environment.CurrentManagedThreadId));
        }

        // 5.3 用窗口和缓冲对事件分组
        private void button3_Click(object sender, EventArgs e)
        {
            /* 有一系列事件，需要在它们到达时进行分组。举个例子，需要对一些成对的输入作出响应。第二个例子，需要在2秒钟的窗口期内，对所有输入进行响应。
             * Rx提供了两个对到达的序列进行分组的操作：Buffer和Window。Buffer会留住到达的事件，直到收完一组事件，然后会把这一组事件以一个集合的形式一次性地转送过去。
             *   Window会在逻辑上对到达的事件进行分组，但会在每个事件到达时立即传递过去。Buffer的返回类型是IObservable<IList<T>>（由若干个集合组成的事件流）; 
             *   Window的返回类型是IObservable<IObservable<T>>（由若干个事件流组成的事件流）。
             * 下面的例子使用Interval，每秒创建1个OnNext通知，然后每2个通知做一次缓冲：
             * 下面的例子有些类似，使用Window创建一些事件组，每组包含2个事件：
             * 这几个例子说明了Buffer和Window的区别。Buffer等待组内的所有事件，然后把所有事件作为一个集合发布。Window用同样的方法进行分组，但它是在每个事件到达时就发布。
             * Buffer和Window都可以使用时间段作为参数。在下面的例子中，所有的鼠标移动事件被收集进窗口，每秒一个窗口：
             * Buffer和Window可用来抑制输入信息，并把输入塑造成我们想要的样子。另一个实用技术是限流（throttling），将在5.4节介绍。
             * Buffer和Windows都有其他重载，可用在更高级的场合。参数为skip和timeShift的重载能创建互相重合的组，还可跳过组之间的元素。
             *   还有一些重载可使用委托，可对组的边界进行动态定义。
             */
            //Observable.Interval(TimeSpan.FromSeconds(1))
            //  .Buffer(2)
            //  .Subscribe(x => Trace.WriteLine(
            //      DateTime.Now.Second + ": Got " + x[0] + " and " + x[1]));

            //Observable.Interval(TimeSpan.FromSeconds(1))
            //  .Window(2)
            //  .Subscribe(group =>
            //  {
            //      Trace.WriteLine(DateTime.Now.Second + ": Starting new group");
            //      group.Subscribe(
            //          x => Trace.WriteLine(DateTime.Now.Second + ": Saw " + x),
            //          () => Trace.WriteLine(DateTime.Now.Second + ": Ending group"));
            //  });

            Observable.FromEventPattern<MouseEventHandler, MouseEventArgs>(
                  handler => (s, a) => handler(s, a),
                  handler => MouseMove += handler,
                  handler => MouseMove -= handler)
              .Buffer(TimeSpan.FromSeconds(1))
              .Subscribe(x => Trace.WriteLine(
                  DateTime.Now.Second + ": Saw " + x.Count + " items."));
        }

        // 5.4 用限流和抽样抑制事件流
        private void button4_Click(object sender, EventArgs e)
        {
            /* 有时事件来得太快，这是编写响应式代码时经常碰到的问题。一个速度太快的事件流可导致程序的处理过程崩溃。
             * Rx专门提供了几个操作符，用来对付大量涌现的事件数据。Throttle和Sample这两个操作符提供了两种不同方法来抑制快速涌来的输入事件。
             * Throttle建立了一个超时窗口，超时期限可以设置。当一个事件到达时，它就重新开始计时。当超时期限到达时，它就把窗口内到达的最后一个事件发布出去。
             *   下面的例子也是监视鼠标移动，但使用了Throttle，在鼠标保持静止1秒后才报告最近一条移动事件。
             * Throttle常用于类似“文本框自动填充”这样的场合，用户在文本框中输入文字，当他停止输入时，才需要进行真正的检索。
             * 为抑制快速运动的事件序列，Sample操作符使用了另一种方法。Sample建立了一个有规律的超时时间段，每个时间段结束时，它就发布该时间段内最后的一条数据。
             *   如果这个时间段没有数据，就不发布。下面的例子捕获鼠标移动，每隔一秒采样一次。与Throttle不同，使用Sample的例子中，不需要让鼠标静止一段时间，就可要看到结果。
             * 对于快速涌来的输入，限流和抽样是很重要的两种工具。别忘了还有一个过滤输入的简单方法，就是采用标准LINQ的Where操作符。可以这样说，
             *   Throttle和Sample操作符与Where基本差不多，唯一的区别是Throttle、Sample根据时间段过滤，而Where根据事件的数据过滤。在抑制快速涌来的输入流时，
             *   这三种操作符提供了三种不同的方法。
             */
            //Observable.FromEventPattern<MouseEventHandler, MouseEventArgs>(
            //        handler => (s, a) => handler(s, a),
            //        handler => MouseMove += handler,
            //        handler => MouseMove -= handler)
            //    .Select(x => x.EventArgs.Location)
            //    .Throttle(TimeSpan.FromSeconds(1))
            //    .Subscribe(x => Trace.WriteLine(
            //        DateTime.Now.Second + ": Saw " + (x.X + x.Y)));

            Observable.FromEventPattern<MouseEventHandler, MouseEventArgs>(
                  handler => (s, a) => handler(s, a),
                  handler => MouseMove += handler,
                  handler => MouseMove -= handler)
              .Select(x => x.EventArgs.Location)
              .Sample(TimeSpan.FromSeconds(1))
              .Subscribe(x => Trace.WriteLine(
                  DateTime.Now.Second + ": Saw " + (x.X + x.Y)));
        }

        // 5.5 超时
        private void button5_Click(object sender, EventArgs e)
        {
            /* 我们希望事件能在预定的时间内到达，即使事件不到达，也要确保程序能及时进行响应。通常此类事件是单一的异步操作（例如，等待Web服务请求的响应）。
             * Timeout操作符在输入流上建立一个可调节的超时窗口。一旦新的事件到达，就重置超时窗口。如果超过期限后事件仍没到达，Timeout操作符就结束流，
             *   并产生一个包含TimeoutException的OnError通知。下面的代码向一个域名发出Web请求，并使用1秒作为超时值：
             * Timeout非常适用于异步操作，例如Web请求，但它也能用于任何事件流。下面的例子在监视鼠标移动时使用Timeout，使用起来更加简单：
             * 值得注意的是，一旦向OnError发送TimeoutException，整个事件流就结束了，不会继续传来鼠标移动事件。为了阻止这种情况出现，Timeout操作符具有重载方式，
             *   在超时发生时用另一个流来替代，而不是抛出异常并结束流。下面的例子，在超时之前观察鼠标移动，超时发生后进行切换，观察鼠标点击：
             * Timeout操作符对优秀的程序来说是十分必要的，因为我们总是希望程序能及时响应，即使外部环境不理想。它可用于任何事件流，尤其是在异步操作时。
             *   需要注意，此时内部的操作并没有真正取消，操作将继续执行，直到成功或失败。
             */
            var client = new HttpClient();
            client.GetStringAsync("http://www.example.com/").ToObservable()
              .Timeout(TimeSpan.FromSeconds(1))
              .Subscribe(
                  x => Trace.WriteLine(DateTime.Now.Second + ": Saw " + x.Length),
                  ex => Trace.WriteLine(ex));

            //Observable.FromEventPattern<MouseEventHandler, MouseEventArgs>(
            //      handler => (s, a) => handler(s, a),
            //      handler => MouseMove += handler,
            //      handler => MouseMove -= handler)
            //  .Select(x => x.EventArgs.Location)
            //    .Timeout(TimeSpan.FromSeconds(1))
            //    .Subscribe(
            //        x => Trace.WriteLine(DateTime.Now.Second + ": Saw " + (x.X + x.Y)),
            //        ex => Trace.WriteLine(ex));

            var clicks = Observable.FromEventPattern<MouseEventHandler, MouseEventArgs>(
                  handler => (s, a) => handler(s, a),
                  handler => MouseDown += handler,
                  handler => MouseDown -= handler)
              .Select(x => x.EventArgs.Location);
            Observable.FromEventPattern<MouseEventHandler, MouseEventArgs>(
                  handler => (s, a) => handler(s, a),
                  handler => MouseMove += handler,
                  handler => MouseMove -= handler)
              .Select(x => x.EventArgs.Location)
              .Timeout(TimeSpan.FromSeconds(1), clicks)
              .Subscribe(
                  x => Trace.WriteLine(
                      DateTime.Now.Second + ": Saw " + x.X + ", " + x.Y),
                  ex => Trace.WriteLine(ex));
        }
        
    }
}
