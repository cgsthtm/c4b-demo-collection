﻿using Nito.AsyncEx;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace C4B.AsynchronousTask.Demo.ConcurrencyInCSharpCookbool_v1
{
    /* 本章介绍一些专门用于并发或异步开发的新集合。
     * 不可变集合是永远不会改变的集合。这种集合看起来好像没什么用处，但实际上它们用途很广泛，甚至能用在单线程、非并发的程序中。只读操作（如枚举）直接访问不可变集合实例。
     *   写入操作（如增加一个项目）会返回一个新的不可变集合实例，而不是修改原来的实例。乍一听上去，这种做法浪费存储空间，但不可变集合之间通常共享了大部分存储空间，
     *   因此其实浪费并不大。并且不可变集合有个优势，多个线程访问是安全的。因为是无法修改的，所以是线程安全的。不可变集合在NuGet包Microsoft.Bcl.Immutable中。
     * 线程安全集合是可同时被多个线程修改的可变集合。线程安全集合混合使用了细粒度锁定和无锁技术，以确保线程被阻塞的时间最短（通常情况下是根本不阻塞）。
     *   对很多线程安全集合进行枚举操作时，内部创建了该集合的一个快照（snapshot），并对这个快照进行枚举操作。线程安全集合的主要优点是多个线程可以安全地对其进行访问，
     *   而代码只会被阻塞很短的时间，或根本不阻塞。表8-2是各平台对线程安全集合的支持情况。
     * 生产者/消费者集合是一种可变集合，这类集合的设计带有特殊的目的：支持（可能有多个）生产者向集合推送项目，同时支持（可能有多个）消费者从集合取走项目。
     *   它们在生产者代码和消费者代码之间架设了桥梁，并且可通过设置来限制集合中的项目数量。生产者/消费者集合可以有阻塞或异步的API。
     *   例如，集合为空时，一个阻塞的生产者/消费者集合会阻塞正在调用的消费者线程，直到有一个项目加入集合它才停止。
     *   但是一个异步的生产者/消费者集合会使消费者线程进行异步等待，直到加入另一个项目。
     *   AsyncProducerConsumerQueue<T> 和AsyncCollection<T> 在NuGet包Nito.AsyncEx中。BufferBlock<T>在NuGet包Microsoft.Tpl.Dataflow中。
     */
    public partial class _8集合 : Form
    {
        public _8集合()
        {
            InitializeComponent();
        }

        // 8.1 不可变栈和队列
        private void button1_Click(object sender, EventArgs e)
        {
            /* 需要一个不会经常修改、可以被多个线程安全访问的栈或队列。例如，可用来表示一系列操作的队列，可用来表示一系列取消操作的栈。
             * 不可变栈和队列是最简单的不可变集合。它们的特征与标准的Stack<T>和Queue<T>非常相似。在性能上，不可变栈和队列与标准栈和队列具有一样的时间复杂度。
             *   但是在需要频繁修改的简单情况下，标准栈和队列的速度更快。
             * 栈是“后进先出”的数据结构。下面的代码创建一个空的不可变栈，接着压入两个项目，枚举这些项目，最后弹出项目：
             * 在上面的例子中，值得注意的是，程序对局部变量stack进行了覆盖。不可变集合采用的模式是返回一个修改过的集合，原始的集合引用是不变化的。
             *   这意味着，如果引用了特定的不可变集合的实例，它是不会变化的，具体可看下面的例子：
             * 两个栈实际上在内部共享了存储项目13的内存。这种实现方式的效率很高，并且可以很方便地创建当前状态的快照。每个不可变集合的实例都是绝对线程安全的，
             *   但也能在单线程程序中使用。根据我的经验，如果代码功能增加，或者需要存储很多快照并希望它们能尽可能多地共享内存，那不可变集合就特别有用。
             * 队列与栈类似，但队列是“先进先出”的数据结构。下面的代码创建一个空的不可变队列，然后加入两个项目，枚举这些项目，最后把项目从队列中取出：
             * 本节介绍了两个最简单的不可变集合：栈和队列。也介绍了几个适用于所有不可变集合的重要设计理念。
             *   · 不可变集合的一个实例是永远不改变的。
             *   · 因为不会改变，所以是绝对线程安全的。
             *   · 对不可变集合使用修改方法时，返回修改后的集合。
             * 不可变集合非常适用于共享状态，但不适合用来做交换数据的通道。特别是在线程间的通信中不要使用不可变队列，可以改用生产者/消费者队列，以获得更好的效果。
             * ImmutableStack<T> 和ImmutableQueue<T> 在NuGet包Microsoft.Bcl. Immutable中。
             */
            var stack = ImmutableStack<int>.Empty;
            stack = stack.Push(13);
            stack = stack.Push(7);
            // 先显示“7”，接着显示“13”。
            foreach (var item in stack)
                Trace.WriteLine(item);
            int lastItem;
            stack = stack.Pop(out lastItem);
            // lastItem == 7

            var stack1 = ImmutableStack<int>.Empty;
            stack1 = stack1.Push(13);
            var biggerStack1 = stack1.Push(7);
            // 先显示“7”，接着显示“13”。
            foreach (var item in biggerStack1)
                Trace.WriteLine(item);
            // 只显示“13”。
            foreach (var item in stack1)
                Trace.WriteLine(item);

            var queue = ImmutableQueue<int>.Empty;
            queue = queue.Enqueue(13);
            queue = queue.Enqueue(7);
            // 先显示“13”，接着显示“7”。
            foreach (var item in queue)
                Trace.WriteLine(item);
            int nextItem;
            queue = queue.Dequeue(out nextItem);
            // 显示“13”。
            Trace.WriteLine(nextItem);
        }

        // 8.2 不可变列表
        private void button2_Click(object sender, EventArgs e)
        {
            /* 需要一个这样的数据结构：支持索引，不经常修改，可以被多个线程安全访问。列表是多功能数据结构，可用于所有类型的程序状态。
             * 不可变列表确实可以索引，但需要注意性能问题。不能简单地用它来替代List<T>。ImmutableList<T>支持与List<T>类似的方法，看下面的例子：
             * 不可变列表的内部是用二叉树组织数据的。这么做是为了让不可变列表的实例之间共享的内存最大化。这导致ImmutableList<T>和List<T>在常用操作上有性能上的差别（参见表8-5）。
             * 如果在已有的代码中用ImmutableList<T>来代替List<T>，需要弄清楚算法逻辑是如何访问集合中元素的。这意味着应该尽量使用foreach而不是用for。
             *   对ImmutableList<T>进行foreach循环的耗时是O(N)，而对同一个集合进行for循环的耗时是O(N*logN)：
             * ImmutableList<T>是一种优秀的多功能数据结构，但因为有性能上的差异，不能盲目地用它来代替List<T>。默认情况下一般使用List<T>，就是说，通常都要使用List<T>，
             *   除非确实需要使用其他集合。ImmutableList<T>的使用就没那么普遍，需要仔细考虑其他不可变集合，并选择一个最合适的。
             *   ImmutableList<T>在NuGet包Microsoft.Bcl.Immutable中。
             */
            var list = ImmutableList<int>.Empty;
            list = list.Insert(0, 13);
            list = list.Insert(1, 7);
            // 先显示“7”，接着显示“13”。
            foreach (var item in list)
                Trace.WriteLine(item);
            list = list.RemoveAt(1);

            // 遍历ImmutableList<T>的最好方法。
            foreach (var item in list)
                Trace.WriteLine(item);
            // 这个方法运行正常，但速度会慢得多。
            for (int i = 0; i != list.Count; ++i)
                Trace.WriteLine(list[i]);
        }

        // 8.3 不可变Set集合
        private void button3_Click(object sender, EventArgs e)
        {
            /* 需要一个这样的数据结构：不需要存放重复内容，不经常修改，可以被多个线程安全访问。例如，文件的词汇索引就是使用Set集合的一个实例。
             * 有两种不可变Set集合类型：ImmutableHashSet<T>只是一个不含重复元素的集合，ImmutableSortedSet<T>是一个已排序的不含重复元素的集合。
             *   这两种Set集合类型都有相似的接口：
             * 只是已排序的Set集合可以使用索引访问，类似于列表：
             * 未排序的Set集合和已排序的Set集合，两者的性能差不多（见表8-6）。
             * 如果不是一定要排序，我建议大家使用未排序的Set集合。有些类型只支持判断是否相等，而不支持比较大小，因此未排序Set集合支持的类型比已排序Set集合要多得多。
             * 关于已排序Set集合有一点要特别注意，它索引操作的时间复杂度是O(log N)，而不是O(1)，这跟8.2节中ImmutableList<T>的情况类似。
             *   这意味着它们适用同样的警告：使用ImmutableSortedSet<T>时，应该尽量用foreach而不是用for。
             * 不可变Set集合是非常实用的数据结构，但是填充较大不可变Set集合的速度会很慢。大多数不可变集合有特殊的构建方法，可以先快速地以可变方式构建，然后转换成不可变集合。
             *   这种构建方法可用于很多不可变集合，但我发现对不可变Set集合是最有用的。
             * ImmutableHashSet<T> 和ImmutableSortedSet<T> 在NuGet包Microsoft.Bcl. Immutable中。
             */
            var hashSet = ImmutableHashSet<int>.Empty;
            hashSet = hashSet.Add(13);
            hashSet = hashSet.Add(7);
            // 显示“7”和“13”，次序不确定。
            foreach (var item in hashSet)
                Trace.WriteLine(item);
            hashSet = hashSet.Remove(7);

            var sortedSet = ImmutableSortedSet<int>.Empty;
            sortedSet = sortedSet.Add(13);
            sortedSet = sortedSet.Add(7);
            // 先显示“7”，接着显示“13”。
            foreach (var item in sortedSet)
                Trace.WriteLine(item);
            var smallestItem = sortedSet[0];
            // smallestItem == 7
            sortedSet = sortedSet.Remove(7);
        }

        // 8.4 不可变字典
        private void button4_Click(object sender, EventArgs e)
        {
            /* 需要一个不经常修改且可被多个线程安全访问的键/值集合。例如需要存储查询集合中的参考数据。这些参考数据很少修改但需要被不同的线程访问。
             * 有两种不可变字典类型：ImmutableDictionary<TKey, TValue>和ImmutableSortedDictionar y<TKey, TValue>。也许你已经猜到了，
             *   ImmutableSortedDictionary确保它的元素是已经排序的，而ImmutableDictionary的元素次序是无法预知的。这两种集合类型的成员非常相似：
             * 注意SetItem的用法。在可变字典中，可以使用这样的语句：dictionary[key] =item。但是不可变字典必须返回一个更新后的不可变字典，因此用SetItem方法来代替：
             * 未排序字典和已排序字典在性能上差别不大，但是我建议大家使用未排序字典，除非是必须排序（见表8-7）。未排序字典的速度稍微快一点。
             *   而且未排序字典可以使用任何键类型，而已排序字典要求键的类型必须是完全可比较的。
             * 根据经验，字典是处理应用状态时很普遍又实用的工具。它能用在任何类型的键/值或查询。跟其他不可变集合一样，不可变字典有一个在元素较多时进行快速构建的机制。
             *   例如，想要在启动时装载初始参考数据，就可以使用这种构建机制构造出初始的不可变字典。相反，如果参考数据是在程序运行时逐步构建的，那可以使用常规的Add方法。
             */
            var dictionary = ImmutableDictionary<int, string>.Empty;
            dictionary = dictionary.Add(10, "Ten");
            dictionary = dictionary.Add(21, "Twenty-One");
            dictionary = dictionary.SetItem(10, "Diez");
            // 显示“10Diez”和“21Twenty-One”，次序不确定。
            foreach (var item in dictionary)
                Trace.WriteLine(item.Key + item.Value);
            var ten = dictionary[10];
            // ten == "Diez"
            dictionary = dictionary.Remove(21);

            var sortedDictionary = ImmutableSortedDictionary<int, string>.Empty;
            sortedDictionary = sortedDictionary.Add(10, "Ten");
            sortedDictionary = sortedDictionary.Add(21, "Twenty-One");
            sortedDictionary = sortedDictionary.SetItem(10, "Diez");
            // 先显示“10Diez”，接着显示“21Twenty-One”。
            foreach (var item in sortedDictionary)
                Trace.WriteLine(item.Key + item.Value);
            var ten1 = sortedDictionary[10];
            // ten1 == "Diez"
            sortedDictionary = sortedDictionary.Remove(21);
        }

        // 8.5 线程安全字典
        private void button5_Click(object sender, EventArgs e)
        {
            /* 需要有一个键/值集合，多个线程同时读写时仍能保持同步。例如一个简单的驻留内存缓存。
             * .NET中的ConcurrentDictionary<TKey, TValue>类是数据结构中的精品。它是线程安全的，混合使用了细粒度锁定和无锁技术，以确保绝大多数情况下能进行快速访问。
             *   熟悉它的API确实需要一定的时间。因为要处理多线程的并发访问，它与标准的Dictionary<TKey, TValue>类完全不同。但是一旦学完本节内容，你就会发现
             *   Concurrent Dictionary<TKey, TValue>是最实用的集合类型之一。
             * 首先我们来看如何在集合中写入一个数据。要设置一个键对应的值，可使用AddOrUpdate，如：
             * AddOrUpdate方法有些复杂，这是因为这个方法必须执行多个步骤，具体步骤取决于并发字典的当前内容。方法的第一个参数是键。第二个参数是一个委托，
             *   它把键（本例中为0）转换成添加到字典的值（本例中为“Zero”）。只有当字典中没有这个键时，这个委托才会运行。第三个参数也是一个委托，
             *   它把键（0）和原来的值转换成字典中修改后的值（“Zero”）。只有当字典中已经存在这个键时，这个委托才会运行。
             *   AddOrUpdate返回这个键对应的新值（与其中一个委托返回的值相同）。
             * 有一点会让大家非常吃惊：为使并发字典正常运行，AddOrUpdate可能要多次调用其中一个（或两个）委托。这种情况很少，但确实会发生。
             *   因此这些委托必须简单、快速，并且不能有副作用。也就是说，这些委托只能创建新的值，不能修改程序中其他变量。
             *   这个原则适用于所有ConcurrentDictionary<TKey, TValue>的方法所使用的委托。
             * 因为要处理线程安全方面的所有问题，这部分内容是比较难的。其余的API就简单多了。其实还有几种方法可以向字典中添加数据。一个简便方法是使用索引语法：
             * 索引语法的功能相对较弱，它不支持根据当前的值进行修改的方法。如果把数据直接存入字典，那使用这种语法会更简单，效果也不错。
             * 来看一下如何读取值。很简单，使用TryGetValue就行：
             * 需要注意，有多个线程在对并发字典进行读取、修改、添加和删除值的操作。不试着读取一下，很多情况下是很难确定某个键是否存在的。
             * 删除值的操作跟读取一样简单：
             * TryRemove几乎和TryGetValue一样，除了（当然了）它是进行删除操作的，如果键存在，就删除“键/值”对。
             * 我觉得ConcurrentDictionary<TKey, TValue>是一个很好的类，主要是因为有功能特别强大的AddOrUpdate方法。但是它并不适合于所有场合。
             *   如果多个线程读写一个共享集合，使用ConcurrentDictionary<TKey, TValue>是最合适的。如果不会频繁修改（很少修改），
             *   那更适合使用ImmutableDictionary<TKey,TValue>。
             * ConcurrentDictionary<TKey, TValue>最适合用在需要共享数据的场合，即多个线程共享同一个集合。如果一些线程只添加元素，另一些线程只移除元素，
             *   那最好使用生产者/消费者集合。
             * ConcurrentDictionary<TKey, TValue>并不是唯一的线程安全集合。BCL库还提供了ConcurrentStack<T>、ConcurrentQueue<T>和ConcurrentBag<T>。
             *   不过它们很少单独使用，一般只是用来实现生产者/消费者集合，本章后面会介绍。
             */
            var dictionary = new ConcurrentDictionary<int, string>();
            var newValue = dictionary.AddOrUpdate(0,
                key => "Zero",
                (key, oldValue) => "Zero");

            // 使用与上一个例子同样的“字典”。
            // 添加（或修改）键0，对应值“Zero”。
            dictionary[0] = "Zero";

            // 使用与前面一样的“字典”。
            string currentValue;
            bool keyExists = dictionary.TryGetValue(0, out currentValue);

            // 使用与前面一样的“字典”。
            string removedValue;
            bool keyExisted = dictionary.TryRemove(0, out removedValue);
        }

        // 8.6 阻塞队列
        private void button6_Click(object sender, EventArgs e)
        {
            /* 需要有一个管道，在线程之间传递消息或数据。例如，一个线程正在装载数据，装载的同时把数据压进管道。与此同时，另一个线程在管道的接收端接收并处理数据。
             * .NET的BlockingCollection<T>类可用作这种管道。BlockingCollection<T>默认是阻塞队列，具有“先进先出”的特征。因为阻塞队列要被多个线程共用，
             *   通常把它定义成私有和只读：
             * 通常一个线程要么向集合中添加项目，要么移除项目，但不会两者都做。添加项目的线程为生产者线程，移除项目的线程为消费者线程。
             * 生产者线程通过调用Add方法来添加项目，在添加完成（即所有项目都已经添加完毕）后调用CommpleteAdding方法。这个方法通知集合，表示“没有更多的项目需要添加了”，
             *   然后该集合会通知消费者线程。在下面的简单例子中，生产者添加两个项目，然后做“完成”的标志：
             * 如果想要有多个消费者，可以在多个线程中同时调用GetConsumingEnumerable。每个项目只会传给其中的一个线程。当集合处理完毕后，这个枚举过程也结束。
             * 除非能保证消费者的速度总是比生产者快，使用这种管道时，都要考虑一旦生产者比消费者快，会发生什么情况。如果生产项目的速度比消费快，就需要对队列进行限流。
             *   BlockingCollection<T>类可以很方便地实现限流功能，可以在创建队列时设置限流的项目个数。下面的例子把集合的项目数量限制为1个：
             * 这样，同样的生产者代码的运行方式就会不同，具体看代码中的注释：
             * 前面例子中的消费者线程都使用了GetConsumingEnumerable方法。这是最常用的做法，但也可使用Take方法，它每次只会消费一个项目，而不是用一个循环使用所有的项目。
             * 如果有独立的线程（如线程池线程）作为生产者或消费者，阻塞队列就是一个十分不错的选择。如果要以异步方式访问管道，例如UI线程作为消费者，
             *   用阻塞队列就不大合适了。8.8节会介绍异步队列。
             * 如果准备在程序中使用这样的管道，可考虑改用TPL数据流库。在很多情况下，使用TPL数据流会比自己创建管道和后台线程要简单。特别是BufferBlock<T>可以作为阻塞队列使用。
             *   不过，并不是每个平台都支持TPL数据流的，对那些不支持TPL数据流的平台来说，选择阻塞队列是很适用的。
             * 如果要有最好的跨平台支持，也可以使用AsyncEx库中的AsyncProducerConsumerQueue<T>，它可以用作阻塞队列。表8-8列出了各平台对阻塞队列的支持情况。
             */
            blockingQueue.Add(7);
            blockingQueue.Add(13);
            blockingQueue.CompleteAdding(); // 消费者线程通常运行一个循环，等待下一个项目然后处理该项目。若使用上述生产者代码并放在独立的线程里（例如用Task.Run），就可以用下面的方法使用这些项目了：
                                            // 先显示“7”，后显示“13”。
            foreach (var item in blockingQueue.GetConsumingEnumerable())
                Trace.WriteLine(item);

            BlockingCollection<int> blockingQueue1 = new BlockingCollection<int>(boundedCapacity: 1);

            // 这个添加过程立即完成。
            blockingQueue1.Add(7);
            // 7被移除后，添加13才会完成。
            blockingQueue1.Add(13);
            blockingQueue1.CompleteAdding();
        }
        private readonly BlockingCollection<int> blockingQueue = new BlockingCollection<int>();

        // 8.7 阻塞栈和包
        private void button7_Click(object sender, EventArgs e)
        {
            /* 需要有一个管道，在线程之间传递消息或数据，但不想（或不需要）这个管道使用“先进先出”的语义。
             * 在默认情况下，.NET中的BlockingCollection<T>用作阻塞队列，但它也可以作为任何类型的生产者/消费者集合。BlockingCollection<T>实际上是对线程安全集合进行了封装，
             *   实现了IProducerConsumerCollection<T>接口。因此可以在创建BlockingCollection<T>实例时指明规则，可选择后进先出（栈）或无序（包），如下例所示：
             * 有一点很重要并需要引起注意，就是这时已经出现了有关项目次序的竞态条件。如果先运行生产者代码，后运行消费者代码，那项目的次序就和使用栈完全一样：
             * 但是如果生产者代码和消费者代码在不同的线程中（这是常见情况），消费者会一直取得最近加入的项目。例如，生产者加入7，接着消费者取走7，生产者加入13，
             *   接着消费者取走13。消费者在返回第一个项目前，不会等待生产者调用CompleteAdding。
             * 阻塞队列有关限流的注意事项，同样适用于阻塞栈和包。如果生产者的速度比消费者快，又要限制阻塞栈和包对内存的使用，就可以使用8.6节讨论过的限流方法。
             * 本节的消费者代码使用GetConsumingEnumerable，这是最常用的做法。但也可使用Take方法，它每次只会消费一个项目，而不是用一个循环消费掉所有的项目。
             * 如果不用阻塞方式，而是要用异步方式访问共享的栈或包（例如，UI线程作为消费者），请看8.9节。
             */
            BlockingCollection<int> blockingStack = new BlockingCollection<int>(
                new ConcurrentStack<int>());
            BlockingCollection<int> blockingBag = new BlockingCollection<int>(
                new ConcurrentBag<int>());

            // 生产者代码
            blockingStack.Add(7);
            blockingStack.Add(13);
            blockingStack.CompleteAdding();
            // 消费者代码
            // 先显示“13”，后显示“7”。
            foreach (var item in blockingStack.GetConsumingEnumerable())
                Trace.WriteLine(item);
        }

        // 8.8 异步队列
        private async void button8_Click(object sender, EventArgs e)
        {
            /* 需要有一个管道，在代码的各个部分之间以先进先出的方式传递消息或数据。例如，一段代码在加载数据，并向管道推送数据。同时UI线程在接收并显示数据。
             * 只需要一个带有异步API的队列。在.NET核心框架中没有这样的类，但是在NuGet中有好几个可以选择。
             * 第一个选择是使用TPL数据流库的BufferBlock<T>。下面的例子展示了声明BufferBlock<T>实例的方法、生产者代码和消费者代码的样式：
             *   BufferBlock<T>本身也支持限流，详见8.10节。
             *   例子中消费者代码使用了OutputAvailableAsync，这个方法其实只能用在仅有一个消费者的情况下。如果有多个消费者，即使队列中只有一个项目，
             *   OutputAvailableAsync也可能对每个消费者都返回true。如果队列中项目都取完了，ReceiveAsync会抛出Invalid OperationException异常。
             *   因此在有多个消费者时，消费者的代码通常更像下面这样：
             * 如果所用的平台支持TPL数据流，我建议大家使用BufferBlock<T>的方案。可惜并不是所有平台都支持TPL数据流。如果平台不支持BufferBlock<T>，
             *   那可以用NuGet包Nito.AsyncEx中的AsyncProducerConsumerQueue<T>类。它的API与BufferBlock<T>类似，但不完全相同：
             * AsyncProducerConsumerQueue<T> 具有限流功能，如果生产者的运行速度可能比消费者快，这个功能就是必需的。只要在构造队列时使用适当的参数即可：
             * 例子中的消费者代码也使用OutputAvailableAsync，并且也有跟BufferBlock<T>同样的问题。AsyncProducerConsumerQueue<T>类提供了TryDequeueAsync成员，
             *   可以用来避免冗长的消费者代码。如果有多个消费者，消费者代码通常像这样：
             * BufferBlock<T>和AsyncProducerConsumerQueue<T>相比，我更推荐使用前者，仅仅是因为对BufferBlock<T>进行的测试要完整得多。
             *   但是有很多平台不支持BufferBlock<T>，尤其是一些较老的平台（见表8-9）。
             */
            BufferBlock<int> asyncQueue = new BufferBlock<int>();
            // 生产者代码
            await asyncQueue.SendAsync(7);
            await asyncQueue.SendAsync(13);
            asyncQueue.Complete();
            // 消费者代码
            // 先显示“7”，后显示“13”。
            while (await asyncQueue.OutputAvailableAsync())
                Trace.WriteLine(await asyncQueue.ReceiveAsync());

            while (true)
            {
                int item;
                try
                {
                    item = await asyncQueue.ReceiveAsync();
                }
                catch (InvalidOperationException)
                {
                    break;
                }
                Trace.WriteLine(item);
            }

            AsyncProducerConsumerQueue<int> asyncQueue1 = new AsyncProducerConsumerQueue<int>();
            // 生产者代码
            await asyncQueue1.EnqueueAsync(7);
            await asyncQueue1.EnqueueAsync(13);
            asyncQueue1.CompleteAdding();
            // 消费者代码
            // 先显示“7”，后显示“13”。
            while (await asyncQueue1.OutputAvailableAsync())
                Trace.WriteLine(await asyncQueue1.DequeueAsync());
            
            AsyncProducerConsumerQueue<int> asyncQueue2 = new AsyncProducerConsumerQueue<int>(maxCount: 1);
            // 这个添加过程会立即完成。
            await asyncQueue2.EnqueueAsync(7);
            // 这个添加过程会（异步地）等待，直到7被移除，
            // 然后才会加入13。
            await asyncQueue2.EnqueueAsync(13);
            asyncQueue2.CompleteAdding();

            //while (true)
            //{
            //    var dequeueResult = await asyncQueue2.TryDequeueAsync();
            //    if (!dequeueResult.Success)
            //        break;
            //    Trace.WriteLine(dequeueResult.Item);
            //}
        }

        // 8.9 异步栈和包
        private async void button9_Click(object sender, EventArgs e)
        {
            /* 需要一个管道，用来在程序的各个部分之间传递消息或数据，但不希望（或不需要）这个管道使用“先进先出”的语义。
             * Nito.AsyncEx库提供了AsyncCollection<T>类，它默认表现为异步队列，但也可以作为任何类型的生产者/消费者集合。
             *   AsyncCollection<T>对IProducerConsumerCollection<T>进行了封装。AsyncCollection<T>相当于是异步版的.NET BlockingCollection<T>类。
             * AsyncCollection<T>支持后进先出（栈）或无序（包）的语义，取决于构造函数中传入什么集合：
             * 注意在栈的项目次序上有竞态条件。如果所有生产者完成后，消费者才开始运行，那项目的次序就像一个普通的栈：
             * 但是，当生产者和消费者都并发运行时（这是常见情况），消费者总是会得到最近加入的项目。这导致这个集合从整体上看不像是一个栈。当然了，包是根本没有次序的。
             * AsyncCollection<T>具有限流功能，如果生产者添加项目到集合的速度可能比消费者从集合取走项目的速度快，这个功能就是必需的。只要在构造集合时使用合适的值就行了：
             * 这样，同样的生产者代码会根据需要异步地等待了：
             * 例子中的消费者代码使用了OutputAvailableAsync，这个方法同样有如8.8节描写的那种限制。如果有多个消费者，消费者代码通常更像这样：
             */
            AsyncCollection<int> asyncStack = new AsyncCollection<int>(
                new ConcurrentStack<int>());
            AsyncCollection<int> asyncBag = new AsyncCollection<int>(
                new ConcurrentBag<int>());

            // 生产者代码
            await asyncStack.AddAsync(7);
            await asyncStack.AddAsync(13);
            asyncStack.CompleteAdding();
            // 消费者代码
            // 先显示“13”，后显示“7”。
            while (await asyncStack.OutputAvailableAsync())
                Trace.WriteLine(await asyncStack.TakeAsync());

            AsyncCollection<int> asyncStack1 = new AsyncCollection<int>(
                new ConcurrentStack<int>(), maxCount: 1);

            // 这个添加过程会立即完成。
            await asyncStack.AddAsync(7);
            // 这个添加（异步地）等待，直到7被移除，
            // 然后才会加入13。
            await asyncStack.AddAsync(13);
            asyncStack.CompleteAdding();

            //while (true)
            //{
            //    var takeResult = await asyncStack1.TryTakeAsync();
            //    if (!takeResult.Success)
            //        break;
            //    Trace.WriteLine(takeResult.Item);
            //}
        }

        // 8.10 阻塞/异步队列
        private async void button10_Click(object sender, EventArgs e)
        {
            /* 需要一个管道，用“先进先出”的方式在程序的各部分之间传递消息或数据。并且要有足够的灵活性，能以同步或异步方式来处理生产者终端或消费者终端。
             *   例如，一个后台线程在装载数据并把数据压入管道，我们希望当管道太满时该线程能同步地阻塞。同时，UI线程在从管道接收数据，
             *   我们希望这个线程异步地从管道拉取数据，以便UI保持响应。
             * 我们已经看了8.6节中的阻塞队列、8.8节中的异步队列，但是还有几种同时支持阻塞API和异步API的队列。首先是NuGet库TPL数据流中的BufferBlock<T>和ActionBlock<T>。
             *   BufferBlock<T>能很方便地用作异步的生产者/消费者队列（详见8.8节）：
             * BufferBlock<T>也有用于生产者和消费者的同步API：
             * 但是使用了BufferBlock<T>的消费者代码是十分笨拙的，因为这不是“数据流的风格”。TPL数据流库包含几个能互相连接的块，可以定义一个响应式网格。
             *   在本例中，可以用ActionBlock<T>定义一个带有特定动作的生产者/消费者队列：
             * 如果你选定的平台不支持TPL数据流库，那可以使用Nito.AsyncEx中的AsyncProducerConsumerQueue<T>类，它同时也支持同步和异步方法：
             * 虽然AsyncProducerConsumerQueue<T>支持更多的平台，我仍建议大家尽可能使用Buffer Block<T>或ActionBlock<T>，因为对TPL数据流库做过的测试比Nito.AsyncEx更加完整。
             * 像AsyncProducerConsumerQueue<T>这样的TPL数据流块也支持限流功能，可以通过传递构造函数的参数来实现此功能。如果生产者压入项目的速度比消费者消耗项目的速度快，
             *   就会导致程序占用大量的内存，这种情况下必须使用限流功能。表8-11列出了个平台对同步/异步队列的支持情况。
             */
            BufferBlock<int> queue = new BufferBlock<int>();
            // 生产者代码
            await queue.SendAsync(7);
            await queue.SendAsync(13);
            queue.Complete();
            // 单个消费者时的代码
            while (await queue.OutputAvailableAsync())
                Trace.WriteLine(await queue.ReceiveAsync());
            // 多个消费者时的代码
            while (true)
            {
                int item;
                try
                {
                    item = await queue.ReceiveAsync();
                }
                catch (InvalidOperationException)
                {
                    break;
                }
                Trace.WriteLine(item);
            }

            BufferBlock<int> queue1 = new BufferBlock<int>();
            // 生产者代码
            queue1.Post(7);
            queue1.Post(13);
            queue1.Complete();
            // 消费者代码
            while (true)
            {
                int item;
                try
                {
                    item = queue1.Receive();
                }
                catch (InvalidOperationException)
                {
                    break;
                }
                Trace.WriteLine(item);
            }

            // 消费者代码被传给队列的构造函数
            ActionBlock<int> queue2 = new ActionBlock<int>(item => Trace.WriteLine(item));
            // 异步的生产者代码
            await queue2.SendAsync(7);
            await queue2.SendAsync(13);
            // 同步的生产者代码
            queue2.Post(7);
            queue2.Post(13);
            queue2.Complete();

            AsyncProducerConsumerQueue<int> queue3 = new AsyncProducerConsumerQueue<int>();
            // 异步的生产者代码
            await queue3.EnqueueAsync(7);
            await queue3.EnqueueAsync(13);
            // 同步的生产者代码
            queue3.Enqueue(7);
            queue3.Enqueue(13);
            queue3.CompleteAdding();
            // 单个消费者时的异步代码
            while (await queue.OutputAvailableAsync())
                Trace.WriteLine(await queue3.DequeueAsync());
            // 多个消费者时的异步代码
            //while (true)
            //{
            //    var result = await queue3.TryDequeueAsync();
            //    if (!result.Success)
            //        break;
            //    Trace.WriteLine(result.Item);
            //}
            // 同步的消费者代码
            foreach (var item in queue3.GetConsumingEnumerable())
                Trace.WriteLine(item);
        }
    }
}
