﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace C4B.AsynchronousTask.Demo
{
    public class ThreadSynchronization
    {
        static int _Total = int.MaxValue;
        static int _Count = 0;
        readonly static object _Sync = new object();
        private static object? _Data;

        /// <summary>
        /// Listing 22.1: Unsynchronized State
        /// </summary>
        public static int _22_1_UnsynchronizedState(string[] args)
        {
            if (args?.Length > 0) { _ = int.TryParse(args[0], out _Total); }

            Console.WriteLine($"Increment and decrementing {_Total} times...");

            // Use Task.Factory.StartNew for .NET 4.0
            Task task = Task.Run(() => Decrement());

            // Increment
            for (int i = 0; i < _Total; i++)
            {
                _Count++;
            }

            task.Wait();
            Console.WriteLine($"Count = {_Count}");

            return _Count;
            /* 关于示例 22.1，需要注意的重要一点是输出不是 0。如果直接（按顺序）调用Decrement（）就会如此。但是，当异步调用 Decrement（） 时，
            *   会发生争用条件，因为 _Count++ 和 _Count-- 语句中的各个步骤混合在一起。
            * 示例 22.1 中的问题是争用条件，其中多个线程可以同时访问相同的数据元素。如此示例执行所示，允许多个线程访问相同的数据元素可能会破坏数据完整性，
            *   即使在单处理器计算机上也是如此。为了解决这个潜在的问题，代码需要围绕数据进行同步。同步代码或数据以供多个线程同时访问是线程安全的。
            */
        }
        public static void Decrement()
        {
            // Decrement
            for (int i = 0; i < _Total; i++)
            {
                _Count--;
            }
        }

        /// <summary>
        /// Listing 22.2: Unsynchronized Local Variables
        /// </summary>
        public static void _22_2UnsynchronizedLocalVariables(string[] args)
        {
            /* 在迭代之间共享局部变量的并行 for 循环会将变量暴露给并发访问和争用条件（参见 清单 22.2）。
             */
            int total = int.MaxValue;
            if (args?.Length > 0) { _ = int.TryParse(args[0], out total); }
            Console.WriteLine($"Increment and decrementing {total} times...");
            int x = 0;
            Parallel.For(0, total, i =>
            {
                x++;
                x--;
            });
            Console.WriteLine($"Count = {x}");
            /* 在这个例子中，x（一个局部变量）在并行的 for 循环中被访问，因此多个线程将同时修改它，从而创建一个与清单 22.1 中非常相似的争用条件。
             *   输出不太可能产生值 0，即使 x 递增和递减的次数相同。
             */
        }

        /// <summary>
        /// Listing 22.3: Synchronizing with a Monitor Explicitly
        /// </summary>
        public static void _22_3SynchronizingWithAMonitorExplicitly(string[] args)
        {
            /* 若要同步多个线程，以便它们无法同时执行特定的代码段，可以使用监视器阻止第二个线程在第一个线程退出受保护的代码部分之前进入该部分。
             *   监视器功能是名为 System.Threading.Monitor 的类的一部分，受保护代码部分的开头和结尾分别通过对静态方法 Monitor.Enter（） 和 Monitor.Exit（） 
             *   的调用进行标记。
             * 示例 22.3（结果显示在输出 22.2 中）显式演示了使用 Monitor 类进行同步。如此列表所示，重要的是调用 Monitor.Enter（） 和 Monitor.Exit（） 
             *   之间的所有代码都用 try/finally 块括起来。如果没有此块，受保护部分可能会发生异常，并且可能永远不会调用 Monitor.Exit（），从而无限期地阻塞其他线程。
             */
            if (args?.Length > 0) { _ = int.TryParse(args[0], out _Total); }
            Console.WriteLine($"Increment and decrementing {_Total} times...");

            // Use Task.Factory.StartNew for .NET 4.0
            Task task = Task.Run(() => Decrement3());

            // Increment
            for (int i = 0; i < _Total; i++)
            {
                bool lockTaken = false;
                try
                {
                    Monitor.Enter(_Sync, ref lockTaken);
                    _Count++;
                }
                finally
                {
                    if (lockTaken)
                    {
                        Monitor.Exit(_Sync);
                    }
                }
            }

            task.Wait();
            Console.WriteLine($"Count = {_Count}");
        }
        public static void Decrement3()
        {
            for (int i = 0; i < _Total; i++)
            {
                bool lockTaken = false;
                try
                {
                    Monitor.Enter(_Sync, ref lockTaken);
                    _Count--;
                }
                finally
                {
                    if (lockTaken)
                    {
                        Monitor.Exit(_Sync);
                    }
                }
            }
        }

        /// <summary>
        /// Listing 22.4: Synchronization Using the lock Keyword
        /// </summary>
        public static void _22_4SynchronizationUsingTheLockKeyword(string[] args)
        {
            /* 由于经常需要在多线程代码中使用 Monitor 进行同步，并且由于很容易忘记 try/finally 块，因此 C# 提供了一个特殊的关键字来处理此锁定同步模式。
             *   示例 22.4 演示了 lock 关键字的使用，输出 22.3 显示了结果。
             */
            if (args?.Length > 0) { _ = int.TryParse(args[0], out _Total); }
            Console.WriteLine($"Increment and decrementing {_Total} times...");

            // Use Task.Factory.StartNew for .NET 4.0
            Task task = Task.Run(() => Decrement4());

            // Increment
            for (int i = 0; i < _Total; i++)
            {
                lock (_Sync)
                {
                    _Count++;
                }
            }

            task.Wait();
            Console.WriteLine($"Count = {_Count}");
            /* 通过锁定访问_Count的代码部分（使用 lock 或 Monitor），可以使 Main（） 和 Decrement（） 方法线程安全，这意味着可以同时从多个线程安全地调用它们。
             * 同步的代价是性能降低。例如，示例 22.4 的执行时间比示例 22.1 长一个数量级，这表明与递增和递减计数的执行相比，lock 的执行速度相对较慢。
             */
        }
        public static void Decrement4()
        {
            for (int i = 0; i < _Total; i++)
            {
                lock (_Sync)
                {
                    _Count--;
                }
            }
        }

        /// <summary>
        /// Listing 22.5: async Main() with C# 7.1
        /// </summary>
        /// <param name="args"></param>
        public static async void _22_5AsyncMainWithC_7_1(string[] args)
        {
            /* 在清单 22.1 中，尽管 Task.Run（（） => Decrement（）） 返回了一个 Task，但没有使用 await 运算符。原因是在 C# 7.1 之前，Main（） 不支持使用 async。
             *   但是，给定 C# 7.1，可以重构代码以使用 async/await 模式，如清单 22.5 所示。
             */
            if (args?.Length > 0) { _ = int.TryParse(args[0], out _Total); }
            Console.WriteLine($"Increment and decrementing {_Total} times...");

            // Use Task.Factory.StartNew for .NET 4.0
            Task task = Task.Run(() => Decrement4());

            // Increment
            for (int i = 0; i < _Total; i++)
            {
                lock (_Sync)
                {
                    _Count++;
                }
            }

            await task;
            Console.WriteLine($"Count = {_Count}");
        }

        /// <summary>
        /// Listing 22.6: Synchronization Using System.Threading.Interlocked
        /// </summary>
        public static void _22_6SynchronizationUsingSystem_Threading_Interlocked(object newValue)
        {
            /* 示例 22.6 将_Data设置为新值，只要前面的值为 null。如方法名称所示，此模式是比较/交换模式。Interlocked.CompareExchange（） 方法
             *   不是手动在行为等效的比较和交换代码周围放置锁，而是为同步操作提供了一个内置方法，该方法对值 （null） 执行相同的检查，
             *   并在第一个参数等于第二个参数时更新该参数。表 22.2 显示了互锁支持的其他同步方法。
             */
            // If _Data is null then set it to newValue
            Interlocked.CompareExchange(
                ref _Data, newValue, null);

            int location1 = 1;
            int value = 2;
            int comparand = 3;
            Console.WriteLine("运行前：");
            Console.WriteLine($" location1 = {location1}    |   value = {value} |   comparand = {comparand}");
            Console.WriteLine("当 location1 != comparand 时");
            int result = Interlocked.CompareExchange(ref location1, value, comparand);
            Console.WriteLine($" location1 = {location1} | value = {value} |  comparand = {comparand} |  location1 改变前的值  {result}");
            Console.WriteLine("当 location1 == comparand 时");
            comparand = 1;
            result = Interlocked.CompareExchange(ref location1, value, comparand);
            Console.WriteLine($" location1 = {location1} | value = {value} |  comparand = {comparand} |  location1 改变前的值  {result}");
            /* 比较两个值是否相等，如果相等，则替换第一个值。
             * 属实没看懂。。。
             */
        }

        /// <summary>
        /// Listing 22.9: Creating a Single Instance Application
        /// </summary>
        public static void _22_9CreatingASingleInstanceApplication()
        {
            /* 除了 System.Threading.Monitor 和 System.Threading.Interlocked 之外，还有几种同步技术可用。
             * System.Threading.Mutex 在概念上类似于 System.Threading.Monitor 类（不支持 Pulse（） 方法），只是 lock 关键字不使用它，并且可以命名互斥体，
             *   以便它们支持跨多个进程的同步。使用 Mutex 类，可以同步对文件或某些其他跨进程资源的访问。由于互斥是跨进程资源，.NET 2.0 增加了支持，
             *   允许通过 System.Security.AccessControl.MutexSecurity 对象设置访问控制。Mutex 类的一个用途是限制应用程序，使其不能同时运行多次，
             *   如清单 22.9 所示的 Output 22.4。
             */
            // Obtain the mutex name from the full 
            // assembly name.
            string mutexName =
                Assembly.GetEntryAssembly()!.FullName!;

            // firstApplicationInstance indicates

            // whether this is the first
            // application instance.
            using Mutex mutex = new Mutex(false, mutexName,
                 out bool firstApplicationInstance);

            if (!firstApplicationInstance)
            {
                Console.WriteLine(
                    "This application is already running.");
            }
            else
            {
                Console.WriteLine("ENTER to shut down");
                Console.ReadLine();
            }
            /* 在这种情况下，应用程序只能在计算机上运行一次，即使它由不同的用户启动也是如此。若要将实例限制为每个用户一次，请在分配互斥 Name 时
             *   添加 System.Environment.UserName（需要 .NET Framework 或 .NET Standard 2.0 Microsoft）作为后缀。
             */
        }

        static ManualResetEventSlim _MainSignaledResetEvent;
        static ManualResetEventSlim _DoWorkSignaledResetEvent;
        public static void DoWork()
        {
            Console.WriteLine("DoWork() started....");
            _DoWorkSignaledResetEvent.Set();
            _MainSignaledResetEvent.Wait();
            Console.WriteLine("DoWork() ending....");
        }
        /// <summary>
        /// Listing 22.10: Waiting for ManualResetEventSlim
        /// </summary>
        public static void _22_10WaitingForManualResetEventSlim()
        {
            /* 控制线程中的特定指令相对于另一个线程中的指令何时执行的不确定性的一种方法是使用重置事件。尽管使用了术语事件，但重置事件与 C# 委托和事件无关。
             *   相反，重置事件是一种强制代码等待另一个线程执行的方法，直到另一个线程发出信号。它们对于测试多线程代码特别有用，因为在验证结果之前可以等待特定状态。
             * 重置事件类型是 System.Threading.ManualResetEvent 和 Microsoft .NET Framework 4 添加的轻量级版本 System.Threading.ManualResetEventSlim。
             *   (正如即将发布的高级主题：优先使用手动重置事件和信号量而不是自动重置事件所讨论的那样，还有第三种类型，System.Threading.AutoResetEvent，
             *   但程序员应该避免使用它，而支持前两种类型之一。) 重置事件的关键方法是 Set（） 和 Wait（）（在 ManualResetEvent 上称为 WaitOne（） ）。
             *   调用 Wait（） 方法将导致线程阻塞，直到另一个线程调用 Set（） 或等待期超时。示例 22.10 演示了它是如何工作的，输出 22.6 显示了结果。
             */
            using (_MainSignaledResetEvent = new ManualResetEventSlim())
            using (_DoWorkSignaledResetEvent = new ManualResetEventSlim())
            {
                Console.WriteLine("Application started....");
                Console.WriteLine("Starting task....");

                // Use Task.Factory.StartNew for .NET 4.0
                Task task = Task.Run(() => DoWork());

                // Block until DoWork() has started
                _DoWorkSignaledResetEvent.Wait();
                Console.WriteLine(" Waiting while thread executes...");
                _MainSignaledResetEvent.Set();
                task.Wait();
                Console.WriteLine("Thread completed");
                Console.WriteLine("Application shutting down....");
            }
            /* Output 22.6
                Application started....
                Starting thread....
                DoWork() started....
                Waiting while thread executes...
                DoWork() ending....
                Thread completed
                Application shutting down....
             * 调用重置事件的 Wait（） 方法（对于 ManualResetEvent，此方法称为 WaitOne（））会阻止调用线程，直到另一个线程发出信号并允许被阻塞的线程继续。
             *   Wait（）/WaitOne（） 覆盖不是无限期地阻止，而是包含一个参数（以毫秒为单位）或作为 TimeSpan 对象，用于阻止的最大时间量。
             *   指定超时期限时，如果在发出重置事件信号之前发生超时，则来自 WaitOne（） 的返回将为 false。ManualResetEvent.Wait（） 还包括一个采用取消令牌的版本，
             *   允许第 19 章中讨论的取消请求。
             * ManualResetEventSlim 和 ManualResetEvent 之间的区别在于，后者默认使用内核同步，而前者经过优化以避免访问内核，除非作为最后的手段。
             *   因此，ManualResetEventSlim的性能更高，即使它可能使用更多的CPU周期。因此，您通常应该使用 ManualResetEventSlim，除非需要等待多个事件或跨进程。
             * 请注意，重置事件实现了 IDisposable，因此当不再需要它们时，应释放它们。在示例 22.10 中，我们通过 using 语句执行此操作。
             *   （CancelTokenSource包含一个ManualResetEvent，这就是为什么它也实现了IDisposable。)
             */
        }

        /* Table 22.4: Concurrent Collection Classes 表 22.4.并发集合类
         *   ·BlockingCollection<T> 提供一个阻塞集合，支持生成者/使用者方案，其中生成者将数据写入集合，而使用者读取数据。此类提供一种泛型集合类型，
         *    该类型同步添加和删除操作，而无需考虑后端存储（无论是队列、堆栈、列表还是其他存储）。BlockingCollection 为实现 IProducerConsumerCollection 
         *    接口的集合提供阻塞和绑定支持。
         *   ·ConcurrentBag<T> T 类型对象的线程安全无序集合。
         *   ·ConcurrentDictionary<TKey, TValue> 线程安全字典;键和值的集合。
         *   ·ConcurrentQueue<T>* 支持类型 T 对象上的先进先出 （FIFO） 语义的线程安全队列。
         *   ·ConcurrentStack<T>* 支持类型 T 对象上的先进后出 （FILO） 语义的线程安全堆栈。
         */

        static ThreadLocal<double> _Count224 = new ThreadLocal<double>(() => 0.01134);
        public static double Count224
        {
            get { return _Count224.Value; }
            set { _Count224.Value = value; }
        }
        public static void Decrement224()
        {
            Count224 = -Count224;
            for (double i = 0; i < short.MaxValue; i++)
            {
                Count224--;
            }
            Console.WriteLine(
                "Decrement Count = {0}", Count224);
        }
        /// <summary>
        /// Listing 22.11: Using ThreadLocal<T> for Thread Local Storage
        /// </summary>
        public static void _22_11UsingThreadLocal_T_ForThreadLocalStorage()
        {
            /* 在某些情况下，使用同步锁可能会导致不可接受的性能和可伸缩性限制。在其他情况下，围绕特定数据元素提供同步可能过于复杂，尤其是在原始编码之后添加同步时。
             * 同步的一种替代解决方案是隔离，实现隔离的一种方法是线程本地存储。使用线程本地存储，每个线程都有自己的专用变量实例。在此方案中，不需要同步，
             *   因为同步仅在单个线程上下文中发生的数据是没有意义的。线程本地存储实现的两个示例是 ThreadLocal 和 ThreadStaticAttribute。
             * 将线程本地存储与 Microsoft .NET Framework 4 或更高版本一起使用涉及声明 ThreadLocal 类型的字段（或变量，在编译器关闭的情况下）。
             *   结果是每个线程的字段的不同实例，如清单 22.11 和输出 22.7 所示。请注意，即使字段是静态的，也存在不同的实例。
             */
            Thread thread = new Thread(Decrement224);
            thread.Start();

            // Increment
            for (double i = 0; i < short.MaxValue; i++)
            {
                Count224++;
            }
            thread.Join();
            Console.WriteLine("Main Count = {0}", Count224);
            /* Output 22.7
                Decrement Count = -32767.01134
                Main Count = 32767.01134
             * 如输出 22.7 所示，执行 Main（） 的线程的 Count 值永远不会被执行 Decrement（） 的线程递减。对于 Main（） 的线程，初始值为 0.01134，最终值为 32767.01134。
             *   Decrement（） 具有类似的值，只是它们是负数。由于 Count 基于 ThreadLocal 类型的静态字段，因此运行 Main（） 的线程和运行 Decrement（） 的线程在 
             *   _Count.Value 中存储了独立的值。
             */
        }

        [ThreadStatic]
        static double _Count2212 = 0.01134;
        public static double Count2212
        {
            get { return ThreadSynchronization._Count2212; }
            set { ThreadSynchronization._Count2212 = value; }
        }
        public static void Decrement2212()
        {
            for (int i = 0; i < short.MaxValue; i++)
            {
                Count2212--;
            }
            Console.WriteLine("Decrement Count = {0}", Count2212);
        }
        /// <summary>
        /// Listing 22.12: Using ThreadStaticAttribute for Thread Local Storage
        /// </summary>
        public static void _22_12UsingThreadStaticAttributeForThreadLocalStorage()
        {
            /* 具有 ThreadStaticAttribute 的线程本地存储
             * 使用 ThreadStaticAttribute 修饰静态字段，如清单 22.12 所示（结果显示在输出 22.8 中），是将静态变量指定为每个线程实例的第二种方法。
             *   此技术相对于 ThreadLocal 有一些注意事项，但也具有在 .NET Framework 4 之前可用的Microsoft优点。（此外，由于 ThreadLocal 基于 ThreadStaticAttribute，
             *   因此在足够频繁的重复小迭代中，它将消耗更少的内存并提供轻微的性能优势。)
             */
            Thread thread = new Thread(Decrement2212);
            thread.Start();

            // Increment
            for (int i = 0; i < short.MaxValue; i++)
            {
                Count2212++;
            }

            thread.Join();
            Console.WriteLine("Main Count = {0}", Count2212);
            /* Output 22.8
                Decrement Count = -32767
                Main Count = 32767.01134
             * 如清单 22.11 所示，执行 Main（） 的线程的 Count 值永远不会被执行 Decrement（） 的线程递减。对于 Main（） 的线程，初始值为负_Total，
             *   最终值为 0。换句话说，使用 ThreadStaticAttribute 时，每个线程的 Count 值特定于线程，不能跨线程访问。
             * 请注意，与清单 22.11 不同，为递减计数显示的值没有任何十进制数字，表明它从未初始化为 0.01134。尽管 _Count 的值是在声明期间分配的
             *   （在此示例中为 private 双_Count = 0.01134），但只会初始化与运行静态构造函数的线程关联的线程静态实例。在清单 22.12 中，只有执行 Main（） 
             *   的线程才会有一个初始化为 0.01134 的线程本地存储变量。_Count Decrement（） 递减的值将始终初始化为 0（默认值（double），因为 _Count 是双精度值）。
             *   同样，如果构造函数初始化线程本地存储字段，则只有调用该线程的构造函数才会初始化线程本地存储实例。因此，最好在每个线程最初调用的方法中初始化线程本地存储字段。
             *   但是，这并不总是合理的，尤其是在与异步有关时：不同的计算片段可能在不同的线程上运行，从而导致每个片段上的线程本地存储值意外不同。
             */
        }

        /// <summary>
        /// Listing 22.13: Using Task.Delay() as a Timer
        /// </summary>
        public static async Task _22_13UsingTask_Delay_AsATimer(System.Threading.CancellationToken token)
        {
            /* 有时，需要将代码执行延迟特定时间段或在特定时间段后注册通知。示例包括按特定时间间隔刷新屏幕，而不是在频繁发生数据更改时立即刷新屏幕。
             *   实现计时器的一种方法是利用 async/await 模式和 .NET 4.5 中添加的 Task.Delay（） 方法。正如我们在第 19 章中指出的，
             *   TAP 的一个关键特性是异步调用后执行的代码将在支持的线程上下文中继续，从而避免任何 UI 跨线程问题。清单 22.13 提供了如何使用 Task.Delay（） 方法的示例。
             */
            for (int minute = 0; minute < 25; minute++)
            {
                DisplayMinuteTicker(minute);
                for (int second = 0; second < 60; second++)
                {
                    await Task.Delay(1000, token);
                    if (token.IsCancellationRequested)
                        break;
                    DisplaySecondTicker();
                }
                if (token.IsCancellationRequested)
                    break;
            }
            /* 对 Task.Delay（1000） 的调用将设置一个倒数计时器，该计时器在 1 秒后触发并执行其后显示的继续代码。
             * 幸运的是，TAP对同步上下文的使用专门解决了在UI线程上执行UI相关代码的问题。在此之前，必须使用特定的计时器类，这些类是 UI 线程安全的，或者可以这样配置。
             *   System.Windows.Forms.Timer、System.Windows.Threading.DispatcherTimer 和 System.Timers.Timer 等计时器（如果配置正确）对 UI 线程友好。
             *   其他的，如System.Threading.Timer，则针对性能进行了优化。
             */
        }
        private static void DisplaySecondTicker()
        {
            // ...
            throw new NotImplementedException();
        }
        private static void DisplayMinuteTicker(int minute)
        {
            // ...
            throw new NotImplementedException();
        }
        /* Chapter 22: Summary
         * 在本章中，我们研究了各种同步机制，并了解了如何使用各种类来防止竞争条件。覆盖范围包括锁定关键字，该关键字在幕后利用System.Threading.Monitor。
         *   其他同步类包括 System.Threading.Interlocked、System.Threading.Mutext、System.Threading.WaitHandle、reset events、semaphores 和并发集合类。
         * 尽管在改进早期版本的 .NET 和今天的多线程编程方面取得了所有进展，但多线程编程的同步仍然很复杂，许多陷阱等待着粗心的开发人员。为了避免这些沙坑，
         *   已经确定了几种最佳实践，包括以相同的顺序一致地获取同步目标，并使用同步逻辑包装静态成员。
         * 在本章结束之前，我们研究了 Task.Delay（） 方法，这是一个 .NET 4.5 引入的 API，用于实现基于 TAP 的计时器。
         */
    }
}
