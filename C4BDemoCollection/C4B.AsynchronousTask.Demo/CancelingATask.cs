﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace C4B.AsynchronousTask.Demo
{
    public class CancelingATask
    {
        /// <summary>
        /// Listing 19.8: Canceling a Task Using CancellationToken
        /// </summary>
        public static void _19_8CancelingATaskUsingCancellationToken()
        {
            /* 在本章前面，我们描述了为什么粗鲁地中止线程以取消该线程正在执行的任务是一个坏主意。TPL 使用协作取消，这是一种更礼貌、更强大、更可靠的技术，
             *   用于安全地取消不再需要的任务。支持取消的任务通过定期轮询 CancelToken 对象以查看是否已发出取消请求来监视该对象（位于 System.Threading 命名空间中）。
             *   清单 19.8 演示了取消请求和对请求的响应。输出 19.5 显示了结果。
             */
            string stars = "*".PadRight(Console.WindowWidth - 1, '*');
            Console.WriteLine("Push ENTER to exit.");

            CancellationTokenSource cancellationTokenSource =
                new CancellationTokenSource();
            // Use Task.Factory.StartNew<string>() for
            // TPL prior to .NET 4.5
            Task task = Task.Run(
                () =>
                    WritePi(cancellationTokenSource.Token),
                        cancellationTokenSource.Token);

            // Wait for the user's input
            Console.ReadLine();

            cancellationTokenSource.Cancel();
            Console.WriteLine(stars);
            task.Wait();
            Console.WriteLine();
            /* Output 19.5
                Push ENTER to exit.
                3.141592653589793238462643383279502884197169399375105820974944592307816
                40628620899862803482534211706798214808651328230664709384460955058223172
                5359408128481117450
                **********************************************************************
                2
             */
            /* 启动任务后，Console.Read（） 会阻塞主线程。同时，任务继续执行，计算 pi 的下一个数字并将其打印出来。用户按 Enter 后，
             *   执行会遇到对 CancelTokenSource.Cancel（） 的调用。在示例 19.8 中，我们拆分了对任务的调用。从调用到任务的 Cancel（）。Wait（） 并在两者之间打印出一行星号。
             *   此步骤的目的是表明在观察到取消令牌之前很可能会发生额外的迭代 - 因此输出 19.5 中在星星之后的额外 2。
             *   出现 2 是因为 CancelTokenSource.Cancel（） 不会粗鲁地阻止任务执行。任务将继续运行，直到它检查令牌，并在看到令牌的所有者请求取消任务时礼貌地关闭。
             * 调用 Cancel（） 有效地对从 CancellationTokenSource.Token 复制的所有取消令牌上设置 IsCancellationRequested 属性。但是，有几点需要注意：
             *   ·是CancellationToken，而不是CancellationTokenSource，被赋予异步任务。CancellationToken 允许轮询取消请求;CancellationTokenSource 提供令牌，
             *    并在取消令牌时发出信号（参见图 19.3）。通过传递 CancellationToken 而不是 CancellationTokenSource，我们不必担心CancellationTokenSource
             *    上的线程同步问题，因为后者仍然只能由原始线程访问。
             *   ·CancellationToken 是一个结构，因此它是按值复制的。CancellationTokenSource.Token 返回的值生成令牌的副本。事实上，CancellationToken  是一种值类型，
             *    并且创建了副本，因此可以线程安全地访问 CancellationTokenSource.Token，它只能在 WritePi（） 方法中使用。
             */
        }

        /// <summary>
        /// Listing 19.9 provides an example of using Task.Factory.StartNew().
        /// </summary>
        /// <param name="digits"></param>
        /// <returns></returns>
        public static Task<string> CalculatePiAsync(int digits)
        {
            /* 给定 .NET 4.5，默认情况下应使用 Task.Run（），除非它证明是不够的。例如，如果需要使用 TaskCreationOptions 控制任务，如果需要指定备用调度程序，
             *   或者出于性能原因，要传入对象状态，则应考虑使用 Task.Factory.StartNew（）。只有在极少数情况下，需要将创建与调度分开，才应考虑构造函数实例化，然后调用 Start（）。
             */
            return Task.Factory.StartNew<string>(
                () => CalculatePi(digits));
        }

        private static string CalculatePi(int digits)
        {
            // TODO 此示例使用 PiCalculator.Calculate（） 方法，我们将在第 21 章的“并行执行循环迭代”一节中进一步深入探讨。
            Thread.Sleep(200);
            return "111111111";
        }

        /// <summary>
        /// Listing 19.10: Cooperatively Executing Long-Running Tasks
        /// </summary>
        public static void _19_10CooperativelyExecutingLongRunningTasks()
        {
            /* 线程池假定工作项将受处理器限制且生存期相对较短;它做出这些假设是为了有效地限制创建的线程数。这可以防止过度分配昂贵的线程资源和超额订阅处理器，
             *   从而导致过多的上下文切换和时间切片。但是，如果开发人员知道任务将长时间运行，因此将长期保留基础线程资源，该怎么办？在这种情况下，
             *   开发人员可以通知调度程序任务不太可能很快完成其工作。这有两个效果。首先，它向调度程序提示，也许应该专门为此任务创建一个专用线程，
             *   而不是尝试使用线程池中的线程。其次，它向调度程序暗示，也许这是允许调度比处理器处理它们更多的任务的好时机。这将导致更多的时间切片发生，这是一件好事。
             *   我们不希望一个长时间运行的任务占用整个处理器并阻止运行较短的任务使用它。短期运行的任务将能够使用其时间片来完成大部分工作，
             *   并且长时间运行的任务不太可能注意到与其他任务共享处理器而导致的相对轻微的延迟。为此，请在调用 StartNew（） 时使用 TaskCreationOptions.LongRun 选项，
             *   如清单 19.10 所示。（Task.Run（） 不支持 TaskCreationOptions 参数。
             */
            CancellationTokenSource cancellationTokenSource =
                new CancellationTokenSource();
            Task task = Task.Factory.StartNew(
                () =>
                    WritePi(cancellationTokenSource.Token),
                        TaskCreationOptions.LongRunning);
            /* 指南：·务必通知任务工厂新创建的任务可能长时间运行，以便它可以适当地管理它。·请谨慎使用 TaskCreationOptions.LongRunning。
             * 请注意，任务还支持 IDisposable。这是必需的，因为任务可能会在等待它完成时分配一个等待句柄;由于 WaitHandle 支持 IDisposable，
             *   因此 Task 也根据最佳实践支持 IDisposable。但是，请注意，前面的代码示例不包括 Dispose（） 调用，也不依赖于通过 using 语句隐式调用。
             *   相反，列表依赖于程序退出时的自动 WaitHandle 终结器调用。
             * 这种方法导致了两个值得注意的结果。首先，句柄的寿命更长，因此消耗的资源比应有的要多。其次，垃圾回收器的效率略低，因为最终的对象会存活到下一代。
             *   但是，在任务案例中，这两个问题都是无关紧要的，除非正在完成大量任务。因此，即使从技术上讲，所有代码都应该释放任务，但除非性能指标需要，否则您不必费心这样做，
             *   而且这很容易 - 也就是说，如果您确定任务已完成并且没有其他代码正在使用它们。
             */
        }

        private static void WritePi(CancellationToken cancellationToken)
        {
            const int batchSize = 1;
            string piSection = string.Empty;
            int i = 0;

            while (!cancellationToken.IsCancellationRequested
                || i == int.MaxValue)
            {
                piSection = PiCalculator.Calculate(batchSize, (i++) * batchSize);
                Console.Write(piSection);
            }
        }
    }
}
