﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.ExceptionServices;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace C4B.AsynchronousTask.Demo
{
    /* 正如我们在第 19 章中看到的，任务为异步工作的操作提供了一个抽象。任务自动调度到正确数量的线程，大任务可以通过将小任务链接在一起来组合，
     *   就像大程序可以由多个小方法组成一样。但是，任务也存在一些缺点。任务的主要困难在于它们将程序逻辑“由内而外”地转动。为了说明这一点，我们从同步方法开始本章，
     *   该方法在 I/O 密集型高延迟操作（Web 请求）上被阻止。然后，我们通过利用 async/await 上下文关键字1 来修改此方法，演示异步代码的创作和可读性方面的显著简化。
     * 在本章结束时，我们将介绍异步流（C# 8.0 引入的功能），用于定义和利用异步迭代器。
     */
    public class ProgrammingTaskBasedAsynchronousPattern
    {
        public const string DefaultUrl = "https://IntelliTect.com";

        /// <summary>
        /// Listing 20.1: A Synchronous Web Request
        /// </summary>
        public static void _20_1ASynchronousWebRequest(string[] args)
        {
            /* Synchronously Invoking a High-Latency Operation 同步调用高延迟操作
             * 在示例 20.1 中，代码使用 WebClient 下载网页并搜索某些文本出现的次数。输出 20.1 显示了结果。
             */
            if (args.Length == 0)
            {
                Console.WriteLine("ERROR: No findText argument specified.");
                return;
            }
            string findText = args[0];

            string url = DefaultUrl;
            if (args.Length > 1)
            {
                url = args[1];
                // Ignore additional parameters
            }
            Console.WriteLine(
                $"Searching for '{findText}' at URL '{url}'.");

            Console.WriteLine("Downloading...");
            WebClient webClient = new WebClient();
            byte[] downloadData =
                webClient.DownloadData(url);

            Console.WriteLine("Searching...");
            int textOccurrenceCount = CountOccurrences(
                downloadData, findText);

            Console.WriteLine(
                @$"'{findText}' appears {textOccurrenceCount} times at URL '{url}'.");
            /* 示例 20.1 中的逻辑相对简单 — 使用常见的 C# 习惯用法。确定 url 和 findText 值后，Main（） 调用 CountOccurrences（），实例化 WebClient，
             *   并调用同步方法 DownloadData（） 来下载内容。给定下载的数据，它将此数据传递给 CountOccurrences（），后者将其加载到 MemoryStream 中，
             *   并利用 StreamReader 的 Read（） 方法来检索数据块并搜索 findText 值。（我们使用 DownloadData（） 方法而不是更简单的 DownloadString（） 方法，
             *   以便在从示例 20.2 和示例 20.3 中的流中读取时演示额外的异步调用。
             * 当然，这种方法的问题在于调用线程被阻塞，直到 I/O 操作完成;这浪费了一个线程，该线程可以在执行操作时执行有用的工作。
             *   因此，例如，我们不能执行任何其他代码，例如异步指示进度的代码。换句话说，“下载...”和“搜索...”在相应操作之前调用，而不是在操作期间调用。
             *   虽然这样做在这里无关紧要，但想象一下，我们想同时执行额外的工作，或者至少提供一个动画繁忙指示器。
             */
        }
        private static int CountOccurrences(byte[] downloadData, string findText)
        {
            int textOccurrenceCount = 0;

            MemoryStream stream = new MemoryStream(downloadData);
            StreamReader reader = new StreamReader(stream);

            int findIndex = 0;
            int length = 0;
            do
            {
                char[] data = new char[reader.BaseStream.Length];
                length = reader.Read(data, 0, 1000);
                for (int i = 0; i < length; i++)
                {
                    if (findText[findIndex] == data[i])
                    {
                        findIndex++;
                        if (findIndex == findText.Length)
                        {
                            // Text was found
                            textOccurrenceCount++;
                            findIndex = 0;
                        }
                    }
                    else
                    {
                        findIndex = 0;
                    }
                }
            }
            while (length != 0);

            return textOccurrenceCount;
        }

        /// <summary>
        /// Listing 20.2: An Asynchronous Web Request
        /// </summary>
        /// <param name="args"></param>
        public static void _20_2AnAsynchronousWebRequest2(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("ERROR: No findText argument specified.");
                return;
            }
            string findText = args[0];

            string url = DefaultUrl;
            if (args.Length > 1)
            {
                url = args[1];
                // Ignore additional parameters
            }
            Console.WriteLine(
                $"Searching for '{findText}' at URL '{url}'.");

            using WebClient webClient = new WebClient();
            Console.Write("Downloading...");
            Task task = webClient.DownloadDataTaskAsync(url)
                .ContinueWith(antecedent =>
                {
                    byte[] downloadData = antecedent.Result;
                    Console.Write($"{Environment.NewLine}Searching...");
                    return CountOccurrencesAsync2(
                        downloadData, findText);
                })
                .Unwrap()
                .ContinueWith(antecedent =>
                {
                    int textOccurrenceCount = antecedent.Result;
                    Console.WriteLine(
                         @$"{Environment.NewLine}'{findText}' appears {textOccurrenceCount} times at URL '{url}'.");

                });

            try
            {
                while (!task.Wait(100))
                {
                    Console.Write(".");
                }
            }
            catch (AggregateException exception)
            {
                exception = exception.Flatten();
                try
                {
                    exception.Handle(innerException =>
                    {
                        // Rethrowing rather than using
                        // if condition on the type
                        ExceptionDispatchInfo.Capture(
                            innerException)
                            .Throw();
                        return true;
                    });
                }
                catch (WebException)
                {
                    // ...
                }
                catch (IOException)
                {
                    // ...
                }
                catch (NotSupportedException)
                {
                    // ...
                }
            }
        }
        private static async Task<int> CountOccurrencesAsync2(byte[] downloadData, string findText)
        {
            int textOccurrenceCount = 0;

            MemoryStream stream = new MemoryStream(downloadData);
            StreamReader reader = new StreamReader(stream);

            int findIndex = 0;
            int length = 0;
            do
            {
                char[] data = new char[reader.BaseStream.Length];
                length = await reader.ReadAsync(data, 0, 1000);
                for (int i = 0; i < length; i++)
                {
                    if (findText[findIndex] == data[i])
                    {
                        findIndex++;
                        if (findIndex == findText.Length)
                        {
                            // Text was found
                            textOccurrenceCount++;
                            findIndex = 0;
                        }
                    }
                    else
                    {
                        findIndex = 0;
                    }
                }
            }
            while (length != 0);

            return textOccurrenceCount;
        }

        /// <summary>
        /// Listing 20.3: Asynchronous High-Latency Invocation with the Task-Based Asynchronous Pattern
        /// </summary>
        /// <param name="args"></param>
        public static async void _20_3AsynchronousHighLatencyInvocationWithTheTaskBasedAsynchronousPattern(string[] args)
        {
            /* 为了解决复杂性问题，清单 20.3 还提供了异步执行，但使用了基于任务的异步，它利用了 async/await 特性。使用 async/await，编译器可以处理复杂性，
             *   使开发人员能够专注于业务逻辑。而不是担心链接 ContinueWith（） 语句，检索前因。结果、Unwrap（） 调用、复杂错误处理等，
             *   async/await 允许您使用简单的语法来装饰代码，通知编译器它应该处理复杂性。此外，当任务完成并且需要执行其他代码时，编译器会自动负责调用相应线程上的剩余代码。
             *   例如，不能让两个不同的线程与单个线程 UI 平台交互，而 async/await 可解决此问题。（请参阅本章后面有关在 Windows UI 中使用 async/await 功能的部分。
             * 换句话说，async/await 语法告诉编译器在编译时重新组织代码，即使代码编写得相对简单，并解决开发人员必须明确考虑的无数复杂性 — 参见 Falls 20.3。
             */
            if (args.Length == 0)
            {
                Console.WriteLine("ERROR: No findText argument specified.");
                return;
            }
            string findText = args[0];

            string url = DefaultUrl;
            if (args.Length > 1)
            {
                url = args[1];
                // Ignore additional parameters
            }
            Console.WriteLine(
                $"Searching for '{findText}' at URL '{url}'.");

            using WebClient webClient = new WebClient();
            Task<byte[]> taskDownload =
                webClient.DownloadDataTaskAsync(url);

            Console.Write("Downloading...");
            while (!taskDownload.Wait(100))
            {
                Console.Write(".");
            }

            byte[] downloadData = await taskDownload;

            Task<int> taskSearch = CountOccurrencesAsync2(
             downloadData, findText);

            Console.Write($"{Environment.NewLine}Searching...");

            while (!taskSearch.Wait(100))
            {
                Console.Write(".");
            }

            int textOccurrenceCount = await taskSearch;

            Console.WriteLine(
                @$"{Environment.NewLine}'{findText}' appears {textOccurrenceCount} times at URL '{url}'.");
        }

        ///// <summary>
        ///// Listing 20.4: Returning ValueTask<T> from an async Method
        ///// ValueTask适用于.NET	Core 2.1, Core 2.2, Core 3.0, Core 3.1, 5, 6, 7, 8；.NET Standard	2.1
        ///// </summary>
        ///// <param name="buffer"></param>
        ///// <returns></returns>
        //public static async ValueTask<byte[]> CompressAsync(byte[] buffer)
        //{
        //    if (buffer.Length == 0)
        //    {
        //        return buffer;
        //    }
        //    using MemoryStream memoryStream = new MemoryStream();
        //    using System.IO.Compression.GZipStream gZipStream =
        //        new System.IO.Compression.GZipStream(
        //            memoryStream,
        //                System.IO.Compression.CompressionMode.Compress);
        //    await gZipStream.WriteAsync(buffer.AsMemory(0, buffer.Length));

        //    return memoryStream.ToArray();
        //}

        ///// <summary>
        ///// Listing 20.6: Async Streams
        ///// IAsyncEnumerable适用于.NET	Core 3.0, Core 3.1, 5, 6, 7；.NET Standard	2.1
        ///// </summary>
        //public static async void _20_6AsyncStreams()
        //{
        //    string directoryPath = Directory.GetCurrentDirectory();
        //    string searchPattern = "*";
        //    // ...
        //    using Cryptographer cryptographer = new Cryptographer();

        //    IEnumerable<string> files = Directory.EnumerateFiles(
        //        directoryPath, searchPattern);

        //    // Create a cancellation token source to cancel 
        //    // if the operation takes more than a minute.
        //    using CancellationTokenSource cancellationTokenSource =
        //        new CancellationTokenSource(1000 * 60);

        //    await foreach ((string fileName, string encryptedFileName)
        //        in EncryptFilesAsync(files, cryptographer)
        //        .Zip(files.ToAsyncEnumerable())
        //        .WithCancellation(cancellationTokenSource.Token)
        //        )
        //    {
        //        Console.WriteLine($"{fileName}=>{encryptedFileName}");
        //    }
        //}
        //public static async IAsyncEnumerable<string> EncryptFilesAsync(
        //IEnumerable<string> files, Cryptographer cryptographer,
        //[EnumeratorCancellation] CancellationToken cancellationToken = default)
        //{
        //    foreach (string fileName in files)
        //    {
        //        yield return await EncryptFileAsync(fileName, cryptographer);
        //        cancellationToken.ThrowIfCancellationRequested();
        //    }
        //}
        //private static async ValueTask<string> EncryptFileAsync(
        //    string fileName, Cryptographer cryptographer)
        //{
        //    string encryptedFileName = $"{fileName}.encrypt";
        //    await using FileStream outputFileStream =
        //        new FileStream(encryptedFileName, FileMode.Create);

        //    string data = await File.ReadAllTextAsync(fileName);

        //    await cryptographer.EncryptAsync(data, outputFileStream);

        //    return encryptedFileName;
        //}

        /// <summary>
        /// Listing 20.10: An Asynchronous Client-Server Interaction as a Lambda Expression
        /// </summary>
        public static void _20_10AnAsynchronousClientServerInteractionAsALambdaExpression()
        {
            string url = "http://www.IntelliTect.com";

            Console.Write(url);

            Func<string, Task> writeWebRequestSizeAsync =
                async (string webRequestUrl) =>
                {
                    // Error handling omitted for 
                    // elucidation
                    WebRequest webRequest =
                       WebRequest.Create(url);

                    WebResponse response =
                        await webRequest.GetResponseAsync();

                    // Explicitly counting rather than invoking
                    // webRequest.ContentLength to demonstrate
                    //  multiple await operators
                    using (StreamReader reader =
                        new StreamReader(
                            response.GetResponseStream()))
                    {
                        string text =
                            (await reader.ReadToEndAsync());
                        Console.WriteLine(text);
                    }
                };

            Task task = writeWebRequestSizeAsync(url);

            while (!task.Wait(100))
            {
                Console.Write(".");
            }
        }

        /// <summary>
        /// Listing 20.13: Calling Task.ContinueWith()
        /// Task Schedulers and the Synchronization Context 任务计划程序和同步上下文
        /// </summary>
        public static void _20_13CallingTask_ContinueWith()
        {
            /* 有时，本章会提到任务计划程序及其在确定如何有效地将工作分配给线程方面的作用。以编程方式，任务计划程序是 System.Threading.Tasks.TaskScheduler 的实例。
             *   默认情况下，此类使用线程池来适当地调度任务，确定如何安全有效地执行任务 - 何时重用、释放或创建其他任务。
             * 可以创建自己的任务计划程序，通过从 TaskScheduler 类派生新类型来对如何计划任务做出不同的选择。通过使用静态 FromCurrentSynchronizationContext（） 方法，
             *   您可以获取一个 TaskScheduler，该程序将任务计划到当前线程（或者更准确地说，调度到与当前线程关联的同步上下文），而不是到其他工作线程。
             * 执行任务和执行继续任务的同步上下文非常重要，因为等待任务会查询同步上下文（假设有同步上下文），以便任务可以高效安全地执行。
             */
            DisplayStatus("Before");
            Task taskA =
                Task.Run(() =>
                     DisplayStatus("Starting..."))
                .ContinueWith(antecedent =>
                     DisplayStatus("Continuing A..."));
            Task taskB = taskA.ContinueWith(antecedent =>
          DisplayStatus("Continuing B..."));
            Task taskC = taskA.ContinueWith(antecedent =>
                DisplayStatus("Continuing C..."));
            Task.WaitAll(taskB, taskC);
            DisplayStatus("Finished!");
            /* Output 20.3
                1: Before
                3: Starting...
                4: Continuing A...
                3: Continuing C...
                4: Continuing B...
                1: Finished!
             * 此输出值得注意的是，线程 ID 有时会更改，有时会重复。在这种普通控制台应用程序中，同步上下文（可从 SynchronizationContext.Current 访问）为 null — 
             *   默认同步上下文会导致线程池改为处理线程分配。这解释了为什么线程 ID 在任务之间会发生变化：有时线程池确定使用新线程更有效，
             *   有时它决定最佳操作过程是重用现有线程。
             * 幸运的是，同步上下文会自动针对至关重要的应用程序类型进行设置。例如，如果创建任务的代码在 ASP.NET 创建的线程中运行，则该线程将具有与其关联的 
             *   AspNetSynchronizationContext 类型的同步上下文。相反，如果您的代码在 Windows UI 应用程序中创建的线程
             *   （即 Windows Presentation Foundation （WPF） 或 Windows Forms）中运行，则该线程将分别具有 
             *   DispatcherSynchronizationContext 或 WindowsFormsSynchronizationContext 的实例。
             *   （对于控制台应用程序和 Windows 服务，线程将具有默认同步上下文的实例。由于 TPL 会查询同步上下文，并且该同步上下文会根据执行环境而变化，
             *   因此 TPL 能够安排在高效且安全的上下文中执行的延续。
             * 若要修改代码以便改用同步上下文，必须 （1） 设置同步上下文，然后 （2） 使用 async/await 来确保查阅同步上下文。
             */
        }
        private static void DisplayStatus(string message)
        {
            string text =
                    $@"{Thread.CurrentThread.ManagedThreadId}: {message}";
            Console.WriteLine(text);
        }
    }
}
