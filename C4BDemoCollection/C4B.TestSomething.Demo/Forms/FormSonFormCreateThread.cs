﻿// <copyright file="FormSonFormCreateThread.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.Forms
{
    using System;
    using System.Diagnostics;
    using System.Windows.Forms;
    using C4B.TestSomething.Demo.BLL;
    using C4BCodesLibrary.C4BCodes.Utilities;

    /// <summary>
    /// 子窗体中创建线程，关闭子窗体后线程是否存在?.
    /// </summary>
    public partial class FormSonFormCreateThread : Form
    {
        private TestSonFormCreateThreadLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="FormSonFormCreateThread"/> class.
        /// </summary>
        public FormSonFormCreateThread()
        {
            this.InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.logic.StartCAN(true);
        }

        private void FormSonFormCreateThread_Load(object sender, EventArgs e)
        {
            this.logic = new TestSonFormCreateThreadLogic();
            ////int count = CommandQueue<int>.AllCommandConcurrentQueue.Count;
            int count = CommandQueue<int>.AllCommandQueue.Count;
            for (int i = 0; i < count; i++)
            {
                ////CommandQueue<int>.AllCommandConcurrentQueue.TryDequeue(out var data);
                var data = CommandQueue<int>.AllCommandQueue.Dequeue();
                Console.WriteLine($"清空接收队列中的数据: {data}");
                Debug.WriteLine($"清空接收队列中的数据: {data}");
            }
        }

        private void FormSonFormCreateThread_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.logic.StartCAN(false);
        }
    }
}