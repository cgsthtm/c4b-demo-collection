﻿// <copyright file="FrmTestFormSwitchSetTop.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.Forms
{
    using System;
    using System.Windows.Forms;
    using C4B.TestSomething.Demo.Utility;
    using C4BCodesLibrary.C4BCodes.Win32Api;

    /// <summary>
    /// 测试多路阀工位软件切换置顶.
    /// </summary>
    public partial class FrmTestFormSwitchSetTop : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrmTestFormSwitchSetTop"/> class.
        /// </summary>
        public FrmTestFormSwitchSetTop()
        {
            this.InitializeComponent();
        }

        private void BtnConnectRedis_Click(object sender, EventArgs e)
        {
            var connStr = this.txtRedisConStr.Text.Trim();
            if (!string.IsNullOrEmpty(connStr))
            {
                RedisHelper.SetRedisConnection(connStr);
                RedisHelper.RedisDataBase.StringSet("SetTop", "0");
                this.timer1.Enabled = true; // 开启时钟
            }
            else
            {
                MessageBox.Show("请填写数据库连接字符串");
            }
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            if (RedisHelper.Redis != null)
            {
                var setTopVal = RedisHelper.RedisDataBase.StringGet("SetTop").ToString();
                this.lblSetTopVal.Text = setTopVal;
                if (setTopVal == "1")
                {
                    if (!Win32ApiHelper.IsTopMost(this.Handle))
                    {
                        Win32ApiHelper.SetTopMost(this.Handle, true);
                    }
                }
                else if (setTopVal == "0")
                {
                    if (Win32ApiHelper.IsTopMost(this.Handle))
                    {
                        Win32ApiHelper.SetTopMost(this.Handle, false);
                    }
                }
            }
        }

        private void BtnSetTop1_Click(object sender, EventArgs e)
        {
            if (RedisHelper.Redis != null)
            {
                RedisHelper.RedisDataBase.StringSet("SetTop", "1");
            }
            else
            {
                MessageBox.Show("请先连接Redis数据库");
            }
        }

        private void BtnSetTop0_Click(object sender, EventArgs e)
        {
            if (RedisHelper.Redis != null)
            {
                RedisHelper.RedisDataBase.StringSet("SetTop", "0");
            }
            else
            {
                MessageBox.Show("请先连接Redis数据库");
            }
        }
    }
}