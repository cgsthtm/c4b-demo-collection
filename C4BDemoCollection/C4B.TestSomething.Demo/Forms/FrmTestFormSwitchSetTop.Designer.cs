﻿namespace C4B.TestSomething.Demo.Forms
{
    partial class FrmTestFormSwitchSetTop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRedisConStr = new System.Windows.Forms.TextBox();
            this.btnConnectRedis = new System.Windows.Forms.Button();
            this.btnSetTop1 = new System.Windows.Forms.Button();
            this.btnSetTop0 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblSetTopVal = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "redis数据库连接字符串：";
            // 
            // txtRedisConStr
            // 
            this.txtRedisConStr.Location = new System.Drawing.Point(162, 10);
            this.txtRedisConStr.Name = "txtRedisConStr";
            this.txtRedisConStr.Size = new System.Drawing.Size(459, 21);
            this.txtRedisConStr.TabIndex = 1;
            this.txtRedisConStr.Text = "127.0.0.1:6379,password=root@redis";
            // 
            // btnConnectRedis
            // 
            this.btnConnectRedis.Location = new System.Drawing.Point(627, 10);
            this.btnConnectRedis.Name = "btnConnectRedis";
            this.btnConnectRedis.Size = new System.Drawing.Size(75, 23);
            this.btnConnectRedis.TabIndex = 2;
            this.btnConnectRedis.Text = "连接Redis";
            this.btnConnectRedis.UseVisualStyleBackColor = true;
            this.btnConnectRedis.Click += new System.EventHandler(this.BtnConnectRedis_Click);
            // 
            // btnSetTop1
            // 
            this.btnSetTop1.Location = new System.Drawing.Point(546, 37);
            this.btnSetTop1.Name = "btnSetTop1";
            this.btnSetTop1.Size = new System.Drawing.Size(75, 23);
            this.btnSetTop1.TabIndex = 3;
            this.btnSetTop1.Text = "SetTop1";
            this.btnSetTop1.UseVisualStyleBackColor = true;
            this.btnSetTop1.Click += new System.EventHandler(this.BtnSetTop1_Click);
            // 
            // btnSetTop0
            // 
            this.btnSetTop0.Location = new System.Drawing.Point(627, 37);
            this.btnSetTop0.Name = "btnSetTop0";
            this.btnSetTop0.Size = new System.Drawing.Size(75, 23);
            this.btnSetTop0.TabIndex = 4;
            this.btnSetTop0.Text = "SetTop0";
            this.btnSetTop0.UseVisualStyleBackColor = true;
            this.btnSetTop0.Click += new System.EventHandler(this.BtnSetTop0_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(362, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "SetTop值：";
            // 
            // lblSetTopVal
            // 
            this.lblSetTopVal.AutoSize = true;
            this.lblSetTopVal.Location = new System.Drawing.Point(433, 42);
            this.lblSetTopVal.Name = "lblSetTopVal";
            this.lblSetTopVal.Size = new System.Drawing.Size(23, 12);
            this.lblSetTopVal.TabIndex = 6;
            this.lblSetTopVal.Text = "N/A";
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // FrmTestFormSwitchSetTop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblSetTopVal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSetTop0);
            this.Controls.Add(this.btnSetTop1);
            this.Controls.Add(this.btnConnectRedis);
            this.Controls.Add(this.txtRedisConStr);
            this.Controls.Add(this.label1);
            this.Name = "FrmTestFormSwitchSetTop";
            this.Text = "FrmTestFormSwitchSetTop";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtRedisConStr;
        private System.Windows.Forms.Button btnConnectRedis;
        private System.Windows.Forms.Button btnSetTop1;
        private System.Windows.Forms.Button btnSetTop0;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblSetTopVal;
        private System.Windows.Forms.Timer timer1;
    }
}