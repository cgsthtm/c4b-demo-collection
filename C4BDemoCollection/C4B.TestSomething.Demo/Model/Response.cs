﻿// <copyright file="Response.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.Model
{
    /// <summary>
    /// 响应报文实体类.
    /// </summary>
    [System.SerializableAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "response", Namespace = "", IsNullable = false)]
    public partial class Response
    {
        /// <summary>
        /// Gets or sets 命令字.
        /// </summary>
        [System.Xml.Serialization.XmlElement(elementName: "cmd")]
        public string Cmd { get; set; }

        /// <summary>
        /// Gets or sets 响应码.
        /// </summary>
        [System.Xml.Serialization.XmlElement(elementName: "code")]
        public byte Code { get; set; }

        /// <summary>
        /// Gets or sets 响应说明.
        /// </summary>
        [System.Xml.Serialization.XmlElement(elementName: "msg")]
        public string Msg { get; set; }

        /// <summary>
        /// Gets or sets 数据.
        /// </summary>
        [System.Xml.Serialization.XmlElement(elementName: "data")]
        public dynamic Data { get; set; }
    }
}