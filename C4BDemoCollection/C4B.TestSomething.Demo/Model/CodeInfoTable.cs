﻿// <copyright file="CodeInfoTable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.Model
{
    using SqlSugar;

    /// <summary>
    /// 内部代码CodeInfoTable.
    /// </summary>
    public class CodeInfoTable
    {
        /// <summary>
        /// Gets or sets 代码类型ID.
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = false)]
        public string CodeTypeID { get; set; }

        /// <summary>
        /// Gets or sets 代码ID.
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = false)]
        public string CodeID { get; set; }

        /// <summary>
        /// Gets or sets 代码名称.
        /// </summary>
        public string CodeName { get; set; }

        /// <summary>
        /// Gets or sets 代码说明.
        /// </summary>
        public string CodeInfo0 { get; set; }

        /// <summary>
        /// Gets or sets 关联信息.
        /// </summary>
        public string CodeInfo1 { get; set; }

        /// <summary>
        /// Gets or sets 关联信息.
        /// </summary>
        public string CodeInfo2 { get; set; }

        /// <summary>
        /// Gets or sets 关联信息.
        /// </summary>
        public string CodeInfo3 { get; set; }

        /// <summary>
        /// Gets or sets 是否启用.
        /// </summary>
        public int IsUse { get; set; }

        /// <summary>
        /// Gets or sets 更新时间.
        /// </summary>
        public System.DateTime Updatetime { get; set; }

        /// <summary>
        /// Gets or sets 代码英文名称.
        /// </summary>
        public string CodeEnName { get; set; }
    }
}
