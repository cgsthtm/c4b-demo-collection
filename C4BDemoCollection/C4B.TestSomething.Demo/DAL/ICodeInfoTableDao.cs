﻿
namespace C4B.TestSomething.Demo.DAL
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using C4B.TestSomething.Demo.Model;

    /// <summary>
    /// ICodeInfoTableDao.
    /// </summary>
    public interface ICodeInfoTableDao
    {
        /// <summary>
        /// 获取所有.
        /// </summary>
        /// <returns>res.</returns>
        List<CodeInfoTable> QueryAll();

        /// <summary>
        /// 批量更新.
        /// </summary>
        /// <param name="list">列表.</param>
        /// <param name="whereColumns">where列表.</param>
        /// <param name="updateColumns">update列表.</param>
        /// <returns>res.</returns>
        Task<int> BulkUpdateAsync(List<CodeInfoTable> list, string[] whereColumns, string[] updateColumns);
    }
}
