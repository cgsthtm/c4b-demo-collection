﻿// <copyright file="CodeInfoTableDao.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.DAL
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using C4B.TestSomething.Demo.Model;
    using C4B.TestSomething.Demo.Utility;

    /// <summary>
    /// CodeInfoTableDao.
    /// </summary>
    public class CodeInfoTableDao : ICodeInfoTableDao
    {
        /// <inheritdoc/>
        public List<CodeInfoTable> QueryAll()
        {
            return SqlSugarHelper.Db.Queryable<CodeInfoTable>().ToList();
        }

        /// <inheritdoc/>
        public Task<int> BulkUpdateAsync(List<CodeInfoTable> list, string[] whereColumns, string[] updateColumns)
        {
            return SqlSugarHelper.Db.Fastest<CodeInfoTable>().BulkUpdateAsync(list, whereColumns, updateColumns);
        }
    }
}
