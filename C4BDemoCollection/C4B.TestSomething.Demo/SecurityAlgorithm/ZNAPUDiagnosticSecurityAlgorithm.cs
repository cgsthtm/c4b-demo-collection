﻿// <copyright file="ZNAPUDiagnosticSecurityAlgorithm.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.SecurityAlgorithm
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// 郑州日产PU电气平台诊断安全算法.
    /// </summary>
    public class ZNAPUDiagnosticSecurityAlgorithm
    {
        /// <summary>
        /// 应用诊断安全算法掩码.
        /// </summary>
        private static readonly uint MASK = 0x51D96F04;

        /// <summary>
        /// 根据种子和掩码获取密匙.
        /// </summary>
        /// <param name="seed">种子.</param>
        /// <param name="mask">掩码.</param>
        /// <returns>密匙.</returns>
        public static uint SeedToKey(uint seed, uint mask)
        {
            uint key = 0;
            if (seed != 0)
            {
                for (int i = 0; i < 35; i++)
                {
                    if ((seed & 0x80000000) != 0)
                    {
                        seed = (seed << 1) ^ mask;
                    }
                    else
                    {
                        seed = seed << 1;
                    }
                }

                key = seed;
            }

            return key;
        }

        /// <summary>
        /// 根据种子和掩码获取密匙.
        /// </summary>
        /// <param name="seed">种子.</param>
        /// <param name="mask">掩码.</param>
        /// <returns>密匙.</returns>
        [DllImport("ZNA_P20_AngulatRadar_SeednKey.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GenerateKeyEx(IntPtr seed, IntPtr mask);
    }
}
