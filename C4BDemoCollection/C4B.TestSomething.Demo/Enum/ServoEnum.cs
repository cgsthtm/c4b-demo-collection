﻿// <copyright file="ServoEnum.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.Enum
{
    using System.ComponentModel;

    /// <summary>
    /// 伺服位置.
    /// </summary>
    public enum ServoPosition
    {
        /// <summary>
        /// 左前Z伺服
        /// </summary>
        [Description("左前Z伺服")]
        LFZ,

        /// <summary>
        /// 右前Z伺服
        /// </summary>
        [Description("右前Z伺服")]
        RFZ,

        /// <summary>
        /// 左后X伺服
        /// </summary>
        [Description("左后X伺服")]
        LBX,

        /// <summary>
        /// 左后Z伺服
        /// </summary>
        [Description("左后Z伺服")]
        LBZ,

        /// <summary>
        /// 右后X伺服
        /// </summary>
        [Description("右后X伺服")]
        RBX,

        /// <summary>
        /// 右后Z伺服
        /// </summary>
        [Description("右后Z伺服")]
        RBZ,

        /// <summary>
        /// 左前转伺服
        /// </summary>
        [Description("左前转伺服")]
        LFTurn,

        /// <summary>
        /// 右前转伺服
        /// </summary>
        [Description("右前转伺服")]
        RFTurn,

        /// <summary>
        /// 左后转伺服
        /// </summary>
        [Description("左后转伺服")]
        LBTurn,

        /// <summary>
        /// 右后转伺服
        /// </summary>
        [Description("右后转伺服")]
        RBTurn,
    }

    /// <summary>
    /// 伺服写动作.
    /// </summary>
    public enum ServoWriteAction2
    {
        /// <summary>
        /// 伺服使能
        /// </summary>
        [Description("伺服使能")]
        Enable,

        /// <summary>
        /// 伺服激活
        /// </summary>
        [Description("伺服激活")]
        Activate,

        /// <summary>
        /// 伺服故障复位
        /// </summary>
        [Description("伺服故障复位")]
        FaultReset,

        /// <summary>
        /// 伺服目标位置
        /// </summary>
        [Description("伺服目标位置")]
        TargetLocation,

        /// <summary>
        /// 伺服目标速度
        /// </summary>
        [Description("伺服目标速度")]
        TargetSpeed,
    }

    /// <summary>
    /// 伺服写动作.
    /// </summary>
    public enum ServoWriteAction
    {
        /// <summary>
        /// 左前Z伺服使能
        /// </summary>
        [Description("左前Z伺服使能")]
        LFZEnable = 20,

        /// <summary>
        /// 左前Z伺服激活
        /// </summary>
        [Description("左前Z伺服激活")]
        LFZActivate,

        /// <summary>
        /// 左前Z伺服故障复位
        /// </summary>
        [Description("左前Z伺服故障复位")]
        LFZFaultReset,

        /// <summary>
        /// 右前Z伺服使能
        /// </summary>
        [Description("右前Z伺服使能")]
        RFZEnable = 29,

        /// <summary>
        /// 右前Z伺服激活
        /// </summary>
        [Description("右前Z伺服激活")]
        RFZActivate,

        /// <summary>
        /// 右前Z伺服故障复位
        /// </summary>
        [Description("右前Z伺服故障复位")]
        RFZFaultReset,

        /// <summary>
        /// 左后X伺服使能
        /// </summary>
        [Description("左后X伺服使能")]
        LBXEnable = 38,

        /// <summary>
        /// 左后X伺服激活
        /// </summary>
        [Description("左后X伺服激活")]
        LBXActivate,

        /// <summary>
        /// 左后X伺服故障复位
        /// </summary>
        [Description("左后X伺服故障复位")]
        LBXFaultReset,

        /// <summary>
        /// 左后Z伺服使能
        /// </summary>
        [Description("左后Z伺服使能")]
        LBZEnable = 47,

        /// <summary>
        /// 左后Z伺服激活
        /// </summary>
        [Description("左后Z伺服激活")]
        LBZActivate,

        /// <summary>
        /// 左后Z伺服故障复位
        /// </summary>
        [Description("左后Z伺服故障复位")]
        LBZFaultReset,

        /// <summary>
        /// 右后X伺服使能
        /// </summary>
        [Description("右后X伺服使能")]
        RBXEnable = 56,

        /// <summary>
        /// 右后X伺服激活
        /// </summary>
        [Description("右后X伺服激活")]
        RBXActivate,

        /// <summary>
        /// 右后X伺服故障复位
        /// </summary>
        [Description("右后X伺服故障复位")]
        RBXFaultReset,

        /// <summary>
        /// 右后Z伺服使能
        /// </summary>
        [Description("右后Z伺服使能")]
        RBZEnable = 65,

        /// <summary>
        /// 右后Z伺服激活
        /// </summary>
        [Description("右后Z伺服激活")]
        RBZActivate,

        /// <summary>
        /// 右后Z伺服故障复位
        /// </summary>
        [Description("右后Z伺服故障复位")]
        RBZFaultReset,

        /// <summary>
        /// 左前转伺服使能
        /// </summary>
        [Description("左前转伺服使能")]
        LFTurnEnable = 74,

        /// <summary>
        /// 左前转伺服激活
        /// </summary>
        [Description("左前转伺服激活")]
        LFTurnActivate,

        /// <summary>
        /// 左前转伺服故障复位
        /// </summary>
        [Description("左前转伺服故障复位")]
        LFTurnFaultReset,

        /// <summary>
        /// 右前转伺服使能
        /// </summary>
        [Description("右前转伺服使能")]
        RFTurnEnable = 83,

        /// <summary>
        /// 右前转伺服激活
        /// </summary>
        [Description("右前转伺服激活")]
        RFTurnActivate,

        /// <summary>
        /// 右前转伺服故障复位
        /// </summary>
        [Description("右前转伺服故障复位")]
        RFTurnFaultReset,

        /// <summary>
        /// 左后转伺服使能
        /// </summary>
        [Description("左后转伺服使能")]
        LBTurnEnable = 92,

        /// <summary>
        /// 左后转伺服激活
        /// </summary>
        [Description("左后转伺服激活")]
        LBTurnActivate,

        /// <summary>
        /// 左后转伺服故障复位
        /// </summary>
        [Description("左后转伺服故障复位")]
        LBTurnFaultReset,

        /// <summary>
        /// 右后转伺服使能
        /// </summary>
        [Description("右后转伺服使能")]
        RBTurnEnable = 101,

        /// <summary>
        /// 右后转伺服激活
        /// </summary>
        [Description("右后转伺服激活")]
        RBTurnActivate,

        /// <summary>
        /// 右后转伺服故障复位
        /// </summary>
        [Description("右后转伺服故障复位")]
        RBTurnFaultReset,

        /// <summary>
        /// 左前Z伺服目标位置
        /// </summary>
        [Description("左前Z伺服目标位置")]
        LFZTargetLocation = 110,

        /// <summary>
        /// 左前Z伺服目标速度
        /// </summary>
        [Description("左前Z伺服目标速度")]
        LFZTargetSpeed = 112,

        /// <summary>
        /// 右前Z伺服目标位置
        /// </summary>
        [Description("右前Z伺服目标位置")]
        RFZTargetLocation = 118,

        /// <summary>
        /// 右前Z伺服目标速度
        /// </summary>
        [Description("右前Z伺服目标速度")]
        RFZTargetSpeed = 120,

        /// <summary>
        /// 左后X伺服目标位置
        /// </summary>
        [Description("左后X伺服目标位置")]
        LBXTargetLocation = 126,

        /// <summary>
        /// 左后X伺服目标速度
        /// </summary>
        [Description("左后X伺服目标速度")]
        LBXTargetSpeed = 128,

        /// <summary>
        /// 左后Z伺服目标位置
        /// </summary>
        [Description("左后Z伺服目标位置")]
        LBZTargetLocation = 134,

        /// <summary>
        /// 左后Z伺服目标速度
        /// </summary>
        [Description("左后Z伺服目标速度")]
        LBZTargetSpeed = 136,

        /// <summary>
        /// 右后X伺服目标位置
        /// </summary>
        [Description("右后X伺服目标位置")]
        RBXTargetLocation = 142,

        /// <summary>
        /// 右后X伺服目标速度
        /// </summary>
        [Description("右后X伺服目标速度")]
        RBXTargetSpeed = 144,

        /// <summary>
        /// 右后Z伺服目标位置
        /// </summary>
        [Description("右后Z伺服目标位置")]
        RBZTargetLocation = 150,

        /// <summary>
        /// 右后Z伺服目标速度
        /// </summary>
        [Description("右后Z伺服目标速度")]
        RBZTargetSpeed = 152,

        /// <summary>
        /// 左前转伺服目标位置
        /// </summary>
        [Description("左前转伺服目标位置")]
        LFTurnTargetLocation = 158,

        /// <summary>
        /// 左前转伺服目标速度
        /// </summary>
        [Description("左前转伺服目标速度")]
        LFTurnTargetSpeed = 160,

        /// <summary>
        /// 右前转伺服目标位置
        /// </summary>
        [Description("右前转伺服目标位置")]
        RFTurnTargetLocation = 166,

        /// <summary>
        /// 右前转伺服目标速度
        /// </summary>
        [Description("右前转伺服目标速度")]
        RFTurnTargetSpeed = 168,

        /// <summary>
        /// 左后转伺服目标位置
        /// </summary>
        [Description("左后转伺服目标位置")]
        LBTurnTargetLocation = 174,

        /// <summary>
        /// 左后转伺服目标速度
        /// </summary>
        [Description("左后转伺服目标速度")]
        LBTurnTargetSpeed = 176,

        /// <summary>
        /// 右后转伺服目标位置
        /// </summary>
        [Description("右后转伺服目标位置")]
        RBTurnTargetLocation = 182,

        /// <summary>
        /// 右后转伺服目标速度
        /// </summary>
        [Description("右后转伺服目标速度")]
        RBTurnTargetSpeed = 184,
    }

    /// <summary>
    /// 伺服读动作.
    /// </summary>
    public enum ServoReadAction2
    {
        /// <summary>
        /// 伺服模式错误
        /// </summary>
        [Description("伺服模式错误")]
        ModeError,

        /// <summary>
        /// 伺服通讯错误
        /// </summary>
        [Description("伺服通讯错误")]
        CommunicationError,

        /// <summary>
        /// 伺服已使能
        /// </summary>
        [Description("伺服已使能")]
        Enabled,

        /// <summary>
        /// 伺服故障
        /// </summary>
        [Description("伺服故障")]
        Fault,

        /// <summary>
        /// 伺服报警
        /// </summary>
        [Description("伺服报警")]
        Warning,

        /// <summary>
        /// 伺服当前报警代码
        /// </summary>
        [Description("伺服当前报警代码")]
        CurrentWarningCode,

        /// <summary>
        /// 伺服当前故障代码
        /// </summary>
        [Description("伺服当前故障代码")]
        CurrentFaultCode,
    }

    /// <summary>
    /// 左前Z伺服读状态.
    /// </summary>
    public enum LFZServoReadStatus
    {
        /// <summary>
        /// 伺服模式错误
        /// </summary>
        [Description("伺服模式错误")]
        ModeError = 40023,

        /// <summary>
        /// 伺服通讯错误
        /// </summary>
        [Description("伺服通讯错误")]
        CommunicationError,

        /// <summary>
        /// 伺服已使能
        /// </summary>
        [Description("伺服已使能")]
        Enabled,

        /// <summary>
        /// 伺服故障
        /// </summary>
        [Description("伺服故障")]
        Fault,

        /// <summary>
        /// 伺服报警
        /// </summary>
        [Description("伺服报警")]
        Warning,
    }

    /// <summary>
    /// 左前Z伺服读当前代码.
    /// </summary>
    public enum LFZServoReadCurrentCode
    {
        /// <summary>
        /// 伺服当前报警代码
        /// </summary>
        [Description("伺服当前报警代码")]
        CurrentWarningCode = 40191,

        /// <summary>
        /// 伺服当前故障代码
        /// </summary>
        [Description("伺服当前故障代码")]
        CurrentFaultCode,
    }

    /// <summary>
    /// 右前Z伺服读状态.
    /// </summary>
    public enum RFZServoReadStatus
    {
        /// <summary>
        /// 伺服模式错误
        /// </summary>
        [Description("伺服模式错误")]
        ModeError = 40032,

        /// <summary>
        /// 伺服通讯错误
        /// </summary>
        [Description("伺服通讯错误")]
        CommunicationError,

        /// <summary>
        /// 伺服已使能
        /// </summary>
        [Description("伺服已使能")]
        Enabled,

        /// <summary>
        /// 伺服故障
        /// </summary>
        [Description("伺服故障")]
        Fault,

        /// <summary>
        /// 伺服报警
        /// </summary>
        [Description("伺服报警")]
        Warning,
    }

    /// <summary>
    /// 右前Z伺服读当前代码.
    /// </summary>
    public enum RFZServoReadCurrentCode
    {
        /// <summary>
        /// 伺服当前报警代码
        /// </summary>
        [Description("伺服当前报警代码")]
        CurrentWarningCode = 40194,

        /// <summary>
        /// 伺服当前故障代码
        /// </summary>
        [Description("伺服当前故障代码")]
        CurrentFaultCode,
    }

    /// <summary>
    /// 左后X伺服读状态.
    /// </summary>
    public enum LBXServoReadStatus
    {
        /// <summary>
        /// 伺服模式错误
        /// </summary>
        [Description("伺服模式错误")]
        ModeError = 40041,

        /// <summary>
        /// 伺服通讯错误
        /// </summary>
        [Description("伺服通讯错误")]
        CommunicationError,

        /// <summary>
        /// 伺服已使能
        /// </summary>
        [Description("伺服已使能")]
        Enabled,

        /// <summary>
        /// 伺服故障
        /// </summary>
        [Description("伺服故障")]
        Fault,

        /// <summary>
        /// 伺服报警
        /// </summary>
        [Description("伺服报警")]
        Warning,
    }

    /// <summary>
    /// 左后X伺服读当前代码.
    /// </summary>
    public enum LBXServoReadCurrentCode
    {
        /// <summary>
        /// 伺服当前报警代码
        /// </summary>
        [Description("伺服当前报警代码")]
        CurrentWarningCode = 40197,

        /// <summary>
        /// 伺服当前故障代码
        /// </summary>
        [Description("伺服当前故障代码")]
        CurrentFaultCode,
    }

    /// <summary>
    /// 左后Z伺服读状态.
    /// </summary>
    public enum LBZServoReadStatus
    {
        /// <summary>
        /// 伺服模式错误
        /// </summary>
        [Description("伺服模式错误")]
        ModeError = 40050,

        /// <summary>
        /// 伺服通讯错误
        /// </summary>
        [Description("伺服通讯错误")]
        CommunicationError,

        /// <summary>
        /// 伺服已使能
        /// </summary>
        [Description("伺服已使能")]
        Enabled,

        /// <summary>
        /// 伺服故障
        /// </summary>
        [Description("伺服故障")]
        Fault,

        /// <summary>
        /// 伺服报警
        /// </summary>
        [Description("伺服报警")]
        Warning,
    }

    /// <summary>
    /// 左后Z伺服读当前代码.
    /// </summary>
    public enum LBZServoReadCurrentCode
    {
        /// <summary>
        /// 伺服当前报警代码
        /// </summary>
        [Description("伺服当前报警代码")]
        CurrentWarningCode = 40200,

        /// <summary>
        /// 伺服当前故障代码
        /// </summary>
        [Description("伺服当前故障代码")]
        CurrentFaultCode,
    }

    /// <summary>
    /// 右后X伺服读状态.
    /// </summary>
    public enum RBXServoReadStatus
    {
        /// <summary>
        /// 伺服模式错误
        /// </summary>
        [Description("伺服模式错误")]
        ModeError = 40059,

        /// <summary>
        /// 伺服通讯错误
        /// </summary>
        [Description("伺服通讯错误")]
        CommunicationError,

        /// <summary>
        /// 伺服已使能
        /// </summary>
        [Description("伺服已使能")]
        Enabled,

        /// <summary>
        /// 伺服故障
        /// </summary>
        [Description("伺服故障")]
        Fault,

        /// <summary>
        /// 伺服报警
        /// </summary>
        [Description("伺服报警")]
        Warning,
    }

    /// <summary>
    /// 右后X伺服读当前代码.
    /// </summary>
    public enum RBXServoReadCurrentCode
    {
        /// <summary>
        /// 伺服当前报警代码
        /// </summary>
        [Description("伺服当前报警代码")]
        CurrentWarningCode = 40203,

        /// <summary>
        /// 伺服当前故障代码
        /// </summary>
        [Description("伺服当前故障代码")]
        CurrentFaultCode,
    }

    /// <summary>
    /// 右后Z伺服读状态.
    /// </summary>
    public enum RBZServoReadStatus
    {
        /// <summary>
        /// 伺服模式错误
        /// </summary>
        [Description("伺服模式错误")]
        ModeError = 40068,

        /// <summary>
        /// 伺服通讯错误
        /// </summary>
        [Description("伺服通讯错误")]
        CommunicationError,

        /// <summary>
        /// 伺服已使能
        /// </summary>
        [Description("伺服已使能")]
        Enabled,

        /// <summary>
        /// 伺服故障
        /// </summary>
        [Description("伺服故障")]
        Fault,

        /// <summary>
        /// 伺服报警
        /// </summary>
        [Description("伺服报警")]
        Warning,
    }

    /// <summary>
    /// 右后Z伺服读当前代码.
    /// </summary>
    public enum RBZServoReadCurrentCode
    {
        /// <summary>
        /// 伺服当前报警代码
        /// </summary>
        [Description("伺服当前报警代码")]
        CurrentWarningCode = 40206,

        /// <summary>
        /// 伺服当前故障代码
        /// </summary>
        [Description("伺服当前故障代码")]
        CurrentFaultCode,
    }

    /// <summary>
    /// 左前转伺服读状态.
    /// </summary>
    public enum LFTurnServoReadStatus
    {
        /// <summary>
        /// 伺服模式错误
        /// </summary>
        [Description("伺服模式错误")]
        ModeError = 40077,

        /// <summary>
        /// 伺服通讯错误
        /// </summary>
        [Description("伺服通讯错误")]
        CommunicationError,

        /// <summary>
        /// 伺服已使能
        /// </summary>
        [Description("伺服已使能")]
        Enabled,

        /// <summary>
        /// 伺服故障
        /// </summary>
        [Description("伺服故障")]
        Fault,

        /// <summary>
        /// 伺服报警
        /// </summary>
        [Description("伺服报警")]
        Warning,
    }

    /// <summary>
    /// 左前转伺服读当前代码.
    /// </summary>
    public enum LFTurnServoReadCurrentCode
    {
        /// <summary>
        /// 伺服当前报警代码
        /// </summary>
        [Description("伺服当前报警代码")]
        CurrentWarningCode = 40209,

        /// <summary>
        /// 伺服当前故障代码
        /// </summary>
        [Description("伺服当前故障代码")]
        CurrentFaultCode,
    }

    /// <summary>
    /// 右前转伺服读状态.
    /// </summary>
    public enum RFTurnServoReadStatus
    {
        /// <summary>
        /// 伺服模式错误
        /// </summary>
        [Description("伺服模式错误")]
        ModeError = 40086,

        /// <summary>
        /// 伺服通讯错误
        /// </summary>
        [Description("伺服通讯错误")]
        CommunicationError,

        /// <summary>
        /// 伺服已使能
        /// </summary>
        [Description("伺服已使能")]
        Enabled,

        /// <summary>
        /// 伺服故障
        /// </summary>
        [Description("伺服故障")]
        Fault,

        /// <summary>
        /// 伺服报警
        /// </summary>
        [Description("伺服报警")]
        Warning,
    }

    /// <summary>
    /// 右前转伺服读当前代码.
    /// </summary>
    public enum RFTurnServoReadCurrentCode
    {
        /// <summary>
        /// 伺服当前报警代码
        /// </summary>
        [Description("伺服当前报警代码")]
        CurrentWarningCode = 40212,

        /// <summary>
        /// 伺服当前故障代码
        /// </summary>
        [Description("伺服当前故障代码")]
        CurrentFaultCode,
    }

    /// <summary>
    /// 左后转伺服读状态.
    /// </summary>
    public enum LBTurnServoReadStatus
    {
        /// <summary>
        /// 伺服模式错误
        /// </summary>
        [Description("伺服模式错误")]
        ModeError = 40095,

        /// <summary>
        /// 伺服通讯错误
        /// </summary>
        [Description("伺服通讯错误")]
        CommunicationError,

        /// <summary>
        /// 伺服已使能
        /// </summary>
        [Description("伺服已使能")]
        Enabled,

        /// <summary>
        /// 伺服故障
        /// </summary>
        [Description("伺服故障")]
        Fault,

        /// <summary>
        /// 伺服报警
        /// </summary>
        [Description("伺服报警")]
        Warning,
    }

    /// <summary>
    /// 左后转伺服读当前代码.
    /// </summary>
    public enum LBTurnServoReadCurrentCode
    {
        /// <summary>
        /// 伺服当前报警代码
        /// </summary>
        [Description("伺服当前报警代码")]
        CurrentWarningCode = 40215,

        /// <summary>
        /// 伺服当前故障代码
        /// </summary>
        [Description("伺服当前故障代码")]
        CurrentFaultCode,
    }

    /// <summary>
    /// 右后转伺服读状态.
    /// </summary>
    public enum RBTurnServoReadStatus
    {
        /// <summary>
        /// 伺服模式错误
        /// </summary>
        [Description("伺服模式错误")]
        ModeError = 40104,

        /// <summary>
        /// 伺服通讯错误
        /// </summary>
        [Description("伺服通讯错误")]
        CommunicationError,

        /// <summary>
        /// 伺服已使能
        /// </summary>
        [Description("伺服已使能")]
        Enabled,

        /// <summary>
        /// 伺服故障
        /// </summary>
        [Description("伺服故障")]
        Fault,

        /// <summary>
        /// 伺服报警
        /// </summary>
        [Description("伺服报警")]
        Warning,
    }

    /// <summary>
    /// 右后转伺服读当前代码.
    /// </summary>
    public enum RBTurnServoReadCurrentCode
    {
        /// <summary>
        /// 伺服当前报警代码
        /// </summary>
        [Description("伺服当前报警代码")]
        CurrentWarningCode = 40218,

        /// <summary>
        /// 伺服当前故障代码
        /// </summary>
        [Description("伺服当前故障代码")]
        CurrentFaultCode,
    }

    /// <summary>
    /// 电机位置.
    /// </summary>
    public enum ElectricalMachineryPosition
    {
        /// <summary>
        /// 左前Z电机
        /// </summary>
        [Description("左前Z电机")]
        LFZ,

        /// <summary>
        /// 右前Z电机
        /// </summary>
        [Description("右前Z电机")]
        RFZ,

        /// <summary>
        /// 左后X电机
        /// </summary>
        [Description("左后X电机")]
        LBX,

        /// <summary>
        /// 左后Z电机
        /// </summary>
        [Description("左后Z电机")]
        LBZ,

        /// <summary>
        /// 右后X电机
        /// </summary>
        [Description("右后X电机")]
        RBX,

        /// <summary>
        /// 右后Z电机
        /// </summary>
        [Description("右后Z电机")]
        RBZ,

        /// <summary>
        /// 左前转电机
        /// </summary>
        [Description("左前转电机")]
        LFTurn,

        /// <summary>
        /// 右前转电机
        /// </summary>
        [Description("右前转电机")]
        RFTurn,

        /// <summary>
        /// 左后转电机
        /// </summary>
        [Description("左后转电机")]
        LBTurn,

        /// <summary>
        /// 右后转电机
        /// </summary>
        [Description("右后转电机")]
        RBTurn,
    }

    /// <summary>
    /// 电机读动作.
    /// </summary>
    public enum ElectricalMachineryReadAction
    {
        /// <summary>
        /// 左前Z电机位置到达
        /// </summary>
        [Description("左前Z电机位置到达")]
        LFZInPosition = 40028,

        /// <summary>
        /// 右前Z电机位置到达
        /// </summary>
        [Description("右前Z电机位置到达")]
        RFZInPosition = 40037,

        /// <summary>
        /// 左后X电机位置到达
        /// </summary>
        [Description("左后X电机位置到达")]
        LBXInPosition = 40046,

        /// <summary>
        /// 左后Z电机位置到达
        /// </summary>
        [Description("左后Z电机位置到达")]
        LBZInPosition = 40055,

        /// <summary>
        /// 右后X电机位置到达
        /// </summary>
        [Description("右后X电机位置到达")]
        RBXInPosition = 40064,

        /// <summary>
        /// 右后Z电机位置到达
        /// </summary>
        [Description("右后Z电机位置到达")]
        RBZInPosition = 40073,

        /// <summary>
        /// 左前转电机位置到达
        /// </summary>
        [Description("左前转电机位置到达")]
        LFTurnInPosition = 40082,

        /// <summary>
        /// 右前转电机位置到达
        /// </summary>
        [Description("右前转电机位置到达")]
        RFTurnInPosition = 40091,

        /// <summary>
        /// 左后转电机位置到达
        /// </summary>
        [Description("左后转电机位置到达")]
        LBTurnInPosition = 40100,

        /// <summary>
        /// 右后转电机位置到达
        /// </summary>
        [Description("右后转电机位置到达")]
        RBTurnInPosition = 40109,

        /// <summary>
        /// 左前Z电机实际速度 占2个地址
        /// </summary>
        [Description("左前Z电机实际速度")]
        LFZActualSpeed = 40114,

        /// <summary>
        /// 左前Z电机当前位置 占2个地址
        /// </summary>
        [Description("左前Z电机当前位置")]
        LFZCurrentPosition = 40116,

        /// <summary>
        /// 右前Z电机实际速度 占2个地址
        /// </summary>
        [Description("右前Z电机实际速度")]
        RFZActualSpeed = 40122,

        /// <summary>
        /// 右前Z电机当前位置 占2个地址
        /// </summary>
        [Description("右前Z电机当前位置")]
        RFZCurrentPosition = 40124,

        /// <summary>
        /// 左后X电机实际速度 占2个地址
        /// </summary>
        [Description("左后X电机实际速度")]
        LBXActualSpeed = 40130,

        /// <summary>
        /// 左后X电机当前位置 占2个地址
        /// </summary>
        [Description("左后X电机当前位置")]
        LBXCurrentPosition = 40132,

        /// <summary>
        /// 左后Z电机实际速度 占2个地址
        /// </summary>
        [Description("左后Z电机实际速度")]
        LBZActualSpeed = 40138,

        /// <summary>
        /// 左后Z电机当前位置 占2个地址
        /// </summary>
        [Description("左后Z电机当前位置")]
        LBZCurrentPosition = 40140,

        /// <summary>
        /// 右后X电机实际速度 占2个地址
        /// </summary>
        [Description("右后X电机实际速度")]
        RBXActualSpeed = 40146,

        /// <summary>
        /// 右后X电机当前位置 占2个地址
        /// </summary>
        [Description("右后X电机当前位置")]
        RBXCurrentPosition = 40148,

        /// <summary>
        /// 右后Z电机实际速度 占2个地址
        /// </summary>
        [Description("右后Z电机实际速度")]
        RBZActualSpeed = 40154,

        /// <summary>
        /// 右后Z电机当前位置 占2个地址
        /// </summary>
        [Description("右后Z电机当前位置")]
        RBZCurrentPosition = 40156,

        /// <summary>
        /// 左前转电机实际速度 占2个地址
        /// </summary>
        [Description("左前转电机实际速度")]
        LFTurnActualSpeed = 40162,

        /// <summary>
        /// 左前转电机当前位置 占2个地址
        /// </summary>
        [Description("左前转电机当前位置")]
        LFTurnCurrentPosition = 40164,

        /// <summary>
        /// 右前转电机实际速度 占2个地址
        /// </summary>
        [Description("右前转电机实际速度")]
        RFTurnActualSpeed = 40170,

        /// <summary>
        /// 右前转电机当前位置 占2个地址
        /// </summary>
        [Description("右前转电机当前位置")]
        RFTurnCurrentPosition = 40172,

        /// <summary>
        /// 左后转电机实际速度 占2个地址
        /// </summary>
        [Description("左后转电机实际速度")]
        LBTurnActualSpeed = 40178,

        /// <summary>
        /// 左后转电机当后位置 占2个地址
        /// </summary>
        [Description("左后转电机当后位置")]
        LBTurnCurrentPosition = 40180,

        /// <summary>
        /// 右后转电机实际速度 占2个地址
        /// </summary>
        [Description("右后转电机实际速度")]
        RBTurnActualSpeed = 40186,

        /// <summary>
        /// 右后转电机当后位置 占2个地址
        /// </summary>
        [Description("右后转电机当后位置")]
        RBTurnCurrentPosition = 40188,

        /// <summary>
        /// 左前Z电机通讯错误
        /// </summary>
        [Description("左前Z电机通讯错误")]
        LFZCommunicationError = 40190,

        /// <summary>
        /// 右前Z电机通讯错误
        /// </summary>
        [Description("右前Z电机通讯错误")]
        RFZCommunicationError = 40193,

        /// <summary>
        /// 左后X电机通讯错误
        /// </summary>
        [Description("左后X电机通讯错误")]
        LBXCommunicationError = 40196,

        /// <summary>
        /// 左后Z电机通讯错误
        /// </summary>
        [Description("左后Z电机通讯错误")]
        LBZCommunicationError = 40199,

        /// <summary>
        /// 右后X电机通讯错误
        /// </summary>
        [Description("右后X电机通讯错误")]
        RBXCommunicationError = 40202,

        /// <summary>
        /// 右后Z电机通讯错误
        /// </summary>
        [Description("右后Z电机通讯错误")]
        RBZCommunicationError = 40205,

        /// <summary>
        /// 左前转电机通讯错误
        /// </summary>
        [Description("左前转电机通讯错误")]
        LFTurnCommunicationError = 40208,

        /// <summary>
        /// 右前转电机通讯错误
        /// </summary>
        [Description("右前转电机通讯错误")]
        RFTurnCommunicationError = 40211,

        /// <summary>
        /// 左后转电机通讯错误
        /// </summary>
        [Description("左后转电机通讯错误")]
        LBTurnCommunicationError = 40214,

        /// <summary>
        /// 右后转电机通讯错误
        /// </summary>
        [Description("右后转电机通讯错误")]
        RBTurnCommunicationError = 40217,
    }

    /// <summary>
    /// 信号测试读.
    /// </summary>
    public enum SignalTest
    {
        /// <summary>
        /// 左前电机下限位
        /// </summary>
        [Description("左前电机下限位")]
        LFElectricalMachineryDownLimit = 0,

        /// <summary>
        /// 左前电机上限位
        /// </summary>
        [Description("左前电机上限位")]
        LFElectricalMachineryUpLimit,

        /// <summary>
        /// 右前电机下限位
        /// </summary>
        [Description("右前电机下限位")]
        RFElectricalMachineryDownLimit,

        /// <summary>
        /// 右前电机上限位
        /// </summary>
        [Description("右前电机上限位")]
        RFElectricalMachineryUpLimit,

        /// <summary>
        /// 左后电机前限位
        /// </summary>
        [Description("左后电机前限位")]
        LBElectricalMachineryFrontLimit,

        /// <summary>
        /// 左后电机后限位
        /// </summary>
        [Description("左后电机后限位")]
        LBElectricalMachineryBackLimit,

        /// <summary>
        /// 左后电机下限位
        /// </summary>
        [Description("左后电机下限位")]
        LBElectricalMachineryDownLimit,

        /// <summary>
        /// 左后电机上限位
        /// </summary>
        [Description("左后电机上限位")]
        LBElectricalMachineryUpLimit,

        /// <summary>
        /// 右后电机前限位
        /// </summary>
        [Description("右后电机前限位")]
        RBElectricalMachineryFrontLimit,

        /// <summary>
        /// 右后电机后限位
        /// </summary>
        [Description("右后电机后限位")]
        RBElectricalMachineryBackLimit,

        /// <summary>
        /// 右后电机下限位
        /// </summary>
        [Description("右后电机下限位")]
        RBElectricalMachineryDownLimit,

        /// <summary>
        /// 右后电机上限位
        /// </summary>
        [Description("右后电机上限位")]
        RBElectricalMachineryUpLimit,

        /// <summary>
        /// 前摆左近磁性开关
        /// </summary>
        [Description("前摆左近磁性开关")]
        FLNearMagneticSwitch,

        /// <summary>
        /// 前摆左远磁性开关
        /// </summary>
        [Description("前摆左远磁性开关")]
        FLFarMagneticSwitch,

        /// <summary>
        /// 前摆右近磁性开关
        /// </summary>
        [Description("前摆右近磁性开关")]
        FRNearMagneticSwitch,

        /// <summary>
        /// 前摆右远磁性开关
        /// </summary>
        [Description("前摆右远磁性开关")]
        FRFarMagneticSwitch,

        /// <summary>
        /// 后摆左近磁性开关
        /// </summary>
        [Description("后摆左近磁性开关")]
        BLNearMagneticSwitch,

        /// <summary>
        /// 后摆左远磁性开关
        /// </summary>
        [Description("后摆左远磁性开关")]
        BLFarMagneticSwitch,

        /// <summary>
        /// 后摆右近磁性开关
        /// </summary>
        [Description("后摆右近磁性开关")]
        BRNearMagneticSwitch,

        /// <summary>
        /// 后摆右远磁性开关
        /// </summary>
        [Description("后摆右远磁性开关")]
        BRFarMagneticSwitch,

        /// <summary>
        /// 到位光电
        /// </summary>
        [Description("到位光电")]
        PhotoElectricityInPosition,

        /// <summary>
        /// 光幕1
        /// </summary>
        [Description("光幕1")]
        LightCurtain1,

        /// <summary>
        /// 光幕2
        /// </summary>
        [Description("光幕2")]
        LightCurtain2,

        /// <summary>
        /// 急停信号
        /// </summary>
        [Description("急停信号")]
        UrgentStopSignal,

        /// <summary>
        /// 手动模式
        /// </summary>
        [Description("手动模式")]
        ManualMode,

        /// <summary>
        /// 自动模式
        /// </summary>
        [Description("自动模式")]
        AutomaticMode,

        /// <summary>
        /// 急停复位信号
        /// </summary>
        [Description("急停复位信号")]
        UrgentStopResetSignal,

        /// <summary>
        /// 开始信号
        /// </summary>
        [Description("开始信号")]
        StartSignal,

        /// <summary>
        /// 结束信号
        /// </summary>
        [Description("结束信号")]
        StopSignal,
    }

    /// <summary>
    /// 摆正器.
    /// </summary>
    public enum PositionDevice
    {
        /// <summary>
        /// 前摆正器展
        /// </summary>
        [Description("前摆正器展")]
        FrontSpread = 0,

        /// <summary>
        /// 前摆正器收
        /// </summary>
        [Description("前摆正器收")]
        FrontGather,

        /// <summary>
        /// 后摆正器展
        /// </summary>
        [Description("后摆正器展")]
        BackSpread = 0,

        /// <summary>
        /// 后摆正器收
        /// </summary>
        [Description("后摆正器收")]
        BackGather,
    }
}