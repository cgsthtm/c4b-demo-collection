﻿// <copyright file="AutoFacHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.Utility
{
    using Autofac;
    using C4B.TestSomething.Demo.BLL;
    using C4B.TestSomething.Demo.DAL;

    /// <summary>
    /// AutoFacHelper.
    /// </summary>
    public sealed class AutoFacHelper
    {
        /// <summary>
        /// Instance.
        /// </summary>
        public static readonly AutoFacHelper Instance = new AutoFacHelper();

        // 私有构造.
        private AutoFacHelper()
        {
        }

        /// <summary>
        /// Gets Container.
        /// </summary>
        public IContainer Container { get; private set; }

        /// <summary>
        /// 获取实例.
        /// </summary>
        /// <returns>实例.</returns>
        public static AutoFacHelper GetInstance()
        {
            return Instance;
        }

        /// <summary>
        /// 注册服务.
        /// </summary>
        public void Register()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<CodeInfoTableDao>().As<ICodeInfoTableDao>();
            builder.RegisterType<CodeInfoTableService>().As<ICodeInfoTableService>();
            this.Container = builder.Build();
        }
    }
}
