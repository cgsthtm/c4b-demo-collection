﻿

namespace C4B.TestSomething.Demo.Utility
{
    using System;
    using System.Configuration;
    using SqlSugar;

    /// <summary>
    /// SqlSugarHelper.
    /// </summary>
    public class SqlSugarHelper // 不能是泛型类
    {
        // 多库情况下使用说明：
        // 如果是固定多库可以传 new SqlSugarScope(List<ConnectionConfig>,db=>{}) 文档：多租户
        // 如果是不固定多库 可以看文档Saas分库

        /// <summary>
        /// 单例模式Db.
        /// </summary>
        public static readonly SqlSugarScope Db = new SqlSugarScope(
            new ConnectionConfig()
            {
                ////ConnectionString = "server=.;uid=sa;pwd=Admin1234;database=NEVTestDB2.0", // 连接符字串
                ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString.ToString(),
                DbType = DbType.SqlServer,    // 数据库类型
                IsAutoCloseConnection = true, // 不设成true要手动close
            },
            db =>
            {
                // (A)全局生效配置点
                // 调试SQL事件，可以删掉
                db.Aop.OnLogExecuting = (sql, pars) =>
                {
                    Console.WriteLine(sql); // 输出sql,查看执行sql
                    //// UtilMethods.GetSqlString(DbType.SqlServer,sql,pars) // 5.0.8.2 获取无参数化 SQL
                };
            });
    }
}