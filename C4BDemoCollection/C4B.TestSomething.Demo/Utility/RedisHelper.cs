﻿// <copyright file="RedisHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.Utility
{
    using StackExchange.Redis;

    /// <summary>
    /// RedisHelper.
    /// </summary>
    public class RedisHelper
    {
        /// <summary>
        /// Gets redis.
        /// </summary>
        public static ConnectionMultiplexer Redis { get; private set; }

        /// <summary>
        /// Gets RedisDataBase.
        /// </summary>
        public static IDatabase RedisDataBase
        {
            get
            {
                IDatabase databse = null;
                if (Redis != null)
                {
                    databse = Redis.GetDatabase();
                }

                return databse;
            }
        }

        /// <summary>
        /// SetRedisConnection.
        /// </summary>
        /// <param name="connectionStr">conStr.</param>
        public static void SetRedisConnection(string connectionStr)
        {
            Redis = ConnectionMultiplexer.Connect(connectionStr);
        }
    }
}