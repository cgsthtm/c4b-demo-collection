﻿// <copyright file="RecvDataThread.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.Utility
{
    using System;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Threading.Tasks;
    using C4BCodesLibrary.C4BCodes.Utilities;

    /// <summary>
    /// RecvDataThread.
    /// </summary>
    internal class RecvDataThread
    {
        private static object locker = new object();
        private bool mbStart;
        private Task recv_thread_;
        private CancellationTokenSource cancellationTokenSource;

        /// <summary>
        /// Initializes a new instance of the <see cref="RecvDataThread"/> class.
        /// </summary>
        public RecvDataThread()
        {
            this.cancellationTokenSource = new CancellationTokenSource();
        }

        /// <summary>
        /// SetStart.
        /// </summary>
        /// <param name="start">start.</param>
        public void SetStart(bool start)
        {
            this.mbStart = start;
            if (start)
            {
                var token = this.cancellationTokenSource.Token;
                this.recv_thread_ = new Task(() => this.RecvDataFunc(token), token);
                this.recv_thread_.Start();
            }
            else
            {
                this.cancellationTokenSource?.Cancel();
                try
                {
                    this.recv_thread_.Wait();

                    // 如运行到这里，说明在取消请求生效前，操作正常完成。
                    Console.WriteLine($"在取消请求生效前，操作正常完成");
                    Debug.WriteLine($"在取消请求生效前，操作正常完成");
                }
                catch (OperationCanceledException calEx)
                {
                    // 如运行到这里，说明操作在完成前被取消。
                    Console.WriteLine($"运行到这里，说明操作在完成前被取消");
                    Debug.WriteLine($"运行到这里，说明操作在完成前被取消");
                    Console.WriteLine($"Thread:{Task.CurrentId} 被销毁");
                    Debug.WriteLine($"Thread:{Task.CurrentId} 被销毁");
                }
                catch (Exception ex)
                {
                    // 如运行到这里，说明在取消请求生效前，操作出错并结束。
                    Console.WriteLine($"在取消请求生效前，操作出错并结束");
                    Debug.WriteLine($"在取消请求生效前，操作出错并结束");
                    ////throw;
                }
                finally
                {
                    this.recv_thread_ = null;
                    Console.WriteLine($"Task置空");
                    Debug.WriteLine($"Task置空");
                    ////int count = CommandQueue<int>.AllCommandConcurrentQueue.Count;
                    int count = CommandQueue<int>.AllCommandQueue.Count;
                    for (int i = 0; i < count; i++)
                    {
                        ////CommandQueue<int>.AllCommandConcurrentQueue.TryDequeue(out var data);
                        var data = CommandQueue<int>.AllCommandQueue.Dequeue();
                        Console.WriteLine($"清空接收队列中的数据: {data}");
                        Debug.WriteLine($"清空接收队列中的数据: {data}");
                    }
                }
            }
        }

        /// <summary>
        /// RecvDataFunc.
        /// </summary>
        /// <param name="cancellationToken">tocken.</param>
        protected void RecvDataFunc(CancellationToken cancellationToken)
        {
            ////int count = 0;
            while (!cancellationToken.IsCancellationRequested)
            {
                cancellationToken.ThrowIfCancellationRequested();
                lock (locker)
                {
                    Console.WriteLine($"{DateTime.Now.ToString("ss.fff")} Thread:{Task.CurrentId}");
                    Debug.WriteLine($"{DateTime.Now.ToString("ss.fff")} Thread:{Task.CurrentId}");
                    int size = Marshal.SizeOf(typeof(int));
                    IntPtr ptr = Marshal.AllocHGlobal((int)5 * size);
                    for (int i = 0; i < 5; i++)
                    {
                        Marshal.PtrToStructure((IntPtr)((long)ptr + (i * size)), typeof(int));
                    }

                    ////CommandQueue<int>.AllCommandConcurrentQueue.Enqueue(Task.CurrentId ?? 0);
                    CommandQueue<int>.AllCommandQueue.Enqueue(Task.CurrentId ?? 0);
                    ////count++;
                    ////Debug.WriteLine($"入队次: {count}");
                    Marshal.FreeHGlobal(ptr);
                }

                Thread.Sleep(1000);
            }
        }
    }
}