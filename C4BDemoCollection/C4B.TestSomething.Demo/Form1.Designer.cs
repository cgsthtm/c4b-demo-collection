﻿namespace C4B.TestSomething.Demo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioBtnChineseLang = new System.Windows.Forms.RadioButton();
            this.radioBtnEnglishLang = new System.Windows.Forms.RadioButton();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.lblConnectionString = new System.Windows.Forms.Label();
            this.button16 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnTestTaskTimer = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.btnTestWindowTop = new System.Windows.Forms.Button();
            this.timerTestWindowTop = new System.Windows.Forms.Timer(this.components);
            this.button18 = new System.Windows.Forms.Button();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.button19 = new System.Windows.Forms.Button();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 4);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(158, 22);
            this.button1.TabIndex = 0;
            this.button1.Text = "解析.HEX文件";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(177, 4);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(320, 22);
            this.button2.TabIndex = 1;
            this.button2.Text = "调用博世安全访问算法SecurityAccess_DFLZ_customer.dll";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 30);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(158, 22);
            this.button3.TabIndex = 2;
            this.button3.Text = "解析SREC文件";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(177, 30);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(196, 22);
            this.button4.TabIndex = 3;
            this.button4.Text = "维特WT53R-485激光测距传感器";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(379, 30);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(209, 22);
            this.button5.TabIndex = 4;
            this.button5.Text = "大端序、小端序 0x07 0x0B -> 0x070B";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(13, 58);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(158, 22);
            this.button6.TabIndex = 5;
            this.button6.Text = "NModbus写多个寄存器";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(177, 58);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(158, 22);
            this.button7.TabIndex = 6;
            this.button7.Text = "NModbus读多个寄存器";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(341, 58);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(158, 22);
            this.button8.TabIndex = 7;
            this.button8.Text = "NModbus读多个输入";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(12, 84);
            this.button9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(158, 22);
            this.button9.TabIndex = 8;
            this.button9.Text = "UDS计算连续帧";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(505, 58);
            this.button10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(201, 22);
            this.button10.TabIndex = 9;
            this.button10.Text = "NModbus读测试LFZ电机位置";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.Button10_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(502, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "label1";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(177, 84);
            this.button11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(158, 22);
            this.button11.TabIndex = 11;
            this.button11.Text = "high3392 low3 to int";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.Button11_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(13, 110);
            this.button12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(158, 22);
            this.button12.TabIndex = 12;
            this.button12.Text = "XML字符串转实体类";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.Button12_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioBtnChineseLang);
            this.groupBox1.Controls.Add(this.radioBtnEnglishLang);
            this.groupBox1.Location = new System.Drawing.Point(12, 138);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(171, 42);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // radioBtnChineseLang
            // 
            this.radioBtnChineseLang.AutoSize = true;
            this.radioBtnChineseLang.Location = new System.Drawing.Point(86, 18);
            this.radioBtnChineseLang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioBtnChineseLang.Name = "radioBtnChineseLang";
            this.radioBtnChineseLang.Size = new System.Drawing.Size(71, 16);
            this.radioBtnChineseLang.TabIndex = 16;
            this.radioBtnChineseLang.TabStop = true;
            this.radioBtnChineseLang.Text = "标题中文";
            this.radioBtnChineseLang.UseVisualStyleBackColor = true;
            this.radioBtnChineseLang.CheckedChanged += new System.EventHandler(this.RadioBtnLanguage_CheckedChanged);
            // 
            // radioBtnEnglishLang
            // 
            this.radioBtnEnglishLang.AutoSize = true;
            this.radioBtnEnglishLang.Location = new System.Drawing.Point(7, 18);
            this.radioBtnEnglishLang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioBtnEnglishLang.Name = "radioBtnEnglishLang";
            this.radioBtnEnglishLang.Size = new System.Drawing.Size(71, 16);
            this.radioBtnEnglishLang.TabIndex = 15;
            this.radioBtnEnglishLang.TabStop = true;
            this.radioBtnEnglishLang.Text = "标题英文";
            this.radioBtnEnglishLang.UseVisualStyleBackColor = true;
            this.radioBtnEnglishLang.CheckedChanged += new System.EventHandler(this.RadioBtnLanguage_CheckedChanged);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(177, 110);
            this.button13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(158, 22);
            this.button13.TabIndex = 16;
            this.button13.Text = "EPPLUS Demo";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.Button13_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(341, 110);
            this.button14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(158, 22);
            this.button14.TabIndex = 17;
            this.button14.Text = "ZNA-PU平台诊断安全算法";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.Button14_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(505, 110);
            this.button15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(158, 22);
            this.button15.TabIndex = 18;
            this.button15.Text = "EPPLUS Demo1";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.Button15_Click);
            // 
            // lblConnectionString
            // 
            this.lblConnectionString.AutoSize = true;
            this.lblConnectionString.Location = new System.Drawing.Point(505, 138);
            this.lblConnectionString.Name = "lblConnectionString";
            this.lblConnectionString.Size = new System.Drawing.Size(119, 12);
            this.lblConnectionString.TabIndex = 19;
            this.lblConnectionString.Text = "lblConnectionString";
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(341, 138);
            this.button16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(158, 22);
            this.button16.TabIndex = 20;
            this.button16.Text = "ZNA-PU平台诊断安全算法dll";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.Button16_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // btnTestTaskTimer
            // 
            this.btnTestTaskTimer.Location = new System.Drawing.Point(13, 186);
            this.btnTestTaskTimer.Name = "btnTestTaskTimer";
            this.btnTestTaskTimer.Size = new System.Drawing.Size(156, 23);
            this.btnTestTaskTimer.TabIndex = 21;
            this.btnTestTaskTimer.Text = "测试在Task中修改Timer";
            this.btnTestTaskTimer.UseVisualStyleBackColor = true;
            this.btnTestTaskTimer.Click += new System.EventHandler(this.BtnTestTaskTimer_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(177, 186);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(196, 23);
            this.button17.TabIndex = 22;
            this.button17.Text = "置timer1的Enable变量true";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.Button17_Click);
            // 
            // btnTestWindowTop
            // 
            this.btnTestWindowTop.Location = new System.Drawing.Point(379, 187);
            this.btnTestWindowTop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTestWindowTop.Name = "btnTestWindowTop";
            this.btnTestWindowTop.Size = new System.Drawing.Size(158, 22);
            this.btnTestWindowTop.TabIndex = 23;
            this.btnTestWindowTop.Text = "测试窗口置顶";
            this.btnTestWindowTop.UseVisualStyleBackColor = true;
            this.btnTestWindowTop.Click += new System.EventHandler(this.BtnTestWindowTop_Click);
            // 
            // timerTestWindowTop
            // 
            this.timerTestWindowTop.Tick += new System.EventHandler(this.TimerTestWindowTop_Tick);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(543, 187);
            this.button18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(158, 22);
            this.button18.TabIndex = 24;
            this.button18.Text = "Timer中测试Thread.Sleep";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.Button18_Click);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.Timer2_Tick);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(11, 214);
            this.button19.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(158, 22);
            this.button19.TabIndex = 25;
            this.button19.Text = "子窗体中创建线程";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.Button19_Click);
            // 
            // timer3
            // 
            this.timer3.Tick += new System.EventHandler(this.Timer3_Tick);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(177, 214);
            this.button20.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(360, 22);
            this.button20.TabIndex = 26;
            this.button20.Text = "测试Timer中的switch case中的break和return";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.Button20_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(543, 214);
            this.button21.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(158, 22);
            this.button21.TabIndex = 27;
            this.button21.Text = "测试多路阀切换置顶";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.Button21_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 415);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.btnTestWindowTop);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.btnTestTaskTimer);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.lblConnectionString);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioBtnChineseLang;
        private System.Windows.Forms.RadioButton radioBtnEnglishLang;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label lblConnectionString;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnTestTaskTimer;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button btnTestWindowTop;
        private System.Windows.Forms.Timer timerTestWindowTop;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
    }
}

