﻿namespace C4B.TestSomething.Demo.Demo.EPPLUSDemo
{
    partial class FormReplaceStringsOfFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSourceFileName = new System.Windows.Forms.Label();
            this.txtSourceFileName = new System.Windows.Forms.TextBox();
            this.lblTargetFileName = new System.Windows.Forms.Label();
            this.txtTargetFileName = new System.Windows.Forms.TextBox();
            this.richTxtContent = new System.Windows.Forms.RichTextBox();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblFileName = new System.Windows.Forms.Label();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.lblRowRange = new System.Windows.Forms.Label();
            this.txtRowStartNum = new System.Windows.Forms.TextBox();
            this.txtRowEndNum = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblSourceFileName
            // 
            this.lblSourceFileName.AutoSize = true;
            this.lblSourceFileName.Location = new System.Drawing.Point(12, 15);
            this.lblSourceFileName.Name = "lblSourceFileName";
            this.lblSourceFileName.Size = new System.Drawing.Size(43, 13);
            this.lblSourceFileName.TabIndex = 3;
            this.lblSourceFileName.Text = "源文件";
            // 
            // txtSourceFileName
            // 
            this.txtSourceFileName.Location = new System.Drawing.Point(73, 12);
            this.txtSourceFileName.Name = "txtSourceFileName";
            this.txtSourceFileName.ReadOnly = true;
            this.txtSourceFileName.Size = new System.Drawing.Size(715, 20);
            this.txtSourceFileName.TabIndex = 2;
            this.txtSourceFileName.DoubleClick += new System.EventHandler(this.TxtSourceFileName_DoubleClick);
            // 
            // lblTargetFileName
            // 
            this.lblTargetFileName.AutoSize = true;
            this.lblTargetFileName.Location = new System.Drawing.Point(12, 41);
            this.lblTargetFileName.Name = "lblTargetFileName";
            this.lblTargetFileName.Size = new System.Drawing.Size(55, 13);
            this.lblTargetFileName.TabIndex = 5;
            this.lblTargetFileName.Text = "目标文件";
            // 
            // txtTargetFileName
            // 
            this.txtTargetFileName.Location = new System.Drawing.Point(73, 38);
            this.txtTargetFileName.Name = "txtTargetFileName";
            this.txtTargetFileName.ReadOnly = true;
            this.txtTargetFileName.Size = new System.Drawing.Size(715, 20);
            this.txtTargetFileName.TabIndex = 4;
            this.txtTargetFileName.DoubleClick += new System.EventHandler(this.TxtTargetFileName_DoubleClick);
            // 
            // richTxtContent
            // 
            this.richTxtContent.Location = new System.Drawing.Point(15, 93);
            this.richTxtContent.Name = "richTxtContent";
            this.richTxtContent.Size = new System.Drawing.Size(773, 345);
            this.richTxtContent.TabIndex = 6;
            this.richTxtContent.Text = "";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(713, 62);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 7;
            this.btnGenerate.Text = "生成";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.BtnGenerate_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(12, 67);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(43, 13);
            this.lblFileName.TabIndex = 8;
            this.lblFileName.Text = "文件名";
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(73, 64);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.ReadOnly = true;
            this.txtFileName.Size = new System.Drawing.Size(397, 20);
            this.txtFileName.TabIndex = 9;
            // 
            // lblRowRange
            // 
            this.lblRowRange.AutoSize = true;
            this.lblRowRange.Location = new System.Drawing.Point(476, 67);
            this.lblRowRange.Name = "lblRowRange";
            this.lblRowRange.Size = new System.Drawing.Size(43, 13);
            this.lblRowRange.TabIndex = 10;
            this.lblRowRange.Text = "行范围";
            // 
            // txtRowStartNum
            // 
            this.txtRowStartNum.Location = new System.Drawing.Point(537, 64);
            this.txtRowStartNum.Name = "txtRowStartNum";
            this.txtRowStartNum.Size = new System.Drawing.Size(52, 20);
            this.txtRowStartNum.TabIndex = 11;
            this.txtRowStartNum.Text = "571";
            // 
            // txtRowEndNum
            // 
            this.txtRowEndNum.Location = new System.Drawing.Point(595, 64);
            this.txtRowEndNum.Name = "txtRowEndNum";
            this.txtRowEndNum.Size = new System.Drawing.Size(52, 20);
            this.txtRowEndNum.TabIndex = 12;
            this.txtRowEndNum.Text = "1426";
            // 
            // FormReplaceStringsOfFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtRowEndNum);
            this.Controls.Add(this.txtRowStartNum);
            this.Controls.Add(this.lblRowRange);
            this.Controls.Add(this.txtFileName);
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.richTxtContent);
            this.Controls.Add(this.lblTargetFileName);
            this.Controls.Add(this.txtTargetFileName);
            this.Controls.Add(this.lblSourceFileName);
            this.Controls.Add(this.txtSourceFileName);
            this.Name = "FormReplaceStringsOfFile";
            this.Text = "FormReplaceStringsOfFile";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSourceFileName;
        private System.Windows.Forms.TextBox txtSourceFileName;
        private System.Windows.Forms.Label lblTargetFileName;
        private System.Windows.Forms.TextBox txtTargetFileName;
        private System.Windows.Forms.RichTextBox richTxtContent;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label lblRowRange;
        private System.Windows.Forms.TextBox txtRowStartNum;
        private System.Windows.Forms.TextBox txtRowEndNum;
    }
}