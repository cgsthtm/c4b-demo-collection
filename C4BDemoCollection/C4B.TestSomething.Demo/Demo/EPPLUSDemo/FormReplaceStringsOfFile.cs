﻿// <copyright file="FormReplaceStringsOfFile.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.Demo.EPPLUSDemo
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;
    using C4BCodesLibrary.C4BCodes.Comparer;
    using C4BCodesLibrary.C4BCodes.Extensions;
    using OfficeOpenXml;

    /// <summary>
    /// 替换文件中的字符串.
    /// </summary>
    public partial class FormReplaceStringsOfFile : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormReplaceStringsOfFile"/> class.
        /// </summary>
        public FormReplaceStringsOfFile()
        {
            this.InitializeComponent();
        }

        private void TxtSourceFileName_DoubleClick(object sender, EventArgs e)
        {
            this.openFileDialog1.DefaultExt = "Excel Files (*.xlsx)";
            this.openFileDialog1.InitialDirectory = @"D:\MyWork\GitSourceCode\electricsafety2.0";
            DialogResult dialogResult = this.openFileDialog1.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                this.txtSourceFileName.Text = this.openFileDialog1.FileName;
            }
        }

        private void TxtTargetFileName_DoubleClick(object sender, EventArgs e)
        {
            this.openFileDialog1.DefaultExt = "C# Files (*.cs)";
            this.openFileDialog1.InitialDirectory = @"D:\MyWork\GitSourceCode\electricsafety2.0\trunk\ElectricitySafelyTest\ElectricitySafelyTest";
            DialogResult dialogResult = this.openFileDialog1.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                this.txtTargetFileName.Text = this.openFileDialog1.FileName;
                this.txtFileName.Text = this.openFileDialog1.SafeFileName;
            }
        }

        private void BtnGenerate_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtSourceFileName.Text) && !string.IsNullOrEmpty(this.txtTargetFileName.Text))
            {
                var sourcefileName = this.txtSourceFileName.Text;
                var targetfileName = this.txtTargetFileName.Text;
                List<string> zhStrList = new List<string>();
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(sourcefileName))
                {
                    var sheet = package.Workbook.Worksheets["ElectricitySafelyTest"];
                    int rowStart = 0;
                    int.TryParse(this.txtRowStartNum.Text, out rowStart);
                    int rowEnd = 0;
                    int.TryParse(this.txtRowEndNum.Text, out rowEnd);
                    const string colFileName = "B";
                    const string colZh = "C";
                    for (int i = rowStart; i <= rowEnd; i++)
                    {
                        var fileName = sheet.Cells[$"{colFileName}{i}"].Text;
                        var zh = sheet.Cells[$"{colZh}{i}"].Text;
                        if (zh != "微软雅黑")
                        {
                            zhStrList.Add(zh);
                        }
                    }

                    string fileContent = string.Empty;
                    using (FileStream fs = new FileStream(targetfileName, FileMode.Open, FileAccess.ReadWrite))
                    {
                        using (StreamReader sr = new StreamReader(fs))
                        {
                            fileContent = sr.ReadToEnd();
                        }
                    }

                    using (FileStream fs = new FileStream(targetfileName, FileMode.Open, FileAccess.ReadWrite))
                    {
                        var distinctZhStrList = zhStrList.Distinct().ToList();
                        distinctZhStrList.Sort(new LengthFirstStringComparer());
                        foreach (var zhStr in distinctZhStrList)
                        {
                            var newZhStr = this.GetNewStr(zhStr);
                            var pattern = $"\"{zhStr.ToUnicodeStr()}\"|\"{zhStr.ToUnicodeStr()}+.*\""; // 正则表达式替换
                            Regex regex = new Regex(pattern);
                            var newStr = $"ResourceCultureHelper.GetString(\"{newZhStr}\")";
                            fileContent = regex.Replace(fileContent, newStr);
                        }

                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.Write(fileContent);
                            sw.Flush();
                        }
                    }

                    this.richTxtContent.Text = fileContent;
                }
            }
        }

        private string GetNewStr(string zhStr)
        {
            return zhStr.Replace("-", string.Empty).Replace("“", string.Empty).Replace("”", string.Empty).Replace("！", string.Empty).Replace("，", string.Empty)
                        .Replace("(", string.Empty).Replace(")", string.Empty).Replace("\r\n", string.Empty).Replace(".", string.Empty).Replace(" ", string.Empty)
                        .Replace("：", string.Empty).Replace("/", string.Empty).Replace("、", string.Empty).Replace(",", string.Empty).Replace(":", string.Empty)
                        .Replace("（", string.Empty).Replace("）", string.Empty).Replace("!", string.Empty).Replace("+", string.Empty).Replace("{", string.Empty)
                        .Replace("}", string.Empty).Replace("×", string.Empty).Replace("|", string.Empty).Replace("*", string.Empty);
        }
    }
}