﻿namespace C4B.TestSomething.Demo.Demo.EPPLUSDemo
{
    partial class FormExcelEnglishStringsToDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFilePath = new System.Windows.Forms.Label();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnPushEnglishStringsToDb = new System.Windows.Forms.Button();
            this.txtRowEndNum = new System.Windows.Forms.TextBox();
            this.txtRowStartNum = new System.Windows.Forms.TextBox();
            this.lblRowRange = new System.Windows.Forms.Label();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::C4B.TestSomething.Demo.Demo.EPPLUSDemo.WaitForm1), true, true);
            this.SuspendLayout();
            // 
            // lblFilePath
            // 
            this.lblFilePath.AutoSize = true;
            this.lblFilePath.Location = new System.Drawing.Point(13, 13);
            this.lblFilePath.Name = "lblFilePath";
            this.lblFilePath.Size = new System.Drawing.Size(55, 13);
            this.lblFilePath.TabIndex = 0;
            this.lblFilePath.Text = "文件路径";
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(74, 10);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(714, 20);
            this.txtFilePath.TabIndex = 1;
            this.txtFilePath.DoubleClick += new System.EventHandler(this.TxtFilePath_DoubleClick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnPushEnglishStringsToDb
            // 
            this.btnPushEnglishStringsToDb.Location = new System.Drawing.Point(190, 37);
            this.btnPushEnglishStringsToDb.Name = "btnPushEnglishStringsToDb";
            this.btnPushEnglishStringsToDb.Size = new System.Drawing.Size(156, 23);
            this.btnPushEnglishStringsToDb.TabIndex = 2;
            this.btnPushEnglishStringsToDb.Text = "将英文更新到数据库";
            this.btnPushEnglishStringsToDb.UseVisualStyleBackColor = true;
            this.btnPushEnglishStringsToDb.Click += new System.EventHandler(this.BtnPushEnglishStringsToDb_Click);
            // 
            // txtRowEndNum
            // 
            this.txtRowEndNum.Location = new System.Drawing.Point(132, 39);
            this.txtRowEndNum.Name = "txtRowEndNum";
            this.txtRowEndNum.Size = new System.Drawing.Size(52, 20);
            this.txtRowEndNum.TabIndex = 15;
            this.txtRowEndNum.Text = "3656";
            // 
            // txtRowStartNum
            // 
            this.txtRowStartNum.Location = new System.Drawing.Point(74, 39);
            this.txtRowStartNum.Name = "txtRowStartNum";
            this.txtRowStartNum.Size = new System.Drawing.Size(52, 20);
            this.txtRowStartNum.TabIndex = 14;
            this.txtRowStartNum.Text = "2";
            // 
            // lblRowRange
            // 
            this.lblRowRange.AutoSize = true;
            this.lblRowRange.Location = new System.Drawing.Point(13, 42);
            this.lblRowRange.Name = "lblRowRange";
            this.lblRowRange.Size = new System.Drawing.Size(43, 13);
            this.lblRowRange.TabIndex = 13;
            this.lblRowRange.Text = "行范围";
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // FormExcelEnglishStringsToDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtRowEndNum);
            this.Controls.Add(this.txtRowStartNum);
            this.Controls.Add(this.lblRowRange);
            this.Controls.Add(this.btnPushEnglishStringsToDb);
            this.Controls.Add(this.txtFilePath);
            this.Controls.Add(this.lblFilePath);
            this.Name = "FormExcelEnglishStringsToDB";
            this.Text = "FormExcelEnglishStringsToDB";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFilePath;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnPushEnglishStringsToDb;
        private System.Windows.Forms.TextBox txtRowEndNum;
        private System.Windows.Forms.TextBox txtRowStartNum;
        private System.Windows.Forms.Label lblRowRange;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
    }
}