﻿// <copyright file="WaitForm1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.Demo.EPPLUSDemo
{
    using DevExpress.XtraWaitForm;

    /// <summary>
    /// WaitForm1.
    /// </summary>
    public partial class WaitForm1 : WaitForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WaitForm1"/> class.
        /// </summary>
        public WaitForm1()
        {
            this.InitializeComponent();
            this.progressPanel1.AutoHeight = true;
        }

        /// <summary>
        /// WaitFormCommand.
        /// </summary>
        public enum WaitFormCommand
        {
        }

        /// <inheritdoc/>
        public override void SetCaption(string caption)
        {
            base.SetCaption(caption);
            this.progressPanel1.Caption = caption;
        }

        /// <inheritdoc/>
        public override void SetDescription(string description)
        {
            base.SetDescription(description);
            this.progressPanel1.Description = description;
        }

        /// <inheritdoc/>
        public override void ProcessCommand(System.Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }
    }
}