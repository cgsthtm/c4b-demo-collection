﻿// <copyright file="FormGetXmlFromExcel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.Demo.EPPLUSDemo
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using OfficeOpenXml;

    /// <summary>
    /// Excel内容转Xml.
    /// </summary>
    public partial class FormGetXmlFromExcel : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormGetXmlFromExcel"/> class.
        /// </summary>
        public FormGetXmlFromExcel()
        {
            this.InitializeComponent();
        }

        private void FormGetXmlFromExcel_Load(object sender, EventArgs e)
        {
            this.cmbLanguage.SelectedIndex = 0;
        }

        private void BtnGenerate_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtFileName.Text))
            {
                var fileName = this.txtFileName.Text;
                bool isIgnoreEn = this.cmbLanguage.Text.Contains("en-US") ? false : true;
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(fileName))
                {
                    var sheet = package.Workbook.Worksheets["Sheet1"];
                    const int rowStart = 52;
                    const int rowEnd = 1040;
                    const string colZh = "C";
                    const string colEn = "D";
                    StringBuilder sb = new StringBuilder();
                    for (int i = rowStart; i <= rowEnd; i++)
                    {
                        var zh = sheet.Cells[$"{colZh}{i}"].Text;
                        var en = sheet.Cells[$"{colEn}{i}"].Text;
                        sb.AppendLine(this.GetXml(zh, en, isNameIgnorePunctuation: true, isIgnoreEn: isIgnoreEn));
                    }

                    this.richTxtContent.Text = sb.ToString();
                }
            }
        }

        private void TxtFileName_DoubleClick(object sender, EventArgs e)
        {
            this.openFileDialog1.DefaultExt = "Excel Files (*.xlsx)";
            this.openFileDialog1.InitialDirectory = @"D:\MyWork\GitSourceCode\electricsafety2.0";
            DialogResult dialogResult = this.openFileDialog1.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                this.txtFileName.Text = this.openFileDialog1.FileName;
            }
        }

        /// <summary>
        /// 获取XML字符串.
        /// </summary>
        /// <param name="name">待翻译中文.</param>
        /// <param name="value">翻译成的英文.</param>
        /// <param name="isNameIgnorePunctuation">是否Name忽略标点符号.</param>
        /// <param name="isIgnoreEn">是否忽略英文.</param>
        /// <returns>xml字符串.</returns>
        private string GetXml(string name, string value, bool isNameIgnorePunctuation = true, bool isIgnoreEn = false)
        {
            string sb = string.Empty;
            var nameStr = isNameIgnorePunctuation ?
                name.Replace("-", string.Empty).Replace("“", string.Empty).Replace("”", string.Empty).Replace("！", string.Empty).Replace("，", string.Empty)
                .Replace("(", string.Empty).Replace(")", string.Empty).Replace("\r\n", string.Empty).Replace(".", string.Empty).Replace(" ", string.Empty)
                .Replace("：", string.Empty).Replace("/", string.Empty).Replace("、", string.Empty).Replace(",", string.Empty).Replace(":", string.Empty)
                .Replace("（", string.Empty).Replace("）", string.Empty).Replace("!", string.Empty).Replace("+", string.Empty).Replace("{", string.Empty)
                .Replace("}", string.Empty).Replace("×", string.Empty).Replace("|", string.Empty).Replace("*", string.Empty) : name;
            sb += $"<data name=\"{nameStr}\" xml:space=\"preserve\">\r\n";
            value = isIgnoreEn ? name : value;
            sb += $"  <value>{value}</value>\r\n";
            sb += $"</data>";
            return sb;
        }
    }
}
