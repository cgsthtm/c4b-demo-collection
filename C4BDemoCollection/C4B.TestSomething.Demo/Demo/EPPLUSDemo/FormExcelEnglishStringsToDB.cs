﻿// <copyright file="FormExcelEnglishStringsToDB.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.Demo.EPPLUSDemo
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;
    using Autofac;
    using C4B.TestSomething.Demo.BLL;
    using C4B.TestSomething.Demo.Model;
    using C4B.TestSomething.Demo.Utility;
    using OfficeOpenXml;

    /// <summary>
    /// FormExcelEnglishStringsToDB.
    /// </summary>
    public partial class FormExcelEnglishStringsToDB : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormExcelEnglishStringsToDB"/> class.
        /// </summary>
        public FormExcelEnglishStringsToDB()
        {
            this.InitializeComponent();
        }

        private void TxtFilePath_DoubleClick(object sender, System.EventArgs e)
        {
            this.openFileDialog1.DefaultExt = "Excel Files (*.xlsx)";
            this.openFileDialog1.InitialDirectory = @"D:\MyWork\GitSourceCode\electricsafety2.0";
            DialogResult dialogResult = this.openFileDialog1.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                this.txtFilePath.Text = this.openFileDialog1.FileName;
            }
        }

        private async void BtnPushEnglishStringsToDb_Click(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtFilePath.Text))
            {
                var sourcefileName = this.txtFilePath.Text;

                try
                {
                    // DB
                    List<CodeInfoTable> codeInfoTables = null;
                    using (var scrop = AutoFacHelper.GetInstance().Container.BeginLifetimeScope())
                    {
                        ICodeInfoTableService service = scrop.Resolve<ICodeInfoTableService>();
                        codeInfoTables = service.GetAllCodeInfos();

                        // Excel
                        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                        using (var package = new ExcelPackage(sourcefileName))
                        {
                            var sheet = package.Workbook.Worksheets["内部代码-待翻译"];
                            int rowStart = 0;
                            int.TryParse(this.txtRowStartNum.Text, out rowStart);
                            int rowEnd = 0;
                            int.TryParse(this.txtRowEndNum.Text, out rowEnd);
                            const string colZhStr = "C"; // 列
                            const string colEnStr = "D";

                            for (int i = rowStart; i <= rowEnd; i++)
                            {
                                var zhStr = sheet.Cells[$"{colZhStr}{i}"].Text;
                                var enStr = sheet.Cells[$"{colEnStr}{i}"].Text;
                                CodeInfoTable codeInfoTable = codeInfoTables.Where(it => it.CodeName == zhStr).FirstOrDefault();
                                if (codeInfoTable != null)
                                {
                                    codeInfoTable.CodeEnName = enStr;
                                }
                            }

                            this.splashScreenManager1.ShowWaitForm();
                            int res = await service.BulkUpdateCodeEnNameAsync(codeInfoTables); // 批量更新代码英文名称
                            this.splashScreenManager1.CloseWaitForm();
                            MessageBox.Show($"更新成功！结果:{res}");
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}