﻿// <copyright file="Form1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Reflection;
    using System.Runtime.ExceptionServices;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using C4B.TestSomething.Demo.Demo.EPPLUSDemo;
    using C4B.TestSomething.Demo.Enum;
    using C4B.TestSomething.Demo.Forms;
    using C4B.TestSomething.Demo.SecurityAlgorithm;
    using C4BCodesLibrary.C4BCodes.Enum;
    using C4BCodesLibrary.C4BCodes.Utilities;
    using C4BCodesLibrary.C4BCodes.Utilities.PComm;
    using C4BCodesLibrary.C4BCodes.Win32Api;
    using NModbus;
    using SuperSocket.SocketBase;
    using SuperSocket.SocketBase.Protocol;

    /// <summary>
    /// Form1.
    /// </summary>
    public partial class Form1 : Form
    {
        private bool isAllowTimer1Enable = false;

        /// <summary>
        /// 蓝色窗体.
        /// </summary>
        private Forms.FormSetTop1 form1 = new Forms.FormSetTop1();

        /// <summary>
        /// 红色窗体.
        /// </summary>
        private Forms.FormSetTop2 form2 = new Forms.FormSetTop2();

        /// <summary>
        /// 测试Timer中的switch case中的break和return.
        /// </summary>
        private int bkPoint = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="Form1"/> class.
        /// </summary>
        public Form1()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 他妈的，如果DllImportAttribute不加CallingConvention = CallingConvention.Cdecl，就会报错！卧槽！.
        /// </summary>
        /// <param name="iSeedArray">种子字节数组.</param>
        /// <param name="iSeedArraySize">种子长度.</param>
        /// <param name="iSecurityLevel">安全等级.</param>
        /// <param name="iVariant">变量.</param>
        /// <param name="ioKeyArray">密匙字节数组.</param>
        /// <param name="iKeyArraySize">密匙长度.</param>
        /// <param name="oSize">输出长度.</param>
        /// <returns>res.</returns>
        [DllImport("SecurityAccess_DFLZ_customer.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GenerateKeyEx(IntPtr iSeedArray, uint iSeedArraySize, uint iSecurityLevel, string iVariant, IntPtr ioKeyArray, uint iKeyArraySize, ref uint oSize);

        /// <summary>
        /// read hex file.
        /// </summary>
        /// <param name="fileName">filename.</param>
        /// <returns>return result.</returns>
        public bool ReadHexFile(string fileName)
        {
            if (fileName == null || fileName.Trim() == string.Empty) // 文件存在
            {
                return false;
            }

            using (FileStream fs = new FileStream(fileName, FileMode.Open)) // open file
            {
                StreamReader hexReader = new StreamReader(fs);    // 读取数据流
                string szLine = string.Empty;
                string szHex = string.Empty;
                string szAddress = string.Empty;
                string szLength = string.Empty;
                while (true)
                {
                    szLine = hexReader.ReadLine();      // 读取Hex中一行
                    if (szLine == null)
                    {
                        break;
                    } // 读取完毕，退出

                    if (szLine.Substring(0, 1) == ":") // 判断首字符是”:”
                    {
                        if (szLine.Substring(1, 8) == "00000001")
                        {
                            break;
                        } // 文件结束标识

                        // 直接解析数据类型标识为 : 00 和 01 的格式
                        if ((szLine.Substring(8, 1) == "0") || (szLine.Substring(8, 1) == "1"))
                        {
                            szHex += szLine.Substring(9, szLine.Length - 11);  // 所有数据分一组 
                            szAddress += szLine.Substring(3, 4); // 所有起始地址分一组
                            szLength += szLine.Substring(1, 2); // 所有数据长度归一组
                        }
                    }
                }

                // 将数据字符转换为Hex，并保存在数组 szBin[]
                int j = 0;
                int length = szHex.Length;      // 获取长度
                byte[] szBin = new byte[length / 2];
                for (int i = 0; i < length; i += 2)
                {
                    szBin[j] = (byte)int.Parse(szHex.Substring(i, 2), System.Globalization.NumberStyles.HexNumber); // 两个字符合并一个Hex
                    j++;
                }

                // 将起始地址的字符转换为Hex，并保存在数组 szAdd []
                j = 0;
                length = szAddress.Length;      // get bytes number of szAddress
                int[] szAdd = new int[length / 4];
                for (int i = 0; i < length; i += 4)
                {
                    szAdd[j] = int.Parse(szAddress.Substring(i, 4), System.Globalization.NumberStyles.HexNumber);    // 两个字符合并一个Hex
                    j++;
                }

                // 将长度字符转换为Hex，并保存在数组 szLen []
                j = 0;
                length = szLength.Length;      // get bytes number of szAddress
                byte[] szLen = new byte[length / 2];
                for (int i = 0; i < length; i += 2)
                {
                    szLen[j] = (byte)int.Parse(szLength.Substring(i, 2), System.Globalization.NumberStyles.HexNumber); // 合并成hex类型
                    j++;
                }

                Console.WriteLine(1);
            }

            return true;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            DialogResult dialogResult = openFileDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                HexFileHelper.SRecFile srecFile = new HexFileHelper.SRecFile(openFileDialog.FileName);
                srecFile.ReadSRecFile();
                sw.Stop();
                Debug.WriteLine(sw.ElapsedMilliseconds.ToString());
                Debug.WriteLine($"起始地址：{srecFile.StartsAt.ToHexString()}");
                Debug.WriteLine($"数据长度：{srecFile.Length.ToHexString()}");
                ////Debug.WriteLine($"数据：{srecFile.Data.ToHexString()}");
            }
        }

        private async void Button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            DialogResult dialogResult = openFileDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                ////this.ReadHexFile(openFileDialog.FileName);
                HexFileHelper.HexFile hexFile = new HexFileHelper.HexFile(openFileDialog.FileName);
                Task task = hexFile.ReadHexFile();
                await task;
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            // iSeedArray
            ////var seed = new byte[] { 0x1e, 0xdd, 0xa3, 0x59, 0x1e, 0xdd, 0xa3, 0x59 };
            ////var seed = new byte[] { 0x3C, 0x9F, 0x56, 0xF5, 0x3C, 0x9F, 0x56, 0xF5 };
            var seed = new byte[] { 0x38, 0xB7, 0x4A, 0x9A, 0x38, 0xB7, 0x4A, 0x9A };
            IntPtr seedPtr = Marshal.AllocHGlobal(Marshal.SizeOf(seed[0]) * seed.Length);
            Marshal.Copy(seed, 0, seedPtr, seed.Length);

            // iSeedArraySize
            uint seedArrSize = (uint)seed.Length;

            // iSecurityLevel
            uint level = 3;
            ////uint level = 2; // fault
            ////uint level = 1; // fault
            ////uint level = 11; // fault

            // iVariant
            string variant = "A";

            // ioKeyArray
            var key = new byte[8] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            IntPtr keyPtr = Marshal.AllocHGlobal(Marshal.SizeOf(key[0]) * key.Length);
            Marshal.Copy(key, 0, keyPtr, key.Length);
            var key1 = new byte[8] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            Marshal.Copy(keyPtr, key1, 0, key.Length);
            Debug.WriteLine($"key1 :{key1.ToHexString()}");

            // Marshal.Copy(key, 0, keyPtr, key.Length);
            // iKeyArraySize
            uint keyArrSize = (uint)key.Length;

            // oSize
            uint size = 8;

            try
            {
                GenerateKeyEx(seedPtr, seedArrSize, level, variant, keyPtr, keyArrSize, ref size);

                var seed2 = new byte[seed.Length];
                var key2 = new byte[key.Length];
                Marshal.Copy(seedPtr, seed2, 0, seed.Length);
                Marshal.Copy(keyPtr, key2, 0, key.Length);
                Marshal.Copy(keyPtr, key, 0, key.Length);

                Debug.WriteLine($"seed2:{seed2.ToHexString()}");
                Debug.WriteLine($"key2 :{key2.ToHexString()}");
                Debug.WriteLine($"key  :{key.ToHexString()}");
            }
            finally
            {
                Marshal.FreeHGlobal(seedPtr);
                Marshal.FreeHGlobal(keyPtr);
            }
        }

        private async void Button4_Click(object sender, EventArgs e)
        {
            int port = 5;

            CancellationTokenSource cts = new CancellationTokenSource();
            Task task = Task.Run(
                async () =>
            {
                PCommHelper.sio_open(port);
                PCommHelper.sio_ioctl(port, PCommHelper.B115200, PCommHelper.P_NONE | PCommHelper.BIT_8 | PCommHelper.STOP_1);
                byte[] wd50 = new byte[] { 0x50, 0x03, 0x00, 0x34, 0x00, 0x01, 0xC8, 0x45 };
                byte[] wd51 = new byte[] { 0x51, 0x03, 0x00, 0x34, 0x00, 0x01, 0xC8, 0x45 };
                int count = 0;

                while (!cts.IsCancellationRequested)
                {
                    // 发送
                    PCommHelper.sio_write(port, ref wd50[0], wd50.Length);

                    // 接受维特WT53R-485返回的数据
                    await Task.Delay(100);
                    int len = PCommHelper.sio_iqueue(port);
                    if (len > 0)
                    {
                        byte[] rd = new byte[len];
                        PCommHelper.sio_read(port, ref rd[0], len);
                        byte[] data = new byte[2];
                        Array.Copy(rd, 3, data, 0, 2);
                        short value = ByteHelper.ConvertToDecimalistValue<short>(data, ByteOrdering.BigEndian);
                        Debug.WriteLine($"0x50 原始数据：{rd.ToHexString()} 距离：{value}");
                        if (++count > 10)
                        {
                            cts.Cancel();
                        }
                    }

                    await Task.Delay(1000);
                }
            }, cts.Token);
            await Task.Delay(5000);
            try
            {
                await task;
            }
            catch (AggregateException exception)
            {
                exception = exception.Flatten();
                try
                {
                    exception.Handle(innerException =>
                    {
                        // Rethrowing rather than using
                        // if condition on the type
                        ExceptionDispatchInfo.Capture(
                            innerException)
                            .Throw();
                        return true;
                    });
                }
                catch (WebException)
                {
                    // ...
                }
                catch (IOException)
                {
                    // ...
                }
                catch (NotSupportedException)
                {
                    // ...
                }
            }

            Debug.Write("yy");
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            /*
大端序（Big-Endian）和小端序（Little-Endian）是两种不同的数据存储顺序。这两种方式都会影响到计算机在内存中如何存储多字节的数据类型，例如16位或32位整数。

大端序（Big-Endian）：
在大端序中，高位字节存储在内存中的较低地址处，低位字节存储在较高的地址处。这种存储方式与人类通常的阅读习惯相符，因此也被称作“网络字节顺序”。

例如，如果你有一个16位的值0x1234，它在大端序下的存储方式如下：

code
Address:  0x00  0x01
Value:   0x12  0x34
小端序（Little-Endian）：
在小端序中，低位字节存储在内存中的较低地址处，高位字节存储在较高的地址处。许多现代计算机系统都采用小端序进行数据存储。

例如，对于相同的16位值0x1234，在小端序下的存储方式如下：

code
Address:  0x00  0x01
Value:   0x34  0x12
需要注意的是，当你处理多字节的数据时，必须了解你的系统使用的是哪种字节序，以便正确地解释这些数据。如果需要在不同字节序的系统之间交换数据，你可能需要进行转换。
             */

            // 注意：这个例子假设你的原始字节序列是小端序（little-endian），并且你想得到的结果也是小端序。
            // 如果这不是你想要的，或者字节顺序相反，你可能需要调整转换过程以适应你的具体需求。
            byte[] b = new byte[] { 0x07, 0x0B };

            // 将字节顺序反转
            var br = b.Reverse().ToArray();

            // 将两个字节合并为一个短整型（short）
            short value = BitConverter.ToInt16(br, 0);

            // 现在你有一个16位的值
            Console.WriteLine("Value: " + value);
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            var appServer = new AppServer();
            appServer.NewSessionConnected += new SessionHandler<AppSession>(this.AppServer_NewSessionConnected);
            appServer.NewRequestReceived += new RequestHandler<AppSession, StringRequestInfo>(this.AppServer_NewRequestReceived);

            // Setup the appServer
            if (!appServer.Setup(502)) // Setup with listening port
            {
                Console.WriteLine("Failed to setup!");
                Console.ReadKey();
                return;
            }

            // Try to start the appServer
            if (!appServer.Start())
            {
                Console.WriteLine("Failed to start!");
                Console.ReadKey();
                return;
            }

            appServer.Stop();

            // Modbus写多个寄存器 - 郑州日产BSD标定项目 - 摆正器
            TcpClient tcpClient = new TcpClient("127.0.0.1", 502);
            var modbusFactory = new ModbusFactory();
            IModbusMaster modbusMaster = modbusFactory.CreateMaster(tcpClient);
            modbusMaster.WriteMultipleRegisters(0x02, 1000, new ushort[] { 1, 1, 1, 1 });

            // Modbus写多个寄存器 - 郑州日产BSD标定项目 - 伺服电机
            modbusMaster.WriteMultipleRegisters(0x03, 20, new ushort[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
            this.ServoWriteControl(modbusMaster, ServoWriteAction.LFZEnable, new ushort[] { 0 });
            var bytes = BitConverter.GetBytes(100); // 将100写入左前Z伺服目标位置
            Array.Reverse(bytes);
            var shortArray = new[] { (ushort)bytes.BytesToShort(0), (ushort)bytes.BytesToShort(2) };
            var shortArray1 = ByteHelper.ConvertBytesToUShorts(100);
            this.ServoWriteControl(modbusMaster, ServoWriteAction.LFZTargetLocation, shortArray1);
            ushort lowOrderValue = BitConverter.ToUInt16(BitConverter.GetBytes(100), 0);
            ushort highOrderValue = BitConverter.ToUInt16(BitConverter.GetBytes(100), 2);
            this.ServoWriteControl(modbusMaster, ServoWriteAction.LFZTargetSpeed, new ushort[] { lowOrderValue, highOrderValue });

            /* C#中BitConverter.GetBytes方法返回的字节顺序取决于执行环境的CPU架构。
             * 在小端（Little-Endian）架构（例如常见的Intel x86和x64处理器）上运行时，BitConverter.GetBytes会按照小端序来排列字节数组。而在大端（Big-Endian）架构上，则会使用大端序。
             * 小端序（Little-Endian）的存储方式是将最低有效字节存储在最小地址处。举个例子，假设有一个16位的整数值 0x1234（十进制：4660），它的二进制形式为 0001 0010 0011 0100。
             * 所以，如果使用C#的BitConverter.GetBytes在一个小端序机器上获取这个整数的字节序列，将会得到数组 { 0x34, 0x12 }。
             */
            ushort lowOrderValue1 = BitConverter.ToUInt16(BitConverter.GetBytes(100), 0); // 大端序-高位在前，如0xFF342109在地址0x1000上的字节为FF，在0x1003上的字节为0x09
            ushort highOrderValue1 = BitConverter.ToUInt16(BitConverter.GetBytes(100), 2); // 100转为字节数组为{ 0x64,0,0,0 }
            this.ServoWriteControl(modbusMaster, ServoWriteAction.RFZTargetLocation, new ushort[] { lowOrderValue1, highOrderValue1 });
        }

        /// <summary>
        /// 伺服写控制.
        /// </summary>
        /// <param name="modbusMaster">modbus对象.</param>
        /// <param name="servoWriteAction">伺服写动作枚举.</param>
        /// <param name="writeData">写入数据.</param>
        private void ServoWriteControl(IModbusMaster modbusMaster, ServoWriteAction servoWriteAction, ushort[] writeData)
        {
            modbusMaster.WriteMultipleRegisters(0x03, (ushort)servoWriteAction, writeData);
        }

        private void AppServer_NewRequestReceived(AppSession session, StringRequestInfo requestInfo)
        {
            switch (requestInfo.Key.ToUpper())
            {
                case "ECHO":
                    session.Send(requestInfo.Body);
                    break;

                case "ADD":
                    session.Send(requestInfo.Parameters.Select(p => Convert.ToInt32(p)).Sum().ToString());
                    break;

                case "MULT":

                    var result = 1;

                    foreach (var factor in requestInfo.Parameters.Select(p => Convert.ToInt32(p)))
                    {
                        result *= factor;
                    }

                    session.Send(result.ToString());
                    break;
            }
        }

        private void AppServer_NewSessionConnected(AppSession session)
        {
            Console.WriteLine($"SessionID: {session.SessionID} ");
            ////session.Send("Welcome to SuperSocket Telnet Server");
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            TcpClient tcpClient = new TcpClient("127.0.0.1", 502);
            var modbusFactory = new ModbusFactory();
            IModbusMaster modbusMaster = modbusFactory.CreateMaster(tcpClient);

            // NModbus读多个寄存器
            ushort[] ushorts = modbusMaster.ReadHoldingRegisters(0x03, 20, 125);
            int index = 20;
            foreach (ushort s in ushorts)
            {
                Console.WriteLine($"{index} {s}");
                index++;
            }

            Console.WriteLine($"{(ushort)(ushorts[95] << 16 | ushorts[94])}");
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            TcpClient tcpClient = new TcpClient("127.0.0.1", 502);
            var modbusFactory = new ModbusFactory();
            IModbusMaster modbusMaster = modbusFactory.CreateMaster(tcpClient);

            // NModbus读多个输入
            bool[] ushorts = modbusMaster.ReadInputs(0x01, 0, 29);
            int index = 0;
            foreach (bool s in ushorts)
            {
                Console.WriteLine($"{index} {s}");
                index++;
            }
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            byte[] data = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };
            byte[] res = this.GetFirstFrame(data);
            foreach (byte b in res)
            {
                Console.WriteLine(b.ToString("X2"));
            }
        }

        private byte[] GetFirstFrame(byte[] data)
        {
            int validDataLen = data.Length + 3; // 2E F1 90
            int exceptHeadByteLen = validDataLen - 6;
            int leftFrameCount = exceptHeadByteLen / 7;
            if (exceptHeadByteLen % 7 > 0)
            {
                leftFrameCount += 1;
            }

            int totalByteLen = validDataLen + leftFrameCount + 2; // 有效数据长度+剩余帧数+首帧前2个字节=总字节个数
            byte[] commands = new byte[totalByteLen];

            // 组装首帧
            commands[0] = 0x10;
            commands[1] = (byte)validDataLen;
            commands[2] = 0x2E;
            commands[3] = 0xF1;
            commands[4] = 0x90;
            Array.Copy(data, 0, commands, 5, 3);

            // 组装剩余帧
            byte frameIndex = 0x21;
            int indexOfData = 3;
            int indexOfCommands = 8;
            for (int i = 0; i < leftFrameCount; i++)
            {
                var command = new byte[8];
                command[0] = frameIndex;
                int leftDataLen = data.Length - indexOfData;
                if (leftDataLen >= 7)
                {
                    Array.Copy(data, indexOfData, command, 1, 7);
                    indexOfData += 7;
                    frameIndex += 1;
                    Array.Copy(command, 0, commands, indexOfCommands, 8);
                    indexOfCommands += 8;
                }
                else
                {
                    Array.Copy(data, indexOfData, command, 1, leftDataLen);
                    indexOfData += leftDataLen;
                    frameIndex += 1;
                    Array.Copy(command, 0, commands, indexOfCommands, leftDataLen + 1);
                    indexOfCommands += leftDataLen + 1;
                }
            }

            return commands;
        }

        /// <summary>
        /// 测试.
        /// </summary>
        /// <param name="sender">sender.</param>
        /// <param name="e">e.</param>
        private void Button10_Click(object sender, EventArgs e)
        {
            TcpClient tcpClient = new TcpClient("10.14.7.214", 18032);
            var modbusFactory = new ModbusFactory();
            IModbusMaster modbusMaster = modbusFactory.CreateMaster(tcpClient);

            // NModbus读多个寄存器
            ushort[] ushorts = modbusMaster.ReadHoldingRegisters(0x01, 114, 2); // 114:3 115:3392
            this.label1.Text = $"高位在前 high0:{ushorts[0]} low1:{ushorts[1]} val:{(int)(ushorts[1] << 16 | ushorts[0])}"; // 222298115
            this.label1.Text = $"高位在前 high0:{ushorts[0]} low1:{ushorts[1]} val:{(int)(ushorts[0] << 16 | ushorts[1])}"; // 200000
        }

        private void Button11_Click(object sender, EventArgs e)
        {
            ushort h = 3;
            ushort l = 3392;
            int val = (int)(h << 16 | l);
            MessageBox.Show($"{val}");
        }

        private void Button12_Click(object sender, EventArgs e)
        {
            var xmlStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?><response><cmd>GetChargeDetection</cmd><code>0</code><msg>成功</msg><data><BatteryMaxTemp>30</BatteryMaxTemp><BatteryCellMaxVolt>3.7</BatteryCellMaxVolt><BatteryCellVoltRange>0.1</BatteryCellVoltRange><BatteryMaxTempNum>2</BatteryMaxTempNum><BatteryCellMaxVoltNum>4</BatteryCellMaxVoltNum><BatteryMinTemp>21</BatteryMinTemp><BatteryMinTempNum>4</BatteryMinTempNum><ChargingVoltageMeter>3.47</ChargingVoltageMeter><ChargingCurrentMeter>3.23</ChargingCurrentMeter><SOC>38</SOC><OutputVoltage>3.44</OutputVoltage><OutputCurrent>3.98</OutputCurrent><DemandVoltage>4.29</DemandVoltage><DemandCurrent>4.32</DemandCurrent><ChargingMode>1</ChargingMode><MaxAllowChargingVoltage>5.67</MaxAllowChargingVoltage><MaxAllowChargingCurrent>4.29</MaxAllowChargingCurrent><TotalEnergy>400</TotalEnergy><MaxAllowTotalVoltage>23.4</MaxAllowTotalVoltage><MaxAllowChargingTemp>45</MaxAllowChargingTemp><VehicleSOC>50</VehicleSOC><BatteryVoltage>24.3</BatteryVoltage><BatteryType>1</BatteryType><BatteryPackNum>34534567</BatteryPackNum></data></response>";
            Model.Response res = XmlHelper.Deserialize<Model.Response>(xmlStr);
            Console.WriteLine(res.Cmd);
            for (int i = 0; i < res.Data.Length; i++)
            {
                Console.WriteLine($"{res.Data[i].Name} : {res.Data[i].InnerText}");
            }
        }

        private void RadioBtnLanguage_CheckedChanged(object sender, EventArgs e)
        {
            ResourceCultureHelper.BaseName = "C4B.TestSomething.Demo.Strings.Resource1";
            ResourceCultureHelper.ExecutingAssembly = Assembly.GetExecutingAssembly();
            RadioButton radioButton = (RadioButton)sender;
            switch (radioButton.Name)
            {
                case "radioBtnEnglishLang":
                    if (radioButton.Checked)
                    {
                        ResourceCultureHelper.SetCurrentCulture("en-US");
                        this.Text = ResourceCultureHelper.GetString("你好");
                    }

                    break;
                case "radioBtnChineseLang":
                    if (radioButton.Checked)
                    {
                        ResourceCultureHelper.SetCurrentCulture("zh-CN");
                        this.Text = ResourceCultureHelper.GetString("你好");
                    }

                    break;
                default:
                    break;
            }
        }

        private void Button13_Click(object sender, EventArgs e)
        {
            ////FormGetXmlFromExcel form = new FormGetXmlFromExcel();
            ////form.ShowDialog();
            FormReplaceStringsOfFile form = new FormReplaceStringsOfFile();
            form.ShowDialog();
        }

        /// <summary>
        /// 郑州日产PU平台诊断安全算法.
        /// </summary>
        /// <param name="sender">s.</param>
        /// <param name="e">e.</param>
        private void Button14_Click(object sender, EventArgs e)
        {
            uint seed = 0x51D96F04;
            uint mask = 0x51D96F04;
            uint key = ZNAPUDiagnosticSecurityAlgorithm.SeedToKey(seed, mask);
            Console.WriteLine($"密匙：{key.ToString("X2")}");
            byte[] reverses = BitConverter.GetBytes(key).Reverse().ToArray();
            Console.WriteLine($"密匙：");
            foreach (byte b in reverses)
            {
                Console.WriteLine(b.ToString("X2")); // 2C916034
            }
        }

        private void Button15_Click(object sender, EventArgs e)
        {
            FormExcelEnglishStringsToDB form = new FormExcelEnglishStringsToDB();
            form.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            this.lblConnectionString.Text = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString.ToString();
        }

        private void Button16_Click(object sender, EventArgs e)
        {
            uint seed = 0x51D96F04;
            uint mask = 0x51D96F04;
            var seedArr = new byte[] { 0x38, 0xB7, 0x4A, 0x9A };
            var maskArr = new byte[] { 0x72, 0xF9, 0x6D, 0x1E };
            IntPtr seedPtr = Marshal.AllocHGlobal(Marshal.SizeOf(seedArr[0]) * seedArr.Length);
            IntPtr maskPtr = Marshal.AllocHGlobal(Marshal.SizeOf(maskArr[0]) * maskArr.Length);
            Marshal.Copy(seedArr, 0, seedPtr, seedArr.Length);
            Marshal.Copy(maskArr, 0, maskPtr, maskArr.Length);
            IntPtr keyPtr = ZNAPUDiagnosticSecurityAlgorithm.GenerateKeyEx(seedPtr, maskPtr);
            var key = new byte[4] { 0xFF, 0xFF, 0xFF, 0xFF };
            Marshal.Copy(keyPtr, key, 0, key.Length);
            Console.WriteLine($"密匙：{BitConverter.ToUInt32(key, 0)}");
            byte[] reverses = key.Reverse().ToArray();
            Console.WriteLine($"密匙：");
            foreach (byte b in reverses)
            {
                Console.WriteLine(b.ToString("X2")); // 2C916034
            }
        }

        private async void BtnTestTaskTimer_Click(object sender, EventArgs e)
        {
            await Task.Run(async () =>
            {
                while (!this.isAllowTimer1Enable)
                {
                    await Task.Delay(300).ConfigureAwait(false);
                }

            });
            ////this.timer1.Enabled = true; // 可以
            this.Invoke(new Action(() => this.timer1.Enabled = true)); // 可以
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            Debug.WriteLine("1");
        }

        private void Button17_Click(object sender, EventArgs e)
        {
            this.isAllowTimer1Enable = true;
        }

        private void BtnTestWindowTop_Click(object sender, EventArgs e)
        {
            this.form1.Show();
            this.form2.Show();
            this.timerTestWindowTop.Interval = 3000; // 3秒间隔
            this.timerTestWindowTop.Enabled = true;
        }

        private void TimerTestWindowTop_Tick(object sender, EventArgs e)
        {
            if (Win32ApiHelper.IsTopMost(this.form1.Handle))
            {
                Win32ApiHelper.SetTopMost(this.form1.Handle, false);
                Win32ApiHelper.SetTopMost(this.form2.Handle, true);
            }
            else
            {
                Win32ApiHelper.SetTopMost(this.form1.Handle, true);
                Win32ApiHelper.SetTopMost(this.form2.Handle, false);
            }
        }

        private void Button18_Click(object sender, EventArgs e)
        {
            this.timer2.Enabled = true;
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            Console.WriteLine("Timer中测试Thread.Sleep");
            for (int i = 0; i < 5; i++)
            {
                Thread.Sleep(2000);
            }

            Console.WriteLine("Timer中测试Thread.Sleep_OK");
        }

        private void Button19_Click(object sender, EventArgs e)
        {
            FormSonFormCreateThread form = new FormSonFormCreateThread();
            form.ShowDialog();
            form.Dispose();
        }

        private void Button20_Click(object sender, EventArgs e)
        {
            this.timer3.Enabled = true;
        }

        private void Timer3_Tick(object sender, EventArgs e)
        {
            switch (this.bkPoint)
            {
                case 0:
                    Console.WriteLine($"bkPoint:{this.bkPoint}");
                    this.bkPoint = 1;
                    break;

                case 1:
                    Console.WriteLine($"bkPoint:{this.bkPoint}");
                    this.bkPoint = 2;
                    break;

                case 2:
                    Console.WriteLine($"bkPoint:{this.bkPoint}");
                    if (false)
                    {
                        this.bkPoint = 999;
                        break;
                    }
                    else
                    {
                        this.bkPoint = 4;
                        return;
                    }

                    this.bkPoint = 3;

                    break;

                case 3:
                    Console.WriteLine($"bkPoint:{this.bkPoint}");
                    this.bkPoint = 4;
                    break;

                case 4:
                    Console.WriteLine($"bkPoint:{this.bkPoint}");
                    this.bkPoint = 5;
                    break;

                case 5:
                    Console.WriteLine($"bkPoint:{this.bkPoint}");
                    this.bkPoint= 6;
                    break;

                case 999:
                    Console.WriteLine($"bkPoint:{this.bkPoint}");
                    this.bkPoint = int.MaxValue;
                    break;

                default:
                    break;
            }
        }

        private void Button21_Click(object sender, EventArgs e)
        {
            FrmTestFormSwitchSetTop frm = new FrmTestFormSwitchSetTop();
            frm.ShowDialog();
        }
    }
}