﻿// <copyright file="TestSonFormCreateThreadLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.BLL
{
    using C4B.TestSomething.Demo.Utility;

    /// <summary>
    /// TetsSonFormCreateThreadLogic.
    /// </summary>
    internal class TestSonFormCreateThreadLogic
    {
        private RecvDataThread recvDataThread;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestSonFormCreateThreadLogic"/> class.
        /// </summary>
        public TestSonFormCreateThreadLogic()
        {
        }

        /// <summary>
        /// StartCAN.
        /// </summary>
        /// <param name="start">start.</param>
        public void StartCAN(bool start)
        {
            if (this.recvDataThread == null)
            {
                this.recvDataThread = new RecvDataThread();
                this.recvDataThread.SetStart(start);
            }
            else
            {
                this.recvDataThread.SetStart(start);
            }
        }
    }
}
