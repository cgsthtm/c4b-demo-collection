﻿// <copyright file="CodeInfoTableService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.BLL
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using C4B.TestSomething.Demo.DAL;
    using C4B.TestSomething.Demo.Model;

    /// <summary>
    /// CodeInfoTableService.
    /// </summary>
    public class CodeInfoTableService : ICodeInfoTableService
    {
        private ICodeInfoTableDao codeInfoDao;

        /// <summary>
        /// Initializes a new instance of the <see cref="CodeInfoTableService"/> class.
        /// </summary>
        /// <param name="dao">dao.</param>
        public CodeInfoTableService(ICodeInfoTableDao dao)
        {
            this.codeInfoDao = dao;
        }

        /// <inheritdoc/>
        public Task<int> BulkUpdateCodeEnNameAsync(List<CodeInfoTable> list)
        {
            return this.codeInfoDao.BulkUpdateAsync(list, new string[] { "CodeTypeID", "CodeID" }, new string[] { "CodeEnName" });
        }

        /// <inheritdoc/>
        public List<CodeInfoTable> GetAllCodeInfos()
        {
            return this.codeInfoDao.QueryAll();
        }
    }
}
