﻿// <copyright file="ICodeInfoTableService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.TestSomething.Demo.BLL
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using C4B.TestSomething.Demo.Model;

    /// <summary>
    /// ICodeInfoTableService.
    /// </summary>
    public interface ICodeInfoTableService
    {
        /// <summary>
        /// 获取所有CodeInfo.
        /// </summary>
        /// <returns>列表.</returns>
        List<CodeInfoTable> GetAllCodeInfos();

        /// <summary>
        /// 批量更新代码英文名称.
        /// </summary>
        /// <param name="list">列表.</param>
        /// <returns>res.</returns>
        Task<int> BulkUpdateCodeEnNameAsync(List<CodeInfoTable> list);
    }
}