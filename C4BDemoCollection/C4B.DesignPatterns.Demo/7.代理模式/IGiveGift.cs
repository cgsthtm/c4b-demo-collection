﻿// <copyright file="IGiveGift.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._7.代理模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 代理送礼物接口.
    /// </summary>
    public interface IGiveGift
    {
        /// <summary>
        /// 送钱.
        /// </summary>
        void GiveDolls();

        /// <summary>
        /// 送花.
        /// </summary>
        void GiveFlowers();

        /// <summary>
        /// 送巧克力.
        /// </summary>
        void GiveChocolate();
    }
}
