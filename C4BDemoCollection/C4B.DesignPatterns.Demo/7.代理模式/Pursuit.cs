﻿// <copyright file="Pursuit.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._7.代理模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 追求者类.
    /// </summary>
    public class Pursuit : IGiveGift // 追求者实现“送礼物”接口
    {
        private SchoolGirl mm;

        /// <summary>
        /// Initializes a new instance of the <see cref="Pursuit"/> class.
        /// </summary>
        /// <param name="mm">学妹.</param>
        public Pursuit(SchoolGirl mm)
        {
            this.mm = mm;
        }

        /// <inheritdoc/>
        public void GiveChocolate()
        {
            SerilogHelper.Instance.Logger.Information("追求者送 {mm} 巧克力", this.mm.Name);
        }

        /// <inheritdoc/>
        public void GiveDolls()
        {
            SerilogHelper.Instance.Logger.Information("追求者送 {mm} 洋娃娃", this.mm.Name);
        }

        /// <inheritdoc/>
        public void GiveFlowers()
        {
            SerilogHelper.Instance.Logger.Information("追求者送 {mm} 花骨朵", this.mm.Name);
        }
    }
}
