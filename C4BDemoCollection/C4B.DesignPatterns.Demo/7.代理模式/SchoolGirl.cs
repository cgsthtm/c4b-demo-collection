﻿// <copyright file="SchoolGirl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._7.代理模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 学妹类.
    /// </summary>
    public class SchoolGirl
    {
        /// <summary>
        /// Gets or sets 姓名.
        /// </summary>
        public string Name { get; set; }
    }
}
