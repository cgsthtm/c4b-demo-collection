﻿// <copyright file="RealSubject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._7.代理模式.代理模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// RealSubject类，定义Proxy所代表的真实实体.
    /// </summary>
    public class RealSubject : Subject
    {
        /// <inheritdoc/>
        public override void Request()
        {
            SerilogHelper.Instance.Logger.Information("真实的请求.");
        }
    }
}
