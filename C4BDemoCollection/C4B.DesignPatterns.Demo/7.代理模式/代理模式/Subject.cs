﻿// <copyright file="Subject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._7.代理模式.代理模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Subject类，定义了RealSubject和Proxy的共用接口，这样就在任何使用RealSubject的地方都可以使用Proxy.
    /// </summary>
    public abstract class Subject
    {
        /// <summary>
        /// 请求.
        /// </summary>
        public abstract void Request();
    }
}
