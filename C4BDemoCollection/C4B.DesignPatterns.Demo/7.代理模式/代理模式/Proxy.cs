﻿// <copyright file="Proxy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._7.代理模式.代理模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Proxy类，保存一个引用使得代理可以访问实体，并提供一个与Subject的接口相同的接口，这样代理就可以用来替代实体.
    /// </summary>
    public class Proxy : Subject
    {
        private RealSubject realSubject;

        /// <inheritdoc/>
        public override void Request()
        {
            if (this.realSubject == null)
            {
                this.realSubject = new RealSubject();
            }

            this.realSubject.Request();
        }
    }
}
