﻿// <copyright file="Proxy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._7.代理模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 代理类.
    /// 代理模式（Proxy），为其他对象提供一种代理以控制对这个对象的访问。[DP].
    /// 
    /// </summary>
    public class Proxy : IGiveGift // 让代理也实现“送礼物”接口
    {
        private Pursuit gg;

        /// <summary>
        /// Initializes a new instance of the <see cref="Proxy"/> class.
        /// </summary>
        /// <param name="mm">学妹.</param>
        public Proxy(SchoolGirl mm)
        {
            this.gg = new Pursuit(mm); // 构造函数中实例化追求者
        }

        /// <inheritdoc/>
        public void GiveChocolate()
        {
            this.gg.GiveChocolate(); // 在实现方法中去调用追求者类的相关方法，达到代理的目的
        }

        /// <inheritdoc/>
        public void GiveDolls()
        {
            this.gg.GiveDolls();
        }

        /// <inheritdoc/>
        public void GiveFlowers()
        {
            this.gg.GiveFlowers();
        }
    }
}
