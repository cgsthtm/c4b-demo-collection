﻿// <copyright file="AbstractClass.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._10.模板方法模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 抽象模板类.
    /// 模板方法模式，定义一个操作中的算法的骨架，而将一些步骤延迟到子类中。模板方法使得子类可以不改变一个算法的结构即可重定义该算法的某些特定步骤。[DP]
    /// 模板方法模式是通过把不变行为搬移到超类，去除子类中的重复代码来体现它的优势。模板方法模式就是提供了一个很好的代码复用平台。
    /// 当不变的和可变的行为在方法的子类实现中混合在一起的时候，不变的行为就会在子类中重复出现。我们通过模板方法模式把这些行为搬移到单一的地方，这样就帮助子类摆脱重复的不变行为的纠缠.
    /// 因为有时候，我们会遇到由一系列步骤构成的过程需要执行。这个过程从高层次上看是相同的，但有些步骤的实现可能不同。这时候，我们通常就应该要考虑用模板方法模式了.
    /// 模板方法模式是很常用的模式，对继承和多态玩得好的人几乎都会在继承体系中多多少少用到它。比如在.NET或Java类库的设计中，通常都会利用模板方法模式提取类库中的公共行为到抽象类中.
    /// </summary>
    public abstract class AbstractClass
    {
        /// <summary>
        /// 原生操作1.
        /// </summary>
        public abstract void PrimitiveOperation1(); // 一些抽象行为放到子类去实现

        /// <summary>
        /// 原生操作2.
        /// </summary>
        public abstract void PrimitiveOperation2();

        /// <summary>
        /// 模板方法，给出了逻辑骨架，而逻辑的组成是一些相应的抽象操作，他们都推迟到子类实现。顶层逻辑也可能调用一些具体方法.
        /// </summary>
        public void TemplateMethod()
        {
            this.PrimitiveOperation1();
            this.PrimitiveOperation2();
            SerilogHelper.Instance.Logger.Information("模板方法TemplateMethod()执行完毕");
        }
    }
}
