﻿// <copyright file="TestPaperB.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._10.模板方法模式.模板方法举例.提炼代码.再提炼
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 学生乙抄的试卷.
    /// </summary>
    public class TestPaperB : TestPaper
    {
        /// <inheritdoc/>
        protected override string Answer1()
        {
            return "c";
        }

        /// <inheritdoc/>
        protected override string Answer2()
        {
            return "a";
        }

        /// <inheritdoc/>
        protected override string Answer3()
        {
            return "a";
        }
    }
}
