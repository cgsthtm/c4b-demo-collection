﻿// <copyright file="TestPaper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._10.模板方法模式.模板方法举例.提炼代码.再提炼
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;

    /// <summary>
    /// 金庸小说考题试卷.
    /// </summary>
    public class TestPaper
    {
        /// <summary>
        /// 试题1.
        /// </summary>
        public void TestQuestion1()
        {
            Console.WriteLine(" 杨过得到，后来给了郭靖，炼成倚天剑、屠龙刀的玄铁可能是[ ] a.球磨铸铁 b.马口铁 c.高速合金钢 d.碳素纤维 ");
            Console.WriteLine($"答案：{this.Answer1()}");
        }

        /// <summary>
        /// 试题2.
        /// </summary>
        public void TestQuestion2()
        {
            Console.WriteLine(" 杨过、程英、陆无双铲除了情花，造成[ ] a.使这种植物不再害人 b.使一种珍稀物种灭绝 c.破坏了那个生物圈的生态平衡 d.造成该地区沙漠化");
            Console.WriteLine($"答案：{this.Answer2()}");
        }

        /// <summary>
        /// 试题3.
        /// </summary>
        public void TestQuestion3()
        {
            Console.WriteLine(" 蓝凤凰致使华山师徒、桃谷六仙呕吐不止，如果你是大夫，会给他们开什么药[ ] a.阿司匹林 b.牛黄解毒片 c.氟哌酸 d.让他们喝大量的生牛奶 e.以上全不对");
            Console.WriteLine($"答案：{this.Answer3()}");
        }

        /// <summary>
        /// 答案1.
        /// </summary>
        /// <returns>答案.</returns>
        protected virtual string Answer1()
        {
            return string.Empty;
        }

        /// <summary>
        /// 答案2.
        /// </summary>
        /// <returns>答案.</returns>
        protected virtual string Answer2()
        {
            return string.Empty;
        }

        /// <summary>
        /// 答案3.
        /// </summary>
        /// <returns>答案.</returns>
        protected virtual string Answer3()
        {
            return string.Empty;
        }
    }
}
