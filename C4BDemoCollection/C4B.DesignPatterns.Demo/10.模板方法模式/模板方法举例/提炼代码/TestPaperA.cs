﻿// <copyright file="TestPaperA.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._10.模板方法模式.模板方法举例.提炼代码
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;

    /// <summary>
    /// 学生甲抄的试卷.
    /// 这还只是初步的泛化，你仔细看看，两个学生的类里面，还有没有类似的代码。
    /// 啊，感觉相同的东西还是有的，比如都有‘base.试题1()’，还有‘Console.WriteLine("答案:")，我感觉除了选项的abcd，其他都是重复的.
    /// 我们既然用了继承，并且肯定这个继承有意义，就应该要成为子类的模板，所有重复的代码都应该要上升到父类去，而不是让每个子类都去重复.
    /// 当我们要完成在某一细节层次一致的一个过程或一系列步骤，但其个别步骤在更详细的层次上的实现可能不同时，我们通常考虑用模板方法模式来处理.
    /// </summary>
    public class TestPaperA : TestPaper
    {
        /// <summary>
        /// 试题1.
        /// </summary>
        public new void TestQuestion1()
        {
            base.TestQuestion1();
            Console.WriteLine("答案：b"); // 只有这里，不同的学生会有不同的结果，其他都是一样的。于是我们就改这里，增加一个虚方法。
        }

        /// <summary>
        /// 试题2.
        /// </summary>
        public new void TestQuestion2()
        {
            base.TestQuestion2();
            Console.WriteLine("答案：b");
        }

        /// <summary>
        /// 试题3.
        /// </summary>
        public new void TestQuestion3()
        {
            base.TestQuestion3();
            Console.WriteLine("答案：b");
        }
    }
}
