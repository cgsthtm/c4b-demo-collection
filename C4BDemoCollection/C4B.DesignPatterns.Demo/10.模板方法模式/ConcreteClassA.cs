﻿// <copyright file="ConcreteClassA.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._10.模板方法模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 具体类A.实现抽象模板类.
    /// ConcreteClass，实现父类所定义的一个或多个抽象方法。每一个AbstractClass都可以有任意多个ConcreteClass与之对应，
    ///   而每一个ConcreteClass都可以给出这些抽象方法（也就是顶级逻辑的组成步骤）的不同实现，从而使得顶级逻辑的实现各不相同.
    /// </summary>
    public class ConcreteClassA : AbstractClass
    {
        /// <inheritdoc/>
        public override void PrimitiveOperation1()
        {
            SerilogHelper.Instance.Logger.Information("具体类A方法1实现");
        }

        /// <inheritdoc/>
        public override void PrimitiveOperation2()
        {
            SerilogHelper.Instance.Logger.Information("具体类A方法2实现");
        }
    }
}
