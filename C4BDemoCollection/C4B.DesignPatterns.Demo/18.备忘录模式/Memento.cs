﻿// <copyright file="Memento.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._18.备忘录模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 备忘录Memento类.
    /// Memento模式比较适用于功能比较复杂的，但需要维护或记录属性历史的类，或者需要保存的属性只是众多属性中的一小部分时，Originator可以根据保存的Memento信息还原到前一状态。
    /// 如果在某个系统中使用命令模式时，需要实现命令的撤销功能，那么命令模式可以使用备忘录模式来存储可撤销操作的状态[DP]。
    /// 有时一些对象的内部信息必须保存在对象以外的地方，但是必须要由对象自己读取，这时，使用备忘录可以把复杂的对象内部信息对其他的对象屏蔽起来[DP]，从而可以恰当地保持封装的边界.
    /// 我们需要保存的并不是全部信息，而只是部分，那么就应该有一个独立的备忘录类Memento，它只拥有需要保存的信息的属性.
    /// </summary>
    public class Memento
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Memento"/> class.
        /// </summary>
        /// <param name="state">状态数据.</param>
        public Memento(string state) // 构造方法，将相关数据导入.
        {
            this.State = state;
        }

        /// <summary>
        /// Gets or sets 需要保存的数据属性，可以是多个.
        /// </summary>
        public string State { get; set; }
    }
}