﻿// <copyright file="Caretaker.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._18.备忘录模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 管理者Caretaker类.
    /// </summary>
    public class Caretaker
    {
        /// <summary>
        /// Gets or sets 备忘录.
        /// </summary>
        public Memento Memento { get; set; }
    }
}
