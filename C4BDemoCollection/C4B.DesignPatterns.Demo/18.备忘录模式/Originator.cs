﻿// <copyright file="Originator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._18.备忘录模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 发起人Originator类.
    /// 备忘录（Memento）：在不破坏封装性的前提下，捕获一个对象的内部状态，并在该对象之外保存这个状态。这样以后就可将该对象恢复到原先保存的状态。[DP]。
    /// 你要注意，备忘录模式也是有缺点的，角色状态需要完整存储到备忘录对象中，如果状态数据很大很多，那么在资源消耗上，备忘录对象会非常耗内存.
    /// </summary>
    public class Originator
    {
        /// <summary>
        /// Gets or sets 需要保存的属性，可能由多个.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// 创建备忘录，将当前需要保存的信息导入并实例化出一个Memento对象.
        /// </summary>
        /// <returns>备忘录.</returns>
        public Memento CreateMemento()
        {
            return new Memento(this.State);
        }

        /// <summary>
        /// 恢复备忘录，将Memento导入并将相关数据恢复.
        /// </summary>
        /// <param name="memento">备忘录.</param>
        public void SetMemento(Memento memento)
        {
            this.State = memento.State;
        }

        /// <summary>
        /// 显示数据.
        /// </summary>
        public void Show()
        {
            SerilogHelper.Instance.Logger.Information("State={0}", this.State);
        }
    }
}