﻿// <copyright file="Waiter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._23.命令模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 服务员.
    /// </summary>
    public class Waiter
    {
        private IList<Command> orders = new List<Command>(); // 存放具体命令的容器

        /// <summary>
        /// 设置订单.
        /// </summary>
        /// <param name="command">命令.</param>
        public void SetOrder(Command command)
        {
            if (command.ToString() == "C4B.DesignPatterns.Demo._23.命令模式.BakeChickenWing")
            {
                SerilogHelper.Instance.Logger.Information("服务员：没有烤鸡翅了，请点别的烧烤。"); // 在客户提出请求时，对没货的烧烤进行回绝
            }
            else
            {
                this.orders.Add(command);
                SerilogHelper.Instance.Logger.Information("增加订单：" + command.ToString() + "  " + "时间：" + DateTime.Now.ToString()); // 记录客户订单日志，以备算账收钱
            }
        }

        /// <summary>
        /// 取消订单.
        /// </summary>
        /// <param name="command">命令.</param>
        public void CancelOrder(Command command)
        {
            this.orders.Remove(command);
            SerilogHelper.Instance.Logger.Information("取消订单：" + command.ToString() + "  " + "时间：" + DateTime.Now.ToString());
        }

        /// <summary>
        /// 通知全部执行.
        /// </summary>
        public void Notify()
        {
            foreach (var cmd in this.orders)
            {
                cmd.ExcuteCommand(); // 根据用户点好的烧烤订单，通知厨房制作
            }
        }
    }
}