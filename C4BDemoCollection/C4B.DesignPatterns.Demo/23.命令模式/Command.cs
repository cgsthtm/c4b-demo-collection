﻿// <copyright file="Command.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._23.命令模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 抽象命令.
    /// </summary>
    public abstract class Command
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Command"/> class.
        /// </summary>
        /// <param name="receiver">接收者.</param>
        public Command(Barbecuer receiver)
        {
            this.Receiver = receiver;
        }

        /// <summary>
        /// Gets or sets 接收者.
        /// </summary>
        protected Barbecuer Receiver { get; set; }

        /// <summary>
        /// 执行命令.
        /// </summary>
        public abstract void ExcuteCommand();
    }
}