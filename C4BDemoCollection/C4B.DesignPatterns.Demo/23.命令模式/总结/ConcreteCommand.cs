﻿// <copyright file="ConcreteCommand.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._23.命令模式.总结
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// ConcreteCommand类，将一个接收者对象绑定于一个动作，调用接收者相应的操作，以实现Execute.
    /// </summary>
    public class ConcreteCommand : Command
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConcreteCommand"/> class.
        /// </summary>
        /// <param name="receiver">接收者.</param>
        public ConcreteCommand(Receiver receiver)
            : base(receiver)
        {
        }

        /// <summary>
        /// 执行.
        /// </summary>
        public override void Execute()
        {
            this.Receiver.Action();
        }
    }
}