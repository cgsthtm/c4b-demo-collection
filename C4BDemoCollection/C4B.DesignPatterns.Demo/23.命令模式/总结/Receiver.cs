﻿// <copyright file="Receiver.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._23.命令模式.总结
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;

    /// <summary>
    /// Receiver类，知道如何实施与执行一个与请求相关的操作，任何类都可能作为一个接收者.
    /// </summary>
    public class Receiver
    {
        /// <summary>
        /// 动作.
        /// </summary>
        public void Action()
        {
            Console.WriteLine("执行请求！");
        }
    }
}