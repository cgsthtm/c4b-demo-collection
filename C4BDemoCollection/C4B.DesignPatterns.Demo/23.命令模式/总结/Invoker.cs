﻿// <copyright file="Invoker.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._23.命令模式.总结
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Invoker类，要求该命令执行这个请求.
    /// </summary>
    public class Invoker
    {
        private Command command;

        /// <summary>
        /// 设置命令.
        /// </summary>
        /// <param name="command">命令.</param>
        public void SetCommand(Command command)
        {
            this.command = command;
        }

        /// <summary>
        /// 执行命令.
        /// </summary>
        public void ExecuteCommand()
        {
            this.command.Execute();
        }
    }
}