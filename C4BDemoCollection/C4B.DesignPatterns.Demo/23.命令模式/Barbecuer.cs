﻿// <copyright file="Barbecuer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._23.命令模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 烤串者.
    /// </summary>
    public class Barbecuer
    {
        /// <summary>
        /// 烤羊肉串.
        /// </summary>
        public void BakeMutton()
        {
            SerilogHelper.Instance.Logger.Information("烤羊肉串！");
        }

        /// <summary>
        /// 烤鸡翅.
        /// </summary>
        public void BakeChickenWing()
        {
            SerilogHelper.Instance.Logger.Information("烤鸡翅！");
        }
    }
}