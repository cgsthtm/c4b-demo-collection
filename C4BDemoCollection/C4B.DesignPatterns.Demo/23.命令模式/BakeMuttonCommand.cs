﻿// <copyright file="BakeMuttonCommand.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._23.命令模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 烤羊肉串命令.
    /// </summary>
    public class BakeMuttonCommand : Command
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BakeMuttonCommand"/> class.
        /// </summary>
        /// <param name="receiver">接收者.</param>
        public BakeMuttonCommand(Barbecuer receiver)
            : base(receiver)
        {
        }

        /// <summary>
        /// 具体命令类执行命令时，执行具体的行为.
        /// </summary>
        public override void ExcuteCommand()
        {
            this.Receiver.BakeMutton();
        }
    }
}