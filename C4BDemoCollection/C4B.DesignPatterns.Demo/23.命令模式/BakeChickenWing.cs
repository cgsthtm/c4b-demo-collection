﻿// <copyright file="BakeChickenWing.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._23.命令模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 烤鸡翅命令.
    /// </summary>
    public class BakeChickenWing : Command
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BakeChickenWing"/> class.
        /// </summary>
        /// <param name="receiver">接收者.</param>
        public BakeChickenWing(Barbecuer receiver)
            : base(receiver)
        {
        }

        /// <summary>
        /// 具体命令类执行命令时，执行具体的行为.
        /// </summary>
        public override void ExcuteCommand()
        {
            this.Receiver.BakeChickenWing();
        }
    }
}