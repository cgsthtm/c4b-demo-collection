﻿// <copyright file="SubSystemOne.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._12.外观模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 子系统1.
    /// </summary>
    public class SubSystemOne
    {
        /// <summary>
        /// 方法1.
        /// </summary>
        public void MethodOne()
        {
            SerilogHelper.Instance.Logger.Information("子系统方法一");
        }
    }
}
