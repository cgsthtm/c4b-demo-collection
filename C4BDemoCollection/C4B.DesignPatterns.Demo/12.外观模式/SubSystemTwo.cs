﻿// <copyright file="SubSystemTwo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._12.外观模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 子系统2.
    /// </summary>
    public class SubSystemTwo
    {
        /// <summary>
        /// 方法2.
        /// </summary>
        public void MethodTwo()
        {
            SerilogHelper.Instance.Logger.Information("子系统方法二");
        }
    }
}
