﻿// <copyright file="Facade.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._12.外观模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 外观模式（Facade），为子系统中的一组接口提供一个一致的界面，此模式定义了一个高层接口，这个接口使得这一子系统更加容易使用。[DP]
    /// “那外观模式在什么时候使用最好呢？”小菜问道。“这要分三个阶段来说:
    /// 1.首先，在设计初期阶段，应该要有意识的将不同的两个层分离，比如经典的三层架构，就需要考虑在数据访问层和业务逻辑层、业务逻辑层和表示层的层与层之间建立外观Facade，
    ///   这样可以为复杂的子系统提供一个简单的接口，使得耦合大大降低。
    /// 2.其次，在开发阶段，子系统往往因为不断的重构演化而变得越来越复杂，大多数的模式使用时也都会产生很多很小的类，这本是好事，
    ///   但也给外部调用它们的用户程序带来了使用上的困难，增加外观Facade可以提供一个简单的接口，减少它们之间的依赖。
    /// 3.第三，在维护一个遗留的大型系统时，可能这个系统已经非常难以维护和扩展了，但因为它包含非常重要的功能，新的需求开发必须要依赖于它。此时用外观模式Facade也是非常合适的。
    /// 你可以为新系统开发一个外观Facade类，来提供设计粗糙或高度复杂的遗留代码的比较清晰简单的接口，让新系统与Facade对象交互，Facade与遗留代码交互所有复杂的工作。[R2P]”
    /// 对于复杂难以维护的老系统，直接去改或去扩展都可能产生很多问题，分两个小组，一个开发Facade与老系统的交互，另一个只要了解Facade的接口，直接开发新系统调用这些接口即可，
    ///   确实可以减少很多不必要的麻烦.
    /// </summary>
    public class Facade
    {
        // 外观类，他需要了解所有的子系统的方法或属性，进行组合，以备外界调用
        private SubSystemOne one;
        private SubSystemTwo two;
        private SubSystemThree three;

        /// <summary>
        /// Initializes a new instance of the <see cref="Facade"/> class.
        /// </summary>
        public Facade()
        {
            this.one = new SubSystemOne();
            this.two = new SubSystemTwo();
            this.three = new SubSystemThree();
        }

        /// <summary>
        /// 方法A.
        /// </summary>
        public void MethodA()
        {
            SerilogHelper.Instance.Logger.Information("方法组A() ----- ");
            this.one.MethodOne();
            this.two.MethodTwo();
        }

        /// <summary>
        /// 方法B.
        /// </summary>
        public void MethodB()
        {
            SerilogHelper.Instance.Logger.Information("方法组B() ----- ");
            this.two.MethodTwo();
            this.three.MethodThree();
        }
    }
}
