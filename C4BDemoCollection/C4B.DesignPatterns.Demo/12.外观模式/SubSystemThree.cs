﻿// <copyright file="SubSystemThree.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._12.外观模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 子系统3.
    /// </summary>
    public class SubSystemThree
    {
        /// <summary>
        /// 方法3.
        /// </summary>
        public void MethodThree()
        {
            SerilogHelper.Instance.Logger.Information("子系统方法三");
        }
    }
}
