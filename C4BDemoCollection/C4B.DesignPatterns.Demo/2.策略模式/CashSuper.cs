﻿// <copyright file="CashSuper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._2.策略模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 现金收费抽象类.
    /// </summary>
    public abstract class CashSuper
    {
        /// <summary>
        /// 现金收取超类的抽象方法，收取现金，参数为原价，返回为当前价.
        /// </summary>
        /// <param name="money">原价.</param>
        /// <returns>当前价.</returns>
        public abstract double AcceptCash(double money);
    }
}
