﻿// <copyright file="CashRebate.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._2.策略模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 打折收费子类.
    /// </summary>
    public class CashRebate : CashSuper
    {
        private double moneyRebate = 1d;

        /// <summary>
        /// Initializes a new instance of the <see cref="CashRebate"/> class.
        /// </summary>
        /// <param name="moneyRebate">折扣率.如八折，就是0.8</param>
        public CashRebate(double moneyRebate)
        {
            this.moneyRebate = moneyRebate;
        }

        /// <summary>
        /// 打折收费，初始化时，必须要输入折扣率，如八折，就是0.8.
        /// </summary>
        /// <param name="money">原价.</param>
        /// <returns>当前价.</returns>
        public override double AcceptCash(double money)
        {
            return money * this.moneyRebate;
        }
    }
}
