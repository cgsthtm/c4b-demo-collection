﻿// <copyright file="CashContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._2.策略模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 收费对象生成工厂.策略与简单工厂结合.
    /// 简单工厂模式我需要让客户端认识两个类， CashSuper和CashFactory，而策略模式与简单工厂结合的用法，客户端就只需要认识一个类CashContext就可以了。耦合更加降低.
    /// </summary>
    public class CashContext
    {
        private CashSuper cs = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="CashContext"/> class.
        /// </summary>
        /// <param name="type">收费策略.</param>
        public CashContext(string type) // 通过构造方法，传入具体收费策略
        {
            switch (type)
            {
                // 将实例化具体策略的过程由客户端转移到Context中，简单工厂的应用
                case "正常收费":
                    this.cs = new CashNormal();
                    break;
                case "满300减100":
                    this.cs = new CashReturn(300d, 100d);
                    break;
                case "打8折":
                    this.cs = new CashRebate(0.8d);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 获取最终收费金额.
        /// </summary>
        /// <param name="money">原价.</param>
        /// <returns>当前价.</returns>
        public double GetResult(double money)
        {
            return this.cs.AcceptCash(money); // 根据收费策略的不同，获得计算结果
        }
    }
}
