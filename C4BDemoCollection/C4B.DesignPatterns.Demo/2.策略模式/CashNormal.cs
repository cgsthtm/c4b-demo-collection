﻿// <copyright file="CashNormal.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._2.策略模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 正常收费子类.
    /// </summary>
    public class CashNormal : CashSuper
    {
        /// <summary>
        /// 正常收费，原价返回.
        /// </summary>
        /// <param name="money">原价.</param>
        /// <returns>当前价.</returns>
        public override double AcceptCash(double money)
        {
            return money;
        }
    }
}
