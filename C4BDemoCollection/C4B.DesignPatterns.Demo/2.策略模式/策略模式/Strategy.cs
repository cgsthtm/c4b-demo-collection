﻿// <copyright file="Strategy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._2.策略模式.策略模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 抽象算法类.
    /// Strategy类，定义所有支持的算法的公共接口.
    /// </summary>
    public abstract class Strategy
    {
        /// <summary>
        /// 算法方法.
        /// </summary>
        public abstract void AlgorithmInterface();
    }
}
