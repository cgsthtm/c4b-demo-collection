﻿// <copyright file="ConcreteStrategyB.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._2.策略模式.策略模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;

    /// <summary>
    /// 具体算法B.
    /// </summary>
    public class ConcreteStrategyB : Strategy
    {
        /// <inheritdoc/>
        public override void AlgorithmInterface()
        {
            Console.WriteLine("算法B实现");
        }
    }
}
