﻿// <copyright file="CashReturn.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._2.策略模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;

    /// <summary>
    /// 返利收费子类.
    /// </summary>
    public class CashReturn : CashSuper
    {
        private double moneyCondition = 0d; // 满多少返多少的返利条件
        private double moneyReturn = 0d; // 返利

        /// <summary>
        /// Initializes a new instance of the <see cref="CashReturn"/> class.
        /// </summary>
        /// <param name="moneyCondition">满多少返利的条件.</param>
        /// <param name="moneyReturn">返利多少.</param>
        public CashReturn(double moneyCondition, double moneyReturn)
        {
            this.moneyCondition = moneyCondition;
            this.moneyReturn = moneyReturn;
        }

        /// <summary>
        /// 返利收费，若大于返利条件，则需要减去返利值.
        /// </summary>
        /// <param name="money">原价.</param>
        /// <returns>当前价.</returns>
        public override double AcceptCash(double money)
        {
            double result = money;
            if (money > this.moneyCondition)
            {
                result = money - (Math.Floor(money / this.moneyCondition) * this.moneyReturn);
            }

            return result;
        }
    }
}
