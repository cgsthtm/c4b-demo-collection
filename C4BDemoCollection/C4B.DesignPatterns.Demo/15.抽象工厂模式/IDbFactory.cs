﻿// <copyright file="IDbFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 抽象工厂模式（Abstract Factory），提供一个创建一系列相关或相互依赖对象的接口，而无需指定它们具体的类。[DP].
    /// <para>
    /// IFactory是一个抽象工厂接口，它里面应该包含所有的产品创建的抽象方法。而ConcreteFactory1和ConcreteFactory2就是具体的工厂了。
    /// 就像SqlserverFactory和AccessFactory一样.
    /// </para>
    /// <para>
    /// 通常是在运行时刻再创建一个ConcreteFactory类的实例，这个具体的工厂再创建具有特定实现的产品对象，也就是说，为创建不同的产品对象，客户端应使用不同的具体工厂.
    /// </para>
    /// <para>
    /// 在一个应用中只需要在初始化的时候出现一次，这就使得改变一个应用的具体工厂变得非常容易，它只需要改变具体工厂即可使用不同的产品配置.
    /// </para>
    /// </summary>
    public interface IDbFactory
    {
        /// <summary>
        /// 创建用户.
        /// </summary>
        /// <returns>创建用户接口.</returns>
        IUser CreateUser();
    }
}