﻿// <copyright file="SqlServerFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// SqlServerFactory类，实现IFactory接口，实例化SqlserverUser.
    /// </summary>
    public class SqlServerFactory : IDbFactory
    {
        /// <inheritdoc/>
        public IUser CreateUser()
        {
            return new SqlServerUser();
        }
    }
}
