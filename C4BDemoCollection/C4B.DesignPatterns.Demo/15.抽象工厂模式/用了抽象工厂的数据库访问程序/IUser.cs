﻿// <copyright file="IUser.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.用了抽象工厂的数据库访问程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 访问用户接口.
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// 新增.
        /// </summary>
        /// <param name="user">用户.</param>
        void Insert(User user);

        /// <summary>
        /// 获取.
        /// </summary>
        /// <param name="id">用户ID.</param>
        /// <returns>用户.</returns>
        User GetUser(int id);
    }
}
