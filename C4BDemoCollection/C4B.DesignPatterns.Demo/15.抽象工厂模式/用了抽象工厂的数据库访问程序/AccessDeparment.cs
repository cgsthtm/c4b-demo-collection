﻿// <copyright file="AccessDeparment.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.用了抽象工厂的数据库访问程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Access操作Department表.
    /// </summary>
    public class AccessDeparment : IDepartment
    {
        /// <inheritdoc/>
        public Department GetDepartment(int id)
        {
            SerilogHelper.Instance.Logger.Information("在Access中根据ID得到Department表一条记录.");
            return null;
        }

        /// <inheritdoc/>
        public void Insert(Department department)
        {
            SerilogHelper.Instance.Logger.Information("在Access中给Department表增加一条记录.");
        }
    }
}
