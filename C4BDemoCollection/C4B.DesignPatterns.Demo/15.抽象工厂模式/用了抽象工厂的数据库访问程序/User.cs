﻿// <copyright file="User.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.用了抽象工厂的数据库访问程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 用户实体类.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets or sets 用户ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets 用户姓名.
        /// </summary>
        public int Name { get; set; }
    }
}