﻿// <copyright file="IFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.用了抽象工厂的数据库访问程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 抽象工厂接口.
    /// </summary>
    public interface IFactory
    {
        /// <summary>
        /// 创建访问用户.
        /// </summary>
        /// <returns>访问用户.</returns>
        IUser CreateUser();

        /// <summary>
        /// 创建访问部门.
        /// </summary>
        /// <returns>访问部门.</returns>
        IDepartment CreateDepartment(); // 增加的接口方法
    }
}
