﻿// <copyright file="IDepartment.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.用了抽象工厂的数据库访问程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 访问部门接口.
    /// </summary>
    public interface IDepartment
    {
        /// <summary>
        /// 新增.
        /// </summary>
        /// <param name="department">部门.</param>
        void Insert(Department department);

        /// <summary>
        /// 获取.
        /// </summary>
        /// <param name="id">部门ID.</param>
        /// <returns>部门.</returns>
        Department GetDepartment(int id);
    }
}
