﻿// <copyright file="SqlserverFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.用了抽象工厂的数据库访问程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Sqlserver工厂.
    /// </summary>
    internal class SqlserverFactory : IFactory
    {
        /// <inheritdoc/>
        public IDepartment CreateDepartment()
        {
            return new SqlserverDepartment();
        }

        /// <inheritdoc/>
        public IUser CreateUser()
        {
            return new SqlserverUser();
        }
    }
}
