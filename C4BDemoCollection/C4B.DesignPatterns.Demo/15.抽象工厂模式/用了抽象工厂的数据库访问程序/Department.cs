﻿// <copyright file="Department.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.用了抽象工厂的数据库访问程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 部门实体类.
    /// </summary>
    public class Department
    {
        /// <summary>
        /// Gets or sets 部门ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets 部门名称.
        /// </summary>
        public int Name { get; set; }
    }
}
