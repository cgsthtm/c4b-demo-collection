﻿// <copyright file="AccessUser.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Access用户访问类.
    /// </summary>
    public class AccessUser : IUser
    {
        /// <inheritdoc/>
        public User GetUser(int id)
        {
            SerilogHelper.Instance.Logger.Information("在Access中根据ID得到User表的一条记录");
            return null;
        }

        /// <inheritdoc/>
        public void Insert(User user)
        {
            SerilogHelper.Instance.Logger.Information("在Access中给User表增加一条记录");
        }
    }
}
