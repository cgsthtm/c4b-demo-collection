﻿// <copyright file="SqlServerUser.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// SqlserverUser类，用于访问SQL Server的User.
    /// </summary>
    public class SqlServerUser : IUser
    {
        /// <inheritdoc/>
        public User GetUser(int id)
        {
            SerilogHelper.Instance.Logger.Information("在SQL Server中根据ID得到User表的一条记录");
            return null;
        }

        /// <inheritdoc/>
        public void Insert(User user)
        {
            SerilogHelper.Instance.Logger.Information("在SQL Server中给User表增加一条记录");
        }
    }
}
