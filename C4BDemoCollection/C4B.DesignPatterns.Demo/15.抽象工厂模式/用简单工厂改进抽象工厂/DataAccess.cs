﻿// <copyright file="DataAccess.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.用简单工厂改进抽象工厂
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 去除IFactory、SqlserverFactory和AccessFactory三个工厂类，取而代之的是DataAccess类，用一个简单工厂模式来实现.
    /// </summary>
    public class DataAccess
    {
        private static readonly string DB = "Access"; // 数据库名称，可替换为Sqlserver

        /// <summary>
        /// 由于db的事先设置，所以此处可以根据选择实例化出相应的对象.
        /// </summary>
        /// <returns>IUser.</returns>
        public static IUser CreateUser()
        {
            IUser user = null;
            switch (DB)
            {
                case "Sqlserver":
                    user = new SqlServerUser();
                    break;
                case "Access":
                    user = new AccessUser();
                    break;
            }

            return user;
        }
    }
}
