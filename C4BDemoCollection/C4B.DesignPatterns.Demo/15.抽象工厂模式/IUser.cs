﻿// <copyright file="IUser.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// IUser接口，用于客户端访问，解除与具体数据库访问的耦合.
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// 插入.
        /// </summary>
        /// <param name="user">User实体.</param>
        void Insert(User user);

        /// <summary>
        /// 获取.
        /// </summary>
        /// <param name="id">用户id.</param>
        /// <returns>返回User对象.</returns>
        User GetUser(int id);
    }
}