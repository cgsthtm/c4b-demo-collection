﻿// <copyright file="SqlserverUser.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.最基本的数据访问程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Sqlserver操作User.
    /// </summary>
    public class SqlserverUser
    {
        /// <summary>
        /// 新增.
        /// </summary>
        /// <param name="user">用户.</param>
        public void Insert(User user)
        {
            SerilogHelper.Instance.Logger.Information("在SQL Server中给User表增加一条记录.");
        }

        /// <summary>
        /// 获取.
        /// </summary>
        /// <param name="id">用户ID.</param>
        public void GetUser(int id)
        {
            SerilogHelper.Instance.Logger.Information("在SQL Server中根据ID得到User表一条记录.");
        }
    }
}
