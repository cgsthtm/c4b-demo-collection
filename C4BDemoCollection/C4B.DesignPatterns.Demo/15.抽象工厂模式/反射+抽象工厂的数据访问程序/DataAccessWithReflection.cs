﻿// <copyright file="DataAccessWithReflection.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.反射_抽象工厂的数据访问程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System.Configuration; // 引入配置文件
    using System.Reflection; // 引入反射

    /// <summary>
    /// 依赖注入（Dependency Injection），从字面上不太好理解，我们也不去管它。关键在于如何去用这种方法来解决我们的switch问题。
    /// 本来依赖注入是需要专门的IoC容器提供，比如Spring.NET，显然当前这个程序不需要这么麻烦，你只需要再了解一个简单的.NET技术‘反射’就可以了。
    /// 所有在用简单工厂的地方，都可以考虑用反射技术来去除switch或if，解除分支判断带来的耦合.
    /// </summary>
    public class DataAccessWithReflection
    {
        private static readonly string AssemblyName = "C4B.DesignPatterns.Demo"; // 程序集名称
        private static readonly string DB = ConfigurationManager.AppSettings["DB"]; // 读取配置文件DB节点的值，用于动态创建数据库访问实例

        /// <summary>
        /// 由于db的事先设置，所以此处可以根据选择实例化出相应的对象.
        /// </summary>
        /// <returns>IUser</returns>
        public static IUser CreateUser()
        {
            // 现在如果我们增加了Oracle数据访问，相关的类的增加是不可避免的，这点无论我们用任何办法都解决不了，不过这叫扩展。
            string className = AssemblyName + "._15.抽象工厂模式." + DB + "User";
            SerilogHelper.Instance.Logger.Information("根据反射动态加载的应用程序集和类为：{0}", className);
            return (IUser)Assembly.Load(AssemblyName).CreateInstance(className);
        }
    }
}