﻿// <copyright file="IFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.用工厂方法的数据库访问程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 工厂接口.
    /// </summary>
    public interface IFactory
    {
        /// <summary>
        /// 创建具体工厂.
        /// </summary>
        /// <returns>返回IUSer.</returns>
        IUser CreateFactory();
    }
}
