﻿// <copyright file="IUser.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.用工厂方法的数据库访问程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 访问用户表接口.
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// 获取.
        /// </summary>
        /// <param name="id">用户ID.</param>
        void GetUser(int id);

        /// <summary>
        /// 新增.
        /// </summary>
        /// <param name="user">用户.</param>
        void Insert(User user);
    }
}
