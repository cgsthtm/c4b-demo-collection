﻿// <copyright file="SqlserverUser.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._15.抽象工厂模式.用工厂方法的数据库访问程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Sqlserver操作用户表.
    /// </summary>
    public class SqlserverUser : IUser
    {
        /// <inheritdoc/>
        public void GetUser(int id)
        {
            SerilogHelper.Instance.Logger.Information("在SQL Server中根据ID得到User表一条记录.");
        }

        /// <inheritdoc/>
        public void Insert(User user)
        {
            SerilogHelper.Instance.Logger.Information("在SQL Server中给User表增加一条记录.");
        }
    }
}
