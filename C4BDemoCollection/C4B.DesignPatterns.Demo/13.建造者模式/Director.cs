﻿// <copyright file="Director.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._13.建造者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 指挥者类.
    /// </summary>
    public class Director
    {
        /// <summary>
        /// 指挥建造过程.
        /// </summary>
        /// <param name="builder">抽象Builder.</param>
        public void Construct(Builder builder)
        {
            builder.BuildPartA();
            builder.BuildPartB();
        }
    }
}
