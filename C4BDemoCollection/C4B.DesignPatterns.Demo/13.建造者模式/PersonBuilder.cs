﻿// <copyright file="PersonBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._13.建造者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System.Drawing;

    /// <summary>
    /// 如果你需要将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示的意图时，我们需要应用于一个设计模式，‘建造者（Builder）模式’，又叫生成器模式。
    /// 如果我们用了建造者模式，那么用户就只需指定需要建造的类型就可以得到它们，而具体建造的过程和细节就不需知道了。
    /// 建造者模式（Builder），将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示。[DP]
    /// 建造者模式的好处就是使得建造代码与表示代码分离，由于建造者隐藏了该产品是如何组装的，所以若需要改变一个产品的内部表示，只需要再定义一个具体的建造者就可以了.
    /// 建造者模式是在当创建复杂对象的算法应该独立于该对象的组成部分以及它们的装配方式时适用的模式.
    /// </summary>
    public abstract class PersonBuilder
    {
        /* 定义一个抽象的建造人的类，来把这个过程给稳定住，不让任何人遗忘当中的任何一步。
         * 例如下面的抽象的构造头部、身体、胳膊、腿等函数。
         */

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonBuilder"/> class.
        /// </summary>
        /// <param name="g">图形graphics.</param>
        /// <param name="p">笔pen.</param>
        public PersonBuilder(Graphics g, Pen p)
        {
            this.G = g;
            this.P = p;
        }

        /// <summary>
        /// Gets or sets 图形.
        /// </summary>
        protected Graphics G { get; set; }

        /// <summary>
        /// Gets or sets 画笔.
        /// </summary>
        protected Pen P { get; set; }

        /// <summary>
        /// 构造头.
        /// </summary>
        public abstract void BuildHead();

        /// <summary>
        /// 构造身体.
        /// </summary>
        public abstract void BuildBody();

        /// <summary>
        /// 构造左胳膊.
        /// </summary>
        public abstract void BuildArmLeft();

        /// <summary>
        /// 构造右胳膊.
        /// </summary>
        public abstract void BuildArmRight();

        /// <summary>
        /// 构造左腿.
        /// </summary>
        public abstract void BuildLegLeft();

        /// <summary>
        /// 构造右腿.
        /// </summary>
        public abstract void BuildLegRight();
    }
}