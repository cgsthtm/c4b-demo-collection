﻿// <copyright file="ConcreteBuilder2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._13.建造者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 具体建造者2.
    /// </summary>
    public class ConcreteBuilder2 : Builder
    {
        private Product product = new Product();

        /// <inheritdoc/>
        public override void BuildPartA()
        {
            this.product.Add("部件X"); // 建造具体的两个部件，部件X和部件Y
        }

        /// <inheritdoc/>
        public override void BuildPartB()
        {
            this.product.Add("部件Y");
        }

        /// <inheritdoc/>
        public override Product GetResult()
        {
            return this.product;
        }
    }
}
