﻿// <copyright file="Builder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._13.建造者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 抽象建造者类.
    /// </summary>
    public abstract class Builder
    {
        /// <summary>
        /// 建造A部分.
        /// </summary>
        public abstract void BuildPartA();

        /// <summary>
        /// 建造B部分.
        /// </summary>
        public abstract void BuildPartB();

        /// <summary>
        /// 获取结果.
        /// </summary>
        /// <returns>产品.</returns>
        public abstract Product GetResult();
    }
}
