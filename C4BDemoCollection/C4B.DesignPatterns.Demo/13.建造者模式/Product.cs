﻿// <copyright file="Product.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._13.建造者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System.Collections.Generic;

    /// <summary>
    /// 产品类，由多个部件组成.
    /// </summary>
    public class Product
    {
        private IList<string> parts = new List<string>();

        /// <summary>
        /// 添加产品部件.
        /// </summary>
        /// <param name="part">部件.</param>
        public void Add(string part)
        {
            this.parts.Add(part);
        }

        /// <summary>
        /// 列举所有产品部件.
        /// </summary>
        public void Show()
        {
            SerilogHelper.Instance.Logger.Information("产品 创建-----");
            foreach (var part in this.parts)
            {
                SerilogHelper.Instance.Logger.Information("{part}", part);
            }
        }
    }
}