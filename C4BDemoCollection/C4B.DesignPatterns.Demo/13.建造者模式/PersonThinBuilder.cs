﻿// <copyright file="PersonThinBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._13.建造者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System.Drawing;

    /// <summary>
    /// 瘦子类继承抽象类.
    /// </summary>
    public class PersonThinBuilder : PersonBuilder
    {
        /* 我们需要建造一个瘦的小人，则让这个瘦子类去继承这个抽象类，那就必须去重写这些抽象方法了。
         */

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonThinBuilder"/> class.
        /// </summary>
        /// <param name="g">图形.</param>
        /// <param name="p">笔.</param>
        public PersonThinBuilder(Graphics g, Pen p)
            : base(g, p)
        {
        }

        /// <inheritdoc/>
        public override void BuildArmLeft()
        {
            this.G.DrawLine(this.P, 60, 50, 40, 100);
        }

        /// <inheritdoc/>
        public override void BuildArmRight()
        {
            this.G.DrawLine(this.P, 70, 50, 90, 100);
        }

        /// <inheritdoc/>
        public override void BuildBody()
        {
            this.G.DrawRectangle(this.P, 60, 50, 10, 50);
        }

        /// <inheritdoc/>
        public override void BuildHead()
        {
            this.G.DrawEllipse(this.P, 50, 20, 30, 30);
        }

        /// <inheritdoc/>
        public override void BuildLegLeft()
        {
            this.G.DrawLine(this.P, 60, 100, 45, 150);
        }

        /// <inheritdoc/>
        public override void BuildLegRight()
        {
            this.G.DrawLine(this.P, 70, 100, 85, 150);
        }
    }
}