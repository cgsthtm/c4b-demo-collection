﻿// <copyright file="PersonDirector.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._13.建造者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 建造者模式中一个很重要的类，指挥者（Director），用它来控制建造过程，也用它来隔离用户与建造过程的关联。
    /// 建造者模式是逐步建造产品的，所以建造者的Builder类里的那些建造方法必须要足够普遍，以便为各种类型的具体建造者构造。
    /// 那都是什么时候需要使用建造者模式呢？它主要是用于创建一些复杂的对象，这些对象内部构建间的建造顺序通常是稳定的，但对象内部的构建通常面临着复杂的变化.
    /// </summary>
    public class PersonDirector
    {
        private PersonBuilder personBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonDirector"/> class.
        /// </summary>
        /// <param name="personBuilder">用户告诉指挥者，需要什么样的小人.</param>
        public PersonDirector(PersonBuilder personBuilder) // 用户告诉指挥者，需要什么样的小人
        {
            this.personBuilder = personBuilder;
        }

        /// <summary>
        /// 根据用户的选择建造小人.
        /// </summary>
        public void CreatePerson()
        {
            this.personBuilder.BuildHead();
            this.personBuilder.BuildBody();
            this.personBuilder.BuildArmLeft();
            this.personBuilder.BuildArmRight();
            this.personBuilder.BuildLegLeft();
            this.personBuilder.BuildLegRight();
        }
    }
}