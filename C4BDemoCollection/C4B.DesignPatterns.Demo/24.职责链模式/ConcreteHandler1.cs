﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.DesignPatterns.Demo._24.职责链模式
{
    public class ConcreteHandler1 : Handler
    {
        public override void HandleRequest(int request)
        {
            if (request >= 0 && request < 10) // 0~10处理此请求
            {
                SerilogHelper.Instance.Logger.Information("{0} 处理请求 {1}", this.GetType().Name, request);
            }
            else if (successor != null)
            {
                successor.HandleRequest(request); // 当无法处理请求时，转移到继任者处理该请求
            }
        }
    }
}
