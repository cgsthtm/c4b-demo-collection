﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.DesignPatterns.Demo._24.职责链模式
{
    /// <summary>
    /// 职责链模式（Chain of Responsibility）：使多个对象都有机会处理请求，从而避免请求的发送者和接收者之间的耦合关系。将这个对象连成一条链，并沿着这条链传递该请求，直到有一个对象处理它为止。[DP]
    /// 职责链的好处：当客户提交一个请求时，请求是沿链传递直至有一个ConcreteHandler对象负责处理它。[DP]
    /// 由于是在客户端来定义链的结构，也就是说，我可以随时地增加或修改处理一个请求的结构。增强了给对象指派职责的灵活性[DP]
    /// 这的确是很灵活，不过也要当心，一个请求极有可能到了链的末端都得不到处理，或者因为没有正确配置而得不到处理，这就很糟糕了。需要事先考虑全面。
    /// 你需要在每个具体管理者处理请求时，做出判断，是可以处理这个请求，还是必须要‘推卸责任’，转移给后继者去处理。
    /// 责任链模式很好地解决了原来大量的分支判断造成难维护、灵活性差的问题。
    /// Handler类，定义一个处理请示的接口
    /// </summary>
    public abstract class Handler
    {
        protected Handler successor;

        /// <summary>
        /// 设置继任者
        /// </summary>
        /// <param name="successor"></param>
        public void SetSuccessor(Handler successor)
        {
            this.successor = successor;
        }

        /// <summary>
        /// 处理请求的抽象方法
        /// </summary>
        /// <param name="request"></param>
        public abstract void HandleRequest(int request);
    }
}
