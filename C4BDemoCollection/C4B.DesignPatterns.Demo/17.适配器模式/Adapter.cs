﻿// <copyright file="Adapter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._17.适配器模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 适配器模式（Adapter），将一个类的接口转换成客户希望的另外一个接口。Adapter模式使得原本由于接口不兼容而不能一起工作的那些类可以一起工作。[DP].
    /// 在GoF的设计模式中，对适配器模式讲了两种类型，类适配器模式和对象适配器模式，由于类适配器模式通过多重继承对一个接口与另一个接口进行匹配，
    ///   而C#、VB.NET、JAVA等语言都不支持多重继承（C++支持），也就是一个类只有一个父类，所以我们这里主要讲的是对象适配器。
    /// Adapter（通过在内部包装一个Adaptee对象，把源接口转换成目标接口）
    /// 使用一个已经存在的类，但如果它的接口，也就是它的方法和你的要求不相同时，就应该考虑用适配器模式。
    /// 其实用适配器模式也是无奈之举，很有点‘亡羊补牢’的感觉，没办法呀，是软件就有维护的一天，维护就有可能会因不同的开发人员、不同的产品、
    ///   不同的厂家而造成功能类似而接口不同的情况，此时就是适配器模式大展拳脚的时候了。
    /// 是要在双方都不太容易修改的时候再使用适配器模式适配，而不是一有不同时就使用。。那有没有设计之初就需要考虑用适配器模式的时候？
    ///   当然有，比如公司设计一系统时考虑使用第三方开发组件，而这个组件的接口与我们自己的系统接口是不相同的，而我们也完全没有必要为了迎合它而改动自己的接口，
    ///   此时尽管是在开发的设计阶段，也是可以考虑用适配器模式来解决接口不同的问题.
    /// </summary>
    public class Adapter : Target
    {
        private Adaptee daptee = new Adaptee(); // 内部包装一个Adaptee对象

        /// <summary>
        /// 把源接口转换成目标接口.
        /// 这样就可以把表面上调用Request()方法变成实际调用SpecificRequest()方法了.
        /// </summary>
        public override void Request()
        {
            this.daptee.SpecificRequest();
        }
    }
}