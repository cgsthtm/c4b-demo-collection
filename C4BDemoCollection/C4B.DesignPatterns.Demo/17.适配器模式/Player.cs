﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._17.适配器模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 抽象球员类.
    /// </summary>
    public abstract class Player
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="name">姓名.</param>
        public Player(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Gets 姓名.
        /// </summary>
        protected string Name { get; }

        /// <summary>
        /// 进攻.
        /// </summary>
        public abstract void Attack(); // 进攻和防守方法

        /// <summary>
        /// 防守.
        /// </summary>
        public abstract void Defense();
    }
}