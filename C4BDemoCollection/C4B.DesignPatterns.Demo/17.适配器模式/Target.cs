﻿// <copyright file="Target.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._17.适配器模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Target（这是客户所期待的接口。可以是具体的或抽象的类，也可以是接口）.
    /// 适配器模式（Adapter），将一个类的接口转换成客户希望的另外一个接口。Adapter模式使得原本由于接口不兼容而不能一起工作的那些类可以一起工作。[DP]
    /// 适配器模式主要解决什么问题呢？简单地说，就是需要的东西就在面前，但却不能使用，而短时间又无法改造它，于是我们就想办法适配它。
    /// 在软件开发中，也就是系统的数据和行为都正确，但接口不符时，我们应该考虑用适配器，目的是使控制范围之外的一个原有对象与某个接口匹配。
    /// 适配器模式主要应用于希望复用一些现存的类，但是接口又与复用环境要求不一致的情况，比如在需要对早期代码复用一些功能等应用上很有实际价值.
    /// </summary>
    public class Target
    {
        /// <summary>
        /// 目标接口.
        /// </summary>
        public virtual void Request()
        {
            SerilogHelper.Instance.Logger.Information("普通请求！");
        }
    }
}