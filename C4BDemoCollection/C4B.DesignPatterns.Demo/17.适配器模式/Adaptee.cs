﻿// <copyright file="Adaptee.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._17.适配器模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Adaptee（需要适配的类）.
    /// </summary>
    public class Adaptee
    {
        /// <summary>
        /// 需要适配的接口.
        /// </summary>
        public void SpecificRequest()
        {
            SerilogHelper.Instance.Logger.Information("特殊请求！");
        }
    }
}