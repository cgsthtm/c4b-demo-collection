﻿// <copyright file="CenterPlayer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._17.适配器模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 中锋球员类.
    /// </summary>
    public class CenterPlayer : Player
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CenterPlayer"/> class.
        /// </summary>
        /// <param name="name">姓名.</param>
        public CenterPlayer(string name)
            : base(name)
        {
        }

        /// <inheritdoc/>
        public override void Attack()
        {
            SerilogHelper.Instance.Logger.Information("中锋 {0} 进攻", this.Name);
        }

        /// <inheritdoc/>
        public override void Defense()
        {
            SerilogHelper.Instance.Logger.Information("中锋 {0} 防守", this.Name);
        }
    }
}