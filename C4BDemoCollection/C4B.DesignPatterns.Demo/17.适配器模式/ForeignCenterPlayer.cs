﻿// <copyright file="ForeignCenterPlayer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._17.适配器模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 外籍中锋球员类.
    /// </summary>
    public class ForeignCenterPlayer
    {
        /// <summary>
        /// Gets or sets 姓名.
        /// </summary>
        public string Name { get; set; } // 外籍中锋球员的姓名故意用属性而不是构造方法来区别与前三个球员类的不同

        /// <summary>
        /// 表明外籍中锋球员只懂得中文进攻.
        /// </summary>
        public void 进攻()
        {
            SerilogHelper.Instance.Logger.Information("外籍中锋 {0} 进攻", this.Name);
        }

        /// <summary>
        /// 表明外籍中锋球员只懂得中文防守.
        /// </summary>
        public void 防守()
        {
            SerilogHelper.Instance.Logger.Information("外籍中锋 {0} 防守", this.Name);
        }
    }
}