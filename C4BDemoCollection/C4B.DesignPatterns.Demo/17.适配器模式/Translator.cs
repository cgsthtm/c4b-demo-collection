﻿// <copyright file="Translator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._17.适配器模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 翻译者.
    /// 如果能事先预防接口不同的问题，不匹配问题就不会发生；在有小的接口不统一问题发生时，及时重构，问题不至于扩大；
    /// 只有碰到无法改变原有设计和代码的情况时，才考虑适配。事后控制不如事中控制，事中控制不如事前控制.
    /// </summary>
    public class Translator : Player
    {
        private ForeignCenterPlayer player; // 声明一个内部外籍中锋球员对象，表明翻译者与外籍中锋球员有关联

        /// <summary>
        /// Initializes a new instance of the <see cref="Translator"/> class.
        /// </summary>
        /// <param name="name">姓名.</param>
        public Translator(string name)
            : base(name)
        {
            this.player = new ForeignCenterPlayer(); // 实例化外籍中锋球员
            this.player.Name = name;
        }

        /// <summary>
        /// 翻译者将Attack翻译为进攻，告诉外籍中锋球员.
        /// </summary>
        public override void Attack()
        {
            this.player.进攻();
        }

        /// <summary>
        /// 翻译者将Defense翻译为防守，告诉外籍中锋球员.
        /// </summary>
        public override void Defense()
        {
            this.player.防守();
        }
    }
}