﻿// <copyright file="SerilogHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DesignPatterns.Demo
{
    using Serilog;
    using Serilog.Core;
    using Serilog.Sinks.SystemConsole.Themes;

    /// <summary>
    /// Serilog帮助类.
    /// </summary>
    public sealed class SerilogHelper
    {
        private static readonly SerilogHelper LogInstance = new SerilogHelper();

        private SerilogHelper()
        {
            this.Logger = new LoggerConfiguration().MinimumLevel.Debug().WriteTo.Console(theme: AnsiConsoleTheme.Literate).CreateLogger();
        }

        /// <summary>
        /// Gets Instance.
        /// </summary>
        public static SerilogHelper Instance
        {
            get { return LogInstance; }
        }

        /// <summary>
        /// Gets or sets Logger.
        /// </summary>
        public Logger Logger { get; set; }
    }
}