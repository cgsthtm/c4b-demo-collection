﻿// <copyright file="SubFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._8.工厂方法模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using C4B.DesignPatterns.Demo._1.简单工厂模式;

    /// <summary>
    /// 减法操作工厂.
    /// </summary>
    public class SubFactory : IFactory
    {
        /// <inheritdoc/>
        public Operation CreateOperation()
        {
            return new OperationSub();
        }
    }
}