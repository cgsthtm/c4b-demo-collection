﻿// <copyright file="IFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._8.工厂方法模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using C4B.DesignPatterns.Demo._1.简单工厂模式;

    /// <summary>
    /// 工厂接口.
    /// <para>
    /// 工厂方法模式（Factory Method），定义一个用于创建对象的接口，让子类决定实例化哪一个类。工厂方法使一个类的实例化延迟到其子类。[DP].
    /// </para>
    /// <para>
    /// 工厂方法模式实现时，客户端需要决定实例化哪一个工厂来实现运算类，选择判断的问题还是存在的，也就是说，工厂方法把简单工厂的内部逻辑判断移到了客户端代码来进行。
    /// 你想要加功能，本来是改工厂类的，而现在是修改客户端.
    /// </para>
    /// <para>
    /// 工厂方法克服了简单工厂违背开放-封闭原则的缺点，又保持了封装对象创建过程的优点.
    /// </para>
    /// <para>
    /// 工厂方法模式是简单工厂模式的进一步抽象和推广。由于使用了多态性，工厂方法模式保持了简单工厂模式的优点，而且克服了它的缺点。
    /// 但缺点是由于每加一个产品，就需要加一个产品工厂的类，增加了额外的开发量.
    /// </para>
    /// </summary>
    public interface IFactory
    {
        /// <summary>
        /// 创建具体操作对象.
        /// </summary>
        /// <returns>返回具体操作类.</returns>
        Operation CreateOperation();
    }
}