﻿// <copyright file="SimpleFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._8.工厂方法模式.工厂方法举例
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 简单工厂类.
    /// </summary>
    public class SimpleFactory
    {
        /// <summary>
        /// 创建雷锋.
        /// </summary>
        /// <param name="type">类型.</param>
        /// <returns>雷锋.</returns>
        public static LeiFeng CreateLeiFeng(string type)
        {
            LeiFeng result = null;
            switch (type)
            {
                case "学雷锋的大学生":
                    result = new Undergraduate();
                    break;

                case "志愿者":
                    result = new Volunteer();
                    break;
                default:
                    break;
            }

            return result;
        }
    }
}
