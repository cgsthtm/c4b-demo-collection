﻿// <copyright file="UndergraduateFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._8.工厂方法模式.工厂方法举例
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 学雷锋的大学生工厂.
    /// </summary>
    public class UndergraduateFactory : IFactory
    {
        /// <inheritdoc/>
        public LeiFeng CreateLeiFeng()
        {
            return new Undergraduate();
        }
    }
}
