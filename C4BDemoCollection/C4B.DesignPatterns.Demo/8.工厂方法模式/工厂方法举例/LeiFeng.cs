﻿// <copyright file="LeiFeng.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._8.工厂方法模式.工厂方法举例
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 雷锋类.
    /// </summary>
    public class LeiFeng
    {
        /// <summary>
        /// 扫地.
        /// </summary>
        public void Sweep()
        {
            SerilogHelper.Instance.Logger.Information("扫地");
        }

        /// <summary>
        /// 洗衣.
        /// </summary>
        public void Wash()
        {
            SerilogHelper.Instance.Logger.Information("洗衣");
        }
    }
}
