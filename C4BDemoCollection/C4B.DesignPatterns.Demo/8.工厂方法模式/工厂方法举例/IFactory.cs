﻿// <copyright file="IFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._8.工厂方法模式.工厂方法举例
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 工厂方法接口.
    /// </summary>
    public interface IFactory
    {
        /// <summary>
        /// 创建雷锋.
        /// </summary>
        /// <returns>雷锋.</returns>
        LeiFeng CreateLeiFeng();
    }
}
