﻿// <copyright file="VolunteerFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._8.工厂方法模式.工厂方法举例
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 志愿者工厂.
    /// </summary>
    public class VolunteerFactory : IFactory
    {
        /// <inheritdoc/>
        public LeiFeng CreateLeiFeng()
        {
            return new Volunteer();
        }
    }
}
