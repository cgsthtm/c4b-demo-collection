﻿// <copyright file="Secretary.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;

    /// <summary>
    /// 秘书类，与老板类类似.
    /// </summary>
    public class Secretary : ISubject
    {
        /// <summary>
        /// Gets or sets 委托.用于更新操作.
        /// </summary>
        public Action Update { get; set; } // 声明委托，名叫更新

        /// <summary>
        /// Gets or sets 状态.
        /// </summary>
        public string SubjectState { get; set; }

        /// <summary>
        /// 通知.
        /// </summary>
        public void Notify()
        {
            this.Update(); // 在访问“通知”方法时，调用“更新”
        }
    }
}
