﻿// <copyright file="Boss.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;

    /// <summary>
    /// 老板类.
    /// </summary>
    public class Boss : ISubject
    {
        /// <summary>
        /// Gets or sets 更新.
        /// 委托就是一种引用方法的类型。一旦为委托分配了方法，委托将与该方法具有完全相同的行为。委托方法的使用可以像其他任何方法一样，具有参数和返回值。
        ///   委托可以看作是对函数的抽象，是函数的‘类’，委托的实例将代表一个具体的函数。
        /// 一旦为委托分配了方法，委托将与该方法具有完全相同的行为。而且，一个委托可以搭载多个方法，所有方法被依次唤起。更重要的是，它可以使得委托对象所搭载的方法并不需要属于同一个类。
        /// 委托对象所搭载的所有方法必须具有相同的原形和形式，也就是拥有相同的参数列表和返回值类型。
        /// 注意，是先有观察者模式，再有委托事件技术.
        /// </summary>
        public Action Update { get; set; } // 声明委托，名叫更新

        /// <inheritdoc/>
        public string SubjectState { get; set; }

        /// <summary>
        /// 通知.
        /// </summary>
        public void Notify()
        {
            this.Update(); // 在访问“通知”方法时，调用“更新”
        }
    }
}
