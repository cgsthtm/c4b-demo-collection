﻿// <copyright file="ISubject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 通知者接口.
    /// </summary>
    public interface ISubject
    {
        /// <summary>
        /// Gets or sets 状态.
        /// </summary>
        string SubjectState { get; set; }

        /// <summary>
        /// 通知.
        /// </summary>
        void Notify();
    }
}
