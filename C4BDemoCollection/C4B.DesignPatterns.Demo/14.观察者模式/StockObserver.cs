﻿// <copyright file="StockObserver.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 看股票的同事.
    /// “看股票观察者”类和“看NBA观察者”类，去掉了父类“抽象观察类”，所以补上一些代码，并将“更新”方法名改为各自适合的方法名.
    /// </summary>
    public class StockObserver
    {
        private readonly string name;
        private readonly ISubject sub;

        /// <summary>
        /// Initializes a new instance of the <see cref="StockObserver"/> class.
        /// </summary>
        /// <param name="name">观察者姓名.</param>
        /// <param name="sub">具体通知者.</param>
        public StockObserver(string name, ISubject sub)
        {
            this.name = name;
            this.sub = sub;
        }

        /// <summary>
        /// 关闭股票行情.
        /// </summary>
        public void CloseStockMarket() // 现实中就是这样的，方法名本就不一定相同。
        {
            SerilogHelper.Instance.Logger.Information("{0} {1} 关闭股票行情，继续工作！", this.sub.SubjectState, this.name);
        }
    }
}
