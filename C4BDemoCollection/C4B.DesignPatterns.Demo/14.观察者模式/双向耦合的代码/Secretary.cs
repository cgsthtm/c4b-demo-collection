﻿// <copyright file="Secretary.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式.双向耦合的代码
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// 前台秘书类.
    /// ‘前台’类和这个‘看股票者’类之间相互耦合.前台类要增加观察者，观察者类需要前台的状态.
    /// 开放封闭原则-修改原有代码说明设计不够好.
    /// 依赖倒转原则-应该让程序依赖于抽象，而不是相互依赖.
     /// </summary>
    public class Secretary
    {
        // 同事列表
        private List<StockObserver> stockObservers = new List<StockObserver>();

        /// <summary>
        /// Gets or sets 前台秘书动作.前台通过电话，所说的话或所做的事.
        /// </summary>
        public string SecretaryAction { get; set; }

        /// <summary>
        /// 增加.
        /// </summary>
        /// <param name="stockObserver">看股票的同事.</param>
        public void Attach(StockObserver stockObserver)
        {
            // 有几个同事请前台秘书帮忙，就给集合添加几个对象
            this.stockObservers.Add(stockObserver);
        }

        /// <summary>
        /// 通知.
        /// </summary>
        public void Notify()
        {
            // 待老板来时，就给所有登记的同事发通知，“老板来了”
            foreach (var item in this.stockObservers.Cast<StockObserver>())
            {
                item.Update();
            }
        }
    }
}
