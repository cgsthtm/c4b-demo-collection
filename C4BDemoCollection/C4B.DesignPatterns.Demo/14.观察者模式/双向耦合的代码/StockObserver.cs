﻿// <copyright file="StockObserver.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式.双向耦合的代码
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 看股票同事类.
    /// ‘前台’类和这个‘看股票者’类之间相互耦合.前台类要增加观察者，观察者类需要前台的状态.
    /// 开放封闭原则-修改原有代码说明设计不够好.
    /// 依赖倒转原则-应该让程序依赖于抽象，而不是相互依赖.
    /// </summary>
    public class StockObserver
    {
        private string name;
        private Secretary sub;

        /// <summary>
        /// Initializes a new instance of the <see cref="StockObserver"/> class.
        /// </summary>
        /// <param name="name">姓名.</param>
        /// <param name="sub">前台秘书.</param>
        public StockObserver(string name, Secretary sub)
        {
            this.name = name;
            this.sub = sub;
        }

        /// <summary>
        /// 更新.
        /// </summary>
        public void Update()
        {
            SerilogHelper.Instance.Logger.Information($"{this.sub.SecretaryAction} {this.name} 关闭股票行情，继续工作！");
        }
    }
}
