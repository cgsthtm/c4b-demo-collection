﻿// <copyright file="Secretary.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式.双向耦合的代码.解耦实践1
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System.Collections.Generic;

    /// <summary>
    /// 前台秘书类.
    /// </summary>
    public class Secretary
    {
        // 同事列表
        private List<Observer> observers = new List<Observer>();

        /// <summary>
        /// Gets or sets 前台秘书动作.前台通过电话，所说的话或所做的事.
        /// </summary>
        public string SecretaryAction { get; set; }

        /// <summary>
        /// 增加.
        /// </summary>
        /// <param name="observer">观察者.</param>
        public void Attach(Observer observer) // 针对抽象编程，减少了与具体类的耦合
        {
            this.observers.Add(observer);
        }

        /// <summary>
        /// 减少.
        /// </summary>
        /// <param name="observer">观察者.</param>
        public void Detach(Observer observer) // 针对抽象编程，减少了与具体类的耦合
        {
            this.observers.Remove(observer);
        }

        /// <summary>
        /// 通知.
        /// </summary>
        public void Nofity()
        {
            foreach (var item in this.observers)
            {
                item.Update();
            }
        }
    }
}
