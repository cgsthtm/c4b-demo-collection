﻿// <copyright file="NBAObserver.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式.双向耦合的代码.解耦实践1
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 具体观察者.看NBA的同事.
    /// </summary>
    public class NBAObserver : Observer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NBAObserver"/> class.
        /// </summary>
        /// <param name="name">姓名.</param>
        /// <param name="sub">前台秘书.</param>
        public NBAObserver(string name, Secretary sub) // 在具体的观察者中与‘前台秘书’这个具体的类耦合了，应该将其抽象出来
            : base(name, sub)
        {
        }

        /// <inheritdoc/>
        public override void Update()
        {
            SerilogHelper.Instance.Logger.Information($"{this.Sub.SecretaryAction} {this.Name} 关闭NBA直播，继续工作！");
        }
    }
}
