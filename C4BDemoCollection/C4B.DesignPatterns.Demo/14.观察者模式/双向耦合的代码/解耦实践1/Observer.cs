﻿// <copyright file="Observer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式.双向耦合的代码.解耦实践1
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 抽象的观察者类.
    /// </summary>
    public abstract class Observer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Observer"/> class.
        /// </summary>
        /// <param name="name">姓名.</param>
        /// <param name="sub">前台秘书.</param>
        public Observer(string name, Secretary sub)
        {
            this.Name = name;
            this.Sub = sub;
        }

        /// <summary>
        /// Gets or sets 姓名.
        /// </summary>
        protected string Name { get; set; }

        /// <summary>
        /// Gets or sets 前台秘书.
        /// </summary>
        protected Secretary Sub { get; set; }

        /// <summary>
        /// 更新.
        /// </summary>
        public abstract void Update();
    }
}
