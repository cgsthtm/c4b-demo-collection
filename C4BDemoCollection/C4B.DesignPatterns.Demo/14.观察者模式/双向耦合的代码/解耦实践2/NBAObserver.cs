﻿// <copyright file="NBAObserver.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式.双向耦合的代码.解耦实践2
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 看NBA直播同事.实现抽象观察类.
    /// </summary>
    public class NBAObserver : Observer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NBAObserver"/> class.
        /// </summary>
        /// <param name="name">观察者姓名.</param>
        /// <param name="sub">具体通知者.</param>
        public NBAObserver(string name, ISubject sub)
            : base(name, sub)
        {
        }

        /// <inheritdoc/>
        public override void Update()
        {
            SerilogHelper.Instance.Logger.Information($"{this.Sub.SubjectState} {this.Name} 关闭NBA直播，继续工作！");
        }
    }
}
