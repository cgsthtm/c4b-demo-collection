﻿// <copyright file="ISubject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式.双向耦合的代码.解耦实践2
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 抽象通知者接口.
    /// </summary>
    public interface ISubject
    {
        /// <summary>
        /// Gets or sets 通知者状态.
        /// </summary>
        string SubjectState { get; set; }

        /// <summary>
        /// 添加.
        /// </summary>
        /// <param name="observer">观察者.</param>
        void Attach(Observer observer);

        /// <summary>
        /// 减少.
        /// </summary>
        /// <param name="observer">观察者.</param>
        void Detach(Observer observer);

        /// <summary>
        /// 通知.
        /// </summary>
        void Nofity();
    }
}
