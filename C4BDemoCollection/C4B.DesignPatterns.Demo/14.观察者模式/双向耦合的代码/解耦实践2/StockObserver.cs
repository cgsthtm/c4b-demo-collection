﻿// <copyright file="StockObserver.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式.双向耦合的代码.解耦实践2
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 看股票的同事.实现观察者抽象类.
    /// </summary>
    public class StockObserver : Observer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StockObserver"/> class.
        /// </summary>
        /// <param name="name">观察者姓名.</param>
        /// <param name="sub">具体的通知者.</param>
        public StockObserver(string name, ISubject sub) // 原来是前台，现改成抽象通知者
            : base(name, sub)
        {
        }

        /// <inheritdoc/>
        public override void Update()
        {
            SerilogHelper.Instance.Logger.Information($"{this.Sub.SubjectState} {this.Name} 关闭股票行情，继续工作！");
        }
    }
}
