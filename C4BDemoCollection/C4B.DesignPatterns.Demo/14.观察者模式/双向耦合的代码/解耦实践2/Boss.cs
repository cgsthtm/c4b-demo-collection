﻿// <copyright file="Boss.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式.双向耦合的代码.解耦实践2
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 老板.实现通知者接口.
    /// </summary>
    public class Boss : ISubject
    {
        // 同事列表
        private List<Observer> observers = new List<Observer>();

        /// <summary>
        /// Gets or sets 老板状态.
        /// </summary>
        public string SubjectState { get; set; }

        /// <inheritdoc/>
        public void Attach(Observer observer)
        {
            this.observers.Add(observer);
        }

        /// <inheritdoc/>
        public void Detach(Observer observer)
        {
            this.observers.Remove(observer);
        }

        /// <inheritdoc/>
        public void Nofity()
        {
            foreach (var item in this.observers)
            {
                item.Update();
            }
        }
    }
}
