﻿// <copyright file="Observer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式.双向耦合的代码.解耦实践2
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 抽象观察者.
    /// </summary>
    public abstract class Observer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Observer"/> class.
        /// </summary>
        /// <param name="name">观察者姓名.</param>
        /// <param name="sub">通知者.</param>
        public Observer(string name, ISubject sub) // 原来是前台，现改成抽象通知者
        {
            this.Name = name;
            this.Sub = sub;
        }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        protected string Name { get; set; }

        /// <summary>
        /// Gets or sets ISubject.
        /// </summary>
        protected ISubject Sub { get; set; }

        /// <summary>
        /// 更新.
        /// </summary>
        public abstract void Update();
    }
}
