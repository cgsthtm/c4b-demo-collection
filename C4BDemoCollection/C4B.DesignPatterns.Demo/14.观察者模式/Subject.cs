﻿// <copyright file="Subject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System.Collections.Generic;

    /// <summary>
    /// 观察者模式又叫做发布-订阅（Publish/Subscribe）模式。
    /// 观察者模式定义了一种一对多的依赖关系，让多个观察者对象同时监听某一个主题对象。这个主题对象在状态发生变化时，会通知所有观察者对象，使它们能够自动更新自己。[DP]
    /// Subject类，可翻译为主题或抽象通知者，一般用一个抽象类或者一个接口实现。它把所有对观察者对象的引用保存在一个聚集里，每个主题都可以有任何数量的观察者。
    ///   抽象主题提供一个接口，可以增加和删除观察者对象.
    /// </summary>
    public abstract class Subject
    {
        private IList<Observer> observers = new List<Observer>();

        /// <summary>
        /// 添加.
        /// </summary>
        /// <param name="observer">观察者.</param>
        public void Attach(Observer observer)
        {
            this.observers.Add(observer);
        }

        /// <summary>
        /// 移除.
        /// </summary>
        /// <param name="observer">观察者.</param>
        public void Detach(Observer observer)
        {
            this.observers.Remove(observer);
        }

        /// <summary>
        /// 通知.
        /// </summary>
        public void Notify()
        {
            foreach (var ob in this.observers)
            {
                ob.Update();
            }
        }
    }
}