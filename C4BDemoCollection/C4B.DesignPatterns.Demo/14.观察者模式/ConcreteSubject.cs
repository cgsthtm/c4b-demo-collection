﻿// <copyright file="ConcreteSubject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// ConcreteSubject类.
    /// 叫做具体主题或具体通知者，将有关状态存入具体观察者对象；在具体主题的内部状态改变时，给所有登记过的观察者发出通知。具体主题角色通常用一个具体子类实现.
    /// </summary>
    public class ConcreteSubject : Subject
    {
        /// <summary>
        /// Gets or sets 具体被观察者状态.
        /// </summary>
        public string SubjectState { get; set; }
    }
}
