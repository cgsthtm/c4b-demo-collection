﻿// <copyright file="ConcreteObserver.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// ConcreteObserver类.
    /// 具体观察者，实现抽象观察者角色所要求的更新接口，以便使本身的状态与主题的状态相协调。
    /// 具体观察者角色可以保存一个指向具体主题对象的引用。具体观察者角色通常用一个具体子类实现.
    /// </summary>
    public class ConcreteObserver : Observer
    {
        private string name;
        private string observerState;
        private ConcreteSubject subject;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConcreteObserver"/> class.
        /// </summary>
        /// <param name="subject">具体通知者.</param>
        /// <param name="name">观察者姓名.</param>
        public ConcreteObserver(ConcreteSubject subject, string name)
        {
            this.subject = subject;
            this.name = name;
        }

        /// <summary>
        /// Gets or sets 具体通知者.
        /// </summary>
        public ConcreteSubject Subject
        {
            get { return this.subject; }
            set { this.subject = value; }
        }

        /// <inheritdoc/>
        public override void Update()
        {
            this.observerState = this.subject.SubjectState;
            SerilogHelper.Instance.Logger.Information("观察者{0}的新状态是{1}", this.name, this.observerState);
        }
    }
}
