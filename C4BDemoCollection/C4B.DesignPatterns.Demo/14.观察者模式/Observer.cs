﻿// <copyright file="Observer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._14.观察者模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Observer类，抽象观察者，为所有的具体观察者定义一个接口，在得到主题的通知时更新自己。这个接口叫做更新接口。抽象观察者一般用一个抽象类或者一个接口实现。
    ///   更新接口通常包含一个Update()方法，这个方法叫做更新方法。
    /// 现实编程中，具体的观察者完全有可能是风马牛不相及的类，但它们都需要根据通知者的通知来做出Update()的操作，所以让它们都实现下面这样的一个接口就可以实现这个想法了。
    /// 将一个系统分割成一系列相互协作的类有一个很不好的副作用，那就是需要维护相关对象间的一致性。我们不希望为了维持一致性而使各类紧密耦合，这样会给维护、扩展和重用都带来不便[DP]
    ///   而观察者模式的关键对象是主题Subject和观察者Observer，一个Subject可以有任意数目的依赖它的Observer，一旦Subject的状态发生了改变，所有的Observer都可以得到通知。
    /// Subject发出通知时并不需要知道谁是它的观察者，也就是说，具体观察者是谁，它根本不需要知道。而任何一个具体观察者不知道也不需要知道其他观察者的存在。
    /// 什么时候考虑使用观察者模式呢？当一个对象的改变需要同时改变其他对象的时候。补充一下，而且它不知道具体有多少对象有待改变时，应该考虑使用观察者模式。
    /// 观察者模式所做的工作其实就是在解除耦合。让耦合的双方都依赖于抽象，而不是依赖于具体。从而使得各自的变化都不会影响另一边的变化.
    /// </summary>
    public abstract class Observer
    {
        /// <summary>
        /// 更新.
        /// </summary>
        public abstract void Update();
    }
}
