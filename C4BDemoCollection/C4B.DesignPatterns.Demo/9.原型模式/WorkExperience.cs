﻿// <copyright file="WorkExperience.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._9.原型模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;

    /// <summary>
    /// 工作经历类.
    /// </summary>
    public class WorkExperience : ICloneable // 让“工作经历”实现ICloneable接口
    {
        /// <summary>
        /// Gets or sets 工作时间.
        /// </summary>
        public string WorkDate { get; set; }

        /// <summary>
        /// Gets or sets 公司.
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// 克隆.
        /// </summary>
        /// <returns>object.</returns>
        public object Clone()
        {
            return (object)this.MemberwiseClone(); // “工作经历”类实现克隆方法
        }
    }
}
