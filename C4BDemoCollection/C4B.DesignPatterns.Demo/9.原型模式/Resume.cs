﻿// <copyright file="Resume.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._9.原型模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;

    /// <summary>
    /// 简历类.
    /// 由于在一些特定场合，会经常涉及深复制或浅复制，比如说，数据集对象DataSet，它就有Clone()方法和Copy()方法，
    ///   Clone()方法用来复制DataSet的结构，但不复制DataSet的数据，实现了原型模式的浅复制。Copy()方法不但复制结构，也复制数据，其实就是实现了原型模式的深复制。
    /// 原型模式（Prototype），用原型实例指定创建对象的种类，并且通过拷贝这些原型创建新的对象。[DP].
    /// 原型模式其实就是从一个对象再创建另外一个可定制的对象，而且不需知道任何创建的细节.
    /// 对于.NET而言，在System命名空间中提供了ICloneable接口，其中就是唯一的一个方法Clone()，这样你就只需要实现这个接口就可以完成原型模式了.
    /// </summary>
    public class Resume : ICloneable // ICloneable接口.
    {
        private string name;
        private string sex;
        private string age;
        private WorkExperience work;

        /// <summary>
        /// Initializes a new instance of the <see cref="Resume"/> class.
        /// </summary>
        /// <param name="name">姓名.</param>
        public Resume(string name)
        {
            this.name = name;
            this.work = new WorkExperience(); // 实例化工作经历
        }

        private Resume(WorkExperience work)
        {
            this.work = (WorkExperience)work.Clone(); // 私有构造函数中提供Clone方法调用，以便克隆“工作经历”的数据
        }

        /// <summary>
        /// 设置个人信息.
        /// </summary>
        /// <param name="sex">性别.</param>
        /// <param name="age">年龄.</param>
        public void SetPersonalInfo(string sex, string age)
        {
            this.sex = sex;
            this.age = age;
        }

        /// <summary>
        /// 设置工作经历.
        /// </summary>
        /// <param name="workDate">工作日期.</param>
        /// <param name="company">公司.</param>
        public void SerWorkExperience(string workDate, string company)
        {
            this.work.WorkDate = workDate;
            this.work.Company = company;
        }

        /// <summary>
        /// 展示工作经历.
        /// </summary>
        public void Display()
        {
            SerilogHelper.Instance.Logger.Information("{name} {sex} {age}", this.name, this.sex, this.age);
            SerilogHelper.Instance.Logger.Information("工作经历：{workDate} {company}", this.work.WorkDate, this.work.Company);
        }

        /// <summary>
        /// 克隆.
        /// </summary>
        /// <returns>Object.</returns>
        public object Clone()
        {
            Resume obj = new Resume(this.work); // 调用私有构造函数，让“工作经历”克隆完成，然后再给这个“简历”对象的相关字段赋值，最终返回一个深复制的简历对象
            obj.name = this.name;
            obj.sex = this.sex;
            obj.age = this.age;
            return obj;
        }
    }
}
