﻿// <copyright file="ConcretePrototype1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._9.原型模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 具体原型类.
    /// </summary>
    public class ConcretePrototype1 : Prototype
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConcretePrototype1"/> class.
        /// </summary>
        /// <param name="id">id.</param>
        public ConcretePrototype1(string id)
            : base(id)
        {
        }

        /// <inheritdoc/>
        public override Prototype Clone()
        {
            // 创建当前对象的浅表副本。方法是创建一个新对象，然后将当前对象的非静态字段复制到该新对象。
            // 如果字段是值类型的，则对该字段进行逐位复制。如果该字段是引用类型，则复制引用但不复制引用的对象；因此原始对象及其副本引用同一对象。
            return (Prototype)this.MemberwiseClone();
        }
    }
}
