﻿// <copyright file="Prototype.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._9.原型模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 原型模式（Prototype），用原型实例指定创建对象的种类，并且通过拷贝这些原型创建新的对象。[DP]
    /// 原型模式其实就是从一个对象再创建另外一个可定制的对象，而且不需知道任何创建的细节。
    /// 对于.NET而言，这个原型抽象类Prototype是用不着的，因为克隆实在是太常用了，所以.NET在System命名空间中提供了ICloneable接口，
    ///   其中就是唯一的一个方法Clone()，这样你就只需要实现这个接口就可以完成原型模式了。
    /// 每NEW一次，都需要执行一次构造函数，如果构造函数的执行时间很长，那么多次的执行这个初始化操作就实在是太低效了。一般在初始化的信息不发生变化的情况下，克隆是最好的办法。
    ///   这既隐藏了对象创建的细节，又对性能是大大的提高，何乐而不为呢？
    /// 不用重新初始化对象，而是动态地获得对象运行时的状态。
    /// MemberwiseClone()方法是这样，如果字段是值类型的，则对该字段执行逐位复制，如果字段是引用类型，则复制引用但不复制引用的对象；因此，原始对象及其复本引用同一对象。
    /// ‘浅复制’，被复制对象的所有变量都含有与原来的对象相同的值，而所有的对其他对象的引用都仍然指向原来的对象。
    /// ‘深复制’，把引用对象的变量指向复制过的新对象，而不是原有的被引用的对象。
    /// ‘深复制’要深入到多少层，需要事先就考虑好，而且要当心出现循环引用的问题，需要小心处理，这里比较复杂，可以慢慢研究.
    /// </summary>
    public abstract class Prototype
    {
        private string id;

        /// <summary>
        /// Initializes a new instance of the <see cref="Prototype"/> class.
        /// </summary>
        /// <param name="id">id.</param>
        public Prototype(string id)
        {
            this.id = id;
        }

        /// <summary>
        /// Gets Id.
        /// </summary>
        public string Id
        {
            get
            {
                return this.id;
            }
        }

        /// <summary>
        /// 克隆.
        /// </summary>
        /// <returns>Prototype.</returns>
        public abstract Prototype Clone();
    }
}
