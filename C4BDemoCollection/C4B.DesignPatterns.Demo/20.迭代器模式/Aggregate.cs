﻿// <copyright file="Aggregate.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._20.迭代器模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Aggregate聚集抽象类.
    /// </summary>
    public abstract class Aggregate
    {
        /// <summary>
        /// 创建迭代器.
        /// </summary>
        /// <returns>迭代器.</returns>
        public abstract Iterator CreateIterator(); // 创建迭代器
    }
}