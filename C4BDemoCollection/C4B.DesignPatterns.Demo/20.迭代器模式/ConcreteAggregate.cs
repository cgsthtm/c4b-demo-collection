﻿// <copyright file="ConcreteAggregate.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._20.迭代器模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System.Collections.Generic;

    /// <summary>
    /// ConcreteAggregate具体聚集类　继承Aggregate.
    /// </summary>
    public class ConcreteAggregate : Aggregate
    {
        private IList<object> items = new List<object>();

        /// <summary>
        /// Gets 聚集总个数.
        /// </summary>
        public int Count
        {
            get { return this.items.Count; } // 返回聚集总个数
        }

        /// <summary>
        /// 索引器.
        /// </summary>
        /// <param name="index">索引.</param>
        /// <returns>对象.</returns>
        public object this[int index] // 声明一个索引器
        {
            get { return this.items[index]; }
            set { this.items.Insert(index, value); }
        }

        /// <inheritdoc/>
        public override Iterator CreateIterator()
        {
            return new ConcreteIterator(this);
        }
    }
}