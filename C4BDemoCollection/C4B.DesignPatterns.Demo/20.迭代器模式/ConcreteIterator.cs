﻿// <copyright file="ConcreteIterator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._20.迭代器模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// ConcreteIterator具体迭代器类，继承Iterator.
    /// </summary>
    public class ConcreteIterator : Iterator
    {
        private ConcreteAggregate aggregate; // 定义一个具体聚集对象
        private int current = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConcreteIterator"/> class.
        /// </summary>
        /// <param name="aggregate">聚集.</param>
        public ConcreteIterator(ConcreteAggregate aggregate)
        {
            this.aggregate = aggregate;
        }

        /// <inheritdoc/>
        public override object CurrentItem()
        {
            return this.aggregate[this.current]; // 返回当前聚集对象
        }

        /// <inheritdoc/>
        public override object First()
        {
            return this.aggregate[0];
        }

        /// <inheritdoc/>
        public override bool IsDone()
        {
            return this.current >= this.aggregate.Count ? true : false;
        }

        /// <inheritdoc/>
        public override object Next()
        {
            object ret = null;
            this.current++;
            if (this.current < this.aggregate.Count)
            {
                ret = this.aggregate[this.current];
            }

            return ret;
        }
    }
}