﻿// <copyright file="ConcreteIteratorDesc.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._20.迭代器模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 反向迭代器.
    /// </summary>
    public class ConcreteIteratorDesc : Iterator
    {
        private ConcreteAggregate aggregate; // 定义了一个具体聚集对象
        private int current = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConcreteIteratorDesc"/> class.
        /// </summary>
        /// <param name="concreteAggregate">聚集对象.</param>
        public ConcreteIteratorDesc(ConcreteAggregate concreteAggregate)
        {
            this.aggregate = concreteAggregate;
            this.current = concreteAggregate.Count - 1;
        }

        /// <inheritdoc/>
        public override object CurrentItem()
        {
            return this.aggregate[this.current];
        }

        /// <inheritdoc/>
        public override object First()
        {
            return this.aggregate[this.aggregate.Count - 1];
        }

        /// <inheritdoc/>
        public override bool IsDone()
        {
            return this.current < 0 ? true : false;
        }

        /// <inheritdoc/>
        public override object Next()
        {
            object ret = null;
            this.current--;
            if (this.current >= 0)
            {
                ret = this.aggregate[this.current];
            }

            return ret;
        }
    }
}
