﻿// <copyright file="Iterator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._20.迭代器模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Iterator迭代器抽象类.
    /// 迭代器模式（Iterator），提供一种方法顺序访问一个聚合对象中各个元素，而又不暴露该对象的内部表示。[DP].
    /// 当你需要访问一个聚集对象，而且不管这些对象是什么都需要遍历的时候，你就应该考虑用迭代器模式。
    /// 你需要对聚集有多种方式遍历时，可以考虑用迭代器模式。为遍历不同的聚集结构提供如开始、下一个、是否结束、当前哪一项等统一的接口。
    /// 过现今来看迭代器模式实用价值远不如学习价值大了，MartinFlower甚至在自己的网站上提出撤销此模式。因为现在高级编程语言如C#、JAVA等本身已经把这个模式做在语言中了。
    /// IEnumerable接口也是为迭代器模式而准备。
    /// 迭代器（Iterator）模式就是分离了集合对象的遍历行为，抽象出一个迭代器类来负责，这样既可以做到不暴露集合的内部结构，又可让外部代码透明地访问集合内部的数据。
    /// 迭代器模式在访问数组、集合、列表等数据时，尤其是数据库数据操作时，是非常普遍的应用，但由于它太普遍了，所以各种高级语言都对它进行了封装，所以反而给人感觉此模式本身不太常用了.
    /// </summary>
    public abstract class Iterator
    {
        /// <summary>
        /// 得到开始对象.
        /// </summary>
        /// <returns>对象.</returns>
        public abstract object First();

        /// <summary>
        /// 得到下一个对象.
        /// </summary>
        /// <returns>对象.</returns>
        public abstract object Next();

        /// <summary>
        /// 是否完成迭代.
        /// </summary>
        /// <returns>结果.</returns>
        public abstract bool IsDone();

        /// <summary>
        /// 当前项.
        /// </summary>
        /// <returns>对象.</returns>
        public abstract object CurrentItem();
    }
}