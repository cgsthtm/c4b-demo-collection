﻿// <copyright file="Composite.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._19.组合模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Composite定义有枝节点行为，用来存储子部件，在Component接口中实现与子部件有关的操作，比如增加Add和删除Remove。
    /// 树可能有无数的分枝，但只需要反复用Composite就可以实现树状结构了。
    /// 透明方式，也就是说在Component中声明所有用来管理子对象的方法，其中包括Add、Remove等。这样实现Component接口的所有子类都具备了Add和Remove。
    ///   这样做的好处就是叶节点和枝节点对于外界没有区别，它们具备完全一致的行为接口。但问题也很明显，因为Leaf类本身不具备Add()、Remove()方法的功能，所以实现它是没有意义的。
    /// 安全方式，也就是在Component接口中不去声明Add和Remove方法，那么子类的Leaf也就不需要去实现它，而是在Composite声明所有用来管理子类对象的方法，
    ///   这样做就不会出现刚才提到的问题，不过由于不够透明，所以树叶和树枝类将不具有相同的接口，客户端的调用需要做相应的判断，带来了不便。
    /// 需求中是体现部分与整体层次的结构时，以及你希望用户可以忽略组合对象与单个对象的不同，统一地使用组合结构中的所有对象时，就应该考虑用组合模式了。
    /// 所有的Web控件的基类都是System.Web.UI.Control，而Control基类中就有Add和Remove方法，这就是典型的组合模式的应用.
    /// </summary>
    public class Composite : Component
    {
        private List<Component> children = new List<Component>(); // 一个子对象集合，用来存储其下属的枝节点和叶节点

        /// <summary>
        /// Initializes a new instance of the <see cref="Composite"/> class.
        /// </summary>
        /// <param name="name">名称.</param>
        public Composite(string name)
            : base(name)
        {
        }

        /// <inheritdoc/>
        public override void Add(Component c)
        {
            this.children.Add(c);
        }

        /// <summary>
        /// 显示其枝节点名称，并对其下级进行遍历.
        /// </summary>
        /// <param name="depth">深度.</param>
        public override void Display(int depth)
        {
            SerilogHelper.Instance.Logger.Information("{0}", new string('-', depth) + this.Name);
            foreach (var component in this.children)
            {
                component.Display(depth + 2);
            }
        }

        /// <inheritdoc/>
        public override void Remove(Component c)
        {
            this.children.Remove(c);
        }
    }
}