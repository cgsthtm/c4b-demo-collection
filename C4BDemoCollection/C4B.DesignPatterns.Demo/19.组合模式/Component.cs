﻿// <copyright file="Component.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._19.组合模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 组合模式（Composite），将对象组合成树形结构以表示‘部分-整体’的层次结构。组合模式使得用户对单个对象和组合对象的使用具有一致性。[DP]
    /// Component为组合中的对象声明接口，在适当情况下，实现所有类共有接口的默认行为.
    /// </summary>
    public abstract class Component
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Component"/> class.
        /// </summary>
        /// <param name="name">名称.</param>
        public Component(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Gets or sets 名称.
        /// </summary>
        protected string Name { get; set; }

        /// <summary>
        /// 添加.
        /// </summary>
        /// <param name="c">组成部分.</param>
        public abstract void Add(Component c); // 通常用Add和Remove方法来提供增加或移除树枝或树枝的功能

        /// <summary>
        /// 移除.
        /// </summary>
        /// <param name="c">组成部分.</param>
        public abstract void Remove(Component c);

        /// <summary>
        /// 显示.
        /// </summary>
        /// <param name="depth">深度.</param>
        public abstract void Display(int depth);
    }
}