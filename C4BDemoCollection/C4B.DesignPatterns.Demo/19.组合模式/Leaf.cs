﻿// <copyright file="Leaf.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._19.组合模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;

    /// <summary>
    /// Leaf在组合中表示叶节点对象，叶节点没有子节点.
    /// </summary>
    public class Leaf : Component
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Leaf"/> class.
        /// </summary>
        /// <param name="name">名称.</param>
        public Leaf(string name)
            : base(name)
        {
        }

        /// <inheritdoc/>
        public override void Add(Component c)
        {
            // 由于叶子没有再增加分枝和树叶，所以Add和Remove方法实现它没有意义，但这样做可以消除叶节点和枝节点对象在抽象层次的区别，他们具备完全一致的接口。
            SerilogHelper.Instance.Logger.Information("Cannot add to a leaf");
        }

        /// <inheritdoc/>
        public override void Display(int depth)
        {
            SerilogHelper.Instance.Logger.Information("{0}", new string('-', depth) + this.Name);
        }

        /// <inheritdoc/>
        public override void Remove(Component c)
        {
            SerilogHelper.Instance.Logger.Information("Cannot remove from a leaf");
        }
    }
}