﻿// <copyright file="Company.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._19.组合模式.组合模式举例
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 公司.
    /// </summary>
    public abstract class Company
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Company"/> class.
        /// </summary>
        /// <param name="name">名称.</param>
        public Company(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Gets or sets 名称.
        /// </summary>
        protected string Name { get; set; }

        /// <summary>
        /// 增加.
        /// </summary>
        /// <param name="c">公司.</param>
        public abstract void Add(Company c);

        /// <summary>
        /// 移除.
        /// </summary>
        /// <param name="c">公司.</param>
        public abstract void Remove(Company c);

        /// <summary>
        /// 显示.
        /// </summary>
        /// <param name="depth">深度.</param>
        public abstract void Display(int depth);

        /// <summary>
        /// 履行职责.
        /// </summary>
        public abstract void LineOfDuty();
    }
}