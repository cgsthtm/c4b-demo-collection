﻿// <copyright file="ConcreteCompany.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._19.组合模式.组合模式举例
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System.Collections.Generic;

    /// <summary>
    /// 具体公司.
    /// </summary>
    public class ConcreteCompany : Company
    {
        private List<Company> children = new List<Company>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ConcreteCompany"/> class.
        /// </summary>
        /// <param name="name">名称.</param>
        public ConcreteCompany(string name)
            : base(name)
        {
        }

        /// <inheritdoc/>
        public override void Add(Company c)
        {
            this.children.Add(c);
        }

        /// <inheritdoc/>
        public override void Display(int depth)
        {
            SerilogHelper.Instance.Logger.Information(new string('-', depth) + this.Name);
            foreach (var item in this.children)
            {
                item.Display(depth + 2);
            }
        }

        /// <inheritdoc/>
        public override void LineOfDuty()
        {
            foreach (var item in this.children)
            {
                item.LineOfDuty();
            }
        }

        /// <inheritdoc/>
        public override void Remove(Company c)
        {
            this.children.Remove(c);
        }
    }
}
