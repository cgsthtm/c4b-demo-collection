﻿// <copyright file="HRDepartment.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._19.组合模式.组合模式举例
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 人力资源部.
    /// </summary>
    public class HRDepartment : Company
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HRDepartment"/> class.
        /// </summary>
        /// <param name="name">名称.</param>
        public HRDepartment(string name)
            : base(name)
        {
        }

        /// <inheritdoc/>
        public override void Add(Company c)
        {
        }

        /// <inheritdoc/>
        public override void Display(int depth)
        {
            SerilogHelper.Instance.Logger.Information(new string('-', depth) + this.Name);
        }

        /// <inheritdoc/>
        public override void LineOfDuty()
        {
            SerilogHelper.Instance.Logger.Information($"{this.Name} 员工招聘培训管理");
        }

        /// <inheritdoc/>
        public override void Remove(Company c)
        {
        }
    }
}
