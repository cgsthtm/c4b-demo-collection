﻿// <copyright file="FinanceDepartment.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._19.组合模式.组合模式举例
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 财务部.
    /// </summary>
    public class FinanceDepartment : Company
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FinanceDepartment"/> class.
        /// </summary>
        /// <param name="name">名称.</param>
        public FinanceDepartment(string name)
            : base(name)
        {
        }

        /// <inheritdoc/>
        public override void Add(Company c)
        {
        }

        /// <inheritdoc/>
        public override void Display(int depth)
        {
            SerilogHelper.Instance.Logger.Information(new string('-', depth) + this.Name);
        }

        /// <inheritdoc/>
        public override void LineOfDuty()
        {
            SerilogHelper.Instance.Logger.Information($"{this.Name} 公司财务收支管理");
        }

        /// <inheritdoc/>
        public override void Remove(Company c)
        {
        }
    }
}
