﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.DesignPatterns.Demo._28.访问者模式
{
    /// <summary>
    /// ConcreteVisitor1和ConcreteVisitor2类，具体访问者，实现每个由Visitor声明的操作。每个操作实现算法的一部分，而该算法片断乃是对应于结构中对象的类。
    /// </summary>
    public class ConcreteVisitor1 : Visitor
    {
        public override void VisitConcreteElementA(ConcreteElementA concreteElementA)
        {
            SerilogHelper.Instance.Logger.Information("{0}被{1}访问", concreteElementA.GetType().Name, this.GetType().Name);
        }

        public override void VisitConcreteElementB(ConcreteElementB concreteElementB)
        {
            SerilogHelper.Instance.Logger.Information("{0}被{1}访问", concreteElementB.GetType().Name, this.GetType().Name);
        }
    }

    // ConcreteVisitor2与上面类似
    public class ConcreteVisitor2 : Visitor
    {
        public override void VisitConcreteElementA(ConcreteElementA concreteElementA)
        {
            SerilogHelper.Instance.Logger.Information("{0}被{1}访问", concreteElementA.GetType().Name, this.GetType().Name);
        }

        public override void VisitConcreteElementB(ConcreteElementB concreteElementB)
        {
            SerilogHelper.Instance.Logger.Information("{0}被{1}访问", concreteElementB.GetType().Name, this.GetType().Name);
        }
    }
}
