﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.DesignPatterns.Demo._28.访问者模式
{
    /// <summary>
    /// ConcreteElementA和ConcreteElementB类，具体元素，实现Accept操作。
    /// </summary>
    public class ConcreteElementA : Element
    {
        public override void Accept(Visitor visitor)
        {
            visitor.VisitConcreteElementA(this); // 充分利用双分派技术，实现处理与数据结构的分离
        }

        public void OperateA() { } // 其他的相关方法
    }
}
