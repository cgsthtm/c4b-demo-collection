﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.DesignPatterns.Demo._28.访问者模式
{
    /// <summary>
    /// Visitor类，为该对象结构中ConcreteElement的每一个类声明一个Visit操作。
    /// 访问者模式（Visitor），表示一个作用于某对象结构中的各元素的操作。它使你可以在不改变各元素的类的前提下定义作用于这些元素的新操作。[DP]
    /// 访问者模式适用于数据结构相对稳定的系统。
    /// 访问者模式的目的是要把处理从数据结构分离出来。很多系统可以按照算法和数据结构分开，如果这样的系统有比较稳定的数据结构，又有易于变化的算法的话，使用访问者模式就是比较合适的，因为访问者模式使得算法操作的增加变得容易。
    /// 反之，如果这样的系统的数据结构对象易于变化，经常要有新的数据对象增加进来，就不适合使用访问者模式。
    /// 访问者模式的优点就是增加新的操作很容易，因为增加新的操作就意味着增加一个新的访问者。访问者模式将有关的行为集中到一个访问者对象中。
    /// 访问者的缺点其实也就是使增加新的数据结构变得困难了。
    /// GoF四人中的一个作者就说过：‘大多时候你并不需要访问者模式，但当一旦你需要访问者模式时，那就是真的需要它了。’事实上，我们很难找到数据结构不变化的情况，所以用访问者模式的机会也就不太多了。
    /// 访问者模式的能力和复杂性是把双刃剑，只有当你真正需要它的时候，才考虑使用它。有很多的程序员为了展示自己的面向对象的能力或是沉迷于模式当中，往往会误用这个模式，所以一定要好好理解它的适用性。
    /// </summary>
    public abstract class Visitor
    {
        public abstract void VisitConcreteElementA(ConcreteElementA concreteElementA);

        public abstract void VisitConcreteElementB(ConcreteElementB concreteElementB);
    }
}
