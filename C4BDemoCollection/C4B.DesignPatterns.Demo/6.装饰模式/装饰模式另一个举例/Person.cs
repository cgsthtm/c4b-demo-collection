﻿// <copyright file="Person.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._6.装饰模式.装饰模式另一个举例
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 人类.
    /// </summary>
    public class Person
    {
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="Person"/> class.
        /// </summary>
        public Person()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Person"/> class.
        /// </summary>
        /// <param name="name">姓名.</param>
        public Person(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// 展示装扮.
        /// </summary>
        public virtual void Show()
        {
            SerilogHelper.Instance.Logger.Information($"装扮的{this.name}");
        }
    }
}
