﻿// <copyright file="TShirts.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._6.装饰模式.装饰模式另一个举例
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// T恤类.
    /// </summary>
    public class TShirts : Finery
    {
        /// <inheritdoc/>
        public override void Show()
        {
            SerilogHelper.Instance.Logger.Information("T恤");
            base.Show();
        }
    }
}
