﻿// <copyright file="Finery.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._6.装饰模式.装饰模式另一个举例
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 装扮基类.
    /// </summary>
    public class Finery : Person
    {
        /// <summary>
        /// Gets or sets 组件.
        /// </summary>
        protected Person Component { get; set; }

        /// <summary>
        /// 装扮.
        /// </summary>
        /// <param name="component">组件.</param>
        public void Decorate(Person component)
        {
            this.Component = component;
        }

        /// <inheritdoc/>
        public override void Show()
        {
            this.Component?.Show();
        }
    }
}
