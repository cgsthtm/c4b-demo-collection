﻿// <copyright file="Decorator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._6.装饰模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 装饰者.
    /// 装饰模式（Decorator），动态地给一个对象添加一些额外的职责，就增加功能来说，装饰模式比生成子类更为灵活。[DP].
    /// 装饰模式是利用SetComponent来对对象进行包装的。这样每个装饰对象的实现就和如何使用这个对象分离开了，每个装饰对象只关心自己的功能，不需要关心如何被添加到对象链当中[DPE].
    /// 学习模式要善于变通，如果只有一个ConcreteComponent类而没有抽象的Component类，那么Decorator类可以是ConcreteComponent的一个子类。
    /// 同样道理，如果只有一个ConcreteDecorator类，那么就没有必要建立一个单独的Decorator类，而可以把Decorator和ConcreteDecorator的责任合并成一个类.
    /// 装饰模式是为已有功能动态地添加更多功能的一种方式。
    /// 当系统需要新功能的时候，是向旧的类中添加新的代码。这些新加的代码通常装饰了原有类的核心职责或主要行为，比如用西装或嘻哈服来装饰小菜，
    ///   但这种做法的问题在于，它们在主类中加入了新的字段，新的方法和新的逻辑，从而增加了主类的复杂度。而这些新加入的东西仅仅是为了满足一些只在某种特定情况下才会执行的特殊行为的需要.
    /// 装饰模式却提供了一个非常好的解决方案，它把每个要装饰的功能放在单独的类中，并让这个类包装它所要装饰的对象，
    ///   因此，当需要执行特殊行为时，客户代码就可以在运行时根据需要有选择地、按顺序地使用装饰功能包装对象了[DP].
    /// 装饰模式的优点我总结下来就是，把类中的装饰功能从类中搬移去除，这样可以简化原有的类.这样做更大的好处就是有效地把类的核心职责和装饰功能区分开了。而且可以去除相关类中重复的装饰逻辑.
    /// 装饰模式的装饰顺序很重要哦，比如加密数据和过滤词汇都可以是数据持久化前的装饰功能，但若先加密了数据再用过滤功能就会出问题了，
    ///   最理想的情况，是保证装饰类之间彼此独立，这样它们就可以以任意的顺序进行组合了.
    /// </summary>
    public abstract class Decorator : Component
    {
        private Component component; // 设置Component

        /// <summary>
        /// 设置组件.
        /// </summary>
        /// <param name="component">组件.</param>
        public void SetComponent(Component component)
        {
            this.component = component;
        }

        /// <summary>
        /// 重写Operation，实际执行的是_component的Operation.
        /// </summary>
        public override void Operation()
        {
            this.component?.Operation();
        }
    }
}
