﻿// <copyright file="Component.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._6.装饰模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Component是定义一个对象接口，可以给这些对象动态地添加职责.
    /// </summary>
    public abstract class Component
    {
        /// <summary>
        /// 操作.
        /// </summary>
        public abstract void Operation();
    }
}
