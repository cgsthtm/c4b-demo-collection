﻿// <copyright file="ConcreteDecoratorA.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._6.装饰模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 具体装饰对象A.
    /// </summary>
    public class ConcreteDecoratorA : Decorator
    {
        private string addedState; // 本类独有功能，以区别于ConcreteDecoratorB

        /// <inheritdoc/>
        public override void Operation()
        {
            base.Operation(); // 首先运行原Component的Operation()，再执行本类的功能，相当于对原Component进行了装饰
            this.addedState = "New State";
            SerilogHelper.Instance.Logger.Information("具体装饰对象A的操作");
        }
    }
}
