﻿// <copyright file="ConcreteComponent.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._6.装饰模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 具体对象.也可以给这个对象添加一些职责.
    /// </summary>
    public class ConcreteComponent : Component
    {
        /// <inheritdoc/>
        public override void Operation()
        {
            SerilogHelper.Instance.Logger.Information("具体对象的操作");
        }
    }
}
