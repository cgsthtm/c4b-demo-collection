﻿// <copyright file="ConcreteDecoratorB.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._6.装饰模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 具体装饰对象B.
    /// </summary>
    public class ConcreteDecoratorB : Decorator
    {
        /// <inheritdoc/>
        public override void Operation()
        {
            base.Operation(); // 首先运行原Component的Operation()，再执行本类功能，如AddedBehavior()，相当于对原Component进行了装饰
            this.AddedBehavior();
            SerilogHelper.Instance.Logger.Information("具体装饰对象B的操作");
        }

        /// <summary>
        /// 本类特有功能.
        /// </summary>
        public void AddedBehavior()
        {
            // 本类独有方法，以区别于ConcreteDecoratorA
            SerilogHelper.Instance.Logger.Information("ConcreteDecoratorB的独有方法AddedBehavior()被执行...");
        }
    }
}
