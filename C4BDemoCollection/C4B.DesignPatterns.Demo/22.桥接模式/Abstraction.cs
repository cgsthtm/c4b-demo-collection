﻿// <copyright file="Abstraction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._22.桥接模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 在发现我们需要多角度去分类实现对象，而只用继承会造成大量的类增加，不能满足开放-封闭原则时，就应该要考虑用桥接模式了.
    /// </summary>
    public class Abstraction
    {
        /// <summary>
        /// Gets or sets 实现者.
        /// </summary>
        protected Implementor Implementor { get; set; }

        /// <summary>
        /// 设置实现者.
        /// </summary>
        /// <param name="implementor">实现者.</param>
        public void SetImplementor(Implementor implementor)
        {
            this.Implementor = implementor;
        }

        /// <summary>
        /// 操作.
        /// </summary>
        public virtual void Operation()
        {
            this.Implementor.Operation();
        }
    }
}