﻿// <copyright file="RefinedAbstraction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._22.桥接模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 在发现我们需要多角度去分类实现对象，而只用继承会造成大量的类增加，不能满足开放-封闭原则时，就应该要考虑用桥接模式了
    /// 只要真正深入地理解了设计原则，很多设计模式其实就是原则的应用而已，或许在不知不觉中就在使用设计模式了.
    /// </summary>
    public class RefinedAbstraction : Abstraction
    {
        /// <inheritdoc/>
        public override void Operation()
        {
            this.Implementor.Operation();
        }
    }
}
