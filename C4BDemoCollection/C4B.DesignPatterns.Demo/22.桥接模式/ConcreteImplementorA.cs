﻿// <copyright file="ConcreteImplementorA.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._22.桥接模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 具体实现者A.
    /// </summary>
    public class ConcreteImplementorA : Implementor
    {
        /// <inheritdoc/>
        public override void Operation()
        {
            SerilogHelper.Instance.Logger.Information("具体实现A的方法执行");
        }
    }
}
