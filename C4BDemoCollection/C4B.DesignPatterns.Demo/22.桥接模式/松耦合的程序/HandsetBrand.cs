﻿// <copyright file="HandsetBrand.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._22.桥接模式.松耦合的程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 手机品牌.
    /// </summary>
    public abstract class HandsetBrand
    {
        /// <summary>
        /// Gets or sets 手机软件.
        /// </summary>
        protected HandsetSoft Soft { get; set; }

        /// <summary>
        /// 设置手机软件.
        /// </summary>
        /// <param name="handsetSoft">手机软件.</param>
        public void SetHandsetSoft(HandsetSoft handsetSoft)
        {
            this.Soft = handsetSoft;
        }

        /// <summary>
        /// 运行.
        /// </summary>
        public abstract void Run();
    }
}
