﻿// <copyright file="HandsetBrandN.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._22.桥接模式.松耦合的程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// N品牌手机.
    /// </summary>
    public class HandsetBrandN : HandsetBrand
    {
        /// <inheritdoc/>
        public override void Run()
        {
            this.Soft.Run();
        }
    }
}
