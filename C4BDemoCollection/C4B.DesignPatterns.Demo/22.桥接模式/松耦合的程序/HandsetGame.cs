﻿// <copyright file="HandsetGame.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._22.桥接模式.松耦合的程序
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 手机游戏.
    /// </summary>
    public class HandsetGame : HandsetSoft
    {
        /// <inheritdoc/>
        public override void Run()
        {
            SerilogHelper.Instance.Logger.Information("运行手机游戏");
        }
    }
}
