﻿// <copyright file="SingletonWithLock2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._21.单例模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 单例模式的双重锁定方式。
    /// 我们不用让线程每次都加锁，而只是在实例未被创建的时候再加锁处理。同时也能保证多线程的安全。这种做法被称为Double-Check Locking（双重锁定）.
    /// </summary>
    public class SingletonWithLock2
    {
        private static readonly object Obj = new object(); // 程序运行时创建一个静态只读的进程辅助对象
        private static SingletonWithLock2 instance;

        private SingletonWithLock2()
        {
        }

        /// <summary>
        /// 由于有了lock，就保证了多线程环境下的同时访问也不会造成多个实例的生成。但每次调用GetInstance方法时都需要lock，好像不太好吧？
        /// 当instance为null并且同时有两个线程调用GetInstance()方法时，它们将都可以通过第一重instance==null的判断。
        ///   然后由于lock机制，这两个线程则只有一个进入，另一个在外排队等候，必须要其中的一个进入并出来后，另一个才能进入。
        /// 而此时如果没有了第二重的instance是否为null的判断，则第一个线程创建了实例，而第二个线程还是可以继续再创建新的实例，这就没有达到单例的目的。你明白了吗.
        /// </summary>
        /// <returns>实例.</returns>
        public static SingletonWithLock2 GetInstance()
        {
            if (instance == null) // 先判断实例是否存在，不存在再加锁处理
            {
                lock (Obj) // 在同一个时刻加了锁的那部分程序只有一个线程可以进入
                {
                    if (instance == null)
                    {
                        instance = new SingletonWithLock2();
                    }
                }
            }

            return instance;
        }
    }
}