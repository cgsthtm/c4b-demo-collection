﻿// <copyright file="SingletonWithStaticInit.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._21.单例模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 单例模式的静态初始化方式。
    /// 其实在实际应用当中，C#与公共语言运行库也提供了一种‘静态初始化’方法，这种方法不需要开发人员显式地编写线程安全代码，即可解决多线程环境下它是不安全的问题。[MSDN].
    /// 这样的实现与前面的示例类似，也是解决了单例模式试图解决的两个基本问题：全局访问和实例化控制，公共静态属性为访问实例提供了一个全局访问点。不同之处在于它依赖公共语言运行库来初始化变量。
    /// 由于构造方法是私有的，因此不能在类本身以外实例化Singleton类；因此，变量引用的是可以在系统中存在的唯一的实例。
    ///   不过要注意，instance变量标记为readonly，这意味着只能在静态初始化期间或在类构造函数中分配变量[MSDN].
    /// 这种静态初始化的方式是在自己被加载时就将自己实例化，所以被形象地称之为饿汉式单例类，原先的单例模式处理方式是要在第一次被引用时，才会将自己实例化，所以就被称为懒汉式单例类。[J&DP].
    /// 由于饿汉式，即静态初始化的方式，它是类一加载就实例化的对象，所以要提前占用系统资源。然而懒汉式，又会面临着多线程访问的安全性问题，
    ///   需要做双重锁定这样的处理才可以保证安全。所以到底使用哪一种方式，取决于实际的需求。
    /// 从C#语言角度来讲，饿汉式的单例类已经足够满足我们的需求了.
    /// </summary>
    public sealed class SingletonWithStaticInit // sealed阻止发生派生，而派生可能会增加实例
    {
        /// <summary>
        /// 在第一次引用类的任何成员时创建实例。公共语言运行库负责处理变量初始化.
        /// </summary>
        private static readonly SingletonWithStaticInit Instance = new SingletonWithStaticInit();

        private SingletonWithStaticInit()
        {
        }

        /// <summary>
        /// 获取实例.
        /// </summary>
        /// <returns>实例.</returns>
        public static SingletonWithStaticInit GetInstance()
        {
            return Instance;
        }
    }
}