﻿// <copyright file="Singleton.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._21.单例模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 单例模式（Singleton），保证一个类仅有一个实例，并提供一个访问它的全局访问点。[DP].
    /// 通常我们可以让一个全局变量使得一个对象被访问，但它不能防止你实例化多个对象。一个最好的办法就是，让类自身负责保存它的唯一实例。
    ///   这个类可以保证没有其他实例可以被创建，并且它可以提供一个访问该实例的方法。[DP].
    /// 实用类通常也会采用私有化的构造方法来避免其有实例。但它们还是有很多不同的，比如实用类不保存状态，仅提供一些静态方法或静态属性让你使用，而单例类是有状态的。
    ///   实用类不能用于继承多态，而单例虽然实例唯一，却是可以有子类来继承。实用类只不过是一些方法属性的集合，而单例却是有着唯一的对象实例.
    /// </summary>
    public class Singleton
    {
        private static Singleton instance;

        private Singleton() // private私有构造方法，防止外界利用new创建此类实例.
        {
        }

        /// <summary>
        /// 获取实例.
        /// </summary>
        /// <returns>实例对象.</returns>
        public static Singleton GetInstance()
        {
            if (instance == null)
            {
                instance = new Singleton(); // 如果实例不存在，则new一个新实例，否则返回已有的实例
            }

            return instance;
        }
    }
}