﻿// <copyright file="SingletonWithLock.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._21.单例模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 多线程的程序中，多个线程同时，注意是同时访问Singleton类，调用GetInstance()方法，会有可能造成创建多个实例的。
    /// 可以给进程一把锁来处理。这里需要解释一下lock语句的涵义，lock是确保当一个线程位于代码的临界区时，另一个线程不进入临界区。
    ///   如果其他线程试图进入锁定的代码，则它将一直等待（即被阻止），直到该对象被释放。[MSDN].
    /// </summary>
    public class SingletonWithLock
    {
        private static readonly object Obj = new object(); // 程序运行时创建一个静态只读的进程辅助对象
        private static SingletonWithLock instance;

        private SingletonWithLock()
        {
        }

        /// <summary>
        /// 由于有了lock，就保证了多线程环境下的同时访问也不会造成多个实例的生成。但每次调用GetInstance方法时都需要lock，好像不太好吧？.
        ///   这种做法是会影响性能的，可以对这个类再做改良.
        /// </summary>
        /// <returns>实例.</returns>
        public static SingletonWithLock GetInstance()
        {
            lock (Obj) // 在同一个时刻加了锁的那部分程序只有一个线程可以进入
            {
                if (instance == null)
                {
                    instance = new SingletonWithLock();
                }
            }

            return instance;
        }
    }
}