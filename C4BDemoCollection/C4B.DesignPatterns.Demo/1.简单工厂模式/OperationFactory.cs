﻿// <copyright file="OperationFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._1.简单工厂模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 应该考虑用一个单独的类来做这个创造实例的过程，这就是工厂.
    /// </summary>
    public class OperationFactory
    {
        /// <summary>
        /// 创建操作.
        /// </summary>
        /// <param name="operate">指定产品.</param>
        /// <returns>返回具体Operation.</returns>
        public static Operation CreateOperate(string operate)
        {
            Operation oper = null;
            switch (operate)
            {
                case "+":
                    oper = new OperationAdd();
                    break;

                case "-":
                    oper = new OperationSub();
                    break;

                default:
                    break;
            }

            return oper;
        }
    }
}