﻿// <copyright file="OperationAdd.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._1.简单工厂模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 加法操作.
    /// </summary>
    public class OperationAdd : Operation
    {
        /// <inheritdoc/>
        public override double GetResult()
        {
            return this.NumA + this.NumB;
        }
    }
}