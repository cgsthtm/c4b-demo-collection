﻿// <copyright file="Operation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._1.简单工厂模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 操作基类.
    /// </summary>
    public class Operation
    {
        /// <summary>
        /// Gets or sets 操作数A.
        /// </summary>
        public double NumA { get; set; } = 0d;

        /// <summary>
        /// Gets or sets 操作数B.
        /// </summary>
        public double NumB { get; set; } = 0d;

        /// <summary>
        /// 获取操作结果.
        /// </summary>
        /// <returns>操作结果.</returns>
        public virtual double GetResult()
        {
            double result = 0d;
            return result;
        }
    }
}