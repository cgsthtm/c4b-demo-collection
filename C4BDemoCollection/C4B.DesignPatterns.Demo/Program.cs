﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DesignPatterns.Demo
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;
    using C4B.DesignPatterns.Demo._1.简单工厂模式;
    using C4B.DesignPatterns.Demo._10.模板方法模式;
    using C4B.DesignPatterns.Demo._12.外观模式;
    using C4B.DesignPatterns.Demo._13.建造者模式;
    using C4B.DesignPatterns.Demo._14.观察者模式;
    using C4B.DesignPatterns.Demo._15.抽象工厂模式;
    using C4B.DesignPatterns.Demo._15.抽象工厂模式.反射_抽象工厂的数据访问程序;
    using C4B.DesignPatterns.Demo._15.抽象工厂模式.用简单工厂改进抽象工厂;
    using C4B.DesignPatterns.Demo._16.状态模式;
    using C4B.DesignPatterns.Demo._16.状态模式.工作状态函数版;
    using C4B.DesignPatterns.Demo._17.适配器模式;
    using C4B.DesignPatterns.Demo._18.备忘录模式;
    using C4B.DesignPatterns.Demo._19.组合模式;
    using C4B.DesignPatterns.Demo._19.组合模式.组合模式举例;
    using C4B.DesignPatterns.Demo._2.策略模式;
    using C4B.DesignPatterns.Demo._2.策略模式.策略模式;
    using C4B.DesignPatterns.Demo._20.迭代器模式;
    using C4B.DesignPatterns.Demo._21.单例模式;
    using C4B.DesignPatterns.Demo._22.桥接模式;
    using C4B.DesignPatterns.Demo._22.桥接模式.松耦合的程序;
    using C4B.DesignPatterns.Demo._23.命令模式;
    using C4B.DesignPatterns.Demo._24.职责链模式;
    using C4B.DesignPatterns.Demo._25.中介者模式;
    using C4B.DesignPatterns.Demo._26.享元模式;
    using C4B.DesignPatterns.Demo._27.解释器模式;
    using C4B.DesignPatterns.Demo._28.访问者模式;
    using C4B.DesignPatterns.Demo._6.装饰模式;
    using C4B.DesignPatterns.Demo._6.装饰模式.装饰模式另一个举例;
    using C4B.DesignPatterns.Demo._7.代理模式;
    using C4B.DesignPatterns.Demo._8.工厂方法模式;
    using C4B.DesignPatterns.Demo._9.原型模式;
    using Serilog;

    /// <summary>
    /// 主程序.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            var log = new LoggerConfiguration().MinimumLevel.Debug().WriteTo.Console().CreateLogger();

            // 1.简单工厂模式
            Operation oper = OperationFactory.CreateOperate("+");
            oper.NumA = 1;
            oper.NumB = 2;
            double result = oper.GetResult();
            log.Information("1.简单工厂模式，测试结果：{res}", result);

            // 2.策略模式
            // 2.1.策略模式
            _2.策略模式.策略模式.Context strategyContext = new _2.策略模式.策略模式.Context(new ConcreteStrategyA());
            strategyContext.ContextInterface();

            // 2.2.策略模式与简单工厂结合
            CashContext cs = new CashContext("满300减100");
            result = cs.GetResult(900d);
            log.Information("2.策略模式与简单工厂结合，测试结果：{res}", result);

            // 3.单一职责原则

            // 4.开放封闭原则

            // 5.依赖倒转与里氏替换原则

            // 6.装饰模式
            log.Information("6.装饰模式：");
            ConcreteComponent c = new ConcreteComponent();
            ConcreteDecoratorA d1 = new ConcreteDecoratorA();
            ConcreteDecoratorB d2 = new ConcreteDecoratorB();
            d1.SetComponent(c); // 装饰的方法是，首先用d1包装c，再用d2包装d1，最后执行d2的Operation()。所以实际上先执行了c的Operation()，然后执行了d1的Operation()，最后执行了d2的Operation()
            d2.SetComponent(d1);
            d2.Operation();
            Person person = new Person("小芳");
            TShirts shirts = new TShirts();
            BigTrouser bigTrouser = new BigTrouser();
            shirts.Decorate(person);
            bigTrouser.Decorate(shirts);
            bigTrouser.Show();

            // 7.代理模式
            log.Information("7.代理模式：");
            SchoolGirl huahua = new SchoolGirl() { Name = "花花" };
            Proxy proxy = new Proxy(huahua);
            proxy.GiveFlowers();
            proxy.GiveChocolate();
            proxy.GiveDolls();
            _7.代理模式.代理模式.Proxy proxy1 = new _7.代理模式.代理模式.Proxy();
            proxy1.Request();

            // 8.工厂方法模式
            IFactory operateFactory = new AddFactory();
            oper = operateFactory.CreateOperation();
            oper.NumA = 1;
            oper.NumB = 2;
            result = oper.GetResult();
            log.Information("8.工厂方法模式，测试结果：{res}", result);

            // 简单工厂的坏味道：“好，此时你就发现了，你需要在任何实例化的时候写出这个工厂的代码。这里有重复，也就有了坏味道。你再用工厂方法模式写一遍。”
            _8.工厂方法模式.工厂方法举例.LeiFeng leiFengA = _8.工厂方法模式.工厂方法举例.SimpleFactory.CreateLeiFeng("志愿者");
            leiFengA.Wash();
            _8.工厂方法模式.工厂方法举例.LeiFeng leiFengB = _8.工厂方法模式.工厂方法举例.SimpleFactory.CreateLeiFeng("志愿者");
            leiFengB.Sweep();

            // 工厂方法模式
            _8.工厂方法模式.工厂方法举例.IFactory factory1 = new _8.工厂方法模式.工厂方法举例.VolunteerFactory(); // 要换成学雷锋的大学生改这里就可以了
            _8.工厂方法模式.工厂方法举例.LeiFeng leiFengC = factory1.CreateLeiFeng();
            leiFengC.Wash();
            leiFengC.Sweep();

            // 9.原型模式
            ConcretePrototype1 p1 = new ConcretePrototype1("I");
            ConcretePrototype1 c1 = (ConcretePrototype1)p1.Clone(); // 浅复制
            log.Information("9.原型模式，克隆结果：{res}", c1.Id);
            Resume r1 = new Resume("大鸟");
            r1.SetPersonalInfo("男", "29");
            r1.SerWorkExperience("1999-2000", "XX公司");
            Resume r2 = (Resume)r1.Clone(); // 深复制
            r2.SerWorkExperience("2000-2006", "YY公司");
            Resume r3 = (Resume)r1.Clone();
            r3.SerWorkExperience("2006-2022", "ZZ公司");
            r1.Display();
            r2.Display();
            r3.Display();

            // 10.模板方法模式
            // 重复等于易错加难改
            log.Information("10.模板方法模式：未使用模板方法 重复等于易错加难改");
            log.Information("学生甲抄的试卷：");
            _10.模板方法模式.模板方法举例.重复等于易错和难改.TestPaperA testPaperA = new _10.模板方法模式.模板方法举例.重复等于易错和难改.TestPaperA();
            testPaperA.TestQuestion1();
            testPaperA.TestQuestion2();
            testPaperA.TestQuestion3();
            log.Information("学生乙抄的试卷：");
            _10.模板方法模式.模板方法举例.重复等于易错和难改.TestPaperB testPaperB = new _10.模板方法模式.模板方法举例.重复等于易错和难改.TestPaperB();
            testPaperB.TestQuestion1();
            testPaperB.TestQuestion2();
            testPaperB.TestQuestion3();
            log.Information("10.模板方法模式：模板方法举例");
            log.Information("学生甲抄的试卷：");
            _10.模板方法模式.模板方法举例.提炼代码.再提炼.TestPaper stuA = new _10.模板方法模式.模板方法举例.提炼代码.再提炼.TestPaperA();
            stuA.TestQuestion1();
            stuA.TestQuestion2();
            stuA.TestQuestion3();
            log.Information("学生乙抄的试卷：");
            _10.模板方法模式.模板方法举例.提炼代码.再提炼.TestPaper stuB = new _10.模板方法模式.模板方法举例.提炼代码.再提炼.TestPaperB();
            stuB.TestQuestion1();
            stuB.TestQuestion2();
            stuB.TestQuestion3();
            log.Information("10.模板方法模式：");
            AbstractClass ac;
            ac = new ConcreteClassA();
            ac.TemplateMethod();
            ac = new ConcreteClassB();
            ac.TemplateMethod();

            // 11.迪米特法则

            // 12.外观模式
            log.Information("12.外观模式：");
            Facade facade = new Facade(); // 由于Facade的作用，客户端可以根本不知道三个子系统的存在
            facade.MethodA();
            facade.MethodB();

            // 13.建造者模式
            Pen p = new Pen(Color.Yellow);
            Size size = new Size(1024, 768);
            Form form = new Form() { Size = size };
            PictureBox pictureBox1 = new PictureBox() { Size = size };
            Bitmap b = new Bitmap(1024, 768);
            Graphics g = Graphics.FromImage(b);
            PersonBuilder ptb = new PersonThinBuilder(g, p); // 创建瘦子类
            PersonDirector pdThin = new PersonDirector(ptb); // 指挥者
            pdThin.CreatePerson(); // 建造
            pictureBox1.Image = b;
            form.Controls.Add(pictureBox1);
            form.ShowDialog();

            // 使用建造者模式
            log.Information("13.建造者模式：");
            Director director = new Director();
            Builder b1 = new ConcreteBuilder1();
            Builder b2 = new ConcreteBuilder2();
            director.Construct(b1); // 客户不需知道具体的建造过程
            Product pd1 = b1.GetResult(); // 指挥者用ConcreteBuilder1的方法来建造产品
            pd1.Show();
            director.Construct(b2);
            Product pd2 = b2.GetResult(); // 指挥者用ConcreteBuilder2的方法来建造产品
            pd2.Show();

            // 14.观察者模式
            log.Information("14.观察者模式：未使用观察者模式-双向耦合的代码");
            _14.观察者模式.双向耦合的代码.Secretary secretary = new _14.观察者模式.双向耦合的代码.Secretary();
            _14.观察者模式.双向耦合的代码.StockObserver observer1 = new _14.观察者模式.双向耦合的代码.StockObserver("ob1", secretary);
            _14.观察者模式.双向耦合的代码.StockObserver observer2 = new _14.观察者模式.双向耦合的代码.StockObserver("ob2", secretary);
            secretary.Attach(observer1);
            secretary.Attach(observer2);
            secretary.SecretaryAction = "老板回来了！";
            secretary.Notify();
            log.Information("14.观察者模式：未使用观察者模式-解耦实践2");
            _14.观察者模式.双向耦合的代码.解耦实践2.Boss boss1 = new _14.观察者模式.双向耦合的代码.解耦实践2.Boss();
            _14.观察者模式.双向耦合的代码.解耦实践2.NBAObserver ob1 = new _14.观察者模式.双向耦合的代码.解耦实践2.NBAObserver("ob1", boss1);
            _14.观察者模式.双向耦合的代码.解耦实践2.StockObserver ob2 = new _14.观察者模式.双向耦合的代码.解耦实践2.StockObserver("ob2", boss1);
            boss1.Attach(ob1);
            boss1.Attach(ob2);
            boss1.SubjectState = "我回来了！";
            boss1.Nofity();
            log.Information("14.观察者模式：");
            ConcreteSubject concreteSubject = new ConcreteSubject();
            concreteSubject.Attach(new ConcreteObserver(concreteSubject, "X"));
            concreteSubject.Attach(new ConcreteObserver(concreteSubject, "Y"));
            concreteSubject.Attach(new ConcreteObserver(concreteSubject, "Z"));
            concreteSubject.SubjectState = "ABC";
            concreteSubject.Notify();
            concreteSubject.SubjectState = "123";
            concreteSubject.Notify();
            Boss boss = new Boss();
            StockObserver colleague1 = new StockObserver("张三", boss);
            NBAObserver colleague2 = new NBAObserver("李四", boss);
            boss.Update += colleague1.CloseStockMarket; // 委托，将不同类的不同方法委托给Update.
            boss.Update += colleague2.CloseNBADirectSeeding;
            boss.SubjectState = "我胡汉三回来了！";
            boss.Notify(); // 通知

            // 15.抽象工厂模式
            log.Information("15.抽象工厂模式：未使用抽象工厂-最基本的数据访问程序");
            _15.抽象工厂模式.最基本的数据访问程序.User user1 = new _15.抽象工厂模式.最基本的数据访问程序.User() { ID = 6, Name = "cgs1" };
            _15.抽象工厂模式.最基本的数据访问程序.SqlserverUser sqlserverUser = new _15.抽象工厂模式.最基本的数据访问程序.SqlserverUser();
            sqlserverUser.Insert(user1);     // 这里之所以不能换数据库，就是因为与SQL Server耦合，sqlserverUser被框死在SQL Server上了
            sqlserverUser.GetUser(user1.ID); // 如果这里是灵活的（多态的），那么在执行Insert和GetUser时就不用考虑是在用SQLServer还是Access了
            log.Information("15.抽象工厂模式：未使用抽象工厂-用工厂方法的数据库访问程序");
            User user = new User();
            IDbFactory factory = new SqlServerFactory(); // 若要改成Access数据库，只需要改成IDbFactory factory = new AccessFactory();
            IUser iu = factory.CreateUser();
            iu.Insert(user);
            iu.GetUser(1);
            log.Information("15.抽象工厂模式：用了抽象工厂的数据库访问程序");
            _15.抽象工厂模式.用了抽象工厂的数据库访问程序.IFactory factory2 = new _15.抽象工厂模式.用了抽象工厂的数据库访问程序.AccessFactory();
            _15.抽象工厂模式.用了抽象工厂的数据库访问程序.IDepartment iDepartment = factory2.CreateDepartment();
            iDepartment.Insert(new _15.抽象工厂模式.用了抽象工厂的数据库访问程序.Department());
            iDepartment.GetDepartment(6);
            log.Information("15.抽象工厂模式--用简单工厂改进抽象工厂：");
            iu = DataAccess.CreateUser();
            iu.Insert(user);
            iu.GetUser(1);
            log.Information("15.抽象工厂模式--反射+抽象工厂的数据访问程序：");
            iu = DataAccessWithReflection.CreateUser();
            iu.Insert(user);
            iu.GetUser(1);

            // 16.状态模式
            log.Information("16.状态模式：未使用状态模式-函数版");
            WorkStateFunctionVersion.Hour = 12;
            WorkStateFunctionVersion.WorkFinished = false;
            WorkStateFunctionVersion.WriteProgram();
            log.Information("16.状态模式：");
            _16.状态模式.Context ct = new _16.状态模式.Context(new ConcreteStateA()); // 设置Context的初始状态为ConcreteStateA
            ct.Request(); // 不断地请求，同时更改状态
            ct.Request();
            ct.Request();
            ct.Request();
            Work emergencyProjects = new Work();
            emergencyProjects.Hour = 9;
            emergencyProjects.WriteProgram();
            emergencyProjects.Hour = 10;
            emergencyProjects.WriteProgram();
            emergencyProjects.Hour = 12;
            emergencyProjects.WriteProgram();
            emergencyProjects.Hour = 13;
            emergencyProjects.WriteProgram();
            emergencyProjects.Hour = 14;
            emergencyProjects.WriteProgram();
            emergencyProjects.Hour = 17;
            emergencyProjects.WriteProgram();
            emergencyProjects.TaskFinished = false;
            emergencyProjects.WriteProgram();
            emergencyProjects.Hour = 19;
            emergencyProjects.WriteProgram();
            emergencyProjects.Hour = 22;
            emergencyProjects.WriteProgram();

            // 17.适配器模式
            log.Information("17.适配器模式：");
            Target target = new Adapter();
            target.Request(); // 对客户端来说就是调用的Target的Request()
            Player cp = new CenterPlayer("保罗");
            cp.Attack();
            cp.Defense();
            Player fcp = new Translator("姚明");
            fcp.Attack();
            fcp.Defense();

            // 18.备忘录模式
            log.Information("18.备忘录模式：");
            Originator originator = new Originator();
            originator.State = "On"; // Originator初始状态，状态属性为“On”
            originator.Show();
            Caretaker caretaker = new Caretaker();
            caretaker.Memento = originator.CreateMemento(); // 保存状态时，由于有了很好的封装，可以隐藏Originator的实现细节
            originator.State = "Off"; // Originator改变了状态属性为“Off”
            originator.Show();
            originator.SetMemento(caretaker.Memento); // 恢复初始状态
            originator.Show();

            // 19.组合模式
            log.Information("19.组合模式：");

            // 通过Component接口操作组合部件的对象
            Composite root = new Composite("root"); // 生成树根root，根上长出两个叶节点LeafA和LeafB
            root.Add(new Leaf("Leaf A"));
            root.Add(new Leaf("Leaf B"));
            Composite comp = new Composite("Composite X"); // 根上长出分枝Composite X，分枝上也有两个叶节点LeafXA和LeafXB
            comp.Add(new Leaf("Leaf XA"));
            comp.Add(new Leaf("Leaf XB"));
            root.Add(comp);
            Composite comp2 = new Composite("Composite XY"); // 在Composite X上再长出分枝Composite XY，分枝上也有两个叶节点LeafXYA和LeafXYB
            comp2.Add(new Leaf("Leaf XYA"));
            comp2.Add(new Leaf("Leaf XYB"));
            comp.Add(comp2);
            root.Add(new Leaf("Leaf C")); // 根部又长出两个叶节点LeafC和LeafD
            Leaf leafD = new Leaf("Leaf D");
            root.Add(leafD);
            root.Remove(leafD); // LeafD这片叶子被风吹跑了
            root.Display(1); // 显示大树的样子
            log.Information("19.组合模式：举例-结构图");
            ConcreteCompany concreteCompany = new ConcreteCompany("北京总公司");
            concreteCompany.Add(new HRDepartment("总共公司人力资源部"));
            concreteCompany.Add(new FinanceDepartment("总共公司财务部"));
            ConcreteCompany concreteCompany1 = new ConcreteCompany("上海华东分公司");
            concreteCompany1.Add(new HRDepartment("华东分公司人力资源部"));
            concreteCompany1.Add(new FinanceDepartment("华东分公司财务部"));
            concreteCompany.Add(concreteCompany1);
            ConcreteCompany concreteCompany2 = new ConcreteCompany("南京办事处");
            concreteCompany2.Add(new HRDepartment("南京办事处人力资源部"));
            concreteCompany2.Add(new FinanceDepartment("南京办事处财务部"));
            concreteCompany1.Add(concreteCompany2);
            concreteCompany.Display(1);
            log.Information("19.组合模式：举例-职责");
            concreteCompany.LineOfDuty();

            // 20.迭代器模式
            log.Information("20.迭代器模式：");
            ConcreteAggregate a = new ConcreteAggregate();
            a[0] = "cgs";
            a[1] = "hh";
            a[2] = "ds";
            a[3] = "xc";
            Iterator i = new ConcreteIterator(a); // 声明迭代器
            object item = i.First(); // 聚集的第一个对象
            while (!i.IsDone())
            {
                log.Information("{0}", i.CurrentItem());
                i.Next(); // 移动迭代器指针
            }

            // 21.单例模式
            log.Information("21.单例模式：");
            Singleton s1 = Singleton.GetInstance();
            Singleton s2 = Singleton.GetInstance();
            log.Information("s1==s2 : {0}", s1 == s2);
            SingletonWithStaticInit s3 = SingletonWithStaticInit.GetInstance();
            SingletonWithStaticInit s4 = SingletonWithStaticInit.GetInstance();
            log.Information("s3==s4 : {0}", s3 == s4);

            // 22.桥接模式
            log.Information("22.桥接模式：松耦合的程序");
            HandsetBrand handsetBrand = new HandsetBrandN();
            handsetBrand.SetHandsetSoft(new HandsetGame());
            handsetBrand.Run();
            handsetBrand.SetHandsetSoft(new HandsetAddressList());
            handsetBrand.Run();
            handsetBrand = new HandsetBrandM();
            handsetBrand.SetHandsetSoft(new HandsetGame());
            handsetBrand.Run();
            handsetBrand.SetHandsetSoft(new HandsetAddressList());
            handsetBrand.Run();
            log.Information("22.桥接模式：");
            Abstraction ab = new RefinedAbstraction();
            ab.SetImplementor(new ConcreteImplementorA());
            ab.Operation();
            ab.SetImplementor(new ConcreteImplementorB());
            ab.Operation();

            // 23.命令模式
            log.Information("23.命令模式：");
            Barbecuer boy = new Barbecuer(); // 开店前的准备
            Command bakeMuttonCmd1 = new BakeMuttonCommand(boy);
            Command bakeMuttonCmd2 = new BakeMuttonCommand(boy);
            Command bakeChickenWingCmd1 = new BakeChickenWing(boy);
            Waiter girl = new Waiter();
            girl.SetOrder(bakeMuttonCmd1); // 开门营业，顾客点菜
            girl.SetOrder(bakeMuttonCmd2);
            girl.SetOrder(bakeChickenWingCmd1);
            girl.Notify(); // 点菜完毕，通知厨房；订单下好后，一次性通知
            log.Information("23.命令模式：总结");
            _23.命令模式.总结.Receiver receiver1 = new _23.命令模式.总结.Receiver();
            _23.命令模式.总结.Command command1 = new _23.命令模式.总结.ConcreteCommand(receiver1);
            _23.命令模式.总结.Invoker invoker1 = new _23.命令模式.总结.Invoker();
            invoker1.SetCommand(command1);
            invoker1.ExecuteCommand();

            // 24.职责链模式
            log.Information("24.职责链模式：");
            Handler h1 = new ConcreteHandler1();
            Handler h2 = new ConcreteHandler2();
            Handler h3 = new ConcreteHandler3();
            h1.SetSuccessor(h2);
            h2.SetSuccessor(h3); // 设置职责链
            int[] requests = { 2, 5, 14, 22, 18, 3, 27, 20 };
            foreach (int request in requests) // 循环给最小权限者提交请求，不同的数额，由不同权限处理者处理
            {
                h1.HandleRequest(request);
            }

            // 25.中介者模式
            log.Information("25.中介者模式：");
            ConcreteMediator m = new ConcreteMediator();
            ConcreteColleague1 zc1 = new ConcreteColleague1(m); // 让两个具体同事对象认识中介者对象
            ConcreteColleague2 zc2 = new ConcreteColleague2(m);
            m.C1 = zc1;
            m.C2 = zc2; // 让中介者认识各个具体同事对象类
            zc1.Send("吃饭了吗？");
            zc2.Send("没有呢，你打算请客？"); // 具体同事对象的发送消息都是由中介者对象完成的

            // 26.享元模式
            log.Information("26.享元模式：");
            int extrinsicstate = 22; // 代码外部状态
            FlyweightFactory f = new FlyweightFactory();
            Flyweight fx = f.GetFlyweight("X");
            fx.Operation(--extrinsicstate);
            Flyweight fy = f.GetFlyweight("Y");
            fy.Operation(--extrinsicstate);
            Flyweight fz = f.GetFlyweight("Z");
            fz.Operation(--extrinsicstate);
            Flyweight uf = new UnsharedConcreteFlyweight();
            uf.Operation(--extrinsicstate);

            // 27.解释器模式
            log.Information("27.解释器模式：");
            _27.解释器模式.Context context = new _27.解释器模式.Context();
            IList<AbstractExpression> list = new List<AbstractExpression>();
            list.Add(new TerminalExpression()); // 构建表示该文法定义的语言中一个特定的句子的抽象语法树。
            list.Add(new NonTerminalExpression());
            list.Add(new TerminalExpression());
            list.Add(new TerminalExpression());
            foreach (AbstractExpression exp in list)
            {
                exp.Interpret(context);
            }

            // 28.访问者模式
            log.Information("28.访问者模式：");
            ObjectStructure o = new ObjectStructure();
            o.Attach(new ConcreteElementA());
            o.Attach(new ConcreteElementB());
            ConcreteVisitor1 v1 = new ConcreteVisitor1();
            ConcreteVisitor2 v2 = new ConcreteVisitor2();
            o.Accept(v1);
            o.Accept(v2);

            Console.ReadLine();
        }
    }
}