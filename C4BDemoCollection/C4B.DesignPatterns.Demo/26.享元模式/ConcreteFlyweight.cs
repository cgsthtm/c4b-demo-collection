﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.DesignPatterns.Demo._26.享元模式
{
    /// <summary>
    /// ConcreteFlyweight是继承Flyweight超类或实现Flyweight接口，并为内部状态增加存储空间。
    /// </summary>
    public class ConcreteFlyweight : Flyweight
    {
        public override void Operation(int extrinsicstate)
        {
            SerilogHelper.Instance.Logger.Information("具体Flyweight：{0}", extrinsicstate);
        }
    }
}
