﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.DesignPatterns.Demo._25.中介者模式
{
    /// <summary>
    /// 具体同事类2
    /// </summary>
    public class ConcreteColleague2 : Colleague
    {
        public ConcreteColleague2(Mediator mediator) : base(mediator)
        {
        }

        public void Send(string message)
        {
            mediator.Send(message, this); // 发送消息时，通常是中介者发出去的
        }

        public void Notify(string message)
        {
            SerilogHelper.Instance.Logger.Information("同事2得到消息：{0}", message);
        }
    }
}
