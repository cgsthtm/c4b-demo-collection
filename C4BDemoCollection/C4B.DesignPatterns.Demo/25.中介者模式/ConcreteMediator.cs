﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.DesignPatterns.Demo._25.中介者模式
{
    /// <summary>
    /// 中介者模式（Mediator），用一个中介对象来封装一系列的对象交互。中介者使各对象不需要显式地相互引用，从而使其耦合松散，而且可以独立地改变它们之间的交互。[DP]
    /// 中介者模式很容易在系统中应用，也很容易在系统中误用。当系统出现了‘多对多’交互复杂的对象群时，不要急于使用中介者模式，而要先反思你的系统在设计上是不是合理。
    /// 中介者模式的优点首先是Mediator的出现减少了各个Colleague的耦合，使得可以独立地改变和复用各个Colleague类和Mediator，比如任何国家的改变不会影响到其他国家，而只是与安理会发生变化。
    /// 具体中介者类ConcreteMediator可能会因为ConcreteColleague的越来越多，而变得非常复杂，反而不容易维护了。
    /// 由于ConcreteMediator控制了集中化，于是就把交互复杂性变为了中介者的复杂性，这就使得中介者会变得比任何一个ConcreteColleague都复杂。
    /// 事实上，联合国安理会秘书长的工作应该是非常繁忙的，谁叫他就是‘全球最大的官’呢。也正因为此，中介者模式的优点来自集中控制，其缺点也是它，使用时是要考虑清楚哦。
    /// “中介者模式一般应用于一组对象以定义良好但是复杂的方式进行通信的场合，比如刚才得到的窗体Form对象或Web页面aspx，以及想定制一个分布在多个类中的行为，而又不想生成太多的子类的场合。
    /// 具体中介者类
    /// </summary>
    public class ConcreteMediator : Mediator
    {
        private ConcreteColleague1 c1; // 需要了解所有的具体同事对象
        private ConcreteColleague2 c2;
        public ConcreteColleague1 C1
        {
            set { c1 = value; }
        }
        public ConcreteColleague2 C2
        {
            set { c2 = value; }
        }

        public override void Send(string message, Colleague colleague) // 重写发送消息方法，根据对象做选择判断，通知对象
        {
            if (colleague == c1)
            {
                c2.Notify(message);
            }
            else
            {
                c1.Notify(message);
            }
        }
    }
}
