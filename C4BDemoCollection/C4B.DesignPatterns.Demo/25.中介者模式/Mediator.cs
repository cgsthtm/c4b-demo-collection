﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.DesignPatterns.Demo._25.中介者模式
{
    /// <summary>
    /// 中介者模式（Mediator），用一个中介对象来封装一系列的对象交互。中介者使各对象不需要显式地相互引用，从而使其耦合松散，而且可以独立地改变它们之间的交互。[DP]
    /// 抽象中介者类
    /// </summary>
    public abstract class Mediator
    {
        public abstract void Send(string message, Colleague colleague); // 定义一个抽象的发送消息方法，得到同事对象和发送消息
    }
}
