﻿// <copyright file="WorkStateClassificationVersion.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._16.状态模式.工作状态分类版
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 工作状态分类版.
    /// </summary>
    public class WorkStateClassificationVersion
    {
        /// <summary>
        /// Gets or sets 小时.
        /// </summary>
        public int Hour { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether 工作完成.
        /// </summary>
        public bool WorkFinished { get; set; }

        /// <summary>
        /// 写程序.面向过程的代码.
        /// MartinFowler曾在《重构》中写过一个很重要的代码坏味道，叫做‘Long Method’，方法如果过长其实极有可能是有坏味道了.
        /// </summary>
        public void WriteProgram() // 定义一个写程序的函数，用来根据时间的不同体现不同的工作状态
        {
            if (this.Hour < 12)
            {
                SerilogHelper.Instance.Logger.Information($"当前时间：{this.Hour}点 上午工作，精神百倍");
            }
            else if (this.Hour < 13)
            {
                SerilogHelper.Instance.Logger.Information($"当前时间：{this.Hour}点 饿了，午饭，犯困，午休");
            }
            else if (this.Hour < 17)
            {
                SerilogHelper.Instance.Logger.Information($"当前时间：{this.Hour}点 下午状态还不错，继续努力");
            }
            else
            {
                if (this.WorkFinished)
                {
                    SerilogHelper.Instance.Logger.Information($"当前时间：{this.Hour}点 下班回家");
                }
                else
                {
                    if (this.Hour < 21)
                    {
                        SerilogHelper.Instance.Logger.Information($"当前时间：{this.Hour}点 加班哦，疲累至极");
                    }
                    else
                    {
                        SerilogHelper.Instance.Logger.Information($"当前时间：{this.Hour}点 不行了，快屎了");
                    }
                }
            }
        }
    }
}
