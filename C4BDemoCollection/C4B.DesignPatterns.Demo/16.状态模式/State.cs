﻿// <copyright file="State.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._16.状态模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// State类，抽象状态类，定义一个接口以封装与Context的一个特定状态相关的行为。
    /// MartinFowler曾在《重构》中写过一个很重要的代码坏味道，叫做‘Long Method’，方法如果过长其实极有可能是有坏味道了。
    /// 你要知道，你这个方法很长，而且有很多的判断分支，这也就意味着它的责任过大了。无论是任何状态，都需要通过它来改变，这实际上是很糟糕的。
    /// 面向对象设计其实就是希望做到代码的责任分解。这个类违背了‘单一职责原则’。但如何做呢？
    /// 状态模式（State），当一个对象的内在状态改变时允许改变其行为，这个对象看起来像是改变了其类。[DP]
    /// 状态模式主要解决的是当控制一个对象状态转换的条件表达式过于复杂时的情况。把状态的判断逻辑转移到表示不同状态的一系列类当中，可以把复杂的判断逻辑简化。
    /// 状态模式的好处是将与特定状态相关的行为局部化，并且将不同状态的行为分割开来[DP]
    /// 将特定的状态相关的行为都放入一个对象中，由于所有与状态相关的代码都存在于某个ConcreteState中，所以通过定义新的子类可以很容易地增加新的状态和转换[DP]
    /// 这样做的目的就是为了消除庞大的条件分支语句，大的分支判断会使得它们难以修改和扩展，就像我们最早说的刻版印刷一样，任何改动和变化都是致命的。
    /// 状态模式通过把各种状态转移逻辑分布到State的子类之间，来减少相互间的依赖，好比把整个版面改成了一个又一个的活字，此时就容易维护和扩展了.
    /// </summary>
    public abstract class State
    {
        /// <summary>
        /// 定义一个接口以封装与Context的一个特定状态相关的行为.
        /// </summary>
        /// <param name="context">上下文.</param>
        public abstract void Handle(Context context);
    }
}