﻿// <copyright file="SleepingState.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._16.状态模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 睡觉工作状态类.
    /// </summary>
    public class SleepingState : WorkState
    {
        /// <inheritdoc/>
        public override void WriteProgram(Work w)
        {
            SerilogHelper.Instance.Logger.Information("当前时间：{0}点 不行了，睡了睡了", w.Hour);
        }
    }
}
