﻿// <copyright file="EveningState.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._16.状态模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 晚上工作状态类.
    /// </summary>
    public class EveningState : WorkState
    {
        /// <inheritdoc/>
        public override void WriteProgram(Work w)
        {
            if (w.TaskFinished)
            {
                w.SetState(new RestState()); // 如果工作完成，转入休息工作状态
                w.WriteProgram();
            }
            else
            {
                if (w.Hour < 21)
                {
                    SerilogHelper.Instance.Logger.Information("当前时间：{0}点 加班哦，疲累至极", w.Hour);
                }
                else
                {
                    w.SetState(new SleepingState()); // 超过21点，转入睡眠工作状态
                    w.WriteProgram();
                }
            }
        }
    }
}