﻿// <copyright file="NoonState.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._16.状态模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 中午工作状态类.
    /// </summary>
    public class NoonState : WorkState
    {
        /// <inheritdoc/>
        public override void WriteProgram(Work w)
        {
            if (w.Hour < 13)
            {
                SerilogHelper.Instance.Logger.Information("当前时间：{0}点 饿了，午饭；犯困，午休。", w.Hour);
            }
            else
            {
                w.SetState(new AfternoonState()); // 超过13点，则转入下午工作状态
                w.WriteProgram();
            }
        }
    }
}