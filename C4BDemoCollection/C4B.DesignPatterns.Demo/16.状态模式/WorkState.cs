﻿// <copyright file="WorkState.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._16.状态模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 抽象状态类.
    /// </summary>
    public abstract class WorkState
    {
        /// <summary>
        /// 定义一个抽象方法“写程序”.
        /// </summary>
        /// <param name="w">工作类.</param>
        public abstract void WriteProgram(Work w);
    }
}