﻿// <copyright file="AfternoonState.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._16.状态模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 下午工作状态类.
    /// </summary>
    public class AfternoonState : WorkState
    {
        /// <inheritdoc/>
        public override void WriteProgram(Work w)
        {
            if (w.Hour < 17)
            {
                SerilogHelper.Instance.Logger.Information("当前时间：{0}点 下午状态还不错，继续努力", w.Hour);
            }
            else
            {
                w.SetState(new EveningState()); // 超过17点，则转入傍晚工作状态
                w.WriteProgram();
            }
        }
    }
}