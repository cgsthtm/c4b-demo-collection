﻿// <copyright file="ForenoonState.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._16.状态模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 上午工作状态类.
    /// </summary>
    public class ForenoonState : WorkState
    {
        /// <inheritdoc/>
        public override void WriteProgram(Work w)
        {
            if (w.Hour < 12)
            {
                SerilogHelper.Instance.Logger.Information("当前时间：{0}点 上午工作，精神百倍", w.Hour);
            }
            else
            {
                w.SetState(new NoonState()); // 超过12点，则转入中午工作状态
                w.WriteProgram();
            }
        }
    }
}