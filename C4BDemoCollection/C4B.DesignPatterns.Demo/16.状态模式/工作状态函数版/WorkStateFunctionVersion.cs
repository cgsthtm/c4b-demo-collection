﻿// <copyright file="WorkStateFunctionVersion.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._16.状态模式.工作状态函数版
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 工作状态函数版.面向过程的代码.
    /// </summary>
    public class WorkStateFunctionVersion
    {
        /// <summary>
        /// Gets or sets 小时.
        /// </summary>
        public static int Hour { get; set; } = 0;

        /// <summary>
        /// Gets or sets a value indicating whether 任务标记完成.
        /// </summary>
        public static bool WorkFinished { get; set; } = false;

        /// <summary>
        /// 写程序.面向过程的代码.
        /// </summary>
        public static void WriteProgram() // 定义一个写程序的函数，用来根据时间的不同体现不同的工作状态
        {
            if (Hour < 12)
            {
                SerilogHelper.Instance.Logger.Information($"当前时间：{Hour}点 上午工作，精神百倍");
            }
            else if (Hour < 13)
            {
                SerilogHelper.Instance.Logger.Information($"当前时间：{Hour}点 饿了，午饭，犯困，午休");
            }
            else if (Hour < 17)
            {
                SerilogHelper.Instance.Logger.Information($"当前时间：{Hour}点 下午状态还不错，继续努力");
            }
            else
            {
                if (WorkFinished)
                {
                    SerilogHelper.Instance.Logger.Information($"当前时间：{Hour}点 下班回家");
                }
                else
                {
                    if (Hour < 21)
                    {
                        SerilogHelper.Instance.Logger.Information($"当前时间：{Hour}点 加班哦，疲累至极");
                    }
                    else
                    {
                        SerilogHelper.Instance.Logger.Information($"当前时间：{Hour}点 不行了，快屎了");
                    }
                }
            }
        }
    }
}