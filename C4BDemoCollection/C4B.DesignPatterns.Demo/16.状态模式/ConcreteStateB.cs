﻿// <copyright file="ConcreteStateB.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._16.状态模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 具体状态B.每一个子类实现一个与Context的一个状态相关的行为.
    /// </summary>
    public class ConcreteStateB : State
    {
        /// <inheritdoc/>
        public override void Handle(Context context)
        {
            context.State = new ConcreteStateA(); // 设置ConcreteStateB的下一状态是ConcreteStateA
        }
    }
}