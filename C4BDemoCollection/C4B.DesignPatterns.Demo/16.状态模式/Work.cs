﻿// <copyright file="Work.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._16.状态模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 工作类.
    /// </summary>
    public class Work
    {
        private WorkState current;

        /// <summary>
        /// Initializes a new instance of the <see cref="Work"/> class.
        /// </summary>
        public Work()
        {
            this.current = new ForenoonState(); // 工作初始化为上午工作状态，即上午9点开始上班
        }

        /// <summary>
        /// Gets or sets 小时.
        /// </summary>
        public double Hour { get; set; } // 钟点属性，状态转换的依据

        /// <summary>
        /// Gets or sets a value indicating whether 工作完成.
        /// </summary>
        public bool TaskFinished { get; set; } // 任务完成属性，是否能下班的依据

        /// <summary>
        /// 设置状态.
        /// </summary>
        /// <param name="s">新的状态.</param>
        public void SetState(WorkState s)
        {
            this.current = s;
        }

        /// <summary>
        /// 调用具体状态的函数.
        /// </summary>
        public void WriteProgram()
        {
            this.current.WriteProgram(this);
        }
    }
}