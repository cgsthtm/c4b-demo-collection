﻿// <copyright file="Context.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._16.状态模式
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// Context类，维护一个ConcreteState子类的实例，这个实例定义当前的状态.
    /// </summary>
    public class Context
    {
        private State state;

        /// <summary>
        /// Initializes a new instance of the <see cref="Context"/> class.
        /// </summary>
        /// <param name="state">具体状态.</param>
        public Context(State state) // 定义Context的初始状态
        {
            this.state = state;
        }

        /// <summary>
        /// Gets or sets 可读写的状态属性，用于读取当前状态和设置新状态.
        /// </summary>
        public State State
        {
            get
            {
                return this.state;
            }

            set
            {
                this.state = value;
                SerilogHelper.Instance.Logger.Information("当前状态：{0}", this.state.GetType().Name);
            }
        }

        /// <summary>
        /// 对请求做处理，并设置下一状态.
        /// </summary>
        public void Request()
        {
            this.state.Handle(this);
        }
    }
}