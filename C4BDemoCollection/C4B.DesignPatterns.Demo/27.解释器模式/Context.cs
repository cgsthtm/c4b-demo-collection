﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.DesignPatterns.Demo._27.解释器模式
{
    /// <summary>
    /// Context，包含解释器之外的一些全局信息。
    /// </summary>
    public class Context
    {
        public string Input { get; set; }
        public string Output { get; set; }
    }
}
