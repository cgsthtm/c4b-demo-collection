﻿// <copyright file="OrderProcessor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._0.类图关系.依赖关系Dependent.方法参数依赖
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 订单处理器.
    /// </summary>
    public class OrderProcessor
    {
        /* 方法参数依赖
         * 在这个例子中，OrderProcessor类在处理订单时依赖于PaymentService来完成支付操作。这种依赖是通过ProcessOrder方法的参数传递实现的。
         * 依赖关系在类图中用实心三角形和一条虚线表示。
         */

        /// <summary>
        /// 处理订单.
        /// </summary>
        /// <param name="order">订单.</param>
        /// <param name="paymentService">支付服务.</param>
        public void ProcessOrder(Order order, PaymentService paymentService)
        {
            // 使用paymentService处理订单支付
            paymentService.ProcessPayment(order.TotalAmount);

            // ...其他处理逻辑
        }
    }
}
