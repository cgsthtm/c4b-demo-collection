﻿// <copyright file="Order.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._0.类图关系.依赖关系Dependent.方法参数依赖
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 订单类.
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Gets or sets 总金额.
        /// </summary>
        public decimal TotalAmount { get; set; }
    }
}
