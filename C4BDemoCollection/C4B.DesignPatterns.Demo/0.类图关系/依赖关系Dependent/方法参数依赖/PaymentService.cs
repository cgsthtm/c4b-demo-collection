﻿// <copyright file="PaymentService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._0.类图关系.依赖关系Dependent.方法参数依赖
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 支付服务类.
    /// </summary>
    public class PaymentService
    {
        /// <summary>
        /// 处理支付.
        /// </summary>
        /// <param name="amount">金额.</param>
        public void ProcessPayment(decimal amount)
        {
            // 支付处理逻辑
        }
    }
}
