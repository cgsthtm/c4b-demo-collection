﻿// <copyright file="MailSender.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._0.类图关系.依赖关系Dependent.属性依赖
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System.Net.Mail;

    /// <summary>
    /// 邮件发送类.
    /// </summary>
    public class MailSender
    {
        /* 属性依赖
         * 这个例子展示了构造函数注入的方式，MailSender类依赖于SmtpClient来发送邮件，这种依赖关系在类实例化时通过构造函数传入。
         */
        private readonly SmtpClient smtpClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="MailSender"/> class.
        /// </summary>
        /// <param name="smtpClient">smtp客户端.</param>
        public MailSender(SmtpClient smtpClient)
        {
            this.smtpClient = smtpClient;
        }

        /// <summary>
        /// 发送邮件.
        /// </summary>
        /// <param name="from">发送方.</param>
        /// <param name="to">接收方.</param>
        /// <param name="subject">主题.</param>
        /// <param name="body">内容.</param>
        public void SendMail(string from, string to, string subject, string body)
        {
            // 使用_smtpClient发送邮件
            this.smtpClient.Send(from, to, subject, body);
        }
    }
}
