﻿// <copyright file="Dog.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._0.类图关系.泛化继承关系Inheritance
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 狗类.
    /// </summary>
    public class Dog : Animal
    {
        /* 泛化（Inheritance）- 继承
         * 泛化是面向对象编程中的一个基本概念，表示类之间的“is-a”关系。在C#中，通过关键字:来实现继承。
         * 在这个例子中，Dog类继承自Animal类，表明狗是一种动物。
         * 泛化继承关系在类图中用空心三角形和一条实线表示。
         */
    }
}
