﻿// <copyright file="Course.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._0.类图关系.关联关系Association.聚合关系Aggregation
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System.Collections.Generic;

    /// <summary>
    /// 课程类.
    /// </summary>
    public class Course
    {
        /* 聚合（Aggregation）
         * 聚合也是关联的一种，但它表示的是整体与部分之间较弱的拥有关系，部分可以独立于整体存在。在C#中，聚合的表示方式与组合类似，但设计上应明确部分可以有自己的生命周期。
         * 在这个例子中，一个Course可以有多个Student，学生可以注册不同的课程，学生和课程都可以独立存在。
         * 聚合关系在类图中用空心菱形和一条实线表示。
         */

        /// <summary>
        /// Gets or sets 学生列表.
        /// </summary>
        public List<Student> Students { get; set; }
    }
}
