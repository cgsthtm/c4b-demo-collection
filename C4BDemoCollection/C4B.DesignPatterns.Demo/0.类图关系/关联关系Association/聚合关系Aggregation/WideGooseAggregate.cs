﻿// <copyright file="WideGooseAggregate.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._0.类图关系.关联关系Association.聚合关系Aggregation
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 雁群类.
    /// </summary>
    public class WideGooseAggregate
    {
        /// <summary>
        /// 雁群与大雁存在聚合关系.
        /// 在WideGooseAggregate雁群类中，有大雁数组对象arrayWideGeese.
        /// </summary>
        private WideGoose[] arrayWideGeese;
    }
}
