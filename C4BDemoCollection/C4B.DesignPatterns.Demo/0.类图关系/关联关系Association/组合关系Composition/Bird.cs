﻿// <copyright file="Bird.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DesignPatterns.Demo._0.类图关系.关联关系Association.组合关系Composition
{
    /// <summary>
    /// 鸟类.
    /// </summary>
    public class Bird
    {
        /* 鸟类Bird和翅膀类Wing存在组合关系。
         * 在鸟Bird类中，初始化时，实例化翅膀Wing，他们之间同时生成。
         */

        private Wing wing;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bird"/> class.
        /// </summary>
        /// <param name="wing">翅膀.</param>
        public Bird(Wing wing)
        {
            this.wing = wing;
        }
    }
}
