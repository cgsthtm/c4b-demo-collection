﻿// <copyright file="Company.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._0.类图关系.关联关系Association.组合关系Composition
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System.Collections.Generic;

    /// <summary>
    /// 公司类.
    /// </summary>
    public class Company
    {
        /* 组合（Composition）
         * 组合是一种强关联形式，表示整体拥有部分，并且部分的生命期依赖于整体。在C#中，同样通过包含另一个类的实例来实现，但强调了生命周期的依赖。
         * 在这个例子中，Company类包含了Employee对象的列表，员工的生命期与公司紧密相关联，如果公司不存在，员工对象也失去了存在的意义。
         * 组合关系在类图中用实心菱形和一条实线表示。
         */

        private List<Employee> employees = new List<Employee>();

        /// <summary>
        /// 添加员工.
        /// </summary>
        /// <param name="employee">员工.</param>
        public void AddEmployee(Employee employee)
        {
            this.employees.Add(employee);
        }
    }
}
