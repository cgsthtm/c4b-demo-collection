﻿// <copyright file="Person.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._0.类图关系.关联关系Association
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// 人类.
    /// </summary>
    public class Person
    {
        /* 关联（Association）
         * 关联描述了不同类之间的结构化关系，可以是双向或单向的。在C#中，关联通常通过持有另一个类的实例（对象）来表示。
         * 这里，Person类有一个Pet属性，表示一个人可以拥有一只宠物。这是关联的一个简单例子。
         * 关联关系也分为组合和聚合关系。
         */

        /// <summary>
        /// Gets or sets 宠物.
        /// </summary>
        public Pet Pet { get; set; }
    }
}
