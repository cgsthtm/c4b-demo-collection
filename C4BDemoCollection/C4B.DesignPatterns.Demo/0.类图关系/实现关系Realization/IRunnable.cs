﻿// <copyright file="IRunnable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._0.类图关系.实现关系Realization
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    /// <summary>
    /// IRunnable接口.
    /// </summary>
    public interface IRunnable
    {
        /// <summary>
        /// 跑.
        /// </summary>
        void Run();
    }
}
