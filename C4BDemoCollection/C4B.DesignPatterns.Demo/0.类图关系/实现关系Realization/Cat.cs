﻿// <copyright file="Cat.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

#pragma warning disable SA1300 // Element should begin with upper-case letter
namespace C4B.DesignPatterns.Demo._0.类图关系.实现关系Realization
#pragma warning restore SA1300 // Element should begin with upper-case letter
{
    using System;

    /// <summary>
    /// 猫类.
    /// </summary>
    public class Cat : IRunnable
    {
        /* 实现（Realization）- 接口
         * 实现关系表示一个类遵循某个接口所定义的合同。在C#中，使用关键字:来实现接口。
         * 这里，Cat类实现了IRunnable接口，意味着它必须提供Run方法的实现。
         * 实现关系在类图中用空心三角形和一条虚线表示。
         */

        /// <inheritdoc/>
        public void Run()
        {
            Console.WriteLine("Cat is running.");
        }
    }
}
