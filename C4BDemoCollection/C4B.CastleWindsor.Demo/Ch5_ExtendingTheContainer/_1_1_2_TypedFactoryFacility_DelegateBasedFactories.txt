﻿https://github.com/castleproject/Windsor/blob/master/docs/typed-factory-facility-delegate-based.md

# Typed Factory Facility - delegate-based factories
在一些简单的情况下，当您只需要解析单个组件时，创建额外的接口可能会感觉有点多余。对于这种情况，温莎（2.5版或更高版本）
允许您依赖于委托而不是接口，并且委托将回调到容器，以便在您调用它时为您提供实例。

## Using factories
基于委托的工厂没有什么神奇之处。你用他们作为常规的委托，而温莎做所有的繁重工作。
~~~
public class UsingDelegate
{
   private Func<IFoo> factory;

   public void UsingDelegate(Func<IFoo> fooFactory)
   {
      factory = fooFactory;
   }

   public void SomeOperation()
   {
      var foo = factory();
      foo.Bar();
   }

}
~~~
在这种情况下，创建UsingDelegate不会触发IFoo的创建（创建可能很昂贵-想想NHibernate的SessionFactory）。
IFoo只会在调用SomeOperation时创建，这可能是UsingDelegate创建后的一个日志时间。

## Registering factories (implicitly)
将委托公开为依赖项的行为足以指示工具它应该提供工厂。这称为隐式注册。但它不能只是一个委托。与基于接口的工厂类似的规则也适用：
- 委托必须具有非void返回类型。
- 委托不能有out参数
生活方式：以这种方式注册的工厂将有瞬态transient的生活方式。https://github.com/castleproject/Windsor/blob/master/docs/lifestyles.md#transient

## Registering factories explicitly
当您隐式注册工厂时，您放弃了配置工厂的能力。如果您需要一些非默认配置，您可以注册并显式配置工厂。
~~~
container.Register(
	 Component.For<Func<IFoo>>().AsFactory(c => c.SelectedWith("nonDefaultFactory"))
);
~~~

## How dependencies are matched
对于基于委托的工厂，依赖的匹配略有不同。

### First by name
对于委托：
~~~
public delegate IFoo FooFactory(IBar bar);
~~~
如果为服务IFoo注册的组件具有名为bar的依赖项，则它将对传递给委托的值感到满意。

### Then by type
相反，如果组件具有IBar类型的名为mySuperBar的依赖项，则不会在FooFactory委托上找到它。然后，不管IBar类型的名称如何，
它都会去查看是否存在任何对IBar类型的依赖，如果找到了，它就会使用它。
Func<>委托家族不允许重复的依赖关系类型：当使用通用委托Func<>（其中任何一个）时，不可能有具有重复参数类型的委托。️
由于它们的名称只是无意义的arg1、arg2等，因此无法区分Func<IFoo、IFoo>中的参数。在这种情况下，使用专用的委托类型，或者更好的基于接口的工厂.

## Releasing components resolved via factory
重要的是要记住，如果容器的发布策略跟踪您解析的组件，工厂也将保存对该组件的引用。这是为了让温莎在工厂本身被释放时释放您通过工厂解析的组件。
NoTrackingReleasePolicy不会释放工厂：由于不跟踪组件的释放策略不能释放它们，因此在使用基于委托的工厂时使用 LifecycledComponentsReleasePolicy 
（这是默认值）非常重要，以便能够正确释放通过工厂解析的组件。
https://github.com/castleproject/Windsor/blob/master/docs/release-policy.md