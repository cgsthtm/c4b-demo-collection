﻿https://github.com/castleproject/Windsor/blob/master/docs/README.md#concepts

概念：
1. IoC和IoC容器 https://github.com/castleproject/Windsor/blob/master/docs/ioc.md
2. Services, Components and Dependencies - 服务组件和依赖 https://github.com/castleproject/Windsor/blob/master/docs/services-and-components.md
3. 组件是如何被创建的 https://github.com/castleproject/Windsor/blob/master/docs/how-components-are-created.md
4. 依赖是如何被解析的 https://github.com/castleproject/Windsor/blob/master/docs/how-dependencies-are-resolved.md