﻿# How components are created - 组件是如何创建的
当从温莎容器请求组件时，容器会执行几个步骤以提供实例。
1. container: register the components lazily if needed; locate handler
2. handler: invoke dynamic parameters
3. lifecyle manager: get instance
4. component activator: create new instance
5. component activator: invoke lifecyle steps
6. release policy: track component
7. container: return instance

关于实例生命周期和生活方式：此页面有两个目的。️
首先，它解释了当组件的实例被请求时，无论组件的生活方式(https://github.com/castleproject/Windsor/blob/master/docs/lifestyles.md)如何，温莎都做什么。
除此之外，它还描述了组件生命周期的第一部分，即它的开始和诞生（或者用技术术语来说，是什么导致了实例的创建，以及它是如何创建的）。
请记住，这不是实例的整个生命周期，这只是第一步。
要了解整个生命周期，一直到组件的死亡，请参阅有关实例生命周期(https://github.com/castleproject/Windsor/blob/master/docs/lifecycle.md)的页面.

## Locating handler - 定位处理器
当组件被请求时，容器执行的第一步是检查所请求的组件是否在容器中注册。
容器向其命名子系统(https://github.com/castleproject/Windsor/blob/master/docs/subsystems.md)询问组件，
如果找不到组件，容器将尝试延迟注册(https://github.com/castleproject/Windsor/blob/master/docs/lazy-component-loaders.md)它，
如果不成功，将抛出一个异常NotFoundException。

假设可以找到正确的组件，容器将轮询它的处理程序(https://github.com/castleproject/Windsor/blob/master/docs/handlers.md)并要求它解析组件实例。
处理程序是实现IHandler接口的类型。温莎使用处理程序来解析给定服务的组件，然后释放它们。
处理程序还允许访问允许开发人员以图解方式检查组件的ComponentModel(https://github.com/castleproject/Windsor/blob/master/docs/componentmodel.md)。

## What handler does - 处理器做什么
这是一个简单的例子：
- 它调用所有与之关联的ComponentResolvingDelegate，使它们有机会在解析实际启动之前影响解析。
  例如，当delegates passed to DynamicParameters method
  (https://github.com/castleproject/Windsor/blob/master/docs/inline-dependencies.md#supplying-dynamic-dependencies) of
  fluent registration API(https://github.com/castleproject/Windsor/blob/master/docs/fluent-registration-api.md)
- 如果没有提供内联参数，则检查组件及其所有强制依赖项是否可以解析。如果没有，则抛出HandlerException。
- 否则，处理程序要求其生活方式管理器(https://github.com/castleproject/Windsor/blob/master/docs/lifestyles.md)解决该组件。

## What lifestyle manager does - lifestyle管理器做什么
生活方式管理器的角色相对简单。如果它有一个可以重用的组件实例，它会获取该实例并立即将该实例返回给处理程序。
如果没有，它会要求它的组件激活器(https://github.com/castleproject/Windsor/blob/master/docs/component-activators)为它创建一个。

## What component activator does - 组件激活器做什么
组件激活器：组件激活器负责创建组件的实例。不同的激活剂有不同的方式来实现这一点。
当您通过UsingFactoryMethod创建组件时，您提供的委托将被调用以创建实例。
Factory Support Facility(https://github.com/castleproject/Windsor/blob/master/docs/facilities.md)或
Remoting Facility(https://github.com/castleproject/Windsor/blob/master/docs/facilities.md)有自己的一组激活器，用于执行组件的自定义初始化。

大多数情况下，您将使用DefaultActivator，它执行以下操作：
- 通过调用组件的构造函数实例化组件。️如何选择构造函数：要了解默认组件激活器如何选择要使用的构造函数，
  请参阅此处(https://github.com/castleproject/Windsor/blob/master/docs/how-constructor-is-selected.md)。
- 当实例被创建时，它将解析组件的属性依赖项。️如何注入属性：要了解默认组件激活器如何将依赖项注入属性，
  请参阅此处(https://github.com/castleproject/Windsor/blob/master/docs/how-properties-are-injected.md)。
- 当组件被完全创建时，它调用组件的所有commission concerns(https://github.com/castleproject/Windsor/blob/master/docs/lifecycle.md)。
- 在内核上触发ComponentCreated事件。
- 将实例返回给生活方式管理器。

## What handler, release policy and container do - 处理程序、发布策略和容器做什么
如果需要，生活方式管理器可以将实例保存到某个上下文缓存中，以便以后可以重用它，并将其传递给处理程序。
Handler可以选择性地请求发布策略来跟踪组件，如果这是被允许的和需要的，然后将其传递给容器，容器将其返回给用户。