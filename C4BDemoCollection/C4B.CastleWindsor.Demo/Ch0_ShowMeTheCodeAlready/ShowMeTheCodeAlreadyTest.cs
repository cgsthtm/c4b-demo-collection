﻿// <copyright file="ShowMeTheCodeAlreadyTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.CastleWindsor.Demo.Ch0_ShowMeTheCodeAlready
{
    using Castle.Windsor;
    using Castle.Windsor.Installer;

    /// <summary>
    /// ShowMeTheCodeAlreadyTest.
    /// </summary>
    public class ShowMeTheCodeAlreadyTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // application starts...
            var container = new WindsorContainer();

            // adds and configures all components using WindsorInstallers from executing assembly
            container.Install(FromAssembly.This());

            // instantiate and configure root component and all its dependencies and their dependencies and...
            var king = container.Resolve<IKing>();
            king.RuleTheCastle();

            // clean up, application exits
            container.Dispose();
        }
    }
}
