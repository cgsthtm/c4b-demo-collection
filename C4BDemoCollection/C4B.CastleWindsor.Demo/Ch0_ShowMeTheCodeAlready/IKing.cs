﻿// <copyright file="IKing.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.CastleWindsor.Demo.Ch0_ShowMeTheCodeAlready
{
    /// <summary>
    /// IKing接口.
    /// </summary>
    public interface IKing
    {
        /// <summary>
        /// RuleTheCastle.
        /// </summary>
        void RuleTheCastle();
    }
}
