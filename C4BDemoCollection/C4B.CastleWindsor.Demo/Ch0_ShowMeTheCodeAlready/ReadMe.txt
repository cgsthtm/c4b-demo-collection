﻿温莎使用起来非常简单。下面的代码不仅仅是hello world-这是许多大型真实的应用程序使用温莎的原因。
有关API、特性、模式和实践的更多详细信息，请参阅完整文档。https://github.com/castleproject/Windsor/blob/master/docs/README.md
~~~
// application starts...
var container = new WindsorContainer();

// adds and configures all components using WindsorInstallers from executing assembly
container.Install(FromAssembly.This());

// instantiate and configure root component and all its dependencies and their dependencies and...
var king = container.Resolve<IKing>();
king.RuleTheCastle();

// clean up, application exits
container.Dispose();
~~~

So what about those installers? Here's one.
~~~
public class RepositoriesInstaller : IWindsorInstaller
{
	public void Install(IWindsorContainer container, IConfigurationStore store)
	{
		container.Register(Classes.FromThisAssembly()
			                .Where(Component.IsInSameNamespaceAs<King>())
			                .WithService.DefaultInterfaces()
			                .LifestyleTransient());
	}
}
~~~
