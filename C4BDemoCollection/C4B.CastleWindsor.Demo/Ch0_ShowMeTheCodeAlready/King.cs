﻿// <copyright file="King.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.CastleWindsor.Demo.Ch0_ShowMeTheCodeAlready
{
    using System;

    /// <summary>
    /// King实现IKing接口.
    /// </summary>
    public class King : IKing
    {
        /// <inheritdoc/>
        public void RuleTheCastle()
        {
            Console.WriteLine("King.RuleTheCastle() executed.");
        }
    }
}
