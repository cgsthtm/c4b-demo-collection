﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.CastleWindsor.Demo
{
    using System;
    using C4B.CastleWindsor.Demo.Ch0_ShowMeTheCodeAlready;
    using C4B.CastleWindsor.Demo.Ch1_SamplesAndTutorials.Ch1_BasicWindsorTutorial;
    using Castle.Windsor;
    using Castle.Windsor.Installer;

    /// <summary>
    /// Program.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Ch0_ShowMeTheCodeAlready
            ShowMeTheCodeAlreadyTest.Test();

            // Ch1_SamplesAndTutorials - Ch1_BasicWindsorTutorial
            TestDependency.Test();

            Console.ReadLine();
        }
    }
}