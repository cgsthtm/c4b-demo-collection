﻿https://github.com/castleproject/Windsor/blob/master/docs/performance-counters.md

# Windsor's support for performance counters
温莎3引入了对Windows性能计数器的支持。目前温莎只发布一个计数器-“发布策略跟踪的对象”，它显示给定容器的发布策略跟踪的对象总数。
搜索内存泄漏：这是一个非常有用的特性，它将帮助您快速验证非发布跟踪组件实例是否有问题。️

## Using Counters
默认情况下不启用此功能。下面的代码显示了如何启用它：
~~~
var container = new WindsorContainer();
var diagnostic = LifecycledComponentsReleasePolicy.GetTrackedComponentsDiagnostic(container.Kernel);
var counter = LifecycledComponentsReleasePolicy.GetTrackedComponentsPerformanceCounter(new PerformanceMetricsFactory());
container.Kernel.ReleasePolicy = new LifecycledComponentsReleasePolicy(diagnostic, counter);
~~~
温莎将检查它是否具有所有必需的权限，如果具有，它将确保创建正确的类别和计数器，并将在应用程序运行时更新计数器实例。
创建的计数器实例将在进程的生存期内存在，并将在进程终止时删除。

所需权限：如果温莎没有权限创建计数器类别和计数器对象（需要注册表写入权限），它将在不发布计数器实例的情况下静默继续，
在这种情况下 LifecycledComponentsReleasePolicy.GetTrackedComponentsPerformanceCounter() 将返回NullPerformanceCounter。
以管理员身份运行您的应用程序（或Visual Studio，如果正在调试），您只需要执行一次。

要查看数据，请打开性能监视器（计算机管理控制台的一部分，可从Windows控制面板的管理工具部分访问）。
然后点击添加（Ctrl+N）并找到“Castle Windsor城堡温莎”部分。如上所述，它将只包含一个计数器-“Objectstrackedbyrelease policy”，以及它的实例列表。

