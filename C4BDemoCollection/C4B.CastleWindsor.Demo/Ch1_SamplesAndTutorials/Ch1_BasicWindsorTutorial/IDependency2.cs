﻿// <copyright file="IDependency2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.CastleWindsor.Demo.Ch1_SamplesAndTutorials.Ch1_BasicWindsorTutorial
{
    /// <summary>
    /// IDependency2.
    /// </summary>
    public interface IDependency2
    {
        /// <summary>
        /// Gets or sets SomeOtherObject.
        /// </summary>
        object SomeOtherObject { get; set; }
    }
}
