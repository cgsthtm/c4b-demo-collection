﻿// <copyright file="Dependency2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.CastleWindsor.Demo.Ch1_SamplesAndTutorials.Ch1_BasicWindsorTutorial
{
    /// <summary>
    /// Dependency2实现IDependency2接口.
    /// </summary>
    internal class Dependency2 : IDependency2
    {
        /// <inheritdoc/>
        public object SomeOtherObject { get; set; }
    }
}
