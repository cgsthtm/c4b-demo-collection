﻿// <copyright file="TestDependency.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>


namespace C4B.CastleWindsor.Demo.Ch1_SamplesAndTutorials.Ch1_BasicWindsorTutorial
{
    using Castle.Windsor;

    /// <summary>
    /// TestDependency.
    /// </summary>
    public class TestDependency
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // CREATE A WINDSOR CONTAINER OBJECT AND REGISTER THE INTERFACES, AND THEIR CONCRETE IMPLEMENTATIONS.
            var container = new WindsorContainer();
            container.Register(Castle.MicroKernel.Registration.Component.For<Main>());
            container.Register(Castle.MicroKernel.Registration.Component.For<IDependency1>().ImplementedBy<Dependency1>());
            container.Register(Castle.MicroKernel.Registration.Component.For<IDependency2>().ImplementedBy<Dependency2>());

            // CREATE THE OBJECT AND INVOKE ITS METHOD(S) AS DESIRED.
            var mainThing = container.Resolve<Main>();
            mainThing.DoSomething();

            container.Dispose();
        }
    }
}
