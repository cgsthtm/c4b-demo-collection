﻿// <copyright file="Dependency1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.CastleWindsor.Demo.Ch1_SamplesAndTutorials.Ch1_BasicWindsorTutorial
{
    /// <summary>
    /// Dependency1实现IDependency1接口.
    /// </summary>
    internal class Dependency1 : IDependency1
    {
        /// <inheritdoc/>
        public object SomeObject { get; set; }
    }
}
