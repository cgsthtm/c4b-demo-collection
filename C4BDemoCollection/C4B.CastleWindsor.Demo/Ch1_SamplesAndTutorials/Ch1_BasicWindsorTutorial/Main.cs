﻿// <copyright file="Main.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.CastleWindsor.Demo.Ch1_SamplesAndTutorials.Ch1_BasicWindsorTutorial
{
    using System;

    /// <summary>
    /// DependencyTestClass.
    /// </summary>
    public class Main
    {
        private IDependency1 object1;
        private IDependency2 object2;

        // 注意构造函数需要两个参数，和接口一样。在这里，我们注入依赖项，而不是创建每个依赖项的实例。

        /// <summary>
        /// Initializes a new instance of the <see cref="Main"/> class.
        /// </summary>
        /// <param name="dependency1">IDependency1.</param>
        /// <param name="dependency2">IDependency2.</param>
        public Main(IDependency1 dependency1, IDependency2 dependency2)
        {
            this.object1 = dependency1;
            this.object2 = dependency2;
        }

        /// <summary>
        /// DoSomething.
        /// </summary>
        public void DoSomething()
        {
            this.object1.SomeObject = "Hello World";
            this.object2.SomeOtherObject = "Hello Mars";
            Console.WriteLine(this.object1.SomeObject);
            Console.WriteLine(this.object2.SomeOtherObject);
        }
    }
}
