﻿// <copyright file="IDependency1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.CastleWindsor.Demo.Ch1_SamplesAndTutorials.Ch1_BasicWindsorTutorial
{
    /// <summary>
    /// IDependency1.
    /// </summary>
    public interface IDependency1
    {
        /// <summary>
        /// Gets or sets SomeObject.
        /// </summary>
        object SomeObject { get; set; }
    }
}
