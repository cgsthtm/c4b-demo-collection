﻿https://github.com/castleproject/Windsor/blob/master/docs/componentmodel-construction-contributors.md

# ComponentModel construction contributors
ConstructionModel构造贡献者是实现 IContributeComponentModelConstruction 接口的对象。正如其名称所暗示的那样，
它们在创建后立即将ComponentModel构造到其最终状态。

不要在贡献者之外修改ComponentModel：不鼓励在构建贡献者之外的其他地方修改组件模型。在ComponentModel被它的构造贡献者处理之后，
它应该被认为是只读的。在以后的任何时候修改它都可能导致并发问题和其他难以跟踪的问题。

## The IContributeComponentModelConstruction interface
onstructionModel构造贡献者需要实现单个方法：
~~~
void ProcessModel(IKernel kernel, ComponentModel model);
~~~
基于其他贡献者提供的信息，内核，模型的配置或其自身的状态，它们可以检查或修改model参数。温莎使用几个内置的贡献者本身来设置诸如配置，
参数，生活方式，生命周期步骤，依赖关系等。

### Writing your own
编写自定义贡献器是扩展/自定义温莎的最常见方法之一。为了举例，我们假设我们想让所有组件上的ILogger类型的所有属性都是强制性的
（默认情况下，属性依赖关系在温莎中是可选的）。要做到这一点，我们可以写一个贡献者，看起来像下面这样：
~~~
public class RequireLoggerProperties : IContributeComponentModelConstruction
{
    public void ProcessModel(IKernel kernel, ComponentModel model)
    {
        model.Properties
            .Where(p => p.Dependency.TargetType == typeof(ILogger))
            .All(p => p.Dependency.IsOptional = false);
    }
}
~~~
贡献者扫描每个组件的所有属性依赖项，试图找到ILogger类型的依赖项，并将其标记为强制性。

### Plugging the contributor in
建贡献者时，需要将其添加到container的ContributorModelBuilder上的贡献者集合中：
~~~
container.Kernel.ComponentModelBuilder.AddContributor(new RequireLoggerProperties());
~~~