﻿# Before and After Map Action
有时，您可能需要在映射发生之前或之后执行自定义逻辑。这些应该是罕见的，因为在AutoMapper之外做这项工作更明显。您可以创建全局前/后映射操作：
~~~
var configuration = new MapperConfiguration(cfg => {
  cfg.CreateMap<Source, Dest>()
    .BeforeMap((src, dest) => src.Value = src.Value + 10)
    .AfterMap((src, dest) => dest.Name = "John");
});
~~~
或者你可以在映射过程中创建before/after map回调：
~~~
int i = 10;
mapper.Map<Source, Dest>(src, opt => {
    opt.BeforeMap((src, dest) => src.Value = src.Value + i);
    opt.AfterMap((src, dest) => dest.Name = HttpContext.Current.Identity.Name);
});
~~~
当您需要将上下文信息输入到之前/之后的地图操作中时，后一种配置很有帮助。

## Using IMappingAction
您可以将Before和After Map Actions封装到小型可重用类中。这些类需要实现 IMappingAction<in TSource, in TDestination> 接口。
使用前面的例子，这里是一个命名一些对象“John”的封装：
~~~
public class NameMeJohnAction : IMappingAction<SomePersonObject, SomeOtherPersonObject>
{
    public void Process(SomePersonObject source, SomeOtherPersonObject destination, ResolutionContext context)
    {
        destination.Name = "John";
    }
}

var configuration = new MapperConfiguration(cfg => {
  cfg.CreateMap<SomePersonObject, SomeOtherPersonObject>()
    .AfterMap<NameMeJohnAction>();
});
~~~

### Asp.Net Core and AutoMapper.Extensions.Microsoft.DependencyInjection
如果您使用的是ASP.NET Core和 AutoMapper.Extensions.Microsoft.DependencyInjection 包，这也是使用Dependency Injection的好方法。
您不能将依赖项注入Profile类，但可以在IMappingAction实现中这样做。
下面的示例展示了如何将访问当前HttpContext的IMappingAction连接到Profileafter map action，利用Dependency Injection：
~~~
public class SetTraceIdentifierAction : IMappingAction<SomeModel, SomeOtherModel>
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public SetTraceIdentifierAction(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
    }

    public void Process(SomeModel source, SomeOtherModel destination, ResolutionContext context)
    {
        destination.TraceIdentifier = _httpContextAccessor.HttpContext.TraceIdentifier;
    }
}

public class SomeProfile : Profile
{
    public SomeProfile()
    {
        CreateMap<SomeModel, SomeOtherModel>()
            .AfterMap<SetTraceIdentifierAction>();
    }
}
~~~
一切都通过以下方式连接在一起：
~~~
public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddAutoMapper(typeof(Startup).Assembly);
    }
    //..
}
~~~
更多信息请参见 AutoMapper.Extensions.Microsoft.DependencyInjection 。