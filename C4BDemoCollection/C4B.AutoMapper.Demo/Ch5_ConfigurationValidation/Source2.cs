﻿// <copyright file="Source2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch5_ConfigurationValidation
{
    /// <summary>
    /// Source2.
    /// </summary>
    internal class Source2
    {
        /// <summary>
        /// Gets or sets SomeValue.
        /// </summary>
        public int SomeValue { get; set; }
    }
}
