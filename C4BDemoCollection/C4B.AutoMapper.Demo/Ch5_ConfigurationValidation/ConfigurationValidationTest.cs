﻿// <copyright file="ConfigurationValidationTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch5_ConfigurationValidation
{
    using C4B.AutoMapper.Demo.Ch4_Configuration;
    using global::AutoMapper;
    using Xunit;

    /// <summary>
    /// ConfigurationValidationTest.
    /// </summary>
    public class ConfigurationValidationTest
    {
        /// <summary>
        /// Test_AssertConfigurationIsValid.
        /// </summary>
        [Fact]
        public static void Test_AssertConfigurationIsValid()
        {
            var configuration = new MapperConfiguration(cfg =>
                cfg.CreateMap<Source2, Destination2>());

            Assert.Throws<AutoMapperConfigurationException>(() => configuration.AssertConfigurationIsValid());
        }

        /// <summary>
        /// Test_OverridingConfigurationErrors.
        /// </summary>
        [Fact]
        public static void Test_OverridingConfigurationErrors()
        {
            /*要修复配置错误（除了重命名源/目标成员之外），您有三种选择来提供备用配置：
                1.Custom Value Resolvers
                2.Projection
                3.Use the Ignore() option
             */
            // 使用第三个选项，我们将使用其他方法填充目标类型上的成员，而不是通过Map操作。
            var configuration = new MapperConfiguration(cfg =>
              cfg.CreateMap<Source2, Destination2>()
                .ForMember(dest => dest.SomeValuefff, opt => opt.Ignore()));
            configuration.AssertConfigurationIsValid();
        }

        /// <summary>
        /// Test_SelectingMembersToValidate.
        /// </summary>
        [Fact]
        public static void Test_SelectingMembersToValidate()
        {
            // 默认情况下，AutoMapper使用目标类型来验证成员。它假定所有目标成员都需要映射。若要修改此行为，请使用CreateMap重载指定要验证的成员列表：
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Source, Destination>(MemberList.Source); // AutoMapper默认使用MemberList.Destination来验证成员
                cfg.CreateMap<Source2, Destination2>(MemberList.None); // 若要完全跳过此映射的验证，请使用MemberList.None。这是ReverseMap的默认值。
            });
            var mapper = configuration.CreateMapper();
            var destDto = mapper.Map<Destination2>(new Source2 { SomeValue = 1 });
            Assert.NotNull(destDto);
            Assert.NotEqual(1, destDto.SomeValuefff);
        }
    }
}
