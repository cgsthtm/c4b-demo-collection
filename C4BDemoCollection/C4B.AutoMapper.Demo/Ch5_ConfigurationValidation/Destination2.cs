﻿// <copyright file="Destination2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch5_ConfigurationValidation
{
    /// <summary>
    /// Destination2.
    /// </summary>
    internal class Destination2
    {
        /// <summary>
        /// Gets or sets SomeValue.
        /// </summary>
        public int SomeValuefff { get; set; }
    }
}
