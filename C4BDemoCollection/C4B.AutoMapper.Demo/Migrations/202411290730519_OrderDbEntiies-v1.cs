﻿namespace C4B.AutoMapper.Demo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderDbEntiiesv1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderLines", "Item_Id", "dbo.Items");
            DropIndex("dbo.OrderLines", new[] { "Item_Id" });
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContactString = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        CustomerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.OrderLines", "Item_Id", c => c.Int());
            CreateIndex("dbo.OrderLines", "Item_Id");
            AddForeignKey("dbo.OrderLines", "Item_Id", "dbo.Items", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderLines", "Item_Id", "dbo.Items");
            DropForeignKey("dbo.Contacts", "CustomerId", "dbo.Customers");
            DropIndex("dbo.OrderLines", new[] { "Item_Id" });
            DropIndex("dbo.Contacts", new[] { "CustomerId" });
            AlterColumn("dbo.OrderLines", "Item_Id", c => c.Int(nullable: false));
            DropTable("dbo.Customers");
            DropTable("dbo.Contacts");
            CreateIndex("dbo.OrderLines", "Item_Id");
            AddForeignKey("dbo.OrderLines", "Item_Id", "dbo.Items", "Id", cascadeDelete: true);
        }
    }
}
