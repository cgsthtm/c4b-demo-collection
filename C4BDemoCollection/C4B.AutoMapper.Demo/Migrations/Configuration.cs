﻿// <copyright file="Configuration.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using C4B.AutoMapper.Demo.Ch17_QueryableExtensions;

    /// <summary>
    /// Configuration.
    /// </summary>
    internal sealed class Configuration : DbMigrationsConfiguration<C4B.AutoMapper.Demo.Ch17_QueryableExtensions.OrderDbContext>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration"/> class.
        /// </summary>
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = false;
            this.ContextKey = "C4B.AutoMapper.Demo.Ch17_QueryableExtensions.OrderDbContext";
        }

        /// <inheritdoc/>
        protected override void Seed(C4B.AutoMapper.Demo.Ch17_QueryableExtensions.OrderDbContext context)
        {
            // This method will be called after migrating to the latest version.

            // You can use the DbSet<T>.AddOrUpdate() helper extension method
            // to avoid creating duplicate seed data.
            List<OrderLine> orders = new List<OrderLine>()
            {
                new OrderLine() { OrderId = 0, Item = new Item() { Name = "Item0" }, Quantity = 10 },
                new OrderLine() { OrderId = 1, Item = new Item() { Name = "Item1" }, Quantity = 11 },
                new OrderLine() { OrderId = 2, Item = new Item() { Name = "Item2" }, Quantity = 12 },
                new OrderLine() { OrderId = 3, Item = new Item() { Name = "Item3" }, Quantity = 13 },
                new OrderLine() { OrderId = 4, Item = new Item() { Name = "Item4" }, Quantity = 14 },
            };
            context.OrderLines.AddOrUpdate<OrderLine>(orders.ToArray());
            List<Customer> customers = new List<Customer>()
            {
                new Customer()
                {
                    Id = 1, FirstName = "01", LastName = "cgs", Contacts = new List<Contact>()
                    {
                        new Contact() { CustomerId = 1, ContactString = "c01", CreatedAt = DateTime.Now },
                        new Contact() { CustomerId = 1, ContactString = "c11", CreatedAt = DateTime.Now.AddMinutes(1) },
                    },
                },
                new Customer()
                {
                    Id = 2, FirstName = "02", LastName = "cgs", Contacts = new List<Contact>()
                    {
                        new Contact() { CustomerId = 2, ContactString = "c02", CreatedAt = DateTime.Now.AddDays(1) },
                        new Contact() { CustomerId = 2, ContactString = "c22", CreatedAt = DateTime.Now.AddDays(1).AddMinutes(1) },
                    },
                },
                new Customer()
                {
                    Id = 3, FirstName = "03", LastName = "cgs", Contacts = new List<Contact>()
                    {
                        new Contact() { CustomerId = 3, ContactString = "c03", CreatedAt = DateTime.Now.AddDays(2) },
                        new Contact() { CustomerId = 3, ContactString = "c23", CreatedAt = DateTime.Now.AddDays(2).AddMinutes(1) },
                    },
                },
            };
            context.Customers.AddOrUpdate<Customer>(customers.ToArray());
            base.Seed(context);
        }
    }
}