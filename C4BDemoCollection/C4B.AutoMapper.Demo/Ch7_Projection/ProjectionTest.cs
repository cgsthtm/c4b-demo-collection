﻿// <copyright file="ProjectionTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch7_Projection
{
    using System;
    using global::AutoMapper;
    using Xunit;

    /// <summary>
    /// ProjectionTest.
    /// </summary>
    public class ProjectionTest
    {
        /// <summary>
        /// Test_Projection.
        /// </summary>
        [Fact]
        public static void Test_Projection()
        {
            // Model
            var calendarEvent = new CalendarEvent
            {
                Date = new DateTime(2008, 12, 15, 20, 30, 0),
                Title = "Company Holiday Party",
            };

            // Configure AutoMapper
            var configuration = new MapperConfiguration(cfg =>
              cfg.CreateMap<CalendarEvent, CalendarEventForm>()
                .ForMember(dest => dest.EventDate, opt => opt.MapFrom(src => src.Date.Date))
                .ForMember(dest => dest.EventHour, opt => opt.MapFrom(src => src.Date.Hour))
                .ForMember(dest => dest.EventMinute, opt => opt.MapFrom(src => src.Date.Minute)));

            // Perform mapping
            var mapper = configuration.CreateMapper();
            CalendarEventForm form = mapper.Map<CalendarEvent, CalendarEventForm>(calendarEvent);
            Assert.Equal(new DateTime(2008, 12, 15), form.EventDate.Date);
            Assert.Equal(20, form.EventHour);
            Assert.Equal(30, form.EventMinute);
            Assert.Equal("Company Holiday Party", form.Title);
        }
    }
}
