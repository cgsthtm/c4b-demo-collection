﻿# Projection
投影将源转换为目标，而不仅仅是展平对象模型。在没有额外配置的情况下，AutoMapper需要一个扁平化的目标来匹配源类型的命名结构。
如果要将源值投影到与源结构不完全匹配的目标中，则必须指定自定义成员映射定义。例如，我们可能想把这个源结构：
~~~
public class CalendarEvent
{
	public DateTime Date { get; set; }
	public string Title { get; set; }
}
~~~
转换成更适合网页上输入表单的东西：
~~~
public class CalendarEventForm
{
	public DateTime EventDate { get; set; }
	public int EventHour { get; set; }
	public int EventMinute { get; set; }
	public string Title { get; set; }
}
~~~
于目标属性的名称与源属性不完全匹配（CalendarEvent.Date需要为CalendarEventForm.EventDate），因此我们需要在类型映射配置中指定自定义成员映射：
~~~
// Model
var calendarEvent = new CalendarEvent
{
	Date = new DateTime(2008, 12, 15, 20, 30, 0),
	Title = "Company Holiday Party"
};

// Configure AutoMapper
var configuration = new MapperConfiguration(cfg =>
  cfg.CreateMap<CalendarEvent, CalendarEventForm>()
	.ForMember(dest => dest.EventDate, opt => opt.MapFrom(src => src.Date.Date))
	.ForMember(dest => dest.EventHour, opt => opt.MapFrom(src => src.Date.Hour))
	.ForMember(dest => dest.EventMinute, opt => opt.MapFrom(src => src.Date.Minute)));

// Perform mapping
CalendarEventForm form = mapper.Map<CalendarEvent, CalendarEventForm>(calendarEvent);

form.EventDate.ShouldEqual(new DateTime(2008, 12, 15));
form.EventHour.ShouldEqual(20);
form.EventMinute.ShouldEqual(30);
form.Title.ShouldEqual("Company Holiday Party");
~~~
每个自定义成员配置都使用一个操作委托来配置每个单独的成员。在上面的示例中，我们使用MapFrom选项执行自定义的源到目标成员映射。
MapFrom方法接受lambda表达式作为参数，然后在映射过程中对其进行计算。MapFrom表达式可以是任何Func<TSource，object>lambda表达式。
