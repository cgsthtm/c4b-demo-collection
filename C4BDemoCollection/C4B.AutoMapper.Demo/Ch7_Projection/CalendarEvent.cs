﻿// <copyright file="CalendarEvent.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch7_Projection
{
    using System;

    /// <summary>
    /// CalendarEvent.
    /// </summary>
    internal class CalendarEvent
    {
        /// <summary>
        /// Gets or sets Date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets Title.
        /// </summary>
        public string Title { get; set; }
    }
}
