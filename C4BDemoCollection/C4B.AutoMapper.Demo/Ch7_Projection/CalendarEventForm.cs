﻿// <copyright file="CalendarEventForm.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch7_Projection
{
    using System;

    /// <summary>
    /// CalendarEventForm.
    /// </summary>
    internal class CalendarEventForm
    {
        /// <summary>
        /// Gets or sets EventDate.
        /// </summary>
        public DateTime EventDate { get; set; }

        /// <summary>
        /// Gets or sets EventHour.
        /// </summary>
        public int EventHour { get; set; }

        /// <summary>
        /// Gets or sets EventMinute.
        /// </summary>
        public int EventMinute { get; set; }

        /// <summary>
        /// Gets or sets Title.
        /// </summary>
        public string Title { get; set; }
    }
}
