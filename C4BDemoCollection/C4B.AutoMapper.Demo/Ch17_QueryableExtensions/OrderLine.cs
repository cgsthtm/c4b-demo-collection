﻿// <copyright file="OrderLine.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch17_QueryableExtensions
{
    /// <summary>
    /// OrderLine.
    /// </summary>
    internal class OrderLine
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets OrderId.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Gets or sets Item.
        /// </summary>
        public Item Item { get; set; }

        /// <summary>
        /// Gets or sets Quantity.
        /// </summary>
        public decimal Quantity { get; set; }
    }
}
