﻿// <copyright file="ContactDTO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch17_QueryableExtensions
{
    /// <summary>
    /// ContactDto.
    /// </summary>
    internal class ContactDto
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets ContactString.
        /// </summary>
        public string ContactString { get; set; }
    }
}
