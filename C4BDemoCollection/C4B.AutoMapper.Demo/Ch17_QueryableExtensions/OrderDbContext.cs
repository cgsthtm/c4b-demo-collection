﻿// <copyright file="OrderDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch17_QueryableExtensions
{
    using System.Data.Entity;

    /// <summary>
    /// OrderDbContext.
    /// </summary>
    internal class OrderDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderDbContext"/> class.
        /// </summary>
        public OrderDbContext()
            : base("Ch17_QueryableExtensions")
        {
            ////Database.SetInitializer(new OrderDbInitializer());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<OrderDbContext, C4B.AutoMapper.Demo.Migrations.Configuration>());
        }

        /// <summary>
        /// Gets or sets Items.
        /// </summary>
        public DbSet<Item> Items { get; set; }

        /// <summary>
        /// Gets or sets OrderLines.
        /// </summary>
        public DbSet<OrderLine> OrderLines { get; set; }

        /// <summary>
        /// Gets or sets Contacts.
        /// </summary>
        public DbSet<Contact> Contacts { get; set; }

        /// <summary>
        /// Gets or sets Customers.
        /// </summary>
        public DbSet<Customer> Customers { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            ////modelBuilder.Entity<Item>().HasKey(x => x.Id);
            ////modelBuilder.Entity<OrderLine>().HasKey(x => x.Id).HasRequired(o => o.Item);
            base.OnModelCreating(modelBuilder);
        }
    }
}
