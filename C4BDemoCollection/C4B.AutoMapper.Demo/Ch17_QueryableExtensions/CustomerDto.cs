﻿// <copyright file="CustomerDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch17_QueryableExtensions
{
    using System.Collections.Generic;

    /// <summary>
    /// CustomerDto.
    /// </summary>
    internal class CustomerDto
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets FullName.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets Contacts.
        /// </summary>
        public virtual IList<Contact> Contacts { get; set; }

        /// <summary>
        /// Gets or sets TotalContacts.
        /// </summary>
        public virtual int TotalContacts { get; set; }
    }
}
