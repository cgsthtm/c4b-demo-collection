﻿# Queryable Extensions
当使用ORM（如NHibernate或Entity Framework）与AutoMapper的标准mapper.Map函数时，您可能会注意到，当AutoMapper试图将结果映射到目标类型时，ORM将查询图中所有对象的所有字段。
如果您的ORM公开了IQueryable，您可以使用AutoMapper的QueryableExtensions辅助方法来解决这个关键问题。
以Entity Framework为例，假设您有一个实体OrderLine，它与实体Item有关系。如果您想将其映射到具有Item的Name属性的OrderLineDTO，
则标准的mapper.Map调用将导致Entity Framework查询整个OrderLine和Item表。
使用这种方法代替。
鉴于以下实体：
~~~
public class OrderLine
{
  public int Id { get; set; }
  public int OrderId { get; set; }
  public Item Item { get; set; }
  public decimal Quantity { get; set; }
}

public class Item
{
  public int Id { get; set; }
  public string Name { get; set; }
}
~~~
以及以下DTO：
~~~
public class OrderLineDTO
{
  public int Id { get; set; }
  public int OrderId { get; set; }
  public string Item { get; set; }
  public decimal Quantity { get; set; }
}
~~~
你可以像这样使用可查询扩展：
~~~
var configuration = new MapperConfiguration(cfg =>
    cfg.CreateMap<OrderLine, OrderLineDTO>()
    .ForMember(dto => dto.Item, conf => conf.MapFrom(ol => ol.Item.Name)));

public List<OrderLineDTO> GetLinesForOrder(int orderId)
{
  using (var context = new orderEntities())
  {
    return context.OrderLines.Where(ol => ol.OrderId == orderId)
             .ProjectTo<OrderLineDTO>(configuration).ToList();
  }
}
~~~
.ProjectTo<OrderLineDTO>()将告诉AutoMapper的映射引擎向IQueryable发出一个select子句，该子句将通知实体框架它只需要查询Item表的Name列，
就像您手动将IQueryable投影到具有Select子句的OrderLineDTO一样。
请注意，要使此功能正常工作，所有类型转换都必须在Mapping中显式处理。例如，您不能依赖Item类的ToString()重写来通知实体框架仅从Name列中进行选择，
并且还必须显式处理任何数据类型更改，例如Double到Decimal。

## The instance API
从8.0开始，IMapper上有类似的ProjectTo方法，当您将IMapper与DI一起使用时，这些方法感觉更自然。

## Preventing lazy loading/SELECT N+1 problems
由于AutoMapper构建的LINQ投影由查询提供程序直接转换为SQL查询，因此映射发生在SQL/ADO.NET级别，而不涉及实体。所有数据都被急切地提取并加载到DTO中。
嵌套集合使用Select来投影子DTO：
~~~
from i in db.Instructors
orderby i.LastName
select new InstructorIndexData.InstructorModel
{
    ID = i.ID,
    FirstMidName = i.FirstMidName,
    LastName = i.LastName,
    HireDate = i.HireDate,
    OfficeAssignmentLocation = i.OfficeAssignment.Location,
    Courses = i.Courses.Select(c => new InstructorIndexData.InstructorCourseModel
    {
        CourseID = c.CourseID,
        CourseTitle = c.Title
    }).ToList()
};
~~~
通过AutoMapper进行的此映射将导致SELECT N+1问题，因为每个子课程将一次查询一个，除非通过ORM指定迫切获取。使用LINQ投影，您的ORM不需要特殊的配置或规范。
ORM使用LINQ投影来构建所需的确切SQL查询。

## Custom projection
如果成员名称不对齐，或者您想要创建计算属性，则可以使用MapFrom（基于表达式的重载）为目标成员提供自定义表达式：
~~~
var configuration = new MapperConfiguration(cfg => cfg.CreateMap<Customer, CustomerDto>()
    .ForMember(d => d.FullName, opt => opt.MapFrom(c => c.FirstName + " " + c.LastName))
    .ForMember(d => d.TotalContacts, opt => opt.MapFrom(c => c.Contacts.Count()));
~~~
AutoMapper将提供的表达式与构建的投影一起传递。只要您的查询提供程序可以解释所提供的表达式，所有内容都将一直传递到数据库。
如果表达式被你的查询提供者（Entity Framework，NHibernate等）拒绝，你可能需要调整你的表达式，直到你找到一个被接受的表表达式。

## Custom Type Conversion
有时候，您需要完全替换从源类型到目标类型的类型转换。在正常的运行时映射中，这是通过ConvertUsing方法完成的。要在LINQ投影中执行模拟，请使用ConvertUsing方法：
~~~
cfg.CreateMap<Source, Dest>().ConvertUsing(src => new Dest { Value = 10 });
~~~
基于表达式的ConvertUsing比基于函数的ConvertUsing重载稍微受限一些，因为只有表达式和基础LINQ提供程序中允许的内容才有效。

## Custom destination type constructors
如果您的目标类型有一个自定义构造函数，但您不想重写整个映射，请使用ConstructUsing基于表达式的方法重载：
~~~
cfg.CreateMap<Source, Dest>()
    .ConstructUsing(src => new Dest(src.Value + 10));
~~~
AutoMapper将根据匹配的名称自动将目标构造函数参数与源成员相匹配，因此仅在AutoMapper无法正确匹配目标构造函数或在构造期间需要额外自定义时才使用此方法。

## String conversion
当目标成员类型是字符串而源成员类型不是时，AutoMapper将自动添加ToString（）。
~~~
public class Order {
    public OrderTypeEnum OrderType { get; set; }
}
public class OrderDto {
    public string OrderType { get; set; }
}
var orders = dbContext.Orders.ProjectTo<OrderDto>(configuration).ToList();
orders[0].OrderType.ShouldEqual("Online");
~~~

## Explicit expansion
在某些情况下，例如OData，通过IQueryable控制器操作返回通用DTO。如果没有明确的指示，AutoMapper将展开结果中的所有成员。若要控制在投影期间展开哪些成员，
请在配置中设置ExplicitExpansion，然后传入要显式展开的成员：
~~~
dbContext.Orders.ProjectTo<OrderDto>(configuration,
    dest => dest.Customer,
    dest => dest.LineItems);
// or string-based
dbContext.Orders.ProjectTo<OrderDto>(configuration,
    null,
    "Customer",
    "LineItems");
// for collections
dbContext.Orders.ProjectTo<OrderDto>(configuration,
    null,
    dest => dest.LineItems.Select(item => item.Product));
~~~

## Aggregations
LINQ可以支持聚合查询，AutoMapper支持LINQ扩展方法。在自定义投影示例中，如果我们将TotalContacts属性重命名为ContactsCount，AutoMapper将匹配Count（）扩展方法，
LINQ提供程序将计数转换为相关的子查询以聚合子记录。
如果LINQ提供程序支持，AutoMapper还可以支持复杂的聚合和嵌套限制：
~~~
cfg.CreateMap<Course, CourseModel>()
    .ForMember(m => m.EnrollmentsStartingWithA,
          opt => opt.MapFrom(c => c.Enrollments.Where(e => e.Student.LastName.StartsWith("A")).Count()));
~~~
此查询返回每个课程中姓氏以字母“A”开头的学生总数。

## Parameterization
偶尔，投影需要运行时参数作为其值。考虑一个需要将当前用户名作为其数据的一部分拉入的投影。我们可以参数化MapFrom配置，而不是使用post-mapping代码：
~~~
string currentUserName = null;
cfg.CreateMap<Course, CourseModel>()
    .ForMember(m => m.CurrentUserName, opt => opt.MapFrom(src => currentUserName));
~~~
当我们投影时，我们将在运行时替换我们的参数：
~~~
dbContext.Courses.ProjectTo<CourseModel>(Config, new { currentUserName = Request.User.Name });
~~~
它的工作原理是在原始表达式中捕获闭包的字段名，然后在查询发送到查询提供程序之前使用匿名对象/字典将该值应用于参数值。
您也可以使用字典来构建投影值：
~~~
dbContext.Courses.ProjectTo<CourseModel>(Config, new Dictionary<string, object> { {"currentUserName", Request.User.Name} });
~~~
但是，使用字典将导致查询中出现硬编码值，而不是参数化查询，因此请谨慎使用。

## Supported mapping options
并非所有映射选项都受支持，因为生成的表达式必须由LINQ提供程序解释。AutoMapper只支持LINQ提供程序支持的内容：
- MapFrom (Expression-based)
- ConvertUsing (Expression-based)
- Ignore
- NullSubstitute
- Value transformers
不支持：
- Condition
- SetMappingOrder
- UseDestinationValue
- MapFrom (Func-based)
- Before/AfterMap
- Custom resolvers
- Custom type converters
- ForPath
- Value converters
- Any calculated property on your domain object
此外，不支持递归或自引用目标类型，因为LINQ提供程序不支持这些类型。通常，分层关系数据模型需要公共表表达式（CTE）来正确解析递归连接。