﻿// <copyright file="Customer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch17_QueryableExtensions
{
    using System.Collections.Generic;

    /// <summary>
    /// Customer.
    /// </summary>
    internal class Customer
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets Contacts.
        /// </summary>
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
