﻿// <copyright file="OrderDbInitializer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch17_QueryableExtensions
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;

    /// <summary>
    /// OrderDbInitializer.
    /// </summary>
    internal class OrderDbInitializer : CreateDatabaseIfNotExists<OrderDbContext>
    {
        /// <inheritdoc/>
        protected override void Seed(OrderDbContext context)
        {
            List<OrderLine> orders = new List<OrderLine>()
            {
                new OrderLine() { OrderId = 0, Item = new Item() { Name = "Item0" }, Quantity = 10 },
                new OrderLine() { OrderId = 1, Item = new Item() { Name = "Item1" }, Quantity = 11 },
                new OrderLine() { OrderId = 2, Item = new Item() { Name = "Item2" }, Quantity = 12 },
                new OrderLine() { OrderId = 3, Item = new Item() { Name = "Item3" }, Quantity = 13 },
                new OrderLine() { OrderId = 4, Item = new Item() { Name = "Item4" }, Quantity = 14 },
            };
            context.OrderLines.AddOrUpdate<OrderLine>(orders.ToArray());
            base.Seed(context);
        }
    }
}
