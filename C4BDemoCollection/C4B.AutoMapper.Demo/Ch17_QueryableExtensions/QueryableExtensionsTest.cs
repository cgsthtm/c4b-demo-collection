﻿// <copyright file="QueryableExtensionsTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch17_QueryableExtensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::AutoMapper;
    using global::AutoMapper.QueryableExtensions;
    using Serilog;
    using Xunit;

    /// <summary>
    /// QueryableExtensionsTest.
    /// </summary>
    public class QueryableExtensionsTest
    {
        /// <summary>
        /// Test_QueryableExtensions.
        /// </summary>
        [Fact]
        public static void Test_QueryableExtensions()
        {
            var configuration = new MapperConfiguration(cfg =>
                cfg.CreateMap<OrderLine, OrderLineDto>()
                .ForMember(dto => dto.Item, conf => conf.MapFrom(ol => ol.Item.Name)));

            using (var context = new OrderDbContext())
            {
                var dtos = context.OrderLines.Where(ol => ol.OrderId == 1)
                         .ProjectTo<OrderLineDto>(configuration).ToList();
                foreach (var item in dtos)
                {
                    Log.Logger.Information("OrderLineDTO: {@item}", item);
                }
            }
        }

        /// <summary>
        /// Test_PreventingLazyLoading.
        /// </summary>
        [Fact]
        public static void Test_PreventingLazyLoading()
        {
            /*由于AutoMapper构建的LINQ投影由查询提供程序直接转换为SQL查询，因此映射发生在SQL/ADO.NET级别，而不涉及实体。所有数据都被急切地提取并加载到DTO中。
             * 嵌套集合使用Select来投影子DTO：
from i in db.Instructors
orderby i.LastName
select new InstructorIndexData.InstructorModel
{
    ID = i.ID,
    FirstMidName = i.FirstMidName,
    LastName = i.LastName,
    HireDate = i.HireDate,
    OfficeAssignmentLocation = i.OfficeAssignment.Location,
    Courses = i.Courses.Select(c => new InstructorIndexData.InstructorCourseModel
    {
        CourseID = c.CourseID,
        CourseTitle = c.Title
    }).ToList()
};
             * 通过AutoMapper进行的此映射将导致SELECT N+1问题，因为每个子课程将一次查询一个，除非通过ORM指定迫切获取。
             * 使用LINQ投影，您的ORM不需要特殊的配置或规范。ORM使用LINQ投影来构建所需的确切SQL查询。
             */
        }

        /// <summary>
        /// Test_CustomProjection.
        /// </summary>
        [Fact]
        public static void Test_CustomProjection()
        {
            // 如果成员名称不对齐，或者您想要创建计算属性，则可以使用MapFrom（基于表达式的重载）为目标成员提供自定义表达式：
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<Customer, CustomerDto>()
                .ForMember(d => d.FullName, opt => opt.MapFrom(c => c.FirstName + " " + c.LastName))
                .ForMember(d => d.TotalContacts, opt => opt.MapFrom(c => c.Contacts.Count())));
            using (var ctx = new OrderDbContext())
            {
                var dto = ctx.Customers.Include("Contacts")
                    .Where(c => c.FirstName == "01" && c.LastName == "cgs")
                    .ProjectTo<CustomerDto>(configuration).FirstOrDefault(); // 使用AutoMapper的QueryableExtensions辅助方法ProjectTo来解决ORM与AutoMapper的标准mapper.Map函数配合使用时，ORM将查询图中所有对象的所有字段的问题。
                Assert.Equal("01 cgs", dto.FullName);
                Assert.Equal(2, dto.TotalContacts);
                Log.Logger.Information("CustomerDto: {@dto}", dto);
            }
        }

        /// <summary>
        /// Test_CustomTypeConversion.
        /// </summary>
        [Fact]
        public static void Test_CustomTypeConversion()
        {
            // 有时候，您需要完全替换从源类型到目标类型的类型转换。在正常的运行时映射中，这是通过ConvertUsing方法完成的。要在LINQ投影中执行模拟，请使用ConvertUsing方法：
            // cfg.CreateMap<Source, Dest>().ConvertUsing(src => new Dest { Value = 10 });
        }

        /// <summary>
        /// Test_CustomDestinationTypeConstructors.
        /// </summary>
        [Fact]
        public static void Test_CustomDestinationTypeConstructors()
        {
            // 如果您的目标类型有一个自定义构造函数，但您不想重写整个映射，请使用ConstructUsing基于表达式的方法重载：
            // cfg.CreateMap<Source, Dest>().ConstructUsing(src => new Dest(src.Value + 10));
        }

        /// <summary>
        /// Test_CustomDestinationTypeConstructors.
        /// </summary>
        [Fact]
        public static void Test_StringConversion()
        {
            // 当目标成员类型是字符串而源成员类型不是时，AutoMapper将自动添加ToString（）。
            /*
public class Order {
    public OrderTypeEnum OrderType { get; set; }
}
public class OrderDto {
    public string OrderType { get; set; }
}
var orders = dbContext.Orders.ProjectTo<OrderDto>(configuration).ToList();
orders[0].OrderType.ShouldEqual("Online");
             */
        }

        /// <summary>
        /// Test_ExplicitExpansion.
        /// </summary>
        [Fact]
        public static void Test_ExplicitExpansion()
        {
            // 在某些情况下，例如OData，通过IQueryable控制器操作返回通用DTO。如果没有明确的指示，AutoMapper将展开结果中的所有成员。
            // 若要控制在投影期间展开哪些成员，请在配置中设置ExplicitExpansion，然后传入要显式展开的成员：
            /*
dbContext.Orders.ProjectTo<OrderDto>(configuration,
    dest => dest.Customer,
    dest => dest.LineItems);
// or string-based
dbContext.Orders.ProjectTo<OrderDto>(configuration,
    null,
    "Customer",
    "LineItems");
// for collections
dbContext.Orders.ProjectTo<OrderDto>(configuration,
    null,
    dest => dest.LineItems.Select(item => item.Product));
             */
        }

        /// <summary>
        /// Test_Aggregations.
        /// </summary>
        [Fact]
        public static void Test_Aggregations()
        {
            // LINQ可以支持聚合查询，AutoMapper支持LINQ扩展方法。在自定义投影示例中，如果我们将TotalContacts属性重命名为ContactsCount，
            // AutoMapper将匹配Count（）扩展方法，LINQ提供程序将计数转换为相关的子查询以聚合子记录。
            // 如果LINQ提供程序支持，AutoMapper还可以支持复杂的聚合和嵌套限制：
            /*
cfg.CreateMap<Course, CourseModel>()
    .ForMember(m => m.EnrollmentsStartingWithA,
          opt => opt.MapFrom(c => c.Enrollments.Where(e => e.Student.LastName.StartsWith("A")).Count()));
             */ // 此查询返回每个课程中姓氏以字母“A”开头的学生总数。
        }

        /// <summary>
        /// Test_Parameterization.
        /// </summary>
        [Fact]
        public static void Test_Parameterization()
        {
            // 偶尔，投影需要运行时参数作为其值。考虑一个需要将当前用户名作为其数据的一部分拉入的投影。我们可以参数化MapFrom配置，而不是使用post-mapping代码：
            /*
string currentUserName = null;
cfg.CreateMap<Course, CourseModel>()
    .ForMember(m => m.CurrentUserName, opt => opt.MapFrom(src => currentUserName));
             */

            // 当我们投影时，我们将在运行时替换我们的参数：
            /*
dbContext.Courses.ProjectTo<CourseModel>(Config, new { currentUserName = Request.User.Name });
             */

            // 它的工作原理是在原始表达式中捕获闭包的字段名，然后在查询发送到查询提供程序之前使用匿名对象/字典将该值应用于参数值。
        }
    }
}
