﻿// <copyright file="Source.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch16_OpenGenerics
{
    /// <summary>
    /// Source.
    /// </summary>
    /// <typeparam name="T">T.</typeparam>
    internal class Source<T>
    {
        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        public T Value { get; set; }
    }
}