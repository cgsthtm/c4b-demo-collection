﻿// <copyright file="OpenGenericsTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch16_OpenGenerics
{
    using global::AutoMapper;
    using Xunit;

    /// <summary>
    /// OpenGenericsTest.
    /// </summary>
    public class OpenGenericsTest
    {
        /// <summary>
        /// Test_OpenGenerics.
        /// </summary>
        [Fact]
        public static void Test_OpenGenerics()
        {
            // Create the mapping
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap(typeof(Source<>), typeof(Destination<>)));
            var mapper = configuration.CreateMapper();
            var source = new Source<int> { Value = 10 };
            var dest = mapper.Map<Source<int>, Destination<int>>(source);
            Assert.Equal(10, dest.Value);
        }
    }
}