﻿// <copyright file="Destination.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch16_OpenGenerics
{
    /// <summary>
    /// Destination.
    /// </summary>
    /// <typeparam name="T">T.</typeparam>
    internal class Destination<T>
    {
        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        public T Value { get; set; }
    }
}
