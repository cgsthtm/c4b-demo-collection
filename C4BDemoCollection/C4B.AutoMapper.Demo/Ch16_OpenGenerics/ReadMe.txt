﻿# Open Generics
AutoMapper可以支持开放的泛型类型映射。为开放泛型类型创建映射：
~~~
public class Source<T> {
    public T Value { get; set; }
}

public class Destination<T> {
    public T Value { get; set; }
}

// Create the mapping
var configuration = new MapperConfiguration(cfg => cfg.CreateMap(typeof(Source<>), typeof(Destination<>)));
~~~
您不需要为封闭泛型类型创建映射。AutoMapper将在运行时应用从开放泛型映射到封闭映射的任何配置：
~~~
var source = new Source<int> { Value = 10 };

var dest = mapper.Map<Source<int>, Destination<int>>(source);

dest.Value.ShouldEqual(10);
~~~
因为C#只允许封闭泛型类型参数，所以必须使用System.Type版本的泛型映射来创建开放泛型类型映射。从那里，您可以使用所有可用的映射配置，并且开放泛型配置将在运行时应用于封闭类型映射。
AutoMapper将在配置验证期间跳过打开的泛型类型映射，因为您仍然可以创建不转换的封闭类型，例如 Source<Foo> -> Destination<Bar> ，其中没有从Foo到Bar的转换。
您还可以创建一个开放的泛型类型转换器：
~~~
var configuration = new MapperConfiguration(cfg =>
   cfg.CreateMap(typeof(Source<>), typeof(Destination<>)).ConvertUsing(typeof(Converter<>)));
~~~
AutoMapper还支持带有任意数量泛型参数的开放泛型类型转换器：
~~~
var configuration = new MapperConfiguration(cfg =>
   cfg.CreateMap(typeof(Source<>), typeof(Destination<>)).ConvertUsing(typeof(Converter<,>)));
~~~
Source中的封闭类型将是第一个泛型参数，Destination的封闭类型将是第二个参数，以关闭Converter<，>。
同样的想法也适用于值解析器。检查这个测试 https://github.com/AutoMapper/AutoMapper/blob/e8249d582d384ea3b72eec31408126a0b69619bc/src/UnitTests/OpenGenerics.cs#L11。