﻿# Conditional Mapping
AutoMapper允许您向属性添加映射该属性之前必须满足的条件。这可以在下面这样的情况下使用，我们试图从一个int映射到一个unsigned int。
~~~
class Foo{
  public int baz;
}

class Bar {
  public uint baz;
}
~~~
在下面的映射中，只有当属性baz在源对象中大于或等于0时，它才会被映射。
~~~
var configuration = new MapperConfiguration(cfg => {
  cfg.CreateMap<Foo,Bar>()
    .ForMember(dest => dest.baz, opt => opt.Condition(src => (src.baz >= 0)));
});
~~~
如果你有一个解析器，请看这里的一个具体例子。https://docs.automapper.org/en/v10.1.1/Custom-value-resolvers.html#resolvers-and-conditions

## Preconditions
同样，还有一个PreCondition方法。不同之处在于它在映射过程中运行得更快，在源值被解析之前（想想MapFrom）。
所以前提条件被调用，然后我们决定哪个将是映射的源（解析），然后条件被调用，最后目标值被赋值。
~~~
var configuration = new MapperConfiguration(cfg => {
  cfg.CreateMap<Foo,Bar>()
    .ForMember(dest => dest.baz, opt => {
        opt.PreCondition(src => (src.baz >= 0));
        opt.MapFrom(src => {
            // Expensive resolution process that can be avoided with a PreCondition
        });
    });
});
~~~
你可以看到自己的步骤。https://docs.automapper.org/en/v10.1.1/Understanding-your-mapping.html
这里有一个具体的例子。https://docs.automapper.org/en/v10.1.1/Custom-value-resolvers.html#resolvers-and-conditions
