﻿// <copyright file="FlatteningTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch11_Flattening
{
    using global::AutoMapper;
    using Xunit;

    /// <summary>
    /// FlatteningTest.
    /// </summary>
    public class FlatteningTest
    {
        /// <summary>
        /// Test_Flattening.
        /// </summary>
        [Fact]
        public static void Test_Flattening()
        {
            // Complex model
            var customer = new Customer
            {
                Name = "George Costanza",
            };
            var order = new Order
            {
                Customer = customer,
            };
            var bosco = new Product
            {
                Name = "Bosco",
                Price = 4.99m,
            };
            order.AddOrderLineItem(bosco, 15);

            // Configure AutoMapper
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderDto>());
            var mapper = configuration.CreateMapper();

            // Perform mapping
            OrderDto dto = mapper.Map<Order, OrderDto>(order);

            Assert.Equal("George Costanza", dto.CustomerName);
            Assert.Equal(74.85m, dto.Total);

            // 在OrderDto类型上，Total属性与Order上的GetTotal（）方法匹配。CustomerName属性与订单上的Customer.Name属性匹配。
            // 只要我们正确地命名目标属性，就不需要配置单个属性匹配。

            // If you want to disable this behavior, you can use the ExactMatchNamingConvention:
            ////cfg.DestinationMemberNamingConvention = new ExactMatchNamingConvention();
        }

        /// <summary>
        /// Test_IncludeMembers.
        /// </summary>
        [Fact]
        public static void Test_IncludeMembers()
        {
            // 如果您在拼合时需要更多控制，可以使用IncludeMembers。如果已经有了从子类型到目标类型的映射，
            // 则可以将子对象的成员映射到目标对象（与不需要子类型映射的经典扁平化不同）。
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Source, Destination>().IncludeMembers(s => s.InnerSource, s => s.OtherInnerSource);
                cfg.CreateMap<InnerSource, Destination>(MemberList.None);
                cfg.CreateMap<OtherInnerSource, Destination>();
            });
            var source = new Source
            {
                Name = "name",
                InnerSource = new InnerSource { Description = "description" },
                OtherInnerSource = new OtherInnerSource { Title = "title" },
            };
            var mapper = configuration.CreateMapper();
            var destination = mapper.Map<Destination>(source);
            Assert.Equal("name", destination.Name);
            Assert.Equal("description", destination.Description);
            Assert.Equal("title", destination.Title);
        }
    }
}
