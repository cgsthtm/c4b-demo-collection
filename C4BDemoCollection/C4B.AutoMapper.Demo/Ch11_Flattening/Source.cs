﻿// <copyright file="Source.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch11_Flattening
{
    /// <summary>
    /// Source.
    /// </summary>
    internal class Source
    {
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets InnerSource.
        /// </summary>
        public InnerSource InnerSource { get; set; }

        /// <summary>
        /// Gets or sets OtherInnerSource.
        /// </summary>
        public OtherInnerSource OtherInnerSource { get; set; }
    }
}
