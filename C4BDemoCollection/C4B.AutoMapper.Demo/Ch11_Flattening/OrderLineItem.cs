﻿// <copyright file="OrderLineItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch11_Flattening
{
    /// <summary>
    /// OrderLineItem.
    /// </summary>
    internal class OrderLineItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderLineItem"/> class.
        /// </summary>
        /// <param name="product">product.</param>
        /// <param name="quantity">quantity.</param>
        public OrderLineItem(Product product, int quantity)
        {
            this.Product = product;
            this.Quantity = quantity;
        }

        /// <summary>
        /// Gets Product.
        /// </summary>
        public Product Product { get; private set; }

        /// <summary>
        /// Gets Quantity.
        /// </summary>
        public int Quantity { get; private set; }

        /// <summary>
        /// GetTotal.
        /// </summary>
        /// <returns>total.</returns>
        public decimal GetTotal()
        {
            return this.Quantity * this.Product.Price;
        }
    }
}
