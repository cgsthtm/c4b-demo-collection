﻿// <copyright file="OrderDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch11_Flattening
{
    /// <summary>
    /// OrderDto.
    /// </summary>
    internal class OrderDto
    {
        /// <summary>
        /// Gets or sets CustomerName.
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets Total.
        /// </summary>
        public decimal Total { get; set; }
    }
}
