﻿# Flattening
对象-对象映射的一个常见用法是获取复杂的对象模型并将其展平为更简单的模型。您可以使用复杂的模型，例如：
~~~
public class Order
{
	private readonly IList<OrderLineItem> _orderLineItems = new List<OrderLineItem>();

	public Customer Customer { get; set; }

	public OrderLineItem[] GetOrderLineItems()
	{
		return _orderLineItems.ToArray();
	}

	public void AddOrderLineItem(Product product, int quantity)
	{
		_orderLineItems.Add(new OrderLineItem(product, quantity));
	}

	public decimal GetTotal()
	{
		return _orderLineItems.Sum(li => li.GetTotal());
	}
}

public class Product
{
	public decimal Price { get; set; }
	public string Name { get; set; }
}

public class OrderLineItem
{
	public OrderLineItem(Product product, int quantity)
	{
		Product = product;
		Quantity = quantity;
	}

	public Product Product { get; private set; }
	public int Quantity { get; private set;}

	public decimal GetTotal()
	{
		return Quantity*Product.Price;
	}
}

public class Customer
{
	public string Name { get; set; }
}
~~~
我们希望将这个复杂的Order对象扁平化为一个更简单的OrderDto，它只包含特定场景所需的数据：
~~~
public class OrderDto
{
	public string CustomerName { get; set; }
	public decimal Total { get; set; }
}
~~~
在AutoMapper中配置源/目标类型对时，配置器会尝试将源类型上的属性和方法与目标类型上的属性进行匹配。如果对于目标类型上的任何属性，
源类型上不存在前缀为“Get”的属性、方法或方法，则AutoMapper将目标成员名称拆分为单个单词（按照PascalCase约定）。
~~~
// Complex model

var customer = new Customer
	{
		Name = "George Costanza"
	};
var order = new Order
	{
		Customer = customer
	};
var bosco = new Product
	{
		Name = "Bosco",
		Price = 4.99m
	};
order.AddOrderLineItem(bosco, 15);

// Configure AutoMapper

var configuration = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderDto>());

// Perform mapping

OrderDto dto = mapper.Map<Order, OrderDto>(order);

dto.CustomerName.ShouldEqual("George Costanza");
dto.Total.ShouldEqual(74.85m);
~~~
我们使用CreateMap方法在AutoMapper中配置了类型映射。AutoMapper只能映射它所知道的类型对，因此我们已经显式地将源/目的地类型对注册到AutoMapper。为了执行映射，我们使用Map方法。
在OrderDto类型上，Total属性与Order上的GetTotal（）方法匹配。CustomerName属性与订单上的Customer.Name属性匹配。只要我们正确地命名目标属性，就不需要配置单个属性匹配。
If you want to disable this behavior, you can use the ExactMatchNamingConvention: 
~~~
cfg.DestinationMemberNamingConvention = new ExactMatchNamingConvention();
~~

## IncludeMembers
如果您在拼合时需要更多控制，可以使用IncludeMembers。如果已经有了从子类型到目标类型的映射，则可以将子对象的成员映射到目标对象（与不需要子类型映射的经典扁平化不同）。
~~~
class Source
{
    public string Name { get; set; }
    public InnerSource InnerSource { get; set; }
    public OtherInnerSource OtherInnerSource { get; set; }
}
class InnerSource
{
    public string Name { get; set; }
    public string Description { get; set; }
}
class OtherInnerSource
{
    public string Name { get; set; }
    public string Description { get; set; }
    public string Title { get; set; }
}
class Destination
{
    public string Name { get; set; }
    public string Description { get; set; }
    public string Title { get; set; }
}

cfg.CreateMap<Source, Destination>().IncludeMembers(s=>s.InnerSource, s=>s.OtherInnerSource);
cfg.CreateMap<InnerSource, Destination>(MemberList.None);
cfg.CreateMap<OtherInnerSource, Destination>();

var source = new Source { Name = "name", InnerSource = new InnerSource{ Description = "description" }, 
                          OtherInnerSource = new OtherInnerSource{ Title = "title" } };
var destination = mapper.Map<Destination>(source);
destination.Name.ShouldBe("name");
destination.Description.ShouldBe("description");
destination.Title.ShouldBe("title");
~~~
因此，这允许您在映射父类型Source和Destination时重用子类型InnerSource和OtherInnerSource的现有映射中的配置。
它的工作方式类似于映射继承(https://docs.automapper.org/en/stable/Flattening.html#Mapping-inheritance.html)，但它使用组合，而不是继承。
IncludeMembers调用中参数的顺序是相关的。当映射目标成员时，第一个匹配将获胜，从源对象本身开始，然后按照指定的顺序映射包含的子对象。
所以在上面的例子中，Name映射自源对象本身，Description映射自InnerSource，因为它是第一个匹配。
请注意，这种匹配是静态的，它发生在配置时，而不是在Map时，因此不考虑子对象的运行时类型。
IncludeMembers与ReverseMap集成。包含的成员将被反转为
~~~
ForPath(destination => destination.IncludedMember, member => member.MapFrom(source => source))
~~~
反之亦然如果这不是您想要的，您可以避免ReverseMap（显式创建反向映射），或者您可以覆盖默认设置（分别使用不带参数的Ignore或IncludeMembers）。
有关详细信息，请查看测试(https://github.com/AutoMapper/AutoMapper/blob/master/src/UnitTests/IMappingExpression/IncludeMembers.cs)。