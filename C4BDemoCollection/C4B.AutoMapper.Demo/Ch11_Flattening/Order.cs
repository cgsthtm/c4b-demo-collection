﻿// <copyright file="Order.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch11_Flattening
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Order.
    /// </summary>
    internal class Order
    {
        private readonly IList<OrderLineItem> orderLineItems = new List<OrderLineItem>();

        /// <summary>
        /// Gets or sets Customer.
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// GetOrderLineItems.
        /// </summary>
        /// <returns>orderLineItems.</returns>
        public OrderLineItem[] GetOrderLineItems()
        {
            return this.orderLineItems.ToArray();
        }

        /// <summary>
        /// AddOrderLineItem.
        /// </summary>
        /// <param name="product">product.</param>
        /// <param name="quantity">quantity.</param>
        public void AddOrderLineItem(Product product, int quantity)
        {
            this.orderLineItems.Add(new OrderLineItem(product, quantity));
        }

        /// <summary>
        /// GetTotal.
        /// </summary>
        /// <returns>total.</returns>
        public decimal GetTotal()
        {
            return this.orderLineItems.Sum(li => li.GetTotal());
        }
    }
}