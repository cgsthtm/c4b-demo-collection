﻿# Lists and Arrays
AutoMapper只需要配置元素类型，而不需要配置可能使用的任何数组或列表类型。例如，我们可能有一个简单的源和目标类型：
~~~
public class Source
{
	public int Value { get; set; }
}

public class Destination
{
	public int Value { get; set; }
}
~~~
支持所有基本的泛型集合类型：
~~~
var configuration = new MapperConfiguration(cfg => cfg.CreateMap<Source, Destination>());

var sources = new[]
	{
		new Source { Value = 5 },
		new Source { Value = 6 },
		new Source { Value = 7 }
	};

IEnumerable<Destination> ienumerableDest = mapper.Map<Source[], IEnumerable<Destination>>(sources);
ICollection<Destination> icollectionDest = mapper.Map<Source[], ICollection<Destination>>(sources);
IList<Destination> ilistDest = mapper.Map<Source[], IList<Destination>>(sources);
List<Destination> listDest = mapper.Map<Source[], List<Destination>>(sources);
Destination[] arrayDest = mapper.Map<Source[], Destination[]>(sources);
~~~
具体来说，支持的源集合类型包括：
- IEnumerable
- IEnumerable<T>
- ICollection
- ICollection<T>
- IList
- IList<T>
- List<T>
- Arrays
对于非泛型的可赋值类型，只支持未映射的可赋值类型，因为AutoMapper无法“猜测”您试图映射的类型。如上面的例子所示，不需要显式配置列表类型，只需要配置它们的成员类型。
当映射到现有集合时，首先清除目标集合。如果这不是您想要的，请查看AutoMapper.Collection。https://github.com/AutoMapper/AutoMapper.Collection

## Handling null collections
映射集合属性时，如果源值为null，则AutoMapper将目标字段映射到空集合，而不是将目标值设置为null。这与实体框架和框架设计指南的行为一致，
即相信C#引用、数组、列表、集合、字典和IEnumerable永远不应为空。
通过在配置映射器时将AllowNullCollections属性设置为true，可以更改此行为。
~~~
var configuration = new MapperConfiguration(cfg => {
    cfg.AllowNullCollections = true;
    cfg.CreateMap<Source, Destination>();
});
~~~
该设置可以全局应用，并且可以使用AllowNull和DoNotAllowNull按配置文件和成员重写。

## Polymorphic element types in collections
很多时候，我们可能在源类型和目标类型中都有一个类型层次。AutoMapper支持多态数组和集合，因此如果找到派生的源/目标类型，则使用派生的源/目标类型。
~~~
public class ParentSource
{
	public int Value1 { get; set; }
}

public class ChildSource : ParentSource
{
	public int Value2 { get; set; }
}

public class ParentDestination
{
	public int Value1 { get; set; }
}

public class ChildDestination : ParentDestination
{
	public int Value2 { get; set; }
}
~~~
AutoMapper仍然需要为子映射进行显式配置，因为AutoMapper无法“猜测”要使用哪个特定的子目标映射。下面是上述类型的一个示例：
~~~
var configuration = new MapperConfiguration(c=> {
    c.CreateMap<ParentSource, ParentDestination>()
	     .Include<ChildSource, ChildDestination>();
    c.CreateMap<ChildSource, ChildDestination>();
});

var sources = new[]
	{
		new ParentSource(),
		new ChildSource(),
		new ParentSource()
	};

var destinations = mapper.Map<ParentSource[], ParentDestination[]>(sources);

destinations[0].ShouldBeInstanceOf<ParentDestination>();
destinations[1].ShouldBeInstanceOf<ChildDestination>();
destinations[2].ShouldBeInstanceOf<ParentDestination>();
~~~