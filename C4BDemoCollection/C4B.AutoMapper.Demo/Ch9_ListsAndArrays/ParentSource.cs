﻿// <copyright file="ParentSource.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch9_ListsAndArrays
{
    /// <summary>
    /// ParentSource.
    /// </summary>
    internal class ParentSource
    {
        /// <summary>
        /// Gets or sets Value1.
        /// </summary>
        public int Value1 { get; set; }
    }
}
