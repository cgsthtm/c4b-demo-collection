﻿// <copyright file="ListsAndArraysTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch9_ListsAndArrays
{
    using System.Collections.Generic;
    using System.Linq;
    using C4B.AutoMapper.Demo.Ch4_Configuration;
    using global::AutoMapper;
    using Xunit;

    /// <summary>
    /// ListsAndArraysTest.
    /// </summary>
    public class ListsAndArraysTest
    {
        /// <summary>
        /// Test_ListsAndArrays.
        /// </summary>
        [Fact]
        public static void Test_ListsAndArrays()
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<Source, Destination>());
            var sources = new[]
            {
                new Source { Value = 5 },
                new Source { Value = 6 },
                new Source { Value = 7 },
            };
            var mapper = configuration.CreateMapper();
            IEnumerable<Destination> ienumerableDest = mapper.Map<Source[], IEnumerable<Destination>>(sources);
            ICollection<Destination> icollectionDest = mapper.Map<Source[], ICollection<Destination>>(sources);
            IList<Destination> ilistDest = mapper.Map<Source[], IList<Destination>>(sources);
            List<Destination> listDest = mapper.Map<Source[], List<Destination>>(sources);
            Destination[] arrayDest = mapper.Map<Source[], Destination[]>(sources);
            Assert.Equal(3, ienumerableDest.Count());
            Assert.Equal(3, icollectionDest.Count());
            Assert.Equal(3, ilistDest.Count());
            Assert.Equal(3, listDest.Count());
            Assert.Equal(3, arrayDest.Length);
        }

        /// <summary>
        /// Test_HandlingNullCollections.
        /// </summary>
        [Fact]
        public static void Test_HandlingNullCollections()
        {
            // 映射集合属性时，如果源值为null，则AutoMapper将目标字段映射到空集合，而不是将目标值设置为null。这与实体框架和框架设计指南的行为一致，
            // 即相信C#引用、数组、列表、集合、字典和IEnumerable永远不应为空。
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = true; // 通过在配置映射器时将AllowNullCollections属性设置为true，可以更改此行为。
                cfg.CreateMap<Source, Destination>();
            });
            var sources = new[]
            {
                null,
                new Source { Value = 6 },
                new Source { Value = 7 },
            };
            var mapper = configuration.CreateMapper();
            IEnumerable<Destination> ienumerableDest = mapper.Map<Source[], IEnumerable<Destination>>(sources);
            Assert.Equal(3, ienumerableDest.Count());

            var configuration1 = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = false;
                cfg.CreateMap<Source, Destination>();
            });
            var mapper1 = configuration1.CreateMapper();
            IEnumerable<Destination> ienumerableDest1 = mapper1.Map<Source[], IEnumerable<Destination>>(sources);
            Assert.Equal(3, ienumerableDest1.Count());

            Source[] sources1 = null;
            IEnumerable<Destination> ienumerableDest2 = mapper1.Map<Source[], IEnumerable<Destination>>(sources1);
            Assert.NotNull(ienumerableDest2); // cfg.AllowNullCollections = false;
            Assert.Empty(ienumerableDest2);

            IEnumerable<Destination> ienumerableDest3 = mapper.Map<Source[], IEnumerable<Destination>>(sources1);
            Assert.Null(ienumerableDest3); // cfg.AllowNullCollections = true;
        }

        /// <summary>
        /// Test_PolymorphicElementTypesInCollections.
        /// </summary>
        [Fact]
        public static void Test_PolymorphicElementTypesInCollections()
        {
            var configuration = new MapperConfiguration(c =>
            {
                c.CreateMap<ParentSource, ParentDestination>()
                     .Include<ChildSource, ChildDestination>();
                c.CreateMap<ChildSource, ChildDestination>();
            });

            var sources = new[]
            {
                new ParentSource(),
                new ChildSource(),
                new ParentSource(),
            };
            var mapper = configuration.CreateMapper();
            var destinations = mapper.Map<ParentSource[], ParentDestination[]>(sources);
            ////destinations[0].ShouldBeInstanceOf<ParentDestination>();
            ////destinations[1].ShouldBeInstanceOf<ChildDestination>();
            ////destinations[2].ShouldBeInstanceOf<ParentDestination>();
        }
    }
}
