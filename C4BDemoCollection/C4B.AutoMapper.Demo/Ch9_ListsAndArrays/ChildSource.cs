﻿// <copyright file="ChildSource.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch9_ListsAndArrays
{
    /// <summary>
    /// ChildSource.
    /// </summary>
    internal class ChildSource : ParentSource
    {
        /// <summary>
        /// Gets or sets Value2.
        /// </summary>
        public int Value2 { get; set; }
    }
}
