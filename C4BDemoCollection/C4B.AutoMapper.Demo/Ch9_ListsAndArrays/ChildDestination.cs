﻿// <copyright file="ChildDestination.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch9_ListsAndArrays
{
    /// <summary>
    /// ChildDestination.
    /// </summary>
    internal class ChildDestination : ParentDestination
    {
        /// <summary>
        /// Gets or sets Value2.
        /// </summary>
        public int Value2 { get; set; }
    }
}
