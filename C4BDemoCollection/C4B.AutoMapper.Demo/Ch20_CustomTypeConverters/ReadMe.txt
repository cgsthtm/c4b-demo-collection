﻿# Custom Type Converters
有时，您需要完全控制一种类型到另一种类型的转换。这通常是当一个类型看起来与另一个完全不同时，转换函数已经存在，并且您希望从“较宽松”的类型转换为较强的类型，
例如从源类型string转换为目标类型Int32。例如，假设我们有一个源类型：
~~~
public class Source
{
	public string Value1 { get; set; }
	public string Value2 { get; set; }
	public string Value3 { get; set; }
}
~~~
但您希望将其映射到：
~~~
public class Destination
{
	public int Value1 { get; set; }
	public DateTime Value2 { get; set; }
	public Type Value3 { get; set; }
}
~~~
如果我们尝试按原样映射这两个类型，AutoMapper将抛出异常（在映射时和配置检查时），因为AutoMapper不知道从string到int、DateTime或Type的任何映射。
要为这些类型创建映射，我们必须提供自定义类型转换器，我们有三种方法：
~~~
void ConvertUsing(Func<TSource, TDestination> mappingFunction);
void ConvertUsing(ITypeConverter<TSource, TDestination> converter);
void ConvertUsing<TTypeConverter>() where TTypeConverter : ITypeConverter<TSource, TDestination>;
~~~
第一个选项是任何接受源并返回目标的函数（也有几个重载）。这适用于简单的情况，但对于较大的情况变得笨拙。
在更困难的情况下，我们可以创建自定义ITypeConverter<TSource，TDestination>：
~~~
public interface ITypeConverter<in TSource, TDestination>
{
	TDestination Convert(TSource source, TDestination destination, ResolutionContext context);
}
~~~
并为AutoMapper提供自定义类型转换器的实例，或仅提供AutoMapper将在运行时实例化的类型。我们上面的源/目标类型的映射配置然后变为：
~~~
[Test]
public void Example()
{
    var configuration = new MapperConfiguration(cfg => {
      cfg.CreateMap<string, int>().ConvertUsing(s => Convert.ToInt32(s));
      cfg.CreateMap<string, DateTime>().ConvertUsing(new DateTimeTypeConverter());
      cfg.CreateMap<string, Type>().ConvertUsing<TypeTypeConverter>();
      cfg.CreateMap<Source, Destination>();
    });
    configuration.AssertConfigurationIsValid();

    var source = new Source
    {
        Value1 = "5",
        Value2 = "01/01/2000",
        Value3 = "AutoMapperSamples.GlobalTypeConverters.GlobalTypeConverters+Destination"
    };

    Destination result = mapper.Map<Source, Destination>(source);
    result.Value3.ShouldEqual(typeof(Destination));
}

public class DateTimeTypeConverter : ITypeConverter<string, DateTime>
{
    public DateTime Convert(string source, DateTime destination, ResolutionContext context)
    {
        return System.Convert.ToDateTime(source);
    }
}

public class TypeTypeConverter : ITypeConverter<string, Type>
{
    public Type Convert(string source, Type destination, ResolutionContext context)
    {
          return Assembly.GetExecutingAssembly().GetType(source);
    }
}
~~~
在第一个映射中，从string到Int32，我们简单地使用内置的Convert.ToInt32函数（作为方法组提供）。接下来的两个使用自定义ITypeConverter实现。
自定义类型转换器的真实的威力在于，AutoMapper在任何映射类型上找到源/目标对时都可以使用它们。我们可以构建一组自定义类型转换器，其他映射配置在其上使用，而不需要任何额外的配置。
在上面的例子中，我们再也不需要指定string/int转换了。当自定义值解析程序必须在类型成员级别配置时，自定义类型转换器在范围上是全局的。
https://docs.automapper.org/en/v10.1.1/Custom-value-resolvers.html

## System Type Converters
.NET Framework还通过TypeConverter类支持类型转换器的概念。AutoMapper在配置检查和映射中支持这些类型的类型转换器，而无需任何手动配置。
AutoMapper使用TypeDescriptor.GetConverter方法来确定是否可以映射源/目标类型对。
http://msdn.microsoft.com/en-us/library/system.componentmodel.typeconverter.aspx
http://msdn.microsoft.com/en-us/library/system.componentmodel.typedescriptor.getconverter.aspx