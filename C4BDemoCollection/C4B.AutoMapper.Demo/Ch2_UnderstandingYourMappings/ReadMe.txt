﻿# Understanding Your Mappings
AutoMapper为您的映射创建执行计划。在调试期间，可以将该执行计划视为表达式树(https://msdn.microsoft.com/en-us/library/mt654263.aspx?f=255&amp;MSPPError=-2147217396)。
您可以通过安装ReadableExpressions VS扩展(https://marketplace.visualstudio.com/items?itemName=vs-publisher-1232914.ReadableExpressionsVisualizers)来更好地查看结果代码。
如果你需要查看VS之外的代码，你可以直接使用ReadableExpressions包(https://www.nuget.org/packages/AgileObjects.ReadableExpressions)。
这个DotNetFiddle(https://dotnetfiddle.net/aJYTGZ)有一个使用NuGet包的实时演示，本文介绍了如何使用VS扩展。
https://agileobjects.co.uk/view-automapper-execution-plan-readableexpressions
~~~
var configuration = new MapperConfiguration(cfg => cfg.CreateMap<Foo, Bar>());
var executionPlan = configuration.BuildExecutionPlan(typeof(Foo), typeof(Bar));
~~~
请确保在发布前删除所有此类代码。
对于ProjectTo，您需要检查IQueryable.Expression。
~~~
var expression = context.Entities.ProjectTo<Dto>().Expression;
~~~