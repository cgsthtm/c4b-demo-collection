﻿// <copyright file="Address.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch2_UnderstandingYourMappings
{
    /// <summary>
    /// Address.
    /// </summary>
    internal class Address
    {
        /// <summary>
        /// Gets or sets Line1.
        /// </summary>
        public string Line1 { get; set; }
    }
}
