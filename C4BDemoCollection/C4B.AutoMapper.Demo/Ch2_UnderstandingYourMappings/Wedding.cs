﻿// <copyright file="Wedding.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch2_UnderstandingYourMappings
{
    using System;

    /// <summary>
    /// Wedding.
    /// </summary>
    internal class Wedding
    {
        /// <summary>
        /// Gets or sets Date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets Bride.
        /// </summary>
        public Person Bride { get; set; }

        /// <summary>
        /// Gets or sets Groom.
        /// </summary>
        public Person Groom { get; set; }
    }
}
