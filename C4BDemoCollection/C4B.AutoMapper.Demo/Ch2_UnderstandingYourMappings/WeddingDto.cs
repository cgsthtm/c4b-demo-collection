﻿// <copyright file="WeddingDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch2_UnderstandingYourMappings
{
    using System;

    /// <summary>
    /// WeddingDto.
    /// </summary>
    internal class WeddingDto
    {
        /// <summary>
        /// Gets or sets Date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets BrideTitle.
        /// </summary>
        public string BrideTitle { get; set; }

        /// <summary>
        /// Gets or sets BrideName.
        /// </summary>
        public string BrideName { get; set; }

        /// <summary>
        /// Gets or sets BrideAddressLine1.
        /// </summary>
        public string BrideAddressLine1 { get; set; }

        /// <summary>
        /// Gets or sets GroomTitle.
        /// </summary>
        public string GroomTitle { get; set; }

        /// <summary>
        /// Gets or sets GroomName.
        /// </summary>
        public string GroomName { get; set; }

        /// <summary>
        /// Gets or sets GroomAddressLine1.
        /// </summary>
        public string GroomAddressLine1 { get; set; }
    }
}
