﻿// <copyright file="Person.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch2_UnderstandingYourMappings
{
    /// <summary>
    /// Person.
    /// </summary>
    internal class Person
    {
        /// <summary>
        /// Gets or sets Title.
        /// </summary>
        public Title Title { get; set; }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Address.
        /// </summary>
        public Address Address { get; set; }
    }
}
