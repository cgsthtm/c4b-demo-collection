﻿// <copyright file="UnderstandingYourMappingsTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch2_UnderstandingYourMappings
{
    using System;
    using AgileObjects.ReadableExpressions;
    using global::AutoMapper;
    using Serilog;

    /// <summary>
    /// UnderstandingYourMappingsTest.
    /// </summary>
    internal class UnderstandingYourMappingsTest
    {
        /// <summary>
        /// Test_BuildExecutionPlan.
        /// </summary>
        public static void Test_BuildExecutionPlan()
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<Foo, Bar>());
            var executionPlan = configuration.BuildExecutionPlan(typeof(Foo), typeof(Bar));
            var description = executionPlan.ToReadableString();
            Log.Information($"executionPlan.ToReadableString(): {description}");
            var mapper = configuration.CreateMapper();
            var foo = new Foo() { Name = "foo1" };
            var barDto = mapper.Map<Bar>(foo);
            Log.Information("BarDto: {@bar}", barDto);

            // For ProjectTo, you need to inspect IQueryable.Expression.
            ////var expression = context.Entities.ProjectTo<Dto>().Expression;

            // https://dotnetfiddle.net/aJYTGZ
            var configuration1 = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Wedding, WeddingDto>();
            });
            var executionPlan1 = configuration1.BuildExecutionPlan(typeof(Wedding), typeof(WeddingDto));
            var description1 = executionPlan1.ToReadableString();
            Log.Information($"executionPlan1.ToReadableString(): {description1}");
            var mapper1 = configuration1.CreateMapper();
            var wedding = new Wedding()
            {
                Date = DateTime.Now,
                Bride = new Person()
                {
                    Title = Title.Miss,
                    Name = "zhh",
                    Address = new Address()
                    {
                        Line1 = "shenzi",
                    },
                },
                Groom = new Person()
                {
                    Title = Title.Mr,
                    Name = "cgs",
                    Address = new Address()
                    {
                        Line1 = "matuo",
                    },
                },
            };
            var weddingDto = mapper1.Map<WeddingDto>(wedding);
            Log.Information("WeddingDto: {@weddingDto}", weddingDto);
        }
    }
}
