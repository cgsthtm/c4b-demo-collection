﻿// <copyright file="Title.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch2_UnderstandingYourMappings
{
    /// <summary>
    /// Title.
    /// </summary>
    internal enum Title
    {
        /// <summary>
        /// Other.
        /// </summary>
        Other,

        /// <summary>
        /// Mr.
        /// </summary>
        Mr,

        /// <summary>
        /// Ms.
        /// </summary>
        Ms,

        /// <summary>
        /// Miss.
        /// </summary>
        Miss,

        /// <summary>
        /// Mrs.
        /// </summary>
        Mrs,

        /// <summary>
        /// Dr.
        /// </summary>
        Dr,
    }
}
