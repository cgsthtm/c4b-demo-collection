﻿# Value Converters
值转换器是类型转换器(https://docs.automapper.org/en/v10.1.1/Custom-type-converters.html)和
值解析器(https://docs.automapper.org/en/v10.1.1/Custom-value-resolvers.html)之间的交叉。
类型转换器是全局作用域的，因此在任何映射中从类型Foo映射到类型Bar时，都将使用类型转换器。值转换器的作用域为单个映射，并接收源和目标对象以解析为要映射到目标成员的值。
可选地，值转换器也可以接收源成员。
在简化的语法中：
- Type converter = Func<TSource, TDestination, TDestination>
- Value resolver = Func<TSource, TDestination, TDestinationMember>
- Member value resolver = Func<TSource, TDestination, TSourceMember, TDestinationMember>
- Value converter = Func<TSourceMember, TDestinationMember>
要配置值转换器，请在成员级别使用：
~~~
public class CurrencyFormatter : IValueConverter<decimal, string> {
    public string Convert(decimal source, ResolutionContext context)
        => source.ToString("c");
}

var configuration = new MapperConfiguration(cfg => {
   cfg.CreateMap<Order, OrderDto>()
       .ForMember(d => d.Amount, opt => opt.ConvertUsing(new CurrencyFormatter()));
   cfg.CreateMap<OrderLineItem, OrderLineItemDto>()
       .ForMember(d => d.Total, opt => opt.ConvertUsing(new CurrencyFormatter()));
});
~~~
当源成员名称不匹配时，可以自定义源成员：
~~~
public class CurrencyFormatter : IValueConverter<decimal, string> {
    public string Convert(decimal source, ResolutionContext context)
        => source.ToString("c");
}

var configuration = new MapperConfiguration(cfg => {
   cfg.CreateMap<Order, OrderDto>()
       .ForMember(d => d.Amount, opt => opt.ConvertUsing(new CurrencyFormatter(), src => src.OrderAmount));
   cfg.CreateMap<OrderLineItem, OrderLineItemDto>()
       .ForMember(d => d.Total, opt => opt.ConvertUsing(new CurrencyFormatter(), src => src.LITotal));
});
~~~
如果您需要由服务定位器(https://docs.automapper.org/en/v10.1.1/Dependency-injection.html)实例化的值转换器，则可以指定类型：
~~~
public class CurrencyFormatter : IValueConverter<decimal, string> {
    public string Convert(decimal source, ResolutionContext context)
        => source.ToString("c");
}

var configuration = new MapperConfiguration(cfg => {
   cfg.CreateMap<Order, OrderDto>()
       .ForMember(d => d.Amount, opt => opt.ConvertUsing<CurrencyFormatter, decimal>());
   cfg.CreateMap<OrderLineItem, OrderLineItemDto>()
       .ForMember(d => d.Total, opt => opt.ConvertUsing<CurrencyFormatter, decimal>());
});
~~~
如果在运行时不知道类型或成员名称，请使用接受System.Type和基于字符串的成员的各种重载：
~~~
public class CurrencyFormatter : IValueConverter<decimal, string> {
    public string Convert(decimal source, ResolutionContext context)
        => source.ToString("c");
}

var configuration = new MapperConfiguration(cfg => {
   cfg.CreateMap(typeof(Order), typeof(OrderDto))
       .ForMember("Amount", opt => opt.ConvertUsing(new CurrencyFormatter(), "OrderAmount"));
   cfg.CreateMap(typeof(OrderLineItem), typeof(OrderLineItemDto))
       .ForMember("Total", opt => opt.ConvertUsing(new CurrencyFormatter(), "LITotal"));
});
~~~
值转换器仅用于内存映射执行。他们不会为ProjectTo工作。