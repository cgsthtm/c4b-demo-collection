﻿# Construction
AutoMapper可以基于源成员映射到目标构造函数：
~~~
public class Source {
    public int Value { get; set; }
}
public class SourceDto {
    public SourceDto(int value) {
        _value = value;
    }
    private int _value;
    public int Value {
        get { return _value; }
    }
}
var configuration = new MapperConfiguration(cfg => cfg.CreateMap<Source, SourceDto>());
~~~
如果目标构造函数参数名称不匹配，您可以在配置时修改它们：
~~~
public class Source {
    public int Value { get; set; }
}
public class SourceDto {
    public SourceDto(int valueParamSomeOtherName) {
        _value = valueParamSomeOtherName;
    }
    private int _value;
    public int Value {
        get { return _value; }
    }
}
var configuration = new MapperConfiguration(cfg =>
  cfg.CreateMap<Source, SourceDto>()
    .ForCtorParam("valueParamSomeOtherName", opt => opt.MapFrom(src => src.Value))
);
~~~
这对LINQ投影和内存映射都有效。
您也可以禁用构造函数映射：
~~~
var configuration = new MapperConfiguration(cfg => cfg.DisableConstructorMapping());
~~~
您可以配置为目标对象考虑哪些构造函数：
~~~
// use only public constructors
var configuration = new MapperConfiguration(cfg => cfg.ShouldUseConstructor = constructor => constructor.IsPublic);
~~~
当映射到记录时，考虑只使用公共构造函数。