﻿// <copyright file="ConstructionTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch10_Construction
{
    using global::AutoMapper;

    /// <summary>
    /// ConstructionTest.
    /// </summary>
    internal class ConstructionTest
    {
        /// <summary>
        /// Test_Construction.
        /// </summary>
        public static void Test_Construction()
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<Source, SourceDto>());
        }

        /// <summary>
        /// Test_IfConstructionParameterDontMatch.
        /// </summary>
        public static void Test_IfConstructionParameterDontMatch()
        {
            // 如果目标构造函数参数名称不匹配，您可以在配置时修改它们：
            var configuration = new MapperConfiguration(cfg =>
                cfg.CreateMap<Source, SourceDto1>()
                .ForCtorParam("valueParamSomeOtherName", opt => opt.MapFrom(src => src.Value)));

            // 您也可以禁用构造函数映射：
            var configuration1 = new MapperConfiguration(cfg => cfg.DisableConstructorMapping());

            // 您可以配置为目标对象考虑哪些构造函数：
            // use only public constructors
            var configuration2 = new MapperConfiguration(cfg => cfg.ShouldUseConstructor = constructor => constructor.IsPublic);
        }
    }
}
