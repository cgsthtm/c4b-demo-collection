﻿// <copyright file="SourceDto1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch10_Construction
{
    /// <summary>
    /// SourceDto1.
    /// </summary>
    internal class SourceDto1
    {
        private int value;

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceDto1"/> class.
        /// </summary>
        /// <param name="valueParamSomeOtherName">valueParamSomeOtherName.</param>
        public SourceDto1(int valueParamSomeOtherName)
        {
            this.value = valueParamSomeOtherName;
        }

        /// <summary>
        /// Gets Value.
        /// </summary>
        public int Value
        {
            get { return this.value; }
        }
    }
}
