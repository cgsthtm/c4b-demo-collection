﻿// <copyright file="SourceDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch10_Construction
{
    /// <summary>
    /// SourceDto.
    /// </summary>
    internal class SourceDto
    {
        private int value;

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceDto"/> class.
        /// </summary>
        /// <param name="value">value.</param>
        public SourceDto(int value)
        {
            this.value = value;
        }

        /// <summary>
        /// Gets Value.
        /// </summary>
        public int Value
        {
            get { return this.value; }
        }
    }
}
