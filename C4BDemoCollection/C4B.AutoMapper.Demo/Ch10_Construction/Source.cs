﻿// <copyright file="Source.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch10_Construction
{
    /// <summary>
    /// Source.
    /// </summary>
    internal class Source
    {
        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        public int Value { get; set; }
    }
}
