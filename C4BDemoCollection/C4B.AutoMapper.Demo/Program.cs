﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using C4B.AutoMapper.Demo.Ch1_GettingStartedGuide;
    using C4B.AutoMapper.Demo.Ch10_Construction;
    using C4B.AutoMapper.Demo.Ch11_Flattening;
    using C4B.AutoMapper.Demo.Ch12_ReverseMappingAndUnflattening;
    using C4B.AutoMapper.Demo.Ch13_MappingInheritance;
    using C4B.AutoMapper.Demo.Ch14_AttributeMapping;
    using C4B.AutoMapper.Demo.Ch15_DynamicAndExpandoObjectMapping;
    using C4B.AutoMapper.Demo.Ch16_OpenGenerics;
    using C4B.AutoMapper.Demo.Ch17_QueryableExtensions;
    using C4B.AutoMapper.Demo.Ch2_UnderstandingYourMappings;
    using C4B.AutoMapper.Demo.Ch4_Configuration;
    using C4B.AutoMapper.Demo.Ch5_ConfigurationValidation;
    using C4B.AutoMapper.Demo.Ch7_Projection;
    using C4B.AutoMapper.Demo.Ch8_NestedMappings;
    using C4B.AutoMapper.Demo.Ch9_ListsAndArrays;
    using global::AutoMapper;
    using global::AutoMapper.QueryableExtensions;
    using Serilog;

    /// <summary>
    /// Program.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration().WriteTo.Console().CreateLogger();
            IMapper mapper = null;
            MapperConfiguration configuration = null;

            // Getting Started Guide | https://docs.automapper.org/en/latest/Getting-started.html
            GettingStartedGuideTest.Test_UseAutoMapper();
            GettingStartedGuideTest.Test_TestMyMappings();

            // Understanding Your Mappings | https://docs.automapper.org/en/latest/Understanding-your-mapping.html
            UnderstandingYourMappingsTest.Test_BuildExecutionPlan();

            // Configuration | https://docs.automapper.org/en/latest/Configuration.html
            ConfigurationTest.Test_ProfileInstances();
            ConfigurationTest.Test_AssemblyScanningForAutoConfiguration();
            ConfigurationTest.Test_NamingConventions();
            ConfigurationTest.Test_ReplacingCharacters();
            ConfigurationTest.Test_RecognizingPrePostfixes();
            ConfigurationTest.Test_GlobalPropertyFieldFiltering();
            ConfigurationTest.Test_ConfiguringVisibility();
            ConfigurationTest.Test_ConfigurationComlilation();

            // Configuration Validation | https://docs.automapper.org/en/latest/Configuration-validation.html
            ConfigurationValidationTest.Test_AssertConfigurationIsValid();
            ConfigurationValidationTest.Test_OverridingConfigurationErrors();
            ConfigurationValidationTest.Test_SelectingMembersToValidate();

            // Dependency Injection | https://docs.automapper.org/en/latest/Dependency-injection.html

            // Projection | https://docs.automapper.org/en/latest/Projection.html
            ProjectionTest.Test_Projection();

            // Nested Mappings | https://docs.automapper.org/en/latest/Nested-mappings.html
            NestedMappingsTest.Test_NestedMappings();

            // Lists and Arrays | https://docs.automapper.org/en/latest/Lists-and-arrays.html
            ListsAndArraysTest.Test_ListsAndArrays();
            ListsAndArraysTest.Test_HandlingNullCollections();
            ListsAndArraysTest.Test_PolymorphicElementTypesInCollections();

            // Construction | https://docs.automapper.org/en/latest/Construction.html
            ConstructionTest.Test_Construction();
            ConstructionTest.Test_IfConstructionParameterDontMatch();

            // Flattening | https://docs.automapper.org/en/latest/Flattening.html
            FlatteningTest.Test_Flattening();
            FlatteningTest.Test_IncludeMembers();

            // Reverse Mapping and Unflattening | https://docs.automapper.org/en/latest/Reverse-Mapping-and-Unflattening.html
            ReverseMappingAndUnflatteningTest.Test_ReverseMappingAndUnflattening();
            ReverseMappingAndUnflatteningTest.Test_CustomizingReverseMapping();
            ReverseMappingAndUnflatteningTest.Test_IncludeMembers();

            // Mapping Inheritance | https://docs.automapper.org/en/latest/Mapping-inheritance.html
            MappingInheritanceTest.Test_MappingInheritance();
            MappingInheritanceTest.Test_RuntimePolymorphism();
            MappingInheritanceTest.Test_SpecifyingInheritanceInDerivedClasses();
            MappingInheritanceTest.Test_As();
            MappingInheritanceTest.Test_InheritanceMappingProperties();

            // Attribute Mapping | https://docs.automapper.org/en/latest/Attribute-mapping.html
            AttributeMappingTest.Test_TypeMapConfiguration();
            AttributeMappingTest.Test_MemberConfiguration();

            // Dynamic and ExpandoObject Mapping??? | https://docs.automapper.org/en/latest/Dynamic-and-ExpandoObject-Mapping.html
            DynamicAndExpandoObjectMappingTest.Test_DynamicAndExpandoObjectMapping();

            // Open Generics | https://docs.automapper.org/en/latest/Open-Generics.html
            OpenGenericsTest.Test_OpenGenerics();

            // Queryable Extensions | https://docs.automapper.org/en/latest/Queryable-Extensions.html
            QueryableExtensionsTest.Test_QueryableExtensions();
            QueryableExtensionsTest.Test_CustomProjection();
            Console.ReadLine();

            #region Expression Translation (UseAsDataSource) | https://docs.automapper.org/en/latest/Expression-Translation-(UseAsDataSource).html

            #endregion Expression Translation (UseAsDataSource) | https://docs.automapper.org/en/latest/Expression-Translation-(UseAsDataSource).html

            #region AutoMapper.Extensions.EnumMapping | https://docs.automapper.org/en/latest/Enum-Mapping.html

            #endregion AutoMapper.Extensions.EnumMapping | https://docs.automapper.org/en/latest/Enum-Mapping.html

            Console.ReadLine();
        }
    }
}