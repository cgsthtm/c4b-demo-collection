﻿# Custom Value Resolvers
尽管AutoMapper涵盖了相当多的目标成员映射场景，但仍有1%到5%的目标值需要在解析时提供一些帮助。很多时候，这种自定义值解析逻辑是可以直接进入我们域的域逻辑。
但是，如果这个逻辑只与映射操作有关，那么它会使我们的源类型与不必要的行为混杂在一起。在这些情况下，AutoMapper允许为目标成员配置自定义值解析程序。
例如，我们可能希望在映射过程中有一个计算值：
~~~
public class Source
{
	public int Value1 { get; set; }
	public int Value2 { get; set; }
}

public class Destination
{
	public int Total { get; set; }
}
~~~
无论出于何种原因，我们都希望Total是源Value属性的总和。由于其他一些原因，我们不能或不应该把这个逻辑放在我们的Source类型上。
要提供自定义值解析器，我们需要首先创建一个实现IValueResolver的类型：
~~~
public interface IValueResolver<in TSource, in TDestination, TDestMember>
{
	TDestMember Resolve(TSource source, TDestination destination, TDestMember destMember, ResolutionContext context);
}
~~~
ResolutionContext包含当前解析操作的所有上下文信息，如源类型、目标类型、源值等。示例实现：
~~~
public class CustomResolver : IValueResolver<Source, Destination, int>
{
	public int Resolve(Source source, Destination destination, int member, ResolutionContext context)
	{
        return source.Value1 + source.Value2;
	}
}
~~~
一旦我们有了IValueResolver实现，我们就需要告诉AutoMapper在解析特定的目标成员时使用这个自定义值解析器。我们有几个选项可以告诉AutoMapper要使用的自定义值解析器，包括：
- MapFrom<TValueResolver>
- MapFrom(typeof(CustomValueResolver))
- MapFrom(aValueResolverInstance)
在下面的示例中，我们将使用第一个选项，通过泛型告诉AutoMapper自定义解析器类型：
~~~
var configuration = new MapperConfiguration(cfg =>
   cfg.CreateMap<Source, Destination>()
	 .ForMember(dest => dest.Total, opt => opt.MapFrom<CustomResolver>()));
configuration.AssertConfigurationIsValid();

var source = new Source
	{
		Value1 = 5,
		Value2 = 7
	};

var result = mapper.Map<Source, Destination>(source);

result.Total.ShouldEqual(12);
~~~
尽管目标成员（Total）没有任何匹配的源成员，但指定自定义冲突解决程序会使配置有效，因为冲突解决程序现在负责为目标成员提供值。
如果我们不关心值解析器中的源/目标类型，或者想在映射中重用它们，我们可以使用“object”作为源/目标类型：
~~~
public class MultBy2Resolver : IValueResolver<object, object, int> {
    public int Resolve(object source, object dest, int destMember, ResolutionContext context) {
        return destMember * 2;
    }
}
~~~

## Custom constructor methods
因为我们只向AutoMapper提供了自定义解析器的类型，所以映射引擎将使用反射来创建值解析器的实例。
如果我们不希望AutoMapper使用反射来创建实例，我们可以直接提供它：
~~~
var configuration = new MapperConfiguration(cfg => cfg.CreateMap<Source, Destination>()
	.ForMember(dest => dest.Total,
		opt => opt.MapFrom(new CustomResolver())
	));
~~~
AutoMapper将使用该特定对象，这在解析器可能具有构造函数参数或需要由IoC容器构造的情况下很有帮助。

## The resolved value is mapped to the destination property
请注意，从解析器返回的值并不是简单地分配给目标属性。将使用任何适用的映射，该映射的结果将是最终的目标属性值。检查执行计划。
https://docs.automapper.org/en/v10.1.1/Understanding-your-mapping.html

## Customizing the source value supplied to the resolver
默认情况下，AutoMapper将源对象传递给冲突解决程序。这限制了冲突解决程序的可重用性，因为冲突解决程序与源类型耦合。但是，如果我们在多个类型中提供一个公共解析器，
我们将AutoMapper配置为重定向提供给解析器的源值，并使用不同的解析器接口，以便解析器可以使用源/目标成员：
~~~
var configuration = new MapperConfiguration(cfg => {
cfg.CreateMap<Source, Destination>()
    .ForMember(dest => dest.Total,
        opt => opt.MapFrom<CustomResolver, decimal>(src => src.SubTotal));
cfg.CreateMap<OtherSource, OtherDest>()
    .ForMember(dest => dest.OtherTotal,
        opt => opt.MapFrom<CustomResolver, decimal>(src => src.OtherSubTotal));
});

public class CustomResolver : IMemberValueResolver<object, object, decimal, decimal> {
    public decimal Resolve(object source, object destination, decimal sourceMember, decimal destinationMember, ResolutionContext context) {
        // logic here
    }
}
~~~

## Passing in key-value to Mapper
当调用map时，你可以通过使用键值和使用自定义解析器从上下文中获取对象来传递额外的对象。
~~~
mapper.Map<Source, Dest>(src, opt => opt.Items["Foo"] = "Bar");
~~~
这是如何设置此自定义解析程序的映射
~~~
cfg.CreateMap<Source, Dest>()
    .ForMember(dest => dest.Foo, opt => opt.MapFrom((src, dest, destMember, context) => context.Items["Foo"]));
~~~

## ForPath
与ForMember类似，从6.1.0开始有ForPath。查看测试示例。https://github.com/AutoMapper/AutoMapper/search?utf8=%E2%9C%93&q=ForPath&type=

## Resolvers and conditions
对于每个属性映射，AutoMapper会在计算条件之前尝试解析目标值。因此，它需要能够做到这一点，而不会引发异常，即使该条件将阻止使用结果值。
作为一个示例，下面是BuildExecutionPlan(https://docs.automapper.org/en/v10.1.1/Understanding-your-mapping.html)
的单个属性的示例输出（使用ReadableExpressions(https://marketplace.visualstudio.com/items?itemName=vs-publisher-1232914.ReadableExpressionsVisualizers)显示）：
~~~
try
{
	var resolvedValue =
	{
		try
		{
			return // ... tries to resolve the destination value here
		}
		catch (NullReferenceException)
		{
			return null;
		}
		catch (ArgumentNullException)
		{
			return null;
		}
	};

	if (condition.Invoke(src, typeMapDestination, resolvedValue))
	{
		typeMapDestination.WorkStatus = resolvedValue;
	}
}
catch (Exception ex)
{
	throw new AutoMapperMappingException(
		"Error mapping types.",
		ex,
		AutoMapper.TypePair,
		AutoMapper.TypeMap,
		AutoMapper.PropertyMap);
};
~~~
如果您还没有为该成员自定义映射，则用于解析属性的默认生成代码通常不会有任何问题。但是，如果您使用自定义代码来映射在不满足条件时将崩溃的属性，那么尽管存在条件，映射也会失败。
此示例代码将失败：
~~~
public class SourceClass 
{ 
	public string Value { get; set; }
}

public class TargetClass 
{
	public int ValueLength { get; set; }
}

// ...

var source = new SourceClass { Value = null };
var target = new TargetClass;

CreateMap<SourceClass, TargetClass>()
	.ForMember(d => d.ValueLength, o => o.MapFrom(s => s.Value.Length))
	.ForAllMembers(o => o.Condition((src, dest, value) => value != null));
~~~
该条件阻止Value属性映射到目标，但自定义成员映射将在该点之前失败，因为它调用Value.Length，而Value为null。
通过使用PreCondition(https://docs.automapper.org/en/v10.1.1/Conditional-mapping.html#preconditions)或确保自定义成员映射代码可以安全地完成而不考虑条件来防止这种情况：
~~~
	.ForMember(d => d.ValueLength, o => o.MapFrom(s => s != null ? s.Value.Length : 0))
~~~
