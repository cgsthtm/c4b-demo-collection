﻿# Value Transformers
值转换器将附加转换应用于单个类型。在分配值之前，AutoMapper将检查要设置的值是否具有任何关联的值转换，并在设置之前应用它们。
您可以在几个不同的级别创建值转换器：
- Globally
- Profile
- Map
- Member
~~~
var configuration = new MapperConfiguration(cfg => {
    cfg.ValueTransformers.Add<string>(val => val + "!!!");
});

var source = new Source { Value = "Hello" };
var dest = mapper.Map<Dest>(source);

dest.Value.ShouldBe("Hello!!!");
~~~
