﻿# Dynamic and ExpandoObject Mapping
AutoMapper可以在没有任何显式配置的情况下映射到动态对象或从动态对象映射：
~~~
public class Foo {
    public int Bar { get; set; }
    public int Baz { get; set; }
    public Foo InnerFoo { get; set; }
}
dynamic foo = new MyDynamicObject();
foo.Bar = 5;
foo.Baz = 6;

var configuration = new MapperConfiguration(cfg => {});

var result = mapper.Map<Foo>(foo);
result.Bar.ShouldEqual(5);
result.Baz.ShouldEqual(6);

dynamic foo2 = mapper.Map<MyDynamicObject>(result);
foo2.Bar.ShouldEqual(5);
foo2.Baz.ShouldEqual(6);
~~~
类似地，您可以直接从Dictionary<string，object>映射到对象，AutoMapper会将键与属性名称排成一行。要映射到目标子对象，可以使用点表示法。
~~~
var result = mapper.Map<Foo>(new Dictionary<string, object> { ["InnerFoo.Bar"] = 42 });
result.InnerFoo.Bar.ShouldEqual(42);
~~~