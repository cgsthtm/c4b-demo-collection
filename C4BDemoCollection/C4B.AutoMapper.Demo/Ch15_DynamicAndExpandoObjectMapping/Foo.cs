﻿// <copyright file="Foo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch15_DynamicAndExpandoObjectMapping
{
    /// <summary>
    /// Foo.
    /// </summary>
    public class Foo
    {
        /// <summary>
        /// Gets or sets Bar.
        /// </summary>
        public int Bar { get; set; }

        /// <summary>
        /// Gets or sets Baz.
        /// </summary>
        public int Baz { get; set; }

        /// <summary>
        /// Gets or sets InnerFoo.
        /// </summary>
        public Foo InnerFoo { get; set; }
    }
}
