﻿// <copyright file="DynamicAndExpandoObjectMappingTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch15_DynamicAndExpandoObjectMapping
{
    using System.Collections.Generic;
    using global::AutoMapper;
    using Xunit;

    /// <summary>
    /// DynamicAndExpandoObjectMappingTest.
    /// </summary>
    public class DynamicAndExpandoObjectMappingTest
    {
        /// <summary>
        /// Test_DynamicAndExpandoObjectMapping.
        /// </summary>
        [Fact]
        public static void Test_DynamicAndExpandoObjectMapping()
        {
            // AutoMapper可以在没有任何显式配置的情况下映射到动态对象或从动态对象映射：
            dynamic foo = new MyDynamicObject();
            foo.Bar = 5;
            foo.Baz = 6;
            var configuration = new MapperConfiguration(cfg => { });
            var mapper = configuration.CreateMapper();
            ////var result = mapper.Map<Foo>(foo); // 报错：Microsoft.CSharp.RuntimeBinder.RuntimeBinderException : “Map”方法没有采用“1”个参数的重载
            ////Assert.Equal(5, ((Foo)result).Bar);
            ////Assert.Equal(6, ((Foo)result).Baz);

            // 类似地，您可以直接从Dictionary<string，object>映射到对象，AutoMapper会将键与属性名称排成一行。要映射到目标子对象，可以使用点表示法。
            var result1 = mapper.Map<Foo>(new Dictionary<string, object> { ["InnerFoo.Bar"] = 42 });
            Assert.Equal(42, result1.InnerFoo.Bar);
        }
    }
}
