﻿# Expression Translation (UseAsDataSource)
Automapper支持将表达式从一个对象转换到另一个单独的包中。这是通过将源类中的属性替换为它们在目标类中映射到的对象来实现的。
~~~
public class OrderLine
{
  public int Id { get; set; }
  public int OrderId { get; set; }
  public Item Item { get; set; }
  public decimal Quantity { get; set; }
}

public class Item
{
  public int Id { get; set; }
  public string Name { get; set; }
}

public class OrderLineDTO
{
  public int Id { get; set; }
  public int OrderId { get; set; }
  public string Item { get; set; }
  public decimal Quantity { get; set; }
}

var configuration = new MapperConfiguration(cfg =>
{
  cfg.AddExpressionMapping();
  
  cfg.CreateMap<OrderLine, OrderLineDTO>()
    .ForMember(dto => dto.Item, conf => conf.MapFrom(ol => ol.Item.Name));
  cfg.CreateMap<OrderLineDTO, OrderLine>()
    .ForMember(ol => ol.Item, conf => conf.MapFrom(dto => dto));
  cfg.CreateMap<OrderLineDTO, Item>()
    .ForMember(i => i.Name, conf => conf.MapFrom(dto => dto.Item));
});
~~~
从DTO表达式映射时
~~~
Expression<Func<OrderLineDTO, bool>> dtoExpression = dto=> dto. .StartsWith("A");
var expression = mapper.Map<Expression<Func<OrderLine, bool>>>(dtoExpression);
~~~
表达式将被转换为 ol => ol.Item.Name.StartsWith("A")
AutoMapper知道dto.Item映射到ol.Item.Name，因此它将其替换为表达式。
表达式转换也可以在集合的表达式上工作。
~~~
Expression<Func<IQueryable<OrderLineDTO>,IQueryable<OrderLineDTO>>> dtoExpression = dtos => dtos.Where(dto => dto.Quantity > 5).OrderBy(dto => dto.Quantity);
var expression = mapper.Map<Expression<Func<IQueryable<OrderLine>,IQueryable<OrderLine>>>(dtoExpression);
~~~
返回ols => ols.Where(ol => ol.Quantity > 5).OrderBy(ol => ol.Quantity)

## Mapping Flattened Properties to Navigation Properties
AutoMapper还支持将表达式中的扁平化（TModel或DTO）属性映射到其对应的（TData）导航属性（当导航属性已从视图模型或DTO中删除时），
例如，模型表达式中的CourseModel.DepartmentName变为数据表达式中的Course.Department。
~~~
public class CourseModel
{
    public int CourseID { get; set; }

    public int DepartmentID { get; set; }
    public string DepartmentName { get; set; }
}
public class Course
{
    public int CourseID { get; set; }

    public int DepartmentID { get; set; }
    public Department Department { get; set; }
}

public class Department
{
    public int DepartmentID { get; set; }
    public string Name { get; set; }
}
~~~
然后将下面的exp映射到expMapped。
~~~
Expression<Func<IQueryable<CourseModel>, IIncludableQueryable<CourseModel, object>>> exp = i => i.Include(s => s.DepartmentName);
Expression<Func<IQueryable<Course>, IIncludableQueryable<Course, object>>> expMapped = 
    mapper.MapExpressionAsInclude<Expression<Func<IQueryable<Course>, IIncludableQueryable<Course, object>>>>(exp);
~~~
结果映射表达式（expMapped.ToString（））是 i => i.Include(s => s.Department); 。此特性允许仅基于视图模型定义查询的导航属性。

## Supported Mapping options
就像可查询扩展只能支持LINQ提供程序所支持的某些东西一样，表达式转换也遵循同样的规则，它可以支持什么，不可以支持什么。

## UseAsDataSource
将表达式映射到另一个表达式是一件乏味的事情，并且会产生很长很难看的代码。
由于不必显式映射表达式，因此useAsDataSource().For<DTO>()使此转换简洁。它还为您调用ProjectTo<TDO>()（如果适用）。
以EF为例
~~~
dataContext.OrderLines.UseAsDataSource().For<OrderLineDTO>().Where(dto => dto.Name.StartsWith("A"))
~~~
相当于
~~~
dataContext.OrderLines.Where(ol => ol.Item.Name.StartsWith("A")).ProjectTo<OrderLineDTO>()
~~~

### When ProjectTo() is not called
表达式转换适用于所有类型的函数，包括Select调用。如果Select在DataSource（）之后使用并更改返回类型，则不会调用ProjectTo<>（），而是使用mapper.Map。
例如：
~~~
dataContext.OrderLines.UseAsDataSource().For<OrderLineDTO>().Select(dto => dto.Name)
~~~
相当于：
~~~
dataContext.OrderLines.Select(ol => ol.Item.Name)
~~~

### Register a callback, for when an UseAsDataSource() query is enumerated
有时，您可能希望编辑从映射查询返回的集合，然后再将其转发到下一个应用程序层。对于.ProjectTo<TDto>，这非常简单，因为直接返回结果IQueryable<TDto>没有意义，
因为您无论如何都无法再编辑它。你很可能会这样做：
~~~
var configuration = new MapperConfiguration(cfg =>
    cfg.CreateMap<OrderLine, OrderLineDTO>()
    .ForMember(dto => dto.Item, conf => conf.MapFrom(ol => ol.Item.Name)));

public List<OrderLineDTO> GetLinesForOrder(int orderId)
{
  using (var context = new orderEntities())
  {
    var dtos = context.OrderLines.Where(ol => ol.OrderId == orderId)
             .ProjectTo<OrderLineDTO>().ToList();
    foreach(var dto in dtos)
    {
        // edit some property, or load additional data from the database and augment the dtos
    }
    return dtos;
  }
}
~~~
但是，如果使用. DataAsDataSource（）方法执行此操作，则将失去其所有功能-即在枚举内部表达式之前修改内部表达式的能力。
为了解决这个问题，我们引入了.OnEnumerated回调。使用它，您可以执行以下操作：
~~~
var configuration = new MapperConfiguration(cfg =>
    cfg.CreateMap<OrderLine, OrderLineDTO>()
    .ForMember(dto => dto.Item, conf => conf.MapFrom(ol => ol.Item.Name)));

public IQueryable<OrderLineDTO> GetLinesForOrder(int orderId)
{
  using (var context = new orderEntities())
  {
    return context.OrderLines.Where(ol => ol.OrderId == orderId)
             .UseAsDataSource()
             .For<OrderLineDTO>()
             .OnEnumerated((dtos) =>
             {
                foreach(var dto in dtosCast<OrderLineDTO>())
                {
                     // edit some property, or load additional data from the database and augment the dtos
                }
             }
   }
}
~~~
当IQueryable<OrderLineDTO>本身被枚举时，将执行此OnEnumerated（IEEnumerated）回调。因此，这也适用于上面提到的OData示例：
OData $filter和$orderby表达式仍然被转换为SQL，OnEnumerated（）回调函数提供了来自数据库的过滤后的有序结果集。