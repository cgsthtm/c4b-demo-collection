﻿# AutoMapper.Extensions.EnumMapping
内置的枚举映射器是不可配置的，它只能被替换。或者，AutoMapper支持在单独的包AutoMapper.Extensions.EnumMapping中基于约定映射枚举值。https://www.nuget.org/packages/AutoMapper.Extensions.EnumMapping/

## Usage
这个库提供了一个ConvertUsingEnumMapping方法用于方法CreateMap。此方法添加从源枚举值到目标枚举值的所有默认映射。
如果你想改变一些映射，那么你可以使用MapValue方法。这是一个可链接的方法。默认情况下，枚举值是按值映射的（显式地：MapByValue（）），但也可以通过调用MapByName（）按名称映射。
~~~
using AutoMapper.Extensions.EnumMapping;

public enum Source
{
    Default = 0,
    First = 1,
    Second = 2
}

public enum Destination
{
    Default = 0,
    Second = 2
}

internal class YourProfile : Profile
{
    public YourProfile()
    {
        CreateMap<Source, Destination>()
            .ConvertUsingEnumMapping(opt => opt
		        // optional: .MapByValue() or MapByName(), without configuration MapByValue is used
		        .MapValue(Source.First, Destination.Default)
            )
            .ReverseMap(); // to support Destination to Source mapping, including custom mappings of ConvertUsingEnumMapping
    }
}
~~~

## Default Convention
如果两个枚举类型具有相同的值（或按名称或按值），则包AutoMapper.Extensions.EnumMapping将所有值从Source类型映射到Destination类型。
如果启用EnumMappingValidation，则所有没有Source等效项的Source枚举值将引发异常。

## ReverseMap Convention
对于方法ReverseMap，使用与默认映射相同的约定，但如果可能，它也会考虑覆盖枚举值映射。
以下步骤确定反向覆盖：
1. 创建源到目标的映射（默认约定），包括自定义覆盖。
2. 创建目标到源的映射（默认约定），不使用自定义覆盖（必须确定）
3. 步骤1中的映射将用于确定ReverseMap的覆盖。因此，映射按Destination值分组。
   ~~~
   3a）如果有一个匹配的`Source`值用于`Destination`值，那么该映射是首选的，不需要覆盖
   ~~~
   一个Destination值可能有多个由覆盖映射指定的Source值。我们必须确定哪个Source值将成为当前Destination值（即新的Source值）的新Destination.
   对于每个分组目标值的每个源值：
   ~~~
   3b）如果`Source` enum值在`Destination` enum类型中不存在，则该映射不能反转
   3c）如果存在不是来自步骤1的映射的“目的地”部分的“源”值，则该映射不能反转。
   3d）如果选项B和c没有排除“源”值，则该“源”值是新的“目的地”值。
   ~~~
4. 在步骤3中确定的所有覆盖将应用于步骤2中的映射。
5. 最后，将应用为方法ReverseMap提供的自定义映射。

## Testing
AutoMapper为验证类型映射提供了一个很好的工具。这个包添加了一个额外的 EnumMapperConfigurationExpressionExtensions.EnableEnumMappingValidation 扩展方法，
以扩展现有的AssertValidationIsValid（）方法来验证枚举映射。
要启用对枚举映射配置的测试，请执行以下操作：
~~~
public class MappingConfigurationsTests
{
    [Fact]
    public void WhenProfilesAreConfigured_ItShouldNotThrowException()
    {
        // Arrange
        var config = new MapperConfiguration(configuration =>
        {
            configuration.EnableEnumMappingValidation();

            configuration.AddMaps(typeof(AssemblyInfo).GetTypeInfo().Assembly);
        });
		
        // Assert
        config.AssertConfigurationIsValid();
    }
}
~~~