﻿// <copyright file="GettingStartedGuideTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch1_GettingStartedGuide
{
    using System;
    using global::AutoMapper;
    using Serilog;

    /// <summary>
    /// GettingStartedGuideTest.
    /// </summary>
    internal class GettingStartedGuideTest
    {
        /// <summary>
        /// Test_UseAutoMapper.
        /// </summary>
        public static void Test_UseAutoMapper()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderDto>());
            var mapper = config.CreateMapper();
            var mapper2 = new Mapper(config);
            var order = new Order { OrderId = 1, OrderDate = DateTime.Now, Sum = 99.00M };
            OrderDto dto = mapper.Map<OrderDto>(order);
            Log.Information("OrderDto: {@dto}", dto);
        }

        /// <summary>
        /// Test_TestMyMappings.
        /// </summary>
        public static void Test_TestMyMappings()
        {
            var config = AutoMapperConfiguration.Configure();
            config.AssertConfigurationIsValid();
        }
    }
}
