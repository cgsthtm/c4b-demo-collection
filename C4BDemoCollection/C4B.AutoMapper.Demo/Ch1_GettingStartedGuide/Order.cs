﻿// <copyright file="Order.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch1_GettingStartedGuide
{
    using System;

    /// <summary>
    /// Order.
    /// </summary>
    internal class Order
    {
        /// <summary>
        /// Gets or sets OrderId.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Gets or sets OrderDate.
        /// </summary>
        public DateTime OrderDate { get; set; }

        /// <summary>
        /// Gets or sets Sum.
        /// </summary>
        public decimal Sum { get; set; }
    }
}