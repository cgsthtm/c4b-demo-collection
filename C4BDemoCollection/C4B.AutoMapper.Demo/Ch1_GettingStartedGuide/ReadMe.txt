﻿# Getting Started Guide
AutoMapper是一个对象-对象映射器。对象-对象映射的工作原理是将一种类型的输入对象转换为另一种类型的输出对象。AutoMapper的有趣之处在于，它提供了一些有趣的约定，
以消除将类型A映射到类型B的繁琐工作。只要类型B遵循AutoMapper的既定约定，映射两个类型几乎不需要任何配置。

## Why use AutoMapper?
映射代码很无聊。测试映射代码更加无聊。AutoMapper提供了简单的类型配置以及简单的映射测试。真实的问题可能是“为什么要使用对象-对象映射？”
映射可以发生在应用程序中的许多地方，但主要发生在层之间的边界，例如UI/域层或服务/域层之间。一层的关注点经常与另一层的关注点冲突，
因此对象-对象映射导致隔离的模型，其中每一层的关注点只能影响该层中的类型。

## How do I use AutoMapper?
首先，您需要同时使用源和目标类型。目标类型的设计可能会受到它所在的层的影响，但只要成员的名称与源类型的成员相匹配，AutoMapper就能最好地工作。
如果您有一个名为“FirstName”的源成员，它将自动映射到名为“FirstName”的目标成员。AutoMapper还支持拼合
(Flattening https://docs.automapper.org/en/stable/Getting-started.html#Flattening.html)。
AutoMapper在将源映射到目标时将忽略空引用异常。这是设计好的。如果您不喜欢这种方法，可以根据需要将AutoMapper的方法与自定义值解析器结合使用。
https://docs.automapper.org/en/stable/Getting-started.html#Custom-value-resolvers.html
一旦有了自己的类型，就可以使用MapperConfiguration和CreateMap为这两种类型创建一个映射。通常每个AppDomain只需要一个MapperConfiguration实例，并且应该在启动期间实例化。
初始设置的更多示例可以在设置(Setup https://docs.automapper.org/en/stable/Getting-started.html#Setup.html)中看到。
~~~
var config = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderDto>());
~~~
左边的类型是源类型，右边的类型是目标类型。要执行映射，请调用Map重载之一：
~~~
var mapper = config.CreateMapper();
// or
var mapper = new Mapper(config);
OrderDto dto = mapper.Map<OrderDto>(order);
~~~
大多数应用程序可以使用依赖注入来注入创建的IMapper实例。
AutoMapper也有这些方法的非泛型版本，用于那些您可能在编译时不知道类型的情况。

## Where do I configure AutoMapper?
每个AppDomain只应进行一次配置。这意味着放置配置代码的最佳位置是在应用程序启动时，例如ASP.NET应用程序的Global.asax文件。通常，配置引导程序类在其自己的类中，
并且从startup方法调用此引导程序类。引导程序类应该构造一个MapperConfiguration对象来配置类型映射。
对于ASP.NETCore，Dependency Injection(https://docs.automapper.org/en/stable/Getting-started.html#Dependency-injection.html#asp-net-core)
文章展示了如何在应用程序中配置AutoMapper。

## How do I test my mappings?
要测试映射，您需要创建一个执行两项操作的测试：
- 调用您的引导程序类来创建所有映射
- 调用MapperConfiguration.AssertConfigurationIsValid
下面是一个示例：
~~~
var config = AutoMapperConfiguration.Configure();

config.AssertConfigurationIsValid();
~~~