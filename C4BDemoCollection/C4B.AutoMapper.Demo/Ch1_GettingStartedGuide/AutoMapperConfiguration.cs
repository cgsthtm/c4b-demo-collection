﻿// <copyright file="AutoMapperConfiguration.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch1_GettingStartedGuide
{
    using global::AutoMapper;

    /// <summary>
    /// AutoMapperConfiguration.
    /// </summary>
    internal class AutoMapperConfiguration
    {
        /// <summary>
        /// Configure.
        /// </summary>
        /// <returns>MapperConfiguration.</returns>
        public static MapperConfiguration Configure()
        {
            return new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderDto>());
        }
    }
}
