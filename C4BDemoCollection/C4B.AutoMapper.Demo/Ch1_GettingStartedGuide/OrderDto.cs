﻿// <copyright file="OrderDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch1_GettingStartedGuide
{
    using System;

    /// <summary>
    /// OrderDto.
    /// </summary>
    internal class OrderDto
    {
        /// <summary>
        /// Gets or sets OrderId.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Gets or sets OrderDate.
        /// </summary>
        public DateTime OrderDate { get; set; }
    }
}