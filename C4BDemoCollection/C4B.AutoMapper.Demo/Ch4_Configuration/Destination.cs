﻿// <copyright file="Destination.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch4_Configuration
{
    /// <summary>
    /// Destination.
    /// </summary>
    internal class Destination
    {
        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Gets or sets Aviator.
        /// </summary>
        public int Aviator { get; set; }

        /// <summary>
        /// Gets or sets SubAirlineFlight.
        /// </summary>
        public int SubAirlineFlight { get; set; }

        /// <summary>
        /// Gets or sets TestConfiguringVisibility.
        /// </summary>
        public string TestConfiguringVisibility { get; set; }
    }
}
