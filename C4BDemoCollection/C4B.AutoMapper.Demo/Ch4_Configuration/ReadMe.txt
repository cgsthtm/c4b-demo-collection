﻿# Configuration
创建一个MapperConfiguration实例并通过构造函数初始化配置：
~~~
var config = new MapperConfiguration(cfg => {
    cfg.CreateMap<Foo, Bar>();
    cfg.AddProfile<FooProfile>();
});
~~~
MapperConfiguration实例可以静态存储在静态字段或依赖注入容器中。一旦创建，就不能更改/修改。
~~~
var configuration = new MapperConfiguration(cfg => {
    cfg.CreateMap<Foo, Bar>();
    cfg.AddProfile<FooProfile>();
});
~~~
从9.0开始，静态API不再可用。

## Profile Instances
组织映射配置的一个好方法是使用profiles。创建从Profile继承的类，并将配置放在构造函数中：
~~~
// This is the approach starting with version 5
public class OrganizationProfile : Profile
{
	public OrganizationProfile()
	{
		CreateMap<Foo, FooDto>();
		// Use CreateMap... Etc.. here (Profile methods are the same as configuration methods)
	}
}

// How it was done in 4.x - as of 5.0 this is obsolete:
// public class OrganizationProfile : Profile
// {
//     protected override void Configure()
//     {
//         CreateMap<Foo, FooDto>();
//     }
// }
~~~
在早期版本中，使用Configure方法而不是构造函数。从版本5开始，Configure（）已经过时。它将在6.0中删除。
profiles内的配置仅适用于profiles内的映射。应用于根配置的配置将应用于创建的所有映射。

### Assembly Scanning for auto configuration
可以通过多种方式将profiles添加到主映射器配置中，可以直接添加：
~~~
cfg.AddProfile<OrganizationProfile>();
cfg.AddProfile(new OrganizationProfile());
~~~
或通过自动扫描profiles：
~~~
// Scan for all profiles in an assembly
// ... using instance approach:
var config = new MapperConfiguration(cfg => {
    cfg.AddMaps(myAssembly);
});
var configuration = new MapperConfiguration(cfg => cfg.AddMaps(myAssembly));

// Can also use assembly names:
var configuration = new MapperConfiguration(cfg =>
    cfg.AddMaps(new [] {
        "Foo.UI",
        "Foo.Core"
    });
);

// Or marker types for assemblies:
var configuration = new MapperConfiguration(cfg =>
    cfg.AddMaps(new [] {
        typeof(HomeController),
        typeof(Entity)
    });
);
~~~
AutoMapper将扫描指定的程序集，查找从Profile继承的类，并将它们添加到配置中。

## Naming Conventions
您可以设置源和目标命名约定
~~~
var configuration = new MapperConfiguration(cfg => {
  cfg.SourceMemberNamingConvention = LowerUnderscoreNamingConvention.Instance;
  cfg.DestinationMemberNamingConvention = PascalCaseNamingConvention.Instance;
});
~~~
这将使以下属性相互映射： property_name -> PropertyName
您也可以按profiles设置此设置
~~~
public class OrganizationProfile : Profile
{
  public OrganizationProfile()
  {
    SourceMemberNamingConvention = LowerUnderscoreNamingConvention.Instance;
    DestinationMemberNamingConvention = PascalCaseNamingConvention.Instance;
    //Put your CreateMap... Etc.. here
  }
}
~~~
如果你不需要命名约定，你可以使用ExactMatchNamingConvention.

## Replacing characters
您还可以在成员名称匹配期间替换源成员中的单个字符或整个单词：
~~~
public class Source
{
    public int Value { get; set; }
    public int Ävíator { get; set; }
    public int SubAirlinaFlight { get; set; }
}
public class Destination
{
    public int Value { get; set; }
    public int Aviator { get; set; }
    public int SubAirlineFlight { get; set; }
}
~~~
我们想替换单个字符，也许翻译一个词：
~~~
var configuration = new MapperConfiguration(c =>
{
    c.ReplaceMemberName("Ä", "A");
    c.ReplaceMemberName("í", "i");
    c.ReplaceMemberName("Airlina", "Airline");
});
~~~

## Recognizing pre/postfixes
有时，源/目标属性会有公共的前缀/后缀，这会导致您不得不进行大量的自定义成员映射，因为名称不匹配。要解决这个问题，您可以识别前缀/后缀：
~~~
public class Source {
    public int frmValue { get; set; }
    public int frmValue2 { get; set; }
}
public class Dest {
    public int Value { get; set; }
    public int Value2 { get; set; }
}
var configuration = new MapperConfiguration(cfg => {
    cfg.RecognizePrefixes("frm");
    cfg.CreateMap<Source, Dest>();
});
configuration.AssertConfigurationIsValid();
~~~
默认情况下，AutoMapper识别前缀“Get”，如果您需要清除前缀：
~~~
var configuration = new MapperConfiguration(cfg => {
    cfg.ClearPrefixes();
    cfg.RecognizePrefixes("tmp");
});
~~~

## Global property/field filtering
默认情况下，AutoMapper会尝试映射每个公共属性/字段。您可以使用属性/字段过滤器过滤掉属性/字段：
~~~
var configuration = new MapperConfiguration(cfg =>
{
	// don't map any fields
	cfg.ShouldMapField = fi => false;

	// map properties with a public or private getter
	cfg.ShouldMapProperty = pi =>
		pi.GetMethod != null && (pi.GetMethod.IsPublic || pi.GetMethod.IsPrivate);
});
~~~

## Configuring visibility
默认情况下，AutoMapper仅识别公共成员。它可以映射到private setters，但是如果整个属性都是private/internal的，它会跳过internal/private方法和属性。
要指示AutoMapper识别具有其他访问权限的成员，请覆盖默认过滤器ShouldMapField和/或ShouldMapProperty：
~~~
var configuration = new MapperConfiguration(cfg =>
{
    // map properties with public or internal getters
    cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
    cfg.CreateMap<Source, Destination>();
});
~~~
映射配置现在将识别内部/私有成员。

## Configuration compilation
由于表达式编译可能会占用一些资源，因此AutoMapper会在第一个映射上延迟编译类型映射计划。但是，这种行为并不总是可取的，因此您可以告诉AutoMapper直接编译其映射：
~~~
var configuration = new MapperConfiguration(cfg => {});
configuration.CompileMappings();
~~~
对于几百个映射，这可能需要几秒钟。如果它比这要多得多，你可能有一些非常大的执行计划。

### Long compilation times
编译时间随着执行计划的大小而增加，这取决于属性的数量及其复杂性。理想情况下，你应该修复你的模型，这样你就有了许多小的DTO，每个DTO都对应一个特定的用例。
但是您也可以在不更改类的情况下减小执行计划的大小。
您可以全局设置每个成员的MapAtRuntime或MaxExecutionPlanDepth（默认值为1，将其设置为零）。
这些方法通过用方法调用替换子对象的执行计划来减小执行计划的大小。编译会更快，但映射本身可能会更慢。搜索存储库以获取更多详细信息，并使用分析器更好地了解效果。
避免PreserveReferences和MaxDepth也有帮助。