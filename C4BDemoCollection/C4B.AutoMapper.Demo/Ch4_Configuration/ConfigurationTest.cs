﻿// <copyright file="ConfigurationTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch4_Configuration
{
    using System;
    using global::AutoMapper;
    using global::AutoMapper.Configuration;
    using Serilog;

    /// <summary>
    /// ConfigurationTest.
    /// </summary>
    internal class ConfigurationTest
    {
        /// <summary>
        /// Test_ProfileInstances.
        /// </summary>
        public static void Test_ProfileInstances()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Foo, Bar>();
                cfg.AddProfile<OrganizationProfile>();
            });
            var mapper = configuration.CreateMapper();
            var foo = new Foo { Id = 1, Name = "ccg", Age = 18, Date = DateTime.Now };
            var fooDto = mapper.Map<FooDto>(foo);
            Log.Information("FooDto: {@fooDto}", fooDto);
        }

        /// <summary>
        /// Test_AssemblyScanningForAutoConfiguration.
        /// </summary>
        public static void Test_AssemblyScanningForAutoConfiguration()
        {
            var myAssembly = typeof(ConfigurationTest).Assembly;

            // Scan for all profiles in an assembly
            // ... using instance approach:
            var configuration1 = new MapperConfiguration(cfg =>
            {
                cfg.AddMaps(myAssembly);
            });
            var configuration2 = new MapperConfiguration(cfg => cfg.AddMaps(myAssembly));

            // Can also use assembly names:
            ////var configuration3 = new MapperConfiguration(cfg =>
            ////    cfg.AddMaps(new[]
            ////    {
            ////        "Foo.UI",
            ////        "Foo.Core",
            ////    }));

            // Or marker types for assemblies:
            ////var configuration4 = new MapperConfiguration(cfg =>
            ////    cfg.AddMaps(new[] {
            ////        typeof(HomeController),
            ////        typeof(Entity),
            ////    })
            ////);

            var mapper = configuration1.CreateMapper();
            var foo = new Foo { Id = 1, Name = "ccg", Age = 18, Date = DateTime.Now };
            var fooDto = mapper.Map<FooDto>(foo);
            Log.Information("FooDto: {@fooDto}", fooDto);
        }

        /// <summary>
        /// Test_NamingConventions.
        /// </summary>
        public static void Test_NamingConventions()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                var myAssembly = typeof(ConfigurationTest).Assembly;
                cfg.SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
                cfg.DestinationMemberNamingConvention = new PascalCaseNamingConvention();
                cfg.CreateMap<Foo1, FooDto1>();
            });
            var mapper = configuration.CreateMapper();
            var foo1 = new Foo1 { foo_name = "ccs" };
            var fooDto1 = mapper.Map<FooDto1>(foo1);
            Log.Information("FooDto1: {@fooDto1}", fooDto1);
        }

        /// <summary>
        /// Test_ReplacingCharacters.
        /// </summary>
        public static void Test_ReplacingCharacters()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.ReplaceMemberName("Ä", "A");
                cfg.ReplaceMemberName("í", "i");
                cfg.ReplaceMemberName("Airlina", "Airline");
                cfg.CreateMap<Source, Destination>();
            });
            var mapper = configuration.CreateMapper();
            var source = new Source() { Value = 112, Ävíator = 113, SubAirlinaFlight = 114 };
            var destination = mapper.Map<Destination>(source);
            Log.Information("Destination: {@destination}", destination);
        }

        /// <summary>
        /// Test_RecognizingPrePostfixes.
        /// </summary>
        public static void Test_RecognizingPrePostfixes()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.RecognizePrefixes("frm");
                cfg.CreateMap<Source1, Destination1>();
            });
            configuration.AssertConfigurationIsValid();
            var mapper = configuration.CreateMapper();
            var source1 = new Source1() { frmValue = 1, frmValue2 = 2};
            var destination1 = mapper.Map<Destination1>(source1);
            Log.Information("Destination1: {@destination1}", destination1);

            // 默认情况下，AutoMapper识别前缀“Get”，如果您需要清除前缀：
            var configuration2 = new MapperConfiguration(cfg =>
            {
                cfg.ClearPrefixes();
                cfg.RecognizePrefixes("tmp");
            });
        }

        /// <summary>
        /// Test_GlobalPropertyFieldFiltering.
        /// </summary>
        public static void Test_GlobalPropertyFieldFiltering()
        {
            // 默认情况下，AutoMapper会尝试映射每个公共属性/字段。您可以使用属性/字段过滤器过滤掉属性/字段：
            var configuration = new MapperConfiguration(cfg =>
            {
                // don't map any fields
                cfg.ShouldMapField = fi => false;

                // map properties with a public or private getter
                cfg.ShouldMapProperty = pi =>
                    pi.GetMethod != null && (pi.GetMethod.IsPublic || pi.GetMethod.IsPrivate);

                cfg.CreateMap<ShouldMapPropertyClass, ShouldMapPropertyClassDto>();
            });
            var mapper = configuration.CreateMapper();
            var dto = mapper.Map<ShouldMapPropertyClassDto>(new ShouldMapPropertyClass() { id = 1, Name = "ccs", Age = 18 });
            Log.Information("Dto: {@dot}", dto);
        }

        /// <summary>
        /// Test_ConfiguringVisibility.
        /// </summary>
        public static void Test_ConfiguringVisibility()
        {
            // 默认情况下，AutoMapper仅识别公共成员。它可以映射到private setters，但是如果整个属性都是private/internal的，它会跳过internal/private方法和属性。
            // 要指示AutoMapper识别具有其他访问权限的成员，请覆盖默认过滤器ShouldMapField和/或ShouldMapProperty：
            var configuration = new MapperConfiguration(cfg =>
            {
                // map properties with public or internal getters
                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly; // 映射配置现在将识别内部/私有成员。
                cfg.CreateMap<Source, Destination>();
            });
            var mapper = configuration.CreateMapper();
            var source = new Source() { Value = 112, Ävíator = 113, SubAirlinaFlight = 114, TestConfiguringVisibility = "yes" };
            var destination = mapper.Map<Destination>(source);
            Log.Information("Destination: {@destination}", destination);
        }

        /// <summary>
        /// Test_ConfigurationComlilation.
        /// </summary>
        public static void Test_ConfigurationComlilation()
        {
            // 由于表达式编译可能会占用一些资源，因此AutoMapper会在第一个映射上延迟编译类型映射计划。
            // 但是，这种行为并不总是可取的，因此您可以告诉AutoMapper直接编译其映射：
            var configuration = new MapperConfiguration(cfg => { });
            configuration.CompileMappings();

            // 对于几百个映射，这可能需要几秒钟。如果它比这要多得多，你可能有一些非常大的执行计划。
        }
    }
}
