﻿// <copyright file="Destination1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch4_Configuration
{
    /// <summary>
    /// Destination.
    /// </summary>
    internal class Destination1
    {
        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Gets or sets Value2.
        /// </summary>
        public int Value2 { get; set; }
    }
}
