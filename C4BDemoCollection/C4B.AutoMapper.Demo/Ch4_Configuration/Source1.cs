﻿// <copyright file="Source1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch4_Configuration
{
    /// <summary>
    /// Source1.
    /// </summary>
    internal class Source1
    {
        /// <summary>
        /// Gets or sets frmValue.
        /// </summary>
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public int frmValue { get; set; }
#pragma warning restore SA1300 // Element should begin with upper-case letter

        /// <summary>
        /// Gets or sets FrmValue2.
        /// </summary>
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public int frmValue2 { get; set; }
#pragma warning restore SA1300 // Element should begin with upper-case letter
    }
}
