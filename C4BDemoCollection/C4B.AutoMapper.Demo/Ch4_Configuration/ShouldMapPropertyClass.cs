﻿// <copyright file="ShouldMapPropertyClass.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch4_Configuration
{
    /// <summary>
    /// ShouldMapPropertyClass.
    /// </summary>
    internal class ShouldMapPropertyClass
    {
        public int id;

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Sets Age.
        /// </summary>
        public int Age { private get; set; }
    }
}
