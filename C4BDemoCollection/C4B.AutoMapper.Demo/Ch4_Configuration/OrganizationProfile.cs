﻿// <copyright file="OrganizationProfile.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch4_Configuration
{
    using global::AutoMapper;

    // This is the approach starting with version 5

    /// <summary>
    /// OrganizationProfile.
    /// </summary>
    public class OrganizationProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrganizationProfile"/> class.
        /// </summary>
        public OrganizationProfile()
        {
            // Naming Conventions
            this.SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
            this.DestinationMemberNamingConvention = new PascalCaseNamingConvention();

            // Put your CreateMap... Etc.. here
            this.CreateMap<Foo, FooDto>();
            //// Use CreateMap... Etc.. here (Profile methods are the same as configuration methods)
        }

        // How it was done in 4.x - as of 5.0 this is obsolete:
        // public class OrganizationProfile : Profile
        // {
        //     protected override void Configure()
        //     {
        //         CreateMap<Foo, FooDto>();
        //     }
        // }
    }
}