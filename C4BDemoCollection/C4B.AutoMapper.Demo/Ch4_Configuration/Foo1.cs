﻿// <copyright file="Foo1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch4_Configuration
{
    /// <summary>
    /// Foo1.
    /// </summary>
    internal class Foo1
    {
        /// <summary>
        /// Gets or sets foo_name.
        /// </summary>
#pragma warning disable SA1300 // Element should begin with upper-case letter
        public string foo_name { get; set; }
#pragma warning restore SA1300 // Element should begin with upper-case letter
    }
}
