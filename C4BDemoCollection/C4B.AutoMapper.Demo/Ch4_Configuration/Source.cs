﻿// <copyright file="Source.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch4_Configuration
{
    /// <summary>
    /// Source.
    /// </summary>
    internal class Source
    {
        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Gets or sets Ävíator.
        /// </summary>
        public int Ävíator { get; set; }

        /// <summary>
        /// Gets or sets SubAirlinaFlight.
        /// </summary>
        public int SubAirlinaFlight { get; set; }

        /// <summary>
        /// Gets or sets TestConfiguringVisibility.
        /// </summary>
        public string TestConfiguringVisibility { internal get; set; }
    }
}
