﻿// <copyright file="FooDto1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch4_Configuration
{
    /// <summary>
    /// FooDto1.
    /// </summary>
    internal class FooDto1
    {
        /// <summary>
        /// Gets or sets FooName.
        /// </summary>
        public string FooName { get; set; }
    }
}
