﻿// <copyright file="ShouldMapPropertyClassDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch4_Configuration
{
    /// <summary>
    /// ShouldMapPropertyClassDto.
    /// </summary>
    internal class ShouldMapPropertyClassDto
    {
        public int id;

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Age.
        /// </summary>
        public int Age { get; set; }
    }
}
