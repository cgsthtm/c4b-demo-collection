﻿// <copyright file="OrderDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch14_AttributeMapping
{
    using global::AutoMapper;
    using global::AutoMapper.Configuration.Annotations;

    // 要声明属性映射，请使用AutoMapAttribute修饰目标类型：
    // 这相当于一个映射<Order，OrderDto>（）配置。

    /// <summary>
    /// OrderDto.
    /// </summary>
    [AutoMap(typeof(Order))]
    internal class OrderDto
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Total.
        /// </summary>
        [Ignore] // 使用IgnoreAttribute从映射和/或验证中忽略单个目标成员：
        public decimal Total { get; set; }

        /// <summary>
        /// Gets or sets CustomerName.
        /// </summary>
        [SourceMember("OrderCustomerName")]
        ////[SourceMember(nameof(Order.OrderCustomerName))]
        public string CustomerName { get; set; }
    }
}
