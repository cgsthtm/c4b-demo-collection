﻿// <copyright file="Order.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch14_AttributeMapping
{
    /// <summary>
    /// Order.
    /// </summary>
    internal class Order
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Total.
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// Gets or sets OrderCustomerName.
        /// </summary>
        public string OrderCustomerName { get; set; }
    }
}
