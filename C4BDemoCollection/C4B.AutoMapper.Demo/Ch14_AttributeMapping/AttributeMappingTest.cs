﻿// <copyright file="AttributeMappingTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch14_AttributeMapping
{
    using global::AutoMapper;
    using Xunit;

    /// <summary>
    /// AttributeMappingTest.
    /// </summary>
    public class AttributeMappingTest
    {
        /// <summary>
        /// Test_TypeMapConfiguration.
        /// </summary>
        [Fact]
        public static void Test_TypeMapConfiguration()
        {
            // 要搜索要配置的映射，请使用AddMaps方法：
            // AddMaps查找流畅的映射配置（Profile类）和基于属性的映射。
            var configuration = new MapperConfiguration(cfg => cfg.AddMaps(typeof(AttributeMappingTest).Assembly));
            var mapper = new Mapper(configuration);
            var order = new Order { Id = 1 };
            var orderDto = mapper.Map<OrderDto>(order);
            Assert.Equal(1, orderDto.Id);
        }

        /// <summary>
        /// Test_MemberConfiguration.
        /// </summary>
        [Fact]
        public static void Test_MemberConfiguration()
        {
            var configuration = new MapperConfiguration(cfg => cfg.AddMaps(typeof(AttributeMappingTest).Assembly));
            var mapper = new Mapper(configuration);
            var order = new Order { Id = 1, Total = 2, OrderCustomerName = "ccs" };
            var orderDto = mapper.Map<OrderDto>(order);
            Assert.Equal(0, orderDto.Total);
            Assert.Equal("ccs", orderDto.CustomerName);
        }
    }
}
