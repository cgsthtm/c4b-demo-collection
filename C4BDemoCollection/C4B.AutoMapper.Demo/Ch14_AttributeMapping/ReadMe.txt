﻿# Attribute Mapping
除了流畅的配置之外，还可以通过属性声明和配置映射。属性映射可以补充或替换流畅映射配置。

## Type Map configuration
要搜索要配置的映射，请使用AddMaps方法：
~~~
var configuration = new MapperConfiguration(cfg => cfg.AddMaps("MyAssembly"));

var mapper = new Mapper(configuration);
~~~
AddMaps查找流畅的映射配置（Profile类）和基于属性的映射。
要声明属性映射，请使用AutoMapAttribute修饰目标类型：
~~~
[AutoMap(typeof(Order))]
public class OrderDto {
    // destination members
~~~
这相当于一个映射<Order，OrderDto>（）配置。

### Customizing type map configuration
要自定义总体类型映射配置，可以在AutoMapAttribute上设置以下属性：
- ReverseMap (bool)
- ConstructUsingServiceLocator (bool)
- MaxDepth (int)
- PreserveReferences (bool)
- DisableCtorValidation (bool)
- IncludeAllDerived (bool)
- TypeConverter (Type)
- AsProxy (bool)
这些都对应于类似的流畅映射配置选项。只需要sourceType值即可进行映射。

## Member configuration
对于基于属性的映射，您可以使用其他配置装饰单个成员。因为属性在C#中有限制（例如，没有表达式），所以可用的配置选项有点有限。
基于命名空间的属性在 AutoMapper.Configuration.Annotations 命名空间中声明。
如果基于属性的配置不可用或不起作用，您可以将基于属性的映射和基于配置文件的映射联合收割机组合起来（尽管这可能会引起混淆）。

### Ignoring members
使用IgnoreAttribute从映射和/或验证中忽略单个目标成员：
~~~
using AutoMapper.Configuration.Annotations;

[AutoMap(typeof(Order))]
public class OrderDto {
    [Ignore]
    public decimal Total { get; set; }
~~~

### Redirecting to a different source member
不能将MapFrom与属性中的表达式一起使用，但SourceMemberAttribute可以重定向到单独的命名成员：
~~~
using AutoMapper.Configuration.Annotations;

[AutoMap(typeof(Order))]
public class OrderDto {
   [SourceMember("OrderTotal")]
   public decimal Total { get; set; }
~~~
或者使用nameof操作符：
~~~
using AutoMapper.Configuration.Annotations;

[AutoMap(typeof(Order))]
public class OrderDto {
   [SourceMember(nameof(Order.OrderTotal))]
   public decimal Total { get; set; }
~~~
您不能使用此属性进行扁平化，只能重定向源类型成员（即名称中没有“Order.Customer.Office.Name”）。配置展平仅适用于fluent配置。

### Additional configuration options
其他基于属性的配置选项包括：
- MapAtRuntimeAttribute
- MappingOrderAttribute
- NullSubstituteAttribute
- UseExistingValueAttribute
- ValueConverterAttribute
- ValueResolverAttribute
每个对应于相同的fluent配置映射选项。