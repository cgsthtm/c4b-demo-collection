﻿# The MyGet Build
AutoMapper使用MyGet发布基于主分支的开发版本。这意味着MyGet构建有时包含当前NuGet包中不可用的修复程序。如果您的问题已修复但尚未发布，请在报告问题之前尝试最新的MyGet版本。
AutoMapper MyGet图库可在此处(https://myget.org/feed/automapperdev/package/nuget/AutoMapper)找到。一定要包括预发布。

## Installing the Package
如果你想将最新的MyGet包安装到一个项目中，你可以使用以下命令：
~~~
Install-Package AutoMapper -Source https://www.myget.org/F/automapperdev/api/v3/index.json -IncludePrerelease
~~~