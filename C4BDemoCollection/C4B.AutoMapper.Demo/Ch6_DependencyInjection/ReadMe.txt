﻿# Dependency Injection
## Examples
### ASP.NET Core
有一个NuGet包(https://www.nuget.org/packages/AutoMapper.Extensions.Microsoft.DependencyInjection/)可与
此处(https://github.com/AutoMapper/AutoMapper.Extensions.Microsoft.DependencyInjection)描述的默认注入机制一起使用，
并在本项目(https://github.com/jbogard/ContosoUniversityCore/blob/master/src/ContosoUniversityCore/Startup.cs)中使用。
版本13.0开始，AddAutoMapper是核心包的一部分，DI包停止使用。
您可以使用profiles(https://docs.automapper.org/en/stable/Dependency-injection.html#Configuration.html#profile-instances)定义配置。
然后，通过在启动时调用IServiceCollection扩展方法AddAutoMapper，让AutoMapper知道这些配置文件是在哪些程序集中定义的：
~~~
services.AddAutoMapper(profileAssembly1, profileAssembly2 /*, ...*/);
~~~
或标记类型：
~~~
services.AddAutoMapper(typeof(ProfileTypeFromAssembly1), typeof(ProfileTypeFromAssembly2) /*, ...*/);
~~~
现在，您可以在运行时将AutoMapper注入到您的服务/控制器中：
~~~
public class EmployeesController {
	private readonly IMapper _mapper;

	public EmployeesController(IMapper mapper) => _mapper = mapper;

	// use _mapper.Map or _mapper.ProjectTo
}
~~~

### Autofac
有一个第三方NuGet包(https://www.nuget.org/packages/AutoMapper.Contrib.Autofac.DependencyInjection)你可能想尝试。
还有，看看这个博客(https://dotnetfalcon.com/autofac-support-for-automapper/)。

### Other DI engines https://github.com/AutoMapper/AutoMapper/wiki/DI-examples

## Low level API-s
AutoMapper支持使用静态服务位置构造自定义值解析器(https://docs.automapper.org/en/stable/Dependency-injection.html#Custom-value-resolvers.html)、
自定义类型转换器(https://docs.automapper.org/en/stable/Dependency-injection.html#Custom-type-converters.html)和
值转换器(https://docs.automapper.org/en/stable/Dependency-injection.html#Value-converters.html)的功能：
~~~
var configuration = new MapperConfiguration(cfg =>
{
    cfg.ConstructServicesUsing(ObjectFactory.GetInstance);

    cfg.CreateMap<Source, Destination>();
});
~~~
或动态服务位置，用于基于实例的容器（包括子/嵌套容器）：
~~~
var mapper = new Mapper(configuration, childContainer.GetInstance);

var dest = mapper.Map<Source, Destination>(new Source { Value = 15 });
~~~

## Queryable Extensions
从8.0开始，您可以使用IMapper.ProjectTo。对于旧版本，您需要将配置传递给扩展方法 IQueryable.ProjectTo<T>(IConfigurationProvider) 。
请注意，ProjectTo比Map更受限制(https://docs.automapper.org/en/stable/Dependency-injection.html#Queryable-Extensions.html#supported-mapping-options)，
因为仅支持基础LINQ提供程序允许的内容。这意味着你不能像使用Map那样将DI用于值解析器和转换器。