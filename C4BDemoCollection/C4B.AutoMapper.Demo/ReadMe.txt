﻿# AutoMapper
基于约定的对象-对象映射器。

AutoMapper使用流畅配置API来定义对象-对象映射策略。AutoMapper使用基于约定的匹配算法来匹配源值和目标值。AutoMapper适用于模型投影场景，
将复杂的对象模型扁平化为DTO和其他简单对象，其设计更适合于序列化，通信，消息传递，或只是域和应用程序层之间的防损坏层。

AutoMapper新手？首先查看入门指南页面。https://docs.automapper.org/en/stable/Getting-started.html