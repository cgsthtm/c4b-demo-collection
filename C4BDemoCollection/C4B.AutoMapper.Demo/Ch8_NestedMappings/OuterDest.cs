﻿// <copyright file="OuterDest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch8_NestedMappings
{
    /// <summary>
    /// OuterDest.
    /// </summary>
    internal class OuterDest
    {
        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Gets or sets Inner.
        /// </summary>
        public InnerDest Inner { get; set; }
    }
}
