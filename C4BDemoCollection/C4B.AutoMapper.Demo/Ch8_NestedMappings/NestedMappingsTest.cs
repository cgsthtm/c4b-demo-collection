﻿// <copyright file="NestedMappingsTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch8_NestedMappings
{
    using global::AutoMapper;
    using Xunit;

    /// <summary>
    /// NestedMappingsTest.
    /// </summary>
    public class NestedMappingsTest
    {
        /// <summary>
        /// Test_NestedMappings.
        /// </summary>
        [Fact]
        public static void Test_NestedMappings()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<OuterSource, OuterDest>();
                cfg.CreateMap<InnerSource, InnerDest>();
            });

            config.AssertConfigurationIsValid();
            var source = new OuterSource
            {
                Value = 5,
                Inner = new InnerSource { OtherValue = 15 },
            };
            var mapper = config.CreateMapper();
            var dest = mapper.Map<OuterSource, OuterDest>(source);
            Assert.Equal(5, dest.Value);
            Assert.NotNull(dest.Inner);
            Assert.Equal(15, dest.Inner.OtherValue);
        }
    }
}
