﻿# Nested Mappings
当映射引擎执行映射时，它可以使用多种方法之一来解析目标成员值。其中一种方法是使用另一种类型映射，其中源成员类型和目标成员类型也在映射配置中配置。
这使我们不仅可以扁平化源类型，还可以创建复杂的目标类型。例如，我们的源类型可能包含另一个复杂类型：
~~~
public class OuterSource
{
	public int Value { get; set; }
	public InnerSource Inner { get; set; }
}

public class InnerSource
{
	public int OtherValue { get; set; }
}
~~~
我们可以简单地将OuterSource.Inner.OtherValue扁平化为一个InnerOtherValue属性，但我们也可能希望为Inner属性创建一个对应的复杂类型：
~~~
public class OuterDest
{
	public int Value { get; set; }
	public InnerDest Inner { get; set; }
}

public class InnerDest
{
	public int OtherValue { get; set; }
}
~~~
在这种情况下，我们需要配置额外的源/目标类型映射：
~~~
var config = new MapperConfiguration(cfg => {
    cfg.CreateMap<OuterSource, OuterDest>();
    cfg.CreateMap<InnerSource, InnerDest>();
});
config.AssertConfigurationIsValid();

var source = new OuterSource
	{
		Value = 5,
		Inner = new InnerSource {OtherValue = 15}
	};
var mapper = config.CreateMapper();
var dest = mapper.Map<OuterSource, OuterDest>(source);

dest.Value.ShouldEqual(5);
dest.Inner.ShouldNotBeNull();
dest.Inner.OtherValue.ShouldEqual(15);
~~~
这里有几件事要注意：
- 配置类型的顺序无关紧要
- 调用Map不需要指定任何内部类型映射，只需要指定用于传入的源值的类型映射
通过扁平化和嵌套映射，我们可以创建各种目标形状来满足我们的任何需求。