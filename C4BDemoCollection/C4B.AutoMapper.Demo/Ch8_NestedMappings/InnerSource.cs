﻿// <copyright file="InnerSource.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch8_NestedMappings
{
    /// <summary>
    /// InnerSource.
    /// </summary>
    internal class InnerSource
    {
        /// <summary>
        /// Gets or sets OtherValue.
        /// </summary>
        public int OtherValue { get; set; }
    }
}
