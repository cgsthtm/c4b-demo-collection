﻿// <copyright file="OuterSource.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch8_NestedMappings
{
    /// <summary>
    /// OuterSource.
    /// </summary>
    internal class OuterSource
    {
        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Gets or sets Inner.
        /// </summary>
        public InnerSource Inner { get; set; }
    }
}
