﻿# Mapping Inheritance
映射继承有两个功能：
- 从基类或接口配置继承映射配置
- 运行时多态映射
继承基类配置是可选的，您可以使用Include显式指定要从基类配置继承的映射，也可以使用IncludeBase显式指定要在派生类型配置中继承的映射：
~~~
CreateMap<BaseEntity, BaseDto>()
   .Include<DerivedEntity, DerivedDto>()
   .ForMember(dest => dest.SomeMember, opt => opt.MapFrom(src => src.OtherMember));

CreateMap<DerivedEntity, DerivedDto>();
~~~
或者
~~~
CreateMap<BaseEntity, BaseDto>()
   .ForMember(dest => dest.SomeMember, opt => opt.MapFrom(src => src.OtherMember));

CreateMap<DerivedEntity, DerivedDto>()
    .IncludeBase<BaseEntity, BaseDto>();
~~~
在上述每种情况下，派生映射都从基本映射继承自定义映射配置。
Include/IncludeBase递归应用，因此您只需包括层次结构中最近的级别。
如果对于某个基类，你有许多直接派生的类，为了方便，你可以包括所有来自基类映射配置的派生映射：
~~~
CreateMap<BaseEntity, BaseDto>()
    .IncludeAllDerived();

CreateMap<DerivedEntity, DerivedDto>();
~~~
请注意，这将搜索所有映射的派生类型，并且比显式指定派生映射要慢。

## Runtime polymorphism
~~~
public class Order { }
public class OnlineOrder : Order { }
public class MailOrder : Order { }

public class OrderDto { }
public class OnlineOrderDto : OrderDto { }
public class MailOrderDto : OrderDto { }

var configuration = new MapperConfiguration(cfg => {
    cfg.CreateMap<Order, OrderDto>()
        .Include<OnlineOrder, OnlineOrderDto>()
        .Include<MailOrder, MailOrderDto>();
    cfg.CreateMap<OnlineOrder, OnlineOrderDto>();
    cfg.CreateMap<MailOrder, MailOrderDto>();
});

// Perform Mapping
var order = new OnlineOrder();
var mapped = mapper.Map(order, order.GetType(), typeof(OrderDto));
Assert.IsType<OnlineOrderDto>(mapped);
~~~
您会注意到，由于映射的对象是OnlineOrder，AutoMapper发现OnlineOrder的映射比OrderDto更具体，并自动选择了OnlineOrder。

# Specifying inheritance in derived classes
您可以指定从派生类继承，而不是配置从基类继承：
~~~
var configuration = new MapperConfiguration(cfg => {
  cfg.CreateMap<Order, OrderDto>()
    .ForMember(o => o.Id, m => m.MapFrom(s => s.OrderId));
  cfg.CreateMap<OnlineOrder, OnlineOrderDto>()
    .IncludeBase<Order, OrderDto>();
  cfg.CreateMap<MailOrder, MailOrderDto>()
    .IncludeBase<Order, OrderDto>();
});
~~~

## As
对于简单的情况，您可以使用As将基础映射重定向到现有的派生映射：
~~~
    cfg.CreateMap<Order, OnlineOrderDto>();
    cfg.CreateMap<Order, OrderDto>().As<OnlineOrderDto>();
    
    mapper.Map<OrderDto>(new Order()).ShouldBeOfType<OnlineOrderDto>();
~~~

## Inheritance Mapping Priorities
这引入了额外的复杂性，因为有多种方式可以映射属性。这些来源的优先次序如下
- Explicit Mapping (using .MapFrom()) 显式映射（使用.MapFrom（））
- Inherited Explicit Mapping 继承的显式映射
- Ignore Property Mapping 忽略属性映射
- Convention Mapping (Properties that are matched via convention) 约定映射（通过约定匹配的属性）
为了演示这一点，让我们修改上面显示的类
~~~
//Domain Objects
public class Order { }
public class OnlineOrder : Order
{
    public string Referrer { get; set; }
}
public class MailOrder : Order { }

//Dtos
public class OrderDto
{
    public string Referrer { get; set; }
}

//Mappings
var configuration = new MapperConfiguration(cfg => {
    cfg.CreateMap<Order, OrderDto>()
        .Include<OnlineOrder, OrderDto>()
        .Include<MailOrder, OrderDto>()
        .ForMember(o=>o.Referrer, m=>m.Ignore());
    cfg.CreateMap<OnlineOrder, OrderDto>();
    cfg.CreateMap<MailOrder, OrderDto>();
});

// Perform Mapping
var order = new OnlineOrder { Referrer = "google" };
var mapped = mapper.Map(order, order.GetType(), typeof(OrderDto));
Assert.IsNull(mapped.Referrer);
~~~
请注意，在我们的映射配置中，我们忽略了Referrer（因为它不存在于order基类中），它的优先级高于约定映射，因此属性不会被映射。
如果您确实希望在从OnlineOrder到OrderDto的映射中映射Referrer属性，则应在映射中包含一个显式映射，如下所示：
~~~
    cfg.CreateMap<OnlineOrder, OrderDto>()
        .ForMember(o=>o.Referrer, m=>m.MapFrom(x=>x.Referrer));
~~~
总的来说，这个特性应该会让使用AutoMapper处理利用继承的类感觉更自然。