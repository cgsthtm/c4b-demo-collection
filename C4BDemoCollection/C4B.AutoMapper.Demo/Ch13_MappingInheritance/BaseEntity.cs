﻿// <copyright file="BaseEntity.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch13_MappingInheritance
{
    /// <summary>
    /// BaseEntity.
    /// </summary>
    internal class BaseEntity
    {
        /// <summary>
        /// Gets or sets OtherMember.
        /// </summary>
        public int OtherMember { get; set; }
    }
}
