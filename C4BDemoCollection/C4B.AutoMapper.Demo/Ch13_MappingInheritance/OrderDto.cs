﻿// <copyright file="OrderDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch13_MappingInheritance
{
    /// <summary>
    /// OrderDto.
    /// </summary>
    internal class OrderDto
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Referrer.
        /// </summary>
        public string Referrer { get; set; }
    }
}
