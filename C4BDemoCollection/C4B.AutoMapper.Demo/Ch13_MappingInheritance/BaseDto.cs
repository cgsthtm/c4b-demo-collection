﻿// <copyright file="BaseDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch13_MappingInheritance
{
    /// <summary>
    /// BaseDto.
    /// </summary>
    internal class BaseDto
    {
        /// <summary>
        /// Gets or sets SomeMember.
        /// </summary>
        public int SomeMember { get; set; }
    }
}
