﻿// <copyright file="DerivedEntity.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch13_MappingInheritance
{
    /// <summary>
    /// DerivedEntity.
    /// </summary>
    internal class DerivedEntity : BaseEntity
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public int Id { get; set; }
    }
}
