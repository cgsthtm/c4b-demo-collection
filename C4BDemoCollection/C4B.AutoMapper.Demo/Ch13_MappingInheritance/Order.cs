﻿// <copyright file="Order.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch13_MappingInheritance
{
    /// <summary>
    /// Order.
    /// </summary>
    internal class Order
    {
        /// <summary>
        /// Gets or sets OrderId.
        /// </summary>
        public int OrderId { get; set; }
    }
}
