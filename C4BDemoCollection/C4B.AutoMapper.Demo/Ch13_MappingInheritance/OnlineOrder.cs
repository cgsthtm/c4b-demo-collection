﻿// <copyright file="OnlineOrder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch13_MappingInheritance
{
    /// <summary>
    /// OnlineOrder.
    /// </summary>
    internal class OnlineOrder : Order
    {
        /// <summary>
        /// Gets or sets Referrer.
        /// </summary>
        public string Referrer { get; set; }
    }
}
