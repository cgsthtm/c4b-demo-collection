﻿// <copyright file="MappingInheritanceTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch13_MappingInheritance
{
    using global::AutoMapper;
    using Xunit;

    /// <summary>
    /// MappingInheritanceTest.
    /// </summary>
    public class MappingInheritanceTest
    {
        /// <summary>
        /// Test_MappingInheritance.
        /// </summary>
        [Fact]
        public static void Test_MappingInheritance()
        {
            // 继承基类配置是可选的，您可以使用Include显式指定要从基类配置继承的映射，也可以使用IncludeBase显式指定要在派生类型配置中继承的映射：
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BaseEntity, BaseDto>()
                   .Include<DerivedEntity, DerivedDto>()
                   .ForMember(dest => dest.SomeMember, opt => opt.MapFrom(src => src.OtherMember));

                cfg.CreateMap<DerivedEntity, DerivedDto>();
            });
            var mapper = configuration.CreateMapper();
            var derivedEntity = new DerivedEntity()
            {
                OtherMember = 1,
                Id = 2,
            };
            var derivedDto = mapper.Map<DerivedDto>(derivedEntity);
            Assert.Equal(1, derivedDto.SomeMember);

            var configuration1 = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BaseEntity, BaseDto>()
                   .ForMember(dest => dest.SomeMember, opt => opt.MapFrom(src => src.OtherMember));

                cfg.CreateMap<DerivedEntity, DerivedDto>()
                    .IncludeBase<BaseEntity, BaseDto>();
            });
            var mapper1 = configuration1.CreateMapper();
            var derivedEntity1 = new DerivedEntity()
            {
                OtherMember = 2,
                Id = 1,
            };
            var derivedDto1 = mapper1.Map<DerivedDto>(derivedEntity1);
            Assert.Equal(2, derivedDto1.SomeMember);

            // 如果对于某个基类，你有许多直接派生的类，为了方便，你可以包括所有来自基类映射配置的派生映射：
            var configuration2 = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BaseEntity, BaseDto>()
                   .IncludeAllDerived() // 请注意，这将搜索所有映射的派生类型，并且比显式指定派生映射要慢。
                   .ForMember(dest => dest.SomeMember, opt => opt.MapFrom(src => src.OtherMember));

                cfg.CreateMap<DerivedEntity, DerivedDto>()
                    .IncludeBase<BaseEntity, BaseDto>();
            });
        }

        /// <summary>
        /// Test_RuntimePolymorphism.
        /// </summary>
        [Fact]
        public static void Test_RuntimePolymorphism()
        {
            // 您会注意到，由于映射的对象是OnlineOrder，AutoMapper发现OnlineOrder的映射比OrderDto更具体，并自动选择了OnlineOrder。
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Order, OrderDto>()
                    .Include<OnlineOrder, OnlineOrderDto>()
                    .Include<MailOrder, MailOrderDto>();
                cfg.CreateMap<OnlineOrder, OnlineOrderDto>();
                cfg.CreateMap<MailOrder, MailOrderDto>();
            });

            // Perform Mapping
            var mapper = configuration.CreateMapper();
            var order = new OnlineOrder();
            var mapped = mapper.Map(order, order.GetType(), typeof(OrderDto));
            Assert.IsType<OnlineOrderDto>(mapped);
        }

        /// <summary>
        /// Test_SpecifyingInheritanceInDerivedClasses.
        /// </summary>
        [Fact]
        public static void Test_SpecifyingInheritanceInDerivedClasses()
        {
            // 您可以指定从派生类继承，而不是配置从基类继承：
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Order, OrderDto>()
                  .ForMember(o => o.Id, m => m.MapFrom(s => s.OrderId));
                cfg.CreateMap<OnlineOrder, OnlineOrderDto>()
                  .IncludeBase<Order, OrderDto>();
                cfg.CreateMap<MailOrder, MailOrderDto>()
                  .IncludeBase<Order, OrderDto>();
            });
            var mapper = configuration.CreateMapper();
            var order = new OnlineOrder() { OrderId = 6 };
            var mapped = mapper.Map(order, order.GetType(), typeof(OrderDto));
            Assert.IsType<OnlineOrderDto>(mapped);
            Assert.Equal(6, ((OnlineOrderDto)mapped).Id);
        }

        /// <summary>
        /// Teset_As.
        /// </summary>
        [Fact]
        public static void Test_As()
        {
            // 对于简单的情况，您可以使用As将基础映射重定向到现有的派生映射：
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Order, OnlineOrderDto>();
                cfg.CreateMap<Order, OrderDto>().As<OnlineOrderDto>();
            });
            var mapper = configuration.CreateMapper();
            var mapped = mapper.Map<OrderDto>(new Order());
            Assert.IsType<OnlineOrderDto>(mapped);
        }

        /// <summary>
        /// Test_InheritanceMappingProperties.
        /// </summary>
        [Fact]
        public static void Test_InheritanceMappingProperties()
        {
            /*这引入了额外的复杂性，因为有多种方式可以映射属性。这些来源的优先次序如下
             * 1.显式映射（使用.MapFrom（）） Explicit Mapping (using .MapFrom())
             * 2.继承的显式映射 Inherited Explicit Mapping
             * 3.忽略属性映射 Ignore Property Mapping
             * 4.约定映射（通过约定匹配的属性） Convention Mapping (Properties that are matched via convention)
             */
            // 请注意，在我们的映射配置中，我们忽略了Referrer（因为它不存在于order基类中），它的优先级高于约定映射，因此属性不会被映射。
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Order, OrderDto>()
                    .Include<OnlineOrder, OrderDto>()
                    .Include<MailOrder, OrderDto>()
                    .ForMember(o => o.Referrer, m => m.Ignore());
                cfg.CreateMap<OnlineOrder, OrderDto>();
                cfg.CreateMap<MailOrder, OrderDto>();
            });
            var mapper = configuration.CreateMapper();
            var order = new OnlineOrder { Referrer = "google" };
            var mapped = mapper.Map(order, order.GetType(), typeof(OrderDto));
            Assert.Null(((OrderDto)mapped).Referrer);

            // 如果您确实希望在从OnlineOrder到OrderDto的映射中映射Referrer属性，则应在映射中包含一个显式映射，如下所示：
            var configuration1 = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Order, OrderDto>()
                    .Include<OnlineOrder, OrderDto>()
                    .Include<MailOrder, OrderDto>()
                    .ForMember(o => o.Referrer, m => m.Ignore());
                cfg.CreateMap<OnlineOrder, OrderDto>()
                    .ForMember(o => o.Referrer, m => m.MapFrom(x => x.Referrer));
                cfg.CreateMap<MailOrder, OrderDto>();
            });
            var mapper1 = configuration1.CreateMapper();
            var order1 = new OnlineOrder { Referrer = "google" };
            var mapped1 = mapper1.Map(order1, order1.GetType(), typeof(OrderDto));
            Assert.NotNull(((OrderDto)mapped1).Referrer);
        }
    }
}
