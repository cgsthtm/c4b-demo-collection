﻿// <copyright file="ReverseMappingAndUnflatteningTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch12_ReverseMappingAndUnflattening
{
    using global::AutoMapper;
    using Xunit;

    /// <summary>
    /// ReverseMappingAndUnflatteningTest.
    /// </summary>
    public class ReverseMappingAndUnflatteningTest
    {
        /// <summary>
        /// Test_ReverseMappingAndUnflattening.
        /// </summary>
        [Fact]
        public static void Test_ReverseMappingAndUnflattening()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Order, OrderDto>()
                   .ReverseMap(); // 通过调用ReverseMap，AutoMapper创建了一个反向映射配置
            });
            var customer = new Customer
            {
                Name = "Bob",
            };
            var order = new Order
            {
                Customer = customer,
                Total = 15.8m,
            };
            var mapper = configuration.CreateMapper();
            var orderDto = mapper.Map<Order, OrderDto>(order);
            orderDto.CustomerName = "Joe";
            mapper.Map(orderDto, order);
            Assert.Equal("Joe", order.Customer.Name);
        }

        /// <summary>
        /// Test_CustomizingReverseMapping.
        /// </summary>
        [Fact]
        public static void Test_CustomizingReverseMapping()
        {
            // AutoMapper将根据原始的扁平化自动从“CustomerName”反转映射“Customer.Name”。如果使用“映射自”，AutoMapper将尝试反转映射：
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Order, OrderDto>()
                  .ForMember(d => d.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
                  .ReverseMap();
            });
            var customer = new Customer
            {
                Name = "Bob",
            };
            var order = new Order
            {
                Customer = customer,
                Total = 15.8m,
            };
            var mapper = configuration.CreateMapper();
            var orderDto = mapper.Map<Order, OrderDto>(order);
            Assert.Equal("Bob", orderDto.CustomerName);
            orderDto.CustomerName = "Joe";
            mapper.Map(orderDto, order);
            Assert.Equal("Joe", order.Customer.Name);

            // 只要MapFrom路径是成员访问器，AutoMapper将从同一路径（CustomerName=>Customer.Name）展开。
            // 如果你需要自定义它，对于反向映射，你可以使用ForPath：
            var configuration1 = new MapperConfiguration(cfg =>
            {
                // 在大多数情况下，你不需要这个，因为原始的MapFrom将为你反转。当获取和设置值的路径不同时使用ForPath。
                cfg.CreateMap<Order, OrderDto>()
                  .ForMember(d => d.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
                  .ReverseMap()
                  .ForPath(s => s.Customer.Name, opt => opt.MapFrom(src => src.CustomerName));
            });
            var customer1 = new Customer
            {
                Name = "Bob",
            };
            var order1 = new Order
            {
                Customer = customer1,
                Total = 15.8m,
            };
            var mapper1 = configuration1.CreateMapper();
            var orderDto1 = mapper1.Map<Order, OrderDto>(order1);
            Assert.Equal("Bob", orderDto1.CustomerName);
            orderDto1.CustomerName = "Joe";
            mapper1.Map(orderDto1, order1);
            Assert.Equal("Joe", order1.Customer.Name);

            // 如果您不希望展开行为，可以删除对ReverseMap的调用并创建两个单独的贴图。或者，您可以使用“忽略”：
            var configuration2 = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Order, OrderDto>()
                  .ForMember(d => d.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
                  .ReverseMap()
                  .ForPath(s => s.Customer.Name, opt => opt.Ignore());
            });
            var customer2 = new Customer
            {
                Name = "Bob",
            };
            var order2 = new Order
            {
                Customer = customer2,
                Total = 15.8m,
            };
            var mapper2 = configuration2.CreateMapper();
            var orderDto2 = mapper2.Map<Order, OrderDto>(order2);
            Assert.Equal("Bob", orderDto2.CustomerName);
            orderDto2.CustomerName = "Joe";
            mapper2.Map(orderDto2, order2);
            Assert.NotEqual("Joe", order2.Customer.Name);
        }

        /// <summary>
        /// Test_IncludeMembers.
        /// </summary>
        [Fact]
        public static void Test_IncludeMembers()
        {
            // ReverseMap还集成了IncludeMembers和配置，如
            ////ForMember(destination => destination.IncludedMember, member => member.MapFrom(source => source))
        }
    }
}
