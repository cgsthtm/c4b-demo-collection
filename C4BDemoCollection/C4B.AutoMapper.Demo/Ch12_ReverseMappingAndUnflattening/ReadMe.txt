﻿# Reverse Mapping and Unflattening
从6.1.0开始，AutoMapper现在支持更丰富的反向映射支持。鉴于我们的实体：
~~~
public class Order {
  public decimal Total { get; set; }
  public Customer Customer { get; set; }
}

public class Customer {
  public string Name { get; set; }
}
~~~
我们可以将其展开为DTO：
~~~
public class OrderDto {
  public decimal Total { get; set; }
  public string CustomerName { get; set; }
}
~~~
我们可以映射两个方向，包括unflattening：
~~~
var configuration = new MapperConfiguration(cfg => {
  cfg.CreateMap<Order, OrderDto>()
     .ReverseMap();
});
~~~
通过调用ReverseMap，AutoMapper创建了一个反向映射配置，其中包括展开：
~~~
var customer = new Customer {
  Name = "Bob"
};

var order = new Order {
  Customer = customer,
  Total = 15.8m
};

var orderDto = mapper.Map<Order, OrderDto>(order);

orderDto.CustomerName = "Joe";

mapper.Map(orderDto, order);

order.Customer.Name.ShouldEqual("Joe");
~~~
如果你想要展开，你必须配置Entity->Dto然后调用ReverseMap从Dto->Entity创建展开类型的映射配置。

## Customizing reverse mapping
AutoMapper将根据原始的扁平化自动从“CustomerName”反转映射“Customer.Name”。如果使用“MapFrom”，AutoMapper将尝试反转映射：
~~~
cfg.CreateMap<Order, OrderDto>()
  .ForMember(d => d.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
  .ReverseMap();
~~~
只要MapFrom路径是成员访问器，AutoMapper将从同一路径（CustomerName=>Customer.Name）展开。
如果你需要自定义它，对于反向映射，你可以使用ForPath：
~~~
cfg.CreateMap<Order, OrderDto>()
  .ForMember(d => d.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
  .ReverseMap()
  .ForPath(s => s.Customer.Name, opt => opt.MapFrom(src => src.CustomerName));
~~~
在大多数情况下，你不需要这个，因为原始的MapFrom将为你反转。当获取和设置值的路径不同时使用ForPath。
如果您不希望展开行为，可以删除对ReverseMap的调用并创建两个单独的贴图。或者，您可以使用“忽略”：
~~~
cfg.CreateMap<Order, OrderDto>()
  .ForMember(d => d.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
  .ReverseMap()
  .ForPath(s => s.Customer.Name, opt => opt.Ignore());
~~~

## IncludeMembers
ReverseMap还集成了IncludeMembers和配置，如
~~~
ForMember(destination => destination.IncludedMember, member => member.MapFrom(source => source))
~~~