﻿// <copyright file="Order.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.AutoMapper.Demo.Ch12_ReverseMappingAndUnflattening
{
    /// <summary>
    /// Order.
    /// </summary>
    internal class Order
    {
        /// <summary>
        /// Gets or sets Total.
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// Gets or sets Customer.
        /// </summary>
        public Customer Customer { get; set; }
    }
}
