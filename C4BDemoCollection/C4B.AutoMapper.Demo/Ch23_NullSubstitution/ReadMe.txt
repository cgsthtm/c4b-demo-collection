﻿# Null Substitution
如果成员链中沿着的任何位置的源值为空，则使用替代替换可以为目标成员提供替代值。这意味着它不是从null映射，而是从您提供的值映射。
~~~
var config = new MapperConfiguration(cfg => cfg.CreateMap<Source, Dest>()
    .ForMember(destination => destination.Value, opt => opt.NullSubstitute("Other Value")));

var source = new Source { Value = null };
var mapper = config.CreateMapper();
var dest = mapper.Map<Source, Dest>(source);

dest.Value.ShouldEqual("Other Value");

source.Value = "Not null";

dest = mapper.Map<Source, Dest>(source);

dest.Value.ShouldEqual("Not null");
~~~
假设替代是源成员类型，并且将在到目标类型之后经历任何映射/转换。