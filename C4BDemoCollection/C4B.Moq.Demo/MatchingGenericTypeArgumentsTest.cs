﻿// <copyright file="MatchingGenericTypeArgumentsTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using global::Moq;
    using Xunit;

    /// <summary>
    /// MatchingGenericTypeArgumentsTest.
    /// </summary>
    public class MatchingGenericTypeArgumentsTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            var mock = new Mock<IFoo>();

            // Generic arguments are matched using the usual type polymorphism rules,
            // so if you want to match any type, you can simply use object as type argument in many cases:
            mock.Setup(m => m.M1<object>()).Returns(true);
            Assert.True(mock.Object.M1<object>());
            Assert.True(mock.Object.M1<int>());

            // Type matchers (Moq 4.13+)
            // In some situations, that might not work. Starting with Moq 4.13, you can use explicit type matchers, e.g.It.IsAnyType
            // which is essentially a placeholder for a type (just like It.IsAny<T>() is a placeholder for any value):
            // matches any type argument:
            mock.Setup(m => m.M1<It.IsAnyType>()).Returns(true);

            // matches only type arguments that are subtypes of / implement T:
            mock.Setup(m => m.M1<It.IsSubtype<string>>()).Returns(true);
            Assert.True(mock.Object.M1<string>());

            // use of type matchers is allowed in the argument list:
            mock.Setup(m => m.M2(It.IsAny<It.IsAnyType>())).Returns(true);
            mock.Setup(m => m.M2(It.IsAny<It.IsSubtype<int>>())).Returns(true);
        }
    }
}