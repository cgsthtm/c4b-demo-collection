﻿// <copyright file="MatchingArgumentsTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System.Text.RegularExpressions;
    using global::Moq;
    using Xunit;

    /// <summary>
    /// MatchingArgumentsTest.
    /// </summary>
    internal class MatchingArgumentsTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            var mock = new Mock<IFoo>();

            // any value
            mock.Setup(foo => foo.DoSomething(It.IsAny<string>())).Returns(true);
            Assert.True(mock.Object.DoSomething("123"));

            // any value passed in a `ref` parameter (requires Moq 4.8 or later):
            mock.Setup(foo => foo.Submit(ref It.Ref<Bar>.IsAny)).Returns(true);
            Bar bar = new Bar();
            Assert.True(mock.Object.Submit(ref bar));

            // matching Func<int>, lazy evaluated
            mock.Setup(foo => foo.Add(It.Is<int>(i => i % 2 == 0))).Returns(true);
            Assert.True(mock.Object.Add(2));
            Assert.False(mock.Object.Add(3));

            // matching ranges
            mock.Setup(foo => foo.Add(It.IsInRange<int>(0, 10, Range.Inclusive))).Returns(true);
            Assert.True(mock.Object.Add(0));
            Assert.True(mock.Object.Add(10));
            Assert.False(mock.Object.Add(-1));
            Assert.False(mock.Object.Add(11));

            // matching regex
            mock.Setup(x => x.DoSomethingStringy(It.IsRegex("[a-d]+", RegexOptions.IgnoreCase))).Returns("foo");
            Assert.Equal(mock.Object.DoSomethingStringy("a"), "foo");
            Assert.Equal(mock.Object.DoSomethingStringy("A"), "foo");
            Assert.Equal(mock.Object.DoSomethingStringy("D"), "foo");
            Assert.NotEqual(mock.Object.DoSomethingStringy("e"), "foo");
        }
    }
}
