﻿// <copyright file="CallbacksTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;
    using System.Collections.Generic;
    using global::Moq;
    using Xunit;

    /// <summary>
    /// SubmitCallback.
    /// </summary>
    /// <param name="bar">bar.</param>
    internal delegate void SubmitCallback(ref Bar bar);

    /// <summary>
    /// CallbacksTest.
    /// </summary>
    internal class CallbacksTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            var mock = new Mock<IFoo>();
            var calls = 0;
            var callArgs = new List<string>();

            mock.Setup(foo => foo.DoSomething("ping"))
                .Callback(() => calls++)
                .Returns(true);
            mock.Object.DoSomething("ping");
            Assert.Equal(1, calls);

            // access invocation arguments
            mock.Setup(foo => foo.DoSomething(It.IsAny<string>()))
                .Callback((string s) => callArgs.Add(s))
                .Returns(true);
            mock.Object.DoSomething("1");
            Assert.Contains<string>("1", callArgs);

            // alternate equivalent generic method syntax
            mock.Setup(foo => foo.DoSomething(It.IsAny<string>()))
                .Callback<string>(s => callArgs.Add(s))
                .Returns(true);
            mock.Object.DoSomething("2");
            Assert.Contains<string>("2", callArgs);

            // access arguments for methods with multiple parameters
            mock.Setup(foo => foo.DoSomething(It.IsAny<int>(), It.IsAny<string>()))
                .Callback<int, string>((i, s) => callArgs.Add(s))
                .Returns(true);
            mock.Object.DoSomething("3");
            Assert.Contains<string>("3", callArgs);

            // callbacks can be specified before and after invocation
            mock.Setup(foo => foo.DoSomething("ping"))
                .Callback(() => Console.WriteLine("Before returns"))
                .Returns(true)
                .Callback(() => Console.WriteLine("After returns"));
            mock.Object.DoSomething("ping");

            // callbacks for methods with `ref` / `out` parameters are possible but require some work (and Moq 4.8 or later):
            mock.Setup(foo => foo.Submit(ref It.Ref<Bar>.IsAny))
                .Callback(new SubmitCallback((ref Bar bar) => Console.WriteLine("Submitting a Bar!")));
            var barf = new Bar();
            mock.Object.Submit(ref barf);

            // returning different values on each invocation
            mock = new Mock<IFoo>();
            calls = 0;
            mock.Setup(foo => foo.GetCount())
                .Callback(() => calls++)
                .Returns(() => calls);

            // returns 0 on first invocation, 1 on the next, and so on
            Console.WriteLine(mock.Object.GetCount());

            // access invocation arguments and set to mock setup property
            mock.SetupProperty(foo => foo.Bar);
            mock.Object.Bar = new Bar { Baz = new Baz() };
            mock.Setup(foo => foo.DoSomething(It.IsAny<string>()))
                .Callback((string s) => mock.Object.Bar.Baz.Name = s)
                .Returns(true);
            mock.Object.DoSomething("cgs");
            Assert.Equal("cgs", mock.Object.Bar.Baz.Name);
        }
    }
}