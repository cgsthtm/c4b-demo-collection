﻿// <copyright file="LINQtoMocksTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;
    using System.Web.Mvc;
    using global::Moq;
    using Xunit;

    /// <summary>
    /// LINQtoMocksTest.
    /// </summary>
    public class LINQtoMocksTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // Moq is the one and only mocking framework that allows specifying mock behavior via declarative specification queries.
            // You can think of LINQ to Mocks as:
            // from the universe of mocks, get me one/those that behave like this (by Fernando Simonazzi)

            // Keep that query form in mind when reading the specifications:
            var services = Mock.Of<IServiceProvider>(sp =>
                sp.GetService(typeof(IRepository)) == Mock.Of<IRepository>(r => r.IsAuthenticated == true) &&
                sp.GetService(typeof(IAuthentication)) == Mock.Of<IAuthentication>(a => a.AuthenticationType == "OAuth"));
            var rep = services.GetService(typeof(IRepository)) as IRepository;
            Assert.True(rep.IsAuthenticated);

            // Multiple setups on a single mock and its recursive mocks
            ControllerContext context = Mock.Of<ControllerContext>(ctx =>
                 ctx.HttpContext.User.Identity.Name == "kzu" &&
                 ctx.HttpContext.Request.IsAuthenticated == true &&
                 ctx.HttpContext.Request.Url == new Uri("http://moq.github.io/moq4/") &&
                 ctx.HttpContext.Response.ContentType == "application/xml");
            Assert.Equal("kzu", context.HttpContext.User.Identity.Name);

            // Setting up multiple chained mocks:
            var context2 = Mock.Of<ControllerContext>(ctx =>
                 ctx.HttpContext.Request.Url == new Uri("http://moqthis.me") &&
                 ctx.HttpContext.Response.ContentType == "application/xml" &&
                 // Chained mock specification
                 ctx.HttpContext.GetSection("server") == Mock.Of<ServerSection>(config =>
                     config.Server.ServerUrl == new Uri("http://moqthis.com/api")));
            Assert.Equal("application/xml", context2.HttpContext.Response.ContentType);

            // LINQ to Mocks is great for quickly stubbing out dependencies that typically don't need further verification.
            // If you do need to verify later some invocation on those mocks, you can easily retrieve them with Mock.Get(instance).
        }
    }
}