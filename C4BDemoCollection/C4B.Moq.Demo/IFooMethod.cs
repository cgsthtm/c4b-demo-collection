﻿// <copyright file="IFooMethod.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System.Threading.Tasks;

    /// <summary>
    /// IFooMethod.
    /// </summary>
    public interface IFooMethod
    {
        /// <summary>
        /// FooMethod.
        /// </summary>
        /// <param name="s">s.</param>
        /// <returns>task.</returns>
        Task<string> FooMethod(string s);
    }
}