﻿// <copyright file="FooEventArgs.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;

    /// <summary>
    /// FooEventArgs.
    /// </summary>
    internal class FooEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FooEventArgs"/> class.
        /// </summary>
        /// <param name="name">name.</param>
        public FooEventArgs(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Gets Name.
        /// </summary>
        public string Name { get; private set; }
    }
}
