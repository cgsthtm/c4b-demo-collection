﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;

    /// <summary>
    /// Program.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            ////MethodTest.Test();
            ////AsyncMethodTest.Test();
            ////MatchingArgumentsTest.Test();
            ////PropertiesTest.Test();
            ////EventsTest.Test();
            ////CallbacksTest.Test();
            ////VerificationTest verification = new VerificationTest();
            ////verification.Test();
            ////CustomizingMockBehaviorTest.Test();
            ////MiscellaneousTest.Test();
            ////AdvancedFeaturesTest.Test();
            ////MatchingGenericTypeArgumentsTest.Test();
            LINQtoMocksTest.Test();

            Console.ReadLine();
        }
    }
}