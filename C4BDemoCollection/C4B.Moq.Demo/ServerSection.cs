﻿// <copyright file="ServerSection.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;

    /// <summary>
    /// ServerSection.
    /// </summary>
    public interface ServerSection
    {
        /// <summary>
        /// Gets or sets Server.
        /// </summary>
        public Server Server { get; set; }
    }
}