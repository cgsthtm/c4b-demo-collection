﻿// <copyright file="CommandBase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;

    /// <summary>
    /// CommandBase.
    /// </summary>
    public class CommandBase
    {
        /// <summary>
        /// PrintExecuteResult.
        /// </summary>
        /// <param name="arg">arg.</param>
        public void PrintExecuteResult(string arg)
        {
            Console.WriteLine(this.Execute(arg));
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <returns>int.</returns>
        protected virtual int Execute()
        {
            return 0;
        } // (1)

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="arg">arg.</param>
        /// <returns>b.</returns>
        protected virtual bool Execute(string arg)
        {
            if (arg.Equals("cgs"))
            {
                return true;
            }
            else
            {
                return false;
            }
        } // (2)
    }
}