﻿// <copyright file="PropertiesTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using global::Moq;
    using Xunit;

    /// <summary>
    /// PropertiesTest.
    /// </summary>
    internal class PropertiesTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            var mock = new Mock<IFoo>();

            mock.Setup(foo => foo.Name).Returns("bar");
            Assert.Equal("bar", mock.Object.Name);

            // auto-mocking hierarchies (a.k.a. recursive mocks)
            mock.Setup(foo => foo.Bar.Baz.Name).Returns("baz");
            Assert.Equal("baz", mock.Object.Bar.Baz.Name);

            // expects an invocation to set the value to "foo"
            mock.SetupSet(foo => foo.Name = "foo");

            // or verify the setter directly
            Assert.Throws<MockException>(() => mock.VerifySet(foo => foo.Name = "foo1"));
            Assert.Throws<MockException>(() => mock.VerifySet(foo => foo.Name = "foo"));
            mock.Object.Name = "foo1";
            mock.VerifySet(foo => foo.Name = "foo1");
            mock.Object.Name = "foo";
            mock.VerifySet(foo => foo.Name = "foo");

            // Setup a property so that it will automatically start tracking its value (also known as Stub):
            // start "tracking" sets/gets to this property
            mock.SetupProperty(f => f.Name);

            // alternatively, provide a default value for the stubbed property
            mock.SetupProperty(f => f.Name, "fo");

            // Now you can do:
            IFoo fo = mock.Object;

            // Initial value was stored
            Assert.Equal("fo", fo.Name);

            // New value set which changes the initial value
            fo.Name = "bar";
            Assert.Equal("bar", fo.Name);

            // Stub all properties on a mock:
            mock.SetupAllProperties();
            IFoo fo2 = mock.Object;
            fo2.Value = 1;
            Assert.Equal(1, fo2.Value);
        }
    }
}