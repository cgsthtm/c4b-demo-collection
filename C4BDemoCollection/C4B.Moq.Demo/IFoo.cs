﻿// <copyright file="IFoo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;
    using System.Threading.Tasks;

    // Assumptions:

    /// <summary>
    /// MyEventHandler.
    /// </summary>
    /// <param name="i">int.</param>
    /// <param name="b">bool.</param>
    public delegate void MyEventHandler(int i, bool b);

    /// <summary>
    /// IFoo.
    /// </summary>
    public interface IFoo : IFooMethod
    {
        /// <summary>
        /// MyEvent.
        /// </summary>
        event MyEventHandler MyEvent;

        /// <summary>
        /// FooEvent.
        /// </summary>
        event EventHandler FooEvent;

        /// <summary>
        /// Gets or sets Bar.
        /// </summary>
        Bar Bar { get; set; }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets Value.
        /// </summary>
        int Value { get; set; }

        /// <summary>
        /// DoSomething.
        /// </summary>
        /// <param name="value">v.</param>
        /// <returns>s.</returns>
        bool DoSomething(string value);

        /// <summary>
        /// DoSomething.
        /// </summary>
        /// <param name="number">n.</param>
        /// <param name="value">v.</param>
        /// <returns>b.</returns>
        bool DoSomething(int number, string value);

        /// <summary>
        /// DoSomethingAsync.
        /// </summary>
        /// <returns>t.</returns>
        Task<bool> DoSomethingAsync();

        /// <summary>
        /// DoSomethingStringy.
        /// </summary>
        /// <param name="value">v.</param>
        /// <returns>s.</returns>
        string DoSomethingStringy(string value);

        /// <summary>
        /// TryParse.
        /// </summary>
        /// <param name="value">v.</param>
        /// <param name="outputValue">o.</param>
        /// <returns>b.</returns>
        bool TryParse(string value, out string outputValue);

        /// <summary>
        /// Submit.
        /// </summary>
        /// <param name="bar">bar.</param>
        /// <returns>b.</returns>
        bool Submit(ref Bar bar);

        /// <summary>
        /// GetCount.
        /// </summary>
        /// <returns>i.</returns>
        int GetCount();

        /// <summary>
        /// Add.
        /// </summary>
        /// <param name="value">v.</param>
        /// <returns>b.</returns>
        bool Add(int value);

        /// <summary>
        /// M1.
        /// </summary>
        /// <typeparam name="T">T.</typeparam>
        /// <returns>b.</returns>
        bool M1<T>();

        /// <summary>
        /// M2.
        /// </summary>
        /// <typeparam name="T">T.</typeparam>
        /// <param name="arg">a.</param>
        /// <returns>b.</returns>
        bool M2<T>(T arg);
    }
}