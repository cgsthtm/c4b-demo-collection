﻿// <copyright file="AsyncMethodTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using global::Moq;
    using Xunit;

    /// <summary>
    /// AsyncMethodTest.
    /// </summary>
    internal class AsyncMethodTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // There are several ways to set up "async" methods (e.g. methods returning a Task<T> or ValueTask<T>):
            var mock = new Mock<IFoo>();

            // Starting with Moq 4.16, you can simply mock.Setup the returned task's .Result property.
            // This works in nearly all setup and verification expressions:
            mock.Setup(foo => foo.DoSomethingAsync().Result).Returns(true);
            Assert.True(mock.Object.DoSomethingAsync().Result);

            // In earlier versions, use setup helper methods like setup.ReturnsAsync, setup.ThrowsAsync where they are available:
            mock.Setup(foo => foo.DoSomethingAsync()).ReturnsAsync(true);
            Assert.True(mock.Object.DoSomethingAsync().Result);

            // (You could also do the following, but that will trigger a compiler warning about synchronous execution of the async lambda:)
            mock.Setup(foo => foo.DoSomethingAsync()).Returns(async () => true);
            Assert.True(mock.Object.DoSomethingAsync().Result);
        }
    }
}