﻿// <copyright file="IAuthentication.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    /// <summary>
    /// IAuthentication.
    /// </summary>
    public interface IAuthentication
    {
        /// <summary>
        /// Gets or sets AuthenticationType.
        /// </summary>
        string AuthenticationType { get; set; }
    }
}