﻿// <copyright file="Baz.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    /// <summary>
    /// Baz.
    /// </summary>
    public class Baz
    {
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public virtual string Name { get; set; }
    }
}