﻿// <copyright file="IBaz.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System.Threading.Tasks;

    /// <summary>
    /// IBaz.
    /// </summary>
    public interface IBaz
    {
        /// <summary>
        /// BazMethod.
        /// </summary>
        /// <param name="s">s.</param>
        /// <returns>task.</returns>
        Task<string> BazMethod(string s);
    }
}