﻿// <copyright file="CustomizingMockBehaviorTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using global::Moq;

    /// <summary>
    /// CustomizingMockBehaviorTest.
    /// </summary>
    public class CustomizingMockBehaviorTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // Make mock behave like a "true Mock", raising exceptions for anything that doesn't have a corresponding expectation:
            // in Moq slang a "Strict" mock; default behavior is "Loose" mock,
            // which never throws and returns default values or empty arrays, enumerables, etc. if no expectation is set for a member
            var mock = new Mock<IFoo>(MockBehavior.Strict);

            // Invoke base class implementation if no expectation overrides the member (a.k.a. "Partial Mocks" in Rhino Mocks):
            // default is false. (this is required if you are mocking Web/Html controls in System.Web!)
            var mock1 = new Mock<IFoo> { CallBase = true };

            // Make an automatic recursive mock:
            // a mock that will return a new mock for every member that doesn't have an expectation and whose return value can be mocked
            // (i.e. it is not a value type)
            var mock3 = new Mock<IFoo> { DefaultValue = DefaultValue.Mock };
            // default is DefaultValue.Empty
            // this property access would return a new mock of Bar as it's "mock-able"
            Bar value = mock3.Object.Bar;
            // the returned mock is reused, so further accesses to the property return 
            // the same mock instance. this allows us to also use this instance to 
            // set further expectations on it if we want
            var barMock = Mock.Get(value);
            barMock.Setup(b => b.Submit()).Returns(true);

            // Centralizing mock instance creation and management:
            // you can create and verify all mocks in a single place by using a MockRepository,
            // which allows setting the MockBehavior, its CallBase and DefaultValue consistently
            var repository = new MockRepository(MockBehavior.Strict) { DefaultValue = DefaultValue.Mock };
            // Create a mock using the repository settings
            var fooMock = repository.Create<IFoo>();
            // Create a mock overriding the repository settings
            var barMock2 = repository.Create<Bar>(MockBehavior.Loose);
            // Verify all verifiable expectations on all mocks created through the repository
            repository.Verify();
        }
    }
}
