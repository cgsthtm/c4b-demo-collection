﻿// <copyright file="Bar.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    /// <summary>
    /// Bar.
    /// </summary>
    public class Bar
    {
        /// <summary>
        /// Gets or sets Baz.
        /// </summary>
        public virtual Baz Baz { get; set; }

        /// <summary>
        /// Gets or sets Submit.
        /// </summary>
        /// <returns>b.</returns>
        public virtual bool Submit()
        {
            return false;
        }
    }
}