﻿// <copyright file="Server.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;

    /// <summary>
    /// Server.
    /// </summary>
    public class Server
    {
        /// <summary>
        /// Gets or sets ServerUrl.
        /// </summary>
        public Uri ServerUrl { get; set; }
    }
}
