﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    /// <summary>
    /// IRepository.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Gets or sets IsAuthenticated.
        /// </summary>
        bool IsAuthenticated { get; set; }
    }
}