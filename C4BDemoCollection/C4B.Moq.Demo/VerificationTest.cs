﻿// <copyright file="VerificationTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;
    using global::Moq;
    using Xunit;

    /// <summary>
    /// VerificationTest.
    /// </summary>
    public class VerificationTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        [Fact]
        public void Test()
        {
            var mock = new Mock<IFoo>();

            // Verifies that a specific invocation matching the given expression was performed on the mock.
            mock.Object.DoSomething("ping");
            mock.Verify(foo => foo.DoSomething("ping"));

            // Verify with custom error message for failure
            Assert.Throws<MockException>(() => mock.Verify(foo => foo.DoSomething("ping1"), "When doing operation X, the service should be pinged always"));

            // Method should never be called
            mock.Verify(foo => foo.DoSomething("ping2"), Times.Never());

            // Called at least once
            mock.Verify(foo => foo.DoSomething("ping"), Times.AtLeastOnce());

            // Verifies that a property was read on the mock.
            // Verify getter invocation, regardless of value.
            var name = mock.Object.Name;
            mock.VerifyGet(foo => foo.Name);

            // Verify setter invocation, regardless of value.
            mock.Object.Name = "cgs";
            mock.VerifySet(foo => foo.Name);

            // Verify setter called with specific value
            Assert.Throws<MockException>(() => mock.VerifySet(foo => foo.Name = "foo"));

            // Verify setter with an argument matcher
            mock.Object.Value = 1;
            mock.VerifySet(foo => foo.Value = It.IsInRange(1, 5, Range.Inclusive));

            // Verify event accessors (requires Moq 4.13 or later):
            mock.Object.FooEvent += null;
            mock.VerifyAdd(foo => foo.FooEvent += It.IsAny<EventHandler>());
            mock.Object.FooEvent -= null;
            mock.VerifyRemove(foo => foo.FooEvent -= It.IsAny<EventHandler>());

            // Verify that no other invocations were made other than those already verified (requires Moq 4.8 or later)
            mock.VerifyNoOtherCalls();
        }
    }
}