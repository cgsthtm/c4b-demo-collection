﻿// <copyright file="MethodTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;
    using global::Moq;
    using Xunit;

    /// <summary>
    /// MethodTest.
    /// </summary>
    internal class MethodTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // 您可以使用Setup设置任何mock的可重写方法的行为，结合例如Returns（因此它们返回值）或Throws（因此它们抛出异常）：
            var mock = new Mock<IFoo>();
            mock.Setup(foo => foo.DoSomething("ping")).Returns(true);
            Assert.True(mock.Object.DoSomething("ping"));

            // out arguments
            var outString = "ack";

            // TryParse will return true, and the out argument will return "ack", lazy evaluated
            mock.Setup(foo => foo.TryParse("ping", out outString)).Returns(true);
            Assert.True(mock.Object.TryParse("ping", out outString));
            Assert.Equal("ack", outString);

            // ref arguments
            var instance = new Bar();

            // Only matches if the ref argument to the invocation is the same instance
            // 只有当对调用的ref参数是相同的实例时才会匹配
            mock.Setup(foo => foo.Submit(ref instance)).Returns(true);
            Assert.True(mock.Object.Submit(ref instance));

            // access invocation arguments when returning a value
            mock.Setup(x => x.DoSomethingStringy(It.IsAny<string>()))
                    .Returns((string s) => s.ToLower());

            // Multiple parameters overloads available
            Assert.Equal("abc", mock.Object.DoSomethingStringy("ABC"));

            // throwing when invoked with specific parameters
            mock.Setup(foo => foo.DoSomething("reset")).Throws<InvalidOperationException>();
            mock.Setup(foo => foo.DoSomething(string.Empty)).Throws(new ArgumentException("command"));
            Assert.Throws<InvalidOperationException>(() => mock.Object.DoSomething("reset"));
            Assert.Throws<ArgumentException>(() => mock.Object.DoSomething(string.Empty));

            // lazy evaluating return value
            var count = 1;
            mock.Setup(foo => foo.GetCount()).Returns(() => count);
            Assert.Equal((int)1, mock.Object.GetCount());

            // async methods (see below for more about async):
            // Starting with Moq 4.16, you can simply mock.Setup the returned task's .Result property. This works in nearly all setup and verification expressions:
            mock.Setup(foo => foo.DoSomethingAsync().Result).Returns(true);
            Assert.True(mock.Object.DoSomethingAsync().Result);
        }
    }
}