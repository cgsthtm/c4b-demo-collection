﻿// <copyright file="MyEmptyDefaultValueProvider.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;
    using System.Collections.Generic;
    using global::Moq;

    /// <summary>
    /// MyEmptyDefaultValueProvider.
    /// </summary>
    internal class MyEmptyDefaultValueProvider : LookupOrFallbackDefaultValueProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MyEmptyDefaultValueProvider"/> class.
        /// </summary>
        public MyEmptyDefaultValueProvider()
        {
            this.Register(typeof(string), (type, mock) => "?");
            this.Register(typeof(List<>), (type, mock) => Activator.CreateInstance(type));
        }
    }
}