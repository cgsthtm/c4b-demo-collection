﻿// <copyright file="EventsTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;
    using global::Moq;

    /// <summary>
    /// EventsTest.
    /// </summary>
    internal class EventsTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            string fooValue = "fooValue";
            var mock = new Mock<IFoo>();

            // Setting up an event's `add` and `remove` accessors (requires Moq 4.13 or later):
            mock.SetupAdd(m => m.FooEvent += It.IsAny<EventHandler>());
            mock.SetupRemove(m => m.FooEvent -= It.IsAny<EventHandler>());

            // Raising an event on the mock???
            mock.Raise(m => m.FooEvent += null, new FooEventArgs(fooValue));
            mock.Raise(
                eventExpression: m => m.FooEvent += (o, e) =>
                    {
                        Console.WriteLine("FooEvent1");
                        Console.WriteLine(((FooEventArgs)e).Name);
                    },
                args: new FooEventArgs(fooValue));
            mock.Raise(m => m.FooEvent += null, new FooEventArgs(fooValue));
            mock.Object.FooEvent += (o, e) =>
            {
                Console.WriteLine("FooEvent2");
                Console.WriteLine(((FooEventArgs)e).Name);
            };
            mock.Raise(m => m.FooEvent += null, new FooEventArgs(fooValue));
            mock.SetupAdd(m => m.FooEvent += (o, e) =>
            {
                Console.WriteLine("FooEvent3");
                Console.WriteLine(((FooEventArgs)e).Name);
            });
            mock.Raise(m => m.FooEvent += null, new FooEventArgs(fooValue));
            mock.Raise(m => m.FooEvent += null, new FooEventArgs(fooValue));

            // Raising an event on the mock that has sender in handler parameters???
            ////mock.Raise(m => m.FooEvent += null, this, new FooEventArgs(fooValue));

            // Raising an event on a descendant down the hierarchy???
            ////mock.Raise(m => m.Child.First.FooEvent += null, new FooEventArgs(fooValue));

            // Causing an event to raise automatically when Submit is invoked???
            ////mock.Setup(foo => foo.Submit()).Raises(f => f.Sent += null, EventArgs.Empty);

            // The raised event would trigger behavior on the object under test, which
            // you would make assertions about later (how its state changed as a consequence, typically)

            // Raising a custom event which does not adhere to the EventHandler pattern
            // Raise passing the custom arguments expected by the event delegate

            ////mock.SetupAdd(m => m.MyEvent += (i, b) =>
            ////{
            ////    if (b)
            ////    {
            ////        Console.WriteLine(i);
            ////    }
            ////    else
            ////    {
            ////        Console.WriteLine(i + 1);
            ////    }
            ////});
            mock.Object.MyEvent += (i, b) =>
            {
                if (b)
                {
                    Console.WriteLine(i);
                }
                else
                {
                    Console.WriteLine(i + 1);
                }
            };
            mock.Raise(foo => foo.MyEvent += null, 25, true);
        }
    }
}