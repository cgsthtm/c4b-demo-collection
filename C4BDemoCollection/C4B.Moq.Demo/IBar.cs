﻿// <copyright file="IBar.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System.Threading.Tasks;

    /// <summary>
    /// IBar.
    /// </summary>
    public interface IBar
    {
        /// <summary>
        /// BarMethod.
        /// </summary>
        /// <param name="s">s.</param>
        /// <returns>task.</returns>
        Task<string> BarMethod(string s);
    }
}