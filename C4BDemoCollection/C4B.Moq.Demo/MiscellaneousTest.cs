﻿// <copyright file="MiscellaneousTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;
    using global::Moq;
    using global::Moq.Protected;
    using Xunit;

    /// <summary>
    /// MiscellaneousTest.
    /// </summary>
    public class MiscellaneousTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            var mock = new Mock<IFoo>();

            // Setting up a member to return different values / throw exceptions on sequential calls:
            mock.SetupSequence(f => f.GetCount())
                .Returns(3) // will be returned on 1st invocation
                .Returns(2) // will be returned on 2nd invocation
                .Returns(1) // will be returned on 3rd invocation
                .Returns(0) // will be returned on 4th invocation
                .Throws(new InvalidOperationException()); // will be thrown on 5th invocation
            Assert.Equal(mock.Object.GetCount(), 3);
            Assert.Equal(mock.Object.GetCount(), 2);

            // Setting up a member to verify that a set of calls happened in sequence:
            var fooService = new Mock<IFoo>(MockBehavior.Strict);
            var barService = new Mock<IBar>(MockBehavior.Strict);
            var bazService = new Mock<IBaz>(MockBehavior.Strict);
            var sequence = new MockSequence();
            fooService.InSequence(sequence).Setup(x => x.FooMethod("a")).ReturnsAsync("b");
            barService.InSequence(sequence).Setup(x => x.BarMethod("c")).ReturnsAsync("d");
            bazService.InSequence(sequence).Setup(x => x.BazMethod("e")).ReturnsAsync("f");

            // In your test, if the system under test calls fooService then barService
            // then bazService's methods, the test will pass.
            // Otherwise if it calls them in a different order than they were set up,
            // you will get a test failure specifying that it was missing a corresponding setup
            Assert.Equal("b", fooService.Object.FooMethod("a").Result);
            Assert.Equal("d", barService.Object.BarMethod("c").Result);
            Assert.Equal("f", bazService.Object.BazMethod("e").Result);

            // Setting expectations for protected members (you can't get IntelliSense for these, so you access them using the member name as a string).
            // Assuming the following class with a protected function should be mocked:
            // at the top of the test fixture
            // using Moq.Protected;
            // In the test, mocking the `int Execute()` method (1)
            var mock2 = new Mock<CommandBase>();
            mock2.Protected()
                 .Setup<int>("Execute")
                 .Returns(5);

            // If you need argument matching, you MUST use ItExpr rather than It
            // planning on improving this for vNext (see below for an alternative in Moq 4.8)
            // Mocking the `bool Execute(string arg)` method (2)
            mock2.Protected()
                .Setup<bool>("Execute", ItExpr.IsAny<string>())
                .Returns(true);

            // Moq 4.8 and later allows you to set up protected members through a completely unrelated type
            // that has the same members and thus provides the type information necessary for IntelliSense to work.
            // Pickin up the example from the bullet point above, you can also use this interface to set up
            // protected generic methods and those having by-ref parameters:
            // Completely unrelated Interface (CommandBase is not derived from it) only created for the test.
            // It contains a method with an identical method signature to the protected method in the actual class which should be mocked
            mock2.Protected().As<ICommandBaseProtectedMembers>()
                .Setup(m => m.Execute(It.IsAny<string>())) // will set up CommandBase.Execute
                .Returns(true);
            var cmdBase = mock2.Object;
            cmdBase.PrintExecuteResult("ggs");
        }
    }
}