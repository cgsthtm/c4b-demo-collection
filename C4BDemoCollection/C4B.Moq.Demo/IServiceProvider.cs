﻿// <copyright file="IServiceProvider.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    using System;

    /// <summary>
    /// IServiceProvider.
    /// </summary>
    public interface IServiceProvider
    {
        /// <summary>
        /// GetService.
        /// </summary>
        /// <param name="type">type.</param>
        /// <returns>obj.</returns>
        object GetService(Type type);
    }
}