﻿// <copyright file="ICommandBaseProtectedMembers.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Moq.Demo
{
    /// <summary>
    /// ICommandBaseProtectedMembers.
    /// </summary>
    internal interface ICommandBaseProtectedMembers
    {
        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="arg">arg.</param>
        /// <returns>b.</returns>
        bool Execute(string arg);
    }
}