﻿// <copyright file="Test.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo
{
    using global::Serilog;

    /// <summary>
    /// Test.
    /// </summary>
    public class Test
    {
        /// <summary>
        /// Test1.
        /// </summary>
        public static void Test1()
        {
            Log.Information("The global logger has been configured");
        }
    }
}