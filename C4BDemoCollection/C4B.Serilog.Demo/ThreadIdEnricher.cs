﻿// <copyright file="ThreadIdEnricher.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo
{
    using System.Threading;
    using global::Serilog.Core;
    using global::Serilog.Events;

    // Enrichers are simple components that add, remove or modify the properties attached to a log event.
    // This can be used for the purpose of attaching a thread id to each event, for example.

    /// <summary>
    /// ThreadIdEnricher.
    /// </summary>
    public class ThreadIdEnricher : ILogEventEnricher
    {
        /// <inheritdoc/>
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty(
                    "ThreadId", Thread.CurrentThread.ManagedThreadId));
        }
    }
}