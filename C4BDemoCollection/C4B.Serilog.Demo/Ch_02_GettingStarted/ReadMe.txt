﻿https://github.com/serilog/serilog/wiki/Getting-Started
根Logger是使用LoggerConfiguration创建的。
using var log = new LoggerConfiguration()
    .WriteTo.Console()
    .CreateLogger();

这通常在应用程序启动时完成一次，并保存日志记录器以供应用程序类稍后使用。如果需要，可以创建多个记录器并独立使用。

Serilog的全局静态可访问日志记录器通过Log.Logger设置，并且可以使用Log类上的静态方法调用。
Log.Logger = log;
Log.Information("The global logger has been configured");