﻿// <copyright file="SetupTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.Ch_02_GettingStarted
{
    using System;
    using global::Serilog;

    /// <summary>
    /// SetupTest.
    /// </summary>
    public class SetupTest
    {
        // https://github.com/serilog/serilog/wiki/Getting-Started

        /// <summary>
        /// Setup.
        /// </summary>
        public static void Setup()
        {
            Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .WriteTo.Console()
            .WriteTo.File("logs/myapp.txt", rollingInterval: RollingInterval.Day)
            .CreateLogger();
        }

        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            Log.Information("Hello, world!");

            int a = 10, b = 0;
            try
            {
                Log.Debug("Dividing {A} by {B}", a, b);
                Console.WriteLine(a / b);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Something went wrong");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}
