﻿// <copyright file="IExampleIoCLog.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.AutofacSerilogIntegration
{
    /// <summary>
    /// 演示接口.
    /// </summary>
    public interface IExampleIoCLog
    {
        /// <summary>
        /// Show.
        /// </summary>
        void Show();
    }
}
