﻿// <copyright file="SomeClassAcceptsLogViaProperty.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.AutofacSerilogIntegration
{
    using global::Serilog;

    /// <summary>
    /// 通过属性注入演示IoC的类.
    /// </summary>
    public class SomeClassAcceptsLogViaProperty : IExampleIoCLog
    {
        /// <summary>
        /// Gets or sets ILogger.
        /// </summary>
        public ILogger Log { get; set; }

        /// <inheritdoc/>
        public void Show()
        {
            this.Log.Information("Hello also!");
        }
    }
}
