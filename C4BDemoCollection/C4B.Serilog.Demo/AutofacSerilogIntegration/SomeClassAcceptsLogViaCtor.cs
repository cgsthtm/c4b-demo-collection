﻿// <copyright file="SomeClass.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.AutofacSerilogIntegration
{
    using global::Serilog;

    /// <summary>
    /// 通过构造函数注入演示IoC的类.
    /// </summary>
    public class SomeClassAcceptsLogViaCtor : IExampleIoCLog
    {
        private readonly ILogger log;

        // AutofacSerilogIntegration这个库将Autofac配置为自动为每个注入ILogger的类配置正确的上下文记录器。
        // 使用IoC的应用程序通常接受依赖项作为构造函数参数：

        /// <summary>
        /// Initializes a new instance of the <see cref="SomeClassAcceptsLogViaCtor"/> class.
        /// </summary>
        /// <param name="log">要注入的ILogger.</param>
        public SomeClassAcceptsLogViaCtor(ILogger log)
        {
            this.log = log;
        }

        /// <inheritdoc/>
        public void Show()
        {
            this.log.Information("Hello!");
        }
    }
}
