﻿// <copyright file="AutofacSerilogIntegrationTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.AutofacSerilogIntegration
{
    using System.Collections.Generic;
    using Autofac;
    using global::AutofacSerilogIntegration;
    using global::Serilog;

    /// <summary>
    /// 测试AutofacSerilogIntegration.
    /// </summary>
    public class AutofacSerilogIntegrationTest
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // create the root logger:
            Log.Logger = new LoggerConfiguration()
                .Enrich.WithProperty("SourceContext", null)
                .WriteTo.Console()
                .CreateLogger();

            // when configuring the Autofac container, call RegisterLogger():
            var builder = new ContainerBuilder();
            builder.RegisterLogger(autowireProperties: true);
            builder.RegisterType<SomeClassAcceptsLogViaCtor>().As<IExampleIoCLog>();
            builder.RegisterType<SomeClassAcceptsLogViaProperty>().As<IExampleIoCLog>();

            // 当使用Serilog时，上下文记录器将记录类型的名称附加到日志事件，以便稍后可以找到和过滤它们：
            var log = Log.ForContext<SomeClassAcceptsLogViaCtor>();
            log.Information("[{SourceContext}] This event is tagged with 'SomeClass'");

            // 构建容器并解析注册的类
            using (var container = builder.Build())
            {
                var examples = container.Resolve<IEnumerable<IExampleIoCLog>>(); // 解析
                foreach (var example in examples)
                {
                    example.Show();
                }
            }
        }
    }
}
