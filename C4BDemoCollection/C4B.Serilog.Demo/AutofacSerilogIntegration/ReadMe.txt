﻿当使用Serilog时，上下文记录器将记录类型的名称附加到日志事件，以便稍后可以找到和过滤它们：
var log = Log.ForContext<SomeClass>();
log.Information("This event is tagged with 'SomeClass'");

使用IoC的应用程序通常接受依赖项作为构造函数参数：
public class SomeClass
{
  readonly ILogger _log;
  
  public SomeClass(ILogger log)
  {
    _log = log;
  }
  
  public void Show()
  {
    _log.Information("This is also an event from 'SomeClass'");
  }
}

AutofacSerilogIntegration这个库将Autofac配置为自动为每个注入ILogger的类配置正确的上下文记录器。

# 用法
1. First install from NuGet:
Install-Package AutofacSerilogIntegration

2. Next, create the root logger:
Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .CreateLogger();

3. Then when configuring the Autofac container, call RegisterLogger():
var builder = new ContainerBuilder();
builder.RegisterLogger();
如果没有记录器显式传递给此函数，则将使用默认的Log.Logger。