﻿// <copyright file="Form1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Globalization;
    using System.Web;
    using System.Windows.Forms;
    using global::Serilog;
    using global::Serilog.Context;
    using global::Serilog.Core;
    using global::Serilog.Events;
    using global::Serilog.Filters;
    using global::Serilog.Formatting.Compact;

    /// <summary>
    /// 测试Serilog窗体.
    /// </summary>
    public partial class FrmTestSerilog : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrmTestSerilog"/> class.
        /// </summary>
        public FrmTestSerilog()
        {
            this.InitializeComponent();
        }

        // 1 Getting started
        private void Button1_Click(object sender, EventArgs e)
        {
            #region Getting started

            //// The root Logger is created using LoggerConfiguration.
            var log = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console() // sent to the console
                .WriteTo.File("logs/myapp.txt", rollingInterval: RollingInterval.Day) // sent to the log file
                .CreateLogger();

            //// This is typically done once at application start-up, and the logger saved for later use by application classes.
            //// Multiple loggers can be created and used independently if required.
            log.Information("Hello, Serilog!");

            //// Serilog's global, statically accessible logger, is set via Log.Logger and can be invoked using the static methods on the Log class.
            Log.Logger = log;
            Test.Test1();

            // The complete example below shows logging in a simple console application, with events sent to the console as well as a date-stamped rolling log file.
            int a = 10, b = 0;
            try
            {
                Log.Debug("Dividing {A} by {B}", a, b);
                Console.WriteLine(a / b);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Something went wrong");
            }
            finally
            {
                Log.CloseAndFlush();
            }

            #endregion Getting started
        }

        // 2 Configuration Basics https://github.com/serilog/serilog/wiki/Configuration-Basics
        private void button2_Click(object sender, EventArgs e)
        {
            /* Serilog uses a simple C# API to configure logging.
             * When external configuration is desirable it can be mixed in (sparingly) using the Serilog.Settings.AppSettings package
             *  or Serilog.Settings.Configuration package.
             */
        }

        // 2.1 Creating a logger
        private void button3_Click(object sender, EventArgs e)
        {
            // Loggers are created using a LoggerConfiguration object:
            Log.Logger = new LoggerConfiguration().CreateLogger();
            Log.Information("No one listens to me!");
            // Finally, once just before the application exits...
            Log.CloseAndFlush();
            // The example above will create a logger that does not record events anywhere. To see log events, a sink must be configured.
        }

        // 2.2 Sinks
        private void button4_Click(object sender, EventArgs e)
        {
            /* Log event sinks generally record log events to some external representation, typically the console, a file or data store.
             * Serilog sinks are distributed via NuGet. A curated list of available sinks(https://github.com/serilog/serilog/wiki/Provided-Sinks)
             *  is listed here on the wiki.
             * This example will use the console sink(https://github.com/serilog/serilog-sinks-console) package, which pretty-prints log data,
             *  and the file sink(https://github.com/serilog/serilog-sinks-file) package, which writes log events to a set of date-stamped text files.
             */
            // Sinks are configured using the WriteTo configuration object.
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger();
            Log.Information("Ah, there you are!");

            // Multiple sinks can be active at the same time. Adding additional sinks is a simple as chaining WriteTo blocks:
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.File("log-.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();
            Log.Information("Ah, there you are!!!!!!!!!");
        }

        // 2.3 Output templates
        private void button5_Click(object sender, EventArgs e)
        {
            // Text-based sinks use output templates to control formatting. this can be modified through the outputTemplate parameter:
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.File("log.txt",
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}",
                    rollingInterval: RollingInterval.Hour)
                .CreateLogger();
            Log.Information("Ah, there you are!");
            /* The default template, shown in the example above, uses built-in properties like Timestamp and Level.
             * Properties from events, including those attached using enrichers(https://github.com/serilog/serilog/wiki/Enrichment),
             *  can also appear in the output template.
             * The {Message:lj} format options cause data embedded in the message to be output in JSON (j) except for string literals, which are output as-is.
             * For more compact level names, use a format such as {Level:u3} or {Level:w3} for three-character upper- or lowercase level names, respectively.
             * Add {Properties:j} to the output template to include additional context information.
             */
        }

        // 2.4 Minimum level
        private void button6_Click(object sender, EventArgs e)
        {
            // Serilog implements the common concept of a 'minimum level' for log event processing.
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .CreateLogger();
            Log.Debug("MinimumLevel.Debug");
            /* The MinimumLevel configuration object provides for one of the log event levels to be specified as the minimum.
             * In the example above, log events with level Debug and higher will be processed and ultimately written to the console.
             * | Level         | Usage                                                        |
| ------------- | ------------------------------------------------------------ |
| `Verbose`     | Verbose is the noisiest level, rarely (if ever) enabled for a production app. |
| `Debug`       | Debug is used for internal system events that are not necessarily observable from the outside, but useful when determining how something happened. |
| `Information` | Information events describe things happening in the system that correspond to its responsibilities and functions. Generally these are the observable actions the system can perform. |
| `Warning`     | When service is degraded, endangered, or may be behaving outside of its expected parameters, Warning level events are used. |
| `Error`       | When functionality is unavailable or expectations broken, an Error event is used. |
| `Fatal`       | The most critical level, Fatal events demand immediate attention. |
             * Default Level - if no MinimumLevel is specified, then Information level events and higher will be processed.
             */
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.Console()
                .CreateLogger();
            Log.Debug("MinimumLevel.Information"); // not print
        }

        // 2.5 Overriding per sink
        private void button7_Click(object sender, EventArgs e)
        {
            // Sometimes it is desirable to write detailed logs to one medium, but less detailed logs to another.
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("log.txt")
                .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information)
                .CreateLogger();
            Log.Debug("restrictedToMinimumLevel: LogEventLevel.Information");
            /* In this example debug logs will be written to the rolling file, while only Information level logs and higher will be written to the console.
             * All provided sinks support the restrictedToMinimumLevel configuration parameter.
             * Logger vs. sink minimums - it is important to realize that the logging level can only be raised for sinks, not lowered.
             *  So, if the logger's MinimumLevel is set to Information then a sink with Debug as its specified level will still only see Information level events.
             *  This is because the logger-level configuration controls which logging statements will result in the creation of events,
             *  while the sink-level configuration only filters these. To create a single logger with a more verbose level, use a separate LoggerConfiguration.
             */
        }

        // 2.6 Enrichers
        private void button8_Click(object sender, EventArgs e)
        {
            // Enrichers are simple components that add, remove or modify the properties attached to a log event.
            //  This can be used for the purpose of attaching a thread id to each event, for example.
            // Enrichers are added using the Enrich configuration object.
            Log.Logger = new LoggerConfiguration()
                .Enrich.With(new ThreadIdEnricher()) // ThreadIdEnricher : ILogEventEnricher
                .WriteTo.Console(
                    outputTemplate: "{Timestamp:HH:mm} [{Level}] ({ThreadId}) {Message}{NewLine}{Exception}")
                .CreateLogger();
            Log.Information("Enrich.With");
            //Task.Run(() =>
            //{
            //    Log.Information("Task.Run");
            //});
            // The configuration above shows how a property added by an enricher can be used in output formatting.
            // If the enriched property value is constant throughout the application run, the shortcut WithProperty method can be used to simplify configuration.
            Log.Logger = new LoggerConfiguration()
                .Enrich.WithProperty("Version1", "V1.0.0")
                .WriteTo.Console(
                    outputTemplate: "{Timestamp:HH:mm} [{Level}] ({Version1}) {Message}{NewLine}{Exception}")
                .CreateLogger();
            Log.Information("Enrich.WithProperty");
            // Enrichers and the properties they attach are generally more useful with sinks that use structured storage,
            //  where the property values can be viewed and filtered.
        }

        // 2.7 Filters
        private void button9_Click(object sender, EventArgs e)
        {
            // Events can be selectively logged by filtering. Filters are just predicates over LogEvent, with some common scenarios handled by the Matching class.
            Log.Logger = new LoggerConfiguration()
                .Enrich.With(new Count9Enricher())
                .WriteTo.Console()
                .Filter.ByExcluding(Matching.WithProperty<int>("Count", p => p < 10))
                .CreateLogger();
            Log.Information("Count9Enricher");
            Log.Logger = new LoggerConfiguration()
                .Enrich.With(new Count11Enricher())
                .WriteTo.Console()
                .Filter.ByExcluding(Matching.WithProperty<int>("Count", p => p < 10))
                .CreateLogger();
            Log.Information("Count11Enricher");
        }

        // 2.8 Sub-loggers???
        private void button10_Click(object sender, EventArgs e)
        {
            // Sometimes a finer level of control over what is seen by a sink is necessary. For this, Serilog allows a full logging pipeline to act as a sink.
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.Logger(lc => lc
                    .Filter.ByIncludingOnly(Matching.WithProperty<int>("Count", p => p < 10))
                    .WriteTo.File("log.txt"))
                .CreateLogger();
            Log.Information("Filter.ByIncludingOnly");
            /* For scenarios not handled well by sub-loggers, it's fine to create multiple independent top-level pipelines.
             *  Only one pipeline can be assigned to Log.Logger, but your app can use as many additional ILogger instances as it requires.
             * Note that destructuring policies will not have an effect if they are specified inside the WriteTo.Logger() callback
             *  since sub-loggers work with already created LogEvents.
             */
        }

        // 3 Structured Data
        private void button11_Click(object sender, EventArgs e)
        {
            /* Serilog is a kind of serialiser. In many cases it has good default behaviour that suits its purpose,
             *  but on occasion it is necessary to instruct Serilog on how to store properties that are attached to a log event.
             * Serilog 是一种序列化器。在许多情况下，它具有适合其用途的良好默认行为，但有时需要指示 Serilog 如何存储附加到日志事件的属性。
             * There are potentially many ways to record an object to the log. Most types can be nicely represented as strings or simple values,
             *  but some make more sense to record as collections, and others as structures with named properties.
             * 可能有很多方法可以将对象记录到日志中。大多数类型可以很好地表示为字符串或简单值，但有些类型更适合记录为集合，而另一些类型则记录为具有命名属性的结构。
             * The storage representation for a log event property makes a big difference to its size in the log,
             *  and the memory and processing overhead involved in getting it there.
             * 日志事件属性的存储表示形式对其在日志中的大小以及获取日志所涉及的内存和处理开销有很大影响。
             * With this in mind, let’s take a look at how Serilog is configured to work in the simple cases.
             * 考虑到这一点，让我们看一下 Serilog 是如何配置为在简单情况下工作的。
             */
        }

        // 3.1 Default Behaviour
        private void button12_Click(object sender, EventArgs e)
        {
            // 3.1.1 Simple, Scalar Values
            // When properties are specified in log events, Serilog does its best to determine the correct representation.
            Log.Logger = new LoggerConfiguration()
               .WriteTo.Console()
               .CreateLogger();
            var count = 456;
            Log.Information("Retrieved {Cou111nt} records", count);
            // There's little ambiguity as to how the Cou111nt property should be stored in this case. Being a simple integer value,
            //  Serilog will choose that as its representation. { "Cou111nt": 456 }
            /* These examples use JSON, but the same principles apply to other formats as well.
             * Out of the box, Serilog recognises the following list as basic scalar types, regardless of any other policies applied:
                Booleans - bool
                Numerics - byte, short, ushort, int, uint, long, ulong, float, double, decimal
                Strings - string, byte[]
                Temporals - DateTime, DateTimeOffset, TimeSpan
                Others - Guid, Uri
                Nullables - nullable versions of any of the types above
             */

            // 3.1.2 Collections
            // If the object passed as a property is IEnumerable, Serilog will treat that property as a collection.
            var fruit = new[] { "Apple", "Pear", "Orange" };
            Log.Information("In my bowl I have {Fruit}", fruit);
            // The equivalent JSON includes an array. { "Fruit": ["Apple", "Pear", "Orange"] }
            // Serilog makes this choice because most enumerable types are of interest for their elements, and represent poorly as structures or strings.
            // Serilog also recognises Dictionary<TKey,TValue>, as long as the key type is one of the scalar types listed above.
            var fruit1 = new Dictionary<string, int> { { "Apple", 1 }, { "Pear", 5 } };
            Log.Information("In my bowl I have {Fruit}", fruit1);
            // Formatters that support dictionaries can record the property as such. { "Fruit": { "Apple": 1, "Pear": 5 }}
            /* IDictionary<TKey,TValue> - objects implementing dictionary interfaces are not serialised as dictionaries.
             *  Firstly because it is less efficient in .NET to check for generic interface compatibility,
             *  and second because a single object may implement more than one generic dictionary interface, creating an ambiguity.
             * IDictionary<TKey,TValue> - 实现字典接口的对象不会序列化为字典。首先是因为在 .NET 中检查泛型接口兼容性的效率较低，
             *  其次是因为单个对象可能实现多个泛型字典接口，从而产生歧义。
             */

            // 3.1.3 Objects
            // Apart from the types above, which are specially handled by Serilog, it is difficult to make intelligent choices about how data should be
            //  rendered and persisted. Objects not explicitly intended for serialisation tend to serialise very poorly.
            // 除了上述类型（由 Serilog 专门处理）之外，很难对数据的呈现和持久化方式做出明智的选择。未明确用于序列化的对象往往序列化效果很差。
            SqlConnection conn = new SqlConnection("");
            Log.Information("Connected to {Connection}", conn);
            // (Yikes! How does one serialise an SqlConnection?)
            // When Serilog doesn't recognise the type, and no operator is specified (see below) then the object will be rendered using ToString().
        }

        // 3.2 Preserving Object Structure 保留对象结构
        private void button13_Click(object sender, EventArgs e)
        {
            // There are many places where, given the capability, it makes sense to serialise a log event property as a structured object.
            //  DTOs (data transfer objects), messages, events and models are often best logged by breaking them down into properties with values.
            // 在许多地方，只要有这种功能，将日志事件属性序列化为结构化对象是有意义的。DTO（数据传输对象）、消息、事件和模型通常最好通过将它们分解为具有值的属性来记录。
            // For this task, Serilog provides the @ destructuring operator. 对于此任务，Serilog 提供了 @ 解构运算符。
            Log.Logger = new LoggerConfiguration()
               .WriteTo.Console()
               .CreateLogger();
            var sensorInput = new { Latitude = 25, Longitude = 134 };
            Log.Information("Processing {@SensorInput}", sensorInput);
            /* ('Destructuring' is a term borrowed from various programming languages; it is a style of pattern matching used to pull values out from
             *  structured data. The usage is Serilog is only notionally related at the moment, but possible future extensions to this operator
             *  could match the broader definition more accurately.)
             * （“解构”是从各种编程语言中借来的术语;它是一种模式匹配风格，用于从结构化数据中提取值。用法是 Serilog 目前仅在概念上相关，
             *  但未来可能扩展此运算符可以更准确地匹配更广泛的定义。
             */

            // 3.2.1 Customizing the stored data
            // Often only a selection of properties on a complex object are of interest. To customise how Serilog persists a destructured complex type,
            //  use the Destructure configuration object on LoggerConfiguration:
            Log.Logger = new LoggerConfiguration()
                .Destructure.ByTransforming<HttpRequest>(
                    r => new { RawUrl = r.RawUrl, Method = r.HttpMethod })
                .WriteTo.Console()
                .CreateLogger();
            HttpRequest httpRequest = new HttpRequest(filename: "111", url: "http://www.bing.com", queryString: "222");
            Log.Information("{@req}", httpRequest);
            /* This example transforms objects of type HttpRequest to preserve only the RawUrl and Method properties. A number of different strategies for
             *  destructuring are available, and custom ones can be created by implementing IDestructuringPolicy.
             * 此示例转换类型的 HttpRequest 对象，以仅保留 RawUrl 和 Method 属性。有许多不同的解构策略可用，可以通过实现 IDestructuringPolicy 来创建自定义策略。
             * Note: the function provided to Destructure.ByTransforming() must return a different type from the one passed in, or it will be called recursively.
             *  Use a custom IDestructuringPolicy instead to implement conditional transformations.
             * 注意：提供给的函数 Destructure.ByTransforming() 必须返回与传入的函数不同的类型，否则它将被递归调用。请改用自定义 IDestructuringPolicy 来实现条件转换。
             */

            // 3.2.2 Operators vs. Formats
            /* While both operators and formats affect the representation of a property, it is important to realise their distinct roles.
             *  Operators are applied at the point the property is captured, to preserve or structure the data in some way.
             *  Formats are used only when displaying properties as text, and don't impact the serialised representation at all.
             * 虽然运算符和格式都会影响属性的表示形式，但重要的是要意识到它们的不同作用。运算符在捕获属性时应用，以以某种方式保留或构建数据。
             *  仅当将属性显示为文本时才使用格式，并且完全不会影响序列化表示形式。
             */

            // 3.2.3 Formatting Collections and Structures
            // When format strings are specified for complex properties, they are generally ignored. Only enumerables take format string strings into account,
            //  passing them to their elements when rendering for display.
            // 当为复杂属性指定格式字符串时，通常会忽略它们。只有枚举对象才会考虑格式字符串，并在呈现以供显示时将它们传递给其元素。
        }

        // 3.3 Forcing Stringification 强制字符串化
        private void button14_Click(object sender, EventArgs e)
        {
            /* Sometimes, the type of an object being logged may not be exactly known, or may vary in a way that is undesirable to preserve in the log events.
             *  In these cases the $ stringification operator will convert the property value to a string before any other processing takes place,
             *  regardless of its type or implemented interfaces.
             * 有时，所记录对象的类型可能不完全已知，或者可能以不希望在日志事件中保留的方式发生变化。在这些情况下，
             *  字符串化运算符将在进行任何其他处理之前将属性值转换为字符串， $ 无论其类型或实现的接口如何。
             */
            Log.Logger = new LoggerConfiguration()
               .WriteTo.Console()
               .CreateLogger();
            var unknown = new[] { 1, 2, 3 };
            Log.Information("Received {$Data}", unknown); // Received "System.Int32[]"
            // Despite being an enumerable type, the unknown variable is captured and rendered as a string. Received "System.Int32[]"
            Log.Information("Received {Data}", unknown); // Received [1, 2, 3]
            Log.Information("Received {@Data}", unknown); // Received [1, 2, 3]
        }

        // 4 Writing Log Events
        private void button15_Click(object sender, EventArgs e)
        {
            // Log events are written to sinks using the Log static class, or the methods on an ILogger. These examples will use Log for syntactic brevity,
            //  but the same methods shown below are available also on the interface.
            // 日志事件使用 Log 静态类或 ILogger .这些示例将用于 Log 语法简洁，但下面显示的相同方法也可用于接口。
            Log.Logger = new LoggerConfiguration()
               .WriteTo.Console()
               .CreateLogger();
            int quota = 1024;
            string user = "cgs";
            Log.Warning("Disk quota {Quota} MB exceeded by {User}", quota, user);
            // The warning event created from this log method will have two associated properties, Quota and User. Assuming quota is an integer,
            //  and user a string, the rendered message may look like the one below.
            // Disk quota 1024 MB exceeded by "cgs"
            // (Serilog renders string values in double quotes to more transparently indicate the underlying data type,
            //  and to make the property value stand out from the surrounding message text.)
            // （Serilog 以双引号呈现值，以更透明地指示基础数据类型，并使属性 string 值从周围的消息文本中脱颖而出。

            // 4.0.1 Message Template Syntax
            /* The string above "Disk quota {Quota} exceeded by {User}" is a Serilog message template. Message templates are a superset of standard .NET format
             *  strings, so any format string acceptable to string.Format() will also be correctly processed by Serilog.
- Property names are written between `{` and `}` brackets
- Property names must be valid C# identifiers, for example `FooBar`, but **not** `Foo.Bar` or `Foo-Bar`
- Brackets can be escaped by doubling them, e.g. `{{` will be rendered as `{`
- Formats that use numeric property names, like `{0}` and `{1}` exclusively, will be matched with the log method's parameters by treating the property names as indexes; this is identical to `string.Format()`'s behaviour
- If any of the property names are non-numeric, then all property names will be matched from left-to-right with the log method's parameters
- Property names may be prefixed with an optional operator, `@` or `$`, to control how the property is serialised
- Property names may be suffixed with an optional format, e.g. `:000`, to control how the property is rendered; these format strings behave exactly as their counterparts within the `string.Format()` syntax
             */

            // 4.0.2 Message Template Recommendations 消息模板建议
            /* Fluent Style Guideline - good Serilog events use the names of properties as content within the message as in the User example above.
             *  This improves readability and makes events more compact.
             * Fluent Style Guideline - 良好的 Serilog 事件使用属性名称作为消息中的内容，如上例所示 User 。这提高了可读性，并使事件更加紧凑。
             * Sentences vs. Fragments - log event messages are fragments, not sentences; for consistency with other libraries that use Serilog,
             *  avoid a trailing period/full stop when possible.
             * 句子与片段 - 日志事件消息是片段，而不是句子;为了与使用 Serilog 的其他库保持一致，请尽可能避免尾随句点/句号。
             * Templates vs. Messages - Serilog events have a message template associated, not a message. Internally, Serilog parses and caches every
             *  template (up to a fixed size limit). Treating the string parameter to log methods as a message, as in the case below,
             *  will degrade performance and consume cache memory.
             * 模板与消息 - Serilog 事件具有关联的消息模板，而不是消息。在内部，Serilog 解析并缓存每个模板（最多有固定的大小限制）。
             *  将日志方法的字符串参数视为消息（如下例所示）将降低性能并消耗缓存内存。
             */
            // Don't:
            Log.Information("The time is " + DateTime.Now);
            // Do:
            Log.Information("The time is {Now}", DateTime.Now);
            // Property Naming - Property names should use PascalCase for consistency with other code and libraries from the Serilog ecosystem.
            // 属性命名 - 属性名称应用于 PascalCase 与 Serilog 生态系统中的其他代码和库保持一致。
            // The message template syntax is specified at messagetemplates.org(https://messagetemplates.org/).
        }

        // 4.1 Log Event Levels
        private void button16_Click(object sender, EventArgs e)
        {
            /* Serilog uses levels as the primary means for assigning importance to log events. The levels in increasing order of importance are:
1. **Verbose** - tracing information and debugging minutiae; generally only switched on in unusual situations
2. **Debug** - internal control flow and diagnostic state dumps to facilitate pinpointing of recognised problems
3. **Information** - events of interest or that have relevance to outside observers; the default enabled minimum logging level
4. **Warning** - indicators of possible issues or service/functionality degradation
5. **Error** - indicating a failure within the application or connected system
6. **Fatal** - critical errors causing complete failure of the application
             */

            // 4.1.1 The role of the Information level
            /* The Information level is unlike the other specified levels - it has no specified semantics and in many ways expresses the absence of other levels.
             * 信息级别与其他指定级别不同 - 它没有指定的语义，并且在许多方面表示没有其他级别。
             * Because Serilog allows the event stream from the application to be processed or analysed, the Information level can be thought of as a synonym
             *  for event. That is, most interesting application event data should be logged at this level.
             * 由于 Serilog 允许处理或分析来自应用程序的事件流，因此可以将信息级别视为事件的同义词。也就是说，最有趣的应用程序事件数据应记录在此级别。
             */

            // 4.1.2 Level Detection
            /* In most cases, applications should write events without checking the active logging level. Level checking is extremely cheap and the overhead of
             *  calling disabled logger methods very low.
             * 在大多数情况下，应用程序应在不检查活动日志记录级别的情况下写入事件。级别检查非常便宜，调用禁用的记录器方法的开销非常低。
             * In rare, performance-sensitive cases, the recommended pattern for level detection is to store the results of level detection in a field, for example:
             * 在极少数对性能敏感的情况下，建议的级别检测模式是将级别检测的结果存储在字段中，例如：
             */
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Debug()
               .WriteTo.Console()
               .CreateLogger();
            bool _isDebug = Log.IsEnabled(LogEventLevel.Debug);
            // The _isDebug field can be checked efficiently before writing log events:
            if (_isDebug)
                Log.Debug("Someone is stuck debugging...");

            // 4.1.3 Dynamic levels
            /* Many larger/distributed apps need to run at a fairly restricted level of logging, say, Information (my preference) or Warning, and only turn up
             *  the instrumentation to Debug or Verbose when a problem has been detected and the overhead of collecting a bit more data is justified.
             * 许多大型/分布式应用需要在相当有限的日志记录级别运行，例如“信息”（我的偏好）或“警告”，并且仅在检测到问题并且收集更多数据的开销合理时才将检测设置为“调试”或“详细”。
             * If an app needs dynamic level switching, the first step is to create an instance of LoggingLevelSwitch when the logger is being configured:
             */
            var levelSwitch = new LoggingLevelSwitch();
            // When configuring the logger, provide the switch using MinimumLevel.ControlledBy():
            var log = new LoggerConfiguration()
              .MinimumLevel.ControlledBy(levelSwitch)
              .WriteTo.Console()
              .CreateLogger();
            // Now, events written to the logger will be filtered according to the switch’s MinimumLevel property.
            // To turn the level up or down at runtime, perhaps in response to a command sent over the network, change the property:
            levelSwitch.MinimumLevel = LogEventLevel.Verbose;
            log.Verbose("This will now be logged");
            levelSwitch.MinimumLevel = LogEventLevel.Information;
            log.Debug("This will not be logged");
        }

        // 4.2 Source Contexts
        private void button17_Click(object sender, EventArgs e)
        {
            // Serilog, like most .NET logging frameworks, allows events to be tagged with their source, generally speaking the name of the class writing them:
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Debug()
               .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}")
               .CreateLogger();
            var myLog = Log.ForContext<MyClass>();
            myLog.Information("Hello!");
            // The event written will include a property SourceContext with value "MyNamespace.MyClass" that can later be used to filter out noisy events,
            //  or selectively write them to particular sinks.
            // Not all properties attached to an event need to be represented in the message template or output format; all properties are carried in a dictionary
            //  on the underlying LogEvent object.
            // 并非附加到事件的所有属性都需要以消息模板或输出格式表示;所有属性都包含在基础 LogEvent 对象的字典中。
            // For more on filters and logger topology see Configuration Basics(https://github.com/serilog/serilog/wiki/Configuration-Basics).
        }

        // 4.3 Correlation 相关
        private void button18_Click(object sender, EventArgs e)
        {
            // Just as ForContext<T>() tags log events with the class that wrote them, other overloads of ForContext() enable log events to be tagged with
            //  identifiers that later support correlation of the events written with that identifier.
            // 正如标记使用编写事件的类记录事件一样 ForContext<T>() ，其他重载 ForContext() 允许使用标识符标记日志事件，这些标识符稍后支持与该标识符写入的事件相关联。
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Debug()
               .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] [{Properties}] {Message:lj}{NewLine}{Exception}")
               .CreateLogger();
            var job = GetNextJob();
            var jobLog = Log.ForContext("JobId", job.Id);
            jobLog.Information("Running a new job");
            job.Run();
            jobLog.Information("Finished");
            // Here both of the log events will carry the JobId property including the job identifier.在这里，两个日志事件都将携带 JobId 包括作业标识符在内的属性。
            // Tip: when logging to sinks that use a text format, such as Serilog.Sinks.Console, you can include {Properties} in the output template to print out
            //  all contextual properties not otherwise included.
            // 提示：记录到使用文本格式（如 Serilog.Sinks.Console）的接收器时，可以在输出模板中包括 {Properties} ，以打印出未包含的所有上下文属性。
        }

        public class Job
        {
            public int Id { get; set; }

            public void Run()
            {
                Log.Information("Running...");
            }
        }

        private Job GetNextJob()
        {
            return new Job() { Id = 91 };
        }

        // 5 Available Sinks
        private void button19_Click(object sender, EventArgs e)
        {
            // More sinks can be found by searching within the serilog tag on NuGet. 可以通过在 NuGet 上的 serilog 标记中搜索来找到更多接收器。
            // https://github.com/serilog/serilog/wiki/Provided-Sinks#list-of-available-sinks
        }

        // 6 Enrichment
        private void button20_Click(object sender, EventArgs e)
        {
            // Log events can be enriched with properties in various ways. A number of pre-built enrichers are provided through NuGet:
            //  Install-Package Serilog.Enrichers.Thread
            // Configuration for enrichment is done via the Enrich configuration object:
            var log = new LoggerConfiguration()
                .Enrich.WithThreadId()
                .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] [{ThreadId}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();
            log.Information("Enrich.WithThreadId");
            // All events written through log will carry a property ThreadId with the id of the managed thread that wrote them.
            //  (By convention, any .WithXyz() methods on Enrich create properties named Xyz.)

            // 6.1 The LogContext
            /* Serilog.Context.LogContext can be used to dynamically add and remove properties from the ambient "execution context"; for example, all messages
             *  written during a transaction might carry the id of that transaction, and so-on.
             * Serilog.Context.LogContext 可用于在环境“执行上下文”中动态添加和删除属性;例如，在事务期间写入的所有消息都可能带有该事务的 ID，依此类推。
             * This feature must be added to the logger at configuration-time using .FromLogContext():
             */
            var log1 = new LoggerConfiguration()
               .Enrich.FromLogContext()
               .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
               .CreateLogger();
            log1.Information("No contextual properties");
            using (LogContext.PushProperty("A", 1))
            {
                log1.Information("Carries property A = {A}");
                using (LogContext.PushProperty("A", 2))
                using (LogContext.PushProperty("B", 1))
                {
                    log1.Information("Carries A = {A} and B = {B}");
                }
                log1.Information("Carries property A = {A}, again");
            }
            // Pushing property onto the context will override any existing properties with the same name, until the object returned from PushProperty()
            //  is disposed, as the property A in the example demonstrates.
            // 将属性推送到上下文中将覆盖任何具有相同名称的现有属性，直到释放从返回 PushProperty() 的对象，如示例中的属性 A 所示。
            // Important: properties must be popped from the context in the precise order in which they were added. Behavior otherwise is undefined.
            // 重要提示：属性必须按照添加属性的精确顺序从上下文中弹出。否则，行为是未定义的。

            // 6.2 Available enricher packages
            /*
The Serilog project provides:
- [Serilog.Enrichers.Environment](https://github.com/serilog/serilog-enrichers-environment) - `WithMachineName()` and `WithEnvironmentUserName()`
- [Serilog.Enrichers.Process](https://github.com/serilog/serilog-enrichers-process) - `WithProcessId()`
- [Serilog.Enrichers.Thread](https://github.com/serilog/serilog-enrichers-thread) - `WithThreadId()`

Other interesting enrichers:
- [Serilog.Web.Classic](https://github.com/serilog-web/classic) - `WithHttpRequestId()` and many other enrichers useful in classic ASP.NET applications
- [Serilog.Exceptions](https://github.com/RehanSaeed/Serilog.Exceptions) - `WithExceptionDetails()` adds additional structured properties from exceptions
- [Serilog.Enrichers.Demystify](https://github.com/nblumhardt/serilog-enrichers-demystify) - `WithDemystifiedStackTraces()`
- [Serilog.Enrichers.ClientInfo](https://github.com/mo-esmp/serilog-enrichers-clientinfo) - `WithClientIp()`, `WithCorrelationId()` and `WithRequestHeader("header-name")` will add properties with client IP, correlation id and HTTP request header value
- [Serilog.Enrichers.ExcelDna](https://github.com/augustoproiete/serilog-enrichers-exceldna) - `WithXllPath()` and many other enrichers useful in Excel-DNA add-ins
- [Serilog.Enrichers.Sensitive](https://github.com/serilog-contrib/Serilog.Enrichers.Sensitive) - `WithSensitiveDataMasking()` masks sensitive data in log events
- [Serilog.Enrichers.GlobalLogContext](https://github.com/augustoproiete/serilog-enrichers-globallogcontext) - `FromGlobalLogContext()` adds properties from the "global context" that can be added dynamically
             */
        }

        // 6 Enrichment
        private void button21_Click(object sender, EventArgs e)
        {
            button20_Click(sender, e);
        }

        // 7 Formatting Output
        private void button22_Click(object sender, EventArgs e)
        {
            // Serilog provides several output formatting mechanisms.
        }

        // 7.1 Formatting plain text
        private void button23_Click(object sender, EventArgs e)
        {
            /* Sinks that write plain text output, such as the console and file-based sinks, generally accept output templates to control how log event data
             *  is formatted.
             * The format of events written by these sinks can be modified using the outputTemplate configuration parameter. For example, to control the console sink:
             */
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console(outputTemplate:
                    "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();
            Log.Information("outputTemplate");
            /* A number of built-in properties can appear in output templates:
- `Exception` - The full exception message and stack trace, formatted across multiple lines. Empty if no exception is associated with the event.
- `Level` - The log event level, formatted as the full level name. For more compact level names, use a format such as `{Level:u3}` or `{Level:w3}` for three-character upper- or lowercase level names, respectively.
- `Message` - The log event's message, rendered as plain text. The `:l` format specifier switches off quoting of strings, and `:j` uses JSON-style rendering for any embedded structured data.
- `NewLine` - A property with the value of `System.Environment.NewLine`.
- `Properties` - All event property values that don't appear elsewhere in the output. Use the `:j` format to use JSON rendering.
- `Timestamp` - The event's timestamp, as a `DateTimeOffset`.
             * Properties from events, including those attached using enrichers, can also appear in the output template.
             */
        }

        // 7.2 Formatting JSON???
        private void button24_Click(object sender, EventArgs e)
        {
            // Many sinks record log events as JSON, or can be configured to do so. To emit JSON, rather than plain text, a formatter can be specified.
            //  This example configures the file sink(https://github.com/serilog/serilog-sinks-file) using the formatter
            //  from Serilog.Formatting.Compact(https://github.com/serilog/serilog-formatting-compact).
            // 许多接收器将日志事件记录为 JSON，或者可以配置为这样做。要发出 JSON，而不是纯文本，可以指定 一个 formatter 。
            //  此示例使用 Serilog.Formatting.Compact 中的格式化程序配置文件接收器。
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console(new CompactJsonFormatter())
                .CreateLogger();
            // 怎么用？
            Log.Information("Hello,{User} {Password}", "cgs", "cgs01");
            /* There are three JSON formatters provided by the Serilog project:
- `Serilog.Formatting.Json.JsonFormatter` - This is the historical default shipped in the *Serilog* package. It produces a complete rendering of the log event and supports a few configuration options.
- `Serilog.Formatting.Compact.CompactJsonFormatter` - A newer, more space-efficient JSON formatter shipped in [Serilog.Formatting.Compact](https://github.com/serilog/serilog-formatting-compact).
- `Serilog.Formatting.Compact.RenderedCompactJsonFormatter` - Also shipped in [Serilog.Formatting.Compact](https://github.com/serilog/serilog-formatting-compact), this formatter pre-renders [message templates](https://messagetemplates.org/) into text.
             */
        }

        // 7.3 Flexible formatting with ExpressionTemplate
        private void button25_Click(object sender, EventArgs e)
        {
            /* The [Serilog.Expressions](https://github.com/serilog/serilog-expressions) package includes the `ExpressionTemplate` class for
             *  [more sophisticated text](https://nblumhardt.com/2021/06/customize-serilog-text-output/) and
             *  [JSON formatting](https://nblumhardt.com/2021/06/customize-serilog-json-output/).
             * Expression templates can include conditional blocks, repeated sections, computations over event properties, and custom formatting functions.
             *  `ExpressionTemplate` implements `ITextFormatter`, so it works with any text-based Serilog sink, including `Console` (with ANSI color themes),
             *  `File`, `Debug`, and `Email`.
             */
        }

        // 7.4 Custom formatters
        private void button26_Click(object sender, EventArgs e)
        {
            /* Both plain text and JSON formatting are implemented using the `ITextFormatter` interface. Implementations of this interface can format log events
             *  into any text-based format.
             * Custom JSON formatters can be built around the `JsonValueFormatter` class included in Serilog. For some details see
             *  [this blog post](https://nblumhardt.com/2016/07/serilog-2-0-json-improvements/).
             */
        }

        // 7.5 Format providers
        private void button27_Click(object sender, EventArgs e)
        {
            /* There are a number of options available to formatting the output of individual types like dates. One example is the use of the format provider
             *  that is accepted by most sinks.
             * Below is a simple console sample using the [*Serilog.Sinks.Console*](https://github.com/serilog/serilog-sinks-console) sink.
             *  This is using the default behavior for rendering a date.
             */
            Log.Logger = new LoggerConfiguration()
                    .WriteTo.Console()
                    .CreateLogger();
            var exampleUser = new User { Id = 1, Name = "Adam", Created = DateTime.Now };
            Log.Information("Created {@User} on {Created}", exampleUser, DateTime.Now);
            Log.CloseAndFlush();
            /* There may be scenarios where it is desirable to override or specify the way a `DateTime` is formatted. This can be done via the implementation of
             *  `IFormatProvider`. This strategy applies to any type that you pass to Serilog.
             */
            var formatter = new CustomDateFormatter("dd-MMM-yyyy", new CultureInfo("en-AU"));
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console(formatProvider: new CultureInfo("en-AU")) // Console 1
                .WriteTo.Console(formatProvider: formatter)                // Console 2
                .CreateLogger();
            Log.Information("Created {@User} on {Created}", exampleUser, DateTime.Now);
            Log.CloseAndFlush();
        }

        private class User
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public DateTime Created { get; set; }
        }

        private class CustomDateFormatter : IFormatProvider
        {
            private readonly IFormatProvider basedOn;
            private readonly string shortDatePattern;

            public CustomDateFormatter(string shortDatePattern, IFormatProvider basedOn)
            {
                this.shortDatePattern = shortDatePattern;
                this.basedOn = basedOn;
            }

            public object GetFormat(Type formatType)
            {
                if (formatType == typeof(DateTimeFormatInfo))
                {
                    var basedOnFormatInfo = (DateTimeFormatInfo)basedOn.GetFormat(formatType);
                    var dateFormatInfo = (DateTimeFormatInfo)basedOnFormatInfo.Clone();
                    dateFormatInfo.ShortDatePattern = this.shortDatePattern;
                    return dateFormatInfo;
                }
                return this.basedOn.GetFormat(formatType);
            }
        }

        // 8 AppSettings
        private void button28_Click(object sender, EventArgs e)
        {
            /* Serilog supports a simple `<appSettings>`-based configuration syntax in `App.config` and `Web.config` files to set the minimum level,
             *  enrich events with additional properties, and control log output.
             * Serilog 支持基于 和 `<appSettings>` `Web.config` 和 files 的简单 `App.config` 配置语法，以设置最低级别、使用其他属性丰富事件以及控制日志输出。
             * Serilog is primarily configured using code, with settings support intended as a supplementary feature. It is not comprehensive but
             *  most logger configuration tasks can be achieved using it.
             * Serilog 主要使用代码进行配置，设置支持旨在作为补充功能。它并不全面，但大多数记录器配置任务都可以使用它来实现。
             */

            // 8.1 Enabling <appSettings> configuration
            // The `<appSettings>` support package needs to be installed from NuGet: Install-Package Serilog.Settings.AppSettings
            // To read configuration from `<appSettings>` use the `ReadFrom.AppSettings()` extension method on your `LoggerConfiguration`:
            Log.Logger = new LoggerConfiguration()
              .ReadFrom.AppSettings() // ... Other configuration here, then
              .CreateLogger();
            /* You can mix and match XML and code-based configuration, but each sink must be configured either using XML or in code - sinks added in code
             *  can't be modified via app settings.
             * 可以混合和匹配 XML 和基于代码的配置，但必须使用 XML 或在代码中配置每个接收器 - 代码中添加的接收器无法通过应用设置进行修改。
             */

            // 8.2 Configuring the logger
            /* To configure the logger, an `<appSettings>` element should be included in the program's *App.config* or *Web.config* file.
<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <appSettings>
    <add key="serilog:minimum-level" value="Verbose" />
    <!-- More settings here -->
             */
            // 8.2.1 Setting the minimum level
            /* To set the logging level for the app use the serilog:minimum-level setting key.
    <add key="serilog:minimum-level" value="Verbose" />
             * Valid values are those defined in the `LogEventLevel` enumeration: `Verbose`, `Debug`, `Information`, `Warning`, `Error`, `Fatal`.
             */
            // 8.2.2 Adding a sink
            /* Sinks are added with the `serilog:write-to` key. The setting name matches the configuration method name that you'd use in code,
             *  so the following are equivalent:
               .WriteTo.LiterateConsole()
             * In XML:
    <add key="serilog:write-to:LiterateConsole" />
             * **NOTE: When using `serilog:\*` keys need to be unique.**
             * Sink assemblies must be specified using the `serilog:using` syntax. For example, to configure
    <add key="serilog:using:LiterateConsole" value="Serilog.Sinks.Literate" />
    <add key="serilog:write-to:LiterateConsole"/>
             * If the sink accepts parameters, these are specified by appending the parameter name to the setting.
                .WriteTo.RollingFile(@"C:\Logs\myapp-{Date}.txt", retainedFileCountLimit: 10)
             * In XML:
    <add key="serilog:using:RollingFile" value="Serilog.Sinks.RollingFile" />
    <add key="serilog:write-to:RollingFile.pathFormat" value="C:\Logs\myapp-{Date}.txt" />
    <add key="serilog:write-to:RollingFile.retainedFileCountLimit" value="10" />
             * Any environment variables specified in a setting value (e.g. %TEMP%) will be expanded appropriately when read.
             */
            // 8.2.3 Using sink extensions from additional assemblies
            /* To use sinks and enrichers from additional assemblies, specify them with the `serilog:using` key.
             * For example, to use configuration from the `Serilog.Sinks.EventLog` assembly:
    <add key="serilog:using:EventLog" value="Serilog.Sinks.EventLog" />
    <add key="serilog:write-to:EventLog.source" value="Serilog Demo" />
             */
            // 8.2.4 Enriching with properties
            /* To attach additional properties to log events, specify them with the `serilog:enrich:with-property` directive.
             * For example, to add the property `Release` with the value `"1.2-develop"` to all events:
    <add key="serilog:enrich:with-property:Release" value="1.2-develop" />
             */
            // 8.2.5 Adding minimum level overrides
            /* Since Serilog 2.1, [minimum level overrides](https://nblumhardt.com/2016/07/serilog-2-minimumlevel-override/) can be added to change the
             *  minimum level for some specific namespaces. This is done with the setting key `serilog:minimum-level:override:` followed by the
             *  *source context prefix*.
             * For instance, the following are equivalent :
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Information()
                    .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                    .MinimumLevel.Override("Microsoft.AspNetCore.Mvc", LogEventLevel.Error)
                    .CreateLogger();
             * and in XML
    <add key="serilog:minimum-level" value="Information" />
    <add key="serilog:minimum-level:override:Microsoft" value="Warning" />
    <add key="serilog:minimum-level:override:Microsoft.AspNetCore.Mvc" value="Error" />
             */
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        // TestRollingFile(Deprecated: new applications should use https://github.com/serilog/serilog-sinks-file instead)
        private void button29_Click(object sender, EventArgs e)
        {
            // This package has been deprecated as it is legacy and no longer maintained.
            // https://github.com/serilog/serilog-sinks-rollingfile
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.RollingFile("logs/myApp-{HalfHour}.log", fileSizeLimitBytes: 1024 * 1, buffered: true)
                .CreateLogger();
            for (int i = 0; i < 2048; i++)
            {
                Log.Information("Hello");
            }
            Log.CloseAndFlush();
        }

        // TestSinkFile
        private void button30_Click(object sender, EventArgs e)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.File("logs/StationApp-ESD-.log",
                    restrictedToMinimumLevel: LogEventLevel.Information,
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}",
                    rollingInterval: RollingInterval.Hour,
                    retainedFileCountLimit: null,
                    fileSizeLimitBytes: 1073741824,
                    rollOnFileSizeLimit: true)
                .CreateLogger();
            string name = "Cgs";
            for (int i = 0; i < 2048; i++)
            {
                Log.Information("Hello,{name}", name);
            }
            Log.Information("End");
            Log.CloseAndFlush();
        }
    }
}