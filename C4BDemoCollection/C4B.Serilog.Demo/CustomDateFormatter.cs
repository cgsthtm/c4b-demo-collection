﻿// <copyright file="CustomDateFormatter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo
{
    using System;
    using System.Globalization;

    /// <summary>
    /// CustomDateFormatter.
    /// </summary>
    internal class CustomDateFormatter : IFormatProvider
    {
        private readonly IFormatProvider basedOn;
        private readonly string shortDatePattern;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomDateFormatter"/> class.
        /// </summary>
        /// <param name="shortDatePattern">shortDatePattern.</param>
        /// <param name="basedOn">IFormatProvider.</param>
        public CustomDateFormatter(string shortDatePattern, IFormatProvider basedOn)
        {
            this.shortDatePattern = shortDatePattern;
            this.basedOn = basedOn;
        }

        /// <summary>
        /// 获取格式化的.
        /// </summary>
        /// <param name="formatType">formatType</param>
        /// <returns>格式化的.</returns>
        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(DateTimeFormatInfo))
            {
                var basedOnFormatInfo = (DateTimeFormatInfo)basedOn.GetFormat(formatType);
                var dateFormatInfo = (DateTimeFormatInfo)basedOnFormatInfo.Clone();
                dateFormatInfo.ShortDatePattern = this.shortDatePattern;
                return dateFormatInfo;
            }

            return this.basedOn.GetFormat(formatType);
        }
    }
}