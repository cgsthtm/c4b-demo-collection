﻿// <copyright file="CastleSerilogFactoryTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.CastleSerilog
{
    using Castle.Services.Logging.SerilogIntegration;
    using global::Serilog;

    /// <summary>
    /// Castle.Core-Serilog测试.
    /// </summary>
    public class CastleSerilogFactoryTest
    {
        private static readonly SerilogFactory SerilogConsoleFactory =
            new SerilogFactory(new LoggerConfiguration()
                .WriteTo.Console().CreateLogger());

        private static readonly Castle.Core.Logging.ILogger ConsoleLogger1 = SerilogConsoleFactory.Create("ConsoleLogger1");
        private static readonly Castle.Core.Logging.ILogger ConsoleLogger2 = SerilogConsoleFactory.Create("ConsoleLogger2");

        private CastleSerilogFactoryTest()
        {
        }

        /// <summary>
        /// ConsoleLogger1.
        /// </summary>
        /// <returns>SerilogFactory.</returns>
        public static Castle.Core.Logging.ILogger GetConsoleLogger1()
        {
            return ConsoleLogger1;
        }

        /// <summary>
        /// ConsoleLogger2.
        /// </summary>
        /// <returns>SerilogFactory.</returns>
        public static Castle.Core.Logging.ILogger GetConsoleLogger2()
        {
            return ConsoleLogger2;
        }
    }
}
