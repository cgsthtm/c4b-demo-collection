﻿namespace C4B.Serilog.Demo
{
    partial class FrmTestSerilog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button30 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "1 Getting started";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(156, 19);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(144, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "2 Configuration basics";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(306, 19);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(144, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "2.1 Creating a logger";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(456, 19);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(144, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "2.2 Sinks";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(606, 19);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(144, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "2.3 Output templates";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(6, 48);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(144, 23);
            this.button6.TabIndex = 5;
            this.button6.Text = "2.4 Minimum level";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(156, 48);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(144, 23);
            this.button7.TabIndex = 6;
            this.button7.Text = "2.5 Overriding per sink";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(306, 48);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(144, 23);
            this.button8.TabIndex = 7;
            this.button8.Text = "2.6 Enrichers";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(456, 48);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(144, 23);
            this.button9.TabIndex = 8;
            this.button9.Text = "2.7 Filters";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(606, 48);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(144, 23);
            this.button10.TabIndex = 9;
            this.button10.Text = "2.8 Sub-loggers???";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(5, 77);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(144, 23);
            this.button11.TabIndex = 10;
            this.button11.Text = "3 Structured Data";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(156, 77);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(144, 23);
            this.button12.TabIndex = 11;
            this.button12.Text = "3.1 Default Behaviour";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(306, 77);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(209, 23);
            this.button13.TabIndex = 12;
            this.button13.Text = "3.2 Preserving Object Structure";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(541, 77);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(209, 23);
            this.button14.TabIndex = 13;
            this.button14.Text = "3.3 Forcing Stringification";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(6, 106);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(144, 23);
            this.button15.TabIndex = 14;
            this.button15.Text = "4 Writing Log Events";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(156, 106);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(144, 23);
            this.button16.TabIndex = 15;
            this.button16.Text = "4.1 Log Event Levels";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(306, 106);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(144, 23);
            this.button17.TabIndex = 16;
            this.button17.Text = "4.2 Source Contexts";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(456, 106);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(144, 23);
            this.button18.TabIndex = 17;
            this.button18.Text = "4.3 Correlation";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(606, 106);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(144, 23);
            this.button19.TabIndex = 18;
            this.button19.Text = "5 Available Sinks";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(6, 135);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(144, 23);
            this.button20.TabIndex = 19;
            this.button20.Text = "6 Enrichment";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(156, 135);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(144, 23);
            this.button21.TabIndex = 20;
            this.button21.Text = "6 Enrichment";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(306, 135);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(144, 23);
            this.button22.TabIndex = 21;
            this.button22.Text = "7 Formatting Output";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(456, 135);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(144, 23);
            this.button23.TabIndex = 22;
            this.button23.Text = "7.1 Formatting plain text";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(606, 135);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(144, 23);
            this.button24.TabIndex = 23;
            this.button24.Text = "7.2 Formatting JSON???";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(6, 164);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(294, 23);
            this.button25.TabIndex = 24;
            this.button25.Text = "7.3 Flexible formatting with ExpressionTemplate ";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(306, 164);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(144, 23);
            this.button26.TabIndex = 25;
            this.button26.Text = "7.4 Custom formatters";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(456, 164);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(144, 23);
            this.button27.TabIndex = 26;
            this.button27.Text = "7.5 Format providers";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(606, 164);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(144, 23);
            this.button28.TabIndex = 27;
            this.button28.Text = "8 AppSettings";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(19, 214);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(144, 23);
            this.button29.TabIndex = 29;
            this.button29.Text = "TestRollingFile(Deprecated)";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button28);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button27);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button26);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button25);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.button24);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button23);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button22);
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.button21);
            this.groupBox1.Controls.Add(this.button10);
            this.groupBox1.Controls.Add(this.button20);
            this.groupBox1.Controls.Add(this.button11);
            this.groupBox1.Controls.Add(this.button19);
            this.groupBox1.Controls.Add(this.button12);
            this.groupBox1.Controls.Add(this.button18);
            this.groupBox1.Controls.Add(this.button13);
            this.groupBox1.Controls.Add(this.button17);
            this.groupBox1.Controls.Add(this.button14);
            this.groupBox1.Controls.Add(this.button16);
            this.groupBox1.Controls.Add(this.button15);
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(775, 196);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(169, 214);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(144, 23);
            this.button30.TabIndex = 31;
            this.button30.Text = "TestSinkFile";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button29);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button30;
    }
}