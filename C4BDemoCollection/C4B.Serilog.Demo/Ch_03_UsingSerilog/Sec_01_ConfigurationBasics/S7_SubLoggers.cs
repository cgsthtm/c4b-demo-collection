﻿// <copyright file="S7_SubLoggers.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.Ch_03_UsingSerilog.Sec_01_ConfigurationBasics
{
    using global::Serilog;
    using global::Serilog.Filters;

    /// <summary>
    /// 子记录器.
    /// </summary>
    public class S7_SubLoggers
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // 有时，需要对接收器所看到的内容进行更精细的控制。为此，Serilog允许一个完整的日志流水线充当接收器。
            var fullName = typeof(S7_SubLoggers).FullName;
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.Logger(lc => lc
                    ////.Filter.ByIncludingOnly(Matching.FromSource("C4B.Serilog.Demo.Installation"))
                    .Filter.ByIncludingOnly(Matching.FromSource<S7_SubLoggers>()) // 怎么日志记录不到logs/log.txt里面?这过滤器到底该怎么用?
                    .WriteTo.File("logs/log.txt"))
                .CreateLogger();

            Log.Information("{SourceContext} S7_SubLoggers1.Test()");
            var log2 = Log.ForContext<S7_SubLoggers>();
            log2.Information("{SourceContext} S7_SubLoggers2.Test()"); // 这样就记录到logs/log.txt里了！！
            Log.CloseAndFlush();

            // 对于子记录器不能很好处理的场景，可以创建多个独立的顶级管道。
            // 只能将一个管道分配给Log.Logger，但您的应用可以根据需要使用任意多个其他ILogger实例。
        }
    }
}
