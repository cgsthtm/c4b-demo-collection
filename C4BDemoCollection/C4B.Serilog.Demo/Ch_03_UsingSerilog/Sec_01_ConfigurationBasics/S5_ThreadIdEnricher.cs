﻿// <copyright file="S5_ThreadIdEnricher.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.Ch_03_UsingSerilog.Sec_01_ConfigurationBasics
{
    using System.Threading;
    using global::Serilog.Core;

    /// <summary>
    /// Enricher集成ILogEventEnricher接口.
    /// </summary>
    public class S5_ThreadIdEnricher : ILogEventEnricher
    {
        /// <inheritdoc/>
        public void Enrich(global::Serilog.Events.LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty(
                "ThreadId", Thread.CurrentThread.ManagedThreadId));
        }
    }
}
