﻿// <copyright file="S1_CreateALoggerWithSinks.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.Ch_03_UsingSerilog.Sec_01_ConfigurationBasics
{
    using global::Serilog;

    /// <summary>
    /// 使用Sink下沉创建Logger.
    /// </summary>
    public class S1_CreateALoggerWithSinks
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // 使用LoggerConfiguration对象创建记录器：
            // Sinks是使用WriteTo配置对象配置的。
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console() // Serilog.Sinks.Console
                .WriteTo.File("logs/log-.txt", rollingInterval: RollingInterval.Day) // Serilog.Sinks.File
                .CreateLogger();
            Log.Information("Ah, there you are!");
        }
    }
}
