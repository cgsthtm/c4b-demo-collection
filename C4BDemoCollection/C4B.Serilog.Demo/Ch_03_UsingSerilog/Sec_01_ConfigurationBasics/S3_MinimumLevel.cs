﻿// <copyright file="S3_MinimumLevel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.Ch_03_UsingSerilog.Sec_01_ConfigurationBasics
{
    using global::Serilog;

    /// <summary>
    /// 最小等级.
    /// </summary>
    public class S3_MinimumLevel
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // MinimumLevel配置对象提供了一个指定为最低的日志事件级别。
            // 在下面的示例中，级别为Debug或更高的日志事件将被处理并最终写入控制台。
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug() // 默认级别-如果未指定MinimumLevel，则将处理Information级别和更高级别的事件。
                .WriteTo.Console()
                .CreateLogger();
            Log.Information("Ah, there you are!");
        }
    }
}
