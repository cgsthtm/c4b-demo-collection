﻿// <copyright file="S6_Goods.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.Ch_03_UsingSerilog.Sec_01_ConfigurationBasics
{
    /// <summary>
    /// 商品.
    /// </summary>
    public class S6_Goods
    {
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Count.
        /// </summary>
        public int Count { get; set; }
    }
}
