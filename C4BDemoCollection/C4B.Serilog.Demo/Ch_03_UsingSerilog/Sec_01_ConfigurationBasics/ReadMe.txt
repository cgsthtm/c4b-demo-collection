﻿https://github.com/serilog/serilog/wiki/Configuration-Basics

Serilog使用一个简单的C# API来配置日志记录。
当需要外部配置时，可以使用Serilog.Settings.AppSettings（https://github.com/serilog/serilog-settings-appsettings）包
或Serilog.Settings.Configuration（https://github.com/serilog/serilog-settings-configuration）包（少量地）混合使用。

# Creating a logger - 创建一个记录器
使用LoggerConfiguration对象创建记录器：
~~~
Log.Logger = new LoggerConfiguration().CreateLogger();
Log.Information("No one listens to me!");

// Finally, once just before the application exits...
Log.CloseAndFlush();
~~~
上面的示例将创建一个不记录任何地方的事件的记录器。要查看日志事件，必须配置接收器(Sink)。

# Sinks - 下沉接收器
日志事件接收器通常将日志事件记录到某些外部表示形式，通常是控制台、文件或数据存储。Serilog sink通过NuGet分发。
接收器是使用WriteTo配置对象配置的。
~~~
Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .CreateLogger();

Log.Information("Ah, there you are!");
~~~
多个接收器可以同时处于活动状态。添加额外的接收器就像链接WriteTo块一样简单：
~~~
Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .WriteTo.File("log-.txt", rollingInterval: RollingInterval.Day)
    .CreateLogger();
~~~

# Output templates - 输出模板
基于文本的接收器使用输出模板来控制格式设置。这可以通过outputTemplate参数进行修改：
~~~
    .WriteTo.File("log.txt",
        outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
~~~
默认模板，如上面的例子所示，使用内置属性，如Timestamp和Level。事件的属性（包括使用enrichers附加的属性）也可以显示在输出模板中。
{Message：lj}格式选项会导致消息中嵌入的数据以JSON（j）格式输出，但字符串文字除外，这些文字按原样输出。
对于更紧凑的级别名称，请分别使用{Level：u3}或{Level：w3}等格式来表示三个字符的上级或下级名称。
将{Properties：j}添加到输出模板以包含其他上下文信息。

# Minimum level - 最低等级
Serilog实现了日志事件处理的“最低级别”的通用概念。
~~~
Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .WriteTo.Console()
    .CreateLogger();
~~~
MinimumLevel配置对象提供了一个指定为最低的日志事件级别。在上面的示例中，级别为0或更高的日志事件将被处理并最终写入控制台。
默认级别-如果未指定MinimumLevel，则将处理信息级别和更高级别的事件。

# Overriding per sink - 每个下沉接收器的覆盖
有时，我们希望将详细的日志写入一个介质，而将不太详细的日志写入另一个介质。
~~~
Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .WriteTo.File("log.txt")
    .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information)
    .CreateLogger();
~~~
在本例中，调试日志将被写入滚动文件，而只有信息级别和更高级别的日志将被写入控制台。

# Enrichers - 丰富器
丰富器是简单的组件，用于添加、删除或修改附加到日志事件的属性。例如，这可以用于将线程id附加到每个事件。
~~~
class ThreadIdEnricher : ILogEventEnricher
{
    public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
    {
        logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty(
                "ThreadId", Thread.CurrentThread.ManagedThreadId));
    }
}
~~~
使用Enrich配置对象添加Enrich。
~~~
Log.Logger = new LoggerConfiguration()
    .Enrich.With(new ThreadIdEnricher())
    .WriteTo.Console(
        outputTemplate: "{Timestamp:HH:mm} [{Level}] ({ThreadId}) {Message}{NewLine}{Exception}")
    .CreateLogger();
~~~
上面的配置显示了如何在输出格式中使用由丰富器添加的属性。
如果丰富的属性值在整个应用程序运行过程中保持不变，则可以使用快捷方式WithProperty方法来简化配置。
~~~
Log.Logger = new LoggerConfiguration()
    .Enrich.WithProperty("Version", "1.0.0")
    .WriteTo.Console()
    .CreateLogger();
~~~

# Filters - 过滤器
事件可以通过过滤有选择地记录。过滤器只是LogEvent上的谓词，一些常见的场景由Matching类处理。
~~~
Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .Filter.ByExcluding(Matching.WithProperty<int>("Count", p => p < 10))
    .CreateLogger();
~~~

# Sub-loggers - 子记录器
有时，需要对接收器所看到的内容进行更精细的控制。为此，Serilog允许一个完整的日志流水线充当接收器。
~~~
Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .WriteTo.Logger(lc => lc
        .Filter.ByIncludingOnly(...)
        .WriteTo.File("log.txt"))
    .CreateLogger();
~~~
对于子记录器不能很好处理的场景，可以创建多个独立的顶级管道。
只能将一个管道分配给Log.Logger，但您的应用可以根据需要使用任意多个其他ILogger实例。