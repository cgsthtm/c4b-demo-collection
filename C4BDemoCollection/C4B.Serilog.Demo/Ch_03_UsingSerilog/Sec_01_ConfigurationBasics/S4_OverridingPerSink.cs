﻿// <copyright file="S4_OverridingPerSink.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.Ch_03_UsingSerilog.Sec_01_ConfigurationBasics
{
    using global::Serilog;
    using global::Serilog.Events;

    /// <summary>
    /// 覆盖每个Sink下沉接收器.
    /// </summary>
    public class S4_OverridingPerSink
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // 有时，我们希望将详细的日志写入一个介质，而将不太详细的日志写入另一个介质。
            // 在本例中，调试日志将被写入滚动文件，而只有Information信息级别和更高级别的日志将被写入控制台。
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("logs/log.txt")
                .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information)
                .CreateLogger();
            Log.Debug("Ah, there you are! - only wirite to RollingFile.");
            Log.Information("Ah, there you are!");

            /*Logger记录器与Sink接收器最小值-重要的是要认识到，只能提高Sink接收器的记录级别，而不能降低。
             * 因此，如果记录器的MinimumLevel设置为Information，则将Debug作为其指定级别的Sink接收器仍将仅看到Information级别的事件。
             */
        }
    }
}
