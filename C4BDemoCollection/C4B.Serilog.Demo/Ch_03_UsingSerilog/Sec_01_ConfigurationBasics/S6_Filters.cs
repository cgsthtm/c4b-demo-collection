﻿// <copyright file="S6_Filters.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.Ch_03_UsingSerilog.Sec_01_ConfigurationBasics
{
    using global::Serilog;
    using global::Serilog.Filters;

    /// <summary>
    /// 过滤器.
    /// </summary>
    public class S6_Filters
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // 事件可以通过过滤有选择地记录。过滤器只是LogEvent上的谓词，一些常见的场景由Matching类处理。
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .Filter.ByExcluding(Matching.WithProperty<int>("Count", p => p < 10))
                .CreateLogger();
            S6_Goods goods1 = new S6_Goods { Name = "Apple", Count = 20 };
            S6_Goods goods2 = new S6_Goods { Name = "Orange", Count = 3 };
            Log.Information<S6_Goods>("goods1-{Count}", goods1);
            Log.Information<S6_Goods>("goods2-{Count}", goods2); // 过滤器不是这样用

            Log.Logger = new LoggerConfiguration()
                .Enrich.WithProperty("Count", 3) // 这种用法才能由Matching处理
                .WriteTo.Console()
                .Filter.ByExcluding(Matching.WithProperty<int>("Count", p => p < 10))
                .CreateLogger();
            Log.Information("{Count}"); // 不打印，因为被Filter过滤了

            Log.Logger = new LoggerConfiguration()
                .Enrich.WithProperty("Count", 20)
                .WriteTo.Console()
                .Filter.ByExcluding(Matching.WithProperty<int>("Count", p => p < 10))
                .CreateLogger();
            Log.Information("{Count}"); // 打印，因为符合Filter过滤器匹配规则

            // 既然创建Logger的时候已经写死了Count的值！这样用过滤器，它有啥用啊？
        }
    }
}
