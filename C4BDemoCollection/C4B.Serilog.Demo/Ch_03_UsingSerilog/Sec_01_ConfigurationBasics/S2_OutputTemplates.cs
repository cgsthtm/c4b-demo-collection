﻿// <copyright file="S2_OutputTemplates.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.Ch_03_UsingSerilog.Sec_01_ConfigurationBasics
{
    using global::Serilog;

    /// <summary>
    /// 输出模板.
    /// </summary>
    public class S2_OutputTemplates
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // 基于文本的接收器使用输出模板来控制格式设置。这可以通过outputTemplate参数进行修改：
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();

            // 默认模板，如上面的例子所示，使用内置属性，如Timestamp和Level。
            // 事件的属性（包括使用enrichers附加的属性 https://github.com/serilog/serilog/wiki/Enrichment ）也可以显示在输出模板中。
            // {Message：lj}格式选项会导致消息中嵌入的数据以JSON（j）格式输出，但字符串文字除外，这些文字按原样输出。
            // 对于更紧凑的级别名称，请分别使用{Level：u3}或{Level：w3}等格式来表示三个字符的上级或下级名称。
            // 将{Properties：j}添加到输出模板以包含其他上下文信息。
            Log.Information("Ah, there you are!");
            var obj = new { Name = "cgs", Age = 18 };
            Log.Information("hello:{@obj}", obj);
        }
    }
}
