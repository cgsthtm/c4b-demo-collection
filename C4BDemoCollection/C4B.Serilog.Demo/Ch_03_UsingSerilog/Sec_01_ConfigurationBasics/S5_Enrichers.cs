﻿// <copyright file="S5_Enrichers.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.Ch_03_UsingSerilog.Sec_01_ConfigurationBasics
{
    using global::Serilog;

    /// <summary>
    /// 丰富器.
    /// </summary>
    public class S5_Enrichers
    {
        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            // 丰富器是简单的组件，用于添加、删除或修改附加到日志事件的属性。例如，这可以用于将线程id附加到每个事件。
            Log.Logger = new LoggerConfiguration()
                .Enrich.With(new S5_ThreadIdEnricher()) // 添加Enrich，注意下面输出模板中配置了({ThreadId})
                .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] (ThreadId:{ThreadId}) {Message:lj}{NewLine}{Exception}")
                .CreateLogger();
            Log.Information("Ah, there you are!");

            // 如果丰富的属性值在整个应用程序运行过程中保持不变，则可以使用快捷方式WithProperty方法来简化配置。
            Log.Logger = new LoggerConfiguration()
                .Enrich.WithProperty("Version", "1.0.0") // 使用WithProperty方法来简化配置
                .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] (Ver:{Version}) {Message:lj}{NewLine}{Exception}")
                .CreateLogger();
            Log.Information("Ah, there you are!");
        }
    }
}
