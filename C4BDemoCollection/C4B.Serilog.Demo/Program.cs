﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo
{
    using System;
    using System.Diagnostics;
    using C4B.Serilog.Demo.AutofacSerilogIntegration;
    using C4B.Serilog.Demo.CastleSerilog;
    using C4B.Serilog.Demo.Ch_01_WhySerilog;
    using C4B.Serilog.Demo.Ch_02_GettingStarted;
    using C4B.Serilog.Demo.Ch_03_UsingSerilog.Sec_01_ConfigurationBasics;
    using global::Serilog;

    /// <summary>
    /// 程序.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            // # Debugging and Diagnostics
            // First, Serilog will write simple diagnostic messages to user-specified output if provided.
            // Call SelfLog.Enable() at program startup:
            ////global::Serilog.Debugging.SelfLog.Enable(msg => Debug.WriteLine(msg));

            // The system console, a file or an in-memory StringWriter can all be used to collect Serilog's output
            // by providing a TextWriter instead of a delegate:
            ////global::Serilog.Debugging.SelfLog.Enable(Console.Error);

            // Serilog never writes its own events to user-defined sinks.

            // Why Serilog?
            ////TextFormattingWithATwist.Test();

            // Getting Started - Setup
            ////SetupTest.Setup();
            ////SetupTest.Test();

            // UsingSerilog - ConfigurationBasics
            ////S1_CreateALoggerWithSinks.Test();
            ////S2_OutputTemplates.Test();
            ////S3_MinimumLevel.Test();
            ////S4_OverridingPerSink.Test();
            ////S5_Enrichers.Test();
            ////S6_Filters.Test();
            S7_SubLoggers.Test();

            // Test Castle.Core-Serilog
            CastleSerilogFactoryTest.GetConsoleLogger1().Info("[{SourceContext}] ConsoleLogger1.Info");
            CastleSerilogFactoryTest.GetConsoleLogger2().Info("[{SourceContext}] ConsoleLogger2.Info");

            // Test AutofacSerilogIntegration
            AutofacSerilogIntegrationTest.Test();

            Console.ReadLine();
            FrmTestSerilog form1 = new FrmTestSerilog();
            form1.ShowDialog();

            ////#region Getting started
            ////// The root Logger is created using LoggerConfiguration.
            ////var log = new LoggerConfiguration()
            ////    .MinimumLevel.Debug()
            ////    .WriteTo.Console() // sent to the console
            ////    .WriteTo.File("logs/myapp.txt", rollingInterval: RollingInterval.Day) // sent to the log file
            ////    .CreateLogger();

            ////// Serilog's global, statically accessible logger, is set via Log.Logger and can be invoked using the static methods on the Log class.
            ////Log.Logger = log;
            ////Test.Test1();

            ////// This is typically done once at application start-up, and the logger saved for later use by application classes.
            ////// Multiple loggers can be created and used independently if required.
            ////log.Information("Hello, Serilog!");

            ////// The complete example below shows logging in a simple console application, with events sent to the console as well as a date-stamped rolling log file.
            ////int a = 10, b = 0;
            ////try
            ////{
            ////    Log.Debug("Dividing {A} by {B}", a, b);
            ////    Console.WriteLine(a / b);
            ////}
            ////catch (Exception ex)
            ////{
            ////    Log.Error(ex, "Something went wrong");
            ////}
            ////finally
            ////{
            ////    Log.CloseAndFlush();
            ////}
            ////#endregion

            ////#region Configuration basics | https://github.com/serilog/serilog/wiki/Configuration-Basics
            //// Serilog uses a simple C# API to configure logging.
            //// When external configuration is desirable it can be mixed in (sparingly) using the Serilog.Settings.AppSettings package or Serilog.Settings.Configuration package.

            //// Creating a logger
            ////#region create a logger that does not record events anywhere
            ////Log.Logger = new LoggerConfiguration().CreateLogger();
            ////Log.Information("No one listens to me!");

            ////// Finally, once just before the application exits...
            ////Log.CloseAndFlush();
            ////#endregion
            //// The example above will create a logger that does not record events anywhere. To see log events, a sink must be configured.

            //// Sinks
            //// Log event sinks generally record log events to some external representation, typically the console, a file or data store. Serilog sinks are distributed via NuGet.
            //// A curated list of available sinks is listed here on the wiki. https://github.com/serilog/serilog/wiki/Provided-Sinks
            //// Multiple sinks can be active at the same time. Adding additional sinks is a simple as chaining WriteTo blocks:
            ////Log.Logger = new LoggerConfiguration()
            ////    .WriteTo.Console()
            ////    .WriteTo.File("logs/log-.txt",
            ////        // The interval at which logging will roll over to a new file.
            ////        rollingInterval: RollingInterval.Minute,
            ////        // Text-based sinks use output templates to control formatting. this can be modified through the outputTemplate parameter:
            ////        outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}"
            ////        // The default template, shown in the example above, uses built-in properties like Timestamp and Level.
            ////        // Properties from events, including those attached using enrichers, can also appear in the output template. https://github.com/serilog/serilog/wiki/Enrichment
            ////    ).CreateLogger();
            ////Log.Information("Test Sinks!");

            ////#region Minimum level
            //// Serilog implements the common concept of a 'minimum level' for log event processing.
            ////Log.Logger = new LoggerConfiguration()
            ////    .MinimumLevel.Debug()
            ////    .WriteTo.Console()
            ////    .CreateLogger();
            //// The MinimumLevel configuration object provides for one of the log event levels to be specified as the minimum.
            //// In the example above, log events with level Debug and higher will be processed and ultimately written to the console.
            //// Default Level - if no MinimumLevel is specified, then Information level events and higher will be processed.
            ////#endregion

            ////#region Overriding per sink
            //// Sometimes it is desirable to write detailed logs to one medium, but less detailed logs to another.
            ////Log.Logger = new LoggerConfiguration()
            ////    .MinimumLevel.Debug()
            ////    .WriteTo.File("log.txt")
            ////    .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information)
            ////    .CreateLogger();
            //// In this example debug logs will be written to the rolling file, while only Information level logs and higher will be written to the console.
            ////#endregion

            ////#region Enrichers
            //// Enrichers are simple components that add, remove or modify the properties attached to a log event. This can be used for the purpose of attaching a thread id to each event, for example.
            //// Enrichers are added using the Enrich configuration object.
            ////Log.Logger = new LoggerConfiguration()
            ////    .Enrich.With(new ThreadIdEnricher()) // ThreadIdEnricher.cs
            ////    .WriteTo.Console(
            ////        outputTemplate: "{Timestamp:HH:mm} [{Level}] ({ThreadId}) {Message}{NewLine}{Exception}")
            ////    .CreateLogger();
            ////Log.Information("11111");
            ////// If the enriched property value is constant throughout the application run, the shortcut WithProperty method can be used to simplify configuration.
            ////Log.Logger = new LoggerConfiguration()
            ////    .Enrich.WithProperty("Version", "1.0.0")
            ////    .WriteTo.Console(
            ////        outputTemplate: "{Timestamp:HH:mm} [{Level}] ({Version}) {Message}{NewLine}{Exception}")
            ////    .CreateLogger();
            ////Log.Information("222222");
            ////#endregion

            ////#region Filters
            //// Events can be selectively logged by filtering. Filters are just predicates over LogEvent, with some common scenarios handled by the Matching class.
            ////Log.Logger = new LoggerConfiguration()
            ////    //.Enrich.With(new Count9Enricher())
            ////    .Enrich.With(new Count11Enricher())
            ////    .WriteTo.Console(outputTemplate: "{Timestamp:HH:mm} [{Level}] ({Count}) {Message}{NewLine}{Exception}")
            ////    .Filter.ByExcluding(Matching.WithProperty<int>("Count", p => p < 10))
            ////    .CreateLogger();
            ////Log.Information("333333");
            ////Log.Logger = new LoggerConfiguration()
            ////    .Enrich.With(new Count9Enricher())
            ////    //.Enrich.With(new Count11Enricher())
            ////    .WriteTo.Console(outputTemplate: "{Timestamp:HH:mm} [{Level}] ({Count}) {Message}{NewLine}{Exception}")
            ////    .Filter.ByExcluding(Matching.WithProperty<int>("Count", p => p < 10))
            ////    .CreateLogger();
            ////Log.Information("444444");
            ////#endregion

            ////#region Sub-loggers ???
            //// Sometimes a finer level of control over what is seen by a sink is necessary. For this, Serilog allows a full logging pipeline to act as a sink.
            ////Log.Logger = new LoggerConfiguration()
            ////.WriteTo.Console()
            ////.WriteTo.Logger(lc => lc
            ////    .Filter.ByIncludingOnly(Matching.WithProperty<int>("Count", p => p < 10))
            ////    .WriteTo.File("logs/log.txt"))
            ////.CreateLogger();
            //// For scenarios not handled well by sub-loggers, it's fine to create multiple independent top-level pipelines.
            //// Only one pipeline can be assigned to Log.Logger, but your app can use as many additional ILogger instances as it requires.
            ////#endregion

            ////#endregion

            ////#region Structured Data | https://github.com/serilog/serilog/wiki/Structured-Data
            ////Log.Logger = new LoggerConfiguration().WriteTo.Console().CreateLogger();

            ////#region Default Behaviour
            //// When properties are specified in log events, Serilog does its best to determine the correct representation.

            //// Simple, Scalar Values
            ////var count = 456;
            ////Log.Information("Retrieved {Count} records", count);
            //// There's little ambiguity as to how the Count property should be stored in this case. Being a simple integer value, Serilog will choose that as its representation.
            //// { "Count": 456 }

            //// Collections
            //// If the object passed as a property is IEnumerable, Serilog will treat that property as a collection.
            ////var fruit = new[] { "Apple", "Pear", "Orange" };
            ////Log.Information("In my bowl I have {Fruit}", fruit);
            //// The equivalent JSON includes an array.
            //// { "Fruit": ["Apple", "Pear", "Orange"] }
            //// Serilog also recognises Dictionary<TKey,TValue>, as long as the key type is one of the scalar types listed above.
            ////var fruit1 = new Dictionary<string, int> { { "Apple", 1 }, { "Pear", 5 } };
            ////Log.Information("In my bowl I have {Fruit}", fruit1);
            //// { "Fruit": { "Apple": 1, "Pear": 5 }}

            //// Objects
            //// Apart from the types above, which are specially handled by Serilog, it is difficult to make intelligent choices about how data should be rendered and persisted.
            //// Objects not explicitly intended for serialisation tend to serialise very poorly.
            ////SqlConnection conn = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            ////Log.Information("Connected to {Connection}", conn);
            //// (Yikes! How does one serialise an SqlConnection?)
            //// When Serilog doesn't recognise the type, and no operator is specified (see below) then the object will be rendered using ToString().
            ////#endregion

            ////#region Preserving Object Structure
            //// There are many places where, given the capability, it makes sense to serialise a log event property as a structured object.
            //// DTOs (data transfer objects), messages, events and models are often best logged by breaking them down into properties with values.
            //// For this task, Serilog provides the @ destructuring operator.
            ////var sensorInput = new { Latitude = 25, Longitude = 134 };
            ////Log.Information("Processing {@SensorInput}", sensorInput); // Processing {"Latitude": 25, "Longitude": 134}
            ////Log.Information("Processing {SensorInput}", sensorInput);  // Processing { Latitude = 25, Longitude = 134 }

            //// Customizing the stored data
            //// Often only a selection of properties on a complex object are of interest. To customise how Serilog persists a destructured complex type, use the Destructure configuration object on LoggerConfiguration:
            ////Log.Logger = new LoggerConfiguration()
            //    .Destructure.ByTransforming<HttpRequest>(
            //        r => new { Url = r.Url, RawUrl = r.RawUrl, Method = r.HttpMethod })
            //    .WriteTo.Console().CreateLogger();
            ////var httpReq = new HttpRequest("abc", "http://www.bing.com", "hello");
            ////Log.Information("{@httpReq}", httpReq);
            //// This example transforms objects of type HttpRequest to preserve only the RawUrl and Method properties.
            //// A number of different strategies for destructuring are available, and custom ones can be created by implementing IDestructuringPolicy
            ////#endregion

            ////#region Forcing Stringification
            //// Sometimes, the type of an object being logged may not be exactly known, or may vary in a way that is undesirable to preserve in the log events.
            //// In these cases the $ stringification operator will convert the property value to a string before any other processing takes place, regardless of its type or implemented interfaces.
            ////var unknown = new[] { 1, 2, 3 };
            ////Log.Information("Received {$Data}", unknown);
            //// Despite being an enumerable type, the unknown variable is captured and rendered as a string.
            //// Received "System.Int32[]"
            ////#endregion
            ////#endregion

            ////#region Writing Log Events | https://github.com/serilog/serilog/wiki/Writing-Log-Events
            //// Log events are written to sinks using the Log static class, or the methods on an ILogger.
            //// These examples will use Log for syntactic brevity, but the same methods shown below are available also on the interface.
            ////int quota = 1024;
            ////var user = "nblumhardt";
            ////Log.Warning("Disk quota {Quota} MB exceeded by {User}", quota, user);
            //// The warning event created from this log method will have two associated properties, Quota and User. Assuming quota is an integer, and user a string, the rendered message may look like the one below.
            //// Disk quota 1024 MB exceeded by "nblumhardt"
            //// (Serilog renders string values in double quotes to more transparently indicate the underlying data type, and to make the property value stand out from the surrounding message text.)

            //// Dynamic levels
            //// If an app needs dynamic level switching, the first step is to create an instance of LoggingLevelSwitch when the logger is being configured:
            ////var levelSwitch = new LoggingLevelSwitch();
            ////levelSwitch.MinimumLevel = LogEventLevel.Warning;
            ////var log = new LoggerConfiguration()
            //      .MinimumLevel.ControlledBy(levelSwitch)
            //      .WriteTo.Console()
            //      .CreateLogger();
            ////log.Debug("levelSwitch.MinimumLevel = LogEventLevel.Warning;");
            ////log.Warning("levelSwitch.MinimumLevel = LogEventLevel.Warning;");
            ////levelSwitch.MinimumLevel = LogEventLevel.Debug;
            ////log.Debug("levelSwitch.MinimumLevel = LogEventLevel.Debug;");
            ////log.Warning("levelSwitch.MinimumLevel = LogEventLevel.Debug;");

            //// Source Contexts ???
            //// Serilog, like most .NET logging frameworks, allows events to be tagged with their source, generally speaking the name of the class writing them:
            ////var myLog = Log.ForContext<MyClass>();
            ////myLog.Information("Hello!");
            ////#endregion

            ////#region Enrichment | https://github.com/serilog/serilog/wiki/Enrichment
            //// Log events can be enriched with properties in various ways. A number of pre-built enrichers are provided through NuGet:
            //// Install-Package Serilog.Enrichers.Thread
            ////var log2 = new LoggerConfiguration()
            //    .Enrich.WithThreadId()
            //    .WriteTo.Console()
            //    .CreateLogger();
            //// All events written through log will carry a property ThreadId with the id of the managed thread that wrote them.
            //// (By convention, any .WithXyz() methods on Enrich create properties named Xyz.)
            ////log2.Information("Serilog.Enrichers.Thread, ThreadId:{ThreadId}");

            //// The LogContext
            //// Serilog.Context.LogContext can be used to dynamically add and remove properties from the ambient "execution context";
            //// This feature must be added to the logger at configuration-time using .FromLogContext():
            ////log2 = new LoggerConfiguration()
            //    .Enrich.FromLogContext()
            //    .WriteTo.Console()
            //    .CreateLogger();
            //// Then, properties can be added and removed from the context using LogContext.PushProperty():
            ////log2.Information("No contextual properties");
            ////using (LogContext.PushProperty("A", 1))
            ////{
            //    log2.Information("Carries property A = 1");
            //    using (LogContext.PushProperty("A", 2))
            //    using (LogContext.PushProperty("B", 1))
            //    {
            //        log2.Information("Carries A = 2 and B = 1");
            //    }
            //    log2.Information("Carries property A = 1, again");
            ////}
            ////#endregion

            ////#region Formatting Output | https://github.com/serilog/serilog/wiki/Formatting-Output
            //// Formatting plain text
            //// The format of events written by these sinks can be modified using the outputTemplate configuration parameter. For example, to control the console sink:
            ////Log.Logger = new LoggerConfiguration()
            //    .WriteTo.Console(outputTemplate:
            //        "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}")
            //    .CreateLogger();

            //// Formatting JSON
            //// Many sinks record log events as JSON, or can be configured to do so. To emit JSON, rather than plain text, a formatter can be specified.
            //// This example configures the file sink using the formatter from Serilog.Formatting.Compact.
            ////Log.Logger = new LoggerConfiguration()
            //    .WriteTo.Console(new CompactJsonFormatter())
            //    .CreateLogger();
            //// There are three JSON formatters provided by the Serilog project:
            //// 1. Serilog.Formatting.Json.JsonFormatter - This is the historical default shipped in the Serilog package. It produces a complete rendering of the log event and supports a few configuration options.
            //// 2. Serilog.Formatting.Compact.CompactJsonFormatter - A newer, more space-efficient JSON formatter shipped in Serilog.Formatting.Compact.
            //// 3. Serilog.Formatting.Compact.RenderedCompactJsonFormatter - Also shipped in Serilog.Formatting.Compact, this formatter pre-renders message templates into text.
            ////Log.Information("Formatting JSON");

            //// Flexible formatting with ExpressionTemplate
            //// The Serilog.Expressions package includes the ExpressionTemplate class for more sophisticated text and JSON formatting.
            //// Expression templates can include conditional blocks, repeated sections, computations over event properties, and custom formatting functions.

            //// Custom formatters
            //// Both plain text and JSON formatting are implemented using the ITextFormatter interface. Implementations of this interface can format log events into any text-based format.

            //// Format providers
            //// There may be scenarios where it is desirable to override or specify the way a DateTime is formatted. This can be done via the implementation of IFormatProvider.
            //// This strategy applies to any type that you pass to Serilog.
            ////var formatter = new CustomDateFormatter("dd-MMM-yyyy", new CultureInfo("en-AU"));
            ////Log.Logger = new LoggerConfiguration()
            //    .WriteTo.Console(formatProvider: new CultureInfo("en-AU")) // Console 1
            //    .WriteTo.Console(formatProvider: formatter)                // Console 2
            //    .CreateLogger();
            ////var exampleUser = new User { Id = 1, Name = "Adam", Created = DateTime.Now };
            ////Log.Information("Created {@User} on {Created}", exampleUser, DateTime.Now);
            ////Log.CloseAndFlush();
            ////#endregion

            ////#region AppSettings | https://github.com/serilog/serilog/wiki/AppSettings
            //// Serilog supports a simple <appSettings>-based configuration syntax in App.config and Web.config files to set the minimum level, enrich events with additional properties, and control log output.
            //// Serilog is primarily configured using code, with settings support intended as a supplementary feature. It is not comprehensive but most logger configuration tasks can be achieved using it.

            //// Enabling <appSettings> configuration
            //// The <appSettings> support package needs to be installed from NuGet: Install-Package Serilog.Settings.AppSettings
            //// To read configuration from <appSettings> use the ReadFrom.AppSettings() extension method on your LoggerConfiguration:
            ////Log.Logger = new LoggerConfiguration()
            //      .ReadFrom.AppSettings()
            //      .WriteTo.Console()
            //      // Other configuration here, then
            //      .CreateLogger();
            //// You can mix and match XML and code-based configuration, but each sink must be configured either using XML or in code - sinks added in code can't be modified via app settings.

            //// Configuring the logger
            //// see App.config
            ////#endregion

            Console.ReadLine();
        }
    }
}