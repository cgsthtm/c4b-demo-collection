﻿// <copyright file="Count9Enricher.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo
{
    using global::Serilog.Core;
    using global::Serilog.Events;

    /// <summary>
    /// Count9Enricher.
    /// </summary>
    public class Count9Enricher : ILogEventEnricher
    {
        /// <inheritdoc/>
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty(
                    "Count", 9));
        }
    }
}