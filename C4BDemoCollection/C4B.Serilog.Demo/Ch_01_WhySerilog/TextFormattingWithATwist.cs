﻿// <copyright file="TextFormattingWithATwist.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Serilog.Demo.Ch_01_WhySerilog
{
    using global::Serilog;

    /// <summary>
    /// 文本格式化.
    /// </summary>
    public class TextFormattingWithATwist
    {
        // https://serilog.net/

        /// <summary>
        /// Test.
        /// </summary>
        public static void Test()
        {
            var log = new LoggerConfiguration()
                .WriteTo.Console().CreateLogger();
            var position = new { Latitude = 25, Longitude = 134 };
            var elapsedMs = 34;

            // 此示例记录两个属性Position和Elapsed沿着日志事件。示例中捕获的JSON格式的属性如下所示：
            // {"Position": {"Latitude": 25, "Longitude": 134}, "Elapsed": 34}
            // Position前面的@操作符告诉Serilog序列化传入的对象，而不是使用ToString()转换它。
            // Elapsed后面的：000段是一个标准的.NET格式字符串，它会影响属性的呈现方式。Serilog附带的控制台接收器将显示上述消息：
            // 09:14:22 [Information] Processed { Latitude: 25, Longitude: 134 } in 034 ms.
            log.Information("Processed {@Position} in {Elapsed:000} ms.", position, elapsedMs);
        }
    }
}
