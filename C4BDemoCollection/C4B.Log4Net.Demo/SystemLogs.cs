﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Log4Net.Demo
{
    public class SystemLogs
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 日志时间
        /// </summary>
        public DateTime LogTime { get; set; }
        /// <summary>
        /// 日志类型
        /// </summary>
        public int LogType { get; set; }
        /// <summary>
        /// 日志内容
        /// </summary>
        public string LogContent { get; set; }
        /// <summary>
        /// 最后编辑人
        /// </summary>
        public string LastModifiedUserId { get; set; }

        public override string ToString()
        {
            return $"Id：{Id},LogTime：{LogTime.ToString()},LogType：{LogType},LogContent：{LogContent},LastModifiedUserId：{LastModifiedUserId}";
        }
    }
}
