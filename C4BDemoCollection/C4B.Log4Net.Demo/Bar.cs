﻿using log4net;

namespace C4B.Log4Net.Demo
{
    public class Bar
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Bar));

        public void DoIt()
        {
            log.Info("Did it again!");
        }
    }
}