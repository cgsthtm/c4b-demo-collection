﻿// Import log4net classes.
using log4net;
using log4net.Config;
using System;

namespace C4B.Log4Net.Demo
{
    internal class Program
    {
        // Define a static logger variable so that it references the
        // Logger instance named "MyApp".
        private static readonly ILog log = LogManager.GetLogger("logToDatabase");

        private static void Main(string[] args)
        {
            // Set up a simple configuration that logs on the console.
            //BasicConfigurator.Configure();

            // The only way to configure an application using the System.Configuration APIs is to call the log4net.Config.XmlConfigurator.Configure() method
            // or the log4net.Config.XmlConfigurator.Configure(ILoggerRepository) method.
            ////XmlConfigurator.Configure();

            // BasicConfigurator replaced with XmlConfigurator.
            //XmlConfigurator.Configure(new System.IO.FileInfo(args[0]));

            log.Info("Entering application.");
            Bar bar = new Bar();
            bar.DoIt();
            LogDbHelper.ConfigureLog4Net();
            LogDbHelper.Info(new SystemLogs
            {
                Id = Guid.NewGuid().ToString(),
                LogTime = DateTime.Now,
                LogType = 1,
                LogContent = "Test AdoNetAppender2",
                LastModifiedUserId = Guid.NewGuid().ToString(),
            });
            Console.ReadLine();
            log.Info("Exiting application.");
        }
    }
}