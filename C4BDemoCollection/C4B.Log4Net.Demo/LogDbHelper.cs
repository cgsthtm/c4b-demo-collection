﻿using log4net;
using log4net.Appender;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace C4B.Log4Net.Demo
{
    public class LogDbHelper
    {
        private static readonly ILog Log = LogManager.GetLogger("logToDatabase");

        /// <summary>
        /// 写入Info.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="e"></param>
        public static void Info(object message, Exception e = null)
        {
            if (typeof(string) != message.GetType())
            {
                log4net.LogicalThreadContext.Properties["Id"] = ((SystemLogs)message).Id;
                log4net.LogicalThreadContext.Properties["LogTime"] = ((SystemLogs)message).LogTime;
                log4net.LogicalThreadContext.Properties["LogType"] = ((SystemLogs)message).LogType;
                log4net.LogicalThreadContext.Properties["LogContent"] = ((SystemLogs)message).LogContent;
                log4net.LogicalThreadContext.Properties["LastModifiedUserId"] = ((SystemLogs)message).LastModifiedUserId;
            }
            else
            {
                log4net.LogicalThreadContext.Properties.Clear();
            }

            if (e == null)
            {
                Log.Info(message);
            }
            else
            {
                Log.Info(message, e);
            }
        }

        /// <summary>
        /// 配置log4net数据库连接字符串.
        /// </summary>
        public static void ConfigureLog4Net()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            Hierarchy hierarchy = LogManager.GetRepository() as Hierarchy;
            if (hierarchy != null && hierarchy.Configured)
            {
                foreach (IAppender appender in hierarchy.GetAppenders())
                {
                    if (appender is AdoNetAppender)
                    {
                        var adoNetAppender = (AdoNetAppender)appender;
                        adoNetAppender.ConnectionString = connectionString;
                        adoNetAppender.ActivateOptions(); //Refresh AdoNetAppenders Settings
                    }
                }
            }

            // 加载配置文件
            var configFile = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\Config\\log4net.config");
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(configFile.FullName);

            // 修改 connectionString (假设你知道如何定位到具体的节点)
            XmlNode connectionStringNode = xmlDoc.SelectSingleNode("//log4net/appender[@name='AdoNetAppender']/connectionString");
            if (connectionStringNode != null)
            {
                connectionStringNode.Attributes["value"].Value = connectionString;
            }

            // 保存修改后的配置文件
            xmlDoc.Save(configFile.FullName);
        }

        /// <summary>
        /// 改变指定appender节点的文件名..用于将日志写入不同文件.
        /// </summary>
        /// <param name="AppenderName">appender配置节点</param>
        /// <param name="NewFilename">新文件名称</param>
        /// <returns></returns>
        public static bool ChangeLogFileName(string AppenderName, string NewFilename)
        {
            // log4net.Repository.ILoggerRepository RootRep;
            // RootRep = log4net.LogManager.GetRepository();
            log4net.Repository.ILoggerRepository RootRep = Log.Logger.Repository;
            foreach (log4net.Appender.IAppender iApp in RootRep.GetAppenders())
            {
                string appenderName = iApp.Name;
                if (iApp.Name.CompareTo(AppenderName) == 0
                    && iApp is log4net.Appender.FileAppender)
                {
                    log4net.Appender.FileAppender fApp = (log4net.Appender.FileAppender)iApp;
                    fApp.File = NewFilename;
                    fApp.ActivateOptions();
                    return true; // Appender found and name changed to NewFilename
                }
            }
            return false; // appender not found
        }
    }
}
