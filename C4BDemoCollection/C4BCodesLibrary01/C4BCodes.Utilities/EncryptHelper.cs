﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    public class EncryptHelper
    {
        /// <summary>
        /// 默认密钥向量
        /// </summary>
        private static byte[] Keys = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };

        /// <summary>
        /// DES密匙
        /// </summary>
        public static string DESKey = "HYSYS.HY";

        /// <summary>
        /// AES密匙
        /// </summary>
        public static string AESKey = "6YHN7ujm1qaz6yhn1qaz6yhn";

        /// <summary>
        /// DES加密算法
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="sKey"></param>
        /// <returns></returns>
        public static string DESEncrypt(string Text, string sKey = "HYSYS.HY")
        {
            try
            {
                byte[] rgbKey = Encoding.UTF8.GetBytes(sKey.Substring(0, 8));
                byte[] rgbIV = Keys;
                byte[] inputByteArray = Encoding.UTF8.GetBytes(Text);
                DESCryptoServiceProvider dCSP = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, dCSP.CreateEncryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                cStream.Close();
                mStream.Close();
                return Convert.ToBase64String(mStream.ToArray());
            }
            catch
            {
                return Text;
            }
        }

        /// <summary>
        /// DES解密算法
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="sKey"></param>
        /// <returns></returns>
        public static string DESDecrypt(string Text, string sKey = "HYSYS.HY")
        {
            try
            {
                byte[] rgbKey = Encoding.UTF8.GetBytes(sKey);
                byte[] rgbIV = Keys;
                byte[] inputByteArray = Convert.FromBase64String(Text);
                DESCryptoServiceProvider DCSP = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, DCSP.CreateDecryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                cStream.Close();
                mStream.Close();
                return Encoding.UTF8.GetString(mStream.ToArray());
            }
            catch
            {
                return Text;
            }
        }

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="encryptString">待加密的密文</param>
        /// <param name="encryptKey">加密密匙</param>
        /// <returns></returns>
        public static string AESEncrypt(string encryptString, string encryptKey = "zhongguohangkong")
        {
            string returnValue;
            byte[] temp = Convert.FromBase64String(AESKey);
            Rijndael AESProvider = Rijndael.Create();
            try
            {
                byte[] byteEncryptString = Encoding.Default.GetBytes(encryptString);
                MemoryStream memoryStream = new MemoryStream();
                CryptoStream cryptoStream = new CryptoStream(memoryStream, AESProvider.CreateEncryptor(Encoding.Default.GetBytes(encryptKey), temp), CryptoStreamMode.Write);
                cryptoStream.Write(byteEncryptString, 0, byteEncryptString.Length);
                cryptoStream.FlushFinalBlock();
                returnValue = Convert.ToBase64String(memoryStream.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }

        /// <summary>
        ///AES 解密
        /// </summary>
        /// <param name="decryptString">待解密密文</param>
        /// <param name="decryptKey">解密密钥</param>
        /// <returns></returns>
        public static string AESDecrypt(string decryptString, string decryptKey = "zhongguohangkong")
        {
            string returnValue = "";
            byte[] temp = Convert.FromBase64String(AESKey);
            Rijndael AESProvider = Rijndael.Create();
            AESProvider.Mode = CipherMode.ECB;
            AESProvider.Padding = PaddingMode.PKCS7;
            try
            {
                byte[] byteDecryptString = System.Convert.FromBase64String(decryptString);
                MemoryStream memoryStream = new MemoryStream();
                CryptoStream cryptoStream = new CryptoStream(memoryStream, AESProvider.CreateDecryptor(Encoding.Default.GetBytes(decryptKey), temp), CryptoStreamMode.Write);
                cryptoStream.Write(byteDecryptString, 0, byteDecryptString.Length);
                cryptoStream.FlushFinalBlock();
                returnValue = Encoding.Default.GetString(memoryStream.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }

        public static string AESKeyyt = "[45/*YUIdse..e;]";

        public static string AESDecrypt1(string value, string _aeskey = null)
        {
            try
            {
                if (string.IsNullOrEmpty(_aeskey))
                {
                    _aeskey = AESKeyyt;
                }
                value = value.Trim();
                byte[] keyArray = Encoding.UTF8.GetBytes(_aeskey);
                byte[] toEncryptArray = Convert.FromBase64String(value.Trim(trimChars: '\r').Trim(trimChars: '\0'));

                RijndaelManaged rDel = new RijndaelManaged();
                rDel.Key = keyArray;
                rDel.Mode = CipherMode.ECB;
                rDel.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = rDel.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                return Encoding.UTF8.GetString(resultArray);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 利用MD5对字符串进行加密
        /// </summary>
        /// <param name="encryptString">待加密的字符串</param>
        /// <returns>返回加密后的字符串</returns>
        public static string MD5Encrypt(string encryptString)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] encryptedBytes = md5.ComputeHash(Encoding.ASCII.GetBytes(encryptString));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < encryptedBytes.Length; i++)
            {
                sb.AppendFormat("{0:x2}", encryptedBytes[i]);
            }
            return sb.ToString();
        }

        /// <summary>
        /// 将字符串进行Base64加密
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Base64Encode(string str)
        {
            byte[] bytes = Encoding.Default.GetBytes(str);
            return Convert.ToBase64String(bytes);
        }

        /// <summary>
        /// 将Base64形式的字符串进行解密
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Base64Decode(string str)
        {
            string result;
            try
            {
                byte[] bytes = Convert.FromBase64String(str);
                result = Encoding.Default.GetString(bytes);
            }
            catch
            {
                result = str;
            }
            return result;
        }
    }
}