﻿using System;
using System.Runtime.InteropServices;

namespace C4BCodesLibrary.C4BCodes.Utilities.ZLGCAN
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN
    {
        public uint acc_code;
        public uint acc_mask;
        public uint reserved;
        public byte filter;
        public byte timing0;
        public byte timing1;
        public byte mode;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct CANFD
    {
        public uint acc_code;
        public uint acc_mask;
        public uint abit_timing;
        public uint dbit_timing;
        public uint brp;
        public byte filter;
        public byte mode;
        public UInt16 pad;
        public uint reserved;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct can_frame
    {
        public uint can_id;  /* 32 bit MAKE_CAN_ID + EFF/RTR/ERR flags */
        public byte can_dlc; /* frame payload length in byte (0 .. CAN_MAX_DLEN) */
        public byte __pad;   /* padding */
        public byte __res0;  /* reserved / padding */
        public byte __res1;  /* reserved / padding */

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] data/* __attribute__((aligned(8)))*/;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct can_frame1
    {
        public uint can_id;  /* 32 bit MAKE_CAN_ID + EFF/RTR/ERR flags */
        public byte can_dlc; /* frame payload length in byte (0 .. CAN_MAX_DLEN) */
        public byte __pad;   /* padding */
        public byte __res0;  /* reserved / padding */
        public byte __res1;  /* reserved / padding */

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] data/* __attribute__((aligned(8)))*/;

        /// <summary>
        /// 自定义10个字节的字节数组，组装成第一版软件的协议
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public byte[] data1/* __attribute__((aligned(10)))*/;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct canfd_frame
    {
        public uint can_id;  /* 32 bit MAKE_CAN_ID + EFF/RTR/ERR flags */
        public byte len;     /* frame payload length in byte */
        public byte flags;   /* additional flags for CAN FD,i.e error code */
        public byte __res0;  /* reserved / padding */
        public byte __res1;  /* reserved / padding */

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public byte[] data/* __attribute__((aligned(8)))*/;
    };

    [StructLayout(LayoutKind.Explicit)]
    public struct ZCAN_CHANNEL_INIT_CONFIG
    {
        [FieldOffset(0)]
        public uint can_type; //type:TYPE_CAN TYPE_CANFD

        [FieldOffset(4)]
        public ZCAN can;

        [FieldOffset(4)]
        public CANFD canfd;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN_Transmit_Data
    {
        public can_frame frame;
        public uint transmit_type;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN_Receive_Data
    {
        public can_frame frame;
        public UInt64 timestamp;//us
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN_Receive_Data1
    {
        public can_frame1 frame;
        public UInt64 timestamp;//us
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN_TransmitFD_Data
    {
        public canfd_frame frame;
        public uint transmit_type;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN_AUTO_TRANSMIT_OBJ     //CAN定时发送帧结构体
    {
        public ushort enable;           //0-禁用，1-使能
        public ushort index;            //定时报文索引
        public uint interval;                  //定时周期
        public ZCAN_Transmit_Data obj;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct ZCANFD_AUTO_TRANSMIT_OBJ    //CANFD定时发送帧结构体
    {
        public ushort enable;           //0-禁用，1-使能
        public ushort index;            //定时报文索引
        public uint interval;                  //定时周期
        public ZCAN_TransmitFD_Data obj;
    };

    public struct DeviceInfo
    {
        public uint device_type;  //设备类型
        public uint channel_count;//设备的通道个数

        public DeviceInfo(uint type, uint count)
        {
            device_type = type;
            channel_count = count;
        }
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN_ReceiveFD_Data
    {
        public canfd_frame frame;
        public UInt64 timestamp;//us
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct ZCAN_CHANNEL_ERROR_INFO
    {
        public uint error_code;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public byte[] passive_ErrData;

        public byte arLost_ErrData;
    };

    //for zlg cloud
    [StructLayout(LayoutKind.Sequential)]
    public struct ZCLOUD_CHNINFO
    {
        public byte enable;
        public byte type;
        public byte isUpload;
        public byte isDownload;
    };

    //for zlg cloud
    [StructLayout(LayoutKind.Sequential)]
    public struct ZCLOUD_DEVINFO
    {
        public int devIndex;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] type;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] id;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] name;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] owner;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] model;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public char[] fwVer;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public char[] hwVer;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] serial;

        public int status;             // 0:online, 1:offline

        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte bGpsUploads;

        public byte channelCnt;   // each channel enable can upload

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public ZCLOUD_CHNINFO[] channels;
    };

    //[StructLayout(LayoutKind.Sequential)]
    //public struct ZCLOUD_DEV_GROUP_INFO
    //{
    //    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
    //    public char[] groupName;
    //    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
    //    public char[] desc;
    //    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
    //    public char[] groupId;
    //    //public ZCLOUD_DEVINFO *pDevices;
    //    public IntPtr pDevices;
    //    public uint devSize;
    //};

    [StructLayout(LayoutKind.Sequential)]
    public struct ZCLOUD_USER_DATA
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] username;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public char[] mobile;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public char[] dllVer;

        public uint devCnt;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 100)]
        public ZCLOUD_DEVINFO[] devices;
    };

    public class Define
    {
        public const int TYPE_CAN = 0;
        public const int TYPE_CANFD = 1;
        public const int ZCAN_USBCAN1 = 3;
        public const int ZCAN_USBCAN2 = 4;
        public const int ZCAN_CANETUDP = 12;
        public const int ZCAN_CANETTCP = 17;
        public const int ZCAN_USBCAN_E_U = 20;
        public const int ZCAN_USBCAN_2E_U = 21;
        public const int ZCAN_PCIECANFD_100U = 38;
        public const int ZCAN_PCIECANFD_200U = 39;
        public const int ZCAN_PCIECANFD_200U_EX = 62;
        public const int ZCAN_PCIECANFD_400U = 61;
        public const int ZCAN_USBCANFD_800U = 59;
        public const int ZCAN_USBCANFD_200U = 41;
        public const int ZCAN_USBCANFD_100U = 42;
        public const int ZCAN_USBCANFD_MINI = 43;
        public const int ZCAN_CLOUD = 46;
        public const int ZCAN_CANFDNET_200U_TCP = 48;
        public const int ZCAN_CANFDNET_200U_UDP = 49;
        public const int ZCAN_CANFDNET_400U_TCP = 52;
        public const int ZCAN_CANFDNET_400U_UDP = 53;
        public const int ZCAN_CANFDNET_800U_TCP = 57;
        public const int ZCAN_CANFDNET_800U_UDP = 58;
        public const int STATUS_ERR = 0;
        public const int STATUS_OK = 1;
    };

    public class Method
    {
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ZCAN_OpenDevice(uint device_type, uint device_index, uint reserved);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_CloseDevice(IntPtr device_handle);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ZCAN_InitCAN(IntPtr device_handle, uint can_index, IntPtr pInitConfig);

        /// <summary>
        /// 设置设备属性
        /// </summary>
        /// <param name="device_handle"></param>
        /// <param name="path"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_SetValue(IntPtr device_handle, string path, byte[] value);

        /// <summary>
        /// 设置设备属性
        /// </summary>
        /// <param name="device_handle"></param>
        /// <param name="path"></param>
        /// <param name=""></param>
        /// <returns></returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_SetValue(IntPtr device_handle, string path, string value);

        /// <summary>
        /// 设置设备属性
        /// </summary>
        /// <param name="device_handle"></param>
        /// <param name="path"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_SetValue(IntPtr device_handle, string path, IntPtr value);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_StartCAN(IntPtr channel_handle);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_ResetCAN(IntPtr channel_handle);

        /// <summary>
        /// 清除缓冲区
        /// </summary>
        /// <param name="channel_handle"></param>
        /// <returns></returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_ClearBuffer(IntPtr channel_handle);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_Transmit(IntPtr channel_handle, IntPtr pTransmit, uint len);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_TransmitFD(IntPtr channel_handle, IntPtr pTransmit, uint len);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_GetReceiveNum(IntPtr channel_handle, byte type);

        /// <summary>
        /// 获取设备属性
        /// </summary>
        /// <param name="device_handle"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ZCAN_GetValue(IntPtr device_handle, string path);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_Receive(IntPtr channel_handle, IntPtr data, uint len, int wait_time = -1);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_ReceiveFD(IntPtr channel_handle, IntPtr data, uint len, int wait_time = -1);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCAN_ReadChannelErrInfo(IntPtr channel_handle, IntPtr pErrInfo);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr GetIProperty(IntPtr device_handle);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool ZCLOUD_IsConnected();

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void ZCLOUD_SetServerInfo(string httpAddr, ushort httpPort,
            string mqttAddr, ushort mqttPort);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCLOUD_ConnectServer(string username, string password);

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern uint ZCLOUD_DisconnectServer();

        [DllImport("zlgcan.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ZCLOUD_GetUserData(int updata);
    }

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int SetValueFunc(string path, byte[] value);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetValueFunc(string path);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetPropertysFunc(string path, string value);

    public struct IProperty
    {
        public SetValueFunc SetValue;
        public GetValueFunc GetValue;
        public GetPropertysFunc GetPropertys;
    };

    /// <summary>
    /// 周立功CAN ZLG致远电子 USBCANFD-800U 帮助类
    /// </summary>
    public class ZlgCanHelper
    {
        /// <summary>
        /// CAN最大数据长度
        /// </summary>
        private const int CAN_MAX_DLEN = 8;

        /// <summary>
        /// 获取指定通道的CAN数据
        /// </summary>
        /// <param name="channel_handle_">通道句柄</param>
        /// <param name="can_data">接收数据数组</param>
        /// <param name="max_len">每次最多接收数量</param>
        /// <returns>接收到的数量</returns>
        public static uint GetChannelReceiveData(IntPtr channel_handle_, ref ZCAN_Receive_Data[] can_data, uint max_len = 1000)
        {
            uint len = Method.ZCAN_GetReceiveNum(channel_handle_, 0);
            if (len > 0)
            {
                int size = Marshal.SizeOf(typeof(ZCAN_Receive_Data));
                len = len > max_len ? max_len : len; // 每次从缓冲区接收最多1000个字节
                IntPtr ptr = Marshal.AllocHGlobal((int)len * size);
                len = Method.ZCAN_Receive(channel_handle_, ptr, len, 50);
                for (int i = 0; i < len; ++i)
                {
                    can_data[i] = (ZCAN_Receive_Data)Marshal.PtrToStructure(
                        (IntPtr)((Int64)ptr + i * size), typeof(ZCAN_Receive_Data));
                }
                Marshal.FreeHGlobal(ptr);
            }
            return len;
        }

        /// <summary>
        /// 获取指定通道的CAN数据1
        /// </summary>
        /// <param name="channel_handle_">通道句柄</param>
        /// <param name="can_data">接收数据数组</param>
        /// <returns>接收到的数量</returns>
        public static uint GetChannelReceiveData1(IntPtr channel_handle_, ref ZCAN_Receive_Data1[] can_data)
        {
            uint len = Method.ZCAN_GetReceiveNum(channel_handle_, 0);
            if (len > 0)
            {
                int size = Marshal.SizeOf(typeof(ZCAN_Receive_Data));
                //len = len > 1000 ? 1000 : len; // 每次从缓冲区接收最多1000个字节
                IntPtr ptr = Marshal.AllocHGlobal((int)len * size);
                len = Method.ZCAN_Receive(channel_handle_, ptr, len, 50);
                for (int i = 0; i < len; ++i)
                {
                    can_data[i] = (ZCAN_Receive_Data1)Marshal.PtrToStructure(
                        (IntPtr)((Int64)ptr + i * size), typeof(ZCAN_Receive_Data1));
                    byte[] idBytes = can_data[i].frame.can_id.ToString("X4").ToCanIdByteArray();
                    Array.Copy(idBytes, 0, can_data[i].frame.data1, 0, 2);
                    Array.Copy(can_data[i].frame.data, 0, can_data[i].frame.data1, 2, 8);
                }
                Marshal.FreeHGlobal(ptr);
            }
            return len;
        }

        /// <summary>
        /// 给指定通道发送CAN数据帧
        /// </summary>
        /// <param name="channel_handle_">通道句柄</param>
        /// <param name="hexIDStr">帧ID，如：0725</param>
        /// <param name="hexDataStr">发送的数据，用空格隔开，如：02 10 03 00 00 00 00 00</param>
        /// <returns></returns>
        public static bool SendCANData(IntPtr channel_handle_, string hexIDStr = "0725", string hexDataStr = "02 10 03 00 00 00 00 00")
        {
            uint id = (uint)Convert.ToInt32(hexIDStr, 16);
            string data = hexDataStr;
            int frame_type_index = 0; // 帧类型 0-标准帧 1-扩展帧
            //int protocol_index = 0;   // 协议 0-CAN 1-CANFD
            int send_type_index = 0;  // 发送方式 0-正常发送 1-单次发送 2-自发自收 3-单次自发自收
            //int canfd_exp_index = 0;  // CANFD加速 0-否 1-是
            uint result; //发送的帧数

            // CAN协议
            ZCAN_Transmit_Data can_data = new ZCAN_Transmit_Data();
            can_data.frame.can_id = MakeCanId(id, frame_type_index, 0, 0);
            can_data.frame.data = new byte[8];
            can_data.frame.can_dlc = (byte)data.ToByteArray(ref can_data.frame.data, CAN_MAX_DLEN);
            can_data.transmit_type = (uint)send_type_index;
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(can_data));
            Marshal.StructureToPtr(can_data, ptr, true);
            result = Method.ZCAN_Transmit(channel_handle_, ptr, 1);
            Marshal.FreeHGlobal(ptr);
            return result == 1;
        }

        /// <summary>
        /// 给指定通道发送CAN数据帧
        /// </summary>
        /// <param name="channel_handle_">通道句柄</param>
        /// <param name="hexIDStr">帧ID，如：0725</param>
        /// <param name="data">发送的数据 字节数组</param>
        /// <returns></returns>
        public static bool SendCANData(IntPtr channel_handle_, string hexIDStr, byte[] data)
        {
            uint id = (uint)Convert.ToInt32(hexIDStr, 16);
            int frame_type_index = 0; // 帧类型 0-标准帧 1-扩展帧
            //int protocol_index = 0;   // 协议 0-CAN 1-CANFD
            int send_type_index = 0;  // 发送方式 0-正常发送 1-单次发送 2-自发自收 3-单次自发自收
            //int canfd_exp_index = 0;  // CANFD加速 0-否 1-是
            uint result; //发送的帧数

            // CAN协议
            ZCAN_Transmit_Data can_data = new ZCAN_Transmit_Data();
            can_data.frame.can_id = MakeCanId(id, frame_type_index, 0, 0);
            can_data.frame.data = data;
            can_data.frame.can_dlc = 8;
            can_data.transmit_type = (uint)send_type_index;
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(can_data));
            Marshal.StructureToPtr(can_data, ptr, true);
            result = Method.ZCAN_Transmit(channel_handle_, ptr, 1);
            Marshal.FreeHGlobal(ptr);

            return result == 1;
        }

        /// <summary>
        /// 给指定通道发送CAN数据帧
        /// </summary>
        /// <param name="channel_handle_">通道句柄</param>
        /// <param name="data">经过组装后的10个字节的数组</param>
        /// <returns></returns>
        public static bool SendCANData(IntPtr channel_handle_, byte[] data)
        {
            var hexIDStr = (new byte[] { data[0], data[1] }).ToHexStr(withSpace: false, reverse: false);
            uint id = (uint)Convert.ToInt32(hexIDStr, 16);
            int frame_type_index = 0; // 帧类型 0-标准帧 1-扩展帧
            //int protocol_index = 0;   // 协议 0-CAN 1-CANFD
            int send_type_index = 0;  // 发送方式 0-正常发送 1-单次发送 2-自发自收 3-单次自发自收
            //int canfd_exp_index = 0;  // CANFD加速 0-否 1-是
            uint result; //发送的帧数

            // CAN协议
            ZCAN_Transmit_Data can_data = new ZCAN_Transmit_Data();
            can_data.frame.can_id = MakeCanId(id, frame_type_index, 0, 0);
            can_data.frame.data = new byte[8];
            Array.Copy(data, 2, can_data.frame.data, 0, 8);
            can_data.frame.can_dlc = 8;
            can_data.transmit_type = (uint)send_type_index;
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(can_data));
            Marshal.StructureToPtr(can_data, ptr, true);
            result = Method.ZCAN_Transmit(channel_handle_, ptr, 1);
            Marshal.FreeHGlobal(ptr);

            return result == 1;
        }

        /// <summary>
        /// 生产CANID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="eff"></param>
        /// <param name="rtr"></param>
        /// <param name="err"></param>
        /// <returns></returns>
        private static uint MakeCanId(uint id, int eff, int rtr, int err)//1:extend frame 0:standard frame
        {
            uint ueff = (uint)(!!(Convert.ToBoolean(eff)) ? 1 : 0);
            uint urtr = (uint)(!!(Convert.ToBoolean(rtr)) ? 1 : 0);
            uint uerr = (uint)(!!(Convert.ToBoolean(err)) ? 1 : 0);
            return id | ueff << 31 | urtr << 30 | uerr << 29;
        }
    }
}