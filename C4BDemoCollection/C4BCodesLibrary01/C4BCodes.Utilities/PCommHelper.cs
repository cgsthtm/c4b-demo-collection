﻿using System.Runtime.InteropServices;

namespace C4BCodesLibrary.C4BCodes.Utilities.PComm
{
    public static class PCommHelper
    {
        /// <summary>
        /// 打开串口
        /// </summary>
        /// <param name="port">串口号</param>
        /// <returns></returns>
        [DllImport("PComm.dll", EntryPoint = "sio_open")]
        public static extern int sio_open(int port);

        /// <summary>
        /// 配置串口通讯参数，例如波特率、校验位、数据位和停止位
        /// <para>例如：ret = sio_ioctl ( 2, B38400, P_NONE | BIT_8 | STOP_1 );</para>
        /// </summary>
        /// <param name="port">串口号</param>
        /// <param name="baud">波特率</param>
        /// <param name="mode">数据位|停止位|校验位</param>
        /// <returns></returns>
        [DllImport("PComm.dll", EntryPoint = "sio_ioctl")]
        public static extern int sio_ioctl(int port, int baud, int mode);

        /// <summary>
        /// 获取输入缓冲区中积累的数据长度
        /// </summary>
        /// <param name="port">串口号</param>
        /// <returns></returns>
        [DllImport("PComm.dll", EntryPoint = "sio_iqueue")]
        public static extern int sio_iqueue(int port);

        /// <summary>
        /// 设置DTR状态
        /// </summary>
        /// <param name="port">串口号</param>
        /// <param name="mode">DTR设置：0-DTR off；1-DTR on</param>
        /// <returns></returns>
        [DllImport("PComm.dll", EntryPoint = "sio_DTR")]
        public static extern int sio_DTR(int port, int mode);

        /// <summary>
        /// 设置RTS状态
        /// </summary>
        /// <param name="port">串口号</param>
        /// <param name="mode">RTS设置：0-RTS off；1-RTS on</param>
        /// <returns></returns>
        [DllImport("PComm.dll", EntryPoint = "sio_RTS")]
        public static extern int sio_RTS(int port, int mode);

        /// <summary>
        /// 刷新驱动程序上的输入/输出缓冲区中的数据
        /// </summary>
        /// <param name="port">串口号</param>
        /// <param name="func">刷新策略：0-刷新输入缓冲区；1-刷新输出缓冲区；2-刷新输入和输出缓冲区</param>
        /// <returns></returns>
        [DllImport("PComm.dll", EntryPoint = "sio_flush")]
        public static extern int sio_flush(int port, int func);

        /// <summary>
        /// 关闭串口
        /// </summary>
        /// <param name="port">串口号</param>
        /// <returns></returns>
        [DllImport("PComm.dll", EntryPoint = "sio_close")]
        public static extern int sio_close(int port);

        /// <summary>
        /// 读取驱动程序中输入缓冲区的数据
        /// </summary>
        /// <param name="port">串口号</param>
        /// <param name="buf">接受缓冲区指针</param>
        /// <param name="length">每次被读取的数据长度</param>
        /// <returns></returns>
        [DllImport("PComm.dll", EntryPoint = "sio_read")]
        public static extern int sio_read(int port, ref byte buf, int length);

        /// <summary>
        /// 将一个数据块放到驱动程序的输出缓冲区中
        /// </summary>
        /// <param name="port">串口号</param>
        /// <param name="buf">发送缓冲区指针</param>
        /// <param name="length">发送数据长度</param>
        /// <returns></returns>
        [DllImport("PComm.dll", EntryPoint = "sio_write")]
        public static extern int sio_write(int port, ref byte buf, int length);

        /// <summary>
        /// 将一个字符写入到驱动程序的输出缓冲区中
        /// </summary>
        /// <param name="port">串口号</param>
        /// <param name="term">字符，0-255</param>
        /// <returns></returns>
        [DllImport("PComm.dll", EntryPoint = "sio_putch")]
        public static extern int sio_putch(int port, int term);

        /// <summary>
        /// 从驱动程序的输入缓冲区中读取一个字符
        /// </summary>
        /// <param name="port">串口号</param>
        /// <returns></returns>
        [DllImport("PComm.dll", EntryPoint = "sio_getch")]
        public static extern int sio_getch(int port);

        // 波特率
        public const int B50 = 0x0;

        public const int B75 = 0x1;
        public const int B110 = 0x2;
        public const int B134 = 0x3;
        public const int B150 = 0x4;
        public const int B300 = 0x5;
        public const int B600 = 0x6;
        public const int B1200 = 0x7;
        public const int B1800 = 0x8;
        public const int B2400 = 0x9;
        public const int B4800 = 0xA;
        public const int B7200 = 0xB;
        public const int B9600 = 0xC;
        public const int B19200 = 0xD;
        public const int B38400 = 0xE;
        public const int B57600 = 0xF;
        public const int B115200 = 0x10;
        public const int B230400 = 0x11;
        public const int B460800 = 0x12;
        public const int B921600 = 0x13;

        // 数据位
        public const int BIT_5 = 0x0;

        public const int BIT_6 = 0x1;
        public const int BIT_7 = 0x2;
        public const int BIT_8 = 0x3;

        // 停止位
        public const int STOP_1 = 0x0;

        public const int STOP_2 = 0x4;

        // 校验位
        public const int P_EVEN = 0x18;

        public const int P_ODD = 0x8;
        public const int P_SPC = 0x38;
        public const int P_MRK = 0x28;
        public const int P_NONE = 0x0;

        // 返回值
        public const int SIO_OK = 0;

        public const int SIO_BADPORT = -1;	/* no such port or port not opened */
        public const int SIO_OUTCONTROL = -2;	/* can't control the board */
        public const int SIO_NODATA = -4;	/* no data to read or no buffer to write */
        public const int SIO_OPENFAIL = -5;	/* no such port or port has be opened */
        public const int SIO_RTS_BY_HW = -6;      /* RTS can't set because H/W flowctrl */
        public const int SIO_BADPARM = -7;	/* bad parameter */
        public const int SIO_WIN32FAIL = -8;	/* call win32 function fail, please call */
    }
}