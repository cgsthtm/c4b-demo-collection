﻿namespace C4BCodesLibrary.C4BCodes.Utilities
{
    public static class StringHelper
    {
        /// <summary>
        /// 扩展方法，删除该字符串中最后一个指定的字符之后的部分
        /// </summary>
        /// <param name="str">源字符串</param>
        /// <param name="strchar">指定字符</param>
        /// <returns>被删除后的字符串</returns>
        public static string DelAfterLastChar(this string str, string strchar)
        {
            return str.Substring(0, str.LastIndexOf(strchar));
        }

        /// <summary>
        /// 扩展方法，获取字符串的最后N个字符串
        /// </summary>
        /// <param name="str">源字符串</param>
        /// <param name="len">长度</param>
        /// <returns>字符串的最后len个字符串</returns>
        public static string GetAftermostString(this string str, int len)
        {
            string rtn = "";
            if (str.Length >= len)
            {
                rtn = str.Substring(str.Length - len, len);
            }
            return rtn;
        }
    }
}