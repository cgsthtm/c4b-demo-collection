﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    public static class XmlHelper
    {
        /// <summary>
        /// 将指定类型序列化为XML字符串
        /// </summary>
        /// <typeparam name="T">类型参数</typeparam>
        /// <param name="obj">类型实例</param>
        /// <returns></returns>
        public static string Serialize<T>(T obj) where T : class, new()
        {
            var xmlStr = "";
            try
            {
                using (var stream = new MemoryStream())
                {
                    var settings = new XmlWriterSettings
                    {
                        Encoding = Encoding.Default,
                        Indent = false,
                        NamespaceHandling = NamespaceHandling.OmitDuplicates
                    };
                    using (var xmlWriter = XmlWriter.Create(stream, settings))
                    {
                        var serializer = new XmlSerializer(typeof(T));

                        var ns = new XmlSerializerNamespaces();
                        ns.Add("", "");
                        serializer.Serialize(xmlWriter, obj, ns);

                        stream.Position = 0;
                        var buffer = new byte[stream.Length];
                        stream.Read(buffer, 0, buffer.Length);
                        xmlStr = Encoding.Default.GetString(buffer);
                    }
                }
            }
            catch (Exception ex)
            {
                xmlStr = ex.Message;
            }
            return xmlStr;
        }

        /// <summary>
        /// 将指定类型序列化为XML字符串
        /// </summary>
        /// <typeparam name="T">类型参数</typeparam>
        /// <param name="obj">类型实例</param>
        /// <param name="XmlDec">是否写入描述</param>
        /// <returns></returns>
        public static string Serialize<T>(T obj, bool XmlDec = false) where T : class, new()
        {
            var xmlStr = "";
            try
            {
                using (var stream = new MemoryStream())
                {
                    var settings = new XmlWriterSettings
                    {
                        Encoding = Encoding.Default,
                        Indent = false,
                        NamespaceHandling = NamespaceHandling.OmitDuplicates,
                        OmitXmlDeclaration = XmlDec
                    };
                    using (var xmlWriter = XmlWriter.Create(stream, settings))
                    {
                        var serializer = new XmlSerializer(typeof(T));

                        var ns = new XmlSerializerNamespaces();
                        ns.Add("", "");
                        serializer.Serialize(xmlWriter, obj, ns);

                        stream.Position = 0;
                        var buffer = new byte[stream.Length];
                        stream.Read(buffer, 0, buffer.Length);
                        xmlStr = Encoding.Default.GetString(buffer);
                    }
                }
            }
            catch (Exception ex)
            {
                xmlStr = ex.Message;
            }
            return xmlStr;
        }

        /// <summary>
        /// 将指定类型序列化为XML字符串
        /// </summary>
        /// <typeparam name="T">类型参数</typeparam>
        /// <param name="obj">类型实例</param>
        /// <param name="result">操作执行结果</param>
        /// <returns></returns>
        public static string Serialize<T>(T obj, out bool result) where T : class, new()
        {
            var xmlStr = "";
            try
            {
                using (var stream = new MemoryStream())
                {
                    var settings = new XmlWriterSettings
                    {
                        Encoding = Encoding.Default,
                        Indent = false,
                        NamespaceHandling = NamespaceHandling.OmitDuplicates
                    };
                    using (var xmlWriter = XmlWriter.Create(stream, settings))
                    {
                        var serializer = new XmlSerializer(typeof(T));

                        var ns = new XmlSerializerNamespaces();
                        ns.Add("", "");
                        serializer.Serialize(xmlWriter, obj, ns);

                        stream.Position = 0;
                        var buffer = new byte[stream.Length];
                        stream.Read(buffer, 0, buffer.Length);
                        xmlStr = Encoding.Default.GetString(buffer);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                xmlStr = ex.Message;
            }
            return xmlStr;
        }

        /// <summary>
        /// 将XML字符串反序列化为指定类型
        /// </summary>
        /// <typeparam name="T">类型参数</typeparam>
        /// <param name="xmlStr">XML字符串</param>
        /// <returns></returns>
        public static T Deserialize<T>(string xmlStr) where T : class, new()
        {
            var obj = new T();
            try
            {
                // utf-8字符串无法反序列化问题,将utf-直接更改为gbk
                xmlStr = Regex.Replace(xmlStr, "encoding=\"UTF-8\"", "encoding=\"GBK\"", RegexOptions.IgnoreCase);
                var buffer = Encoding.Default.GetBytes(xmlStr);
                using (var stream = new MemoryStream(buffer))
                {
                    using (var xmlReader = XmlReader.Create(stream))
                    {
                        var serializer = new XmlSerializer(typeof(T));
                        obj = (T)serializer.Deserialize(xmlReader);
                    }
                }
            }
            catch
            {
            }
            return obj;
        }

        /// <summary>
        /// 将XML字符串反序列化为指定类型
        /// </summary>
        /// <typeparam name="T">类型参数</typeparam>
        /// <param name="xmlStr">XML字符串</param>
        /// <param name="result">操作执行结果</param>
        /// <returns></returns>
        public static T Deserialize<T>(string xmlStr, out bool result) where T : class, new()
        {
            var obj = new T();
            try
            {
                var buffer = Encoding.Default.GetBytes(xmlStr);
                using (var stream = new MemoryStream(buffer))
                {
                    using (var xmlReader = XmlReader.Create(stream))
                    {
                        var serializer = new XmlSerializer(typeof(T));
                        obj = (T)serializer.Deserialize(xmlReader);

                        result = true;
                    }
                }
            }
            catch
            {
                result = false;
            }
            return obj;
        }

        /// <summary>
        /// 获取xml的节点值（适用于有多个相同节点，默认取第一个节点值，即索引为0的节点值）
        /// </summary>
        /// <param name="xmlsrc"></param>
        /// <param name="xmlNode"></param>
        /// <param name="index">索引</param>
        /// <returns></returns>
        public static string GetXmlNodeValue(string xmlsrc, string xmlNode, int index = 0)
        {
            string text = string.Empty;
            string text2 = "<" + xmlNode + ">";
            string value = "</" + xmlNode + ">";
            if (xmlsrc.Contains(text2) && xmlsrc.Contains(value))
            {
                int num = xmlsrc.IndexOf(text2, index, StringComparison.Ordinal) + text2.Length;
                int length = xmlsrc.IndexOf(value, index, StringComparison.Ordinal) - num;
                text = xmlsrc.Substring(num, length);
            }
            return text;
        }

        /// <summary>
        /// 扩展方法，将DataTable转成XML字符串
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <returns></returns>
        public static string ToXml(this DataTable dt)
        {
            StringBuilder xml = new StringBuilder();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.DataType.ToString().Contains("DateTime"))
                    {
                        if (dt.Rows[0][dc.ColumnName] != DBNull.Value)
                            xml.AppendFormat("<{0}>{1}</{0}>", dc.ColumnName, System.Convert.ToDateTime(dt.Rows[0][dc.ColumnName]).ToString("yyyy-MM-dd HH:mm:ss"));
                        else
                            xml.AppendFormat("<{0}>{1}</{0}>", dc.ColumnName, dt.Rows[0][dc.ColumnName]);
                    }
                    else
                        xml.AppendFormat("<{0}>{1}</{0}>", dc.ColumnName, dt.Rows[0][dc.ColumnName]);
                }
            }
            return xml.ToString();
        }
    }
}