﻿using System;
using System.Linq;
using System.Text;

namespace C4BCodesLibrary.C4BCodes.Utilities
{
    public static class ByteHelper
    {
        /// <summary>
        /// 字节数组转十六进制字符串，如：[0xae,0x00,0xcf] => "AE00CF"
        /// </summary>
        /// <param name="bytes">源字节数组</param>
        /// <param name="withSpace">字节之间是否带空格</param>
        /// <param name="reverse">是否反转字节数组</param>
        /// <returns>十六进制字符串</returns>
        public static string ToHexStr(this byte[] bytes, bool withSpace = true, bool reverse = false)
        {
            string hexString = string.Empty;
            if (bytes != null)
            {
                StringBuilder strB = new StringBuilder();
                bytes = reverse ? bytes.Reverse().ToArray() : bytes;
                for (int i = 0; i < bytes.Length; i++)
                {
                    if (withSpace)
                    {
                        strB.Append(bytes[i].ToString("X2") + " ");
                    }
                    else
                    {
                        strB.Append(bytes[i].ToString("X2"));
                    }
                }
                hexString = strB.ToString().TrimEnd();
            }
            return hexString;
        }

        /// <summary>
        /// 十六进制字符串转字节数组，如："AE 00 CF" => [0xae,0x00,0xcf]
        /// </summary>
        /// <param name="data">源十六进制字符串</param>
        /// <param name="bytes">目标字节数组</param>
        /// <param name="maxLen">最大长度</param>
        /// <returns>字节数组长度</returns>
        public static int ToByteArray(this string data, ref byte[] bytes, int maxLen)
        {
            string[] dataArray = data.Split(' ');
            for (int i = 0; (i < maxLen) && (i < dataArray.Length); i++)
            {
                bytes[i] = Convert.ToByte(dataArray[i].Substring(0, 2), 16);
            }
            return dataArray.Length;
        }

        /// <summary>
        /// 十六进制字符串转字节数组，如："AE 00 CF" => [0xae,0x00,0xcf]
        /// </summary>
        /// <param name="data">源十六进制字符串</param>
        /// <param name="maxLen">最大长度</param>
        /// <returns>字节数组</returns>
        public static byte[] ToByteArray(this string data, int maxLen)
        {
            byte[] bytes = new byte[maxLen];
            string[] dataArray = data.Split(' ');
            for (int i = 0; (i < maxLen) && (i < dataArray.Length); i++)
            {
                bytes[i] = Convert.ToByte(dataArray[i].Substring(0, 2), 16);
            }
            return bytes;
        }

        /// <summary>
        /// 十六进制字符串转字节数组，如："0x725" => [0x07,0x25]
        /// </summary>
        /// <param name="data">源字符串，如：0x725</param>
        /// <returns>字节数组</returns>
        public static byte[] ToCanIdByteArray(this string data)
        {
            byte[] bytes = new byte[2];
            Int16 i16 = Convert.ToInt16(data, 16);
            bytes = BitConverter.GetBytes(i16).Reverse().ToArray();
            return bytes;
        }

        /// <summary>
        /// 获取该字节的bit位是0还是1
        /// <para>index：bit位，从低到高1-8</para>
        /// </summary>
        /// <param name="input">该字节</param>
        /// <param name="index">bit位，从低到高1-8</param>
        /// <returns></returns>
        public static int GetBitValue(byte input, int index)
        {
            if (index > 8 || index < 1)
                index = 0;
            else index = index - 1;
            return ((input & (1 << index)) > 0) ? 1 : 0;
        }

        /// <summary>
        /// 设置该字节的某一位的值(将该位设置成0或1)
        /// <para>flag：要设置的值 true(1) / false(0)</para>
        /// </summary>
        /// <param name="data">要设置的字节byte</param>
        /// <param name="index">要设置的位， 值从低到高为 1-8</param>
        /// <param name="flag">要设置的值 true(1) / false(0)</param>
        /// <returns></returns>
        public static byte SetBitValue(byte data, int index, bool flag)
        {
            if (index > 8 || index < 1)
                throw new ArgumentOutOfRangeException();
            int v = index < 2 ? index : (2 << (index - 2));
            return flag ? (byte)(data | v) : (byte)(data & ~v);
        }

        /// <summary>
        /// 比较源字节数组baseBytes是否包含目标字节数组subbytes，连续比较
        /// </summary>
        /// <param name="basebytes">源字节数组</param>
        /// <param name="subbytes">目标字节数组</param>
        /// <returns>是否包含</returns>
        public static bool Contain(byte[] basebytes, byte[] subbytes)
        {
            if (basebytes == null)
                return false;
            int baseNums = basebytes.Length;
            int subNums = subbytes.Length;
            bool ret = false;
            int lxsamenums = 0;
            for (int i = 0; i < baseNums; i++)
            {
                if (baseNums - i >= subNums)
                {
                    for (int j = 0; j < subNums; j++)
                    {
                        if (basebytes[i + j] == subbytes[j])
                            lxsamenums++;
                        else
                            lxsamenums = 0;
                    }
                    if (lxsamenums == subNums)
                    {
                        ret = true;
                        break;
                    }
                }
                else
                    break;
            }
            return ret;
        }
    }
}