﻿// See https://aka.ms/new-console-template for more information
using System.Numerics;
using System.Collections;
using System.Dynamic;
using C4BCodesLibrary.C4BCodes.Utilities;
using System.Xml.Linq;

Console.WriteLine("Hello, World!");

string? palindrome = Console.ReadLine();
string? reverse = palindrome?.Replace(" ", "");
reverse = reverse?.ToLower();
char[]? tmp = reverse?.ToCharArray();
//Array.Reverse(tmp);
Console.WriteLine(palindrome == new string(tmp));

const int size = 64;
ulong mask = 1UL << size - 1;
Console.WriteLine(Convert.ToString(123, 2));

A classA = null;
int[] aaa = new int[0];
Console.WriteLine(aaa is null);
//Console.WriteLine(aaa[0]);

List<int> list = new List<int>();
list.Add(1);
list.Add(2);
list.Add(3);
list.Add(2);
List<int> results = list.FindAll(Even);
foreach (int num in results)
{
    Console.WriteLine(num);
}
dynamic person = DynamicXmlHelper.Parse(
    @"<Person>
          <FirstName>Liang</FirstName>
          <LastName>Yange</LastName>
          <Something>
            <S1>S1Value</S1>
            <S2>S2Value</S2>
          </Something>
      </Person>");
Console.WriteLine($"{person.FirstName} {person.LastName} {person.Something.S1}");

static bool Even(int value) => value % 2 == 0;

public class A
{
    public string a { get; set; }
    public int b { get; set; }
}


