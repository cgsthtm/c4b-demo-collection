﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Serilog;

namespace C4B.Oracle.Demo
{
    // 参考：https://www.oracle.com/tools/technologies/quickstart-dotnet-for-oracle-database.html#fourth-option-tab
    internal class Program
    {
        // Prerequisite: This app assumes the user has already been created with the necessary privileges
        // Set the demo user id and password
        public static string user = "SYSTEM";
        public static string pwd = "Admin1234";

        // Set the net service name, Easy connect, or connect descriptor of the pluggable DB, such as "localhost/XEPDB1" for 18c or higher
        public static string db = "localhost/XE";

        static void Main(string[] args)
        {
            var log = new LoggerConfiguration().WriteTo.Console().CreateLogger();

            string conStringUser = "User Id=" + user + ";Password=" + pwd + ";Data Source=" + db + ";";
            using (OracleConnection con = new OracleConnection(conStringUser))
            {
                using (OracleCommand cmd = con.CreateCommand())
                {
                    try
                    {
                        con.Open();
                        log.Information("Successfully connected to Oracle Database as {0}", user);

                        // Retrieve sample data
                        cmd.CommandText = "SELECT DESCRIPTION,DONE FROM TODOITEM";
                        OracleDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            if (reader.GetBoolean(1))
                                log.Information("{0} is done.", reader.GetString(0));
                            else
                                log.Information("{0} is NOT done.", reader.GetString(0));
                        }
                        reader.Dispose();
                    }
                    catch (Exception ex)
                    {
                        log.Information("{0}", ex.Message);
                    }
                }
            }

            Console.ReadLine();
        }
    }
}
