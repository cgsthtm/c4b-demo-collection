﻿// <copyright file="Form1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo
{
    using C4B.DXApplication.Demo.XPOTutorials;
    using DevExpress.XtraBars;
    using DevExpress.XtraBars.Docking2010.Views;
    using DevExpress.XtraBars.Navigation;
    using DevExpress.XtraEditors;

    /// <summary>
    /// Form1.
    /// </summary>
    public partial class Form1 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Form1"/> class.
        /// </summary>
        public Form1()
        {
            this.InitializeComponent();
            this.AccordionControl.SelectedElement = this.ConnectDataGridToXPOObjectAccordionControlElement;
        }

        private void AccordionControl_SelectedElementChanged(object sender, SelectedElementChangedEventArgs e)
        {
            XtraForm userControl = new XtraForm();
            if (e.Element == null)
            {
                return;
            }

            switch (e.Element.Text)
            {
                case "ConnectDataGridToXPOObject":
                    userControl = new CustomersListForm();
                    break;

                default:
                    break;
            }

            this.TabbedView.AddDocument(userControl);
            this.TabbedView.ActivateDocument(userControl);
        }

        private void BarButtonNavigation_ItemClick(object sender, ItemClickEventArgs e)
        {
            int barItemIndex = this.BarSubItemNavigation.ItemLinks.IndexOf(e.Link);
            this.AccordionControl.SelectedElement = this.AccordionControl.Elements[barItemIndex];
        }

        private void TabbedView_DocumentClosed(object sender, DocumentEventArgs e)
        {
            this.SetAccordionSelectedElement(e);
        }

        private void SetAccordionSelectedElement(DocumentEventArgs e)
        {
            if (this.TabbedView.Documents.Count != 0)
            {
                switch (e.Document.Caption)
                {
                    case "ConnectDataGridToXPOObject":
                        this.AccordionControl.SelectedElement = this.ConnectDataGridToXPOObjectAccordionControlElement;
                        break;

                    default:
                        break;
                }
            }
            else
            {
                this.AccordionControl.SelectedElement = null;
            }
        }
    }
}