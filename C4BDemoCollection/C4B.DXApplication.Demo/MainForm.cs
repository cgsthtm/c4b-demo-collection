﻿// <copyright file="MainForm.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo
{
    using System;
    using System.Drawing;
    using System.Linq;
    using C4B.DXApplication.Demo.XPOTutorials;
    using DevExpress.XtraBars;
    using DevExpress.XtraBars.Docking2010.Views;
    using DevExpress.XtraEditors;

    /// <summary>
    /// MainForm.
    /// </summary>
    public partial class MainForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainForm"/> class.
        /// </summary>
        public MainForm()
        {
            this.InitializeComponent();

            this.TabbedView.DocumentActivated += this.TabbedView_DocumentActivated;
            this.TabbedView.DocumentClosed += this.TabbedView_DocumentClosed;
            this.TabbedView.QueryControl += this.TabbedView_QueryControl;
            this.CustomersAccordionControlElement.Tag = nameof(CustomersListForm);
            this.OrdersAccordionControlElement.Tag = nameof(OrdersListForm);
            ////this.ActivateDocument("Customers", nameof(CustomersListForm));
            this.ActivateDocument("Orders", nameof(OrdersListForm));
        }

        private void ActivateDocument(string caption, string controlName)
        {
            BaseDocument document = this.TabbedView.Documents.FindFirst(d => d.ControlName == controlName);
            if (document == null)
            {
                document = this.TabbedView.AddDocument(caption, controlName);
            }

            this.TabbedView.Controller.Activate(document);
        }

        private void TabbedView_QueryControl(object sender, DevExpress.XtraBars.Docking2010.Views.QueryControlEventArgs e)
        {
            switch (e.Document.ControlName)
            {
                case nameof(CustomersListForm):
                    e.Control = new CustomersListForm();
                    break;

                case nameof(OrdersListForm):
                    e.Control = new OrdersListForm();
                    break;

                default:
                    throw new ArgumentException($"Unknown control name {e.Document.ControlName}");
            }
        }

        private void TabbedView_DocumentClosed(object sender, DocumentEventArgs e)
        {
            if (this.TabbedView.Documents.Count == 0)
            {
                this.AccordionControl.SelectedElement = null;
            }
        }

        private void TabbedView_DocumentActivated(object sender, DocumentEventArgs e)
        {
            this.AccordionControl.SelectedElement = this.AccordionControl.GetElements()
                .Single(t => (string)t.Tag == e.Document.ControlName);
            if (this.RibbonControl.MergedPages.Count > 0)
            {
                this.RibbonControl.SelectedPage = this.RibbonControl.MergedPages[0];
            }
        }

        private XtraUserControl CreateUserControl(string text)
        {
            XtraUserControl result = new XtraUserControl();
            result.Name = text.ToLower() + "UserControl";
            result.Text = text;
            LabelControl label = new LabelControl();
            label.Parent = result;
            label.Appearance.Font = new Font("Tahoma", 25.25F);
            label.Appearance.ForeColor = Color.Gray;
            label.Dock = System.Windows.Forms.DockStyle.Fill;
            label.AutoSizeMode = LabelAutoSizeMode.None;
            label.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            label.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            label.Text = text;
            return result;
        }

        private void CustomersAccordionControlElement_Click(object sender, EventArgs e)
        {
            this.ActivateDocument("Customers", nameof(CustomersListForm));
        }

        private void OrdersAccordionControlElement_Click(object sender, EventArgs e)
        {
            this.ActivateDocument("Orders", nameof(OrdersListForm));
        }

        private void CustomersBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.ActivateDocument("Customers", nameof(CustomersListForm));
        }

        private void OrdersBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.ActivateDocument("Orders", nameof(OrdersListForm));
        }
    }
}