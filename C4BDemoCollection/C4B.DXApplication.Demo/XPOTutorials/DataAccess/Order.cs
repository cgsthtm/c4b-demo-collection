﻿// <copyright file="Order.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.DataAccess
{
    using System;
    using DevExpress.Xpo;

    /// <summary>
    /// 订单.
    /// </summary>
    public class Order : XPObject
    {
        private string fProductName;
        private DateTime fOrderDate;
        private decimal fFreight;
        private Customer fCustomer;

        /// <summary>
        /// Initializes a new instance of the <see cref="Order"/> class.
        /// </summary>
        /// <param name="session">会.</param>
        public Order(Session session)
            : base(session)
        {
        }

        /*The maximum number of characters for a column which XPO creates
         * for a property of the String type is 100 (SizeAttribute.DefaultStringMappingFieldSize).
         * Use the Size attribute to change the maximum number of characters for some properties.
         */

        /// <summary>
        /// Gets or sets 产品名称.
        /// </summary>
        [Size(200)]
        public string ProductName
        {
            get { return this.fProductName; }
            set { this.SetPropertyValue(nameof(this.ProductName), ref this.fProductName, value); }
        }

        /// <summary>
        /// Gets or sets 订单日期.
        /// </summary>
        public DateTime OrderDate
        {
            get { return this.fOrderDate; }
            set { this.SetPropertyValue(nameof(this.OrderDate), ref this.fOrderDate, value); }
        }

        /// <summary>
        /// Gets or sets 货运.
        /// </summary>
        public decimal Freight
        {
            get { return this.fFreight; }
            set { this.SetPropertyValue(nameof(this.Freight), ref this.fFreight, value); }
        }

        // Add the Orders and Customer properties to create a relationship between the Customer and Order classes.

        /// <summary>
        /// Gets or sets 客户.
        /// </summary>
        [Association("CustomerOrders")]
        public Customer Customer
        {
            get { return this.fCustomer; }
            set { this.SetPropertyValue(nameof(this.Customer), ref this.fCustomer, value); }
        }
    }
}