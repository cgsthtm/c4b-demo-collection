﻿// <copyright file="ConnectionHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.DataAccess
{
    using System;
    using System.Configuration;
    using DevExpress.Xpo;
    using DevExpress.Xpo.DB;
    using DevExpress.Xpo.Metadata;

    /// <summary>
    /// ConnectionHelper.
    /// </summary>
    public static class ConnectionHelper
    {
        private static readonly Type[] PersistentTypes = new Type[]
        {
            typeof(Order),
            typeof(Customer),
        };

        /// <summary>
        /// 链接数据层.
        /// </summary>
        /// <param name="threadSafe">是否线程安全.</param>
        public static void Connect(bool threadSafe = true)
        {
            XpoDefault.DataLayer = CreateDataLayer(threadSafe);
        }

        private static IDataLayer CreateDataLayer(bool threadSafe)
        {
            string connStr = ConfigurationManager.ConnectionStrings["XpoTutorial"].ConnectionString;

            // connStr = XpoDefault.GetConnectionPoolString(connStr);  // Uncomment this line if you use a database server like SQL Server, Oracle, PostgreSql etc.
            ReflectionDictionary dictionary = new ReflectionDictionary();
            dictionary.GetDataStoreSchema(PersistentTypes);   // Pass all of your persistent object types to this method.

            // Use AutoCreateOption.DatabaseAndSchema if the database or tables do not exist.
            // Use AutoCreateOption.SchemaAlreadyExists if the database already exists.
            AutoCreateOption autoCreateOption = AutoCreateOption.DatabaseAndSchema;
            IDataStore provider = XpoDefault.GetConnectionProvider(connStr, autoCreateOption);
            return threadSafe ? (IDataLayer)new ThreadSafeDataLayer(dictionary, provider) : new SimpleDataLayer(dictionary, provider);
        }
    }
}