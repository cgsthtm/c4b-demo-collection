﻿// <copyright file="Customer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.DataAccess
{
    using DevExpress.Xpo;

    /// <summary>
    /// 客户实体类.
    /// </summary>
    public class Customer : XPObject
    {
        private string fFirstName;
        private string fLastName;

        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// </summary>
        /// <param name="session">Session.</param>
        public Customer(Session session)
            : base(session)
        {
        }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName
        {
            get { return this.fFirstName; }

            // The SetPropertyValue method raises the PropertyChanged event that is used by data-bound controls to update their display text.
            set { this.SetPropertyValue(nameof(this.FirstName), ref this.fFirstName, value); }
        }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName
        {
            get { return this.fLastName; }
            set { this.SetPropertyValue(nameof(this.LastName), ref this.fLastName, value); }
        }

        // XPO does not map read-only properties to database columns.

        /// <summary>
        /// Gets ContactName.
        /// </summary>
        public string ContactName
        {
            get { return string.Concat(this.FirstName, " ", this.LastName); }
        }

        /*You can calculate the property value directly (as shown above) or use the EvaluateAlias method.
         * Both approaches are correct. The first approach is preferable for heavy calculations
         * because C# or VB.NET provides many ways for performance optimization.
         */
        ////[PersistentAlias("Concat([FirstName], ' ', [LastName])")]
        ////public string ContactName
        ////{
        ////    get { return (string)EvaluateAlias(nameof(ContactName)); }
        ////}

        // Add the Orders and Customer properties to create a relationship between the Customer and Order classes.

        /// <summary>
        /// Gets 订单.
        /// </summary>
        [Association("CustomerOrders")]
        public XPCollection<Order> Orders
        {
            get { return this.GetCollection<Order>(nameof(this.Orders)); }
        }
    }
}