﻿// <copyright file="EditCustomerForm.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials
{
    using System;
    using System.Windows.Forms;
    using C4B.DXApplication.Demo.XPOTutorials.DataAccess;
    using DevExpress.Xpo;
    using DevExpress.Xpo.DB.Exceptions;
    using DevExpress.XtraBars;
    using DevExpress.XtraEditors;

    /// <summary>
    /// EditCustomerForm.
    /// </summary>
    public partial class EditCustomerForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditCustomerForm"/> class.
        /// </summary>
        public EditCustomerForm()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditCustomerForm"/> class.
        /// </summary>
        /// <param name="customerId">顾客ID.</param>
        public EditCustomerForm(int? customerId)
            : this()
        {
            this.CustomerID = customerId;
        }

        /// <summary>
        /// Gets 顾客ID.
        /// </summary>
        public int? CustomerID { get; private set; }

        /// <summary>
        /// Gets UnitOfWork.
        /// </summary>
        protected UnitOfWork UnitOfWork { get; private set; }

        private void EditCustomerForm_Load(object sender, EventArgs e)
        {
            this.Reload();
        }

        private void Reload()
        {
            this.UnitOfWork = new UnitOfWork();
            if (this.CustomerID.HasValue)
            {
                this.CustomerBindingSource.DataSource = this.UnitOfWork.GetObjectByKey<Customer>(this.CustomerID.Value);
            }
            else
            {
                this.CustomerBindingSource.DataSource = new Customer(this.UnitOfWork);
            }
        }

        private void BtnSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                this.UnitOfWork.CommitChanges();

                // A new object does not have an identifier initially - XPO assigns a value to it from the auto-incremented key column
                // when a new object is saved to the database.
                this.CustomerID = ((Customer)this.CustomerBindingSource.DataSource).Oid;
                this.Close();
            }
            catch (LockingException)
            {
                XtraMessageBox.Show(this, "The record was modified or deleted. Click Reload and try again.", "XPO Tutorial", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void BtnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void BtnReload_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Reload();
        }
    }
}