﻿// <copyright file="OrdersListForm.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials
{
    using System;
    using System.Windows.Forms;
    using C4B.DXApplication.Demo.XPOTutorials.DataAccess;
    using DevExpress.Xpo;
    using DevExpress.XtraBars.Docking2010;

    /// <summary>
    /// https://github.com/DevExpress/XPO/blob/master/Tutorials/WinForms/Classic/bind-the-data-grid-to-large-data-source.md
    /// </summary>
    public partial class OrdersListForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersListForm"/> class.
        /// </summary>
        public OrdersListForm()
        {
            this.InitializeComponent();
        }

        private void ConnectDataGridToXPOObjectsForm_Load(object sender, EventArgs e)
        {
            this.Reload();
        }

        private void Reload()
        {
            this.OrdersInstantFeedbackView.Refresh();
        }

        private void ShowEditForm(int? orderID)
        {
            var form = new EditOrderForm(orderID);
            form.FormClosed += this.EditFormClosed;
            var documentManager = DocumentManager.FromControl(this.MdiParent);
            if (documentManager != null)
            {
                documentManager.View.AddDocument(form);
            }
            else
            {
                try
                {
                    form.ShowDialog();
                }
                finally
                {
                    form.Dispose();
                }
            }
        }

        private void EditFormClosed(object sender, FormClosedEventArgs e)
        {
            var form = (EditOrderForm)sender;
            form.FormClosed -= this.EditFormClosed;
            if (form.OrderID.HasValue)
            {
                this.Reload();
                this.OrdersGridView.FocusedRowHandle = this.OrdersGridView.LocateByValue("Oid", form.OrderID.Value);
            }
        }

        private void BtnNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.ShowEditForm(null);
        }

        private void BtnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (Session session = new Session())
            {
                object orderId = this.OrdersGridView.GetFocusedRowCellValue(this.colOid);
                Order order = session.GetObjectByKey<Order>(orderId);
                session.Delete(order);
            }

            this.Reload();
        }

        private void OrdersGridView_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (e.Clicks == 2)
            {
                e.Handled = true;
                int orderID = (int)this.OrdersGridView.GetRowCellValue(e.RowHandle, this.colOid);
                this.ShowEditForm(orderID);
            }
        }

        private void OrdersInstantFeedbackView_ResolveSession(object sender, ResolveSessionEventArgs e)
        {
            e.Session = new Session();
        }

        private void OrdersInstantFeedbackView_DismissSession(object sender, ResolveSessionEventArgs e)
        {
            e.Session.Session.Dispose();
        }
    }
}