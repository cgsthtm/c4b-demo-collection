﻿// <copyright file="EditOrderForm.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials
{
    using System;
    using System.Windows.Forms;
    using C4B.DXApplication.Demo.XPOTutorials.DataAccess;
    using DevExpress.Xpo;
    using DevExpress.Xpo.DB.Exceptions;
    using DevExpress.XtraBars;
    using DevExpress.XtraEditors;

    /// <summary>
    /// https://github.com/DevExpress/XPO/blob/master/Tutorials/WinForms/Classic/bind-the-data-grid-to-large-data-source.md .
    /// </summary>
    public partial class EditOrderForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditOrderForm"/> class.
        /// </summary>
        public EditOrderForm()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditOrderForm"/> class.
        /// </summary>
        /// <param name="orderId">订单ID.</param>
        public EditOrderForm(int? orderId)
            : this()
        {
            this.OrderID = orderId;
        }

        /// <summary>
        /// Gets 订单ID.
        /// </summary>
        public int? OrderID { get; private set; }

        /// <summary>
        /// Gets UnitOfWork.
        /// </summary>
        protected UnitOfWork UnitOfWork { get; private set; }

        private void EditCustomerForm_Load(object sender, EventArgs e)
        {
            this.Reload();
        }

        private void Reload()
        {
            this.UnitOfWork = new UnitOfWork();
            if (this.OrderID.HasValue)
            {
                this.OrderBindingSource.DataSource = this.UnitOfWork.GetObjectByKey<Order>(this.OrderID.Value);
            }
            else
            {
                this.OrderBindingSource.DataSource = new Order(this.UnitOfWork);
            }

            this.CustomersBindingSource.DataSource = new XPCollection<Customer>(this.UnitOfWork);
        }

        private void BtnSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                this.UnitOfWork.CommitChanges();

                // A new object does not have an identifier initially - XPO assigns a value to it from the auto-incremented key column
                // when a new object is saved to the database.
                this.OrderID = ((Order)this.OrderBindingSource.DataSource).Oid;
                this.Close();
            }
            catch (LockingException)
            {
                XtraMessageBox.Show(this, "The record was modified or deleted. Click Reload and try again.", "XPO Tutorial", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void BtnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void BtnReload_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Reload();
        }
    }
}