﻿// <copyright file="ShareConnectionDefaultDataAccessLayer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.ConnectToADataStore
{
    using System;
    using System.Linq;
    using C4B.DXApplication.Demo.XPOTutorials.ConnectToADataStore.Models;
    using DevExpress.Xpo;

    /// <summary>
    /// 共享连接-默认数据访问层.
    /// </summary>
    public static class ShareConnectionDefaultDataAccessLayer
    {
        /// <summary>
        /// Func1 Demo.
        /// </summary>
        public static void Func1()
        {
            // Connect to an in-memory source
            const string connectionString = @"XpoProvider=InMemoryDataStore;Data Source=.\SpecifyConnectionSettings.xml;Read Only=false";

            XpoDefault.DataLayer = XpoDefault.GetDataLayer(connectionString, DevExpress.Xpo.DB.AutoCreateOption.DatabaseAndSchema);

            // Create and save a new data object
            // This Unit of Work uses a default Data Access Layer
            using (var uow = new UnitOfWork())
            {
                var contact = new Contact(uow);
                contact.FirstName = "Alice";
                contact.LastName = "Smith";
                uow.CommitChanges();
            }

            // Read the new data object
            // This Unit of Work uses a default Data Access Layer
            using (var uow = new UnitOfWork())
            {
                var contact = uow.Query<Contact>().FirstOrDefault(c => c.LastName == "Smith");
                Console.WriteLine(contact.FirstName + " " + contact.LastName);
            }
        }
    }
}