﻿// <copyright file="EstablishConnection.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.ConnectToADataStore
{
    using System;
    using System.Linq;
    using C4B.DXApplication.Demo.XPOTutorials.ConnectToADataStore.Models;
    using DevExpress.Xpo;
    using DevExpress.Xpo.DB;
    using DevExpress.Xpo.Metadata;

    /// <summary>
    /// 建立连接.
    /// </summary>
    public static class EstablishConnection
    {
        /// <summary>
        /// 连接字符串1.
        /// </summary>
        public const string ConnectionString1 = @"XpoProvider=InMemoryDataStore;Data Source=.\SpecifyConnectionSettings.xml;Read Only=false";

        /// <summary>
        /// 连接字符串2.
        /// </summary>
        public const string ConnectionString2 = @"XpoProvider=InMemoryDataStore;Data Source=.\EstablishConnection.xml;Read Only=false";
        private static IDisposable[] objectsToDisposeOnDisconnect;
        private static Lazy<XPDictionary> sharedDictionary = new Lazy<XPDictionary>(CreateSharedDictionary);

        /// <summary>
        /// 更改连接.
        /// </summary>
        /// <param name="connectionString">连接字符串.</param>
        public static void ChangeConnection(string connectionString)
        {
            if (objectsToDisposeOnDisconnect != null)
            {
                foreach (IDisposable toDispose in objectsToDisposeOnDisconnect)
                {
                    toDispose.Dispose();
                }
            }

            XpoDefault.DataLayer = XpoDefault.GetDataLayer(connectionString, sharedDictionary.Value, AutoCreateOption.SchemaAlreadyExists, out objectsToDisposeOnDisconnect);
        }

        /// <summary>
        /// Func1.
        /// </summary>
        public static void Func1()
        {
            // Read the new data object
            using (var uow = new UnitOfWork())
            {
                var contact = uow.Query<Contact>().FirstOrDefault(c => c.LastName == "Smith");
                Console.WriteLine(contact.FirstName + " " + contact.LastName);
            }
        }

        /// <summary>
        /// Func1.
        /// </summary>
        public static void Func2()
        {
            // Read the new data object
            using (var uow = new UnitOfWork())
            {
                var contact = uow.Query<Contact>().FirstOrDefault(c => c.LastName == "Smith2");
                Console.WriteLine(contact.FirstName + " " + contact.LastName);
            }
        }

        private static XPDictionary CreateSharedDictionary()
        {
            XPDictionary result = new ReflectionDictionary();
            result.GetDataStoreSchema(typeof(Contact).Assembly);
            return result;
        }
    }
}
