﻿// <copyright file="EstablishConnection2.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.ConnectToADataStore
{
    using System;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Linq;
    using C4B.DXApplication.Demo.XPOTutorials.DataAccess;
    using DevExpress.Xpo;
    using DevExpress.Xpo.DB;
    using DevExpress.Xpo.Metadata;

    /// <summary>
    /// 建立连接.If you want to control the connection state (open/closed), .
    /// create an IDbConnection object and use a constructor to pass the object to a Data Store Provider.
    /// </summary>
    public static class EstablishConnection2
    {
        /// <summary>
        /// Gets SqlConnection.
        /// </summary>
        public static SqlConnection Connection { get; private set; }

        /// <summary>
        /// 连接数据层.
        /// </summary>
        /// <param name="connectionString">连接字符串.</param>
        public static void Connect(string connectionString)
        {
            Connection = new SqlConnection(connectionString);
            IDataStore dataStoreProvider = new MSSqlConnectionProvider(Connection, AutoCreateOption.SchemaAlreadyExists);
            XpoDefault.DataLayer = new SimpleDataLayer(dataStoreProvider);
        }

        /// <summary>
        /// Func1.
        /// </summary>
        public static void Func1()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var customer = uow.Query<Customer>().FirstOrDefault();
                Console.WriteLine(customer.FirstName + " " + customer.LastName);
            }
        }
    }
}
