﻿// <copyright file="Contact.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.ConnectToADataStore.Models
{
    using DevExpress.Xpo;

    /// <summary>
    /// 熟人.
    /// </summary>
    public class Contact : XPObject
    {
        private string fFirstName;
        private string fLastName;

        /// <summary>
        /// Initializes a new instance of the <see cref="Contact"/> class.
        /// </summary>
        /// <param name="session">Session.</param>
        public Contact(Session session)
            : base(session)
        {
        }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName
        {
            get { return this.fFirstName; }
            set { this.SetPropertyValue(nameof(this.FirstName), ref this.fFirstName, value); }
        }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName
        {
            get { return this.fLastName; }
            set { this.SetPropertyValue(nameof(this.LastName), ref this.fLastName, value); }
        }
    }
}
