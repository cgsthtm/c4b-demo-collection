﻿// <copyright file="UsingSqlDependency.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.ConnectToADataStore.CachedDataStore
{
    using System;
    using System.Configuration;
    using System.Linq;
    using C4B.DXApplication.Demo.XPOTutorials.DataAccess;
    using DevExpress.Xpo;
    using DevExpress.Xpo.DB;
    using DevExpress.Xpo.DB.Helpers;
    using DevExpress.Xpo.Metadata;

    /// <summary>
    /// UsingSqlDependency.
    /// </summary>
    public static class UsingSqlDependency
    {
        /// <summary>
        /// Func1.
        /// </summary>
        public static void Func1()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQLConnectionString"].ConnectionString;
            IDisposable[] disposeOnDisconnect;

            // Create the provider
            IDataStore node = MSSql2005SqlDependencyCacheRoot.CreateProviderFromString_WithCache(
                connectionString, AutoCreateOption.DatabaseAndSchema, out disposeOnDisconnect);

            // Create the dictionary
            XPDictionary dict = new ReflectionDictionary();
            dict.GetDataStoreSchema(typeof(Customer).Assembly);

            // Create the data layer
            XpoDefault.DataLayer = new ThreadSafeDataLayer(dict, node);

            // Print the result
            using (Session session = new Session())
            {
                var customer = session.Query<Customer>().FirstOrDefault();
                Console.WriteLine(customer.FirstName + " " + customer.LastName);
            }
        }
    }
}