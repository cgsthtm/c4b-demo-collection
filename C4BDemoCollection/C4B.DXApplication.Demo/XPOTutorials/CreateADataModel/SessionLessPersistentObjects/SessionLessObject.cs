﻿// <copyright file="SessionLessObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.CreateADataModel.SessionLessPersistentObjects
{
    using DevExpress.Xpo;

    /// <summary>
    /// SessionLessObject.
    /// </summary>
    [Persistent("Contact")]
    public class SessionLessObject : XPBaseObject
    {
        private string firstName = string.Empty;

        /// <summary>
        /// The lastName field values are stored in the LastName database column of the string type with max length 254.
        /// </summary>
        [Persistent("LastName")]
        [Size(254)]
        private string lastName = string.Empty;
        private int persistentID;

        /// <summary>
        /// Gets or sets FirstName.
        /// The FirstName property is not persisted.
        /// </summary>
        [NonPersistent]
        public string FirstName
        {
            get { return this.firstName; }
            set { this.firstName = value; }
        }

        /// <summary>
        /// Gets or sets LastName.
        /// The LastName property is associated with the lastName field which will be persisted.
        /// </summary>
        [PersistentAlias(nameof(lastName))]
        public string LastName
        {
            get { return this.lastName; }
            set { this.lastName = value; }
        }

        /// <summary>
        /// Gets FullName.
        /// The FullName readonly property values are stored .
        /// in the FullName database column of the string type with max length 254.
        /// </summary>
        [Persistent("FullName")]
        [Size(254)]
        public string FullName
        {
            get { return string.Format("{0}, {1}", this.lastName, this.firstName); }
        }

        /// <summary>
        /// Gets or sets Oid.
        /// Implementing an Object Identity Value.
        /// </summary>
        [Key(AutoGenerate = true)]
        public int Oid
        {
            get
            {
                return this.persistentID;
            }

            set
            {
                this.persistentID = value;
                this.OnChanged(nameof(this.Oid));
            }
        }
    }
}
