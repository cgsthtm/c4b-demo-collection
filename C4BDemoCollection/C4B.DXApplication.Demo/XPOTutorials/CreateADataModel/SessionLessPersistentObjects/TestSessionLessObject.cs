﻿// <copyright file="TestSessionLessObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.CreateADataModel.SessionLessPersistentObjects
{
    using DevExpress.Xpo;

    /// <summary>
    /// TestSessionLessObject.
    /// </summary>
    public static class TestSessionLessObject
    {
        /// <summary>
        /// Func1.
        /// </summary>
        public static void Func1()
        {
            SessionLessObject contact = new SessionLessObject();
            contact.FirstName = "John";
            contact.LastName = "Doe";
            Session.DefaultSession.Save(contact);

            // ...
            Session.DefaultSession.Reload(contact);

            // ...
            Session.DefaultSession.Delete(contact);
        }
    }
}
