﻿// <copyright file="Payment.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.CreateADataModel
{
    using DevExpress.Xpo;

    /// <summary>
    /// Payment.
    /// </summary>
    public class Payment : XPObject
    {
        private double rate;
        private double hours;

        /// <summary>
        /// Initializes a new instance of the <see cref="Payment"/> class.
        /// </summary>
        /// <param name="session">Session.</param>
        public Payment(Session session)
            : base(session)
        {
        }

        /// <summary>
        /// Gets or sets Rate.
        /// </summary>
        public double Rate
        {
            get
            {
                return this.rate;
            }

            set
            {
                if (this.SetPropertyValue(nameof(this.Rate), ref this.rate, value))
                {
                    // Fires the PropertyChanged event for the Amount property
                    this.OnChanged(nameof(this.Amount));
                }
            }
        }

        /// <summary>
        /// Gets or sets Hours.
        /// </summary>
        public double Hours
        {
            get
            {
                return this.hours;
            }

            set
            {
                if (this.SetPropertyValue(nameof(this.Hours), ref this.hours, value))
                {
                    // Fires the PropertyChanged event for the Amount property
                    this.OnChanged(nameof(this.Amount));
                }
            }
        }

        /// <summary>
        /// Gets Amount.
        /// </summary>
        [PersistentAlias("Rate * Hours")]
        public double Amount
        {
            get
            {
                object tempObject = this.EvaluateAlias(nameof(this.Amount));
                if (tempObject != null)
                {
                    return (double)tempObject;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}