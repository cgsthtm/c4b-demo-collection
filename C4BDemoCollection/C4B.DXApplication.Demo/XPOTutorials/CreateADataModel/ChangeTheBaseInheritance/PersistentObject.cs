﻿// <copyright file="PersistentObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.CreateADataModel.ChangeTheBaseInheritance
{
    using System;
    using System.Collections;
    using DevExpress.Xpo;
    using DevExpress.Xpo.Exceptions;
    using DevExpress.Xpo.Helpers;
    using DevExpress.Xpo.Metadata;

    /// <summary>
    /// PersistentObject.
    /// Changing the Base Inheritance.
    /// </summary>
    public class PersistentObject : XPBaseObject, IXPObject, IComparable
    {
        private Session session;
        private XPClassInfo classInfo;
        private int persistentID;
        private bool changed = false;
        private bool isLoading;
        private IDictionary collections;

        /// <summary>
        /// Initializes a new instance of the <see cref="PersistentObject"/> class.
        /// Providing a Session-Specific Constructor.
        /// </summary>
        /// <param name="session">Session.</param>
        public PersistentObject(Session session)
        {
            this.session = session;
            this.classInfo = session.Dictionary.GetClassInfo(this.GetType());
            if (!session.IsObjectsLoading)
            {
                this.AfterConstruction();
            }
        }

        /// <summary>
        /// ObjectChangeEventHandler.
        /// </summary>
        public new event ObjectChangeEventHandler Changed;

        /// <summary>
        /// Gets or sets Oid.
        /// Implementing an Object Identity Value.
        /// </summary>
        public int Oid
        {
            get
            {
                return this.persistentID;
            }

            set
            {
                this.persistentID = value;
                this.OnChanged(nameof(this.Oid));
            }
        }

        /// <summary>
        /// Gets Session.
        /// </summary>
        public new Session Session
        {
            get { return this.session; }
        }

        /// <summary>
        /// Gets ClassInfo.
        /// </summary>
        public new XPClassInfo ClassInfo
        {
            get { return this.classInfo; }
        }

        /// <summary>
        /// Gets DataLayer.
        /// </summary>
        public IDataLayer DataLayer
        {
            get { return this.session.DataLayer; }
        }

        /// <summary>
        /// Gets Dictionary.
        /// </summary>
        public XPDictionary Dictionary
        {
            get { return this.session.Dictionary; }
        }

        /// <summary>
        /// Gets a value indicating whether IsLoading.
        /// </summary>
        [NonPersistent]
        public new bool IsLoading
        {
            get { return this.isLoading; }
        }

        /// <summary>
        /// Gets This.
        /// Providing Lookup Editors Support.
        /// </summary>
        public new object This
        {
            get { return this; }
        }

        /// <summary>
        /// Gets ObjectLayer.
        /// Implementing the IObjectLayerProvider interface.
        /// </summary>
        IObjectLayer IObjectLayerProvider.ObjectLayer
        {
            get { return this.Session.ObjectLayer; }
        }

        /// <summary>
        /// Gets Collections.
        /// Adding Collection Properties.
        /// </summary>
        private IDictionary Collections
        {
            get
            {
                if (this.collections == null)
                {
                    this.collections = new System.Collections.Specialized.HybridDictionary();
                }

                return this.collections;
            }
        }

        /// <summary>
        /// Implementing General Persistent Object Manipulation.
        /// OnChanged.
        /// </summary>
        public new void OnChanged()
        {
            this.changed = true;
            if (!this.IsLoading)
            {
                this.RaiseChangeEvent(new ObjectChangeEventArgs(this.Session, this, ObjectChangeReason.Reset));
            }
        }

        /// <summary>
        /// Implementing General Persistent Object Manipulation.
        /// General persistent operations.Save.
        /// </summary>
        public new void Save()
        {
            this.Session.Save(this);
        }

        /// <summary>
        /// Implementing General Persistent Object Manipulation.
        /// General persistent operations.Delete.
        /// </summary>
        public new void Delete()
        {
            this.changed = true;
            this.Session.Delete(this);
            this.RaiseChangeEvent(new ObjectChangeEventArgs(this.Session, this, ObjectChangeReason.Delete));
        }

        /// <summary>
        /// Implementing General Persistent Object Manipulation.
        /// General persistent operations.Reload.
        /// </summary>
        public new void Reload()
        {
            this.Session.Reload(this);
            this.collections.Clear(); // Required for collection properties (see implementation below)
            this.RaiseChangeEvent(new ObjectChangeEventArgs(this.Session, this, ObjectChangeReason.Reset));
        }

        /// <summary>
        /// Implementing Persistent Object Comparisons.
        /// </summary>
        /// <param name="value">value.</param>
        /// <returns>int.</returns>
        int IComparable.CompareTo(object value)
        {
            return Comparer.Default.Compare(this.Session.GetKeyValue(this), this.Session.GetKeyValue(value as IXPSimpleObject));
        }

        /// <summary>
        /// Handling Changes in Persistent Objects.
        /// Implement additional logic after an object has been deleted.
        /// </summary>
        void IXPObject.OnDeleted()
        {
        }

        /// <summary>
        /// Handling Changes in Persistent Objects.
        /// Implement additional logic when an object is about to be deleted.
        /// </summary>
        void IXPObject.OnDeleting()
        {
        }

        /// <summary>
        /// Handling Changes in Persistent Objects.
        /// Implement additional logic after an object has been loaded.
        /// </summary>
        void IXPObject.OnLoaded()
        {
            this.isLoading = false;
        }

        /// <summary>
        /// Handling Changes in Persistent Objects.
        /// Implement additional logic before loading an object.
        /// </summary>
        void IXPObject.OnLoading()
        {
            this.isLoading = true;
        }

        /// <summary>
        /// Handling Changes in Persistent Objects.
        /// Implement additional logic after an object has been saved.
        /// </summary>
        void IXPObject.OnSaved()
        {
        }

        /// <summary>
        /// Handling Changes in Persistent Objects.
        /// Implement additional logic before saving an object.
        /// </summary>
        void IXPObject.OnSaving()
        {
        }

        /// <summary>
        /// Implementing General Persistent Object Manipulation.
        /// RaiseChangeEvent.
        /// </summary>
        /// <param name="args">ObjectChangeEventArgs.</param>
        protected new void RaiseChangeEvent(ObjectChangeEventArgs args)
        {
            if (this.Changed != null)
            {
                this.Changed(this, args);
            }
        }

        /////// <summary>
        /////// Implementing General Persistent Object Manipulation.
        /////// OnChanged.
        /////// </summary>
        /////// <param name="propertyName">propertyName.</param>
        ////protected new void OnChanged(string propertyName)
        ////{
        ////    this.changed = true;
        ////    if (!this.IsLoading)
        ////    {
        ////        this.RaiseChangeEvent(new ObjectChangeEventArgs(this.Session, this, ObjectChangeReason.PropertyChanged, propertyName, oldValue, newValue));
        ////    }
        ////}

        /// <summary>
        /// Adding Collection Properties.
        /// Retrieves a collection by the name of the property which exposes the collection.
        /// </summary>
        /// <param name="propertyName">propertyName.</param>
        /// <returns>XPCollection.</returns>
        protected new XPCollection GetCollection(string propertyName)
        {
            XPCollection result = this.Collections[propertyName] as XPCollection;
            if (result == null)
            {
                result = new XPCollection(this.Session, this, this.GetCollectionProperty(propertyName));
                this.Collections.Add(propertyName, result);
            }

            return result;
        }

        /// <summary>
        /// Implement additional construction logic here.
        /// </summary>
        protected new void AfterConstruction()
        {
            if (this.Session is UnitOfWork && this.ClassInfo.IsPersistent)
            {
                this.Session.Save(this);
            }
        }

        /// <summary>
        /// Adding Collection Properties.
        /// GetCollectionProperty.
        /// </summary>
        /// <param name="name">name</param>
        /// <returns>XPMemberInfo.</returns>
        /// <exception cref="PropertyMissingException">PropertyMissingException.</exception>
        private XPMemberInfo GetCollectionProperty(string name)
        {
            XPMemberInfo p = this.ClassInfo.GetMember(name);
            if (p != null)
            {
                return p;
            }

            throw new PropertyMissingException(this.ClassInfo.FullName, name);
        }
    }
}
