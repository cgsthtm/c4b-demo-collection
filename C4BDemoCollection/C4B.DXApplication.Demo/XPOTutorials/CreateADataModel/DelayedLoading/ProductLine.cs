﻿// <copyright file="ProductLine.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.CreateADataModel.DelayedLoading
{
    using DevExpress.Xpo;

    /// <summary>
    /// ProductLine.
    /// </summary>
    public class ProductLine : XPLiteObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductLine"/> class.
        /// </summary>
        /// <param name="session">Session.</param>
        public ProductLine(Session session)
            : base(session)
        {
        }

        /// <summary>
        /// Gets Products.
        /// <para>Collection Properties.</para>
        /// Returns a collection of products that correspond to the current product line.
        /// This collection is always loaded on demand.
        /// </summary>
        public XPCollection<Product> Products
        {
            get { return this.GetCollection<Product>(nameof(this.Products)); }
        }

        /// <summary>
        /// Gets or sets BigPicture.
        /// Persistent Properties.
        /// <para />The DelayedAttribute.UpdateModifiedOnly property specifies whether to include delayed properties in UPDATE statements.
        /// Set this property to true (Delayed(true)) to reduce the amount of data transferred between your application and the database server.
        /// If you have multiple delayed properties in a persistent class, use the DelayedAttribute.GroupName property to arrange delayed properties in groups.
        /// When you read a property that belongs to a group, XPO loads all properties in the same group simultaneously.
        /// <para>Do not use the DelayedAttribute with association properties. XPO always loads association properties on demand.</para>
        /// </summary>
        [Delayed(true)]
        public byte[] BigPicture
        {
            get { return this.GetDelayedPropertyValue<byte[]>(nameof(this.BigPicture)); }
            set { this.SetDelayedPropertyValue<byte[]>(nameof(this.BigPicture), value); }
        }
    }
}
