﻿// <copyright file="Product.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials.CreateADataModel.DelayedLoading
{
    using DevExpress.Xpo;

    /// <summary>
    /// Product.
    /// </summary>
    public class Product : XPLiteObject
    {
        private string productName;

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        /// <param name="session">Session.</param>
        public Product(Session session)
            : base(session)
        {
        }

        /// <summary>
        /// Gets or sets ProductName.
        /// </summary>
        public string ProductName
        {
            get { return this.productName; }
            set { this.SetPropertyValue(nameof(this.productName), ref this.productName, value); }
        }
    }
}
