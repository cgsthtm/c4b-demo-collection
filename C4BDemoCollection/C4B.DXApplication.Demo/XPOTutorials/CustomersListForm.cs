﻿// <copyright file="CustomersListForm.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.XPOTutorials
{
    using System;
    using System.Windows.Forms;
    using C4B.DXApplication.Demo.XPOTutorials.DataAccess;
    using DevExpress.Xpo;
    using DevExpress.XtraBars.Docking2010;

    /// <summary>
    /// CustomersListForm.
    /// </summary>
    public partial class CustomersListForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersListForm"/> class.
        /// </summary>
        public CustomersListForm()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets Session.
        /// </summary>
        protected Session Session { get; private set; }

        private void ConnectDataGridToXPOObjectsForm_Load(object sender, EventArgs e)
        {
            this.Reload();
        }

        private void Reload()
        {
            // Note: You cannot use the Session.Delete method with an object that belongs to a different Session.
            this.Session = new Session();
            this.CustomersBindingSource.DataSource = new XPCollection<Customer>(this.Session);
        }

        private void CustomersGridView_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (e.Clicks == 2)
            {
                e.Handled = true;
                int customerID = (int)this.CustomersGridView.GetRowCellValue(e.RowHandle, this.colOid);
                this.ShowEditForm(customerID);
            }
        }

        private void ShowEditForm(int? customerID)
        {
            var form = new EditCustomerForm(customerID);
            form.FormClosed += this.EditFormClosed;
            var documentManager = DocumentManager.FromControl(this.MdiParent);
            if (documentManager != null)
            {
                documentManager.View.AddDocument(form);
            }
            else
            {
                try
                {
                    form.ShowDialog();
                }
                finally
                {
                    form.Dispose();
                }
            }
        }

        private void EditFormClosed(object sender, FormClosedEventArgs e)
        {
            var form = (EditCustomerForm)sender;
            form.FormClosed -= this.EditFormClosed;
            if (form.CustomerID.HasValue)
            {
                this.Reload();
                this.CustomersGridView.FocusedRowHandle = this.CustomersGridView.LocateByValue("Oid", form.CustomerID.Value);
            }
        }

        private void BtnNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.ShowEditForm(null);
        }

        private void BtnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            object focusedObject = this.CustomersGridView.GetFocusedRow();
            this.Session.Delete(focusedObject);
        }
    }
}