﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo
{
    using System;
    using System.Configuration;
    using System.Windows.Forms;
    using C4B.DXApplication.Demo.MVVMDemo;
    using C4B.DXApplication.Demo.XPOTutorials.ConnectToADataStore;
    using C4B.DXApplication.Demo.XPOTutorials.ConnectToADataStore.CachedDataStore;

    /// <summary>
    /// 入口.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // MVVMDemo
            Application.Run(new MainView());

            // WinForm XPO Demo==================================================================
            ////ConnectionHelper.Connect();
            ////using (UnitOfWork uow = new UnitOfWork())
            ////{
            ////    DemoDataHelper.Seed(uow);
            ////}

            ////Application.Run(new MainForm());

            // Connect to a Data Store===========================================================
            // SpecifyConnectionSettings
            ////SpecifyConnectionSettings.Func1();

            // ShareConnectionDefaultDataAccessLayer
            ////ShareConnectionDefaultDataAccessLayer.Func1();

            // EstablishConnection
            ////EstablishConnection.ChangeConnection(EstablishConnection.ConnectionString1);
            ////EstablishConnection.Func1();
            ////EstablishConnection.ChangeConnection(EstablishConnection.ConnectionString2);
            ////EstablishConnection.Func2();

            // EstablishConnection2
            ////string connStr = ConfigurationManager.ConnectionStrings["MSSQLConnectionString"].ConnectionString;
            ////EstablishConnection2.Connect(connStr);
            ////EstablishConnection2.Func1();

            // CachedDataStore - UsingSqlDependency
            ////UsingSqlDependency.Func1();

            // ReadLine==========================================================================
            Console.ReadLine();
        }
    }
}