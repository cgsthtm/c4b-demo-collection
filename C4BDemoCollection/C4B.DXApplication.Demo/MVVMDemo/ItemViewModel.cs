﻿// <copyright file="ItemViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    using DevExpress.Mvvm;
    using DevExpress.Mvvm.POCO;

    /// <summary>
    /// ItemViewModel.
    /// </summary>
    public class ItemViewModel : ISupportParameter
    {
        /// <summary>
        /// Gets or sets Parameter.
        /// </summary>
        object ISupportParameter.Parameter
        {
            get { return this.Item; }
            set { this.ReloadItem((int)value); }
        }

        /// <summary>
        /// Gets or sets Item.
        /// </summary>
        public virtual TodoItem Item
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether HasChanges.
        /// </summary>
        public virtual bool HasChanges
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets Title.
        /// </summary>
        public string Title
        {
            get
            {
                if (this.Item == null)
                {
                    return string.Empty;
                }

                var suffix = this.HasChanges ? "*" : string.Empty;
                if (this.IsNew)
                {
                    return "New task" + suffix;
                }
                else
                {
                    return "Task #" + this.Item.Id.ToString() + suffix;
                }
            }
        }

        /// <summary>
        /// Gets NavigationService.
        /// </summary>
        protected INavigationService NavigationService
        {
            get { return this.GetService<INavigationService>(); }
        }

        /// <summary>
        /// Gets Repository.
        /// </summary>
        protected IRepository Repository
        {
            get { return this.GetRequiredService<IRepository>(); }
        }

        private bool IsNew
        {
            get { return this.Item.Id == AppModel.NewItemID; }
        }

        /// <summary>
        /// Delete.
        /// </summary>
        public void Delete()
        {
            if (this.Repository.Delete(this.Item.Id))
            {
                // Sending the ReloadRequired message for all items
                Messenger.Default.Send(ReloadRequired.All);
            }
        }

        /// <summary>
        /// Update.
        /// </summary>
        public void Update()
        {
            this.HasChanges = this.Repository.HasChanges(this.Item.Id, this.Item);
        }

        /// <summary>
        /// CanSave.
        /// </summary>
        /// <returns>bool.</returns>
        public bool CanSave()
        {
            return (this.Item != null) && this.HasChanges;
        }

        /// <summary>
        /// Save.
        /// </summary>
        public void Save()
        {
            bool isNew = this.Item.Id == AppModel.NewItemID;
            int savedId = this.Repository.Save(this.Item);
            if (isNew)
            {
                // Sending the ReloadRequired message for new item
                Messenger.Default.Send(ReloadRequired.FromNew(savedId));
            }
            else
            {
                // Sending the ReloadRequired message for the specific item
                Messenger.Default.Send(ReloadRequired.FromId(savedId));
            }
        }

        /// <summary>
        /// CanDelete.
        /// </summary>
        /// <returns>bool.</returns>
        public bool CanDelete()
        {
            return (this.Item != null) && !this.IsNew;
        }

        /// <summary>
        /// Close.
        /// </summary>
        public void Close()
        {
            // TODO: we will uncomment these item in complete application
            var document = this.NavigationService.Current as IDocument;
            this.NavigationService.GoBack();
            if (document != null)
            {
                document.Close(true);
            }
        }

        /// <summary>
        /// OnItemChanged.
        /// </summary>
        protected void OnItemChanged()
        {
            this.HasChanges = false;
            this.RaiseCanExecuteChanged(x => x.Save());
            this.RaiseCanExecuteChanged(x => x.Delete());
            this.RaisePropertyChanged(x => x.Title);
        }

        /// <summary>
        /// OnHasChangesChanged.
        /// </summary>
        protected void OnHasChangesChanged()
        {
            this.RaisePropertyChanged(x => x.Title);
            this.RaiseCanExecuteChanged(x => x.Save());
        }

        private void ReloadItem(int id)
        {
            this.Item = this.Repository.LoadItem(id);
        }
    }
}
