﻿// <copyright file="ItemsViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    using System.Collections.Generic;
    using System.Linq;
    using DevExpress.Mvvm;
    using DevExpress.Mvvm.POCO;

    /// <summary>
    /// ItemsViewModel.
    /// </summary>
    public class ItemsViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemsViewModel"/> class.
        /// </summary>
        public ItemsViewModel()
        {
            this.Title = "What needs to be done?";
            this.ShowAllItems = true;

            // Start listen the ReloadRequired message
            Messenger.Default.Register<ReloadRequired>(this, this.OnReloadRequired);
        }

        /// <summary>
        /// Gets Title.
        /// </summary>
        public string Title
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets Items.
        /// </summary>
        public virtual IList<TodoItem> Items
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets SelectedItem.
        /// </summary>
        public virtual TodoItem SelectedItem
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether ShowAllItems.
        /// </summary>
        public virtual bool ShowAllItems
        {
            get;
            set;
        }

        /// <summary>
        /// Gets Repository.
        /// </summary>
        protected IRepository Repository
        {
            get { return this.GetRequiredService<IRepository>(); }
        }

        /// <summary>
        /// Gets NavigationService.
        /// </summary>
        protected INavigationService NavigationService
        {
            get { return this.GetService<INavigationService>(); }
        }

        /// <summary>
        /// Complete.
        /// </summary>
        /// <param name="item">item.</param>
        public void Complete(TodoItem item)
        {
            item.IsCompleted = true;
            if (this.Repository.HasChanges(item.Id, item))
            {
                this.Repository.Save(item);
            }
        }

        /// <summary>
        /// New.
        /// </summary>
        public void New()
        {
            // TODO: we will uncomment this item in complete application
            this.NavigationService.Navigate("ItemView", AppModel.NewItemID, this);
        }

        /// <summary>
        /// CanEdit.
        /// </summary>
        /// <returns>bool.</returns>
        public bool CanEdit()
        {
            return this.SelectedItem != null;
        }

        /// <summary>
        /// Edit.
        /// </summary>
        public void Edit()
        {
            // TODO: we will uncomment this item in complete application
            this.NavigationService.Navigate("ItemView", this.SelectedItem.Id, this);
        }

        /// <summary>
        /// OnItemsChanged.
        /// </summary>
        protected void OnItemsChanged()
        {
            this.SelectedItem = this.Items.FirstOrDefault();
        }

        /// <summary>
        /// OnSelectedItemChanged.
        /// </summary>
        protected void OnSelectedItemChanged()
        {
            this.RaiseCanExecuteChanged(x => x.Edit());
        }

        /// <summary>
        /// OnShowAllItemsChanged.
        /// </summary>
        protected void OnShowAllItemsChanged()
        {
            this.ReloadItems(this.ShowAllItems);
        }

        private void ReloadItems(bool showAll)
        {
            int savedId = (this.SelectedItem != null) ? this.SelectedItem.Id : AppModel.NewItemID;
            if (showAll)
            {
                this.Items = this.Repository.LoadItems();
            }
            else
            {
                this.Items = this.Repository.LoadItems(x => !x.IsCompleted);
            }

            this.SelectedItem = this.Items.FirstOrDefault(x => x.Id == savedId) ?? this.SelectedItem;
        }

        private void OnReloadRequired(ReloadRequired message)
        {
            if (message == ReloadRequired.All || message.IsNew)
            {
                // Reload all Items when new item added or explicit reload requested
                if (this.ShowAllItems)
                {
                    this.Items = this.Repository.LoadItems();
                }
                else
                {
                    this.Items = this.Repository.LoadItems(x => !x.IsCompleted);
                }
            }
            else // Reload the specific item within the Items collection
            {
                this.Repository.ReloadItem(this.Items, message.Id);
            }
        }
    }
}
