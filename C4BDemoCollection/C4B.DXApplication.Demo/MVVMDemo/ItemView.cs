﻿// <copyright file="ItemView.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    /// <summary>
    /// ItemView.
    /// </summary>
    public partial class ItemView : DevExpress.XtraEditors.XtraUserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemView"/> class.
        /// </summary>
        public ItemView()
        {
            this.InitializeComponent();

            // Initializing bindings only at runtime
            if (!this.mvvmContext.IsDesignMode)
            {
                this.InitializeBindings();
            }
        }

        private void InitializeBindings()
        {
            // Initialize the Fluent API
            this.mvvmContext.ViewModelType = typeof(ItemViewModel);
            var fluent = this.mvvmContext.OfType<ItemViewModel>();

            // Bind the Title property to the label Text
            fluent.SetBinding(this.titleLabel, lbl => lbl.Text, x => x.Title);

            // Bind commands to buttons
            fluent.BindCommand(this.btnBack, x => x.Close);
            fluent.BindCommand(this.btnSave, x => x.Save);
            fluent.BindCommand(this.btnDelete, x => x.Delete);

            // Bind datasource to editors
            this.todoItemBindingSource.DataSource = typeof(TodoItem);
            this.tglCompleted.DataBindings.Add(new System.Windows.Forms.Binding(
                propertyName: "EditValue",
                dataSource: this.todoItemBindingSource,
                dataMember: "IsCompleted",
                formattingEnabled: true,
                dataSourceUpdateMode: System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));

            this.teTitle.DataBindings.Add(new System.Windows.Forms.Binding(
                propertyName: "EditValue",
                dataSource: this.todoItemBindingSource,
                dataMember: "Title",
                formattingEnabled: true,
                dataSourceUpdateMode: System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));

            this.teDescription.DataBindings.Add(new System.Windows.Forms.Binding(
                propertyName: "EditValue",
                dataSource: this.todoItemBindingSource,
                dataMember: "Description",
                formattingEnabled: true,
                dataSourceUpdateMode: System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));

            fluent.SetObjectDataSourceBinding(this.todoItemBindingSource, x => x.Item, x => x.Update);
        }
    }
}
