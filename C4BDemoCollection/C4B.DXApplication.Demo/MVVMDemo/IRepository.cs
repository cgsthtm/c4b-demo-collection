﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// IRepository.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Query count.
        /// </summary>
        /// <param name="filter">filter.</param>
        /// <returns>count.</returns>
        int Count(Func<TodoItem, bool> filter = null);

        /// <summary>
        /// Items Loading.
        /// </summary>
        /// <param name="filter">filter.</param>
        /// <returns>TodoItems.</returns>
        IList<TodoItem> LoadItems(Func<TodoItem, bool> filter = null);

        /// <summary>
        /// Item loading/reloading.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>TodoItem.</returns>
        TodoItem LoadItem(int id);

        /// <summary>
        /// Item loading/reloading.
        /// </summary>
        /// <param name="items">items.</param>
        /// <param name="id">id.</param>
        /// <returns>TodoItem.</returns>
        TodoItem ReloadItem(IList<TodoItem> items, int id);

        /// <summary>
        /// Change tracking.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="item">item.</param>
        /// <returns>bool.</returns>
        bool HasChanges(int id, TodoItem item);

        /// <summary>
        /// Update/Delete operations.
        /// </summary>
        /// <param name="item">item.</param>
        /// <returns>int.</returns>
        int Save(TodoItem item);

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        bool Delete(int id);

    }
}
