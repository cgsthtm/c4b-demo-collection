﻿// <copyright file="AppLogo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    /// <summary>
    /// AppLogo.
    /// </summary>
    public partial class AppLogo : DevExpress.XtraEditors.XtraUserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppLogo"/> class.
        /// </summary>
        public AppLogo()
        {
            this.InitializeComponent();

            // Initializing bindings only at runtime
            if (!this.mvvmContext1.IsDesignMode)
            {
                this.InitializeBindings();
            }
        }

        /// <summary>
        /// InitializeBindings.
        /// </summary>
        private void InitializeBindings()
        {
            // Initialize the Fluent API
            var fluent = this.mvvmContext1.OfType<AppLogoViewModel>();

            // Bind the Greeting property to the label Text
            fluent.SetBinding(this.greetLabel, lbl => lbl.Text, x => x.Greeting);
        }
    }
}
