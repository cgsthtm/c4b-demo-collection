﻿// <copyright file="TodoItemEqualityComparer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    using System.Collections.Generic;

    /// <summary>
    /// TodoItemEqualityComparer.
    /// </summary>
    public class TodoItemEqualityComparer : IEqualityComparer<TodoItem>
    {
        /// <inheritdoc/>
        public bool Equals(TodoItem x, TodoItem y)
        {
            if (x == null && y == null)
            {
                return true;
            }

            if (x == null || y == null)
            {
                return false;
            }

            return x.Id == y.Id;
        }

        /// <inheritdoc/>
        public int GetHashCode(TodoItem obj)
        {
            if (obj == null)
            {
                return 0;
            }

            return obj.Id.GetHashCode();
        }
    }
}
