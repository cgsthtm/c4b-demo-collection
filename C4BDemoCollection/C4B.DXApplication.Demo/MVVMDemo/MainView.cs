﻿// <copyright file="MainView.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    using DevExpress.Utils.MVVM.Services;

    /// <summary>
    /// MainView.
    /// </summary>
    public partial class MainView : DevExpress.XtraEditors.XtraForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainView"/> class.
        /// </summary>
        public MainView()
        {
            this.InitializeComponent();

            // Initializing bindings only at runtime
            if (!this.mvvmContext1.IsDesignMode)
            {
                this.InitializeNavigation();
                this.InitializeBindings();
            }
        }

        private void InitializeNavigation()
        {
            // creating the NavigationFrame as INavigationService
            var navigationService = NavigationService.Create(this.navigationFrame);

            // registering the service instance
            this.mvvmContext1.RegisterService(navigationService);

            // Initialize the Fluent API
            var fluent = this.mvvmContext1.OfType<AppViewModel>();

            // Bind the OnShown command to the Shown event
            fluent.WithEvent(this, "Shown")
                .EventToCommand(x => x.OnShown);
        }

        private void InitializeBindings()
        {
            // Initialize the Fluent API
            var fluent = this.mvvmContext1.OfType<AppViewModel>();

            // Bind the Title property to the Text
            fluent.SetBinding(this, view => view.Text, x => x.Title);
        }
    }
}