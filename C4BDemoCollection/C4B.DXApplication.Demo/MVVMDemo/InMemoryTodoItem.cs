﻿// <copyright file="InMemoryTodoItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    /// <summary>
    /// InMemoryTodoItem.
    /// </summary>
    public class InMemoryTodoItem : TodoItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InMemoryTodoItem"/> class.
        /// </summary>
        /// <param name="id">id.</param>
        public InMemoryTodoItem(int id)
            : base(id)
        {
        }
    }
}
