﻿// <copyright file="InMemoryRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    /// <summary>
    /// InMemoryRepository.
    /// </summary>
    public sealed class InMemoryRepository : InMemoryRepositoryAbstract<TodoItem>, IRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InMemoryRepository"/> class.
        /// </summary>
        public InMemoryRepository()
            : base(AppModel.NewItemID, null)
        {
        }

        /// <inheritdoc/>
        protected override TodoItem Create(int id)
        {
            return new InMemoryTodoItem(id);
        }

        /// <inheritdoc/>
        protected override int GetId(TodoItem item)
        {
            return item.Id;
        }

        /// <inheritdoc/>
        protected override bool IsEmpty(TodoItem item)
        {
            return string.IsNullOrEmpty(item.Title) && string.IsNullOrEmpty(item.Description);
        }

        /// <inheritdoc/>
        protected override TodoItem Copy(TodoItem source, int? id = null)
        {
            int actualId = id.GetValueOrDefault(source.Id);
            return new InMemoryTodoItem(actualId)
            {
                Title = source.Title,
                Description = source.Description,
                IsCompleted = source.IsCompleted,
            };
        }

        /// <inheritdoc/>
        protected override void Assign(TodoItem source, TodoItem target)
        {
            target.Title = source.Title;
            target.Description = source.Description;
            target.IsCompleted = source.IsCompleted;
        }

        /// <inheritdoc/>
        protected override bool Equals(TodoItem source, TodoItem target)
        {
            return
                (source.Title == target.Title) &&
                (source.Description == target.Description) &&
                (source.IsCompleted == target.IsCompleted);
        }
    }
}
