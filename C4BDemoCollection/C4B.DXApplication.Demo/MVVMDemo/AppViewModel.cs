﻿// <copyright file="AppViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    using System.Threading.Tasks;
    using DevExpress.Mvvm;
    using DevExpress.Mvvm.POCO;

    /// <summary>
    /// AppViewModel.
    /// </summary>
    public class AppViewModel
    {
        static AppViewModel()
        {
            // Registering  the global instance of IRepository service
            ServiceContainer.Default.RegisterService(new InMemoryRepository());
        }

        // This is ViewModel for our Application

        /// <summary>
        /// Initializes a new instance of the <see cref="AppViewModel"/> class.
        /// </summary>
        public AppViewModel()
        {
            this.Title = "Getting started witn MVVM - Todo App";
        }

        /// <summary>
        /// Gets Title.
        /// </summary>
        public string Title
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets NavigationService.
        /// </summary>
        protected INavigationService NavigationService
        {
            get { return this.GetService<INavigationService>(); }
        }

        /// <summary>
        /// OnShown.
        /// </summary>
        /// <returns>Task.</returns>
        public async Task OnShown()
        {
            // Wait some time (for demo purposes)
            await Task.Delay(System.TimeSpan.FromSeconds(1));

            // Show AppLogo screen
            this.NavigationService.Navigate("AppLogo", null, this, false);

            // Wait some time before showing ItemsView
            await Task.Delay(System.TimeSpan.FromSeconds(3));

            // TODO: we will uncomment these item in complete application
            this.NavigationService.Navigate("ItemsView", null, this, true);
        }
    }
}
