﻿// <copyright file="AppLogoViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    using System;
    using DevExpress.Mvvm.POCO;

    /// <summary>
    /// AppLogoViewModel.
    /// </summary>
    public class AppLogoViewModel
    {
        // Helper method and constant for creating the html-string
        private const string DoubleLineBreak = "<br><br>";

        /// <summary>
        /// Gets Greeting.
        /// </summary>
        public string Greeting
        {
            get
            {
                // Read this count from DB
                int uncompletedCount = this.Repository.Count(x => !x.IsCompleted);
                return
                    this.Size("Hello " + Environment.UserName + "!", 4) + DoubleLineBreak +
                    this.Size("You have " + uncompletedCount.ToString() + " incompleted tasks for now.", 2) + DoubleLineBreak +
                    this.Size("Have a good day!", 4);
            }
        }

        /// <summary>
        /// Gets Repository.
        /// </summary>
        protected IRepository Repository
        {
            get { return this.GetRequiredService<IRepository>(); }
        }

        /// <summary>
        /// Size tag.
        /// </summary>
        /// <param name="text">content.</param>
        /// <param name="delta">delta.</param>
        /// <returns>tag.</returns>
        protected string Size(string text, int delta)
        {
            return "<size=+" + delta.ToString() + ">" + text + "</size>";
        }
    }
}
