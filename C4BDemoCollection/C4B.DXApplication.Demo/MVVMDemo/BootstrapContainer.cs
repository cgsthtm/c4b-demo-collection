﻿// <copyright file="BootstrapContainer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    using Autofac;

    /// <summary>
    /// BootstrapContainer.
    /// </summary>
    internal sealed class BootstrapContainer
    {
        private static readonly BootstrapContainer Instance = new BootstrapContainer();

        private BootstrapContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<InMemoryRepository>().As<IRepository>();
            this.Container = builder.Build();
        }

        /// <summary>
        /// Gets Container.
        /// </summary>
        public IContainer Container { get; private set; }

        /// <summary>
        /// 获取实例.
        /// </summary>
        /// <returns>实例.</returns>
        public static BootstrapContainer GetInstance()
        {
            return Instance;
        }
    }
}
