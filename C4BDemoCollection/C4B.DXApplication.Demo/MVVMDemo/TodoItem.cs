﻿// <copyright file="TodoItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// TodoItem.
    /// </summary>
    public class TodoItem
    {
        // Deny direct item creation (available for data storage only)

        /// <summary>
        /// Initializes a new instance of the <see cref="TodoItem"/> class.
        /// </summary>
        /// <param name="id">id.</param>
        protected TodoItem(int id)
        {
            this.Id = id;
        }

        // Key field (for data storage).  Hidden from UI

        /// <summary>
        /// Gets Id.
        /// </summary>
        [Display(AutoGenerateField = false)]
        public int Id
        {
            get;
            private set;
        }

        // Data fields

        /// <summary>
        /// Gets or sets Title.
        /// </summary>
        public string Title
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Description.
        /// </summary>
        public string Description
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsCompleted.
        /// </summary>
        public bool IsCompleted
        {
            get;
            set;
        }
    }
}
