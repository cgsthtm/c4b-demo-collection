﻿// <copyright file="ReloadRequired.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    /// <summary>
    /// ReloadRequired.
    /// </summary>
    public class ReloadRequired
    {
        /// <summary>
        /// All.
        /// </summary>
        public static readonly ReloadRequired All = new ReloadRequired(AppModel.NewItemID, false);

        // Deny direct instances creation
        private ReloadRequired(int id, bool isNew)
        {
            this.Id = id;
            this.IsNew = isNew;
        }

        /// <summary>
        /// Gets Id.
        /// </summary>
        public int Id
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether IsNew.
        /// </summary>
        public bool IsNew
        {
            get;
            private set;
        }

        /// <summary>
        /// FromId.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>ReloadRequired.</returns>
        public static ReloadRequired FromId(int id)
        {
            return new ReloadRequired(id, false);
        }

        /// <summary>
        /// FromNew.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>ReloadRequired.</returns>
        public static ReloadRequired FromNew(int id)
        {
            return new ReloadRequired(id, true);
        }
    }
}
