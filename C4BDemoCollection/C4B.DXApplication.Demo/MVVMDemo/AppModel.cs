﻿// <copyright file="AppModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    /// <summary>
    /// AppModel.
    /// </summary>
    public sealed class AppModel
    {
        /// <summary>
        /// NewItemID.
        /// </summary>
        public static readonly int NewItemID = int.MinValue;
    }
}
