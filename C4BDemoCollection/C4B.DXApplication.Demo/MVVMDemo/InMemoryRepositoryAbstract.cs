﻿// <copyright file="InMemoryRepositoryAbstract.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    /// <summary>
    /// InMemoryRepository.
    /// </summary>
    /// <typeparam name="TItem">item.</typeparam>
    public abstract class InMemoryRepositoryAbstract<TItem>
        where TItem : class
    {
        private readonly Dictionary<int, TItem> dataStorage = new Dictionary<int, TItem>();
        private readonly int newItemID;
        private int idGeneratorSeed;

        /// <summary>
        /// Initializes a new instance of the <see cref="InMemoryRepositoryAbstract{TItem}"/> class.
        /// </summary>
        /// <param name="newItemID">newItemId.</param>
        /// <param name="initialItems">initialItems.</param>
        protected InMemoryRepositoryAbstract(int newItemID, IEnumerable<KeyValuePair<int, TItem>> initialItems)
        {
            this.newItemID = newItemID;
            this.dataStorage.Add(newItemID, this.Create(newItemID));
            if (initialItems != null)
            {
                foreach (KeyValuePair<int, TItem> initialItem in initialItems)
                {
                    this.dataStorage.Add(initialItem.Key, initialItem.Value);
                }
            }

            this.idGeneratorSeed = this.dataStorage.Count;
        }

        /// <summary>
        /// Count.
        /// </summary>
        /// <param name="filter">filter.</param>
        /// <returns>count.</returns>
        public int Count(Func<TItem, bool> filter)
        {
            int num = 0;
            foreach (KeyValuePair<int, TItem> item in this.dataStorage)
            {
                if (item.Key != this.newItemID && (filter == null || filter(item.Value)))
                {
                    num++;
                }
            }

            return num;
        }

        /// <summary>
        /// LoadItems.
        /// </summary>
        /// <param name="filter">filter.</param>
        /// <returns>bindingList.</returns>
        public IList<TItem> LoadItems(Func<TItem, bool> filter)
        {
            BindingList<TItem> bindingList = new BindingList<TItem>();
            foreach (KeyValuePair<int, TItem> item in this.dataStorage)
            {
                if (item.Key != this.newItemID && (filter == null || filter(item.Value)))
                {
                    bindingList.Add(this.Copy(item.Value));
                }
            }

            return bindingList;
        }

        /// <summary>
        /// LoadItem.
        /// </summary>
        /// <param name="itemId">itemId.</param>
        /// <returns>item.</returns>
        public TItem LoadItem(int itemId)
        {
            if (this.dataStorage.TryGetValue(itemId, out var value))
            {
                return this.Copy(value);
            }

            return null;
        }

        /// <summary>
        /// ReloadItem.
        /// </summary>
        /// <param name="items">bingdList.</param>
        /// <param name="id">id.</param>
        /// <returns>item.</returns>
        public TItem ReloadItem(IList<TItem> items, int id)
        {
            BindingList<TItem> bindingList = (BindingList<TItem>)items;
            bindingList.RaiseListChangedEvents = false;
            int position = 0;
            TItem val = null;
            for (int i = 0; i < items.Count; i++)
            {
                if (this.GetId(items[i]) == id)
                {
                    val = this.LoadItem(id);
                    items[position = i] = val;
                }
            }

            bindingList.RaiseListChangedEvents = true;
            bindingList.ResetItem(position);
            return val;
        }

        public bool Delete(int id)
        {
            if (id != newItemID)
            {
                return dataStorage.Remove(id);
            }

            return false;
        }

        /// <summary>
        /// Save.
        /// </summary>
        /// <param name="item">item.</param>
        /// <returns>id.</returns>
        public int Save(TItem item)
        {
            int id = this.GetId(item);
            if (id == this.newItemID)
            {
                int num = this.GenerateNewId();
                this.dataStorage.Add(num, this.Copy(item, num));
                return num;
            }

            this.Assign(item, this.dataStorage[id]);
            return id;
        }

        /// <summary>
        /// HasChanges.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="item">item.</param>
        /// <returns>bool.</returns>
        public bool HasChanges(int id, TItem item)
        {
            if (id == this.newItemID)
            {
                return !this.IsEmpty(item);
            }

            return !this.Equals(item, this.dataStorage[id]);
        }

        /// <summary>
        /// GetId.
        /// </summary>
        /// <param name="item">item.</param>
        /// <returns>int.</returns>
        protected abstract int GetId(TItem item);

        /// <summary>
        /// Create.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>item.</returns>
        protected abstract TItem Create(int id);

        /// <summary>
        /// Copy.
        /// </summary>
        /// <param name="source">source.</param>
        /// <param name="id">id.</param>
        /// <returns>item.</returns>
        protected abstract TItem Copy(TItem source, int? id = null);

        /// <summary>
        /// Assign.
        /// </summary>
        /// <param name="source">source.</param>
        /// <param name="target">target.</param>
        protected abstract void Assign(TItem source, TItem target);

        /// <summary>
        /// Equals.
        /// </summary>
        /// <param name="source">source.</param>
        /// <param name="target">target.</param>
        /// <returns>boo.</returns>
        protected abstract bool Equals(TItem source, TItem target);

        /// <summary>
        /// IsEmpty.
        /// </summary>
        /// <param name="source">source.</param>
        /// <returns>bool.</returns>
        protected abstract bool IsEmpty(TItem source);

        private int GenerateNewId()
        {
            return this.idGeneratorSeed++;
        }
    }
}
