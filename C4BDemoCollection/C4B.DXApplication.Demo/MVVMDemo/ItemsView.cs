﻿// <copyright file="ItemsView.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.MVVMDemo
{
    using DevExpress.Utils;
    using DevExpress.XtraEditors;
    using DevExpress.XtraEditors.ViewInfo;

    /// <summary>
    /// ItemsView.
    /// </summary>
    public partial class ItemsView : DevExpress.XtraEditors.XtraUserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemsView"/> class.
        /// </summary>
        public ItemsView()
        {
            this.InitializeComponent();

            // Initializing bindings only at runtime
            if (!this.mvvmContext.IsDesignMode)
            {
                this.InitializeBindings();
            }
        }

        private void OnListBoxControl_CustomizeItem(object sender, CustomizeTemplatedItemEventArgs e)
        {
            var item = e.DataItem as TodoItem;
            if (!item.IsCompleted)
            {
                e.TemplatedItem.Elements["IsCompleted"].ImageOptions.SvgImage = null;
            }
        }

        private void OnListBoxControl_CustomizeContextItem(object sender, ListBoxControlContextButtonCustomizeEventArgs e)
        {
            var item = e.Item as TodoItem;
            e.ContextItem.Visibility = item.IsCompleted ? ContextItemVisibility.Hidden : ContextItemVisibility.Auto;
        }

        private void InitializeBindings()
        {
            // Initialize the Fluent API
            var fluent = this.mvvmContext.OfType<ItemsViewModel>();

            // Bind the Title property to the label Text
            fluent.SetBinding(this.titleLabel, lbl => lbl.Text, x => x.Title);

            // Bind the ShowAllItems option to the toggle switch
            fluent.SetBinding(this.toggleShowAll, tgl => tgl.EditValue, x => x.ShowAllItems);

            // Bind data-items to listbox
            fluent.SetBinding(this.todoItemBindingSource, bs => bs.DataSource, x => x.Items);
            fluent.SetBinding(this.listBoxControl, lb => lb.SelectedValue, x => x.SelectedItem);

            fluent.WithEvent(this.btnNew, "Click").EventToCommand(x => x.New());
            fluent.WithEvent(this.listBoxControl, "DoubleClick").EventToCommand(x => x.Edit);

            // Bind the Complete command to the context button click (with args propagation)
            fluent.WithEvent<ContextItemClickEventArgs>(this.listBoxControl, "ContextButtonClick")
                .EventToCommand(x => x.Complete, args => args.DataItem as TodoItem);
        }
    }
}
