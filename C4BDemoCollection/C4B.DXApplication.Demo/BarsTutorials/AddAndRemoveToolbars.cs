﻿// <copyright file="AddAndRemoveToolbars.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.DXApplication.Demo.BarsTutorials
{
    using System;
    using System.Windows.Forms;
    using DevExpress.XtraBars;

    /// <summary>
    /// AddAndRemoveToolbarsForm.
    /// </summary>
    public partial class AddAndRemoveToolbars : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddAndRemoveToolbars"/> class.
        /// </summary>
        public AddAndRemoveToolbars()
        {
            this.InitializeComponent();
        }

        private void BarButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MessageBox.Show($"'{e.Item.Caption}'被点击了！");
        }

        private void BarEditItem2_ShowingEditor(object sender, DevExpress.XtraBars.ItemCancelEventArgs e)
        {
            ////e.Cancel = true;
        }

        private void BarEditItem2_EditValueChanged(object sender, EventArgs e)
        {
            this.BarStaticItem3.Caption = $"CurrentVal:{this.BarEditItem2.EditValue}";
        }

        private void BarManager1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BarSubItem subMenu = e.Item as BarSubItem;
            if (subMenu != null)
            {
                return;
            }

            MessageBox.Show("Item '" + e.Item.Caption + "' has been clicked");
        }
    }
}