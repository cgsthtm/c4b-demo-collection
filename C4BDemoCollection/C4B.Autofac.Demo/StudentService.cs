﻿// <copyright file="StudentService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo
{
    using System.Collections.Generic;

    /// <summary>
    /// StudentService.
    /// </summary>
    public class StudentService : IStudentService
    {
        private IStudentDao studentDao;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentService"/> class.
        /// </summary>
        /// <param name="studentDao">studentDao.</param>
        public StudentService(IStudentDao studentDao)
        {
            this.studentDao = studentDao;
        }

        /// <inheritdoc/>
        public bool Login(string name, string pwd)
        {
            List<Student> students = this.studentDao.Login(name, pwd);
            return students.Count > 0;
        }

        /// <inheritdoc/>
        public bool SpringOut()
        {
            bool dup = this.studentDao.StatisticDuplicateName();
            return !dup; // 业务：如果学校中有重名的学生，那么就不去春游了
        }
    }
}