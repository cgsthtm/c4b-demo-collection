﻿// <copyright file="AutofacApp.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo
{
    using System;
    using global::Autofac;

    /// <summary>
    /// AutofacApp.
    /// </summary>
    public class AutofacApp
    {
        /// <summary>
        /// SpringOut.
        /// </summary>
        public static void SpringOut()
        {
            // Create the scope, resolve your IDateWriter,
            // use it, then dispose of the scope.
            using (var scope = Program.Container.BeginLifetimeScope())
            {
                var studentService = scope.Resolve<IStudentService>();
                bool canSpringOut = studentService.SpringOut();
                if (canSpringOut)
                {
                    Console.WriteLine("可以春游！");
                }
                else
                {
                    Console.WriteLine("不可以春游！");
                }
            }
        }

        /// <summary>
        /// Login.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="pwd">pwd.</param>
        public static void Login(string name, string pwd)
        {
            // Create the scope, resolve your IDateWriter,
            // use it, then dispose of the scope.
            using (var scope = Program.Container.BeginLifetimeScope())
            {
                var studentService = scope.Resolve<IStudentService>();
                bool loginSuccess = studentService.Login(name, pwd);
                if (loginSuccess)
                {
                    Console.WriteLine("登录成功！");
                }
                else
                {
                    Console.WriteLine("登陆失败！");
                }
            }
        }
    }
}