﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo
{
    using System;
    using C4B.Autofac.Demo.Ch1_GettingStarted;
    using global::Autofac;

    /// <summary>
    /// Program.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Gets Container.
        /// </summary>
        public static IContainer Container { get; private set; }

        private static void Main(string[] args)
        {
            // Ch1_GettingStarted
            GettingStartedTest.WriteDate();

            ////SqlSugarHelper.Test_If_SqlSugar_Have_Problem();
            ////var builder = new ContainerBuilder();
            ////builder.RegisterType<StudentDao>().As<IStudentDao>();
            ////builder.RegisterType<StudentService>().As<IStudentService>();
            ////Container = builder.Build();
            ////AutofacApp.Login("cgs", "cgs");
            ////AutofacApp.SpringOut();

            Console.ReadLine();
        }
    }
}