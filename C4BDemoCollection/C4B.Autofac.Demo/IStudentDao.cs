﻿// <copyright file="IStudentDao.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo
{
    using System.Collections.Generic;

    /// <summary>
    /// IStudentDao.
    /// </summary>
    public interface IStudentDao
    {
        /// <summary>
        /// QueryAll.
        /// </summary>
        /// <returns>students.</returns>
        List<Student> QueryAll();

        /// <summary>
        /// QueryOne.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>student.</returns>
        Student QueryOne(int id);

        /// <summary>
        /// 统计所有的学校中是否有名字相同的同学.
        /// </summary>
        /// <returns>bool.</returns>
        bool StatisticDuplicateName();

        /// <summary>
        /// 登录.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="pwd">pwd.</param>
        /// <returns>students.</returns>
        List<Student> Login(string name, string pwd);
    }
}