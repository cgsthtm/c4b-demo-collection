﻿// <copyright file="BootstrapContainer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo.Ch1_GettingStarted
{
    using global::Autofac;

    /// <summary>
    /// BootstrapContainer.
    /// </summary>
    internal sealed class BootstrapContainer
    {
        private static readonly BootstrapContainer Instance = new BootstrapContainer();

        private BootstrapContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ConsoleOutput>().As<IOutput>();
            builder.RegisterType<TodayWriter>().As<IDateWriter>();
            this.Container = builder.Build();
        }

        /// <summary>
        /// Gets Container.
        /// </summary>
        public IContainer Container { get; private set; }

        /// <summary>
        /// 获取实例.
        /// </summary>
        /// <returns>实例.</returns>
        public static BootstrapContainer GetInstance()
        {
            return Instance;
        }
    }
}
