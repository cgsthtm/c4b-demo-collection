﻿// <copyright file="IOutput.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo.Ch1_GettingStarted
{
    /// <summary>
    /// IOutput.
    /// </summary>
    internal interface IOutput
    {
        /// <summary>
        /// Write.
        /// </summary>
        /// <param name="content">content.</param>
        void Write(string content);
    }
}
