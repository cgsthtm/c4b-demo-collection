﻿// <copyright file="TodayWriter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo.Ch1_GettingStarted
{
    using System;

    /// <summary>
    /// TodayWriter.
    /// </summary>
    internal class TodayWriter : IDateWriter
    {
        private IOutput output;

        /// <summary>
        /// Initializes a new instance of the <see cref="TodayWriter"/> class.
        /// </summary>
        /// <param name="output">output.</param>
        public TodayWriter(IOutput output)
        {
            this.output = output;
        }

        /// <inheritdoc/>
        public void WriteDate()
        {
            this.output.Write(DateTime.Today.ToShortDateString());
        }
    }
}
