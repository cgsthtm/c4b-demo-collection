﻿// <copyright file="GettingStartedTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo.Ch1_GettingStarted
{
    using global::Autofac;

    /// <summary>
    /// GettingStartedTest.
    /// </summary>
    internal class GettingStartedTest
    {
        /// <summary>
        /// WriteDate.
        /// </summary>
        public static void WriteDate()
        {
            // Martin Fowler 有一篇非常好的解释依赖注入/控制反转的文章  http://martinfowler.com/articles/injection.html
            using (var scope = BootstrapContainer.GetInstance().Container.BeginLifetimeScope())
            {
                var writer = scope.Resolve<IDateWriter>();
                writer.WriteDate();
            }

            // 注意: 通常来说, 服务定位模式大多情况应被看作是一种反模式 (阅读文章 http://blog.ploeh.dk/2010/02/03/ServiceLocatorIsAnAntiPattern.aspx).
            // 也就是说, 在代码中四处人为地创建生命周期而少量地使用容器并不是最佳的方式.
            // 使用 Autofac 集成类库 https://autofac-.readthedocs.io/en/latest/integration/index.html 时你通常不必做在示例应用中的这些事.
            // 这些东西都会在应用的中心,”顶层”的位置得到解决, 人为的处理是极少存在的. 当然, 如何构建你的应用取决于你自身.
        }
    }
}
