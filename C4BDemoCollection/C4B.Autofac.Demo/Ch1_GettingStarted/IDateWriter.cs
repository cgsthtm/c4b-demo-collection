﻿// <copyright file="IDateWriter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo.Ch1_GettingStarted
{
    /// <summary>
    /// IDateWriter.
    /// </summary>
    internal interface IDateWriter
    {
        /// <summary>
        /// WriteDate.
        /// </summary>
        void WriteDate();
    }
}
