﻿// <copyright file="ConsoleOutput.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo.Ch1_GettingStarted
{
    using System;

    /// <summary>
    /// ConsoleOutput.
    /// </summary>
    internal class ConsoleOutput : IOutput
    {
        /// <inheritdoc/>
        public void Write(string content)
        {
            Console.WriteLine(content);
        }
    }
}
