﻿// <copyright file="SqlSugarHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo
{
    using System;
    using System.Data.SqlClient;
    using SqlSugar;

    /// <summary>
    /// SqlSugarHelper.
    /// </summary>
    public class SqlSugarHelper // 不能是泛型类
    {
        // 多库情况下使用说明：
        // 如果是固定多库可以传 new SqlSugarScope(List<ConnectionConfig>,db=>{}) 文档：多租户
        // 如果是不固定多库 可以看文档Saas分库

        // 用单例模式

        /// <summary>
        /// Gets Db.
        /// </summary>
        public static SqlSugarScope Db { get; } = new SqlSugarScope(
            new ConnectionConfig()
            {
                // 如果SSMS可以正常连接数据库，而使用UDL文件却无法连接，那么请检查SQL Server配置管理器，
                // 看看SQL Server网络配置下面的MSSQLSERVER的协议，是否禁用了TCP/IP，果然禁用了的话，启用它，
                // 然后重启SQL Server实例服务

                ////ConnectionString = "Password=Admin1234;User ID=sa;Initial Catalog=C4BDemoCollectionDB;Data Source=.", // UDL中的连接符字串
                ConnectionString = "server=.;uid=sa;pwd=Admin1234;database=C4BDemoCollectionDB", // SqlSugar要求的连接符字串
                DbType = DbType.SqlServer, // 数据库类型
                IsAutoCloseConnection = true, // 不设成true要手动close
            },
            db =>
            {
                // (A)全局生效配置点
                // 调试SQL事件，可以删掉
                db.Aop.OnLogExecuting = (sql, pars) =>
                {
                    // 获取原生SQL推荐 5.1.4.63  性能OK
                    Console.WriteLine(UtilMethods.GetNativeSql(sql, pars));

                    // 获取无参数化SQL 对性能有影响，特别大的SQL参数多的，调试使用
                    ////Console.WriteLine(UtilMethods.GetSqlString(DbType.SqlServer,sql,pars))
                };
            });

        /// <summary>
        /// Test_If_SqlSugar_Have_Problem.
        /// </summary>
        public static void Test_If_SqlSugar_Have_Problem()
        {
            var sqlConn = new SqlConnection("Password=Admin1234;User ID=sa;Initial Catalog=C4BDemoCollectionDB;Data Source=.");
            var sqlConn1 = new SqlConnection("server=.;uid=sa;pwd=Admin1234;database=C4BDemoCollectionDB");
            var sqlConn2 = new SqlConnection("server=.;uid=sa;pwd=Admin1234;database=C4BDemoCollectionDB2222");
            try
            {
                sqlConn.Open();  // 成功-原生进行测试 是否是 SqlSugar问题
                sqlConn1.Open(); // 成功
                ////sqlConn2.Open(); // 失败
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}