﻿// <copyright file="Student.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo
{
    using SqlSugar;

    // 实体与数据库结构一样

    /// <summary>
    /// Student.
    /// </summary>
    public class Student
    {
        // 数据是自增需要加上IsIdentity
        // 数据库是主键需要加上IsPrimaryKey
        // 注意：要完全和数据库一致2个属性

        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets SchoolId.
        /// </summary>
        public int? SchoolId { get; set; }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }
    }
}