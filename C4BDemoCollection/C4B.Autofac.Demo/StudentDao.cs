﻿// <copyright file="StudentDao.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo
{
    using System.Collections.Generic;
    using System.Linq;
    using SqlSugar;

    /// <summary>
    /// StudentDao.
    /// </summary>
    public class StudentDao : IStudentDao
    {
        /// <summary>
        /// Login.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="pwd">pwd.</param>
        /// <returns>students.</returns>
        public List<Student> Login(string name, string pwd)
        {
            // 模拟登录失败
            return new List<Student>();
        }

        /// <summary>
        /// QueryAll.
        /// </summary>
        /// <returns>students.</returns>
        public List<Student> QueryAll()
        {
            List<Student> list = SqlSugarHelper.Db.Queryable<Student>().ToList();
            return list;
        }

        /// <summary>
        /// QueryOne.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>student.</returns>
        public Student QueryOne(int id)
        {
            Student student = SqlSugarHelper.Db.Queryable<Student>().Where(x => x.Id == id).First();
            return student;
        }

        /// <summary>
        /// StatisticDuplicateName.
        /// </summary>
        /// <returns>b.</returns>
        public bool StatisticDuplicateName()
        {
            var list = SqlSugarHelper.Db.Queryable<Student>()
                .GroupBy(x => x.Name)
                .Select(n => new { StuName = n.Name, DupCount = SqlFunc.AggregateCount(n.Name) }).ToList();
            bool dup = list.Where(x => x.DupCount > 1).Count() > 0;
            return dup;
        }
    }
}