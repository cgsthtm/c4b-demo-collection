﻿// <copyright file="IStudentService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Autofac.Demo
{
    /// <summary>
    /// IStudentService.
    /// </summary>
    public interface IStudentService
    {
        /// <summary>
        /// 春游.
        /// </summary>
        /// <returns>bool.</returns>
        bool SpringOut();

        /// <summary>
        /// 登录.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="pwd">pwd.</param>
        /// <returns>bool.</returns>
        bool Login(string name, string pwd);
    }
}