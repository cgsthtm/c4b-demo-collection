﻿using log4net;
using log4net.Config;
using SqlSugar;
using System;
using System.Collections.Generic;

namespace C4B.SqlSugar.Demo
{
    internal class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        private static void Main(string[] args)
        {
            BasicConfigurator.Configure();

            // 方式一：使用SqlSugarClient创建数据库对象
            SqlSugarClient db = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = "server=127.0.0.1;Database=c4bdemocollectiondb;Uid=root;Pwd=Admin1234;",
                DbType = DbType.MySql, // 配置mysql数据库
                /** 使用sqlsugar连接mysql的注意事项！！参考sqlsugar官方文档：https://www.donet5.com/Home/Doc?typeId=1219
                 * 1.需要nuget下载安装mysql.data；2.且mysql.data推荐版本为8.0.21和6.9.12
                 */
                IsAutoCloseConnection = true,
                // 从库，配置读写分离官方文档：https://www.donet5.com/Home/Doc?typeId=1209
                SlaveConnectionConfigs = new List<SlaveConnectionConfig>() {
                     new SlaveConnectionConfig() { HitRate=10, ConnectionString="" } ,
                     new SlaveConnectionConfig() { HitRate=10, ConnectionString="" }
                }
                // 多租户，多数据库官方文档：https://www.donet5.com/Home/Doc?typeId=2246
            });

            // 方式二：使用SqlSugarScope单例模式创建数据库对象
            var stuModel = new Student
            {
                SchoolId = 1,
                Name = "cgs"
            };
            SqlSugarHelper.Db.Insertable<Student>(stuModel).ExecuteCommand();

            // 手动注入
            IStudentDao studentDao = new StudentDao();
            IStudentService studentService = new StudentService(studentDao);
            bool canSpringOut = studentService.SpringOut();
            if (canSpringOut)
            {
                log.Info("可以春游！");
            }
            else
            {
                log.Info("不可以春游！");
            }

            // 查询表的所有
            var list = db.Queryable<Student>().ToList();
            foreach (var item in list)
            {
                log.Info(string.Format("Id:{0} SchoolId:{1} Nmme:{2}",item.Id,item.SchoolId,item.Name));
            }

            Console.ReadLine();
        }
    }
}