﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.SqlSugar.Demo
{
    public class StudentService : IStudentService
    {
        private IStudentDao _studentDao;
        public StudentService(IStudentDao studentDao)
        {
            _studentDao = studentDao;
        }

        public bool SpringOut()
        {
            bool dup = _studentDao.StatisticDuplicateName();
            return !dup; // 业务：如果学校中有重名的学生，那么就不去春游了
        }
    }
}
