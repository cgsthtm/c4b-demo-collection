﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.SqlSugar.Demo
{
    public interface IStudentDao
    {
        List<Student> QueryAll();
        Student QueryOne(int id);
        /// <summary>
        /// 统计所有的学校中是否有名字相同的同学
        /// </summary>
        /// <returns></returns>
        bool StatisticDuplicateName();
    }
}
