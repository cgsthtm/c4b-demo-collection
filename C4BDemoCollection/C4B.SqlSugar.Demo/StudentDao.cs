﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.SqlSugar.Demo
{
    public class StudentDao : IStudentDao
    {
        public List<Student> QueryAll()
        {
            List<Student> list = SqlSugarHelper.Db.Queryable<Student>().ToList();
            return list;
        }

        public Student QueryOne(int id)
        {
            Student student = SqlSugarHelper.Db.Queryable<Student>().Where(x => x.Id == id).First();
            return student;
        }

        public bool StatisticDuplicateName()
        {
            List<Student> list = SqlSugarHelper.Db.Queryable<Student>().GroupBy(x => new { x.SchoolId, x.Name }).ToList();
            return true;
        }
    }
}
