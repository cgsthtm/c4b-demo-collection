﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.RandomDataGenerator.Demo
{
    using System;
    using global::RandomDataGenerator.FieldOptions;
    using global::RandomDataGenerator.Randomizers;

    /// <summary>
    /// 程序入口.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// 入口.
        /// </summary>
        /// <param name="args">控制台参数.</param>
        public static void Main(string[] args)
        {
            byte[] a = RandomizerFactory.GetRandomizer(new FieldOptionsBytes() { Min = 1, Max = 10 }).Generate();
            Console.WriteLine("A:===========");
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine(a[i].ToString("X2"));
            }

            Console.WriteLine("B:===========");
            int? b = RandomizerFactory.GetRandomizer(new FieldOptionsInteger() { Min = 1, Max = 10 }).Generate();
            Console.WriteLine(b.ToString());

            Console.WriteLine("C:===========");
            byte[] c = GenerateRandomDS_ResponseFrame_Data();
            for (int i = 0; i < c.Length; i++)
            {
                Console.WriteLine(c[i].ToString("X2"));
            }

            Console.ReadLine();
        }

        /// <summary>
        /// 生成随机DM取数指令的响应帧的数据.
        /// </summary>
        /// <returns>DM取数指令的响应帧的数据.</returns>
        public static byte[] GenerateRandomDS_ResponseFrame_Data()
        {
            /*打包数据：
             *  打包数据为接收到同步指令后打包的数据，该数
                据为实际值乘以10，N为数据点数，数据低字节
                在前，高字节在后，最高位为1表示负数，为0
                表示正数，如F4FFFF表示-1.2（0xFFFFF4：
                -12,-12/10=-1.2)
             */
            int packNum = 3;
            int packNumBytesLen = packNum * 3; // 打包数据字节数组长度
            byte[] res = new byte[1 + 5 + packNumBytesLen]; // 打包数据=打包数*3个字节，每个数据3个字节
            res[0] = 0x02; // 操作码
            byte? hour = RandomizerFactory.GetRandomizer(
                new FieldOptionsByte { Min = 1, Max = 23, }).Generate();      // 时
            byte? minute = RandomizerFactory.GetRandomizer(
                new FieldOptionsByte { Min = 1, Max = 59 }).Generate();       // 分
            byte? second = RandomizerFactory.GetRandomizer(
                new FieldOptionsByte { Min = 0, Max = 59, }).Generate();      // 秒
            short? milliSecond = RandomizerFactory.GetRandomizer(
                new FieldOptionsShort { Min = 0, Max = 999, }).Generate();    // 毫秒
            byte[] milliSecondBytes = BitConverter.GetBytes(milliSecond.Value);
            res[1] = hour.Value;
            res[2] = minute.Value;
            res[3] = second.Value;
            Array.Copy(milliSecondBytes, 0, res, 4, 2);
            int index = 6;
            for (int i = 0; i < packNum; i++)
            {
                bool? high = RandomizerFactory.GetRandomizer(new FieldOptionsBoolean { Min = false, Max = true, }).Generate();
                byte[] bytes = RandomizerFactory.GetRandomizer(new FieldOptionsBytes { Min = 3, Max = 3, }).Generate();
                bytes[2] = (byte)(high.Value ? 0x00 : 0xFF);
                Array.Copy(bytes, 0, res, index, 3);
                index += 3;
            }

            return res;
        }
    }
}
