﻿using Castle.DynamicProxy;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    // We can’t have multiple base classes, but we can implement multiple interfaces and this is what Dynamic Proxy uses for mixins.
    // Let’s see an example of how you would use them:
    internal class PersonFProxy
    {
        private ProxyGenerator generator;
        public PersonFProxy()
        {
            generator = new ProxyGenerator();
        }

        private PersonF CreatePersonProxy()
        {
            var options = new ProxyGenerationOptions();
            // We’re mixing class Person with Dictionary (which could be useful if you want to attach generic information store to a person).
            // We need ProxyGenerationOptions for that. We add mixin to options using AddMixinInstance method, passing the dictionary we want to use. 
            options.AddMixinInstance(new Dictionary<string, object>());
            return (PersonF)generator.CreateClassProxy(typeof(PersonF), new Type[0], options);
        }

        public void Test()
        {
            var person = CreatePersonProxy();
            var dictionary = person as IDictionary;
            dictionary.Add("Next Leave", DateTime.Now.AddMonths(4));
            UseSomewhereElse(person);
        }

        private static void UseSomewhereElse(PersonF person)
        {
            var dictionary = person as IDictionary<string, object>;
            var date = ((DateTime)dictionary["Next Leave"]).Date;
            Console.WriteLine("Next leave date of {0} is {1}", person.Name, date);
        }
    }
}
