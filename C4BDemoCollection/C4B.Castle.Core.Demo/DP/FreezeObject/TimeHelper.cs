﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    // Two things to note about this example. One is that the implementation has few bugs, second one – the class is sealed, so you can’t inherit from it.
    public sealed class TimeHelper : ITimeHelper
    {
        // First bug, and the most obvious is, it does not check for null reference, so when you pass it null string, it will throw.
        // It also does not validate the format of the string in any way (it should rather use DateTime.TryParse method).
        public int GetHour(string dateTime)
        {
            DateTime time = DateTime.Parse(dateTime);
            return time.Hour;
        }

        public int GetMinute(string dateTime)
        {
            DateTime time = DateTime.Parse(dateTime);
            return time.Minute;
        }

        public int GetSecond(string dateTime)
        {
            DateTime time = DateTime.Parse(dateTime);
            return time.Second;
        }
    }
}
