﻿// <copyright file="Freezable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    using System;
    using System.Linq;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 可冻结的.
    /// </summary>
    public static class Freezable
    {
        // Then, in our Freezable static class we create a dictionary, that will map objects, to their freezability state:
        // private static readonly IDictionary<object, IFreezable> InstanceMap = new Dictionary<object, IFreezable>();

        // We only need one instance, so we may keep it as a static field in the Freezable class
        private static readonly ProxyGenerator Generator = new ProxyGenerator();
        private static readonly IInterceptorSelector Selector = new FreezableInterceptorSelector();

        // With that, implementation of Freezable methods should be really straightforward:
        ////public static bool IsFreezable(object obj)
        ////{
        ////    return obj != null && InstanceMap.ContainsKey(obj);
        ////}
        //// Now let’s factor out the dependency with out new found tool. First we refactor IsFreezable method, to the following.
        ////public static bool IsFreezable(object obj)
        ////{
        ////    if (obj == null)
        ////        return false;
        ////    var hack = obj as IProxyTargetAccessor;
        ////    if (hack == null)
        ////        return false;
        ////    return hack.GetInterceptors().Count(i => i is IFreezable) > 0;
        ////}

        /// <summary>
        /// 判断对象是否可冻结.
        /// </summary>
        /// <param name="obj">待判断的对象.</param>
        /// <returns>是否可冻结.</returns>
        public static bool IsFreezable(object obj)
        {
            return AsFreezable(obj) != null;
        }

        ////public static void Freeze(object freezable)
        ////{
        ////    if (!IsFreezable(freezable))
        ////    {
        ////        throw new NotFreezableObjectException(freezable);
        ////    }

        ////    InstanceMap[freezable].Freeze();
        ////}

        /// <summary>
        /// 冻结指定的可冻结的对象.
        /// </summary>
        /// <param name="freezable">待冻结的可冻结对象.</param>
        /// <exception cref="NotFreezableObjectException">不可冻结对象异常.</exception>
        public static void Freeze(object freezable)
        {
            var interceptor = AsFreezable(freezable);
            if (interceptor == null)
            {
                throw new NotFreezableObjectException(freezable);
            }

            interceptor.Freeze();
        }

        ////public static bool IsFrozen(object freezable)
        ////{
        ////    return IsFreezable(freezable) && InstanceMap[freezable].IsFrozen;
        ////}

        /// <summary>
        /// 判断对象是否为冻结状态.
        /// </summary>
        /// <param name="obj">待判断的对象.</param>
        /// <returns>是否为冻结状态.</returns>
        public static bool IsFrozen(object obj)
        {
            var freezable = AsFreezable(obj);
            return freezable != null && freezable.IsFrozen;
        }

        // ProxyGenerator class is the heart of the DP library. It has numerous methods for creating different kinds of proxies,
        // but for now, we will only use one: CreateClassProxy, that creates a proxy for a class.

        /// <summary>
        /// 将目标对象转换成可冻结的类.
        /// </summary>
        /// <typeparam name="TFreezable">泛型约束为类.</typeparam>
        /// <param name="ctorArguments">目标对象.可变参数数组.</param>
        /// <returns>可冻结的类.</returns>
        public static TFreezable MakeFreezable<TFreezable>(params object[] ctorArguments)
            where TFreezable : class
        {
            var freezableInterceptor = new FreezableInterceptor();
            var options = new ProxyGenerationOptions(new FreezableProxyGenerationHook()) { Selector = Selector };
            var proxy = Generator.CreateClassProxy(
                classToProxy: typeof(TFreezable),
                additionalInterfacesToProxy: new Type[0],
                options: options,
                interceptors: new IInterceptor[] { new CallLoggingInterceptor(), freezableInterceptor });
            ////InstanceMap.Add(proxy, freezableInterceptor);
            return proxy as TFreezable;
        }

        /// <summary>
        /// 将目标对象转换为可冻结对象接口.
        /// </summary>
        /// <param name="target">目标对象.</param>
        /// <returns>可冻结对象接口.</returns>
        private static IFreezable AsFreezable(object target)
        {
            if (target == null)
            {
                return null;
            }

            var hack = target as IProxyTargetAccessor;
            if (hack == null)
            {
                return null;
            }

            return hack.GetInterceptors().FirstOrDefault(i => i is FreezableInterceptor) as IFreezable;
        }
    }
}