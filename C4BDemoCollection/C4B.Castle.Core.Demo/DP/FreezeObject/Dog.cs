﻿// <copyright file="Dog.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    /// <summary>
    /// 狗狗.
    /// </summary>
    public class Dog
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Dog"/> class.
        /// </summary>
        /// <param name="name">狗名字.</param>
        public Dog(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Gets or sets 名字.
        /// </summary>
        public virtual string Name { get; set; }
    }
}
