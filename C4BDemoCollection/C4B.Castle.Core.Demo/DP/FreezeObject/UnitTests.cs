﻿// <copyright file="UnitTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::Castle.DynamicProxy;
    using Xunit;

    /// <summary>
    /// xUnitTest.
    /// </summary>
    public class UnitTests
    {
        // Castle DynamicProxy tutorial part I: Introduction and ProxyGenerator

        /// <summary>
        /// 判断用构造函数创建的对象不是可冻结的.
        /// </summary>
        [Fact]
        public void IsFreezable_should_be_false_for_objects_created_with_ctor()
        {
            var nonFreezablePet = new Pet();
            Assert.False(Freezable.IsFreezable(nonFreezablePet));
        }

        /// <summary>
        /// 判断用MakeFreezabel函数创建的对象是可冻结的.
        /// </summary>
        [Fact]
        public void IsFreezable_should_be_true_for_objects_created_with_MakeFreezable()
        {
            var freezablePet = Freezable.MakeFreezable<Pet>();
            Assert.True(Freezable.IsFreezable(freezablePet));
        }

        /// <summary>
        /// 可冻结的对象正常的工作.
        /// </summary>
        [Fact]
        public void Freezable_should_work_normally()
        {
            var pet = Freezable.MakeFreezable<Pet>();
            pet.Age = 3;
            pet.Deceased = true;
            pet.Name = "Rex";
            pet.Age += pet.Name.Length;
            Assert.NotNull(pet.ToString());
        }

        /// <summary>
        /// 当对一个已冻结的对象设置其属性时应该抛出异常.
        /// </summary>
        [Fact]
        public void Frozen_object_should_throw_ObjectFrozenException_when_trying_to_set_a_property()
        {
            var pet = Freezable.MakeFreezable<Pet>();
            pet.Age = 3;

            Freezable.Freeze(pet);

            Assert.Throws<ObjectFrozenException>(() => pet.Name = "This should throw");
        }

        /// <summary>
        /// 当对一个已冻结的对象读取其属性时不应该抛出异常.
        /// </summary>
        [Fact]
        public void Frozen_object_should_not_throw_when_trying_to_read_it()
        {
            var pet = Freezable.MakeFreezable<Pet>();
            pet.Age = 3;

            Freezable.Freeze(pet);

            var age = pet.Age;
            var name = pet.Name;
            var deceased = pet.Deceased;
            var str = pet.ToString();
        }

        /// <summary>
        /// 当冻结一个不可冻结的对象时应抛出异常.
        /// </summary>
        [Fact]
        public void Freeze_nonFreezable_object_should_throw_NotFreezableObjectException()
        {
            var rex = new Pet();
            Assert.Throws<NotFreezableObjectException>(() => Freezable.Freeze(rex));
        }

        /// <summary>
        /// 可冻结的对象不应拦截getter属性.
        /// </summary>
        [Fact]
        public void Freezable_should_not_intercept_property_getters()
        {
            var pet = Freezable.MakeFreezable<Pet>();
            Freezable.Freeze(pet);
            var notUsed = pet.Age; // should not intercept
            var interceptedMethodsCount = this.GetInterceptedMethodsCountFor<FreezableInterceptor>(pet);
            Assert.Equal(0, interceptedMethodsCount);
        }

        /// <summary>
        /// 获取指定对象的指定拦截器的数量.
        /// </summary>
        /// <typeparam name="T">拦截器类型.</typeparam>
        /// <param name="freezable">可冻结的目标对象.</param>
        /// <returns>目标对象的指定的拦截器的数量.</returns>
        private int GetInterceptedMethodsCountFor<T>(object freezable)
            where T : IHasCount
        {
            Assert.True(Freezable.IsFreezable(freezable));

            var hack = freezable as IProxyTargetAccessor;
            Assert.NotNull(hack);
            var loggingInterceptor = hack.GetInterceptors().
                                         Where(i => i is T).
                                         Single() as IHasCount;
            return loggingInterceptor.Count;
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void DynProxyGetTarget_should_return_proxy_itself()
        {
            var pet = Freezable.MakeFreezable<Pet>();
            var hack = pet as IProxyTargetAccessor;
            Assert.NotNull(hack);
            Assert.Same(pet, hack.DynProxyGetTarget());
        }

        [Fact]
        public void Freezable_should_not_hold_any_reference_to_created_objects()
        {
            var pet = Freezable.MakeFreezable<Pet>();
            var petWeakReference = new WeakReference(pet, false);
            pet = null;
            GC.Collect();
            Assert.False(petWeakReference.IsAlive, "Object should have been collected");
        }

        [Fact]
        public void Freezable_should_log_getters_and_setters()
        {
            var pet = Freezable.MakeFreezable<Pet>();
            pet.Age = 4;
            var age = pet.Age;
            int logsCount = GetInterceptedMethodsCountFor<CallLoggingInterceptor>(pet);
            int freezeCount = GetInterceptedMethodsCountFor<FreezableInterceptor>(pet);
            Assert.Equal(2, logsCount);
            Assert.Equal(1, freezeCount);
        }

        [Fact]
        public void Freezable_should_not_intercept_methods()
        {
            var pet = Freezable.MakeFreezable<Pet>();
            pet.ToString();
            int logsCount = GetInterceptedMethodsCountFor<CallLoggingInterceptor>(pet);
            int freezeCount = GetInterceptedMethodsCountFor<FreezableInterceptor>(pet);

            // base implementation of ToString calls each property getter, that we intercept
            // so there will be 3 calls if method is not intercepter, otherwise 4.
            Assert.Equal(3, logsCount);
            Assert.Equal(0, freezeCount);
        }

        [Fact]
        public void Freezable_should_freeze_classes_with_nonVirtual_methods()
        {
            var pet = Freezable.MakeFreezable<WithNonVirtualMethod>();
            pet.Name = "Rex";
            pet.NonVirtualMethod();
        }

        [Fact]
        public void Freezable_should_throw_when_trying_to_freeze_classes_with_nonVirtual_setters()
        {
            var exception = Assert.Throws<InvalidOperationException>(() =>
                Freezable.MakeFreezable<WithNonVirtualSetter>());
            Assert.Equal(
                "Property NonVirtualProperty is not virtual. Can't freeze classes with non-virtual properties.",
                exception.Message);
        }

        [Fact]
        public void Freezable_should_be_able_to_call_nonDefault_constructor()
        {
            var dog = Freezable.MakeFreezable<Dog>("Rex");
            Assert.Equal("Rex", dog.Name);
        }

        [Fact]
        public void Should_be_able_to_wrap_interface_with_one_method()
        {
            Func<string, int> length = s => s.Length;
            var wrapped = DelegateWrapper.WrapAs<IAnsweringEngine>(length);
            Assert.NotNull(wrapped);
            var i = wrapped.GetAnswer("Answer to Life the Universe and Everything");
            Assert.Equal(42, i);
        }

        [Fact]
        public void Should_be_able_to_write_interface_with_two_methods()
        {
            Func<string, string, bool> compare = (s1, s2) => s1.Length.Equals(s2.Length);
            Func<string, int> getHashCode = s => s.Length.GetHashCode();
            var comparer = DelegateWrapper.WrapAs<IEqualityComparer<string>>(compare, getHashCode);
            var stringByLength = new Dictionary<string, string>(comparer)
            {
                { "four", "some string" },
                { "five!", "some other string" }
            };
            Assert.Equal(2, stringByLength.Count);
            var atFive = stringByLength["12345"];
            var atFour = stringByLength["1234"];
            Assert.Equal("some other string", atFive);
        }
    }
}