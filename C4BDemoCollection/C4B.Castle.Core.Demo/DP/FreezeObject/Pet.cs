﻿// <copyright file="Pet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    /// <summary>
    /// 宠物.
    /// </summary>
    public class Pet
    {
        /// <summary>
        /// Gets or sets 名字.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets 年龄.
        /// </summary>
        public virtual int Age { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether 已死亡的.
        /// </summary>
        public virtual bool Deceased { get; set; }

        /// <summary>
        /// 重写.
        /// </summary>
        /// <returns>格式化的字符串.</returns>
        public override string ToString()
        {
            return $"Name: {this.Name}, Age: {Age}, Deceased: {this.Deceased}";
        }
    }
}
