﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    internal class CheckNullInterceptor : IInterceptor
    {
        // When a null value is passes, we want to skip the invocation of the actual method, and return right back to the caller returning 0.
        public void Intercept(IInvocation invocation)
        {
            if (invocation.Arguments[0] == null)
            {
                invocation.ReturnValue = 0;
                return;
            }
            invocation.Proceed();
        }
    }
}
