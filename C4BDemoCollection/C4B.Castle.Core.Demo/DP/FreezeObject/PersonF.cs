﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    // Next we create class proxy for Person class using the options with mixin. The Person class can by just an any ordinary, non-sealed class.
    public class PersonF
    {
        public string Name { get; set; }

        public int Age { get; set; }
    }
}
