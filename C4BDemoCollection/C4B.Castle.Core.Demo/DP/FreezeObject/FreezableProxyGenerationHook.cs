﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    public class FreezableProxyGenerationHook : IProxyGenerationHook
    {
        // ShouldInterceptMethod method, that enables us to decide whether we want to intercept a method or not. 
        public bool ShouldInterceptMethod(Type type, MethodInfo memberInfo)
        {
            // To demonstrate how selector and proxy generation hook work together we’ll instruct the hook to not intercept methods,
            // only properties. Here’s the updated code:
            return memberInfo.IsSpecialName && (IsSetterName(memberInfo.Name) || IsGetterName(memberInfo.Name));
        }

        // NonVirtualMemberNotification, that enables us to act, when user wants to proxy a type that has some non-virtual methods i.e. methods we can’t intercept.
        // NonProxyableMemberNotification?
        public void NonVirtualMemberNotification(Type type, MemberInfo memberInfo)
        {
        }

        private void ValidateNotSetter(MethodInfo method)
        {
            if (method.IsSpecialName && IsSetterName(method.Name))
                throw new InvalidOperationException(
                    string.Format(
                        "Property {0} is not virtual. Can't freeze classes with non-virtual properties.",
                        method.Name.Substring("set_".Length)
                        )
                    );
        }

        // MethodsInspected. It gets called, after all methods on proxied type have been inspected, so it is the last method to be called by proxy generator. 
        // For example if our hook implementation asked some external service (like a WCF service or database) about what to do with non-virtual methods,
        // MethodsInspected would be the place to close the connection and dispose of all resources that are no longer needed.
        public void MethodsInspected()
        {
        }

        // The main limitation is the fact, that it can not intercept non-virtual calls. This may be an issue of varying importance.
        // NonVirtualMemberNotification method, that gets called when proxy generator encounters method it can not proxy.
        public void NonProxyableMemberNotification(Type type, MemberInfo memberInfo)
        {
            var method = memberInfo as MethodInfo;
            if (method != null)
            {
                this.ValidateNotSetter(method);
            }
        }

        private bool IsGetterName(string name)
        {
            return name.StartsWith("get_", StringComparison.Ordinal);
        }

        private bool IsSetterName(string name)
        {
            return name.StartsWith("set_", StringComparison.Ordinal);
        }
    }
}
