﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    internal class DelegateSelector : IInterceptorSelector
    {
        public IInterceptor[] SelectInterceptors(Type type, MethodInfo method, IInterceptor[] interceptors)
        {
            foreach (var interceptor in interceptors)
            {
                var methodInterceptor = interceptor as MethodInterceptor;
                if (methodInterceptor == null)
                    continue;
                var d = methodInterceptor.Delegate;
                if (IsEquivalent(d, method))
                    return new[] { interceptor };
            }
            throw new ArgumentException();
        }

        private static bool IsEquivalent(Delegate d, MethodInfo method)
        {
            var dm = d.Method;
            if (!method.ReturnType.IsAssignableFrom(dm.ReturnType))
                return false;
            var parameters = method.GetParameters();
            var dp = dm.GetParameters();
            if (parameters.Length != dp.Length)
                return false;
            for (int i = 0; i < parameters.Length; i++)
            {
                //BUG: does not take into account modifiers (like out, ref...)
                if (!parameters[i].ParameterType.IsAssignableFrom(dp[i].ParameterType))
                    return false;
            }
            return true;
        }
    }
}
