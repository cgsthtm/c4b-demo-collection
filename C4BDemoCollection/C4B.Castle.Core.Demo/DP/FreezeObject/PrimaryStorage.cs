﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    // PrimaryStorage additionally has IsUp property that we’ll use to determine whether it’s good to use, or not.
    // In the later case we save to the secondary storage, as can be seen in the tests.
    public class PrimaryStorage : IStorage
    {
        private IList<object> _items = new List<object>();

        public IList<object> Items
        {
            get { return _items; }
        }

        public bool IsUp { get; set; }

        public void Save(object data)
        {
            _items.Add(data);
        }
    }
}
