﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    public class DelegateWrapper
    {
        // If you’re scratching your head wondering why I put where T : class generic constraint,
        // it’s because class in this context actually means ‘reference type’.
        // This may not be the best example in the world, but hopefully by now you see the potential capabilities interface proxies without target give you.
        public static T WrapAs<T>(Delegate impl) where T : class
        {
            // Notice also that we do not call invocation.Proceed(). Since there’s no real implementation to proceed to, this would be illegal.
            var generator = new ProxyGenerator();
            var proxy = generator.CreateInterfaceProxyWithoutTarget(typeof(T), new MethodInterceptor(impl));
            return (T)proxy;
        }

        // We now need some way of deciding which delegate should be bound to which method. InterceptorSelector to the rescue!
        public static TInterface WrapAs<TInterface>(Delegate d1, Delegate d2)
        {
            var generator = new ProxyGenerator();
            var options = new ProxyGenerationOptions { Selector = new DelegateSelector() };
            var proxy = generator.CreateInterfaceProxyWithoutTarget(
              typeof(TInterface),
              new Type[0],
              options,
              new MethodInterceptor(d1),
              new MethodInterceptor(d2));
            return (TInterface)proxy;
        }
    }
}
