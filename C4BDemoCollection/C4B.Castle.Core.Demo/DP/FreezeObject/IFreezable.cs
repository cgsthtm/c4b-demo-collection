﻿// <copyright file="IFreezable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    // First we need some way of tracking which objects are freezable and their freezability state. For that we create an interface:

    /// <summary>
    /// IFreezable.
    /// </summary>
    internal interface IFreezable
    {
        /// <summary>
        /// Gets a value indicating whether 是否为已冻结状态.
        /// </summary>
        bool IsFrozen { get; }

        /// <summary>
        /// 冻结.
        /// </summary>
        void Freeze();
    }
}