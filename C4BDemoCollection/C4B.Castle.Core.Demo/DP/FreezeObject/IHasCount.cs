﻿// <copyright file="IHasCount.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    /// <summary>
    /// IHasCount.
    /// </summary>
    public interface IHasCount
    {
        /// <summary>
        /// Gets 总数.
        /// </summary>
        int Count { get; }
    }
}
