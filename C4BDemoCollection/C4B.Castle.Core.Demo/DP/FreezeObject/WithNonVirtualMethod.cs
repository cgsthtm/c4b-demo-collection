﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    public class WithNonVirtualMethod
    {
        public virtual string Name { get; set; }
        public virtual int Age { get; set; }
        public virtual bool Deceased { get; set; }

        public string NonVirtualMethod()
        {
            return $"Name: {Name}, Age: {Age}, Deceased: {Deceased}";
        }
    }
}
