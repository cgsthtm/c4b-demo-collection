﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    public class ObjectFrozenException : Exception
    {
    }

    public class NotFreezableObjectException : Exception 
    {
        public NotFreezableObjectException(object obj) { }
    }
}
