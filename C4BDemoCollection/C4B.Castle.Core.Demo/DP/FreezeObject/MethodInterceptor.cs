﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    internal class MethodInterceptor : IInterceptor
    {
        public Delegate Delegate { get; private set; }

        public MethodInterceptor(Delegate @delegate)
        {
            this.Delegate = @delegate;
        }

        public void Intercept(IInvocation invocation)
        {
            var result = this.Delegate.DynamicInvoke(invocation.Arguments);
            invocation.ReturnValue = result;
        }
    }
}
