﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    // We’re now only left with a way to select appropriate interceptors for intercepted methods. For that, we create a selector.
    internal class TimeFixSelector : IInterceptorSelector
    {
        private static readonly MethodInfo[] methodsToAdjust =
          new[]
          {
              typeof(ITimeHelper).GetMethod("GetHour"),
              typeof(ITimeHelper).GetMethod("GetMinute")
          };
        private CheckNullInterceptor _checkNull = new CheckNullInterceptor();
        private AdjustTimeToUtcInterceptor _utcAdjust = new AdjustTimeToUtcInterceptor();

        public IInterceptor[] SelectInterceptors(Type type, MethodInfo method, IInterceptor[] interceptors)
        {
            if (!methodsToAdjust.Contains(method)) // So far there are no time zones that differ by mere seconds, so that leaves us with two methods to fix. 
                return new IInterceptor[] { _checkNull }.Union(interceptors).ToArray();
            return new IInterceptor[] { _checkNull, _utcAdjust }.Union(interceptors).ToArray();
        }
    }
}
