﻿// <copyright file="FreezableInterceptorSelector.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    using System;
    using System.Linq;
    using System.Reflection;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 可冻结的类的拦截器选择器.
    /// </summary>
    public class FreezableInterceptorSelector : IInterceptorSelector
    {
        /// <inheritdoc/>
        public IInterceptor[] SelectInterceptors(Type type, MethodInfo method, IInterceptor[] interceptors)
        {
            if (IsSetter(method))
                return interceptors;
            return interceptors.Where(i => !(i is FreezableInterceptor)).ToArray();
        }

        /// <summary>
        /// 判断
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        private bool IsSetter(MethodInfo method)
        {
            return method.IsSpecialName && method.Name.StartsWith("set_", StringComparison.Ordinal);
        }
    }
}