﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    // SecondaryStorage stores the messages in the list, and exposes it for the tests as a property:
    public class SecondaryStorage : IStorage
    {
        private IList<object> _items = new List<object>();

        public IList<object> Items
        {
            get { return _items; }
        }

        public void Save(object data)
        {
            _items.Add(data);
        }
    }
}
