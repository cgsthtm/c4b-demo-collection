﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    // If you do want to leverage this feature,
    // consider putting the interceptor that may change the target in front of the pipeline (unless in your scenario other approach makes more sense).
    // This will help you avoid cases where you waste cycles doing some work on a target that gets discarded,
    // and then other interceptors get target object that is not fully compliant with rules you set.
    // For example if you have three interceptors in the pipeline: first one performs low level validation (not null etc),
    // second one swaps target and the third one does logging,
    // the third one may work with assumption, that target object has been validated and assume it is valid,
    // which may not be the case if target object was swapped by the second interceptor, in which case the first interceptor will not get the chance to validate it.
    public class StorageInterceptor : IInterceptor
    {
        private readonly IStorage _secondaryStorage;

        public StorageInterceptor(IStorage secondaryStorage)
        {
            _secondaryStorage = secondaryStorage;
        }

        public void Intercept(IInvocation invocation)
        {
            var primaryStorage = invocation.InvocationTarget as PrimaryStorage;
            if (primaryStorage.IsUp == false)
            {
                ChangeToSecondaryStorage(invocation);
            }
            invocation.Proceed();
        }

        private void ChangeToSecondaryStorage(IInvocation invocation)
        {
            //  For interface proxies with target interface invocations implement additional interface – IChangeProxyTarget.
            //  This is true for only this kind of proxies.
            var changeProxyTarget = invocation as IChangeProxyTarget;
            // The signature sais it accepts System.Object instances,
            // but in reality the new target must be an implementer of target interface of the proxy (hence proxy with target interface). 
            changeProxyTarget.ChangeInvocationTarget(_secondaryStorage);
        }
    }
}
