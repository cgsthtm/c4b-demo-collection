﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    public class CallLoggingInterceptor : IInterceptor, IHasCount
    {
        public int Count { get; private set; } = 0;

        public void Intercept(IInvocation invocation)
        {
            Count++;
            //throw new NotImplementedException();
            invocation.Proceed();
        }
    }
}
