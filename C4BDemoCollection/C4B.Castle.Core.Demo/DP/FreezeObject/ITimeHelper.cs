﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    public interface ITimeHelper
    {
        int GetHour(string dateTime);
        int GetMinute(string dateTime);
        int GetSecond(string dateTime);
    }
}
