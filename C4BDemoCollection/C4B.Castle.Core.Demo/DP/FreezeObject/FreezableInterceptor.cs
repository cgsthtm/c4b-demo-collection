﻿// <copyright file="FreezableInterceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    using System;
    using System.Reflection;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 可冻结的拦截器.实现了IInterceptor/IFreezable/IHasCount接口.
    /// </summary>
    internal class FreezableInterceptor : IInterceptor, IFreezable, IHasCount
    {
        /// <inheritdoc/>
        public bool IsFrozen { get; private set; }

        /// <inheritdoc/>
        public int Count { get; private set; } = 0;

        /// <inheritdoc/>
        public void Freeze()
        {
            this.IsFrozen = true;
        }

        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            this.Count++;
            if (this.IsFrozen && IsSetter(invocation.Method))
            {
                throw new ObjectFrozenException();
            }

            invocation.Proceed();
        }

        private static bool IsSetter(MethodInfo method)
        {
            return method.IsSpecialName && method.Name.StartsWith("set_", StringComparison.OrdinalIgnoreCase);
        }
    }
}