﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.FreezeObject
{
    // In the tests StorageFactory class is the class that encapsulates the proxying logic,
    // and PrimaryStorage and SecondaryStorage are just sample implementation of the IStorage interface.
    // The interface is as simple as it can get:
    public interface IStorage
    {
        void Save(object data);
    }
}
