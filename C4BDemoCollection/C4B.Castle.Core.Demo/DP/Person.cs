﻿// <copyright file="Person.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DP
{
    /// <summary>
    /// Person.
    /// </summary>
    public class Person : IPerson
    {
        /// <inheritdoc/>
        public string FirstName { get; set; }

        /// <inheritdoc/>
        public string LastName { get; set; }

        /// <inheritdoc/>
        public void Change()
        {
            this.FirstName = "Scrappy";
        }
    }
}
