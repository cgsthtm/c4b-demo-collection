﻿// <copyright file="IPerson.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DP
{
    /// <summary>
    /// IPerson.
    /// </summary>
    public interface IPerson
    {
        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        string FirstName { get; set; }

        /// <summary>
        /// Gets or sets  LastName.
        /// </summary>
        string LastName { get; set; }

        /// <summary>
        /// Change Func.
        /// </summary>
        void Change();
    }
}
