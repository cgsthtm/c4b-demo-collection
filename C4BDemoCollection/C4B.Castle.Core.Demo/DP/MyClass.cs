﻿// <copyright file="MyClass.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DP
{
    using System;

    /// <summary>
    /// MyClass.
    /// </summary>
    public class MyClass
    {
        /// <summary>
        /// Gets or sets a value indicating whether Flag.
        /// </summary>
        public virtual bool Flag { get; set; }

        /// <summary>
        /// Execute Func.
        /// </summary>
        public virtual void Execute()
        {
            Console.WriteLine("Execute method called");
        }
    }
}
