﻿// <copyright file="Interceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DP
{
    using System;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 拦截机.
    /// </summary>
    public class Interceptor : IInterceptor
    {
        /// <summary>
        /// 拦截.
        /// </summary>
        /// <param name="invocation">调用接口.</param>
        public void Intercept(IInvocation invocation)
        {
            Console.WriteLine($"Before target call {invocation.Method.Name}");
            try
            {
                invocation.Proceed();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Target exception {ex.Message}");
                throw;
            }
            finally
            {
                Console.WriteLine($"After target call {invocation.Method.Name}");
            }
        }
    }
}