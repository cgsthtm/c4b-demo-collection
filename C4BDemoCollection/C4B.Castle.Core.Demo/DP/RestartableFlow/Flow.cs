﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.RestartableFlow
{
    // Now to simplify future flow definitions, we create a base class that implements basic functions:
    public abstract class Flow<M> : IFlow<M> where M : class, new()
    {
        protected M _model;
        public M Model => _model;
        public object UntypedModel => _model;

        public Flow()
        {
            _model = new M();
        }

        void IFlow.SetModel(object model)
        {
            _model = model as M;
        }

        public abstract void Execute();
    }
}
