﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C4B.Castle.Core.Demo.DP.RestartableFlow
{
    public interface IFlow
    {
        object UntypedModel { get; }
        void Execute();
        void SetModel(object model);
    }

    // Each new flow we execute should have its own Model. So, my next interface is a generic template with the Model argument:
    public interface IFlow<M> : IFlow where M : class
    {
        M Model { get; }
    }
}
