﻿// <copyright file="TestUsingXUnitFramework_Part5.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part5_InterceptorSelector_FineGrainedControlOverProxy
{
    using System.Linq;
    using C4B.Castle.Core.Demo.DPTutorial.Interceptor;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;
    using C4B.Castle.Core.Demo.DPTutorial.Model;
    using global::Castle.DynamicProxy;
    using Xunit;

    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part5
    {
        /// <summary>
        /// 可冻结的应该记录getter和setter属性.
        /// </summary>
        [Fact]
        public void Freezable_should_log_getters_and_setters()
        {
            var pet = Freezable.MakeFreezable_Part4<Pet>();
            pet.Age = 4;
            var age = pet.Age;
            int logsCount = this.GetInterceptedMethodsCountFor<CallLoggingInterceptor>(pet);
            int freezeCount = this.GetInterceptedMethodsCountFor<FreezableInterceptor>(pet);
            Assert.Equal(2, logsCount);
            Assert.Equal(1, freezeCount);
        }

        /// <summary>
        /// 可冻结的不应该拦截方法.
        /// </summary>
        [Fact]
        public void Freezable_should_not_intercept_methods()
        {
            var pet = Freezable.MakeFreezable_Part5<Pet>();
            pet.ToString();
            int logsCount = this.GetInterceptedMethodsCountFor<CallLoggingInterceptor>(pet);
            int freezeCount = this.GetInterceptedMethodsCountFor<FreezableInterceptor>(pet);

            // base implementation of ToString calls each property getter, that we intercept
            // so there will be 3 calls if method is not intercepter, otherwise 4.
            Assert.Equal(3, logsCount);
            Assert.Equal(0, freezeCount);

            pet.Age = 50;
            pet.Deceased = true;
            logsCount = this.GetInterceptedMethodsCountFor<CallLoggingInterceptor>(pet);
            freezeCount = this.GetInterceptedMethodsCountFor<FreezableInterceptor>(pet);
            Assert.Equal(5, logsCount);
            Assert.Equal(2, freezeCount);
            Freezable.Freeze_Part4(pet);
            pet.Age = 51; // 抛出异常
        }

        private int GetInterceptedMethodsCountFor<TFreezable>(object freezable)
            where TFreezable : class, new()
        {
            Assert.True(Freezable.IsFreezable_Part4_2(freezable));

            // 城堡动态代理框架生成的每个代理都实现了IProxyTargetAccessor接口
            var hack = freezable as IProxyTargetAccessor;
            Assert.NotNull(hack);
            var interceptor = hack.GetInterceptors().
                                         Where(i => i is IHasCount && i is TFreezable).
                                         Single() as IHasCount;
            return interceptor.Count;
        }
    }
}
