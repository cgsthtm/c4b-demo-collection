﻿// <copyright file="FreezableProxyGenerationHook.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial
{
    using System;
    using System.Reflection;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 可冻结对象的代理生成钩子.实现了IProxyGenerationHook接口.
    /// </summary>
    public class FreezableProxyGenerationHook : IProxyGenerationHook
    {
        /////// <summary>
        /////// ShouldInterceptMethod方法，它使我们能够决定是否要拦截一个方法.
        /////// </summary>
        /////// <param name="type">Type.</param>
        /////// <param name="memberInfo">MethodInfo.</param>
        /////// <returns>是否要拦截这个方法.</returns>
        ////public bool ShouldInterceptMethod(Type type, MethodInfo memberInfo)
        ////{
        ////    if (memberInfo.Name.StartsWith("get_", StringComparison.Ordinal) ||
        ////        memberInfo.Name.Equals("Equals") ||
        ////        memberInfo.Name.Equals("GetHashCode"))
        ////    {
        ////        return false;
        ////    }
        ////    else
        ////    {
        ////        return true;
        ////    }
        ////}

        /// Part5-InterceptorSelector.
        /// <inheritdoc/>
        public bool ShouldInterceptMethod(Type type, MethodInfo memberInfo)
        {
            if (memberInfo.IsSpecialName &&
                   (this.IsSetterName(memberInfo.Name) ||
                     this.IsGetterName(memberInfo.Name)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// NonVirtualDataNotification，它使我们能够在用户想要代理具有一些非虚拟方法的类型时采取行动，即我们无法拦截的方法?.
        /// </summary>
        /// <param name="type">Type.</param>
        /// <param name="memberInfo">MemberInfo.</param>
        public void NonVirtualMemberNotification(Type type, MemberInfo memberInfo)
        {
            // NonVirtualGenerationNotification方法。当代理生成器遇到它无法代理的方法时，它会被调用。
            var method = memberInfo as MethodInfo;
            if (method != null)
            {
                this.ValidateNotSetter(method);
            }
        }

        /// <inheritdoc/>
        public void MethodsInspected()
        {
            // MethodsInspected。在检查了代理类型上的所有方法之后，它会被调用，因此它是代理生成器调用的最后一个方法。
            // 当您拥有其他两个方法所需的一些宝贵资源时，它很有用。
            // 例如，如果我们的钩子实现询问一些外部服务（如WCF服务或数据库）如何处理非虚拟方法，MethodsInspected将是关闭连接并释放所有不再需要的资源的地方。
        }

        /// <inheritdoc/>
        public void NonProxyableMemberNotification(Type type, MemberInfo memberInfo)
        {
            // NonVirtualGenerationNotification方法。当代理生成器遇到它无法代理的方法时，它会被调用。
            var method = memberInfo as MethodInfo;
            if (method != null)
            {
                this.ValidateNotSetter(method);
            }
        }

        private bool IsGetterName(string name)
        {
            return name.StartsWith("get_", StringComparison.Ordinal);
        }

        private bool IsSetterName(string name)
        {
            return name.StartsWith("set_", StringComparison.Ordinal);
        }

        private void ValidateNotSetter(MethodInfo method)
        {
            if (method.IsSpecialName && this.IsSetterName(method.Name))
            {
                throw new InvalidOperationException(
                    string.Format(
                        "Property {0} is not virtual. Can't freeze classes with non-virtual properties.",
                        method.Name.Substring("set_".Length)));
            }
        }
    }
}