﻿// <copyright file="TestUsingXUnitFramework_Part3.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part3_SelectingWhichMethodsToIntercept
{
    using C4B.Castle.Core.Demo.DPTutorial.Interceptor;
    using C4B.Castle.Core.Demo.DPTutorial.Model;
    using global::Castle.DynamicProxy;
    using System.Linq;
    using Xunit;

    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part3
    {
        /// <summary>
        /// 可冻结的对象的getter属性不应该被拦截.
        /// </summary>
        [Fact]
        public void Freezable_should_not_intercept_property_getters()
        {
            var pet = Freezable.MakeFreezable_Part3<Pet>();
            Freezable.Freeze(pet);
            var notUsed = pet.Age; // should not intercept
            int interceptedMethodsCount = this.GetInterceptedMethodsCountFor(pet);
            Assert.Equal(0, interceptedMethodsCount);
        }

        private int GetInterceptedMethodsCountFor(object freezable)
        {
            Assert.True(Freezable.IsFreezable(freezable));

            // 城堡动态代理框架生成的每个代理都实现了IProxyTargetAccessor接口
            var hack = freezable as IProxyTargetAccessor;
            Assert.NotNull(hack);
            var loggingInterceptor = hack.GetInterceptors().
                                         Where(i => i is CallLoggingInterceptor).
                                         Single() as CallLoggingInterceptor;
            return loggingInterceptor.Count;
        }
    }
}