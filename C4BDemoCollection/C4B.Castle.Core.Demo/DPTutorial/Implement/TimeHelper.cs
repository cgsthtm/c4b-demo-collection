﻿// <copyright file="TimeHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Implement
{
    using System;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;

    /// <summary>
    /// 假设您正在使用某个供应商提供的库，库中还有一个实现ITimeHelper接口的类TimeHelper.
    /// </summary>
    public sealed class TimeHelper : ITimeHelper // 注意该类是密封类
    {
        /*关于这个例子有两点需要注意。第一个是该实现几乎没有错误，第二个-该类是密封的，所以您无法继承它。
         * 1.第一个bug，最明显的是，它不检查空引用，所以当你传递给它空字符串时，它会抛出。
         *   它也不以任何方式验证字符串的格式（它应该使用DateTime.TryParse方法）。
         * 2.更微妙的错误在于，每个方法的输出值都必须表示UTC时间。
         *   另一方面，输入值可以表示任何时区（例如“10/11/2009 9：32：11 AM-04：30“）。
         *   该实现将为UTC以外的时区生成无效值，但由于供应商位于UTC，他们没有对其进行测试。
         *   实际上，这也是由于DateTime无法表示时区信息。
         *   要修复此DateTimeOffset，即引入了DateTime 2结构。
         * 这是一个代理可能有帮助的例子。公平地说，对于这样一个微不足道的类，更好的选择是从头开始滚动自己的接口实现。
         * 然而，让我们假设TimeHelper类要复杂得多，我们确实关心它的其他非错误行为。
         * 我们发现了三个bug，我们想修复这个类，现在让我们编写测试来表达我们的需求：TimeFixTests
         */

        /// <inheritdoc/>
        public int GetHour(string dateTime)
        {
            DateTime time = DateTime.Parse(dateTime);
            return time.Hour;
        }

        /// <inheritdoc/>
        public int GetMinute(string dateTime)
        {
            DateTime time = DateTime.Parse(dateTime);
            return time.Minute;
        }

        /// <inheritdoc/>
        public int GetSecond(string dateTime)
        {
            DateTime time = DateTime.Parse(dateTime);
            return time.Second;
        }
    }
}
