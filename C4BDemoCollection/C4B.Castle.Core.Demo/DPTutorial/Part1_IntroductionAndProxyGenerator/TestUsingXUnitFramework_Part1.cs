﻿// <copyright file="TestUsingXUnitFramework_Part1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part1_IntroductionAndProxyGenerator
{
    using C4B.Castle.Core.Demo.DPTutorial.Model;
    using Xunit;

    /// <summary>
    /// DP在运行时为我们创建了一个真实的对象的透明代理，我们可以拦截对它的调用，并向对象添加逻辑。这是一个非常强大的能力。
    /// 为了指定我们的需求，我们使用xUnit框架进行测试：.
    /// </summary>
    public class TestUsingXUnitFramework_Part1
    {
        /// <summary>
        /// 使用构造函数创建的对象不应该是可冻结的.
        /// </summary>
        [Fact]
        public void IsFreezable_should_be_false_for_objects_created_with_ctor()
        {
            var nonFreezablePet = new Pet();
            Assert.False(Freezable.IsFreezable(nonFreezablePet));
        }

        /// <summary>
        /// 使用MakeFreezable函数创建的对象应该是可冻结的.
        /// </summary>
        [Fact]
        public void IsFreezable_should_be_true_for_objects_created_with_MakeFreezable()
        {
            var freezablePet = Freezable.MakeFreezable_Part1<Pet>();
            Assert.True(Freezable.IsFreezable(freezablePet));
        }

        /// <summary>
        /// 可冻结的对象应该可以正常工作.
        /// </summary>
        [Fact]
        public void Freezable_should_work_normally()
        {
            var pet = Freezable.MakeFreezable_Part1<Pet>();
            pet.Age = 3;
            pet.Deceased = true;
            pet.Name = "Rex";
            pet.Age += pet.Name.Length;
            Assert.NotNull(pet.ToString());
        }

        /// <summary>
        /// 当对一个已冻结的对象设置其属性则应该抛出异常.
        /// </summary>
        [Fact]
        public void Frozen_object_should_throw_ObjectFrozenException_when_trying_to_set_a_property()
        {
            var pet = Freezable.MakeFreezable_Part1<Pet>();
            pet.Age = 3;

            Freezable.Freeze(pet);

            Assert.Throws<ObjectFrozenException>(() => pet.Name = "This should throw");
        }

        /// <summary>
        /// 当对一个已冻结的对象读取其属性时不应该抛出异常.
        /// </summary>
        [Fact]
        public void Frozen_object_should_not_throw_when_trying_to_read_it()
        {
            var pet = Freezable.MakeFreezable_Part1<Pet>();
            pet.Age = 3;

            Freezable.Freeze(pet);

            var age = pet.Age;
            var name = pet.Name;
            var deceased = pet.Deceased;
            var str = pet.ToString();
        }

        /// <summary>
        /// 当对一个不可冻结的对象使用Freeze函数时应该抛出异常.
        /// </summary>
        [Fact]
        public void Freeze_nonFreezable_object_should_throw_NotFreezableObjectException()
        {
            var rex = new Pet();
            Assert.Throws<NotFreezableObjectException>(() => Freezable.Freeze(rex));
        }
    }
}
