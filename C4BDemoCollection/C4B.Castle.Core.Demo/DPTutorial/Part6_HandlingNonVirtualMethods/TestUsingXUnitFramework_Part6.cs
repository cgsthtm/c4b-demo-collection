﻿// <copyright file="TestUsingXUnitFramework_Part6.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part6_HandlingNonVirtualMethods
{
    using C4B.Castle.Core.Demo.DPTutorial.Model;
    using System;
    using Xunit;

    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part6
    {
        /// <summary>
        /// 可冻结的应该使用非虚方法冻结类.
        /// </summary>
        [Fact]
        public void Freezable_should_freeze_classes_with_nonVirtual_methods()
        {
            var pet = Freezable.MakeFreezable_Part5<WithNonVirtualMethod>();
            pet.Name = "Rex";
            pet.NonVirtualMethod();
        }

        /// <summary>
        /// 当尝试用非虚setter设置器冻结一个类应该抛出异常.
        /// </summary>
        [Fact]
        public void Freezable_should_throw_when_trying_to_freeze_classes_with_nonVirtual_setters()
        {
            var exception = Assert.Throws<InvalidOperationException>(() =>
                Freezable.MakeFreezable_Part5<WithNonVirtualSetter>());
            Assert.Equal(
                "Property NonVirtualProperty is not virtual. Can't freeze classes with non-virtual properties.",
                exception.Message);
        }
    }
}
