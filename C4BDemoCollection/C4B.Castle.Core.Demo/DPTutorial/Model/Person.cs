﻿// <copyright file="Person.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Model
{
    /// <summary>
    /// Person类.Part13演示用.
    /// </summary>
    public class Person
    {
        // 注意，它的属性都不是虚的，也没有实现任何接口。这没关系，因为我们不想拦截任何对Person的调用（尽管显然我们仍然可以）。

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Age.
        /// </summary>
        public int Age { get; set; }
    }
}
