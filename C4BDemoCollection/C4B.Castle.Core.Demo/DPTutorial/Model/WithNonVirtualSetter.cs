﻿// <copyright file="WithNonVirtualSetter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Model
{
    /// <summary>
    /// 没有虚属性设置器的类.用于Part6演示.
    /// </summary>
    public class WithNonVirtualSetter
    {
        /// <summary>
        /// Gets or sets NonVirtualProperty.
        /// </summary>
        public string NonVirtualProperty { get; set; }
    }
}
