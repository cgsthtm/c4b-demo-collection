﻿// <copyright file="WithNonVirtualMethod.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Model
{
    using System;

    /// <summary>
    /// 没有虚方法的类.用于Part6演示.
    /// </summary>
    public class WithNonVirtualMethod
    {
        /// <summary>
        /// Gets or sets 名字.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 非虚方法.
        /// </summary>
        public void NonVirtualMethod()
        {
            Console.WriteLine($"this is WithNonVirtualMethod's NonVirtualMethod. Name:{this.Name}");
        }
    }
}
