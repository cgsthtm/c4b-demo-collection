﻿// <copyright file="IFoo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part15_PatternsAndAntipatterns
{
    /// <summary>
    /// IFoo接口.
    /// </summary>
    public interface IFoo
    {
        /// <summary>
        /// Bar.
        /// </summary>
        /// <returns>IFoo.</returns>
        IFoo Bar();
    }
}
