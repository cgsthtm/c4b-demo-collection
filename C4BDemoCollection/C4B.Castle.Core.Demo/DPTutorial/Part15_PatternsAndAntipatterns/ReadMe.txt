﻿我们已经涵盖了几乎所有的动态代理。
现在是总结的时候了，我们将回顾在动态代理之上开发代码时可能遇到的一些最常见的陷阱。

# Leaking this-泄露
考虑这个简单的接口/类对
public interface IFoo
{
  IFoo Bar();
}
public class Foo : IFoo
{
  public IFoo Bar()
  {
    return this;
  }
}

现在，假设我们用target为IFoo创建一个代理，并像这样使用它：
var foo = GetFoo(); // returns proxy
var bar = foo.Bar();
bar.Bar();
你能看到这里的Bug吗？第二个调用不是在代理上执行的，而是在目标对象本身上执行的！我们的代理人泄露了目标。

这个问题显然不会影响类代理（因为在这种情况下，代理和目标是同一个对象）。
为什么动态代理不自己处理这种情况？因为没有简单的方法来处理这件事。
我展示的例子是最简单的一个，但是代理对象可以通过无数不同的方式泄漏它。
它可以泄漏它作为返回对象的属性，它可以泄漏它作为引发事件的发送者参数，它可以将其分配给一些全局变量，它可以将自己传递给自己的参数之一的方法等。

在某些情况下，你通常对此无能为力，知道这样的问题存在并了解其后果是很好的。但在其他情况下，解决这个问题确实很简单。
public class LeakingThisInterceptor:IInterceptor
{
  public void Intercept(IInvocation invocation)
  {
    invocation.Proceed();
    if(invocation.ReturnValue == invocation.InvocationTarget)
    {
      invocation.ReturnValue = invocation.Proxy;
    }
  }
}
添加一个拦截器（将其作为拦截器管道中的最后一个），将泄漏目标切换回代理实例。就这么简单。
注意，这个拦截器专门针对上面示例中的场景（目标通过返回值泄漏）。对于每种情况，您都需要一个专用的拦截器。

# Override equality-重写相等
当涉及到动态代理时，最常见的错误之一是没有覆盖代理生成钩子和拦截器选择器上的Equals/GetHashCode方法，
这意味着你放弃了缓存，而这又与BCL中的错误相结合，意味着性能下降（加上内存消耗增加）。
解决方案非常简单，而且这条规则没有例外--总是覆盖所有实现IProxyGenerationHook或IInterceptorHook的类上的Equals/GetHashCode方法。
从动态代理2.5开始，IInterceptorSelector实现不需要覆盖Equals/GetHashCode来使缓存工作，因为更改后的代理生成算法现在只关心选择器是否存在。

# Make your Proxy Generation Hooks purely functional-使你的代理生成钩子纯功能?

# Make your supporting classes serializable-使您的支持类可序列化
如果你要序列化你的代理，你应该让所有的类都可以序列化。这包括代理生成钩子，拦截器和拦截器选择器。否则，在尝试序列化代理时会出现异常。
这不是强制性的，但我觉得很有用。请注意，在将代理程序集持久化到磁盘时，也需要此选项。

# Use ProxyGenerationHooks and InterceptorSelectors for fine grained control-使用ProxyGenerationHooks和InterceptorSelectors进行细粒度控制
你的拦截器的方法看起来像这样吗？
public void Intercept(IInvocation invocation)
{
  if(invocation.TargetType!=typeof(Foo))
  {
    invocation.Proceed();
    return;
  }
  if(invocation.Method.Name!="Bar")
  {
    invocation.Proceed();
    return;
  }
  if(invocation.Method.GetParameters().Length!=3)
  {
    invocation.Proceed();
    return;
  }
  DoSomeActualWork(invocation);
}
如果他们这样做，往往意味着你做错了什么。将决策移动到代理生成挂钩和拦截器选择器。
- 我想拦截这个方法吗？如果答案是否定的，使用代理生成钩子将其从要代理的方法中过滤出来。
- 如果我想拦截这个方法，我应该使用哪些拦截器？我需要所有这些吗？我只需要一个吗？使用拦截器选择器来控制它。
另一方面，请记住，作为每一个功能，这也是一把双刃剑。过度使用代理生成钩子和拦截器选择器可能会大大降低代理类型缓存的效率，从而影响性能。
和往常一样，考虑一下您需要多少控制以及缓存的影响。

# SRP applies to interceptors-SRP原则适用于拦截器
SRP代表单一责任原则，这意味着一个类应该只做一件事。许多人似乎忘记了它，当谈到拦截器。
他们创建了一个巨大的拦截器类，试图从动态代理中完成他们需要的所有事情-日志记录，安全检查，参数验证，用行为增强目标对象等等。
请记住，动态代理允许您在每个方法调用中都有许多拦截器。使用此功能在拦截器之间分割行为。
您可能最终会得到一些通用的拦截器，用于记录每个类上的每个拦截方法。只要它做的只是记录-那就好了。
你可能会得到一些拦截器，它们只用于某些类的方法，比如从公共基类继承的类。只要这些拦截器只做一件事-这很好。
你可能会得到一些拦截器，它们的存在仅仅是为了拦截特定类或接口上的单个方法。那也行。
使用拦截器选择器将拦截器与它们各自的目标相匹配，不要害怕每个方法有多个拦截器。