﻿// <copyright file="TestUsingXUnitFramework_Part15.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part15_PatternsAndAntipatterns
{
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part15
    {
        private ProxyGenerator generator = new ProxyGenerator();

        /// <summary>
        /// 泄露代理目标.
        /// </summary>
        public void LeakingThis()
        {
            var foo = this.GetFoo();

            // returns proxy-你能看到这里的Bug吗？第二个调用不是在代理上执行的，而是在目标对象本身上执行的！我们的代理人泄露了目标。
            var bar = foo.Bar();
            bar.Bar();
        }

        private IFoo GetFoo()
        {
            var item = new Foo();
            var foo = (IFoo)this.generator.CreateInterfaceProxyWithTarget(typeof(IFoo), item);
            return foo;
        }
    }
}
