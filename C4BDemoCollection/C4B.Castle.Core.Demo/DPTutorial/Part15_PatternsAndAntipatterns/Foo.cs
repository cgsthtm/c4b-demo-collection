﻿// <copyright file="Foo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part15_PatternsAndAntipatterns
{
    /// <summary>
    /// Foo类.
    /// </summary>
    public class Foo : IFoo
    {
        /// <inheritdoc/>
        public IFoo Bar()
        {
            return this;
        }
    }
}
