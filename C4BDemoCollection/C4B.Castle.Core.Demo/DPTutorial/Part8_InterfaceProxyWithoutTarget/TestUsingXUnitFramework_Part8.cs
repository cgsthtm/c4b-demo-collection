﻿// <copyright file="TestUsingXUnitFramework_Part8.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part8_InterfaceProxyWithoutTarget
{
    using System;
    using System.Collections.Generic;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;
    using Xunit;

    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part8
    {
        /// <summary>
        /// 应该可以用一个方法来包装接口.
        /// </summary>
        [Fact]
        public void Should_be_able_to_wrap_interface_with_one_method()
        {
            Func<string, int> length = s => s.Length;
            var wrapped = DelegateWrapper.WrapAs<IAnsweringEngine>(length);
            Assert.NotNull(wrapped);
            var i = wrapped.GetAnswer("Answer to Life the Universe and Everything");
            Assert.Equal(42, i);
        }

        /// <summary>
        /// 应该可以用两个方法写接口?.
        /// </summary>
        [Fact]
        public void Should_be_able_to_write_interface_with_two_methods()
        {
            Func<string, string, bool> compare = (s1, s2) => s1.Length.Equals(s2.Length);
            Func<string, int> getHashCode = s => s.Length.GetHashCode();

            // 为了好玩，我们可能想扩展DelegateWrapper来处理具有多个方法的接口。让我们为此编写一个测试。
            var comparer = DelegateWrapper.WrapAs2<IEqualityComparer<string>>(compare, getHashCode);
            var stringByLength = new Dictionary<string, string>(comparer)
            {
                { "four", "some string" },
                { "five!", "some other string" },
            };

            Assert.Equal(2, stringByLength.Count);
            var atFive = stringByLength["12345"];
            var atFour = stringByLength["1234"];
            Assert.Equal("some other string", atFive);
            Assert.Equal("some string", atFour);
        }
    }
}
