﻿今天，我们将讨论没有目标的接口代理。你可能知道，接口本身是不存在的。
它是一个定义其实现者可以做什么的契约。因此，你需要一个执行者。
但是如果你创建了一个没有目标的接口代理，你就不需要提供实现者。动态代理为您创建它。这是相当强大的。

有些时候，出于各种原因，您不想创建新类来实现接口。其中一种情况可能是使用需要接口的API，在这种情况下委托会更合适。
如果你正在使用接受接口的API，希望你能在里面传递一个委托，我有一个好消息给你-没有目标的接口代理可能会帮助你。