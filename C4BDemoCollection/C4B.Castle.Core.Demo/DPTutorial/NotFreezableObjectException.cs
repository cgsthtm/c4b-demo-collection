﻿// <copyright file="NotFreezableObjectException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial
{
    using System;

    /// <summary>
    /// 不可冻结对象异常.
    /// </summary>
    public class NotFreezableObjectException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotFreezableObjectException"/> class.
        /// </summary>
        /// <param name="obj">不可冻结的对象.</param>
        public NotFreezableObjectException(object obj)
        {
        }
    }
}
