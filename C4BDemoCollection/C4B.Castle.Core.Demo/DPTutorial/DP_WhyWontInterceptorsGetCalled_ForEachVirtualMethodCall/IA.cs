﻿// <copyright file="IA.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.DP_WhyWontInterceptorsGetCalled_ForEachVirtualMethodCall
{
    /// <summary>
    /// IA接口.
    /// </summary>
    public interface IA
    {
        /// <summary>
        /// Foo method.
        /// </summary>
        void Foo();

        /// <summary>
        /// Bar method.
        /// </summary>
        void Bar();
    }
}
