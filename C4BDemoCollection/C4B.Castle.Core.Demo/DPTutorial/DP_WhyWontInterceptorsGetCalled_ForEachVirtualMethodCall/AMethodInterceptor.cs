﻿// <copyright file="AMethodInterceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.DP_WhyWontInterceptorsGetCalled_ForEachVirtualMethodCall
{
    using System;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 拦截器.
    /// </summary>
    public class AMethodInterceptor : IInterceptor
    {
        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            Console.WriteLine("Intercepted: " + invocation.Method.Name);
            invocation.Proceed();
        }
    }
}
