﻿// <copyright file="A.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.DP_WhyWontInterceptorsGetCalled_ForEachVirtualMethodCall
{
    using System;

    /// <summary>
    /// A实现IA接口.
    /// </summary>
    public class A : IA
    {
        /// <inheritdoc/>
        public virtual void Bar()
        {
            Console.Write("Bar");
        }

        /// <inheritdoc/>
        public virtual void Foo()
        {
            Console.Write("foo");
            this.Bar(); // call virtual method
        }
    }
}
