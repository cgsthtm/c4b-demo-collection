﻿// <copyright file="WhyWontInterceptorsGetCalled_ForEachVirtualMethodCall.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.DP_WhyWontInterceptorsGetCalled_ForEachVirtualMethodCall
{
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 为啥动态代理的拦截器不会被调用对于每个虚方法调用.
    /// </summary>
    public class WhyWontInterceptorsGetCalled_ForEachVirtualMethodCall
    {
        /// <summary>
        /// 测试.
        /// </summary>
        public static void Test()
        {
            /*I would have expected the output:
                Intercepted foo
                foo
                Intercepted bar
                bar
              Instead, I get:
                Intercepted foo
                foo
                bar
             */
            IA a = new A();

            // proxy-ing an interface, given an implementation
            IA proxy = new ProxyGenerator()
                           .CreateInterfaceProxyWithTarget(a, new AMethodInterceptor());
            proxy.Foo();
        }

        /// <summary>
        /// 测试2.
        /// </summary>
        public static void Test2()
        {
            /*The result was what I expected in the first place:
                Intercepted foo
                foo
                Intercepted bar
                bar
             */

            // proxy-ing an explicit type
            A proxy = (A)new ProxyGenerator()
                           .CreateClassProxy<A>(new AMethodInterceptor());
            proxy.Foo();
        }
    }
}
