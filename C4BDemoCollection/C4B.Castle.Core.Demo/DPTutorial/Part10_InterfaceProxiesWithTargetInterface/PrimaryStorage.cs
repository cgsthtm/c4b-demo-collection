﻿// <copyright file="PrimaryStorage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part10_InterfaceProxiesWithTargetInterface
{
    using System.Collections.Generic;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;

    /// <summary>
    /// 主存储.
    /// </summary>
    public class PrimaryStorage : IStorage
    {
        private IList<object> items = new List<object>();

        /// <summary>
        /// Gets Items.
        /// </summary>
        public IList<object> Items
        {
            get { return this.items; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsUp-用来确定主存储是否适合使用.
        /// </summary>
        public bool IsUp { get; set; }

        /// <inheritdoc/>
        public void Save(object data)
        {
            this.items.Add(data);
        }
    }
}
