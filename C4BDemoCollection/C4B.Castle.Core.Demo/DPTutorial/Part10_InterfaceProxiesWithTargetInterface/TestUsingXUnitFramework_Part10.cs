﻿// <copyright file="TestUsingXUnitFramework_Part10.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part10_InterfaceProxiesWithTargetInterface
{
    using System.Collections.Generic;
    using System.Linq;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;
    using Xunit;

    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part10
    {
        private PrimaryStorage primaryStorage;
        private SecondaryStorage secondaryStorage;
        private StorageFactory sut;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestUsingXUnitFramework_Part10"/> class.
        /// </summary>
        public TestUsingXUnitFramework_Part10()
        {
            this.primaryStorage = new PrimaryStorage { IsUp = true };
            this.sut = new StorageFactory(this.primaryStorage);
            this.secondaryStorage = new SecondaryStorage();
            this.sut.SecondaryStorage = this.secondaryStorage;
        }

        /// <summary>
        /// 当IsUp时保存应该用主存储.
        /// </summary>
        [Fact]
        public void Save_should_use_primaryStorage_when_it_is_up()
        {
            IStorage storage = this.sut.GetStorage();
            string message = "message";
            storage.Save(message);

            Assert.Empty(this.secondaryStorage.Items);
            Assert.NotEmpty(this.primaryStorage.Items);
            Assert.Equal(message, this.primaryStorage.Items.First());
        }

        /// <summary>
        /// 当主存储不能用时保存应该用辅助存储.
        /// </summary>
        [Fact]
        public void Save_should_use_secondaryStorage_when_primaryStorage_is_down()
        {
            this.primaryStorage.IsUp = false;
            IStorage storage = this.sut.GetStorage();
            string message = "message";
            storage.Save(message);

            Assert.Empty(this.primaryStorage.Items);
            Assert.NotEmpty(this.secondaryStorage.Items);
            Assert.Equal(message, this.secondaryStorage.Items.First());
        }

        /// <summary>
        /// 当主存储从不能用到能用时保存应该回到主存储.
        /// </summary>
        [Fact]
        public void Save_should_go_back_to_primaryStorage_when_is_goes_from_down_to_up()
        {
            IStorage storage = this.sut.GetStorage();
            string message1 = "message1";
            string message2 = "message2";
            string message3 = "message3";

            storage.Save(message1);
            this.primaryStorage.IsUp = false;
            storage.Save(message2);
            this.primaryStorage.IsUp = true;
            storage.Save(message3);
            IList<object> primary = this.primaryStorage.Items;
            IList<object> secondary = this.secondaryStorage.Items;

            Assert.Equal(2, primary.Count);
            Assert.Equal(1, secondary.Count);
            Assert.Contains(message1, primary);
            Assert.Contains(message3, primary);
            Assert.Contains(message2, secondary);
        }
    }
}
