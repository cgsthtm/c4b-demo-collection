﻿// <copyright file="SecondaryStorage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part10_InterfaceProxiesWithTargetInterface
{
    using System.Collections.Generic;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;

    /// <summary>
    /// 辅助存储.
    /// </summary>
    public class SecondaryStorage : IStorage
    {
        private IList<object> items = new List<object>();

        /// <summary>
        /// Gets Items.
        /// </summary>
        public IList<object> Items
        {
            get { return this.items; }
        }

        /// <inheritdoc/>
        public void Save(object data)
        {
            this.items.Add(data);
        }
    }
}
