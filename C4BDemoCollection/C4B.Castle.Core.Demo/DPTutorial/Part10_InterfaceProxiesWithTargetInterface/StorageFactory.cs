﻿// <copyright file="StorageFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part10_InterfaceProxiesWithTargetInterface
{
    using C4B.Castle.Core.Demo.DPTutorial.Interceptor;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// StorageFactory存储工厂.
    /// </summary>
    public class StorageFactory
    {
        private readonly IStorage primaryStorage;
        private ProxyGenerator generator;

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageFactory"/> class.
        /// </summary>
        /// <param name="primaryStorage">主存储.</param>
        public StorageFactory(IStorage primaryStorage)
        {
            this.primaryStorage = primaryStorage;
            this.generator = new ProxyGenerator();
        }

        /// <summary>
        /// Sets 辅助存储.
        /// </summary>
        public IStorage SecondaryStorage { private get; set; }

        /// <summary>
        /// 获取存储.
        /// </summary>
        /// <returns>存储.</returns>
        public IStorage GetStorage()
        {
            var interceptor = new StorageInterceptor(this.SecondaryStorage);
            object storage = this.generator.CreateInterfaceProxyWithTargetInterface(typeof(IStorage), this.primaryStorage, interceptor);
            return storage as IStorage;
        }
    }
}
