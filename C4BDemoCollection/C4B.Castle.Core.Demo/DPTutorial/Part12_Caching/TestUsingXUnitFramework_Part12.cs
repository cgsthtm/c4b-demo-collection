﻿// <copyright file="TestUsingXUnitFramework_Part12.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part12_Caching
{
    using C4B.Castle.Core.Demo.DPTutorial.Interceptor;
    using C4B.Castle.Core.Demo.DPTutorial.Model;
    using global::Castle.DynamicProxy;
    using Xunit;

    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part12
    {
        private ProxyGenerator generator = new ProxyGenerator();
        private ProxyGenerator generator1 = new ProxyGenerator();
        private ProxyGenerator generator2 = new ProxyGenerator();

        /// <summary>
        /// 两个代理语义相同.
        /// </summary>
        [Fact]
        public void Caching_TwoProxies_SemanticallyIdentical()
        {
            /*断言会成功吗？答案是-是的。
             * 在这种简单的情况下，两个代理在语义上是相同的，
             * 因此生成器（或者更准确地说，生成器正在使用的IProxyBuilder）缓存在第一次调用期间使用的类型。
             * 在第二次调用期间，它检查该高速缓存以查看是否已经存在满足它的条件的类型，它找到了它，所以它跳过生成部分并直接进入创建实例。
             */
            var proxy1 = this.generator.CreateClassProxy<Foo>(new FooInterceptor());
            var proxy2 = this.generator.CreateClassProxy<Foo>(new BarInterceptor(), new FooInterceptor());
            Assert.Equivalent(proxy1.GetType(), proxy2.GetType());
        }

        /// <summary>
        /// 两个代理语义相同-视情况而定.
        /// </summary>
        [Fact]
        public void Caching_TowGenerators_TowProxies_SemanticallyIdentical_ItDepends()
        {
            /*请注意，我们现在使用不同的生成器生成每个代理，类型是否相同？答案是--视情况而定。
             * 正如我所说，生成器使用IProxyBuilder，它为它提供了代理类型。生成器使用管理该高速缓存的ModuleScope。
             * 因此，如果两个生成器后面有相同的ModuleScope，答案将是-我们将得到相同的类型。
             * 但是，如果它们具有不同的作用域，则每个生成器将生成自己的类型。
             * 理解这一点很重要，但在真实的生活中，无论如何您可能只需要一个代理生成器，
             * 因此所有代理都将在同一范围内创建，这通常是最好的选择。
             */
            var proxy1 = this.generator1.CreateClassProxy<Foo>(new FooInterceptor());
            var proxy2 = this.generator2.CreateClassProxy<Foo>(new BarInterceptor(), new FooInterceptor());
            Assert.NotEqual(proxy1.GetType(), proxy2.GetType());
        }
    }
}
