﻿// <copyright file="FreezableInterceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interceptor
{
    using System;
    using System.Reflection;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 可冻结的拦截器.创建一个FreezableInterceptor来拦截调用，验证连接对象的冻结状态.
    /// </summary>
    public class FreezableInterceptor : IInterceptor, IFreezable, IHasCount
    {
        private static readonly ProxyGenerator Generator = new ProxyGenerator();

        /// <summary>
        /// Gets a value indicating whether 是否已冻结.
        /// </summary>
        public bool IsFrozen { get; private set; }

        /// <inheritdoc/>
        public int Count { get; private set; }

        /// <inheritdoc/>
        public void Freeze()
        {
            this.IsFrozen = true;
        }

        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            this.Count++;
            if (this.IsFrozen && IsSetter(invocation.Method))
            {
                throw new ObjectFrozenException();
            }

            invocation.Proceed();
        }

        private static bool IsSetter(MethodInfo method)
        {
            return method.IsSpecialName && method.Name.StartsWith("set_", StringComparison.OrdinalIgnoreCase);
        }
    }
}