﻿// <copyright file="FooInterceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interceptor
{
    using global::Castle.DynamicProxy;

    /// <summary>
    /// Foo拦截器.用于Part12演示.
    /// </summary>
    public class FooInterceptor : IInterceptor
    {
        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            // do something
            invocation.Proceed();
        }
    }
}
