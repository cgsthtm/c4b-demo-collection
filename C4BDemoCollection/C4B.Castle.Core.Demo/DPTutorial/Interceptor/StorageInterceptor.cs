﻿// <copyright file="StorageInterceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interceptor
{
    using C4B.Castle.Core.Demo.DPTutorial.Interface;
    using C4B.Castle.Core.Demo.DPTutorial.Part10_InterfaceProxiesWithTargetInterface;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 存储拦截器.Part10演示用.
    /// </summary>
    public class StorageInterceptor : IInterceptor
    {
        private readonly IStorage secondaryStorage;

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageInterceptor"/> class.
        /// </summary>
        /// <param name="secondaryStorage">辅助存储.</param>
        public StorageInterceptor(IStorage secondaryStorage)
        {
            this.secondaryStorage = secondaryStorage;
        }

        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            var primaryStorage = invocation.InvocationTarget as PrimaryStorage;
            if (primaryStorage.IsUp == false)
            {
                this.ChangeToSecondaryStorage(invocation);
            }

            invocation.Proceed();
        }

        private void ChangeToSecondaryStorage(IInvocation invocation)
        {
            /*在这里，您可以看到如何实现目标替换。对于具有目标接口调用的接口代理，实现额外的接口- IChangeProxyTarget。
             * 这只适用于这种代理。接口非常简单-它只有一个方法：void ChangeInvocationTarget(object target)
             */
            var changeProxyTarget = invocation as IChangeProxyTarget;
            changeProxyTarget.ChangeInvocationTarget(this.secondaryStorage);
        }
    }
}
