﻿// <copyright file="MethodInterceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interceptor
{
    using System;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 方法拦截器.
    /// </summary>
    public class MethodInterceptor : IInterceptor
    {
        private readonly Delegate impl;

        /*在实现我们的DelegateWrapper时，我们需要记住一件关于没有目标的接口代理的事情。
         * 他们只是一个壳，需要我们来填补。动态代理确实创建了一个实现接口的类，但它不知道要将什么逻辑放入其中。
         * 因此，我们有责任提供这种逻辑并通过拦截器填充shell。
         */

        // 我们的拦截器不需要很复杂。他们只是得到一个委托，并在调用时调用委托。

        /// <summary>
        /// Initializes a new instance of the <see cref="MethodInterceptor"/> class.
        /// </summary>
        /// <param name="delegate">委托.</param>
        public MethodInterceptor(Delegate @delegate)
        {
            this.impl = @delegate;
        }

        /// <summary>
        /// Gets 委托.
        /// </summary>
        public Delegate Delegate
        {
            get { return this.impl; }
        }

        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            var result = this.impl.DynamicInvoke(invocation.Arguments);
            invocation.ReturnValue = result;

            /*还要注意我们没有调用invocation.Proceed()。由于没有真实的实施，这将是非法的。
             * 如果我们这样做，我们会得到一个NotImplementedException，并带有以下消息：
             * “这是一个DynamicProxy2错误：拦截器试图'继续'一个没有目标的方法，例如，接口方法或抽象方法”。
             * 请记住，拦截器创建了一个管道（参见教程第二部分中的图片）。
             * 因此，只有最后一个拦截器不能调用Proceed，所有其他拦截器都应该调用Proceed。
             */
        }
    }
}
