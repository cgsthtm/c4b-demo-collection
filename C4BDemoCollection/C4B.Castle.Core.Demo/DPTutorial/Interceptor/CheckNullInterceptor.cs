﻿// <copyright file="CheckNullInterceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interceptor
{
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 检查NULL拦截器.用于Part9演示.
    /// </summary>
    public class CheckNullInterceptor : IInterceptor
    {
        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            // 当传递空值时，我们希望跳过实际方法的调用，并直接返回到调用者并返回0。
            if (invocation.Arguments[0] == null)
            {
                invocation.ReturnValue = 0;
                return;
            }

            invocation.Proceed();
        }
    }
}
