﻿// <copyright file="CallLoggingInterceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interceptor
{
    using System;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 调用日志拦截器.将所有拦截的调用记录到控制台.
    /// </summary>
    public class CallLoggingInterceptor : IInterceptor, IHasCount
    {
        /// <summary>
        /// Gets 调用次数.
        /// </summary>
        public int Count { get; private set; }

        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            this.Count++;
            Console.WriteLine($"{invocation.Method.Name}-触发了CallLoggingInterceptor拦截器.拦截次数:{this.Count}");
            invocation.Proceed();
        }
    }
}
