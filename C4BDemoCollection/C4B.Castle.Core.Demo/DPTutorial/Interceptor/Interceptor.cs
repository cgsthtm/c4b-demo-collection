﻿// <copyright file="Interceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interceptor
{
    using System;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 示例拦截器.
    /// </summary>
    [Serializable]
    public class Interceptor : IInterceptor
    {
        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            Console.WriteLine("Before target call");
            invocation.Proceed();
            Console.WriteLine("After target call");
        }
    }
}
