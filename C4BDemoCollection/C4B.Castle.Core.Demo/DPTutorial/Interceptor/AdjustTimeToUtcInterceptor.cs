﻿// <copyright file="AdjustTimeToUtcInterceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interceptor
{
    using System;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 调整时间到UTC拦截器.用于Part9演示.
    /// </summary>
    public class AdjustTimeToUtcInterceptor : IInterceptor
    {
        // 由于DateTime无法理解时区，因此我们必须拦截对可能在输入字符串表示UTC以外的其他时区时产生错误值的方法的调用。
        // 到目前为止，还没有时区相差几秒钟，所以我们有两种方法来解决。我们可以这样写一个拦截器：

        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            var argument = (string)invocation.Arguments[0];
            DateTimeOffset result;
            if (DateTimeOffset.TryParse(argument, out result))
            {
                argument = result.UtcDateTime.ToString();
                invocation.Arguments[0] = argument;
            }

            try
            {
                invocation.Proceed();
            }
            catch (FormatException)
            {
                invocation.ReturnValue = 0;
            }
        }
    }
}
