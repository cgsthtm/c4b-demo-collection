﻿// <copyright file="InvalidationInterceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part11_WhenOneInterfaceIsNotEnough
{
    using global::Castle.DynamicProxy;

    /// <summary>
    /// InvalidationInterceptor拦截器.Part11演示用-Class which does not implement the interface.
    /// </summary>
    public class InvalidationInterceptor : IInterceptor
    {
        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            /*如果你还记得没有目标的接口代理，基本上就是这样，你得到一个空的方法存根，你或多或少必须使用拦截器来填充逻辑。
             * 由于没有目标，管道中的最后一个拦截器不能调用invocation.Proceed（），因为没有实际的实现我们可以继续。
             */
            invocation.ReturnValue = true;
        }
    }
}
