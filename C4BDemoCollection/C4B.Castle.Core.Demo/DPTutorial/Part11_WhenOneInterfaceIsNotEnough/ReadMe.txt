﻿到目前为止，在本教程中，我们已经介绍了大部分的基础知识。然而，我们总是只代理一种类型，无论是类还是接口。
然而往往需要做得更多。如果一个类实现了多个接口，而我们希望它们都被代理，那该怎么办？
如果我们想让代理实现目标类型没有实现的接口怎么办？今天我们将讨论如何做到这一点，所以让我们直奔主题。

如果您查看API，您可能会注意到，对于每种代理，都有采用称为additionalInterfacesToProxy的类型数组的重载。
这是您扩展代理的途径，将代理扩展具有额外的功能。然而，尽管看起来很简单，但在使用其他接口时，您应该注意一些事情。
1. 没有实现接口的类-Class which does not implement the interface
2. 实现接口的类-Class which implements the interface
3. 有目标的接口代理-Interface proxy with target
4. 有目标接口的接口代理-Interface proxy with target interface