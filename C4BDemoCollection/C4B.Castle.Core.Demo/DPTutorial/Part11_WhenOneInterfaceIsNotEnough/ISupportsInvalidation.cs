﻿// <copyright file="ISupportsInvalidation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part11_WhenOneInterfaceIsNotEnough
{
    /// <summary>
    /// ISupportsInvalidation支持无效接口.Part11演示用-Class which does not implement the interface.
    /// </summary>
    public interface ISupportsInvalidation
    {
        /// <summary>
        /// 使无效.
        /// </summary>
        void Invalidate();
    }
}
