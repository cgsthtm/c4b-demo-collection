﻿// <copyright file="TestUsingXUnitFramework_Part11.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part11_WhenOneInterfaceIsNotEnough
{
    using global::Castle.DynamicProxy;
    using Xunit;

    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part11
    {
        private ProxyGenerator generator = new ProxyGenerator();

        /// <summary>
        /// 类代理应该实现额外的接口.
        /// </summary>
        [Fact]
        public void ClassProxy_should_implement_additional_interfaces()
        {
            object proxy = this.generator.CreateClassProxy(
              classToProxy: typeof(EnsurePartnerStatusRule),
              additionalInterfacesToProxy: new[] { typeof(ISupportsInvalidation) },
              new InvalidationInterceptor());

            /*EnsurePartnerStatusRole的实际实现现在并不重要，重要的是它没有实现ISupportsInvalidation接口。
             * 但是测试通过了，所以最终的代理确实实现了它（万岁！）。你认为这个接口是如何实现的？
             * 如果你还记得没有目标的接口代理，基本上就是这样，你得到一个空的方法存根，你或多或少必须使用拦截器来填充逻辑。
             * 由于没有目标，管道中的最后一个拦截器不能调用invocation.Proceed()，因为没有实际的实现我们可以继续。
             */
            Assert.IsAssignableFrom<ISupportsInvalidation>(proxy);
        }

        /// <summary>
        /// 为已经实现额外接口的类代理.
        /// </summary>
        [Fact]
        public void ClassProxy_for_class_already_implementing_additional_interfaces()
        {
            /*注意，我们没有提供任何拦截器，所以如果代理没有将调用转发到ApplyDiscountRule的实现，第二个断言将失败。
             * 那么测试会通过吗？答案是：不会。我们仍然得到一个没有目标的方法。
             * 这是令人惊讶的（我很惊讶），因为这可能不是你们大多数人所期望的。
             * 显然，对于没有目标的接口代理，这也是同样的方式，但是对于有目标的接口代理呢？
             */
            object proxy = this.generator.CreateClassProxy(
              classToProxy: typeof(ApplyDiscountRule),
              additionalInterfacesToProxy: new[] { typeof(ISupportsInvalidation) });
            Assert.IsAssignableFrom<ISupportsInvalidation>(proxy);
            ////Assert.DoesNotThrow(() => (proxy as ISupportsInvalidation).Invalidate()); // DoesNotThrow方法已被移除，使用下面代码代替
            var ex = Record.Exception(() => (proxy as ISupportsInvalidation).Invalidate());
            Assert.Null(ex);
        }

        /// <summary>
        /// 接口代理应该实现二外的接口.
        /// </summary>
        [Fact]
        public void InterfaceProxy_should_implement_additional_interfaces()
        {
            /*显然，我们需要ApplyDiscountRule类型来实现IClientRule接口，但是ISupportInvalidation呢？
             * 注意，对于这个测试，我们也没有通过任何拦截器。那么第二个断言会成功吗？
             * 答案是...视情况而定。实际上，第一个答案非常简单-它不必实现额外的接口，但第二个答案取决于它是否实现额外的接口。
             * 如果ApplyDiscountRule2实现了ISupportsInvalidation接口，那么第二个断言则成功。
             */
            object proxy = this.generator.CreateInterfaceProxyWithTarget(
              interfaceToProxy: typeof(IClientRule),
              additionalInterfacesToProxy: new[] { typeof(ISupportsInvalidation) },
              target: new ApplyDiscountRule2());
            Assert.IsAssignableFrom<ISupportsInvalidation>(proxy);
            ////Assert.DoesNotThrow(() => (proxy as ISupportsInvalidation).Invalidate());
            var ex = Record.Exception(() => (proxy as ISupportsInvalidation).Invalidate());
            Assert.Null(ex);
        }
    }
}
