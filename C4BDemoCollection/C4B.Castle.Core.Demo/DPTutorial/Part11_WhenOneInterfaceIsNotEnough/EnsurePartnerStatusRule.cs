﻿// <copyright file="EnsurePartnerStatusRule.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part11_WhenOneInterfaceIsNotEnough
{
    /// <summary>
    /// EnsurePartnerStatusRole的实际实现现在并不重要，重要的是它没有实现ISupportsInvalidation接口.
    /// </summary>
    public class EnsurePartnerStatusRule
    {
    }
}
