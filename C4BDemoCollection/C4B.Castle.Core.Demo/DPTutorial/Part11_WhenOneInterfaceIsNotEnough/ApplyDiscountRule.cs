﻿// <copyright file="ApplyDiscountRule.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part11_WhenOneInterfaceIsNotEnough
{
    /// <summary>
    /// 应用折扣规则.
    /// </summary>
    public class ApplyDiscountRule : ISupportsInvalidation
    {
        /// <inheritdoc/>
        public void Invalidate()
        {
        }
    }
}
