﻿// <copyright file="DelegateWrapper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial
{
    using System;
    using C4B.Castle.Core.Demo.DPTutorial.Interceptor;
    using C4B.Castle.Core.Demo.DPTutorial.Selector;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 委托包装器-Part8演示用.
    /// </summary>
    public class DelegateWrapper
    {
        /// <summary>
        /// 将委托包装为某个接口的实现.
        /// </summary>
        /// <typeparam name="T">泛型.</typeparam>
        /// <param name="impl">委托.</param>
        /// <returns>某个接口的实现.</returns>
        public static T WrapAs<T>(Delegate impl)
            where T : class // 如果你在挠头，想知道为什么我把where T：class泛型约束放在这里，

            // 那是因为class在这个上下文中实际上意味着“引用类型”。
            // 这可能不是世界上最好的例子，但希望现在您看到了没有目标的接口代理的潜在功能。
        {
            var generator = new ProxyGenerator();
            var proxy = generator.CreateInterfaceProxyWithoutTarget(typeof(T), new MethodInterceptor(impl));
            return (T)proxy;
        }

        /// <summary>
        /// 将委托包装为某个接口的实现.
        /// </summary>
        /// <typeparam name="TInterface">泛型接口.</typeparam>
        /// <param name="d1">委托1.</param>
        /// <param name="d2">委托2.</param>
        /// <returns>某个接口的实现.</returns>
        public static TInterface WrapAs2<TInterface>(Delegate d1, Delegate d2)
        {
            var generator = new ProxyGenerator();
            var options = new ProxyGenerationOptions { Selector = new DelegateSelector() };
            var proxy = generator.CreateInterfaceProxyWithoutTarget(
              typeof(TInterface),
              new Type[0],
              options,
              new MethodInterceptor(d1),
              new MethodInterceptor(d2));
            return (TInterface)proxy;
        }
    }
}
