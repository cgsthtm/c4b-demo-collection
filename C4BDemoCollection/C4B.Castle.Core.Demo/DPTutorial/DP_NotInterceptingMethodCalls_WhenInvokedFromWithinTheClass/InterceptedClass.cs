﻿// <copyright file="InterceptedClass.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.DP_NotInterceptingMethodCalls_WhenInvokedFromWithinTheClass
{
    using System;

    /// <summary>
    /// InterceptedClass类.
    /// </summary>
    public class InterceptedClass
    {
        /// <summary>
        /// 待拦截的方法1.
        /// </summary>
        public virtual void Method1()
        {
            Console.WriteLine("Called Method 1");
            this.Method2();
        }

        /// <summary>
        /// 待拦截的方法2.
        /// </summary>
        public virtual void Method2()
        {
            Console.WriteLine("Called Method 2");
        }
    }
}
