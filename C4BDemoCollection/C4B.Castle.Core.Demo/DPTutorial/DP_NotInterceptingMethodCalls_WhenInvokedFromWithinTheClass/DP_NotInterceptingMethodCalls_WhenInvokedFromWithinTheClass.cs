﻿// <copyright file="DP_NotInterceptingMethodCalls_WhenInvokedFromWithinTheClass.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.DP_NotInterceptingMethodCalls_WhenInvokedFromWithinTheClass
{
    using System;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// DP不拦截方法调用当从代理类内部调用时.
    /// </summary>
    public class DP_NotInterceptingMethodCalls_WhenInvokedFromWithinTheClass
    {
        /// <summary>
        /// 测试.
        /// </summary>
        public static void Test()
        {
            /*I was expecting to get the output:
                Intercepted call to: Method1
                Called Method 1
                Intercepted call to: Method2
                Called Method 2
                Intercepted call to: Method2
                Called Method 2
              However what I got was:
                Intercepted call to: Method1
                Called Method 1
                Called Method 2
                Intercepted call to: Method2
                Called Method 2
             */
            var c = new InterceptedClass();
            var i = new MethodInterceptor();

            var cp = new ProxyGenerator().CreateClassProxyWithTarget(c, i);

            cp.Method1();
            cp.Method2();

            Console.ReadLine();
        }

        /// <summary>
        /// 测试2.
        /// </summary>
        public static void Test2()
        {
            // 现在会打印期望的输出了
            var i = new MethodInterceptor();
            var cp = new ProxyGenerator().CreateClassProxy<InterceptedClass>(i);

            cp.Method1();
            cp.Method2();

            Console.ReadLine();
        }
    }
}
