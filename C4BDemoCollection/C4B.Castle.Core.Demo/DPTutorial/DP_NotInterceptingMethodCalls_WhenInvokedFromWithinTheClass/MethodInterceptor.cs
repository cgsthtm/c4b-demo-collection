﻿// <copyright file="MethodInterceptor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>


namespace C4B.Castle.Core.Demo.DPTutorial.DP_NotInterceptingMethodCalls_WhenInvokedFromWithinTheClass
{
    using System;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 方法拦截器.
    /// </summary>
    public class MethodInterceptor : IInterceptor
    {
        /// <inheritdoc/>
        public void Intercept(IInvocation invocation)
        {
            Console.WriteLine(string.Format("Intercepted call to: " + invocation.Method.Name));
            invocation.Proceed();
        }
    }
}
