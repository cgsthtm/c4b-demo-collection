﻿// <copyright file="ISpeak.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interface
{
    /// <summary>
    /// ISpeak接口.Part13演示用.
    /// </summary>
    public interface ISpeak
    {
        /// <summary>
        /// 说话.
        /// </summary>
        void Speak();
    }
}
