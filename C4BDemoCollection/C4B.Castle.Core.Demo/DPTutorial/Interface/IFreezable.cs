﻿// <copyright file="IFreezable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interface
{
    /// <summary>
    /// IFreezable.
    /// </summary>
    internal interface IFreezable
    {
        /// <summary>
        /// Gets a value indicating whether 是否已冻结.
        /// </summary>
        bool IsFrozen { get; }

        /// <summary>
        /// 冻结.
        /// </summary>
        void Freeze();
    }
}
