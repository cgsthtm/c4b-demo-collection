﻿// <copyright file="ITimeHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interface
{
    /// <summary>
    /// ITimeHelper接口-用于Part9演示.
    /// </summary>
    public interface ITimeHelper
    {
        // 假设您正在使用某个供应商提供的库，该库公开了如下沿着的接口及其实现：
        // 库中还有一个实现接口的类：TimeHelper

        /// <summary>
        /// 获取小时.
        /// </summary>
        /// <param name="dateTime">日期字符串.</param>
        /// <returns>返回小时.</returns>
        int GetHour(string dateTime);

        /// <summary>
        /// 获取分钟.
        /// </summary>
        /// <param name="dateTime">日期字符串.</param>
        /// <returns>返回分钟.</returns>
        int GetMinute(string dateTime);

        /// <summary>
        /// 获取秒.
        /// </summary>
        /// <param name="dateTime">日期字符串.</param>
        /// <returns>返回秒.</returns>
        int GetSecond(string dateTime);
    }
}
