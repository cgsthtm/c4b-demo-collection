﻿// <copyright file="IDeepThought.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interface
{
    /// <summary>
    /// IDeepThought接口-Part8演示用.
    /// </summary>
    public interface IDeepThought
    {
        /// <summary>
        /// 设置回复引擎.
        /// </summary>
        /// <param name="answeringEngine">IAnsweringEngine接口.</param>
        void SetAnsweringEngine(IAnsweringEngine answeringEngine);
    }

    /*现在，考虑到你的应答引擎是非常简单的一行程序，你可能不希望在代码中创建一个新的类，而是传递一个lambda。但这是行不通的。
     * IDeepThought d = GetSuperComputer();
     * d.SetAnsweringEngine( ( string s ) => s.Length );
     */
}
