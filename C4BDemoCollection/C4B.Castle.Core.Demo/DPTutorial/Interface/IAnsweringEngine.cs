﻿// <copyright file="IAnsweringEngine.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interface
{
    /// <summary>
    /// IAnsweringEngine-Part8演示用.
    /// </summary>
    public interface IAnsweringEngine
    {
        /// <summary>
        /// 获取回复.
        /// </summary>
        /// <param name="s">字符串?.</param>
        /// <returns>回复数量?.</returns>
        int GetAnswer(string s);
    }
}
