﻿// <copyright file="IHasCount.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interface
{
    /// <summary>
    /// IHasCount.
    /// </summary>
    public interface IHasCount
    {
        /// <summary>
        /// Gets Count.
        /// </summary>
        int Count { get; }
    }
}
