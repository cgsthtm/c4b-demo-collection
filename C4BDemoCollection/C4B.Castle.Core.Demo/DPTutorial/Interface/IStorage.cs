﻿// <copyright file="IStorage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Interface
{
    /// <summary>
    /// IStorage接口-Part10演示用.
    /// </summary>
    public interface IStorage
    {
        /// <summary>
        /// 保存数据.
        /// </summary>
        /// <param name="data">数据.</param>
        void Save(object data);
    }
}
