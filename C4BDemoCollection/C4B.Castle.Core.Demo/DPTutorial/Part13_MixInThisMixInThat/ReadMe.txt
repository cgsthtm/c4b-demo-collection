﻿到目前为止，我们已经介绍了动态代理的大部分基本特性，除了一个- mixins。
不涉及理论细节，mixin是一个将许多其他对象缝合在一起的对象，展示所有这些对象的行为。让我用伪代码来说明：
var dog = Dog.New();
var cat = Cat.New();
var mixin = mixin(cat, dog);
mixin.Bark();
mixin.Meow();

在大多数语言中，这是通过多重继承来实现的，但这在动态代理中是不允许的，这对mixin在动态代理中的实现和工作方式施加了一定的限制。
我们不能有多个基类，但是我们可以实现多个接口，这就是Dynamic Proxy用于mixin的方法。让我们来看看一个例子，你会如何使用它们：
var options = new ProxyGenerationOptions();
options.AddMixinInstance(new Dictionary<string , object>());
return (Person)generator.CreateClassProxy(typeof(Person), interfaces, options);
我们将Person类与Dictionary类混合在一起（如果您想将通用信息存储附加到一个人，这可能会很有用）。
为此，我们需要ProxyGenerationOptions。我们使用AddMixinInstance方法将mixin添加到选项中，传递我们想要使用的字典。
接下来，我们使用mixin的选项为Person类创建类代理。Person类可以是任何普通的、非密封的类。
public class Person
{
  public string Name { get; set; }
 
  public int Age { get; set; }
}
注意，它的属性都不是虚的，也没有实现任何接口。这没关系，因为我们不想拦截任何对Person的调用（尽管显然我们仍然可以）。

