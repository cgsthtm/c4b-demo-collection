﻿// <copyright file="TestUsingXUnitFramework_Part13.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part13_MixInThisMixInThat
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;
    using C4B.Castle.Core.Demo.DPTutorial.Model;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part13
    {
        private ProxyGenerator generator = new ProxyGenerator();

        /// <summary>
        /// 混合Person和Dictionary.
        /// </summary>
        /// <returns>Person.</returns>
        public Person Mixin_person_and_dictionary()
        {
            // 我们不能有多个基类，但是我们可以实现多个接口，这就是Dynamic Proxy用于mixin的方法。
            /*我们将Person类与Dictionary类混合在一起（如果您想将通用信息存储附加到一个人，这可能会很有用）。
             * 为此，我们需要ProxyGenerationOptions。我们使用AddMixinInstance方法将mixin添加到选项中，传递我们想要使用的字典。
             * 接下来，我们使用mixin的选项为Person类创建类代理。
             */
            var options = new ProxyGenerationOptions();
            options.AddMixinInstance(new Dictionary<string, object>());
            return (Person)this.generator.CreateClassProxy(typeof(Person), new[] { typeof(ISpeak) }, options);
        }

        /// <summary>
        /// 测试混合的对象的代理.
        /// </summary>
        public void TestMixinObjectProxy()
        {
            var person = this.Mixin_person_and_dictionary();
            var dictionary = person as IDictionary;
            dictionary.Add("Next Leave", DateTime.Now.AddMonths(4));
            this.UseSomewhereElse(person);
        }

        private void UseSomewhereElse(Person person)
        {
            var dictionary = person as IDictionary<string, object>;
            var date = ((DateTime)dictionary["Next Leave"]).Date;
            Console.WriteLine("Next leave date of {0} is {1}", person.Name, date);
        }
    }
}
