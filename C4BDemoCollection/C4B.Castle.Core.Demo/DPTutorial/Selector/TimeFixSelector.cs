﻿// <copyright file="TimeFixSelector.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Selector
{
    using System;
    using System.Linq;
    using System.Reflection;
    using C4B.Castle.Core.Demo.DPTutorial.Interceptor;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 修复时间拦截器选择器.用于Part9演示.为被拦截的方法选择合适的拦截器.
    /// </summary>
    public class TimeFixSelector : IInterceptorSelector
    {
        /*我们尝试从给定的字符串中解析出日期和时间，如果成功，我们将其转换为UTC值。
         * 如果我们不能解析字符串，我们让底层实现（即密封的TimeHelper类）处理它。
         * 我们比捕获异常的实现可能抛出，而不是提供默认的返回值。
         */

        private static readonly MethodInfo[] MethodsToAdjust = new[]
        {
            typeof(ITimeHelper).GetMethod("GetHour"),
            typeof(ITimeHelper).GetMethod("GetMinute"),
        };

        private CheckNullInterceptor checkNull = new CheckNullInterceptor();
        private AdjustTimeToUtcInterceptor utcAdjust = new AdjustTimeToUtcInterceptor();

        /// <inheritdoc/>
        public IInterceptor[] SelectInterceptors(Type type, MethodInfo method, IInterceptor[] interceptors)
        {
            /*拦截器检查方法是否是容易出现时区错误的已知方法之一，并在给定拦截器数组的开头添加适当的拦截器。
             * 记住，秩序很重要。这就是为什么CheckNullInterceptor被放在第一位，这样每个后续的拦截器都不必检查参数是否为null。
             */
            if (!MethodsToAdjust.Contains(method))
            {
                return new IInterceptor[] { this.checkNull }.Union(interceptors).ToArray();
            }

            return new IInterceptor[] { this.checkNull, this.utcAdjust }.Union(interceptors).ToArray();
        }
    }
}
