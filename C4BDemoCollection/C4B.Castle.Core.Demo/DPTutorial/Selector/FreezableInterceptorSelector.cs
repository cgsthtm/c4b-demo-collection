﻿// <copyright file="FreezableInterceptorSelector.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Selector
{
    using System;
    using System.Linq;
    using System.Reflection;
    using C4B.Castle.Core.Demo.DPTutorial.Interceptor;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// FreezableInterceptorSelector继承IInterceptorSelector接口.
    /// IInterceptorSelector接口只有一个SelectInterceptors方法.
    /// </summary>
    public class FreezableInterceptorSelector : IInterceptorSelector
    {
        /*SelectInterceptors方法:
         * 对于每个被拦截的方法，都会调用该方法。它接收有关代理类型、方法和向代理注册的所有拦截器的数组的信息。
         * 它将根据这些信息采取行动，并返回它希望用于该方法的拦截器。
         * 此外，虽然ProxyGenerationHook的ShouldInterceptMethod方法只被调用一次，但在生成代理类型时，
         * 会为该类型的每个实例调用Interceptor的SelectInterceptors方法，就在第一次调用该方法之前。
         */

        /// 选择拦截器.
        /// <inheritdoc/>
        public IInterceptor[] SelectInterceptors(Type type, MethodInfo method, IInterceptor[] interceptors)
        {
            // 所以，我们需要检查给定的方法，看看它是否是一个属性设置器，如果不是，不返回FreezableInterceptor。
            if (this.IsSetter(method))
            {
                return interceptors;
            }

            return interceptors.Where(i => !(i is FreezableInterceptor)).ToArray(); // 返回不是FreezableInterceptor的拦截器
        }

        private bool IsSetter(MethodInfo method)
        {
            return method.IsSpecialName && method.Name.StartsWith("set_", StringComparison.Ordinal);
        }
    }
}