﻿// <copyright file="Freezable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using C4B.Castle.Core.Demo.DPTutorial.Interceptor;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;
    using C4B.Castle.Core.Demo.DPTutorial.Selector;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// Freezable.
    /// </summary>
    public static class Freezable
    {
        /*Freezable类的一个设计缺陷:
         * InstanceMap为了完成它的工作，它保存了对它创建的每个可冻结对象的硬引用。如果您只创建了少量的可冻结对象，
         * 并且我们希望这些对象在应用程序运行的整个时间内都是活动的，那么这显然不是什么大问题。
         * 然而，在大多数情况下，我们创建的对象是瞬态的，我们创建了很多对象。从这个角度来看-我们有一个内存泄漏。
         * Freezable类持有对象的引用，因此垃圾收集器无法收集它们并回收它们占用的内存，即使我们可能不再使用这些对象。
         */
        private static readonly IDictionary<object, IFreezable> InstanceMap = new Dictionary<object, IFreezable>();
        private static readonly ProxyGenerator Generator = new ProxyGenerator();

        // Part5-InterceptorSelector
        private static readonly IInterceptorSelector Selector = new FreezableInterceptorSelector();

        /// <summary>
        /// 判断目标对象是否可冻结.
        /// </summary>
        /// <param name="obj">目标对象.</param>
        /// <returns>是否可冻结.</returns>
        public static bool IsFreezable(object obj)
        {
            return obj != null && InstanceMap.ContainsKey(obj);
        }

        /// <summary>
        /// 判断目标对象是否可冻结.重构IsFreezable方法.
        /// </summary>
        /// <param name="obj">目标对象.</param>
        /// <returns>是否可冻结.</returns>
        public static bool IsFreezable_Part4(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var hack = obj as IProxyTargetAccessor;
            if (hack == null)
            {
                return false;
            }

            return hack.GetInterceptors().Count(i => i is IFreezable) > 0;
        }

        /// <summary>
        /// 判断目标对象是否可冻结.重构IsFreezable方法.
        /// </summary>
        /// <param name="obj">目标对象.</param>
        /// <returns>是否可冻结.</returns>
        public static bool IsFreezable_Part4_2(object obj)
        {
            return AsFreezable(obj) != null;
        }

        /// <summary>
        /// 将目标对象冻结.
        /// </summary>
        /// <param name="freezable">目标对象.</param>
        /// <exception cref="NotFreezableObjectException">不可冻结对象异常.</exception>
        public static void Freeze(object freezable)
        {
            if (!IsFreezable(freezable))
            {
                throw new NotFreezableObjectException(freezable);
            }

            InstanceMap[freezable].Freeze();
        }

        /// <summary>
        /// 将目标对象冻结.
        /// </summary>
        /// <param name="freezable">目标对象.</param>
        /// <exception cref="NotFreezableObjectException">不可冻结对象异常.</exception>
        public static void Freeze_Part4(object freezable)
        {
            var interceptor = AsFreezable(freezable);
            if (interceptor == null)
            {
                throw new NotFreezableObjectException(freezable);
            }

            interceptor.Freeze();
        }

        /// <summary>
        /// 判断对象是否已冻结.
        /// </summary>
        /// <param name="freezable">可冻结的对象.</param>
        /// <returns>是否已冻结.</returns>
        public static bool IsFrozen(object freezable)
        {
            return IsFreezable(freezable) && InstanceMap[freezable].IsFrozen;
        }

        /// <summary>
        /// 判断对象是否已冻结.
        /// </summary>
        /// <param name="obj">可冻结的对象.</param>
        /// <returns>是否已冻结.</returns>
        public static bool IsFrozen_Part4(object obj)
        {
            var freezable = AsFreezable(obj);
            return freezable != null && freezable.IsFrozen;
        }

        /// <summary>
        /// 制造可冻结对象.Part1-使用CreateClassProxy函数创建代理,其参数为拦截器.
        /// </summary>
        /// <typeparam name="TFreezable">泛型对象.</typeparam>
        /// <returns>可冻结对象的代理.</returns>
        public static TFreezable MakeFreezable_Part1<TFreezable>()
            where TFreezable : class, new()
        {
            var freezableInterceptor = new FreezableInterceptor();
            var proxy = Generator.CreateClassProxy<TFreezable>(new CallLoggingInterceptor(), freezableInterceptor);
            InstanceMap.Add(proxy, freezableInterceptor);
            return proxy;
        }

        /// <summary>
        /// 制造可冻结对象.Part3-使用ProxyGenerationOptions-FreezableProxyGenerationHook.
        /// </summary>
        /// <typeparam name="TFreezable">泛型对象.</typeparam>
        /// <returns>可冻结对象的代理.</returns>
        public static TFreezable MakeFreezable_Part3<TFreezable>()
            where TFreezable : class, new()
        {
            var freezableInterceptor = new FreezableInterceptor();
            var options = new ProxyGenerationOptions(new FreezableProxyGenerationHook());
            ////var proxy = Generator.CreateClassProxy<TFreezable>(options, new CallLoggingInterceptor(), freezableInterceptor);
            var proxy = Generator.CreateClassProxy(typeof(TFreezable), options, new CallLoggingInterceptor(), freezableInterceptor);
            InstanceMap.Add(proxy, freezableInterceptor);
            return (TFreezable)proxy;
        }

        /// <summary>
        /// 制造可冻结对象.Part4-BreakingHardDependencies.
        /// </summary>
        /// <typeparam name="TFreezable">泛型对象.</typeparam>
        /// <returns>可冻结对象的代理.</returns>
        public static TFreezable MakeFreezable_Part4<TFreezable>()
            where TFreezable : class, new()
        {
            var freezableInterceptor = new FreezableInterceptor();
            var options = new ProxyGenerationOptions(new FreezableProxyGenerationHook());
            ////var proxy = Generator.CreateClassProxy<TFreezable>(options, new CallLoggingInterceptor(), freezableInterceptor);
            var proxy = Generator.CreateClassProxy(typeof(TFreezable), options, new CallLoggingInterceptor(), freezableInterceptor);
            return (TFreezable)proxy;
        }

        /// <summary>
        /// 制造可冻结对象.Part5-InterceptorSelector.
        /// </summary>
        /// <typeparam name="TFreezable">泛型对象.</typeparam>
        /// <returns>可冻结对象的代理.</returns>
        public static TFreezable MakeFreezable_Part5<TFreezable>()
            where TFreezable : class, new()
        {
            var freezableInterceptor = new FreezableInterceptor();
            var options = new ProxyGenerationOptions(new FreezableProxyGenerationHook()) { Selector = Selector };
            var proxy = Generator.CreateClassProxy(typeof(TFreezable), options, new CallLoggingInterceptor(), freezableInterceptor);
            return proxy as TFreezable;
        }

        /// <summary>
        /// 制造可冻结对象.Part7-KindsOfProxyObjects.
        /// </summary>
        /// <param name="ctorArguments">构造函数参数.</param>
        /// <typeparam name="TFreezable">泛型对象.</typeparam>
        /// <returns>可冻结对象的代理.</returns>
        public static TFreezable MakeFreezable_Part7<TFreezable>(params object[] ctorArguments)
            where TFreezable : class // 我们删除了new（）约束，因为显然不需要它。
        {
            var freezableInterceptor = new FreezableInterceptor();
            var options = new ProxyGenerationOptions(new FreezableProxyGenerationHook()) { Selector = Selector };
            var proxy = Generator.CreateClassProxy(
                classToProxy: typeof(TFreezable),
                additionalInterfacesToProxy: new Type[0],
                options: options,
                constructorArguments: ctorArguments,
                new CallLoggingInterceptor(),
                freezableInterceptor);
            return proxy as TFreezable;
        }

        private static IFreezable AsFreezable(object target)
        {
            if (target == null)
            {
                return null;
            }

            /*Part4:
             * 即使我们修复了内存泄漏问题，它仍然不是最好的解决方案。
             * 也就是说，IProxyTargetAccessor是一个有用的接口，当你需要它的时候，知道它在那里是很好的，
             * 但大多数时候你不需要它，几乎在每一种情况下，都有一个更好的方法来实现你的目标，而不使用接口。
             * 它主要用于低级别的框架基础结构代码，如果在其他任何地方使用，您应该将其视为警告标志。
             */
            var hack = target as IProxyTargetAccessor;
            if (hack == null)
            {
                return null;
            }

            return hack.GetInterceptors().FirstOrDefault(i => i is FreezableInterceptor) as IFreezable;
        }
    }
}
