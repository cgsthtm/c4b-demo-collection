﻿// <copyright file="TestUsingXUnitFramework_Part4.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part4_BreakingHardDependencies
{
    using System;
    using C4B.Castle.Core.Demo.DPTutorial.Model;
    using global::Castle.DynamicProxy;
    using Xunit;

    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part4
    {
        /// <summary>
        /// 城堡动态代理框架生成的每个代理都实现了IProxyTargetAccessor接口,
        /// 而IProxyTargetAccessor的DynProxyGetTarget函数应该返回代理他自己.
        /// </summary>
        [Fact]
        public void DynProxyGetTarget_should_return_proxy_itself()
        {
            var pet = Freezable.MakeFreezable_Part3<Pet>();
            var hack = pet as IProxyTargetAccessor;
            Assert.NotNull(hack);
            Assert.Same(pet, hack.DynProxyGetTarget());
        }

        /// <summary>
        /// 可冻结的对象不应该保持任何对创建对象的引用.
        /// </summary>
        [Fact]
        public void Freezable_should_not_hold_any_reference_to_created_objects()
        {
            var pet = Freezable.MakeFreezable_Part3<Pet>();
            var petWeakReference = new WeakReference(pet, false);
            pet = null;
            GC.Collect();
            Assert.False(petWeakReference.IsAlive, "Object should have been collected");
        }
    }
}
