﻿// <copyright file="TimeFix.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part9_InterfaceProxyWithTarget
{
    using C4B.Castle.Core.Demo.DPTutorial.Interface;
    using C4B.Castle.Core.Demo.DPTutorial.Selector;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// TimeFix-将所有代理创建逻辑封装在自己的类中可能是一个好主意.
    /// </summary>
    public class TimeFix
    {
        private ProxyGenerator proxyGenerator = new ProxyGenerator();
        private ProxyGenerationOptions generationOptions = new ProxyGenerationOptions { Selector = new TimeFixSelector() };

        /// <summary>
        /// 修复.
        /// </summary>
        /// <param name="item">ITimeHelper目标.</param>
        /// <returns>修复后的ITimeHelper目标代理.</returns>
        public ITimeHelper Fix(ITimeHelper item)
        {
            return (ITimeHelper)this.proxyGenerator.CreateInterfaceProxyWithTarget(typeof(ITimeHelper), item, this.generationOptions);
        }
    }
}
