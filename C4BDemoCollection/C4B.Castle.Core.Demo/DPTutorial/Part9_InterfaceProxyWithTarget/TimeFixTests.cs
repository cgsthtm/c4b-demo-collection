﻿// <copyright file="TimeFixTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part9_InterfaceProxyWithTarget
{
    using System;
    using C4B.Castle.Core.Demo.DPTutorial.Implement;
    using C4B.Castle.Core.Demo.DPTutorial.Interface;
    using Xunit;

    /// <summary>
    /// 修复TimerHelper的类.
    /// </summary>
    public class TimeFixTests
    {
        /*UTC的英文全称是 "Coordinated Universal Time"，中文通常翻译为“协调世界时”。
         * 它是目前国际上使用最广泛的时间标准，基于原子时秒长定义，
         * 同时通过引入闰秒调整以使其与地球自转的平均太阳时（即世界时，World Time，简称UT）保持一致，差异不超过0.9秒。
         * 协调世界时被用来作为全球性的参照时间，用于众多领域，例如国际通信、航空、航海、科学计算等，
         * 以确保不同地理位置之间的操作能够同步进行。
         */
        private ITimeHelper sut;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeFixTests"/> class.
        /// </summary>
        public TimeFixTests()
        {
            /*这是一个简化的例子，但希望现在你能理解带目标的动态代理是如何工作的，
             * 它们与我们到目前为止讨论的其他类型的代理（类代理和没有目标的接口代理）有何不同，以及如何以及何时可以使用它们。
             */
            var fix = new TimeFix();
            this.sut = fix.Fix(new TimeHelper());
        }

        /// <summary>
        /// GetMinute对于null应该返回0.
        /// </summary>
        [Fact]
        public void GetMinute_should_return_0_for_null()
        {
            int minute = this.sut.GetMinute(null);
            int second = this.sut.GetSecond(null);
            int hour = this.sut.GetHour(null);
            Assert.Equal(0, minute);
            Assert.Equal(0, second);
            Assert.Equal(0, hour);
        }

        /// <summary>
        /// 修复GetHour-正确地处理非UTC时间.
        /// </summary>
        [Fact]
        public void Fixed_GetHour_properly_handles_non_utc_time()
        {
            var dateTimeOffset = new DateTimeOffset(2009, 10, 11, 09, 32, 11, TimeSpan.FromHours(-4.5));
            DateTimeOffset utcTime = dateTimeOffset.ToUniversalTime();
            string noUtcTime = dateTimeOffset.ToString(); // 2009-10-11 09:32:11
            string utcTimeStr = utcTime.ToString();       // 2009-10-11 14:02:11
            int utcHour = this.sut.GetHour(noUtcTime);
            Assert.Equal(utcTime.Hour, utcHour);
        }

        /// <summary>
        /// 修复GetMinute-正确地处理非UTC时间.
        /// </summary>
        [Fact]
        public void Fixed_GetMinute_properly_handles_non_utc_time()
        {
            var dateTimeOffset = new DateTimeOffset(2009, 10, 11, 09, 32, 11, TimeSpan.FromMinutes(45));
            DateTimeOffset utcTime = dateTimeOffset.ToUniversalTime();
            string noUtcTime = dateTimeOffset.ToString();

            int utcMinute = this.sut.GetMinute(noUtcTime);
            Assert.Equal(utcTime.Minute, utcMinute);
        }

        /// <summary>
        /// 修复GetHour-处理无效的格式.
        /// </summary>
        [Fact]
        public void Fixed_GetHour_hadles_entries_in_invalid_format()
        {
            int result = this.sut.GetHour("BOGUS ARGUMENT");
            Assert.Equal(0, result);
        }
    }
}
