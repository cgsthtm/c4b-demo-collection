﻿// <copyright file="TestUsingXUnitFramework_Part9.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part9_InterfaceProxyWithTarget
{
    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part9
    {
        private TimeFixTests timeFixTests;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestUsingXUnitFramework_Part9"/> class.
        /// </summary>
        public TestUsingXUnitFramework_Part9()
        {
            this.timeFixTests = new TimeFixTests();
        }

        /// <summary>
        /// GetMinute对于null应该返回0.
        /// </summary>
        public void GetMinute_should_return_0_for_null()
        {
            this.timeFixTests.GetMinute_should_return_0_for_null();
        }

        /// <summary>
        /// 修复GetHour-正确地处理非UTC时间.
        /// </summary>
        public void Fixed_GetHour_properly_handles_non_utc_time()
        {
            this.timeFixTests.Fixed_GetHour_properly_handles_non_utc_time();
        }

        /// <summary>
        /// 修复GetMinute-正确地处理非UTC时间.
        /// </summary>
        public void Fixed_GetMinute_properly_handles_non_utc_time()
        {
            this.timeFixTests.Fixed_GetMinute_properly_handles_non_utc_time();
        }

        /// <summary>
        /// 修复GetHour-处理无效的格式.
        /// </summary>
        public void Fixed_GetHour_hadles_entries_in_invalid_format()
        {
            this.timeFixTests.Fixed_GetHour_hadles_entries_in_invalid_format();
        }
    }
}
