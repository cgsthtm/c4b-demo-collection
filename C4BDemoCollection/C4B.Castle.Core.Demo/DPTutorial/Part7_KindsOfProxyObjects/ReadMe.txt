﻿除了类代理，代理生成器类能够创建三种不同类型的接口代理：
1、Proxy with target. 代理与目标。这个很容易解释。我们想代理一个接口。由于接口不能独立存在，我们需要一个实现它的类。
该类的实例就是目标。它与我们一直在讨论的类代理非常相似。
2、Proxy without target. 没有目标的代理。这个很棘手您不提供接口的目标实现。动态代理在运行时为您生成它，使用拦截器为其方法提供行为。
3、Proxy with target interface. 具有目标接口的代理。这一点与带目标的代理非常相似。您必须为接口提供目标实现。
不同的是，你可以稍后交换它，并使该方法在其他对象上被调用。

CreateClassProxy
CreateInterfaceProxyWithoutTarget
CreateInterfaceProxyWithTarget
CreateInterfaceProxyWithTargetInterface