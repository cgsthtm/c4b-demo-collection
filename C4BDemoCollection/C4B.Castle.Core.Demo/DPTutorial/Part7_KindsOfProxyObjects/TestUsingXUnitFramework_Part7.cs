﻿// <copyright file="TestUsingXUnitFramework_Part7.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part7_KindsOfProxyObjects
{
    using C4B.Castle.Core.Demo.DPTutorial.Model;
    using Xunit;

    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part7
    {
        /// <summary>
        /// 可冻结的应该能够调用非默认的构造函数.
        /// </summary>
        [Fact]
        public void Freezable_should_be_able_to_call_nonDefault_constructor()
        {
            var dog = Freezable.MakeFreezable_Part7<Dog>("Rex");
            Assert.Equal("Rex", dog.Name);
        }
    }
}
