﻿// <copyright file="ObjectFrozenException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial
{
    using System;

    /// <summary>
    /// 对象已冻结异常.
    /// </summary>
    public class ObjectFrozenException : Exception
    {
    }
}
