﻿// <copyright file="TestUsingXUnitFramework_Part14.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DPTutorial.Part14_PersistingProxies
{
    using System;
    using System.Reflection;
    using C4B.Castle.Core.Demo.DPTutorial.Model;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 使用xUnit框架测试.
    /// </summary>
    public class TestUsingXUnitFramework_Part14
    {
        private ModuleScope scope;
        private string weakModulePath;

        /// <summary>
        /// 持久化代理类.
        /// </summary>
        public void Persist_ProxyClass()
        {
            // 如果我们想持久化代理，我们必须编写更多的代码：
            var savePhysicalAssembly = true;
            var disableSignedModule = true;
            var strongAssemblyName = ModuleScope.DEFAULT_ASSEMBLY_NAME;
            var strongModulePath = ModuleScope.DEFAULT_FILE_NAME;
            var weakAssemblyName = $"Foo.Bar.Proxies";   // DEFAULT_ASSEMBLY_NAME
            this.weakModulePath = $"{Environment.CurrentDirectory}\\Foo.Bar.Proxies.dll"; // DEFAULT_FILE_NAME
            this.scope = new ModuleScope(savePhysicalAssembly, disableSignedModule, strongAssemblyName, strongModulePath, weakAssemblyName, weakModulePath);
            var builder = new DefaultProxyBuilder(this.scope);
            var generator = new ProxyGenerator(builder);
            object dogProxy = generator.CreateClassProxy(typeof(Dog), new[] { "小白" }); // 必须先生成一个代理，才能用scope.SaveAssembly(false)，否则报错

            /*我们必须自己创建ModuleScope（稍后我们将使用它将程序集保存到磁盘），将true作为第一个参数传递，
             * 以告诉DynamicProxy我们希望持久化具有生成类型的程序集。如果我们不这样做，我们将得到一个异常，如果我们试图保存程序集。
             * 然后我们传递两对程序集名称/文件名-强命名版本和弱命名版本。如果您打算只使用一个（就像在这个示例中，我将只保存弱名称程序集），
             * 您可以传递定义为ModuleScope类型上的常量的强类型版本的默认值。
             */

            this.scope.SaveAssembly(false); // 参数值为false意味着我们希望保存没有强名称的程序集。

            // 在调用之后，程序集安全地放置在构造函数中提供的名称下的指定文件夹中。
            // 如果你想的话，你现在可以启动Reflector并查看代理类型的内部工作原理。
        }

        /// <summary>
        /// 加载程序集.
        /// </summary>
        public void LoadAssembly()
        {
            Assembly proxyAssembly = this.GetProxyAssembly();

            // 此方法可用于加载以前生成的和持久化的代理类型从磁盘到此范围的类型缓存，例如，以避免性能与代理生成相关联的命中值
            this.scope.LoadAssemblyIntoCache(proxyAssembly);
        }

        /// <summary>
        /// 检查是否生成任何新类型.
        /// </summary>
        public void CheckIfGenerateAnyNewType()
        {
            // ObtainDynamicModuleWithWeakName方法被移除了?
        }

        private Assembly GetProxyAssembly()
        {
            return Assembly.LoadFrom(this.weakModulePath);
        }
    }
}
