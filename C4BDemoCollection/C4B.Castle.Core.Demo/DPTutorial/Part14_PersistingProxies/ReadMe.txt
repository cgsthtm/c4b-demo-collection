﻿哇，我计划的几个部分教程已经变成了动态代理功能的全面检查。
在所有最重要的特性中，我们基本上只剩下一个代理持久性，这就是我们今天要讨论的。

虽然动态代理的名字表明它对在运行时动态创建代理很有用，但在其他情况下该框架也很有用。
上次我们创建mixin的时候就看到过这样一个场景，根本没有使用代理。

此外，代理的动态方面并不总是我们想要的。这在服务器应用程序中并不明显，
因为应用程序启动一次，然后运行（希望）很长一段时间而不重新启动。
然而，在桌面应用程序中，我们可能会发现自己在每次用户启动应用程序时都会一次又一次地创建许多相同的代理。

从用户的角度来看，这可能会降低应用程序的感知，因为它将花费大量的时间来启动。
当您有许多代理类型时，由于BCL（Base Class Library，即基础类库）中的错误，情况可能会变得非常糟糕。
我称之为bug，但它表现为非线性地增加了创建每个后续代理类型所需的时间。

到目前为止，我们一直在使用默认构造函数创建ProxyGenerator实例。
如果我们想持久化代理，我们必须编写更多的代码：
var savePhysicalAssembly = true;
var strongAssemblyName = ModuleScope.DEFAULT_ASSEMBLY_NAME;
var strongModulePath = ModuleScope.DEFAULT_FILE_NAME;
var weakAssemblyName = "Foo.Bar.Proxies";
var weakModulePath = "Foo.Bar.Proxies.dll";
var scope = new ModuleScope(savePhysicalAssembly, strongAssemblyName, strongModulePath, weakAssemblyName, weakModulePath);
var builder = new DefaultProxyBuilder(scope);
var generator = new ProxyGenerator(builder);

我们必须自己创建ModuleScope（稍后我们将使用它将程序集保存到磁盘），将true作为第一个参数传递，
以告诉DynamicProxy我们希望持久化具有生成类型的程序集。如果我们不这样做，我们将得到一个异常，如果我们试图保存程序集。
然后我们传递两对程序集名称/文件名-强命名版本和弱命名版本。
如果您打算只使用一个（就像在这个示例中，我将只保存弱名称程序集），您可以传递定义为ModuleScope类型上的常量的强类型版本的默认值。

这是任务的第一部分。然后我们使用生成器来创建我们需要的代理类型，当我们完成时（很可能在关闭程序时），我们使用范围来保存程序集。
scope.SaveAssembly(false);
参数值为false意味着我们希望保存没有强名称的程序集。

在调用之后，程序集安全地放置在构造函数中提供的名称下的指定文件夹中。
如果你想的话，你现在可以启动Reflector并查看代理类型的内部工作原理。
一个小问题--记住你只能在一个模块作用域上调用一次save。如果你不这样做，动态代理会提醒你：
Assembly 'Foo.Bar.Proxies' has been saved.

我们现在只缺少一个元素，那就是加载保存的类型。就像我说的，找到程序集本身就是你的责任。当你这样做，并决定使用它，你这样做：
Assembly proxyAssembly = GetProxyAssembly();
scope.LoadAssemblyIntoCache(proxyAssembly);
LoadAssemblyIntoCache方法将在程序集中查找代理类型，并将其找到的所有类型添加到该高速缓存中，
以便下次代理生成器请求与缓存类型匹配的类型时，将使用该类型，而不是创建新类型。

这里有两件事要注意：
- 可以将多个程序集加载到模块作用域的缓存中（重复的程序集将被覆盖）。
- 即使已将其他程序集的类型加载到动态程序集的作用域中，也可以保存动态程序集。
  请注意，只有在这个新程序集中生成的类型才会被保存，而不是从其他程序集中加载的类型（这是非常合乎逻辑的）。
如果你想在保存之前检查是否生成了任何新类型，你可以使用下面的代码：
Type[] types = scope.ObtainDynamicModuleWithWeakName().GetTypes();
if(types.Length>0)
{
    //there are new types.
}
