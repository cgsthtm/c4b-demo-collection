﻿// <copyright file="DAHelloWorldTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DictionaryAdapter.HelloWorldExample
{
    using System.Collections;
    using System.Diagnostics;
    using global::Castle.Components.DictionaryAdapter;

    /// <summary>
    /// 测试HelloWorld.
    /// </summary>
    public class DAHelloWorldTest
    {
        private static Hashtable dictionary = null;
        private static DictionaryAdapterFactory factory = null;
        private static IHelloWorld adapter = null;

        /// <summary>
        /// 为IHelloWorld接口创建一个适配器.
        /// </summary>
        public static void CreateAnAdapterForTheInterface()
        {
            dictionary = new Hashtable();
            factory = new DictionaryAdapterFactory();
            adapter = factory.GetAdapter<IHelloWorld>(dictionary);
            dictionary["Message"] = "Hello world!";
        }

        /// <summary>
        /// 读取适配器.
        /// </summary>
        public static void Reading()
        {
            Debug.Assert(adapter.Message == "Hello world!");
        }

        /// <summary>
        /// 写入适配器.
        /// </summary>
        public static void Writing()
        {
            adapter.Message = "你好";
            Debug.Assert(dictionary["Message"] == "你好");
        }
    }
}
