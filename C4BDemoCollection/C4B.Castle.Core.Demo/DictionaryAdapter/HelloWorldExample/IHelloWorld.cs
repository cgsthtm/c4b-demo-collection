﻿// <copyright file="IHelloWorld.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DictionaryAdapter.HelloWorldExample
{
    /// <summary>
    /// IHelloWorld接口.
    /// </summary>
    public interface IHelloWorld
    {
        /// <summary>
        /// Gets or sets Message.
        /// </summary>
        string Message { get; set; }
    }
}
