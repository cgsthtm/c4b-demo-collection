﻿// <copyright file="KeyPostfixAttribute.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DictionaryAdapter.CustomizingAdapterKeys
{
    using global::Castle.Components.DictionaryAdapter;

    /// <summary>
    /// 自定义的键后缀属性.
    /// </summary>
    public class KeyPostfixAttribute : DictionaryBehaviorAttribute, IDictionaryKeyBuilder
    {
        private string postfix;

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyPostfixAttribute"/> class.
        /// </summary>
        /// <param name="keyPostfix">后缀.</param>
        public KeyPostfixAttribute(string keyPostfix)
        {
            this.postfix = keyPostfix;
        }

        /// <inheritdoc/>
        string IDictionaryKeyBuilder.GetKey(IDictionaryAdapter dictionaryAdapter, string key, PropertyDescriptor property)
        {
            return key + this.postfix;
        }
    }
}
