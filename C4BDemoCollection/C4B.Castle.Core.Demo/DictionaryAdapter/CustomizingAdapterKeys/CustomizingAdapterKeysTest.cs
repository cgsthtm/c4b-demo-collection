﻿// <copyright file="CustomizingAdapterKeysTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DictionaryAdapter.CustomizingAdapterKeys
{
    using System;
    using System.Collections;
    using global::Castle.Components.DictionaryAdapter;

    /// <summary>
    /// 自定义适配器的键.
    /// </summary>
    public class CustomizingAdapterKeysTest
    {
        private static Hashtable dictionary = null;
        private static DictionaryAdapterFactory factory = null;
        private static IDAPerson adapter = null;

        /// <summary>
        /// 为IPerson接口创建一个适配器.
        /// </summary>
        public static void CreateAnAdapterForTheInterface()
        {
            dictionary = new Hashtable();
            factory = new DictionaryAdapterFactory();
            adapter = factory.GetAdapter<IDAPerson>(dictionary);
        }

        /// <summary>
        /// 写适配器.
        /// </summary>
        /// <param name="key">键.</param>
        /// <param name="value">值.</param>
        public static void Write(string key, string value)
        {
            dictionary[key] = value;
        }

        /// <summary>
        /// 读适配器.
        /// </summary>
        /// <param name="key">键.</param>
        public static void Read(string key)
        {
            Console.WriteLine(dictionary[key]);
        }
    }
}