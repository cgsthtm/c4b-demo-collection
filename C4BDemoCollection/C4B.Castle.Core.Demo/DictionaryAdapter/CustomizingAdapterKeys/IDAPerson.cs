﻿// <copyright file="IDAPerson.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo.DictionaryAdapter.CustomizingAdapterKeys
{
    using global::Castle.Components.DictionaryAdapter;

    /// <summary>
    /// IPerson接口.
    /// </summary>
    [KeyPrefix("Person")] // adapter.Name == dictionary["PersonName"];
    [TypeKeyPrefix] // adapter.Name == dictionary["C4B.Castle.Core.Demo.DictionaryAdapter.CustomizingAdapterKeys#Name"];
    public interface IDAPerson
    {
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        [Key("PersonId")] // adapter.Name == dictionary["PersonId"];
        [KeyPostfix("Person")] // adapter.Name == dictionary["NamePerson"];
        string Name { get; set; }

        /// <summary>
        /// Gets or sets Full_Name.
        /// </summary>
        [KeySubstitution("_", ".")] // adapter.Full_Name == dictionary["Full.Name"];
        string Full_Name { get; set; }
    }
}
