﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace C4B.Castle.Core.Demo
{
    using System;
    using C4B.Castle.Core.Demo.DictionaryAdapter.CustomizingAdapterKeys;
    using C4B.Castle.Core.Demo.DictionaryAdapter.HelloWorldExample;
    using C4B.Castle.Core.Demo.DP;
    using C4B.Castle.Core.Demo.DP.FreezeObject;
    using C4B.Castle.Core.Demo.DP.ProxyPattern;
    using C4B.Castle.Core.Demo.DP.RestartableFlow.Tests;
    using C4B.Castle.Core.Demo.DPTutorial.DP_NotInterceptingMethodCalls_WhenInvokedFromWithinTheClass;
    using C4B.Castle.Core.Demo.DPTutorial.DP_WhyWontInterceptorsGetCalled_ForEachVirtualMethodCall;
    using C4B.Castle.Core.Demo.DPTutorial.Part1_IntroductionAndProxyGenerator;
    using C4B.Castle.Core.Demo.DPTutorial.Part10_InterfaceProxiesWithTargetInterface;
    using C4B.Castle.Core.Demo.DPTutorial.Part11_WhenOneInterfaceIsNotEnough;
    using C4B.Castle.Core.Demo.DPTutorial.Part12_Caching;
    using C4B.Castle.Core.Demo.DPTutorial.Part13_MixInThisMixInThat;
    using C4B.Castle.Core.Demo.DPTutorial.Part14_PersistingProxies;
    using C4B.Castle.Core.Demo.DPTutorial.Part15_PatternsAndAntipatterns;
    using C4B.Castle.Core.Demo.DPTutorial.Part3_SelectingWhichMethodsToIntercept;
    using C4B.Castle.Core.Demo.DPTutorial.Part5_InterceptorSelector_FineGrainedControlOverProxy;
    using C4B.Castle.Core.Demo.DPTutorial.Part6_HandlingNonVirtualMethods;
    using C4B.Castle.Core.Demo.DPTutorial.Part8_InterfaceProxyWithoutTarget;
    using C4B.Castle.Core.Demo.DPTutorial.Part9_InterfaceProxyWithTarget;
    using global::Castle.DynamicProxy;

    /// <summary>
    /// 程序.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            ////// Castle DynamicProxy tutorial part I: Introduction and ProxyGenerator
            ////TestUsingXUnitFramework_Part1 xUnitTestP1 = new TestUsingXUnitFramework_Part1();
            ////xUnitTestP1.IsFreezable_should_be_false_for_objects_created_with_ctor();

            ////// Castle Dynamic Proxy tutorial part III: Selecting which methods to intercept
            ////TestUsingXUnitFramework_Part3 xUnitTestP3 = new TestUsingXUnitFramework_Part3();
            ////xUnitTestP3.Freezable_should_not_intercept_property_getters();

            // Castle Dynamic Proxy tutorial part V: InterceptorSelector, fine grained control over proxying
            ////TestUsingXUnitFramework_Part5 xUnitTestP5 = new TestUsingXUnitFramework_Part5();
            ////xUnitTestP5.Freezable_should_not_intercept_methods();

            // Castle Dynamic Proxy tutorial part VI: handling non-virtual methods
            ////TestUsingXUnitFramework_Part6 xUnitTestP6 = new TestUsingXUnitFramework_Part6();
            ////xUnitTestP6.Freezable_should_freeze_classes_with_nonVirtual_methods();
            ////xUnitTestP6.Freezable_should_throw_when_trying_to_freeze_classes_with_nonVirtual_setters();

            // Castle Dynamic Proxy tutorial part VIII: Interface proxy without target
            ////TestUsingXUnitFramework_Part8 xUnitTestP8 = new TestUsingXUnitFramework_Part8();
            ////xUnitTestP8.Should_be_able_to_write_interface_with_two_methods();

            // Castle Dynamic Proxy tutorial part IX: Interface proxy with target
            ////TestUsingXUnitFramework_Part9 xUnitTestP9 = new TestUsingXUnitFramework_Part9();
            ////xUnitTestP9.GetMinute_should_return_0_for_null();
            ////xUnitTestP9.Fixed_GetHour_properly_handles_non_utc_time();
            ////xUnitTestP9.Fixed_GetMinute_properly_handles_non_utc_time();
            ////xUnitTestP9.Fixed_GetHour_hadles_entries_in_invalid_format();

            // Castle Dynamic Proxy tutorial part X: Interface proxies with target interface
            ////TestUsingXUnitFramework_Part10 xUnitTestP10 = new TestUsingXUnitFramework_Part10();
            ////xUnitTestP10.Save_should_use_primaryStorage_when_it_is_up();
            ////xUnitTestP10.Save_should_use_secondaryStorage_when_primaryStorage_is_down();
            ////xUnitTestP10.Save_should_go_back_to_primaryStorage_when_is_goes_from_down_to_up();

            // Castle Dynamic Proxy tutorial part XI: When one interface is not enough
            ////TestUsingXUnitFramework_Part11 xUnitTestP11 = new TestUsingXUnitFramework_Part11();
            ////xUnitTestP11.ClassProxy_should_implement_additional_interfaces();
            ////xUnitTestP11.ClassProxy_for_class_already_implementing_additional_interfaces();
            ////xUnitTestP11.InterfaceProxy_should_implement_additional_interfaces();

            // Castle Dynamic Proxy tutorial part XII: caching
            ////TestUsingXUnitFramework_Part12 xUnitTestP12 = new TestUsingXUnitFramework_Part12();
            ////xUnitTestP12.Caching_TwoProxies_SemanticallyIdentical();
            ////xUnitTestP12.Caching_TowGenerators_TowProxies_SemanticallyIdentical_ItDepends();

            // Castle Dynamic Proxy tutorial part XIII: Mix in this, mix in that
            ////TestUsingXUnitFramework_Part13 xUnitTestP13 = new TestUsingXUnitFramework_Part13();
            ////xUnitTestP13.TestMixinObjectProxy();

            // Castle Dynamic Proxy tutorial part XIV: Persisting proxies
            ////TestUsingXUnitFramework_Part14 xUnitTestP14 = new TestUsingXUnitFramework_Part14();
            ////xUnitTestP14.Persist_ProxyClass();
            ////xUnitTestP14.LoadAssembly();
            ////xUnitTestP14.CheckIfGenerateAnyNewType();

            // Castle Dynamic Proxy tutorial part XV: Patterns and Antipatterns
            ////TestUsingXUnitFramework_Part15 xUnitTestP15 = new TestUsingXUnitFramework_Part15();
            ////xUnitTestP15.LeakingThis();

            // Castle Dynamic Proxy not intercepting method calls when invoked from within the class
            ////DP_NotInterceptingMethodCalls_WhenInvokedFromWithinTheClass.Test();
            ////DP_NotInterceptingMethodCalls_WhenInvokedFromWithinTheClass.Test2();

            // Why won't DynamicProxy's interceptor get called for *each* virtual method call?
            ////WhyWontInterceptorsGetCalled_ForEachVirtualMethodCall.Test();
            ////WhyWontInterceptorsGetCalled_ForEachVirtualMethodCall.Test2();

            // Castle DictionaryAdapter-Hello World Example
            ////DAHelloWorldTest.CreateAnAdapterForTheInterface();
            ////DAHelloWorldTest.Reading();
            ////DAHelloWorldTest.Writing();

            // Castle DictionaryAdapter-CustomizingAdapterKeys
            CustomizingAdapterKeysTest.CreateAnAdapterForTheInterface();
            CustomizingAdapterKeysTest.Write("Name", "NameValue");
            CustomizingAdapterKeysTest.Read("Name");

            // Simple enough – notice we need to mark these property and method as virtual,
            // also notice we’ve done nothing else to the class to show it’s going to be used in a proxy scenario.
            var proxy = new ProxyGenerator().CreateClassProxy<MyClass>(new Interceptor());
            proxy.Flag = true;
            proxy.Execute();

            // In this case the proxy is based upon the interface and simply calls the “target” object properties/methods.
            // Hence this forwarding of calls means the target object does not need to have methods/properties marked as virtual.
            var proxy2 = new ProxyGenerator().CreateInterfaceProxyWithoutTarget<IPerson>(new Interceptor2());
            proxy2.FirstName = "Scooby";
            proxy2.LastName = "Doo";

            var proxy3 = (IPerson)new ProxyGenerator().CreateInterfaceProxyWithTarget(typeof(IPerson), new Person(), new Interceptor());
            proxy3.FirstName = "Scooby";
            proxy3.LastName = "Doo";
            proxy3.Change();

            // RestartableFlow not work
            DemoFlowTests demoFlowTests = new DemoFlowTests();
            demoFlowTests.RunStopRestartFlowTest();

            // FreezeObject
            // And with all of that we barely scratched the surface of what can be done with DynamicProxy.
            var rex = Freezable.MakeFreezable<Pet>();
            rex.Name = "Rex";
            Console.WriteLine(Freezable.IsFreezable(rex)
                ? "Rex is freezable!"
                : "Rex is not freezable. Something is not working");
            Console.WriteLine(rex.ToString());
            Console.WriteLine("Add 50 years");
            rex.Age += 50;
            Console.WriteLine("Age: {0}", rex.Age);
            rex.Deceased = true;

            Console.WriteLine("Deceased: {0}", rex.Deceased);
            Freezable.Freeze(rex);
            try
            {
                rex.Age++;
            }
            catch (ObjectFrozenException)
            {
                Console.WriteLine("Oops. it's frozen. Can't change that anymore");
            }

            Console.WriteLine("--- press enter to close");
            Console.ReadLine();

            // Proxy pattern
            ICar car = new ProxyCar(new Driver(15));
            car.DriveCar();
            car = new ProxyCar(new Driver(25));
            car.DriveCar();

            // FreezeObject UnitTest
            UnitTests unitTests = new UnitTests();
            unitTests.Freezable_should_not_intercept_property_getters();
            unitTests.Freezable_should_log_getters_and_setters();
            unitTests.Freezable_should_not_intercept_methods();
            unitTests.Freezable_should_freeze_classes_with_nonVirtual_methods();
            unitTests.Freezable_should_throw_when_trying_to_freeze_classes_with_nonVirtual_setters();
            unitTests.Should_be_able_to_write_interface_with_two_methods();

            PersonFProxy personProxy = new PersonFProxy();
            personProxy.Test();

            Console.ReadLine();
        }
    }
}